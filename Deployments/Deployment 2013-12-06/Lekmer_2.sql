/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 12/6/2013 3:44:43 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraint FK_tModelFragmentEntity_tModelFragment from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]

-- Drop constraint FK_tModelSetting_tModel from [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModel]

-- Add rows to [sitestructure].[tBlockType]
INSERT INTO [sitestructure].[tBlockType] ([BlockTypeId], [Title], [CommonName], [Ordinal], [AvailableForAllPageTypes]) VALUES (1000043, N'Maksuturva Payment', N'MaksuturvaPayment', 0, 1)
INSERT INTO [sitestructure].[tBlockType] ([BlockTypeId], [Title], [CommonName], [Ordinal], [AvailableForAllPageTypes]) VALUES (1000044, N'Maksuturva Response', N'MaksuturvaResponse', 0, 1)
-- Operation applied to 2 rows out of 2

-- Add rows to [template].[tModel]
SET IDENTITY_INSERT [template].[tModel] ON
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName]) VALUES (1000063, 23, N'Maksuturva Payment', N'MaksuturvaPayment')
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName]) VALUES (1000064, 23, N'Maksuturva Response', N'MaksuturvaResponse')
SET IDENTITY_INSERT [template].[tModel] OFF
-- Operation applied to 2 rows out of 2

-- Add rows to [template].[tModelFragmentRegion]
SET IDENTITY_INSERT [template].[tModelFragmentRegion] ON
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000277, 1000063, N'Head', 10)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000278, 1000063, N'Content', 20)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000279, 1000063, N'Footer', 30)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000280, 1000064, N'Head', 10)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000281, 1000064, N'Content', 20)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000282, 1000064, N'Footer', 30)
SET IDENTITY_INSERT [template].[tModelFragmentRegion] OFF
-- Operation applied to 6 rows out of 6

-- Add rows to [template].[tModelFragment]
SET IDENTITY_INSERT [template].[tModelFragment] ON
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001335, 1000277, N'Head', N'Head', 1000063, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001336, 1000278, N'Content', N'Content', 1000063, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001337, 1000279, N'Footer', N'Footer', 1000063, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001338, 1000278, N'Field', N'Field', 1000063, 20, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001339, 1000024, N'Maksuturva Sub Payment', N'MaksuturvaSubPayment', 1000009, 40, 20)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001340, 1000280, N'Head', N'Head', 1000064, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001341, 1000281, N'Content', N'Content', 1000064, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001342, 1000282, N'Footer', N'Footer', 1000064, 0, 10)
SET IDENTITY_INSERT [template].[tModelFragment] OFF
-- Operation applied to 8 rows out of 8

-- Add rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001255, 1000030, 2, N'isMaksuturvaPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001256, 1000030, 1, N'Iterate:MaksuturvaSubPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001257, 1001339, 2, N'IsSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001258, 1001339, 2, N'IsVisible', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001259, 1001339, 1, N'MaksuturvaSubPayment.Title', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001260, 1001339, 1, N'MaksuturvaSubPayment.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001261, 1001336, 1, N'Maksuturva.Payment.Url', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001262, 1001336, 1, N'Iterate:Field', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001263, 1001338, 1, N'Maksuturva.Field.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001264, 1001338, 1, N'Maksuturva.Field.Value', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001265, 1001339, 1, N'MaksuturvaSubPayment.Form.Name', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
-- Operation applied to 11 rows out of 11

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraint FK_tModelFragmentEntity_tModelFragment to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] WITH CHECK ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])

-- Add constraint FK_tModelSetting_tModel to [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] WITH CHECK ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
COMMIT TRANSACTION
GO
