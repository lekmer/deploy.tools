/****** Object:  Table [orderlek].[tSubPaymentType]    Script Date: 12/06/2013 15:48:41 ******/
SET IDENTITY_INSERT [orderlek].[tSubPaymentType] ON
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (1, 1000007, N'Nordea', N'Nordea E-maksu', N'FI01')
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (2, 1000007, N'DanskeBank', N'Danske Bank Verkkomaksu', N'FI02')
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (3, 1000007, N'Aktia', N'Aktia/Sp/Pop-maksu (Aktia)', N'FI03')
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (4, 1000007, N'POPPankki', N'Aktia/Sp/Pop-maksu (Paikallisosuuspankki)', N'FI04')
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (5, 1000007, N'Tapiola', N'Tapiola Pankki Verkkomaksu', N'FI05')
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (6, 1000007, N'Osuuspankki', N'Osuuspankki Verkkomaksu', N'FI06')
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (7, 1000007, N'Alandsbanken', N'Ålandsbanken E-maksu', N'FI07')
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (8, 1000007, N'Saastopankki', N'Aktia/Sp/Pop-maksu (Säästöpankki)', N'FI08')
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (9, 1000007, N'Handelsbanken', N'Handelsbanken Verkkomaksu', N'FI09')
INSERT [orderlek].[tSubPaymentType] ([SubPaymentTypeId], [PaymentTypeId], [CommonName], [Title], [Code]) VALUES (10, 1000007, N'SPankki', N'S-pankki Verkkomaksu', N'FI10')
SET IDENTITY_INSERT [orderlek].[tSubPaymentType] OFF
