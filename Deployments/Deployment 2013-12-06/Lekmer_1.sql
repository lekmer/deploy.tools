SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [order].[tOrderPayment]'
GO
ALTER TABLE [order].[tOrderPayment] ADD
[MaksuturvaCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vOrderPayment]'
GO
ALTER VIEW [order].[vOrderPayment]
AS
	SELECT
		[OrderPaymentId] AS 'OrderPayment.OrderPaymentId',
		[OrderId] AS 'OrderPayment.OrderId',
		[PaymentTypeId] AS 'OrderPayment.PaymentTypeId',
		[Price] AS 'OrderPayment.Price',
		[Vat] AS 'OrderPayment.Vat',
		[ReferenceId] AS 'OrderPayment.ReferenceId',
		[Captured] AS 'OrderPayment.Captured',
		[MaksuturvaCode] AS 'OrderPayment.MaksuturvaCode'
	FROM
		[order].[tOrderPayment]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tSubPaymentType]'
GO
CREATE TABLE [orderlek].[tSubPaymentType]
(
[SubPaymentTypeId] [int] NOT NULL IDENTITY(1, 1),
[PaymentTypeId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Code] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSubPaymentType] on [orderlek].[tSubPaymentType]'
GO
ALTER TABLE [orderlek].[tSubPaymentType] ADD CONSTRAINT [PK_tSubPaymentType] PRIMARY KEY CLUSTERED  ([SubPaymentTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pSubPaymentTypeGetByPayment]'
GO
CREATE PROCEDURE [orderlek].[pSubPaymentTypeGetByPayment]
	@PaymentTypeId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[tSubPaymentType]
	WHERE
		[PaymentTypeId] = @PaymentTypeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderPaymentSave]'
GO
ALTER PROCEDURE [order].[pOrderPaymentSave]
	@OrderPaymentId INT,
	@OrderId INT,
	@PaymentTypeId INT,
	@Price DECIMAL(16,2),
	@Vat DECIMAL(16,2),
	@ReferenceId VARCHAR(50),
	@Captured BIT = NULL,
	@MaksuturvaCode VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderPayment]
	SET
		[OrderId] = @OrderId,
		[PaymentTypeId] = @PaymentTypeId,
		[Price] = @Price,
		[Vat] = @VAT,
		[ReferenceId] = @ReferenceId,
		[Captured] = @Captured,
		[MaksuturvaCode] = @MaksuturvaCode
	WHERE
		[OrderPaymentId] = @OrderPaymentId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderPayment]
		(
			[OrderId],
			[PaymentTypeId],
			[Price],
			[Vat],
			[ReferenceId],
			[Captured],
			[MaksuturvaCode]
		)
		VALUES
		(
			@OrderId,
			@PaymentTypeId,
			@Price,
			@Vat,
			@ReferenceId,
			@Captured,
			@MaksuturvaCode
		)

		SET @OrderPaymentId = CAST(SCOPE_IDENTITY() AS INT)
	END

	RETURN @OrderPaymentId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [orderlek].[tSubPaymentType]'
GO
ALTER TABLE [orderlek].[tSubPaymentType] ADD CONSTRAINT [UQ_tSubPaymentType_CommonName] UNIQUE NONCLUSTERED  ([CommonName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [orderlek].[tSubPaymentType] ADD CONSTRAINT [UQ_tSubPaymentType_Title] UNIQUE NONCLUSTERED  ([Title])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
