SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tNewsletterUnsubscriberOption]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] DROP CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterUnsubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tNewsletterUnsubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriber] DROP CONSTRAINT [FK_tNewsletterUnsubscriber_tChannel]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tNewsletterUnsubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriber] DROP CONSTRAINT [PK_tNewsletterUnsubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tNewsletterUnsubscriber]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tNewsletterUnsubscriber]
(
[UnsubscriberId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[SentStatus] [bit] NULL,
[CreatedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tNewsletterUnsubscriber] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tNewsletterUnsubscriber]([UnsubscriberId], [ChannelId], [Email], [CreatedDate], [UpdatedDate]) SELECT [UnsubscriberId], [ChannelId], [Email], [CreatedDate], [UpdatedDate] FROM [lekmer].[tNewsletterUnsubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tNewsletterUnsubscriber] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[lekmer].[tNewsletterUnsubscriber]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[lekmer].[tmp_rg_xx_tNewsletterUnsubscriber]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tNewsletterUnsubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tNewsletterUnsubscriber]', N'tNewsletterUnsubscriber'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tNewsletterUnsubscriber] on [lekmer].[tNewsletterUnsubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriber] ADD CONSTRAINT [PK_tNewsletterUnsubscriber] PRIMARY KEY CLUSTERED  ([UnsubscriberId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vNewsletterUnsubscriber]'
GO


ALTER VIEW [lekmer].[vNewsletterUnsubscriber]
AS
SELECT
	[UnsubscriberId] AS 'NewsletterUnsubscriber.UnsubscriberId',
	[ChannelId] AS 'NewsletterUnsubscriber.ChannelId',
	[Email] AS 'NewsletterUnsubscriber.Email',
	[SentStatus] AS 'NewsletterUnsubscriber.SentStatus',
	[CreatedDate] AS 'NewsletterUnsubscriber.CreatedDate',
	[UpdatedDate] AS 'NewsletterUnsubscriber.UpdatedDate'
FROM
	[lekmer].[tNewsletterUnsubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriber_SetSentStatus]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_SetSentStatus]
	@UnubscriberId INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE 
		[lekmer].[tNewsletterUnsubscriber]
	SET
		[SentStatus] = 1
	WHERE
		[UnsubscriberId] = @UnubscriberId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductIsDeleted]'
GO
CREATE PROCEDURE [productlek].[pProductIsDeleted]
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	IF EXISTS (SELECT 1 FROM [product].[tProduct] WHERE [ProductId] = @ProductId AND [IsDeleted] = 1)
	BEGIN
		RETURN 1
	END

	RETURN 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriber_GetAllWithNotSentStatus]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_GetAllWithNotSentStatus]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vNewsletterUnsubscriber] nu
	WHERE
		nu.[NewsletterUnsubscriber.SentStatus] = 0
		OR 
		nu.[NewsletterUnsubscriber.SentStatus] IS NULL
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductTagsDeleteByCampaign]'
GO
ALTER PROCEDURE [lekmer].[pProductTagsDeleteByCampaign]
	@CampaignId	INT
AS
BEGIN
	DECLARE @TagsToDelete TABLE (ProductTagId INT)
	INSERT INTO @TagsToDelete
	SELECT [pt].[ProductTagId]
	FROM [lekmer].[tProductTag] pt
		  INNER JOIN [lekmer].[tProductTagUsage] ptu ON [ptu].[ProductTagId] = [pt].[ProductTagId]
		  WHERE [ptu].[ObjectId] = @CampaignId
			    AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTagUsage] ptu1
								WHERE [ptu1].[ProductTagId] = [pt].[ProductTagId]
								HAVING COUNT(1) > 1)

	--EXEC [lekmer].[pProductTagUsageDelete] NULL, '', @CampaignId
	DELETE ptu
	FROM [lekmer].[tProductTagUsage] ptu
	WHERE ptu.[ObjectId] = @CampaignId
	
	DELETE pt
	FROM [lekmer].[tProductTag] pt
	INNER JOIN @TagsToDelete d ON [d].[ProductTagId] = [pt].[ProductTagId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pReportOrdersGetAllByDate]'
GO

CREATE PROCEDURE [integration].[pReportOrdersGetAllByDate]
	@ChannelId INT,
	@DateFrom DATETIME,
	@DateTo DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		o.[OrderId] AS 'Order.ID',
		o.[Email] AS 'Order.Email',
		o.[CreatedDate] AS 'Order.CreatedDate',
		op.[Price] + o.[FreightCost] + ISNULL(lo.[OptionalFreightCost], 0) + lo.[PaymentCost] AS 'Order.TotalValue'
	FROM
		[order].[tOrder] o
		INNER JOIN [order].[tOrderPayment] op ON op.[OrderId] = o.[OrderId]
		INNER JOIN [lekmer].[tLekmerOrder] lo ON lo.[OrderId] = o.[OrderId]
	WHERE
		o.[ChannelId] = @ChannelId
		AND
		o.[CreatedDate] > @DateFrom
		AND
		o.[CreatedDate] < @DateTo
		AND
		o.[OrderStatusId] = 4
	ORDER BY
		o.[OrderId]

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[pRatingReviewFeedback_Search]'
GO

ALTER PROCEDURE [review].[pRatingReviewFeedback_Search]
	@Author					NVARCHAR(50),
	@Message				NVARCHAR(500),
	@StatusId				INT,
	@CreatedFrom			DATETIME,
	@CreatedTo				DATETIME,
	@ProductId				INT,
	@ProductTitle			NVARCHAR(256),
	@OrderId				INT,
	@ChannelId				INT,
	@RatingId				INT,
	@InappropriateContent	BIT,
	@SortBy					VARCHAR(50) = NULL,
	@SortDescending			BIT = NULL,
	@Page					INT = NULL,
	@PageSize				INT
AS   
BEGIN
	SET NOCOUNT ON
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)
     
    SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' + COALESCE(@SortBy, 'RatingReviewFeedback.CreatedDate')+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			rr.*
		FROM
			[review].[vReviewRecord] rr ' 
			+ CASE WHEN (@RatingId IS NOT NULL) THEN + 'INNER JOIN [review].[tRatingItemProductVote] ri ON rr.[RatingReviewFeedback.RatingReviewFeedbackId] = ri.[RatingReviewFeedbackId] AND ri.[RatingId] = @RatingId ' ELSE '' END
			+'
		WHERE
			(@ProductId IS NULL OR rr.[RatingReviewFeedback.ProductId] = @ProductId)'
			+ CASE WHEN (@ProductTitle IS NOT NULL) THEN + 'AND rr.[ReviewRecord.Product.Title] LIKE ''%' + REPLACE(@ProductTitle, '''', '''''') + '%''' ELSE '' END
			+ CASE WHEN (@StatusId IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.RatingReviewStatusId] = @StatusId ' ELSE '' END
			+ CASE WHEN (@Message IS NOT NULL) THEN + 'AND (rr.[Review.Title] LIKE ''%' + REPLACE(@Message, '''', '''''') + '%'' OR rr.[Review.Message] LIKE ''%' + REPLACE(@Message, '''', '''''') + '%'')' ELSE '' END
			+ CASE WHEN (@Author IS NOT NULL) THEN + 'AND rr.[Review.AuthorName] = @Author ' ELSE '' END
			+ CASE WHEN (@OrderId IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.OrderId] = @OrderId ' ELSE '' END
			+ CASE WHEN (@ChannelId IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.ChannelId] = @ChannelId ' ELSE '' END
			+ CASE WHEN (@InappropriateContent = 1) THEN + 'AND rr.[RatingReviewFeedback.Impropriate] = 1 ' ELSE '' END
			+ CASE WHEN (@CreatedFrom IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.CreatedDate] >= @CreatedFrom ' ELSE '' END
			+ CASE WHEN (@CreatedTo IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.CreatedDate] <= @CreatedTo ' ELSE '' END

     SET @sql = 'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

     IF @Page != 0 AND @Page IS NOT NULL 
     BEGIN
		SET @sql = @sql + '
		WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
     END
     
     SET @sqlCount = 'SELECT COUNT(1) FROM (' + @sqlFragment + ') AS CountResults'
     
	EXEC sp_executesql @sqlCount,
		N'      
			@Author					NVARCHAR(50),
			@Message				NVARCHAR(500),
			@StatusId				INT,
			@CreatedFrom			DATETIME,
            @CreatedTo				DATETIME,
            @ProductId				INT,
            @ProductTitle			NVARCHAR(256),
            @OrderId				INT,
			@ChannelId				INT,
			@RatingId				INT,
            @Page					INT,
            @PageSize				INT',
            @Author,
            @Message,
            @StatusId,
            @CreatedFrom,
            @CreatedTo,
            @ProductId,
            @ProductTitle,
            @OrderId,
			@ChannelId,
			@RatingId,
            @Page,
            @PageSize
                             
	EXEC sp_executesql @sql, 
		N'      
			@Author					NVARCHAR(50),
			@Message				NVARCHAR(500),
			@StatusId				INT,
			@CreatedFrom			DATETIME,
            @CreatedTo				DATETIME,
            @ProductId				INT,
            @ProductTitle			NVARCHAR(256),
            @OrderId				INT,
			@ChannelId				INT,
			@RatingId				INT,
            @Page					INT,
            @PageSize				INT',
            @Author,
            @Message,
            @StatusId,
            @CreatedFrom,
            @CreatedTo,
            @ProductId,
            @ProductTitle,
            @OrderId,
			@ChannelId,
			@RatingId,
            @Page,
            @PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tNewsletterUnsubscriberOption]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] ADD CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterUnsubscriber] FOREIGN KEY ([UnsubscriberId]) REFERENCES [lekmer].[tNewsletterUnsubscriber] ([UnsubscriberId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tNewsletterUnsubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriber] ADD CONSTRAINT [FK_tNewsletterUnsubscriber_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
