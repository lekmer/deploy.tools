﻿SELECT * FROM [order].[tDeliveryMethod]
SELECT * FROM [orderlek].[tDeliveryMethod]
SELECT * FROM [order].[tDeliveryMethodPrice]
SELECT * FROM [order].[tPaymentTypeDeliveryMethod]
SELECT * FROM [order].[tChannelDeliveryMethod]


INSERT INTO [order].[tDeliveryMethod] ( [CommonName], [Title], [ErpId], [TrackingUrl] )
VALUES ( 'BusinessParcel',  N'Företagspaket', '100',  NULL )

INSERT INTO [orderlek].[tDeliveryMethod] ( [DeliveryMethodId], [Priority], [IsCompany] )
VALUES ( 1,  20, 0 ), ( 1000001,  10, 0 ), ( 1000002,  30, 1 )

INSERT INTO [order].[tDeliveryMethodPrice] ( [CountryId], [CurrencyId], [DeliveryMethodId], [FreightCost] )
VALUES ( 1,  1,  1000002,  599.00 )

INSERT INTO [order].[tPaymentTypeDeliveryMethod] ( [PaymentTypeId], [DeliveryMethodId] )
VALUES 
( 1,        1000002 ), 
( 1000002,  1000002 ), 
( 1000003,  1000002 ), 
( 1000004,  1000002 ), 
( 1000005,  1000002 )

INSERT INTO [order].[tChannelDeliveryMethod] ( [ChannelId], [DeliveryMethodId] )
VALUES ( 1, 1000002 )