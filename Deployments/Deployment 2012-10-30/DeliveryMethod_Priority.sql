SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [orderlek].[tDeliveryMethod]'
GO
CREATE TABLE [orderlek].[tDeliveryMethod]
(
[DeliveryMethodId] [int] NOT NULL,
[Priority] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tDeliveryMethod] on [orderlek].[tDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tDeliveryMethod] ADD CONSTRAINT [PK_tDeliveryMethod] PRIMARY KEY CLUSTERED  ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vDeliveryMethod]'
GO

CREATE VIEW [orderlek].[vDeliveryMethod]
AS
	SELECT
		[DeliveryMethodId] AS 'DeliveryMethod.DeliveryMethodId',
		[Priority] AS 'DeliveryMethod.Priority'
	FROM
		[orderlek].[tDeliveryMethod]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomDeliveryMethod]'
GO


ALTER VIEW [order].[vCustomDeliveryMethod]
AS
	SELECT
		[dm].*,
		CAST((CASE WHEN [cddm].[ChannelDefaultDeliveryMethod.DeliveryMethodId] IS NULL THEN 0 ELSE 1 END) AS BIT) AS 'DeliveryMethod.ChannelDefault',
		[ldm].[DeliveryMethod.Priority]
	FROM
		[order].[vDeliveryMethod] dm
		LEFT JOIN [orderlek].[vDeliveryMethod] ldm ON [ldm].[DeliveryMethod.DeliveryMethodId] = [dm].[DeliveryMethod.DeliveryMethodId]
		LEFT JOIN [orderlek].[vChannelDefaultDeliveryMethod] cddm 
			ON [cddm].[ChannelDefaultDeliveryMethod.ChannelId] = [dm].[DeliveryMethod.ChannelId]
			AND [cddm].[ChannelDefaultDeliveryMethod.DeliveryMethodId] = [dm].[DeliveryMethod.DeliveryMethodId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pDeliveryMethodGetAllByPaymentType]'
GO
ALTER PROCEDURE [order].[pDeliveryMethodGetAllByPaymentType]
	@ChannelId INT,
	@PaymentTypeId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dm.*
	FROM 
		[order].[vCustomDeliveryMethod] dm
		INNER JOIN [order].[tPaymentTypeDeliveryMethod] ptdm ON ptdm.DeliveryMethodId = dm.[DeliveryMethod.DeliveryMethodId]
	WHERE
		ptdm.PaymentTypeId = @PaymentTypeId
		AND dm.[DeliveryMethod.ChannelId] = @ChannelId
	ORDER BY
		dm.[DeliveryMethod.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pDeliveryMethodGetAll]'
GO
ALTER PROCEDURE [order].[pDeliveryMethodGetAll]
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomDeliveryMethod]
	WHERE
		[DeliveryMethod.ChannelId] = @ChannelId
	ORDER BY
		[DeliveryMethod.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tDeliveryMethod] ADD CONSTRAINT [FK_tDeliveryMethod(lek)_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
