SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP CONSTRAINT[FK_tLekmerCartItem_tProductSize]
ALTER TABLE [lekmer].[tLekmerCartItem] DROP CONSTRAINT[FK_tLekmerCartItem_tCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] DROP CONSTRAINT[FK_tCartItem_tCart]
ALTER TABLE [order].[tCartItem] DROP CONSTRAINT[FK_tCartItem_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP CONSTRAINT [PK_tLekmerCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP CONSTRAINT [DF_tLekmerCartItem_IsAffectedByCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] DROP CONSTRAINT [PK_tCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] DROP CONSTRAINT [DF_tCartItem_Quantity]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tCartItem] from [order].[tCartItem]'
GO
DROP INDEX [IX_tCartItem] ON [order].[tCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [integration].[usp_UpdateSizeStockLekmer]'
GO
DROP PROCEDURE [integration].[usp_UpdateSizeStockLekmer]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD
[Weight] [decimal] (18, 3) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tDeliveryMethod]'
GO
CREATE TABLE [orderlek].[tDeliveryMethod]
(
[DeliveryMethodId] [int] NOT NULL,
[Priority] [int] NOT NULL,
[IsCompany] [bit] NOT NULL CONSTRAINT [DF_tDeliveryMethod_IsCompany] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tDeliveryMethod] on [orderlek].[tDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tDeliveryMethod] ADD CONSTRAINT [PK_tDeliveryMethod] PRIMARY KEY CLUSTERED  ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tLekmerCartItem]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tLekmerCartItem]
(
[CartItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[IsAffectedByCampaign] [bit] NOT NULL CONSTRAINT [DF_tLekmerCartItem_IsAffectedByCampaign] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tLekmerCartItem]([CartItemId], [ProductId], [SizeId], [ErpId], [IsAffectedByCampaign]) SELECT [CartItemId], [ProductId], [SizeId], [ErpId], [IsAffectedByCampaign] FROM [lekmer].[tLekmerCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tLekmerCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tLekmerCartItem]', N'tLekmerCartItem'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tLekmerCartItem] on [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [PK_tLekmerCartItem] PRIMARY KEY CLUSTERED  ([CartItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [order].[tCartItem]'
GO
CREATE TABLE [order].[tmp_rg_xx_tCartItem]
(
[CartItemId] [int] NOT NULL IDENTITY(1, 1),
[CartId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Quantity] [int] NOT NULL CONSTRAINT [DF_tCartItem_Quantity] DEFAULT ((1)),
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [order].[tmp_rg_xx_tCartItem] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [order].[tmp_rg_xx_tCartItem]([CartItemId], [CartId], [ProductId], [Quantity], [CreatedDate]) SELECT [CartItemId], [CartId], [ProductId], [Quantity], [CreatedDate] FROM [order].[tCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [order].[tmp_rg_xx_tCartItem] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[order].[tCartItem]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[order].[tmp_rg_xx_tCartItem]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [order].[tCartItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[order].[tmp_rg_xx_tCartItem]', N'tCartItem'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItem] on [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] ADD CONSTRAINT [PK_tCartItem] PRIMARY KEY CLUSTERED  ([CartItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tCartItem] on [order].[tCartItem]'
GO
CREATE NONCLUSTERED INDEX [IX_tCartItem] ON [order].[tCartItem] ([CartId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] ADD
[Weight] [decimal] (18, 3) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFreightValueActionDeliveryMethod]'
GO
CREATE TABLE [campaignlek].[tFreightValueActionDeliveryMethod]
(
[CartActionId] [int] NOT NULL,
[DeliveryMethodId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFreightValueActionDeliveryMethod] on [campaignlek].[tFreightValueActionDeliveryMethod]'
GO
ALTER TABLE [campaignlek].[tFreightValueActionDeliveryMethod] ADD CONSTRAINT [PK_tFreightValueActionDeliveryMethod] PRIMARY KEY CLUSTERED  ([CartActionId], [DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tDeliveryMethodWeightPrice]'
GO
CREATE TABLE [orderlek].[tDeliveryMethodWeightPrice]
(
[CountryId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[DeliveryMethodId] [int] NOT NULL,
[WeightFrom] [decimal] (18, 3) NULL,
[WeightTo] [decimal] (18, 3) NULL,
[FreightCost] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tChannelDefaultDeliveryMethod]'
GO
CREATE TABLE [orderlek].[tChannelDefaultDeliveryMethod]
(
[ChannelId] [int] NOT NULL,
[DeliveryMethodId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ChannelDefaultDeliveryMethod] on [orderlek].[tChannelDefaultDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tChannelDefaultDeliveryMethod] ADD CONSTRAINT [PK_ChannelDefaultDeliveryMethod] PRIMARY KEY CLUSTERED  ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO



ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	[product].[fProductHasSizes](ProductId) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations',
	[Weight] AS 'Lekmer.Weight'
FROM
	lekmer.tLekmerProduct AS p

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProduct]'
GO








ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vDeliveryMethodWeightPrice]'
GO
CREATE VIEW [orderlek].[vDeliveryMethodWeightPrice]
AS
	SELECT
		*
	FROM
		[orderlek].[tDeliveryMethodWeightPrice]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pDeliveryMethodWeightPriceGetAllByDeliveryMethod]'
GO
CREATE PROCEDURE [orderlek].[pDeliveryMethodWeightPriceGetAllByDeliveryMethod]
	@DeliveryMethodId	INT,
	@ChannelId			INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM [orderlek].[vDeliveryMethodWeightPrice] dmwp
		 INNER JOIN [core].[tChannel] ch ON [ch].[CurrencyId] = [dmwp].[CurrencyId]
										 AND [ch].[CountryId] = [dmwp].[CountryId]
	WHERE
		[dmwp].[DeliveryMethodId] = @DeliveryMethodId
		AND [ch].[ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vChannelDefaultDeliveryMethod]'
GO
CREATE VIEW [orderlek].[vChannelDefaultDeliveryMethod]
AS
	SELECT
		[ChannelId] 'ChannelDefaultDeliveryMethod.ChannelId',
		[DeliveryMethodId] 'ChannelDefaultDeliveryMethod.DeliveryMethodId'
	FROM
		[orderlek].[tChannelDefaultDeliveryMethod]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vDeliveryMethod]'
GO



CREATE VIEW [orderlek].[vDeliveryMethod]
AS
	SELECT
		[DeliveryMethodId] AS 'DeliveryMethod.DeliveryMethodId',
		[Priority] AS 'DeliveryMethod.Priority',
		[IsCompany] AS 'DeliveryMethod.IsCompany'
	FROM
		[orderlek].[tDeliveryMethod]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomDeliveryMethod]'
GO



ALTER VIEW [order].[vCustomDeliveryMethod]
AS
	SELECT
		[dm].*,
		CAST((CASE WHEN [cddm].[ChannelDefaultDeliveryMethod.DeliveryMethodId] IS NULL THEN 0 ELSE 1 END) AS BIT) AS 'DeliveryMethod.ChannelDefault',
		[ldm].[DeliveryMethod.Priority],
		[ldm].[DeliveryMethod.IsCompany]
	FROM
		[order].[vDeliveryMethod] dm
		LEFT JOIN [orderlek].[vDeliveryMethod] ldm ON [ldm].[DeliveryMethod.DeliveryMethodId] = [dm].[DeliveryMethod.DeliveryMethodId]
		LEFT JOIN [orderlek].[vChannelDefaultDeliveryMethod] cddm 
			ON [cddm].[ChannelDefaultDeliveryMethod.ChannelId] = [dm].[DeliveryMethod.ChannelId]
			AND [cddm].[ChannelDefaultDeliveryMethod.DeliveryMethodId] = [dm].[DeliveryMethod.DeliveryMethodId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pDeliveryMethodGetAll]'
GO
ALTER PROCEDURE [order].[pDeliveryMethodGetAll]
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomDeliveryMethod]
	WHERE
		[DeliveryMethod.ChannelId] = @ChannelId
	ORDER BY
		[DeliveryMethod.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vProductSize]'
GO

ALTER VIEW [lekmer].[vProductSize]
AS
SELECT     
	ps.[ProductId] AS 'ProductSize.ProductId',
	ps.[SizeId] AS 'ProductSize.SizeId',
	ps.[ErpId] AS 'ProductSize.ErpId',
	ps.[NumberInStock] AS 'ProductSize.NumberInStock',
	ps.[MillimeterDeviation] AS 'ProductSize.MillimeterDeviation',
	ps.[OverrideEU] AS 'ProductSize.OverrideEU',
	ps.[OverrideMillimeter] AS 'ProductSize.OverrideMillimeter',
	[ps].[Weight] AS 'ProductSize.Weight',
	s.*
FROM
	[lekmer].[tProductSize] ps
	INNER JOIN [lekmer].[vSize] s ON ps.[SizeId] = s.[Size.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO






ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.LekmerErpId],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId',
		NULL AS 'Lekmer.RecommendedPrice'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryRestrictionProductGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryRestrictionProductGetAllByProduct]
	@ProductId		INT
AS
BEGIN
	SELECT
		[rp].*, [pmc].[ChannelId]
	FROM 
		[lekmer].[tProductRegistryRestrictionProduct] rp
		INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rp].[ProductRegistryId]
	WHERE
		[rp].[ProductId] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryRestrictionBrandGetAllByBrand]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryRestrictionBrandGetAllByBrand]
	@BrandId		INT
AS
BEGIN
	SELECT
		[rb].*, [pmc].[ChannelId]
	FROM 
		[lekmer].[tProductRegistryRestrictionBrand] rb
		INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rb].[ProductRegistryId]
	WHERE
		[rb].[BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductView]'
GO




ALTER VIEW [product].[vCustomProductView]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFreightValueActionDeliveryMethodInsert]'
GO
CREATE PROCEDURE [campaignlek].[pFreightValueActionDeliveryMethodInsert]
	@ActionId INT,
	@DeliveryMethodId INT
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [campaignlek].[tFreightValueActionDeliveryMethod] (
		[CartActionId],
		[DeliveryMethodId]
	)
	VALUES (
		@ActionId,
		@DeliveryMethodId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductRecord]'
GO







ALTER VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight]
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductViewWithoutStatusFilter]'
GO



ALTER VIEW [lekmer].[vCustomProductViewWithoutStatusFilter]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductViewWithoutStatusFilter] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderSave]'
GO
ALTER PROCEDURE [order].[pOrderSave]
	@OrderId INT,
	@Number NVARCHAR(30),
	@CustomerId INT,
	@BillingAddressId INT,
	@DeliveryAddressId INT,
	@CreatedDate DATETIME,
	@FreightCost DECIMAL(16,2),
	@Email VARCHAR(320),
	@DeliveryMethodId INT,
	@ChannelId INT,
	@OrderStatusId int = 1,
	@UsedAlternate bit = 0,
	@IP varchar(15),
	@DeliveryTrackingId varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	update
		[order].[tOrder]
	set
		Number = @Number,            
		CustomerId = @CustomerId,
		BillingAddressId = @BillingAddressId,
		DeliveryAddressId = @DeliveryAddressId,
		CreatedDate = @CreatedDate,
		FreightCost = @FreightCost,
		Email = @Email,
		DeliveryMethodId = @DeliveryMethodId,
		ChannelId = @ChannelId,
		OrderStatusId = @OrderStatusId,
		IP = @IP,
		DeliveryTrackingId = @DeliveryTrackingId
	where
		OrderId = @OrderId

	if @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrder]
		(
			Number,
			CustomerId,
			BillingAddressId,
			DeliveryAddressId,
			CreatedDate,
			FreightCost,
			Email,
			DeliveryMethodId,
			ChannelId,
			OrderStatusId,
			UsedAlternate,
			IP,
			DeliveryTrackingId
		)
		VALUES
		(
			@Number,
			@CustomerId,
			@BillingAddressId,
			@DeliveryAddressId,
			@CreatedDate,
			@FreightCost,
			@Email,
			@DeliveryMethodId,
			@ChannelId,
			@OrderStatusId,
			@UsedAlternate,
			@IP,
			@DeliveryTrackingId
		)
		SET @OrderId = SCOPE_IDENTITY();
	END
	RETURN @OrderId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vFreightValueActionCurrency]'
GO

CREATE VIEW [campaignlek].[vFreightValueActionCurrency]
AS
SELECT
	[fvdm].[CartActionId] AS 'FreightValueAction.ActionId',
	[fvdm].[DeliveryMethodId] AS 'FreightValueAction.DeliveryMethodId'
FROM
	[campaignlek].[tFreightValueActionDeliveryMethod] fvdm
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductWithoutStatusFilter]'
GO



ALTER VIEW [lekmer].[vCustomProductWithoutStatusFilter]
AS 
	SELECT
		p.*,
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.[ParentContentNodeId] AS 'Product.ParentContentNodeId',
		i.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductWithoutStatusFilter] p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] AS ssmc ON ssmc.[ChannelId] = c.[Channel.Id]
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON p.[Product.Id] = pssr.[ProductId] AND ssmc.[SiteStructureRegistryId] = pssr.[SiteStructureRegistryId]
		LEFT JOIN [media].[vCustomImage] AS i ON i.[Image.MediaId] = p.[Product.MediaId] AND i.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]'
GO
ALTER PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	SELECT
		[p].[ProductId] 'Product.Id', 
		[p].[ItemsInPackage] 'Product.ItemsInPackage',
		[p].[ErpId] 'Product.ErpId',
		[p].[EanCode] 'Product.EanCode',
		[p].[NumberInStock] 'Product.NumberInStock', 
		[p].[CategoryId] 'Product.CategoryId',
		COALESCE([pt].[WebShopTitle], [p].WebShopTitle) 'Product.WebShopTitle',
		COALESCE([pt].[Title], [p].Title) 'Product.Title',
		[p].[ProductStatusId] 'Product.ProductStatusId',
		COALESCE([pt].[Description], [p].[Description]) 'Product.Description',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription]) 'Product.ShortDescription',
		@ChannelId 'Product.ChannelId', 
		@CurrencyId 'Product.CurrencyId',
		@PriceListRegistryId 'Product.PriceListRegistryId',
		[i].*,
		[pssr].[ParentContentNodeId] 'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId] 'Product.TemplateContentNodeId',
		[lp].[Lekmer.BrandId],
		[lp].[Lekmer.IsBookable],
		[lp].[Lekmer.AgeFromMonth],
		[lp].[Lekmer.AgeToMonth],
		[lp].[Lekmer.IsNewFrom],
		[lp].[Lekmer.IsNewTo],
		COALESCE ([lpt].[Product.Measurement], [lp].[Lekmer.Measurement]) 'Lekmer.Measurement',
		[lp].[Lekmer.BatteryTypeId],
		[lp].[Lekmer.NumberOfBatteries],
		[lp].[Lekmer.IsBatteryIncluded],
		[lp].[Lekmer.ExpectedBackInStock],
		[lp].[Lekmer.CreatedDate],
		[lp].[Lekmer.SizeDeviationId],
		[lp].[Lekmer.HasSizes],
		[lp].[Lekmer.LekmerErpId],
		[lp].[Lekmer.ShowVariantRelations],
		[lp].[Lekmer.Weight],
		[bt].*,
		[sd].*,
		[pu].[ProductUrl.UrlTitle] 'Lekmer.UrlTitle',
		[rp].[Price] 'Lekmer.RecommendedPrice',
		[pli].*
	FROM
		[product].[tProduct] AS p
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON [lpt].[Product.Id] = [lp].[Lekmer.ProductId] AND [lpt].[Language.Id] = @LanguageId
		LEFT JOIN [lekmer].[vBatteryType] bt ON [bt].[BatteryType.Id] = [lp].[Lekmer.BatteryTypeId]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON [sd].[SizeDeviation.Id] = [lp].[Lekmer.SizeDeviationId]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl	ON [pl].[Id] = [p].[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateLekmerProduct]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateLekmerProduct]
AS
BEGIN
	-- Update tLekmerProduct
	UPDATE 
		[lp]
	SET 
		[lp].[ExpectedBackInStock] =  NULL
	FROM 
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tLekmerProduct] lp				ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
		INNER JOIN [product].[tProduct] p					ON [p].[ProductId] = [lp].[ProductId]
		INNER JOIN [product].[tProductRegistryProduct] pli	ON [pli].[ProductId] = [lp].[ProductId]
																AND [pli].[ProductRegistryId] = SUBSTRING([tp].[HYarticleId], 3, 1)
	WHERE 
		[lp].[ExpectedBackInStock] <> LEN([lp].[ExpectedBackInStock])
		AND [tp].[NoInStock] > 0
	
	-- Update tLekmerProduct
	UPDATE 
		[lp]
	SET 
		[lp].[LekmerErpId] = [tp].[LekmerArtNo],
		[lp].[Weight] = CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
	FROM 
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
	WHERE
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND
			(
				[lp].[LekmerErpId] IS NULL
				OR [lp].[LekmerErpId] <> [tp].[LekmerArtNo]
				OR [lp].[Weight] IS NULL
				OR [lp].[Weight] <> CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
			)
			
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[pCdonExportRestrictionCategoryGetAll]'
GO
ALTER PROCEDURE [export].[pCdonExportRestrictionCategoryGetAll]
AS
BEGIN
	SELECT 
		DISTINCT [src].[CategoryId],
		[rc].[ProductRegistryId],
		[rc].[RestrictionReason],
		[rc].[UserId],
		[rc].[CreatedDate],
		[pmc].[ChannelId]
	FROM [export].[tCdonExportRestrictionCategory] rc
		 CROSS APPLY [product].[fnGetSubCategories] ([rc].[CategoryId]) src
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductSizesLekmer]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateProductSizesLekmer]
	@HYChannel NVARCHAR(40),
	@ProductId INT,
	@HYArticleNoColorSize NVARCHAR(50),
	@HYSizeId NVARCHAR (50),
	@HYSizeValue NVARCHAR (50),
	@NoInStock NVARCHAR(250),
	@Weight NVARCHAR (50)
AS
BEGIN

	DECLARE 
		@Data NVARCHAR(4000),
		@SizeId INT,
		@NeedToInsertProductSize BIT
	
	IF @HYChannel = '001' -- Only SE channel
	IF @HYSizeId NOT IN ('000', '100', '223')
	IF NOT EXISTS(SELECT 1 FROM lekmer.tProductSize WHERE ErpId = @HYArticleNoColorSize)
	
	BEGIN TRY
		BEGIN TRANSACTION

		SET @Data = 'tProductSIZE Shoes: @HYArticleNoColorSize ' + @HYArticleNoColorSize + ' @HYChannel ' + @HYChannel + ' @HYSizeValue ' + @HYSizeValue

		SET @NeedToInsertProductSize = 0
		
		SET @SizeId = (SELECT SizeId FROM lekmer.tSize s WHERE s.ErpId = @HYSizeId)
		
		IF @SizeId IS NOT NULL
		SET @NeedToInsertProductSize = 1
		
		IF @NeedToInsertProductSize = 1
		BEGIN
			IF NOT EXISTS (SELECT 1
						   FROM [lekmer].tProductSize ps
						   WHERE ps.ProductId = @ProductId 
						   AND ps.SizeId = @SizeId)
			BEGIN
				INSERT INTO [lekmer].tProductSize (
					ProductId,
					SizeId,
					ErpId,
					NumberInStock,
					[Weight]
				)
				VALUES (
					@ProductId,
					@SizeId,
					@HYArticleNoColorSize,
					@NoInStock,
					CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), @Weight) / 1000)
				)
			END
		END
	
		COMMIT	
	END TRY
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		
		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			@Data,
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)

	END CATCH
		
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pDeliveryMethodGetAll]'
GO

CREATE PROCEDURE [orderlek].[pDeliveryMethodGetAll]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomDeliveryMethod]
	ORDER BY
		[DeliveryMethod.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[usp_UpdateSizeLekmer]'
GO
CREATE PROCEDURE [integration].[usp_UpdateSizeLekmer]
AS
BEGIN

	UPDATE 
		[ps]
	SET 
		[ps].[NumberInStock] = [tp].[NoInStock],
		[ps].[Weight] = CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
	FROM
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tProductSize] ps ON [ps].[ErpId] = SUBSTRING([tp].[HYarticleId], 5, 16)
	WHERE 
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND 
			(
				[ps].[NumberInStock] <> [tp].[NoInStock]
				OR [ps].[Weight] IS NULL
				OR [ps].[Weight] <> CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
			)

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFreightValueActionDeliveryMethodDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pFreightValueActionDeliveryMethodDeleteAll]
	@ActionId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[campaignlek].[tFreightValueActionDeliveryMethod]
	WHERE
		[CartActionId] = @ActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pDeliveryMethodGetAllByPaymentType]'
GO
ALTER PROCEDURE [order].[pDeliveryMethodGetAllByPaymentType]
	@ChannelId INT,
	@PaymentTypeId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dm.*
	FROM 
		[order].[vCustomDeliveryMethod] dm
		INNER JOIN [order].[tPaymentTypeDeliveryMethod] ptdm ON ptdm.DeliveryMethodId = dm.[DeliveryMethod.DeliveryMethodId]
	WHERE
		ptdm.PaymentTypeId = @PaymentTypeId
		AND dm.[DeliveryMethod.ChannelId] = @ChannelId
	ORDER BY
		dm.[DeliveryMethod.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFreightValueActionDeliveryMethodGetIdAll]'
GO

CREATE PROCEDURE [campaignlek].[pFreightValueActionDeliveryMethodGetIdAll]
	@ActionId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT 
		[DeliveryMethodId]
	FROM 
		[campaignlek].[tFreightValueActionDeliveryMethod]
	WHERE 
		[CartActionId] = @ActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryRestrictionCategoryGetAllByCategory]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryRestrictionCategoryGetAllByCategory]
	@CategoryId		INT
AS
BEGIN
	SELECT
		[rc].*, [pmc].[ChannelId]
	FROM 
		[lekmer].[tProductRegistryRestrictionCategory] rc
		INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
	WHERE
		[rc].[CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductLekmer]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateProductLekmer]
AS
BEGIN
	SET NOCOUNT ON 
	
	-- tCategory
	EXEC [integration].[usp_UpdateTCategoryLekmer] -- creates the items if necessary
	
	-- tBrand
	EXEC [integration].[usp_UpdateBrandLekmer]
	EXEC [integration].[usp_UpdateBrandLekmerCreationDate] -- adds the date the brand was created
	
	-- tProduct
	EXEC [integration].[usp_UpdateProductLekmer]
	EXEC [integration].[usp_ImportUpdateLekmerProduct]
	
	-- tPriceListItem
	EXEC [integration].[usp_UpdateProductPRICELekmer]
	
	-- tProductSize
	EXEC [integration].[usp_UpdateSizeLekmer]

	--------------------------------------

	DECLARE 
		@HYArticleNoFull NVARCHAR(50),
		@HYArticleNoColorSize NVARCHAR(50),
		@HYArticleNoColor NVARCHAR(50),
		@HYSizeId NVARCHAR(50),
		@EanCode NVARCHAR(50),
		@NoInStock NVARCHAR(250),
		@ArticleTitle NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@BrandId INT,
		@HYSizeValue NVARCHAR(50),
		@Weight NVARCHAR(50),
		
		@CategoryId INT,
		@Price DECIMAL,
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@LekmerErpId NVARCHAR(25),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL,
		
		@NewFromDate DATETIME,
		@NewToDate DATETIME,

		-- Variables for Channel
		@HYChannel NVARCHAR(40),
		
		@TradeDoublerProductGroup NVARCHAR(50),
		@ChannelId INT,
		@IsProductRegistry BIT,
		@NeedToInsertProductRelationData BIT
	
	SET @Vat = 25.0
	
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 4, @NewFromDate)

	------------------------------------

	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			tp.HYarticleId,
			tp.ArticleTitle,
			tp.SizeId,
			(tp.Price/100),
			tp.NoInStock,
			tp.ArticleGroupId,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.BrandId,
			tp.EanCode,
			tp.LekmerArtNo,
			[tp].[Weight]
		FROM
			[integration].tempProduct tp
		--WHERE NOT EXISTS (SELECT 1
		--				  FROM lekmer.tLekmerProduct lp, product.tPriceListItem prp
		--				  WHERE lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
		--					AND lp.ProductId = prp.ProductId
		--					AND SUBSTRING(tp.HYarticleId, 3,1) = prp.PriceListId)

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@HYArticleNoFull,
			@ArticleTitle,
			@HYSizeValue,
			@Price,
			@NoInStock,
			@VarugruppId,
			@VaruklassId,
			@VarukodId,
			@HYBrandId,
			@EanCode,
			@LekmerErpId,
			@Weight

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			SET @IsProductRegistry = 0
			SET @NeedToInsertProductRelationData = 0
			
			-- Split @HYArticleNoFull
			-- 001-0000001-1017-108
			SET @HYChannel = SUBSTRING(@HYArticleNoFull, 1, 3)              -- [001]-0000001-1017-108
			SET @HYArticleNoColorSize = SUBSTRING(@HYArticleNoFull, 5, 17)  -- 001-[0000001-1017-108]
			SET @HYSizeId = SUBSTRING(@HYArticleNoFull, 18, 3)              -- 001-0000001-1017-[108]
			SET @HYArticleNoColor = SUBSTRING(@HYArticleNoFull, 5, 12)      -- 001-[0000001-1017]-108
		
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYArticleNoColor)
			IF @ProductId IS NOT NULL
			BEGIN
				IF EXISTS (SELECT 1 FROM product.tProductRegistryProduct
						   WHERE ProductId = @ProductId
								 AND ProductRegistryId = @HYChannel) --Dangerous!
				BEGIN
					SET @IsProductRegistry = 1
					GOTO PRODUCT_END
				END
			END
			
			PRODUCT_START:
			
			---- Skip processing the same @HYErpId + @Channel few times
			--IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			--BEGIN
			--	GOTO PRODUCT_END
			--END ELSE BEGIN
			--	SET	@PrevHYErpId = @HYErpId
			--	SET	@PrevChannel = @Channel
			--END
			
			BEGIN TRANSACTION
			
			SET @CategoryId = (SELECT CategoryId 
							   FROM product.tCategory 
							   WHERE ErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, ''))
			
			SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)

			IF @ProductId IS NULL
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNoFull + ' EanCode ' + @EanCode
				
				-- tProduct
				INSERT INTO [product].tProduct (
					ErpId,
					EanCode,
					IsDeleted,
					NumberInStock,
					CategoryId,
					Title,
					[Description],
					ProductStatusId
				)
				VALUES (
					@HYArticleNoColor,
					@EanCode,
					0, -- IsDeleted
					@NoInStock,
					@CategoryId,
					@ArticleTitle, 
					'', -- Description
					1 -- Offline
				)
											
				SET @ProductId = SCOPE_IDENTITY()
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNoFull + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
	
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId,
					HYErpId,
					BrandId,
					IsBookable,
					AgeFromMonth,
					AgeToMonth,
					IsNewFrom,
					IsNewTo,
					IsBatteryIncluded,
					ExpectedBackInStock,
					LekmerErpId,
					ShowVariantRelations,
					[Weight]
				)
				VALUES (
					@ProductId, 
					@HYArticleNoColor,
					@BrandId,
					0, -- IsBookable, 
					0, -- AgeFromMonth
					0, -- AgeToMonth
					@NewFromDate,
					@NewToDate,
					0, -- IsBatteryIncluded
					NULL, --ExpectedBackInStock
					@LekmerErpId,
					1, -- ShowVariantRelations
					CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), @Weight) / 1000)
				)

			END	 -- Product already exist in tLekmerProduct
			ELSE IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem prp WHERE prp.ProductId = @ProductId AND prp.PriceListId = @HYChannel) --Dangerous!(PriceListId)
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'EXISTING: @HYArticleNoColorSize ' + @HYArticleNoColorSize + ' @HYChannel ' + @HYChannel + ' ProductId' + CAST(@ProductId as varchar(10))
			END
			
			IF @NeedToInsertProductRelationData = 1
			BEGIN
				------------------------------------------------------------------------------------------
				-- tProductRegistryProduct
				
				IF NOT EXISTS (SELECT 1 FROM product.tProductRegistryProduct
							   WHERE ProductId = @ProductId
									 AND ProductRegistryId = @HYChannel) --Dangerous!
				BEGIN
					INSERT INTO product.tProductRegistryProduct (
						ProductId,
						ProductRegistryId
					)
					VALUES (
						@ProductId,
						@HYChannel
					)
				END				
				
				------------------------------------------------------------------------------------------
				-- tPriceListItem
				-- PriceListId and @HYChannel have same values

				IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem
							   WHERE ProductId = @ProductId
									 AND PriceListId = @HYChannel) --Dangerous!(PriceListId)
				BEGIN
					INSERT INTO product.tPriceListItem (
						PriceListId,
						ProductId,
						PriceIncludingVat,
						PriceExcludingVat,
						VatPercentage
					)
					VALUES (
						@HYChannel, 
						@ProductId, 
						@Price, 
						@Price / (1.0+@Vat/100.0),
						@Vat
					)
				END

				------------------------------------------------------------------------------------------ 
				-- tTradeDoublerProductGroupMapping

				SET @ChannelId = CAST(@HYChannel AS INT)
				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '')
													   AND ChannelId = @ChannelId)
				
				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId 
										 AND tTDPGM.ChannelId = @ChannelId)
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							@ChannelId
						)
					END
				END 
			END

			COMMIT

			PRODUCT_END:
			
			------------------------------------------------------------------------------------------ 
			-- ProductSize
				
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [integration].[usp_ImportUpdateProductSizesLekmer]
				@HYChannel,
				@ProductId,
				@HYArticleNoColorSize,
				@HYSizeId,
				@HYSizeValue,
				@NoInStock,
				@Weight
			
		END TRY
		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
				
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)

		END CATCH

		FETCH NEXT FROM cur_product 
			INTO 
				@HYArticleNoFull,
				@ArticleTitle,
				@HYSizeValue,
				@Price,
				@NoInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@HYBrandId,
				@EanCode,
				@LekmerErpId,
				@Weight

	END
	
	CLOSE cur_product
	DEALLOCATE cur_product
	
	EXEC [integration].[usp_UpdateProductBrandLekmer]
	--exec [integration].[usp_FokRestrictionsProducts] -- Outdated, soon to be removed, [usp_ChannelRestrictionsProducts] in its place
	EXEC [integration].[usp_ChannelRestrictionsProducts]
	EXEC [integration].[usp_REATagProduct]
	
	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]
	
	-- Track products chnages to eSales export
	EXEC [integration].[pTrackProductChangesLekmer]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tDeliveryMethod] ADD CONSTRAINT [FK_tDeliveryMethod(lek)_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [FK_tLekmerCartItem_tCartItem] FOREIGN KEY ([CartItemId]) REFERENCES [order].[tCartItem] ([CartItemId])
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [FK_tLekmerCartItem_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFreightValueActionDeliveryMethod]'
GO
ALTER TABLE [campaignlek].[tFreightValueActionDeliveryMethod] ADD CONSTRAINT [FK_tFreightValueActionDeliveryMethod_tFreightValueAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tFreightValueAction] ([CartActionId])
ALTER TABLE [campaignlek].[tFreightValueActionDeliveryMethod] ADD CONSTRAINT [FK_tFreightValueActionDeliveryMethod_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tDeliveryMethodWeightPrice]'
GO
ALTER TABLE [orderlek].[tDeliveryMethodWeightPrice] ADD CONSTRAINT [FK_tDeliveryMethodWeightPrice_tCountry] FOREIGN KEY ([CountryId]) REFERENCES [core].[tCountry] ([CountryId])
ALTER TABLE [orderlek].[tDeliveryMethodWeightPrice] ADD CONSTRAINT [FK_tDeliveryMethodWeightPrice_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
ALTER TABLE [orderlek].[tDeliveryMethodWeightPrice] ADD CONSTRAINT [FK_tDeliveryMethodWeightPrice_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [order].[tCartItem]'
GO
ALTER TABLE [order].[tCartItem] ADD CONSTRAINT [FK_tCartItem_tCart] FOREIGN KEY ([CartId]) REFERENCES [order].[tCart] ([CartId])
ALTER TABLE [order].[tCartItem] ADD CONSTRAINT [FK_tCartItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tChannelDefaultDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tChannelDefaultDeliveryMethod] ADD CONSTRAINT [FK_tChannelDefaultDeliveryMethod_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
ALTER TABLE [orderlek].[tChannelDefaultDeliveryMethod] ADD CONSTRAINT [FK_tChannelDefaultDeliveryMethod_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating extended properties'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The active cart created in a user session.', 'SCHEMA', N'order', 'TABLE', N'tCartItem', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of products of this item.', 'SCHEMA', N'order', 'TABLE', N'tCartItem', 'COLUMN', N'Quantity'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
