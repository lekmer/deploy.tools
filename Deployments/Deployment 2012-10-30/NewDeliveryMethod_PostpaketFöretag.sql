﻿SELECT * FROM [order].[tDeliveryMethod]
SELECT * FROM [orderlek].[tDeliveryMethod]
SELECT * FROM [order].[tDeliveryMethodPrice]
SELECT * FROM [order].[tPaymentTypeDeliveryMethod]
SELECT * FROM [order].[tChannelDeliveryMethod]


INSERT INTO [order].[tDeliveryMethod] ( [CommonName], [Title], [ErpId], [TrackingUrl] )
VALUES ( 'PostCompany',  N'PostpaketFöretag', '100',  NULL )

INSERT INTO [orderlek].[tDeliveryMethod] ( [DeliveryMethodId], [Priority], [IsCompany] )
VALUES ( 1000003,  40, 1 )

INSERT INTO [order].[tDeliveryMethodPrice] ( [CountryId], [CurrencyId], [DeliveryMethodId], [FreightCost] )
VALUES ( 1,  1,  1000003,  49.00 )

INSERT INTO [order].[tPaymentTypeDeliveryMethod] ( [PaymentTypeId], [DeliveryMethodId] )
VALUES 
( 1,        1000003 ), 
( 1000002,  1000003 ), 
( 1000003,  1000003 ), 
( 1000004,  1000003 ), 
( 1000005,  1000003 )

INSERT INTO [order].[tChannelDeliveryMethod] ( [ChannelId], [DeliveryMethodId] )
VALUES ( 1, 1000003 )