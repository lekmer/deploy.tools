SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD
[MonitorThreshold] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerCategory]'
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD
[MonitorThreshold] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBrand]'
GO
ALTER TABLE [lekmer].[tBrand] ADD
[MonitorThreshold] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tBlockProductList]'
GO
CREATE TABLE [lekmer].[tBlockProductList]
(
[BlockId] [int] NOT NULL,
[ColumnCountMobile] [int] NULL,
[RowCountMobile] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockProductList] on [lekmer].[tBlockProductList]'
GO
ALTER TABLE [lekmer].[tBlockProductList] ADD CONSTRAINT [PK_tBlockProductList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomBlockProductList]'
GO


ALTER view [product].[vCustomBlockProductList]
as
	select
		pl.*,
		lpl.ColumnCountMobile 'Lekmer.ColumnCountMobile',
		lpl.RowCountMobile 'Lekmer.RowCountMobile'
	from
		[product].[vBlockProductList] pl left join 
		[lekmer].[tBlockProductList] lpl on pl.[BlockProductList.BlockId] = lpl.[BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO



ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	[product].[fProductHasSizes](ProductId) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations',
	[Weight] AS 'Lekmer.Weight',
	[ProductTypeId] AS 'Lekmer.ProductTypeId',
	[IsAbove60L] AS 'Lekmer.IsAbove60L',
	[StockStatusId] AS 'Lekmer.StockStatusId',
	[MonitorThreshold] AS 'Lekmer.MonitorThreshold'
FROM
	lekmer.tLekmerProduct AS p


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProduct]'
GO


ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductView]'
GO


ALTER VIEW [product].[vCustomProductView]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrand]'
GO

ALTER VIEW [lekmer].[vBrand]
AS
	SELECT     
		[B].[BrandId] 'Brand.BrandId',
		COALESCE ([bt].[BrandTranslation.Title], [B].[Title]) 'Brand.Title',
		COALESCE ([bt].[BrandTranslation.Description], [B].[Description]) 'Brand.Description',		
		COALESCE ([bt].[BrandTranslation.PackageInfo], [B].[PackageInfo]) 'Brand.PackageInfo',		
		[B].[ExternalUrl] 'Brand.ExternalUrl',
		[B].[MediaId] 'Brand.MediaId',
		[B].[ErpId] 'Brand.ErpId',
		[B].[MonitorThreshold] 'Brand.MonitorThreshold',
		[Ch].[ChannelId],
		[I].*,
		[Bssr].[ContentNodeId] 'Brand.ContentNodeId'
	FROM
		lekmer.tBrand AS B
		CROSS JOIN core.tChannel AS Ch
		
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = B.MediaId AND I.[Image.LanguageId] = Ch.LanguageId
		LEFT JOIN lekmer.vBrandTranslation AS bt ON bt.[BrandTranslation.BrandId] = B.BrandId AND bt.[BrandTranslation.LanguageId] = Ch.LanguageId
		LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = Ch.ChannelId
		LEFT JOIN lekmer.tBrandSiteStructureRegistry Bssr ON Bssr.BrandId = B.BrandId AND Bssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pContentMessageTranslationGetAll]'
GO
ALTER PROCEDURE [lekmer].[pContentMessageTranslationGetAll]
	@ContentMessageId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
	    [cmt].*
	FROM
	    [lekmer].[vContentMessageTranslation] cmt
	WHERE 
		cmt.[ContentMessageTranslation.ContentMessageId] = @ContentMessageId
	ORDER BY
		[cmt].[ContentMessageTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDescriptionTranslationGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductDescriptionTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.Description] AS 'Value'
	FROM
		lekmer.[vProductTranslation]
	WHERE 
		[Product.Id] = @ProductId
	ORDER BY
		[Language.Id]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductTitleTranslationGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductTitleTranslationGetAllByProduct]
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.Title] AS 'Value'
	FROM
		[lekmer].[vProductTranslation]
	WHERE 
		[Product.Id] = @ProductId
	ORDER BY
		[Language.Id]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategory]'
GO


ALTER VIEW [product].[vCustomCategory]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		COALESCE ([lct].[PackageInfo], [l].[PackageInfo]) AS [LekmerCategory.PackageInfo],
		[l].[MonitorThreshold] AS [LekmerCategory.MonitorThreshold]
	FROM
		[product].[vCategory] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]
		LEFT JOIN [productlek].[tLekmerCategoryTranslation] lct ON [lct].[CategoryId] = [c].[Category.Id] AND [lct].[LanguageId] = [c].[Category.LanguageId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategorySecure]'
GO


ALTER VIEW [product].[vCustomCategorySecure]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		[l].[PackageInfo] AS [LekmerCategory.PackageInfo],
		[l].[MonitorThreshold] AS [LekmerCategory.MonitorThreshold]
	FROM
		[product].[vCategorySecure] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO


ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.IsAbove60L],
		[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId',
		NULL AS 'Lekmer.RecommendedPrice'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[pRatingItemTranslationTitleGetAll]'
GO

ALTER PROCEDURE [review].[pRatingItemTranslationTitleGetAll]
	@RatingItemId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rit.[RatingItemTranslation.RatingItemId] AS 'Id',
		rit.[RatingItemTranslation.LanguageId] AS 'LanguageId',
		rit.[RatingItemTranslation.Title] AS 'Value'
	FROM
	    [review].[vRatingItemTranslation] rit
	WHERE 
		rit.[RatingItemTranslation.RatingItemId] = @RatingItemId
	ORDER BY
		rit.[RatingItemTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[pBlockTitleTranslationGetAllByBlock]'
GO
ALTER PROCEDURE [sitestructure].[pBlockTitleTranslationGetAllByBlock]
@BlockId	int
AS
BEGIN
	set nocount on

	SELECT 
		[BlockTranslation.BlockId] AS 'Id',
		[BlockTranslation.LanguageId] AS 'LanguageId',
		[BlockTranslation.Title] AS 'Value'
	FROM
		[sitestructure].vCustomBlockTranslation
	WHERE 
		[BlockTranslation.BlockId] = @BlockId
	ORDER BY
		[BlockTranslation.LanguageId]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSave]'
GO

ALTER PROCEDURE [lekmer].[pProductSave]
	@ProductId				INT,
	@ItemsInPackage			INT,
	@ErpId					VARCHAR(50),
	@EANCode				VARCHAR(20),
	@Title					NVARCHAR(256),
	@WebShopTitle			NVARCHAR(256),
	@ProductStatusId		INT,
	@Description			NVARCHAR(MAX),
	@ShortDescription		NVARCHAR(MAX),
	@CategoryId				INT,
	@MediaId				INT,
	@BrandId				INT,
	@AgeFromMonth			INT,
	@AgeToMonth				INT,
	@IsBookable				BIT,
	@IsNewFrom				DATETIME,
	@IsNewTo				DATETIME,
	@Measurement			NVARCHAR(MAX),
	@BatteryTypeId			INT,
	@NumberOfBatteries		INT,
	@IsBatteryIncluded		BIT,
	@ExpectedBackInStock	DATETIME,
	@CreatedDate			DATETIME,
	@SizeDeviationId		INT,
	@ShowVariantRelations	BIT,
	@MonitorThreshold		INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tProduct]
	SET
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	WHERE
		[ProductId] = @ProductId
		
	UPDATE
		[lekmer].[tLekmerProduct]
	SET
		[BrandId]				= @BrandId,
		[AgeFromMonth]			= @AgeFromMonth,
		[AgeToMonth]			= @AgeToMonth,
		[IsBookable]			= @IsBookable,
		[IsNewFrom]				= @IsNewFrom,
		[IsNewTo]				= @IsNewTo,
		[Measurement]			= @Measurement,
		[BatteryTypeId]			= @BatteryTypeId,
		[NumberOfBatteries]		= @NumberOfBatteries,
		[IsBatteryIncluded]		= @IsBatteryIncluded,
		[ExpectedBackInStock]	= @ExpectedBackInStock,
		[CreatedDate]			= @CreatedDate,
		[SizeDeviationId]		= @SizeDeviationId,
		[ShowVariantRelations]	= @ShowVariantRelations,
		[MonitorThreshold]		= @MonitorThreshold
	WHERE
		[ProductId] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrandSecure]'
GO

ALTER VIEW [lekmer].[vBrandSecure]
AS
SELECT     
	[B].[BrandId] 'Brand.BrandId',
    [B].[Title] 'Brand.Title',
    [B].[Description] 'Brand.Description',
    [B].[PackageInfo] 'Brand.PackageInfo',
    [B].[ExternalUrl] 'Brand.ExternalUrl',
    [B].[MediaId] 'Brand.MediaId',
    [B].[ErpId] 'Brand.ErpId',
	[B].[MonitorThreshold] 'Brand.MonitorThreshold',
    [I].*,
    NULL 'Brand.ContentNodeId'
FROM
	lekmer.tBrand B
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = B.MediaId
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[pRatingTranslationDescriptionGetAll]'
GO

ALTER PROCEDURE [review].[pRatingTranslationDescriptionGetAll]
	@RatingId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rt.[RatingTranslation.RatingId] AS 'Id',
		rt.[RatingTranslation.LanguageId] AS 'LanguageId',
		rt.[RatingTranslation.Description] AS 'Value'
	FROM
	    [review].[vRatingTranslation] rt
	WHERE 
		rt.[RatingTranslation.RatingId] = @RatingId
	ORDER BY
		rt.[RatingTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductRecord]'
GO


ALTER VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold]
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [core].[pLanguageGetAll]'
GO
ALTER PROCEDURE [core].[pLanguageGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[core].[vCustomLanguage]
	ORDER BY
		[Language.Id]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandSave]'
GO
ALTER PROCEDURE [lekmer].[pBrandSave]
	@BrandId			INT,
	@Title				VARCHAR(500),
	@MediaId			INT = NULL,
	@Description 		VARCHAR(MAX) = NULL,
	@ExternalUrl 		VARCHAR(500) = NULL,
	@PackageInfo 		NVARCHAR(MAX) = NULL,
	@MonitorThreshold	INT = NULL
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM lekmer.tBrand WHERE Title = @Title AND BrandId <> @BrandId)
		RETURN -1

	UPDATE
		[lekmer].[tBrand]
	SET
		[Title] = @Title,	
		[MediaId] = @MediaId,
		[Description] = @Description,
		[ExternalUrl] = @ExternalUrl,
		[PackageInfo] = @PackageInfo,
		[MonitorThreshold] = @MonitorThreshold
	WHERE
		[BrandId] = @BrandId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tBrand]
		( 
			[Title],
			[MediaId],
			[Description],
			[ExternalUrl],
			[PackageInfo],
			[MonitorThreshold]
		)
		VALUES
		(
			@Title,
			@MediaId ,
			@Description,
			@ExternalUrl,
			@PackageInfo,
			@MonitorThreshold
		)
		SET @BrandId = scope_identity()						
	END 
	
	RETURN @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pFlagTranslationGetAllByFlag]'
GO

ALTER PROCEDURE [lekmer].[pFlagTranslationGetAllByFlag]
	@FlagId int
AS
BEGIN
	SET NOCOUNT ON
	SELECT
	    FlagId AS 'Id',
		LanguageId AS 'LanguageId',
		Title AS 'Value'
	FROM
	    lekmer.tFlagTranslation 
	WHERE 
		FlagId = @FlagId
	ORDER BY
		[LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageGeneralInfoTranslationGetAllByPackage]'
GO
ALTER PROCEDURE [productlek].[pPackageGeneralInfoTranslationGetAllByPackage]
	@PackageId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [PackageTranslation.PackageId]   AS 'Id',
		[PackageTranslation.LanguageId]  AS 'LanguageId',
		[PackageTranslation.GeneralInfo] AS 'Value'
	FROM
	    [productlek].[vPackageTranslation] 
	WHERE
		[PackageTranslation.PackageId] = @PackageId
	ORDER BY
		[PackageTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCategoryTranslationGetAllByCategory]'
GO

ALTER PROCEDURE [lekmer].[pCategoryTranslationGetAllByCategory]
@CategoryId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [CategoryId] AS 'Id',
		[LanguageId] AS 'LanguageId',
		[Title] AS 'Value'
	FROM
	    product.tCategoryTranslation where [CategoryId] = @CategoryId
	ORDER BY
		[LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pBrandPackageInfoTranslationGetAllByBrand]'
GO
ALTER PROCEDURE [productlek].[pBrandPackageInfoTranslationGetAllByBrand]
	@BrandId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [BrandTranslation.BrandId] AS 'Id',
		[BrandTranslation.LanguageId] AS 'LanguageId',
		[BrandTranslation.PackageInfo] AS 'Value'
	FROM
	    [lekmer].[vBrandTranslation]
	WHERE
		[BrandTranslation.BrandId] = @BrandId
	ORDER BY
		[BrandTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockProductListSave]'
GO

CREATE procedure [lekmer].[pBlockProductListSave]
	@BlockId			int,
	@ColumnCount		int,
	@RowCount			int,
	@ColumnCountMobile	int,
	@RowCountMobile		int
as
begin
	declare @productAffectedRows int, 
			@lekmerAffectedRows int
	set nocount on

	update
		[product].[tBlockProductList]
	set
		[ColumnCount]	= @ColumnCount,
		[RowCount]		= @RowCount
	where
		[BlockId]		= @BlockId
	set 
		@productAffectedRows = @@ROWCOUNT

	update
		[lekmer].[tBlockProductList]
	set
		[ColumnCountMobile]		= @ColumnCountMobile,
		[RowCountMobile]		= @RowCountMobile
	where
		[BlockId]				= @BlockId
	set 
		@lekmerAffectedRows = @@ROWCOUNT

	if  @productAffectedRows = 0
	begin
		insert
			[product].[tBlockProductList]
			(
				[BlockId],
				[ColumnCount],
				[RowCount]
			)
		values
			(
				@BlockId,
				@ColumnCount,
				@RowCount
			)
	end

	if  @lekmerAffectedRows = 0
	begin
		insert
			[lekmer].[tBlockProductList]
			(
				[BlockId],
				[ColumnCountMobile],
				[RowCountMobile]
			)
		values
			(
				@BlockId,
				@ColumnCountMobile,
				@RowCountMobile
			)
	end
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pCategoryPackageInfoTranslationGetAllByCategory]'
GO
ALTER PROCEDURE [productlek].[pCategoryPackageInfoTranslationGetAllByCategory]
	@CategoryId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [CategoryId] AS 'Id',
		[LanguageId] AS 'LanguageId',
		[PackageInfo] AS 'Value'
	FROM
	    [productlek].[tLekmerCategoryTranslation] 
	WHERE
		[CategoryId] = @CategoryId
	ORDER BY
		[LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandTitleTranslationGetAllByBrand]'
GO

ALTER PROCEDURE [lekmer].[pBrandTitleTranslationGetAllByBrand]
@BrandId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [BrandTranslation.BrandId] AS 'Id',
		[BrandTranslation.LanguageId] AS 'LanguageId',
		[BrandTranslation.Title] AS 'Value'
	FROM
	    lekmer.vBrandTranslation where [BrandTranslation.BrandId] = @BrandId
	ORDER BY
		[BrandTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [generic].[fStripIllegalSymbols]'
GO

CREATE FUNCTION [generic].[fStripIllegalSymbols] ( @Value VARCHAR(MAX) )
RETURNS VARCHAR(MAX)
AS 
BEGIN
	RETURN
		REPLACE(
			REPLACE(@Value,
				CHAR(0x13), ''),
				CHAR(0x19), '');
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductShortDescriptionTranslationGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductShortDescriptionTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.ShortDescription] AS 'Value'
	FROM
		lekmer.[vProductTranslation]
	WHERE 
		[Product.Id] = @ProductId
	ORDER BY
		[Language.Id]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignLandingPageTranslationWebTitleGetAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignLandingPageTranslationWebTitleGetAll]
	@CampaignLandingPageId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] AS 'Id',
		[clpt].[CampaignLandingPageTranslation.LanguageId] AS 'LanguageId',
		[clpt].[CampaignLandingPageTranslation.WebTitle] AS 'Value'
	FROM
	    [campaignlek].[vCampaignLandingPageTranslation] clpt
	WHERE 
		[clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] = @CampaignLandingPageId
	ORDER BY
		[clpt].[CampaignLandingPageTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSeoSettingDescriptionTranslationGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductSeoSettingDescriptionTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[ProductSeoSetting.Description] AS 'Value'
	FROM
		lekmer.[vProductSeoSettingTranslation]
	WHERE
		[Product.Id] = @ProductId
	ORDER BY
		[Language.Id]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandDescriptionTranslationGetAllByBrand]'
GO

ALTER PROCEDURE [lekmer].[pBrandDescriptionTranslationGetAllByBrand]
@BrandId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [BrandTranslation.BrandId] AS 'Id',
		[BrandTranslation.LanguageId] AS 'LanguageId',
		[BrandTranslation.Description] AS 'Value'
	FROM
	    lekmer.vBrandTranslation where [BrandTranslation.BrandId] = @BrandId
	ORDER BY
		[BrandTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignLandingPageTranslationDescriptionGetAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignLandingPageTranslationDescriptionGetAll]
	@CampaignLandingPageId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] AS 'Id',
		[clpt].[CampaignLandingPageTranslation.LanguageId] AS 'LanguageId',
		[clpt].[CampaignLandingPageTranslation.Description] AS 'Value'
	FROM
	    [campaignlek].[vCampaignLandingPageTranslation] clpt
	WHERE 
		[clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] = @CampaignLandingPageId
	ORDER BY
		[clpt].[CampaignLandingPageTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSeoSettingTitleTranslationGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductSeoSettingTitleTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[ProductSeoSetting.Title] AS 'Value'
	FROM
		lekmer.[vProductSeoSettingTranslation]
	WHERE 
		[Product.Id] = @ProductId
	ORDER BY
		[Language.Id]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCategorySave]'
GO
ALTER PROCEDURE [lekmer].[pCategorySave]
	@CategoryId					INT,
	@Title						NVARCHAR(50),
	@AllowMultipleSizesPurchase	BIT,
	@PackageInfo				NVARCHAR(MAX),
	@MonitorThreshold			INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tCategory]
	SET
		[Title] = @Title
	WHERE
		[CategoryId] = @CategoryId
	
	SET NOCOUNT OFF
		
	UPDATE
		[lekmer].[tLekmerCategory]
	SET
		[AllowMultipleSizesPurchase] = @AllowMultipleSizesPurchase,
		[PackageInfo] = @PackageInfo,
		[MonitorThreshold] = @MonitorThreshold
	WHERE
		[CategoryId] = @CategoryId
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			[lekmer].[tLekmerCategory]
		( 
			[CategoryId],
			[AllowMultipleSizesPurchase],
			[PackageInfo],
			[MonitorThreshold]
		)
		VALUES 
		(
			@CategoryId,
			@AllowMultipleSizesPurchase,
			@PackageInfo,
			@MonitorThreshold
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSeoSettingKeywordsTranslationGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductSeoSettingKeywordsTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[ProductSeoSetting.Keywords] AS 'Value'
	FROM
		lekmer.[vProductSeoSettingTranslation]
	WHERE 
		[Product.Id] = @ProductId
	ORDER BY
		[Language.Id]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pBackofficeProductImportById]'
GO
ALTER PROCEDURE [integration].[pBackofficeProductImportById]
	@HYErpId		NVARCHAR(20),
	@Title			NVARCHAR(256),
	@Description	NVARCHAR(MAX),
	@ChannelNameISO	NVARCHAR(10),
	@TagIds			VARCHAR(MAX),
	@UserName		NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Get language id by Channel
	DECLARE @LanguageId INT
	DECLARE @ChannelId INT
	DECLARE @TitleFixed NVARCHAR(256)
	
	SELECT 
		@LanguageId = l.LanguageId,
		@ChannelId = h.ChannelId
	FROM [core].[tLanguage] l
		 INNER JOIN [core].[tChannel] h ON l.LanguageId = h.LanguageId
		 INNER JOIN [core].[tCountry] c ON h.CountryId = c.CountryId
	WHERE
		c.ISO = @ChannelNameISO
	
	-- Get product id by HYErpId
	DECLARE @ProductId INT
	SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] WHERE HYErpId = @HYErpId)

	-- insert into tProductTransaltion empty rows
	INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
	SELECT
		p.ProductId,
		@LanguageId
	FROM
		[product].[tProduct] p
	WHERE
		NOT EXISTS (SELECT 1 FROM [product].[tProductTranslation] n
					WHERE n.ProductId = p.ProductId 
					AND n.LanguageId = @LanguageId)
					
	IF @Title = ''
		SET @Title = NULL

	SET @TitleFixed = [generic].[fStripIllegalSymbols](@Title)
				
	BEGIN TRY
		BEGIN TRANSACTION				
			
		IF @LanguageId != 1
			BEGIN
				UPDATE [product].[tProductTranslation]
				SET Title = @TitleFixed
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (Title != @TitleFixed OR Title IS NULL)
	
	
				/*BEGIN*/
				-- Remove Restriction from [lekmer].[tProductRegistryRestrictionProduct] if it was added due to empty title.
				-- Get product id by HYErpId
				DECLARE @ProductRegistryId INT
				SET @ProductRegistryId = (SELECT ProductRegistryId FROM [product].[tProductModuleChannel] WHERE ChannelId = @ChannelId)
	
				-- Insert product to tProductRegistryproduct
				IF @TitleFixed IS NOT NULL
				BEGIN
					DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
					WHERE
						ProductRegistryId = @ProductRegistryId
						AND ProductId = @ProductId
						AND RestrictionReason = 'No Translation'
						AND UserId IS NULL
						
					DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]
					INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
					VALUES (@ProductId, @ProductRegistryId)
					
					EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
				END
				/*END*/
				
					
				UPDATE [product].[tProductTranslation]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END
		ELSE
			BEGIN
				IF @TitleFixed IS NOT NULL
				BEGIN
					UPDATE [product].[tProduct]
					SET Title = @TitleFixed
					WHERE
						ProductId = @ProductId
				END
					
				UPDATE [product].[tProduct]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END
			
		-- Update/Insert product tags.
		DECLARE @TagId INT
		DECLARE cur_tag CURSOR FAST_FORWARD FOR
			SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')
			
		OPEN cur_tag
		FETCH NEXT FROM cur_tag INTO @TagId
			
		WHILE @@FETCH_STATUS = 0
			BEGIN
				-- Update/Insert product tags.
				IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
						  	   WHERE [pt].[ProductId] = @ProductId
									 AND [pt].[TagId] = @TagId)
					BEGIN
						INSERT INTO lekmer.tProductTag (ProductId, TagId)
						VALUES (@ProductId, @TagId)
					END
					
				EXEC [lekmer].[pProductTagUsageSave] @ProductId, @TagId, -1						
				
				FETCH NEXT FROM cur_tag INTO @TagId
			END
		CLOSE cur_tag
		DEALLOCATE cur_tag
		
		-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
		INSERT INTO integration.tBackofficeProductInfoImport
		(
			[HYErpId]
			,[Title]
			,[Description]
			,[ChannelNameISO]
			,[TagIdCollection]
			,[UserName]
			,[InsertedDate]
		)
		VALUES 
		(
			@HYErpId
			,@Title
			,@Description
			,@ChannelNameISO
			,@TagIds
			,@UserName
			,CAST(GETDATE() AS SMALLDATETIME)
		)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableTranslationGetAll]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableTranslationGetAll]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
	    [stt].*
	FROM
	    [lekmer].[vSizeTableTranslation] stt
	WHERE 
		stt.[SizeTableTranslation.SizeTableId] = @SizeTableId
	ORDER BY
		stt.[SizeTableTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductMeasurementTranslationGetAllByProduct]'
GO

ALTER PROCEDURE [lekmer].[pProductMeasurementTranslationGetAllByProduct]
@ProductId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.Measurement] AS 'Value'
	FROM
	    lekmer.vLekmerProductTranslation
	WHERE
		[Product.Id] = @ProductId
	ORDER BY
		[Language.Id]

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[pContentNodeGetTree]'
GO
ALTER PROCEDURE [sitestructure].[pContentNodeGetTree]
@SelectedId	int, /* @SelectedId < 0 means registry node, @SelectedId > 0 means content node */
@GetAllRegistries bit
AS
BEGIN
	DECLARE @RegistryId	int
	SET @RegistryId = CASE WHEN (@SelectedId < 0) THEN (-1) * @SelectedId 
		ELSE (SELECT c.[ContentNode.SiteStructureRegistryId] 
		      FROM sitestructure.vCustomContentNodeSecure AS c 
		      WHERE c.[ContentNode.ContentNodeId] = @SelectedId)
		END
	
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max), StatusId int, TypeId int, Ordinal int)
	
	INSERT INTO @tTree
		SELECT
			v.[SiteStructureRegistry.Id] * (-1),
			NULL,
			v.[SiteStructureRegistry.Title],
			(SELECT s.[ContentNodeStatus.Id] FROM sitestructure.vCustomContentNodeStatus AS s WHERE s.[ContentNodeStatus.CommonName] = 'Online'),
			(SELECT t.[ContentNodeType.ContentNodeTypeId] FROM sitestructure.vCustomContentNodeType AS t WHERE t.[ContentNodeType.CommonName] = 'Folder'),
			0
		FROM sitestructure.vCustomSiteStructureRegistry AS v
		WHERE v.[SiteStructureRegistry.Id] = @RegistryId OR @GetAllRegistries = 1
		
	INSERT INTO @tTree
		SELECT
			v.[ContentNode.ContentNodeId],
			v.[ContentNode.SiteStructureRegistryId] * (-1),
			v.[ContentNode.Title],
			v.[ContentNode.ContentNodeStatusId],
			v.[ContentNodeType.ContentNodeTypeId],
			v.[ContentNode.Ordinal]
		FROM sitestructure.vCustomContentNodeSecure AS v
		WHERE v.[ContentNode.SiteStructureRegistryId] = @RegistryId 
			AND v.[ContentNode.ParentContentNodeId] IS NULL
				
	IF (@SelectedId > 0)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[ContentNode.ContentNodeId],
					v.[ContentNode.ParentContentNodeId],
					v.[ContentNode.Title],
					v.[ContentNode.ContentNodeStatusId],
					v.[ContentNodeType.ContentNodeTypeId],
					v.[ContentNode.Ordinal]
				FROM sitestructure.vCustomContentNodeSecure AS v
				WHERE v.[ContentNode.ParentContentNodeId] = @SelectedId

			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT v.[ContentNode.ParentContentNodeId]
									 FROM sitestructure.vCustomContentNodeSecure AS v
									 WHERE v.[ContentNode.ContentNodeId] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT
							v.[ContentNode.ContentNodeId],
							v.[ContentNode.ParentContentNodeId],
							v.[ContentNode.Title],
							v.[ContentNode.ContentNodeStatusId],
							v.[ContentNodeType.ContentNodeTypeId],
							v.[ContentNode.Ordinal]
						FROM sitestructure.vCustomContentNodeSecure AS v
						WHERE v.[ContentNode.ParentContentNodeId] = @ParentId
					SET @SelectedId = @ParentId
				END
		END
		
		DECLARE @PageTypeId int
		SET @PageTypeId = (SELECT ContentNodeTypeId
		                     FROM sitestructure.tContentNodeType WHERE CommonName = 'Page')
		
		SELECT 
			Id,	ParentId, Title, StatusId, TypeId,
			CAST((CASE WHEN (Id < 0) THEN 
				(CASE WHEN EXISTS (SELECT 1 FROM sitestructure.vCustomContentNodeSecure v
									WHERE v.[ContentNode.SiteStructureRegistryId] = -1 * Id)
				THEN 1 ELSE 0 END) ELSE 
				(CASE WHEN EXISTS (SELECT 1 FROM sitestructure.vCustomContentNodeSecure v
								WHERE v.[ContentNode.ParentContentNodeId] = Id)
				THEN 1 ELSE 0 END) END) AS bit) AS 'HasChildren',
			CAST((CASE WHEN (TypeId <> @PageTypeId) THEN 0 ELSE 
				(SELECT cp.IsMaster FROM sitestructure.tContentPage cp 
				 WHERE cp.ContentNodeId = Id) END) AS bit) AS 'IsMasterPage',
			CAST((CASE WHEN (TypeId <> @PageTypeId) THEN 0 ELSE 
				(CASE WHEN EXISTS (SELECT 1 FROM sitestructure.tContentPage cp 
				 WHERE cp.ContentNodeId = Id AND cp.MasterPageId IS NOT NULL) THEN 1 ELSE 0 END) 
					END) AS bit) AS 'HasMasterPage'
		FROM @tTree
		ORDER BY
			Ordinal ASC,
			[Id] DESC -- Always fixed sorting for registries
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[pRatingItemTranslationDescriptionGetAll]'
GO

ALTER PROCEDURE [review].[pRatingItemTranslationDescriptionGetAll]
	@RatingItemId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rit.[RatingItemTranslation.RatingItemId] AS 'Id',
		rit.[RatingItemTranslation.LanguageId] AS 'LanguageId',
		rit.[RatingItemTranslation.Description] AS 'Value'
	FROM
	    [review].[vRatingItemTranslation] rit
	WHERE 
		rit.[RatingItemTranslation.RatingItemId] = @RatingItemId
	ORDER BY
		rit.[RatingItemTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[pRatingTranslationTitleGetAll]'
GO

ALTER PROCEDURE [review].[pRatingTranslationTitleGetAll]
	@RatingId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rt.[RatingTranslation.RatingId] AS 'Id',
		rt.[RatingTranslation.LanguageId] AS 'LanguageId',
		rt.[RatingTranslation.Title] AS 'Value'
	FROM
	    [review].[vRatingTranslation] rt
	WHERE 
		rt.[RatingTranslation.RatingId] = @RatingId
	ORDER BY
		rt.[RatingTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pBackofficeProductImport]'
GO

ALTER PROCEDURE [integration].[pBackofficeProductImport]
	@ChannelNameISO	nvarchar(10) 
AS
BEGIN

	/*NOT IN USE*/
	RAISERROR(N'NOT IN USE, not supported', 16, 1);
	RETURN

	set nocount on
	
	declare @LanguageId int
	set @LanguageId = (
						select l.LanguageId 
						from core.tLanguage l
							inner join core.tChannel h
							on l.LanguageId = h.LanguageId
							inner join core.tCountry c
							on h.CountryId = c.CountryId
						where
							c.ISO = @ChannelNameISO
						)

	begin try
		begin transaction				
			
		if @LanguageId != 1
		begin
		
				update pt
				set 
					pt.Title = i.Title
					--pt.[Description] = i.[Description]
				from 
					integration.tBackofficeProductImport i
					inner join lekmer.tLekmerProduct l
						on l.HYErpId = i.HYErpId
					inner join product.tProductTranslation pt
						on l.ProductId = pt.ProductId	
				where
					pt.LanguageId = @LanguageId
					and (i.[Description] is null or i.[Description] = '')
				
				
				update pt
				set 
					pt.Title = i.Title,
					pt.[Description] = i.[Description]
				from 
					integration.tBackofficeProductImport i
					inner join lekmer.tLekmerProduct l
						on l.HYErpId = i.HYErpId
					inner join product.tProductTranslation pt
						on l.ProductId = pt.ProductId	
				where
					pt.LanguageId = @LanguageId
					and (i.[Description] is not null and i.[Description] != '')
		end
		else
		begin

			update pt
				set 
					pt.Title = i.Title
				from 
					integration.tBackofficeProductImport i
					inner join lekmer.tLekmerProduct l
						on l.HYErpId = i.HYErpId
					inner join product.tProduct pt
						on l.ProductId = pt.ProductId	
				where
					(i.[Description] is null or i.[Description] = '')
					
					
			update pt
				set 
					pt.Title = i.Title,
					pt.[Description] = i.[Description]
				from 
					integration.tBackofficeProductImport i
					inner join lekmer.tLekmerProduct l
						on l.HYErpId = i.HYErpId
					inner join product.tProduct pt
						on l.ProductId = pt.ProductId	
				where
					(i.[Description] is not null and i.[Description] != '')	
		end

		commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductWebShopTitleTranslationGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductWebShopTitleTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.WebShopTitle] AS 'Value'
	FROM
		lekmer.[vProductTranslation]
	WHERE 
		[Product.Id] = @ProductId
	ORDER BY
		[Language.Id]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[pAliasTranslationGetAllById]'
GO
ALTER PROCEDURE [template].[pAliasTranslationGetAllById]
	@AliasId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[template].[vCustomAliasTranslation]
	WHERE
		[Alias.Id] = @AliasId
	ORDER BY
		[Language.Id]
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pIconTranslationDescriptionGetAll]'
GO
ALTER PROCEDURE [lekmer].[pIconTranslationDescriptionGetAll]
	@IconId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    it.[IconTranslation.IconId] AS 'Id',
		it.[IconTranslation.LanguageId] AS 'LanguageId',
		it.[IconTranslation.Description] AS 'Value'
	FROM
	    [lekmer].[vIconTranslation] it
	WHERE 
		it.[IconTranslation.IconId] = @IconId
	ORDER BY
		it.[IconTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableRowTranslationGetAll]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableRowTranslationGetAll]
	@SizeTableRowId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [strt].[SizeTableRowTranslation.SizeTableRowId],
		[strt].[SizeTableRowTranslation.LanguageId],
		[strt].[SizeTableRowTranslation.Column1Value],
		[strt].[SizeTableRowTranslation.Column2Value]
	FROM
	    [lekmer].[vSizeTableRowTranslation] strt
	WHERE 
		[strt].[SizeTableRowTranslation.SizeTableRowId] = @SizeTableRowId
	ORDER BY
		[strt].[SizeTableRowTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pTagTranslationGetAllByTag]'
GO

ALTER PROCEDURE [lekmer].[pTagTranslationGetAllByTag]
@TagId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [TagId] AS 'Id',
		[LanguageId] AS 'LanguageId',
		[Value] AS 'Value'
	FROM
	    lekmer.tTagTranslation where [TagId] = @TagId
	ORDER BY
		[LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pIconTranslationTitleGetAll]'
GO
ALTER PROCEDURE [lekmer].[pIconTranslationTitleGetAll]
	@IconId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [it].[IconTranslation.IconId] AS 'Id',
		[it].[IconTranslation.LanguageId] AS 'LanguageId',
		[it].[IconTranslation.Title] AS 'Value'
	FROM
	    [lekmer].[vIconTranslation] it
	WHERE 
		[it].[IconTranslation.IconId] = @IconId
	ORDER BY
		[it].[IconTranslation.LanguageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockProductList]'
GO
ALTER TABLE [lekmer].[tBlockProductList] ADD CONSTRAINT [FK_tBlockProductList_tBlockProductList1] FOREIGN KEY ([BlockId]) REFERENCES [product].[tBlockProductList] ([BlockId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
