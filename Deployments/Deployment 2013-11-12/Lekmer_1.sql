DROP TABLE [lekmer].[tProductTagTemp]

CREATE TABLE [lekmer].[tProductTagTemp](
	[ProductTagId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[TagId] [int] NOT NULL,
 CONSTRAINT [PK_tProductTagTemp] PRIMARY KEY CLUSTERED 
(
	[ProductTagId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tProductTag_ProductId_TagId] ON [lekmer].[tProductTagTemp] ([ProductId], [TagId]) ON [PRIMARY]

GO
INSERT INTO [lekmer].[tProductTagTemp] ([ProductId], [TagId])
SELECT [ProductId], [TagId] FROM [lekmer].[tProductTag]

GO
EXEC sp_rename 'lekmer.tProductTag', 'tProductTagOld'
EXEC sp_rename N'lekmer.tProductTagOld.PK_tProductTag', N'PK_tProductTagOld', N'INDEX';
EXEC sp_rename 'lekmer.tProductTagTemp', 'tProductTag'
EXEC sp_rename N'lekmer.tProductTag.PK_tProductTagTemp', N'PK_tProductTag', N'INDEX';

-- Foreign Keys
ALTER TABLE [lekmer].[tProductTagOld] DROP CONSTRAINT [FK_tProductTag_tProduct]
GO
ALTER TABLE [lekmer].[tProductTagOld] DROP CONSTRAINT [FK_tProductTag_tTag]
GO


ALTER TABLE [lekmer].[tProductTag] ADD CONSTRAINT [FK_tProductTag_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [lekmer].[tProductTag] ADD CONSTRAINT [FK_tProductTag_tTag] FOREIGN KEY ([TagId]) REFERENCES [lekmer].[tTag] ([TagId])
GO