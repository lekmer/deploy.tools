SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [lekmer].[pProductTagDeleteAll]'
GO
DROP PROCEDURE [lekmer].[pProductTagDeleteAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[tCampaign]'
GO
ALTER TABLE [campaign].[tCampaign] ADD
[TagId] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tProductTagUsage]'
GO
CREATE TABLE [lekmer].[tProductTagUsage]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ProductTagId] [int] NOT NULL,
[ObjectId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductTagUsage] on [lekmer].[tProductTagUsage]'
GO
ALTER TABLE [lekmer].[tProductTagUsage] ADD CONSTRAINT [PK_tProductTagUsage] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductTagUsageSave]'
GO
CREATE PROCEDURE [lekmer].[pProductTagUsageSave]
	@ProductId INT,
	@TagId INT,
	@ObjectId INT
AS
BEGIN
	DECLARE @ProductTagId INT
	SET @ProductTagId = (SELECT [ProductTagId] FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @TagId)
	IF (@ProductTagId IS NOT NULL)
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTagUsage] WHERE [ProductTagId] = @ProductTagId AND [ObjectId] = @ObjectId)
			INSERT INTO [lekmer].[tProductTagUsage] (
				[ProductTagId],
				[ObjectId]
			)
			VALUES (
				@ProductTagId,
				@ObjectId
			)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_OutletTagProduct]'
GO
ALTER PROCEDURE [integration].[usp_OutletTagProduct]
AS
BEGIN
	-- is product
	-- product is passive
	-- product has any discount
	-- stock > 0
	
	--> add tag 'outlet'

	SET NOCOUNT ON
	
	DECLARE @PassiveId INT
	SET @PassiveId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [CommonName] = 'Passive')
	
	DECLARE @OutletTagId INT
	SET @OutletTagId = (SELECT TagId FROM lekmer.tTag WHERE [CommonName] = 'outlet')

	DECLARE @ProductId INT
	DECLARE @NumberInStock INT
	
	DECLARE cur_product CURSOR FAST_FORWARD FOR
	SELECT 
		[lp].[ProductId],
		[p].[NumberInStock]
	FROM
		[lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [lp].[ProductId]
	WHERE
		[lp].[ProductTypeId] = 1 -- Is Product
		AND [lp].[StockStatusId] = @PassiveId -- Is Passive
		AND EXISTS ( -- has any discount
			SELECT 1 
			FROM [export].[tProductPrice] pp 
			WHERE [pp].[ProductId] = [lp].[ProductId]
				  AND [DiscountPriceIncludingVat] IS NOT NULL
				  AND [DiscountPriceExcludingVat] IS NOT NULL
		)

	OPEN cur_product

	FETCH NEXT FROM cur_product 
	INTO @ProductId,
		 @NumberInStock

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- without sizes
		IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId)
			BEGIN
				IF (@NumberInStock > 0)
					IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @OutletTagId)
						BEGIN
							INSERT INTO [lekmer].[tProductTag] ([ProductId], [TagId])
							VALUES (@ProductId, @OutletTagId)
						END
					EXEC [lekmer].[pProductTagUsageSave] @ProductId, @OutletTagId, -1
			END
		ELSE
			BEGIN
				-- with sizes				
				IF EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId AND [NumberInStock] > 0)
					IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @OutletTagId)
						BEGIN 
							INSERT INTO [lekmer].[tProductTag] ([ProductId], [TagId])
							VALUES (@ProductId, @OutletTagId)
						END
					EXEC [lekmer].[pProductTagUsageSave] @ProductId, @OutletTagId, -1
			END
		
		FETCH NEXT FROM cur_product 
		INTO @ProductId,
			 @NumberInStock
	END

	CLOSE cur_product
	DEALLOCATE cur_product
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductTagUsageDelete]'
GO
CREATE PROCEDURE [lekmer].[pProductTagUsageDelete]
	@ProductId	INT = NULL,
	@TagIds		VARCHAR(MAX),
	@ObjectId	INT = NULL,
	@Delimiter	CHAR(1) = ','
AS
BEGIN
	IF (@ProductId IS NULL AND LEN(@TagIds)=0 AND @ObjectId IS NOT NULL)
	BEGIN
		DELETE ptu
		FROM [lekmer].[tProductTagUsage] ptu
		WHERE ptu.[ObjectId] = @ObjectId
		RETURN
	END
	
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'DELETE ptu
				FROM [lekmer].[tProductTagUsage] ptu
				INNER JOIN [lekmer].[tProductTag] pt ON [pt].[ProductTagId] = [ptu].[ProductTagId]
				INNER JOIN [generic].[fnConvertIDListToTable](@TagIds, @Delimiter) AS t ON [t].[ID] = [pt].[TagId]'

	DECLARE @IsProductId BIT = 0
	IF @ProductId IS NOT NULL
	BEGIN
		SET @sql = @sql + ' WHERE [pt].[ProductId] = @ProductId'
		SET @IsProductId = 1
	END
	
	IF @ObjectId IS NOT NULL
	BEGIN
		IF @IsProductId = 1
			SET @sql = @sql + ' AND [ptu].[ObjectId] = @ObjectId'
		ELSE
			SET @sql = @sql + ' WHERE [ptu].[ObjectId] = @ObjectId'
	END
	
	EXEC sp_executesql @sql, 
	N'@ProductId	INT = NULL,
	  @TagIds		VARCHAR(MAX),
	  @ObjectId		INT = NULL,
	  @Delimiter	CHAR(1)',
	  @ProductId,
	  @TagIds,
	  @ObjectId,
	  @Delimiter
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_UpdateProductStatusLekmer]'
GO
ALTER PROCEDURE [integration].[usp_UpdateProductStatusLekmer]
AS
BEGIN

	/*
	When
		product is online
		product is passive
		stock is 0
	Then
		set product offline
		remove tag 'sale'
		remove tag 'outlet'
	*/

	SET NOCOUNT ON
	
	DECLARE @ProductId INT
	DECLARE @NumberInStock INT
	DECLARE @StockStatusId INT
	
	DECLARE @OfflineId INT
	SET @OfflineId = (SELECT [ProductStatusId] FROM [product].[tProductStatus] WHERE [CommonName] = 'Offline')
	
	DECLARE @PassiveId INT
	SET @PassiveId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [CommonName] = 'Passive')
	
	DECLARE @SalesTagId INT
	SET @SalesTagId = (SELECT TagId FROM lekmer.tTag WHERE [CommonName] = 'sale')
	
	DECLARE @OutletTagId INT
	SET @OutletTagId = (SELECT TagId FROM lekmer.tTag WHERE [CommonName] = 'outlet')

	DECLARE cur_product CURSOR FAST_FORWARD FOR
	SELECT 
		[lp].[ProductId],
		[p].[NumberInStock], 
		[lp].[StockStatusId]
	FROM
		[lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [lp].[ProductId]
	WHERE
		[lp].[ProductTypeId] = 1
		AND
		p.[ProductStatusId] = 0 -- Online

	OPEN cur_product

	FETCH NEXT FROM cur_product 
	INTO @ProductId,
		 @NumberInStock,
		 @StockStatusId

	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId)
			BEGIN
				-- no sizes
				-- set offline
				-- remove sale tag
				
				IF (@NumberInStock <= 0 AND @StockStatusId = @PassiveId)
				BEGIN 
					UPDATE [product].[tProduct]
					SET [ProductStatusId] = @OfflineId
					WHERE [ProductId] = @ProductId
					
					EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @OutletTagId, NULL
					EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @SalesTagId, NULL
					
					DELETE FROM lekmer.tProductTag
					WHERE [ProductId] = @ProductId
						  AND TagId IN (@SalesTagId, @OutletTagId)
				END
			END
		ELSE
			BEGIN
				-- product with sizes
				
				IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId AND ([StockStatusId] != @PassiveId OR [NumberInStock] > 0))
				BEGIN 
					-- all sizes are passive
					-- set offline
					-- remove sale tag
				
					UPDATE [product].[tProduct]
					SET [ProductStatusId] = @OfflineId
					WHERE [ProductId] = @ProductId
					
					EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @OutletTagId, NULL
					EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @SalesTagId, NULL
					
					DELETE FROM lekmer.tProductTag
					WHERE [ProductId] = @ProductId
						  AND TagId IN (@SalesTagId, @OutletTagId)
				END
				
				IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId AND [StockStatusId] != @PassiveId)
				BEGIN
					-- set passive
					UPDATE [lekmer].[tLekmerProduct]
					SET [StockStatusId] = @PassiveId
					WHERE [ProductId] = @ProductId
				END
				
				-- update number in stock on product level
				UPDATE [product].[tProduct]
				SET [NumberInStock] = (
										SELECT SUM([ps].[NumberInStock])
										FROM [lekmer].[tProductSize] ps
										WHERE [ps].[ProductId] = @ProductId AND [ps].[NumberInStock] >= 0
									  )
				WHERE [ProductId] = @ProductId
			END
		
		FETCH NEXT FROM cur_product 
		INTO @ProductId,
			 @NumberInStock,
			 @StockStatusId
	END

	CLOSE cur_product
	DEALLOCATE cur_product
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[vCampaign]'
GO

ALTER VIEW [campaign].[vCampaign]
AS
	SELECT
		[c].[CampaignId]			AS 'Campaign.Id',
		[c].[FolderId]				AS 'Campaign.FolderId',
		[c].[Title]					AS 'Campaign.Title',
		[c].[CampaignStatusId]		AS 'Campaign.CampaignStatusId',
		[c].[StartDate]				AS 'Campaign.StartDate',
		[c].[EndDate]				AS 'Campaign.EndDate',
		[c].[Priority]				AS 'Campaign.Priority',
		[c].[Exclusive]				AS 'Campaign.Exclusive',
		[c].[LevelId]				AS 'Campaign.LevelId',
		[cl].[Priority]				AS 'Campaign.LevelPriority',
		[c].[UseLandingPage]		AS 'Campaign.UseLandingPage',
		[c].[PriceTypeId]			AS 'Campaign.PriceTypeId',
		[c].[TagId]					AS 'Campaign.TagId',
		s.*
	FROM
		[campaign].[tCampaign] c
		INNER JOIN [campaign].[vCustomCampaignStatus] s ON [c].[CampaignStatusId] = [s].[CampaignStatus.Id]
		INNER JOIN [campaignlek].[tCampaignLevel] cl ON [cl].[LevelId] = [c].[LevelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pTagDelete]'
GO
ALTER PROCEDURE [lekmer].[pTagDelete]
	@TagId	INT
AS
BEGIN 
	EXEC [lekmer].[pProductTagUsageDelete] NULL, @TagId, NULL
	
	DELETE FROM lekmer.tProductTag
	WHERE TagId = @TagId
	
	DELETE FROM lekmer.tTagTranslation
	WHERE TagId = @TagId
	
	DELETE FROM lekmer.tTag
	WHERE TagId = @TagId
END 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignSave]'
GO
ALTER PROCEDURE [campaign].[pCampaignSave]
	@CampaignId			INT,
	@Title				VARCHAR(500),
	@CampaignStatusId	INT,
	@FolderId			INT,
	@StartDate			DATETIME,
	@EndDate			DATETIME,
	@Exclusive			BIT,
	@Priority			INT,
	@LevelId			INT,
	@UseLandingPage		BIT = 0,
	@PriceTypeId		INT = NULL,
	@TagId				INT = NULL
AS
BEGIN
	UPDATE
		[campaign].[tCampaign]
	SET
		[Title]					= @Title,
		[CampaignStatusId]		= @CampaignStatusId,
		[FolderId]				= @FolderId,
		[StartDate]				= @StartDate,
		[EndDate]				= @EndDate,
		[Exclusive]				= @Exclusive,
		[Priority]				= @Priority,
		[LevelId]				= @LevelId,
		[UseLandingPage]		= @UseLandingPage,
		[PriceTypeId]			= @PriceTypeId,
		[TagId]					= @TagId
	WHERE
		[CampaignId] = @CampaignId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		SET @Priority = (SELECT MAX(PRIORITY) FROM campaign.tCampaign) + 1
		IF @Priority IS NULL
			BEGIN
				SET @Priority = 1
			END
			
		INSERT campaign.tCampaign (
			[Title],
			[CampaignStatusId],
			[FolderId],
			[StartDate],
			[EndDate],
			[Exclusive],
			[Priority],
			[LevelId],
			[UseLandingPage],
			[PriceTypeId],
			[TagId]
		)
		VALUES (
			@Title,
			@CampaignStatusId,
			@FolderId,
			@StartDate,
			@EndDate,
			@Exclusive,
			@Priority,
			@LevelId,
			@UseLandingPage,
			@PriceTypeId,
			@TagId
		)
		
		SET @CampaignId = CAST(SCOPE_IDENTITY() AS INT)
	END
	RETURN @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductTagsDeleteByCampaign]'
GO
CREATE PROCEDURE [lekmer].[pProductTagsDeleteByCampaign]
	@CampaignId	INT
AS
BEGIN
	DECLARE @TagsToDelete TABLE (ProductTagId INT)
	INSERT INTO @TagsToDelete
	SELECT [pt].[ProductTagId]
	FROM [lekmer].[tProductTag] pt
		  INNER JOIN [lekmer].[tProductTagUsage] ptu ON [ptu].[ProductTagId] = [pt].[ProductTagId]
		  WHERE [ptu].[ObjectId] = @CampaignId
			    AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTagUsage] ptu1
								WHERE [ptu1].[ProductTagId] = [pt].[ProductTagId]
								HAVING COUNT(1) > 1)

	EXEC [lekmer].[pProductTagUsageDelete] NULL, '', @CampaignId
	
	DELETE pt
	FROM [lekmer].[tProductTag] pt
	INNER JOIN @TagsToDelete d ON [d].[ProductTagId] = [pt].[ProductTagId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionGetProductIds]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionGetProductIds]
	@ConfigId INT
AS
BEGIN
	DECLARE @IsIncludeAll BIT
	SET @IsIncludeAll = (SELECT [IncludeAllProducts] FROM [campaignlek].[tCampaignActionConfigurator] WHERE [CampaignActionConfiguratorId] = @ConfigId)

	DECLARE @IncludedProducts TABLE (ProductId INT)
	DECLARE @ExcludedProducts TABLE (ProductId INT)

	INSERT INTO @ExcludedProducts ([ProductId])
	(
		SELECT DISTINCT [p].[ProductId]
		FROM [lekmer].[tLekmerProduct] p
		WHERE [p].[BrandId] IN (SELECT [eb].[BrandId]
								FROM [campaignlek].[tCampaignActionExcludeBrand] eb
								WHERE [eb].[ConfigId] = @ConfigId)

		UNION

		SELECT DISTINCT [ep].[ProductId]
		FROM [campaignlek].[tCampaignActionExcludeProduct] ep
		WHERE [ep].[ConfigId] = @ConfigId

		UNION

		SELECT DISTINCT [p].[ProductId]
		FROM [product].[tProduct] p 
		WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tCampaignActionExcludeCategory] ec
								   CROSS APPLY [product].[fnGetSubCategories] (ec.CategoryId) src
								   WHERE [ec].[ConfigId] = @ConfigId)
	)

	IF (@IsIncludeAll = 1)
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		SELECT [ProductId] FROM [product].[tProduct]
	END
	ELSE
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		(
			SELECT DISTINCT [p].[ProductId]
			FROM [lekmer].[tLekmerProduct] p
			WHERE [p].[BrandId] IN (SELECT [ib].[BrandId]
									FROM [campaignlek].[tCampaignActionIncludeBrand] ib
									WHERE [ib].[ConfigId] = @ConfigId)

			UNION

			SELECT DISTINCT [ip].[ProductId]
			FROM [campaignlek].[tCampaignActionIncludeProduct] ip
			WHERE [ip].[ConfigId] = @ConfigId

			UNION

			SELECT DISTINCT [p].[ProductId]
			FROM [product].[tProduct] p 
			WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tCampaignActionIncludeCategory] ic
									   CROSS APPLY [product].[fnGetSubCategories] (ic.CategoryId) src
									   WHERE [ic].[ConfigId] = @ConfigId)
		)
	END

	SELECT [ProductId] FROM @IncludedProducts
	EXCEPT
	SELECT [ProductId] FROM @ExcludedProducts
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailProductActionGetProductIds]'
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionGetProductIds]
	@ActionId INT
AS
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT [ConfigId] FROM [campaignlek].[tGiftCardViaMailProductAction] WHERE [ProductActionId] = @ActionId)

	DECLARE @ProductIds TABLE (ProductId INT)
	INSERT INTO @ProductIds
	EXEC [campaignlek].[pCampaignActionGetProductIds] @ConfigId
	
	SELECT [ProductId] FROM @ProductIds
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductTagInsert]'
GO
CREATE PROCEDURE [lekmer].[pProductTagInsert]
	@ProductId	INT,
	@TagId		INT
AS
BEGIN 
	INSERT INTO lekmer.tProductTag
	(
		ProductId,
		TagId
	)
	VALUES
	(
		@ProductId,
		@TagId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductTagSave]'
GO
ALTER PROCEDURE [lekmer].[pProductTagSave]
	@ProductId	INT,
	@TagId		INT,
	@ObjectId	INT
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @TagId)
		EXEC [lekmer].[pProductTagInsert] @ProductId, @TagId

	EXEC [lekmer].[pProductTagUsageSave] @ProductId, @TagId, @ObjectId	
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignToTag]'
GO
CREATE TABLE [campaignlek].[tCampaignToTag]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CampaignId] [int] NOT NULL,
[ProcessingDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignToTag] on [campaignlek].[tCampaignToTag]'
GO
ALTER TABLE [campaignlek].[tCampaignToTag] ADD CONSTRAINT [PK_tCampaignToTag] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignToTagInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagInsert]
	@CampaignId INT,
	@ProcessingDate DATETIME = NULL
AS
BEGIN
	INSERT [campaignlek].[tCampaignToTag] (
		[CampaignId],
		[ProcessingDate]
	)
	VALUES (
		@CampaignId,
		@ProcessingDate
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductTagsDeleteByProduct]'
GO
CREATE PROCEDURE [lekmer].[pProductTagsDeleteByProduct]
	@ProductId	INT,
	@TagIds		VARCHAR(MAX),
	@ObjectId	INT,
	@Delimiter	CHAR(1) = ','
AS
BEGIN
	EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @TagIds, @ObjectId, @Delimiter

	DELETE pt
	FROM [lekmer].[tProductTag] pt
	INNER JOIN [generic].[fnConvertIDListToTable](@TagIds, @Delimiter) AS t ON [t].[ID] = [pt].[TagId]
	WHERE [pt].[ProductId] = @ProductId
		  AND NOT EXISTS (  SELECT 1 
							FROM [lekmer].[tProductTagUsage] ptu1
							INNER JOIN [lekmer].[tProductTag] pt1 ON [pt1].[ProductTagId] = [ptu1].[ProductTagId]
							WHERE [pt1].[ProductId] = @ProductId AND [pt1].[TagId] = [t].[ID]
							HAVING COUNT(1) > 0)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignToTagDeleteByCampaignId]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagDeleteByCampaignId]
	@CampaignId INT
AS
BEGIN
	DELETE FROM [campaignlek].[tCampaignToTag]
	WHERE [CampaignId] = @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignToTagSave]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagSave]
	@CampaignId INT
AS
BEGIN
	EXEC [campaignlek].[pCampaignToTagDeleteByCampaignId] @CampaignId

	-- campaign deleted
	IF NOT EXISTS (SELECT 1 FROM [campaign].[tCampaign] WHERE [CampaignId] = @CampaignId)
	BEGIN
		EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, NULL
		RETURN
	END
	--------------------------------------------------------------------------------------------------------------------------------------------------
	DECLARE
		@CurrentDate DATETIME = GETDATE(),
		@StartDate DATETIME,
		@EndDate DATETIME,
		@StatusId INT	
	SELECT @StartDate = [StartDate], @EndDate = [EndDate], @StatusId = [CampaignStatusId] FROM [campaign].[tCampaign] WHERE [CampaignId] = @CampaignId
	
	-- campaign offline or online but end by date
	IF (@StatusId = 1 OR (@EndDate IS NOT NULL AND @EndDate < @CurrentDate))
	BEGIN
		EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, NULL
		RETURN
	END
	--------------------------------------------------------------------------------------------------------------------------------------------------
	IF (@StartDate IS NULL OR @StartDate < @CurrentDate)
		EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, NULL
	ELSE
		EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, @StartDate
	
	IF (@EndDate IS NOT NULL)
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM [campaignlek].[tCampaignToTag] WHERE [CampaignId] = @CampaignId AND [ProcessingDate] = @EndDate)
		BEGIN
			EXEC [campaignlek].[pCampaignToTagInsert] @CampaignId, @EndDate
		END
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductDiscountActionGetProductIds]'
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionGetProductIds]
	@ActionId INT
AS
BEGIN
	SELECT ProductId FROM [lekmer].[tProductDiscountActionItem]
	WHERE [ProductActionId] = @ActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaign].[pPercentagePriceDiscountActionGetProductIds]'
GO
CREATE PROCEDURE [campaign].[pPercentagePriceDiscountActionGetProductIds]
	@ActionId INT
AS
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT [ConfigId] FROM [campaign].[tPercentagePriceDiscountAction] WHERE [ProductActionId] = @ActionId)
	
	DECLARE @ProductIds TABLE (ProductId INT)
	INSERT INTO @ProductIds
	EXEC [campaignlek].[pCampaignActionGetProductIds] @ConfigId
	
	SELECT [ProductId] FROM @ProductIds
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignToTagGet]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagGet]
AS
BEGIN
	SELECT TOP(1)
		[Id],
		[CampaignId],
		[ProcessingDate]
	FROM
		[campaignlek].[tCampaignToTag]
	WHERE
		[ProcessingDate] IS NULL
		OR [ProcessingDate] < GETDATE()
	ORDER BY [Id]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionGetProductIds]'
GO
CREATE PROCEDURE [campaignlek].[pFixedPriceActionGetProductIds]
	@ActionId INT
AS
BEGIN
	DECLARE @IsIncludeAll BIT
	SET @IsIncludeAll = (SELECT [IncludeAllProducts] FROM [campaignlek].[tFixedPriceAction] WHERE [ProductActionId] = @ActionId)

	DECLARE @IncludedProducts TABLE (ProductId INT)
	DECLARE @ExcludedProducts TABLE (ProductId INT)

	INSERT INTO @ExcludedProducts ([ProductId])
	(
		SELECT DISTINCT [p].[ProductId]
		FROM [lekmer].[tLekmerProduct] p
		WHERE [p].[BrandId] IN (SELECT [eb].[BrandId]
								FROM [campaignlek].[tFixedPriceActionExcludeBrand] eb
								WHERE [eb].[ProductActionId] = @ActionId)

		UNION

		SELECT DISTINCT [ep].[ProductId]
		FROM [campaignlek].[tFixedPriceActionExcludeProduct] ep
		WHERE [ep].[ProductActionId] = @ActionId

		UNION

		SELECT DISTINCT [p].[ProductId]
		FROM [product].[tProduct] p 
		WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tFixedPriceActionExcludeCategory] ec
								   CROSS APPLY [product].[fnGetSubCategories] (ec.CategoryId) src
								   WHERE [ec].[ProductActionId] = @ActionId)
	)

	IF (@IsIncludeAll = 1)
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		SELECT [ProductId] FROM [product].[tProduct]
	END
	ELSE
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		(
			SELECT DISTINCT [p].[ProductId]
			FROM [lekmer].[tLekmerProduct] p
			WHERE [p].[BrandId] IN (SELECT [ib].[BrandId]
									FROM [campaignlek].[tFixedPriceActionIncludeBrand] ib
									WHERE [ib].[ProductActionId] = @ActionId)

			UNION

			SELECT DISTINCT [ip].[ProductId]
			FROM [campaignlek].[tFixedPriceActionIncludeProduct] ip
			WHERE [ip].[ProductActionId]= @ActionId

			UNION

			SELECT DISTINCT [p].[ProductId]
			FROM [product].[tProduct] p 
			WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tFixedPriceActionIncludeCategory] ic
									   CROSS APPLY [product].[fnGetSubCategories] (ic.CategoryId) src
									   WHERE [ic].[ProductActionId] = @ActionId)
		)
	END

	SELECT [ProductId] FROM @IncludedProducts
	EXCEPT
	SELECT [ProductId] FROM @ExcludedProducts
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pBackofficeProductImportById]'
GO
ALTER PROCEDURE [integration].[pBackofficeProductImportById]
	@HYErpId		NVARCHAR(20),
	@Title			NVARCHAR(256),
	@Description	NVARCHAR(MAX),
	@ChannelNameISO	NVARCHAR(10),
	@TagIds			VARCHAR(MAX),
	@UserName		NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Get language id by Channel
	DECLARE @LanguageId INT
	DECLARE @ChannelId INT
	
	SELECT 
		@LanguageId = l.LanguageId,
		@ChannelId = h.ChannelId
	FROM [core].[tLanguage] l
		 INNER JOIN [core].[tChannel] h ON l.LanguageId = h.LanguageId
		 INNER JOIN [core].[tCountry] c ON h.CountryId = c.CountryId
	WHERE
		c.ISO = @ChannelNameISO
	
	-- Get product id by HYErpId
	DECLARE @ProductId INT
	SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] WHERE HYErpId = @HYErpId)

	-- insert into tProductTransaltion empty rows
	INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
	SELECT
		p.ProductId,
		@LanguageId
	FROM
		[product].[tProduct] p
	WHERE
		NOT EXISTS (SELECT 1 FROM [product].[tProductTranslation] n
					WHERE n.ProductId = p.ProductId 
					AND n.LanguageId = @LanguageId)
					
	IF @Title = ''
		SET @Title = NULL
				
	BEGIN TRY
		BEGIN TRANSACTION				
			
		IF @LanguageId != 1
			BEGIN
				UPDATE [product].[tProductTranslation]
				SET Title = @Title
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (Title != @Title OR Title IS NULL)
	
	
				/*BEGIN*/
				-- Remove Restriction from [lekmer].[tProductRegistryRestrictionProduct] if it was added due to empty title.
				-- Get product id by HYErpId
				DECLARE @ProductRegistryId INT
				SET @ProductRegistryId = (SELECT ProductRegistryId FROM [product].[tProductModuleChannel] WHERE ChannelId = @ChannelId)
	
				-- Insert product to tProductRegistryproduct
				IF @Title IS NOT NULL
				BEGIN
					DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
					WHERE
						ProductRegistryId = @ProductRegistryId
						AND ProductId = @ProductId
						AND RestrictionReason = 'No Translation'
						AND UserId IS NULL
						
					DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]
					INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
					VALUES (@ProductId, @ProductRegistryId)
					
					EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
				END
				/*END*/
				
					
				UPDATE [product].[tProductTranslation]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END
		ELSE
			BEGIN
				IF @Title IS NOT NULL
				BEGIN
					UPDATE [product].[tProduct]
					SET Title = @Title
					WHERE
						ProductId = @ProductId
				END
					
				UPDATE [product].[tProduct]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END
			
		-- Update/Insert product tags.
		DECLARE @TagId INT
		DECLARE cur_tag CURSOR FAST_FORWARD FOR
			SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')
			
		OPEN cur_tag
		FETCH NEXT FROM cur_tag INTO @TagId
			
		WHILE @@FETCH_STATUS = 0
			BEGIN
				-- Update/Insert product tags.
				IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
						  	   WHERE [pt].[ProductId] = @ProductId
									 AND [pt].[TagId] = @TagId)
					BEGIN
						INSERT INTO lekmer.tProductTag (ProductId, TagId)
						VALUES (@ProductId, @TagId)
					END
					
				EXEC [lekmer].[pProductTagUsageSave] @ProductId, @TagId, -1						
				
				FETCH NEXT FROM cur_tag INTO @TagId
			END
		CLOSE cur_tag
		DEALLOCATE cur_tag
		
		-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
		INSERT INTO integration.tBackofficeProductInfoImport
		(
			[HYErpId]
			,[Title]
			,[Description]
			,[ChannelNameISO]
			,[TagIdCollection]
			,[UserName]
			,[InsertedDate]
		)
		VALUES 
		(
			@HYErpId
			,@Title
			,@Description
			,@ChannelNameISO
			,@TagIds
			,@UserName
			,CAST(GETDATE() AS SMALLDATETIME)
		)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionGetProductIds]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionGetProductIds]
	@ActionId INT
AS
BEGIN
	DECLARE @IsIncludeAll BIT
	SET @IsIncludeAll = (SELECT [IncludeAllProducts] FROM [campaignlek].[tFixedDiscountAction] WHERE [ProductActionId] = @ActionId)

	DECLARE @IncludedProducts TABLE (ProductId INT)
	DECLARE @ExcludedProducts TABLE (ProductId INT)
	
	INSERT INTO @ExcludedProducts ([ProductId])
	(
		SELECT DISTINCT [p].[ProductId]
		FROM [lekmer].[tLekmerProduct] p
		WHERE [p].[BrandId] IN (SELECT [eb].[BrandId]
								FROM [campaignlek].[tFixedDiscountActionExcludeBrand] eb
								WHERE [eb].[ProductActionId] = @ActionId)

		UNION

		SELECT DISTINCT [ep].[ProductId]
		FROM [campaignlek].[tFixedDiscountActionExcludeProduct] ep
		WHERE [ep].[ProductActionId] = @ActionId

		UNION

		SELECT DISTINCT [p].[ProductId]
		FROM [product].[tProduct] p 
		WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tFixedDiscountActionExcludeCategory] ec
								   CROSS APPLY [product].[fnGetSubCategories] (ec.CategoryId) src
								   WHERE [ec].[ProductActionId] = @ActionId)
	)
	
	IF (@IsIncludeAll = 1)
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		SELECT [ProductId] FROM [product].[tProduct]
	END
	ELSE
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		(
			SELECT DISTINCT [p].[ProductId]
			FROM [lekmer].[tLekmerProduct] p
			WHERE [p].[BrandId] IN (SELECT [ib].[BrandId]
									FROM [campaignlek].[tFixedDiscountActionIncludeBrand] ib
									WHERE [ib].[ProductActionId] = @ActionId)

			UNION

			SELECT DISTINCT [ip].[ProductId]
			FROM [campaignlek].[tFixedDiscountActionIncludeProduct] ip
			WHERE [ip].[ProductActionId]= @ActionId

			UNION

			SELECT DISTINCT [p].[ProductId]
			FROM [product].[tProduct] p 
			WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tFixedDiscountActionIncludeCategory] ic
									   CROSS APPLY [product].[fnGetSubCategories] (ic.CategoryId) src
									   WHERE [ic].[ProductActionId] = @ActionId)
		)
	END
	
	SELECT [ProductId] FROM @IncludedProducts
	EXCEPT
	SELECT [ProductId] FROM @ExcludedProducts
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignToTagDeleteById]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagDeleteById]
	@Id INT
AS
BEGIN
	DELETE FROM [campaignlek].[tCampaignToTag]
	WHERE [Id] = @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
