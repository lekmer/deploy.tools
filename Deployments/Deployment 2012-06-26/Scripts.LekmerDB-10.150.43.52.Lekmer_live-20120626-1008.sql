/*
Run this script on:

        10.150.43.52.Lekmer_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 018\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 26.06.2012 10:08:29

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[pTagGetAll]'
GO

ALTER PROCEDURE [lekmer].[pTagGetAll]
	@LanguageId INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		t.*
	FROM 
		lekmer.[vTag] t
	WHERE
		t.[Tag.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pTagGetAllByProduct]'
GO
CREATE PROCEDURE [lekmer].[pTagGetAllByProduct]
	@LanguageId INT,
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[t].*
	FROM
		[lekmer].[vTag] t
		INNER JOIN [lekmer].[tProductTag] AS pt ON t.[Tag.TagId] = pt.[TagId]
	WHERE 
		pt.[ProductId] = @ProductId
		AND
		t.[Tag.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pTagGetAllByTagGroup]'
GO
ALTER PROCEDURE [lekmer].[pTagGetAllByTagGroup]
	@LanguageId INT,
	@TagGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[t].*
	FROM
		lekmer.[vTag] t
	WHERE
		[Tag.TagGroupId] = @TagGroupId
		AND [Tag.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pTagGetAllSecure]'
GO

CREATE PROCEDURE [lekmer].[pTagGetAllSecure]
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		*
	FROM 
		lekmer.[vTagSecure]
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductImageGetAllByProduct]'
GO
CREATE PROCEDURE [lekmer].[pProductImageGetAllByProduct]
	@ProductId INT
AS
BEGIN
	SELECT
		[i].*
	FROM 
		[product].[vCustomProductImage] i
	WHERE
		i.[ProductImage.ProductId] = @ProductId
	ORDER BY 
		[i].[ProductImage.GroupId],
		[i].[ProductImage.Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
