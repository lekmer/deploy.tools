SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
USE [Heppo_live]
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

--/////////////////////////////////////
BEGIN TRANSACTION


PRINT N'Dropping extended properties'
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'product', 'VIEW', N'vCustomProduct', NULL, NULL
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane2', 'SCHEMA', N'product', 'VIEW', N'vCustomProduct', NULL, NULL
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'product', 'VIEW', N'vCustomProduct', NULL, NULL
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'product', 'VIEW', N'vCustomProductRecord', NULL, NULL
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'product', 'VIEW', N'vCustomProductRecord', NULL, NULL
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'product', 'VIEW', N'vCustomProductView', NULL, NULL
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane2', 'SCHEMA', N'product', 'VIEW', N'vCustomProductView', NULL, NULL
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'product', 'VIEW', N'vCustomProductView', NULL, NULL
GO
PRINT N'Dropping constraints from [import].[tProduct]'
GO
ALTER TABLE [import].[tProduct] DROP CONSTRAINT [PK__tProduct__3DCBFF4F0B132856]
GO
PRINT N'Dropping constraints from [integration].[tTradeDoublerProductGroupMapping]'
GO
ALTER TABLE [integration].[tTradeDoublerProductGroupMapping] DROP CONSTRAINT [PK__tTradeDo__AE1067042DA04616]
GO
PRINT N'Dropping constraints from [lekmer].[tTradeDoublerProductGroupMapping]'
GO
ALTER TABLE [lekmer].[tTradeDoublerProductGroupMapping] DROP CONSTRAINT [PK__tTradeDo__E780F84C4349CAB9]
GO
PRINT N'Dropping index [IX_tLekmerProduct] from [lekmer].[tLekmerProduct]'
GO
DROP INDEX [IX_tLekmerProduct] ON [lekmer].[tLekmerProduct]
GO
PRINT N'Dropping index [IX] from [product].[tPriceListItem]'
GO
DROP INDEX [IX] ON [product].[tPriceListItem]
GO
PRINT N'Dropping index [IX_tProductRegistryProduct] from [product].[tProductRegistryProduct]'
GO
DROP INDEX [IX_tProductRegistryProduct] ON [product].[tProductRegistryProduct]
GO
PRINT N'Altering [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD
[ShowVariantRelations] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_ShowVariantRelations] DEFAULT ((0))
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP
COLUMN [OldUrl]
GO
PRINT N'Creating index [IX_tLekmerProduct_HYErpId] on [lekmer].[tLekmerProduct]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tLekmerProduct_HYErpId] ON [lekmer].[tLekmerProduct] ([HYErpId])
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO


ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	CAST((CASE WHEN EXISTS (SELECT 1 FROM lekmer.tProductSize s WHERE s.ProductId = p.ProductId) THEN 1 ELSE 0 END) AS BIT) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations'
FROM
	lekmer.tLekmerProduct AS p
GO
PRINT N'Altering [import].[pImportCategory]'
GO

ALTER PROCEDURE [import].[pImportCategory]
	
AS
BEGIN
		DECLARE 
		@Seperator NVARCHAR(1),	
		@ErpCategoryPrefix NVARCHAR(2),
		@ErpClassId NVARCHAR(50),
		@ErpClassGroupId NVARCHAR(50),
		@ErpClassGroupCodeId NVARCHAR(50),
		@ClassGroupCodeId NVARCHAR(50),
		@HYErpId NVARCHAR(50),
		@ArticleClassId NVARCHAR(10),
		@ArticleClassTitle NVARCHAR(40),
		@ArticleGroupId NVARCHAR(10),
		@ArticleGroupTitle NVARCHAR(40),
		@ArticleCodeId NVARCHAR(10),
		@ArticleCodeTitle NVARCHAR(40)
	
	SET @Seperator = '-'
	SET @ErpCategoryPrefix = 'C_'
		
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT DISTINCT
			tp.HYErpId,
			tp.ArticleClassId ,
			tp.ArticleClassTitle,
			tp.ArticleGroupId,
			tp.ArticleGroupTitle,
			tp.ArticleCodeId,
			tp.ArticleCodeTitle
		FROM
			[import].tProduct tp
			LEFT JOIN lekmer.tLekmerProduct lp ON lp.HYErpId = tp.HYErpId 
			LEFT JOIN product.tProduct p ON p.ProductId = lp.ProductId
			LEFT JOIN product.tCategory c ON c.CategoryId = p.CategoryId
		WHERE
			tp.ChannelId = 1
			AND
			(
				@ErpCategoryPrefix 
					+ ISNULL(tp.ArticleClassId, '') + @Seperator 
					+ ISNULL(tp.ArticleGroupId, '') + @Seperator 
					+ ISNULL(tp.ArticleCodeId, '') <> c.ErpId
				OR
					c.ErpId IS NULL
			)
		
	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO 
			@HYErpId,
			@ArticleClassId,
			@ArticleClassTitle,
			@ArticleGroupId,
			@ArticleGroupTitle,
			@ArticleCodeId,
			@ArticleCodeTitle
		
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @ClassGroupCodeId = @ArticleClassId + @Seperator + @ArticleGroupId + @Seperator + @ArticleCodeId
			
			BEGIN TRY
				BEGIN TRANSACTION
					SET @ErpClassId = @ErpCategoryPrefix + @ArticleClassId
					SET @ErpClassGroupId = @ErpClassId + @Seperator + @ArticleGroupId
					SET @ErpClassGroupCodeId = @ErpClassGroupId + @Seperator + @ArticleCodeId
				
					-- varuklass
					IF not exists (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassId)
						INSERT INTO 
							product.tCategory (ParentCategoryId, Title, ErpId)
						SELECT 	
							null,
							@ArticleClassTitle,
							@ErpClassId

					-- Varugrupp
					IF NOT EXISTS (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassGroupId)
						INSERT INTO 
							[product].tCategory (ParentCategoryId, Title, ErpId)
						SELECT 	
							(SELECT CategoryId FROM product.tCategory WHERE ErpId = @ErpClassId),
							@ArticleGroupTitle,		
							@ErpClassGroupId
					
					-- Varukod
					IF (NOT EXISTS (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassGroupCodeId)
						AND @ArticleCodeTitle <> '***Missing***' )
							INSERT INTO 
								[product].tCategory (ParentCategoryId, Title, ErpId)
							SELECT 	
								(SELECT CategoryId FROM product.tCategory WHERE ErpId = @ErpClassGroupId),
								@ArticleCodeTitle,		
								@ErpClassGroupCodeId

					-- ändra referensen på categoryId om den bytat
					UPDATE 
						tp
					SET 
						tp.CategoryId = (SELECT categoryId FROM product.tCategory 
										 WHERE ErpId = @ErpClassGroupCodeId)
					FROM 
						[lekmer].tLekmerProduct lp						
						INNER JOIN product.tProduct tp ON tp.ProductId = lp.ProductId				
					WHERE 
						lp.HYErpId = @HYErpId 
						AND tp.CategoryId <> (SELECT categoryId FROM product.tCategory 
											  WHERE ErpId = @ErpClassGroupCodeId)
					
				COMMIT
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0 ROLLBACK
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				VALUES(@ClassGroupCodeId, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

				CLOSE cur_product
				DEALLOCATE cur_product 
			END CATCH
		
			FETCH NEXT FROM cur_product 
				INTO 
					@HYErpId,
					@ArticleClassId,
					@ArticleClassTitle,
					@ArticleGroupId,
					@ArticleGroupTitle,
					@ArticleCodeId,
					@ArticleCodeTitle
		END
	CLOSE cur_product
	DEALLOCATE cur_product	   	              
                
END
GO
PRINT N'Altering [import].[pImportProductSize]'
GO

ALTER PROCEDURE [import].[pImportProductSize]
	@HYErpId NVARCHAR(50),
	@HYErpIdSize NVARCHAR(50),
	@ChannelId INT,
	@HYSizeId NVARCHAR(50),	
	@NumberInStock INT,
	@VarugruppId NVARCHAR(10),
	@VaruklassId NVARCHAR(10),
	@VarukodId NVARCHAR(10),
	
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE
		@Data NVARCHAR(4000),
		--Variables for Channel (Split)
		@Pos INT,
		@SizeChar NVARCHAR (20),
		@SizeNo NVARCHAR (20)
		
	------------------------------------------------------------------------
	DECLARE @AccessorySizeId INT
	DECLARE @SizeId INT
	DECLARE @CategoryId INT
	DECLARE @NeedToInsertProductSize BIT

	IF @ChannelId = 1
	IF @HYSizeId != 'Onesize'
	IF NOT EXISTS(SELECT 1
				  FROM lekmer.tProductSize
				  WHERE ErpId = @HYErpIdSize)
	BEGIN TRY
		BEGIN TRANSACTION
		
		SET @NeedToInsertProductSize = 0
		SET @Data = 'BEGIN: @HYSizeId ' + @HYSizeId
				
		-- Split SizeId by semicolon
		SET @Pos = CHARINDEX('-', @HYSizeId)
		IF @Pos > 0
		BEGIN
			SET @SizeChar = SUBSTRING(@HYSizeId, 1, @Pos - 1)
			SET @SizeNo = SUBSTRING(@HYSizeId, @Pos + 1, LEN(@HYSizeId) - @Pos)
		END
		ELSE
		BEGIN
			SET @SizeChar = @HYSizeId
			SET @SizeNo = @HYSizeId
		END
		
		-- If the specific size of product is not in tProductSize
		IF @SizeChar NOT IN ('XS','S','M','L','XL','Onesize', 'Storlekslös')
		   AND @HYSizeId NOT IN (SELECT SocksHYText FROM integration.tSocks)
		BEGIN
			SET @Data = 'NEW tProductSIZE Shoes: @HYErpIdSize ' + @HYErpIdSize + ' @Fok ' + CAST(@ChannelId AS VARCHAR(10)) + ' @SizeChar ' + @SizeChar + ' @SizeNo ' + @SizeNo
		
			SET @SizeId = import.fGetSizeId(@SizeChar, @SizeNo)
			
			SET @NeedToInsertProductSize = 1
		END
		
		ELSE IF @SizeChar NOT IN ('Onesize', 'Storlekslös')
			 AND @HYSizeId NOT IN (SELECT SocksHYText FROM integration.tSocks)
		BEGIN
			SET @Data = 'NEW tProductSIZE Accessories: @HYErpIdSize ' + @HYErpIdSize + ' @Fok ' + CAST(@ChannelId AS VARCHAR(10)) + ' @SizeChar ' + @SizeChar
			
			SET @SizeId = (SELECT AccessoriesSizeId 
						   FROM integration.tAccessoriesSize 
						   WHERE AccessoriesSize = @SizeChar)
			
			SET @NeedToInsertProductSize = 1
		END
		
		ELSE IF @HYSizeId IN (SELECT SocksHYText FROM integration.tSocks)
		BEGIN
			SET @Data = 'NEW tProductSIZE Tags: @HYErpIdSize ' + @HYErpIdSize + ' @Fok ' + CAST(@ChannelId AS VARCHAR(10)) + ' @HYSizeId ' + @HYSizeId
		
			-- Tag socks with the appropriate tags
			-- if it is in socks category
			SET @CategoryId = (SELECT CategoryId 
							   FROM product.tCategory 
							   WHERE ErpId = 'C_' 
									 + ISNULL(@VaruklassId, '') + '-' 
									 + ISNULL(@VarugruppId, '') + '-' 
									 + ISNULL(@VarukodId, ''))
			IF @CategoryId IN (1000209, 1000148) -- 'Strumpor' in 'Skor' AND 'Strumpor' in 'Skotillbehör'
			BEGIN
				-- New only socks							
				SET @AccessorySizeId = (SELECT AccessoriesSizeId 
										FROM integration.tAccessoriesSize 
										WHERE AccessoriesSize = @HYSizeId)
				SET @SizeId = (SELECT SizeId FROM lekmer.tSize WHERE EU = @AccessorySizeId)
				
				SET @NeedToInsertProductSize = 1
				
				-- add tags
				INSERT INTO lekmer.tProductTag (
					ProductId,
					TagId
				)
				SELECT
					@ProductId,
					st.TagId
				FROM
					integration.tSocks s
					INNER JOIN integration.tSocksTagHYIdMapping st ON s.SocksHYId = st.SocksHYId
				WHERE
					s.SocksHYText = @HYSizeId
					AND st.TagId NOT IN
									(SELECT TagId 
									 FROM lekmer.tProductTag
									 WHERE productId = @ProductId)																				
			END
		END
		
		IF (@NeedToInsertProductSize = 1 
			AND @SizeId IS NOT NULL 
			AND @ProductId IS NOT NULL)
		BEGIN
			IF NOT EXISTS (SELECT 1
						   FROM [lekmer].tProductSize ps
						   WHERE ps.ProductId = @ProductId 
						   AND ps.SizeId = @SizeId)
			BEGIN
				INSERT INTO [lekmer].tProductSize (
					ProductId, 
					SizeId, 
					ErpId, 
					NumberInStock
				)
				VALUES (
					@ProductId,
					@SizeId, 
					@HYErpIdSize,
					@NumberInStock
				)
			END
		END
		
		COMMIT	
	END TRY
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		
		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			@Data,
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)

	END CATCH
		
END
GO
PRINT N'Altering [lekmer].[pProductSave]'
GO

ALTER PROCEDURE [lekmer].[pProductSave]
	@ProductId				INT,
	@ItemsInPackage			INT,
	@ErpId					VARCHAR(50),
	@EANCode				VARCHAR(20),
	@Title					NVARCHAR(256),
	@WebShopTitle			NVARCHAR(256),
	@ProductStatusId		INT,
	@Description			NVARCHAR(MAX),
	@ShortDescription		NVARCHAR(MAX),
	@CategoryId				INT,
	@MediaId				INT,
	@BrandId				INT,
	@AgeFromMonth			INT,
	@AgeToMonth				INT,
	@IsBookable				BIT,
	@IsNewFrom				DATETIME,
	@IsNewTo				DATETIME,
	@Measurement			NVARCHAR(MAX),
	@BatteryTypeId			INT,
	@NumberOfBatteries		INT,
	@IsBatteryIncluded		BIT,
	@ExpectedBackInStock	DATETIME,
	@CreatedDate			DATETIME,
	@SizeDeviationId		INT,
	@ShowVariantRelations	BIT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tProduct]
	SET
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	WHERE
		[ProductId] = @ProductId
		
	UPDATE
		[lekmer].[tLekmerProduct]
	SET
		[BrandId]				= @BrandId,
		[AgeFromMonth]			= @AgeFromMonth,
		[AgeToMonth]			= @AgeToMonth,
		[IsBookable]			= @IsBookable,
		[IsNewFrom]				= @IsNewFrom,
		[IsNewTo]				= @IsNewTo,
		[Measurement]			= @Measurement,
		[BatteryTypeId]			= @BatteryTypeId,
		[NumberOfBatteries]		= @NumberOfBatteries,
		[IsBatteryIncluded]		= @IsBatteryIncluded,
		[ExpectedBackInStock]	= @ExpectedBackInStock,
		[CreatedDate]			= @CreatedDate,
		[SizeDeviationId]		= @SizeDeviationId,
		[ShowVariantRelations]	= @ShowVariantRelations
	WHERE
		[ProductId] = @ProductId
END
GO
PRINT N'Altering [lekmer].[vFilterProduct]'
GO
ALTER view [lekmer].[vFilterProduct]
as
	select
		p.ProductId 'FilterProduct.ProductId',
		p.CategoryId 'FilterProduct.CategoryId',
		COALESCE(pt.Title, p.Title) 'FilterProduct.Title',
		lp.BrandId 'FilterProduct.BrandId',
		lp.AgeFromMonth 'FilterProduct.AgeFromMonth',
		lp.AgeToMonth 'FilterProduct.AgeToMonth',
		lp.IsNewFrom 'FilterProduct.IsNewFrom',
		ch.ChannelId 'FilterProduct.ChannelId',
		ch.CurrencyId 'FilterProduct.CurrencyId',
		pmc.PriceListRegistryId 'FilterProduct.PriceListRegistryId',
		lpp.[ProductPopularity.Popularity] AS 'FilterProduct.Popularity',
		p.NumberInStock 'FilterProduct.NumberInStock'
	from
		product.tProduct p
		inner join lekmer.tLekmerProduct lp on p.ProductId = lp.ProductId

		INNER JOIN product.tProductRegistryProduct AS prp ON p.ProductId = prp.ProductId
		INNER JOIN product.tProductModuleChannel AS pmc ON prp.ProductRegistryId = pmc.ProductRegistryId
		INNER JOIN core.tChannel AS ch ON pmc.ChannelId = ch.ChannelId

		LEFT JOIN product.tProductTranslation AS pt
			ON p.ProductId = pt.ProductId
			AND ch.LanguageId = pt.LanguageId
			
		LEFT JOIN [lekmer].[vProductPopularity] lpp on pmc.[ChannelId] = lpp.[ProductPopularity.ChannelId] AND p.[ProductId] = lpp.[ProductPopularity.ProductId]

	WHERE
		p.IsDeleted = 0
		AND p.ProductStatusId = 0
GO
PRINT N'Altering [lekmer].[pFilterProductGetAll]'
GO
ALTER procedure [lekmer].[pFilterProductGetAll]
	@CustomerId int,
	@ChannelId	int
as
begin
	SET NOCOUNT ON;
	
	select
		/*Select only necessary columns (see data mapper)*/
		[FilterProduct.ProductId],
		[FilterProduct.CategoryId],
		--[FilterProduct.Title] ,
		[FilterProduct.BrandId] ,
		[FilterProduct.AgeFromMonth] ,
		[FilterProduct.AgeToMonth] ,
		[FilterProduct.IsNewFrom] ,
		--[FilterProduct.ChannelId] ,
		--[FilterProduct.CurrencyId] ,
		--[FilterProduct.PriceListRegistryId] ,
		[FilterProduct.Popularity] ,
		--[Price.PriceListId] ,
		--[Price.ProductId] ,
		[Price.PriceIncludingVat],
		--[Price.PriceExcludingVat] ,
		--[Price.VatPercentage]
		[FilterProduct.NumberInStock]
	from
		lekmer.vFilterProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[FilterProduct.ProductId]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[FilterProduct.CurrencyId],
				p.[FilterProduct.ProductId],
				p.[FilterProduct.PriceListRegistryId],
				@CustomerId
			)
	where
		[FilterProduct.ChannelId] = @ChannelId
	order by
		[FilterProduct.Title]
end
GO
PRINT N'Altering [media].[vCustomImageSecure]'
GO



ALTER view [media].[vCustomImageSecure]
as
	select
		IM.*,
		LI.Link as 'LekmerImage.Link',
		LI.Parameter  as 'LekmerImage.Parameter',
		LI.TumbnailImageUrl  as 'LekmerImage.TumbnailImageUrl',
		LI.HasImage  as 'LekmerImage.HasImage'
	from
		[media].[vImageSecure] as IM
		left join [lekmer].[tLekmerImage] as LI on LI.[MediaId]=IM.[Image.MediaId]
GO
PRINT N'Altering [media].[vCustomImage]'
GO


ALTER view [media].[vCustomImage]
as
	select
		IM.*,
		LI.Link as 'LekmerImage.Link',
		LI.Parameter  as 'LekmerImage.Parameter',
		LI.TumbnailImageUrl  as 'LekmerImage.TumbnailImageUrl',
		LI.HasImage  as 'LekmerImage.HasImage'
	from
		[media].[vImage] as IM
		left join [lekmer].[tLekmerImage] as LI on LI.[MediaId]=IM.[Image.MediaId]
GO
PRINT N'Altering [product].[vCustomProductRecord]'
GO


sp_refreshview 'product.vProductRecord'
GO



ALTER VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations]
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
GO
PRINT N'Altering [integration].[usp_CreateProductUrls]'
GO

ALTER PROCEDURE [integration].[usp_CreateProductUrls]
	@ProductUrlXml XML,
	@HistoryLifeIntervalTypeId INT = 3

	-- Returns.....: 0 on Valid, 1 on failure
AS
BEGIN

	/*
	<products>
		<product id="103024" title="100124-0014" languageid="1"/>
		<product id="103025" title="100124-0014" languageid="1"/>
		<product id="103026" title="100124-0014" languageid="1"/>
	</products>
	*/

	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tProductUrls') IS NOT NULL
		DROP TABLE #tProductUrls
		
	CREATE TABLE #tProductUrls
	(
		ProductId INT NOT NULL,
		Title NVARCHAR (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
		LanguageId INT NOT NULL
		CONSTRAINT PK_#tProductUrls PRIMARY KEY(ProductId, LanguageId)
	)

	BEGIN TRY
		BEGIN TRANSACTION

			-- Populate temp table
			INSERT #tProductUrls (
				ProductId,
				Title,
				LanguageId
			)
			SELECT
				T.c.value('@id[1]', 'int'),
				T.c.value('@title[1]', 'nvarchar(250)'),
				T.c.value('@languageid[1]', 'int')
				--T.c.value('@id[1]', 'int'),
				--T.c.value('@height[1]', 'int'),
				--T.c.value('@width[1]', 'int'),
				--T.c.value('@extension[1]', 'nvarchar(10)')
			FROM
				@ProductUrlXml.nodes('/products/product') T(c)

			--SELECT * FROM #tProductUrls

			--Insert into tProductUlrHistory
			INSERT INTO [lekmer].[tProductUrlHistory] (
				ProductId,
				LanguageId,
				UrlTitleOld,
				HistoryLifeIntervalTypeId
			)
			SELECT
				pu.ProductId,
				pu.LanguageId,
				pu.UrlTitle,
				@HistoryLifeIntervalTypeId
			FROM
				[lekmer].[tProductUrl] pu
				INNER JOIN #tProductUrls ipu ON ipu.ProductId = pu.ProductId AND ipu.LanguageId = pu.LanguageId
			WHERE
				pu.UrlTitle != ipu.Title

			--Remove from tProductUlrHistory
			DELETE
				puh
			FROM
				[lekmer].[tProductUrlHistory] puh				
				INNER JOIN (
					SELECT
						ipu.*
					FROM
						[lekmer].[tProductUrl] pu
						INNER JOIN #tProductUrls ipu ON ipu.ProductId = pu.ProductId AND ipu.LanguageId = pu.LanguageId
					WHERE
						pu.UrlTitle != ipu.Title
				) AS puhNew ON puhNew.ProductId = puh.ProductId AND puhNew.LanguageId = puh.LanguageId AND puhNew.Title = puh.UrlTitleOld
			
			-- Delete from tProductUrl
			DELETE
				p
			FROM
				lekmer.tProductUrl p
				INNER JOIN #tProductUrls purl ON p.ProductId = purl.ProductId AND p.LanguageId = purl.LanguageId

			-- Insert into tProductUrl
			INSERT lekmer.tProductUrl(
				ProductId,
				LanguageId,
				UrlTitle
			)
			SELECT
				ProductId,
				LanguageId,
				Title
			FROM
				#tProductUrls

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH

		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

		--RETURN @Return
	END CATCH		
END
GO
PRINT N'Altering [integration].[pCreateProductUrls]'
GO

ALTER PROCEDURE [integration].[pCreateProductUrls]
	@ProductUrlXml XML,
	@HistoryLifeIntervalTypeId INT = 3
AS
BEGIN
	-- this SP was cleaned because it has the same logic as [integration].[usp_CreateProductUrls].
	-- TODO: Remove this SP  and use [integration].[usp_CreateProductUrls] instead.
	EXEC [integration].[usp_CreateProductUrls] @ProductUrlXml, @HistoryLifeIntervalTypeId
END
GO
PRINT N'Altering [import].[pImportProduct]'
GO

ALTER PROCEDURE [import].[pImportProduct]

AS
BEGIN
	SET NOCOUNT ON

	EXEC [import].[pImportCategory]
	EXEC [import].[pImportBrand]
	EXEC [integration].[usp_UpdateBrandHeppoCreationDate] 
	EXEC [import].[pUpdateProduct]
	EXEC [import].[pUpdateProductNumberInStock]
	
	DECLARE 
		@NeedToInsertProductRelationData BIT,
	
		@HYErpId NVARCHAR(50),
		@HYErpIdSize NVARCHAR(50),
		@Channel INT,
		@NumberInStock INT,
		@ArticleTitle NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@SizeId NVARCHAR(25), -- for taging socks
		@Price DECIMAL,
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@VarugruppTitel NVARCHAR(50),
		@VarukodTitel NVARCHAR(50),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL = 25.0,
		
		@PrevHYErpId NVARCHAR(50),
		@PrevChannel INT,
		
		@HYArticleClassId NVARCHAR(50),
		@CategoryErpId NVARCHAR(50),
		@CategoryId INT,
		@BrandId INT,
		@NewFromDate DATETIME,
		@NewToDate DATETIME,
		@TagGroupId_CategoryLevel3 INT = 1000008,
		@TagId_1 INT,
		@TagId_2 INT,
		@TradeDoublerProductGroup NVARCHAR(50),
		@ChannelId INT,
		@IsProductRegistry BIT
		
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 1, @NewFromDate)
	
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT 
			tp.HYErpId,
			tp.HYErpIdSize,
			tp.ChannelId,
			tp.ArticleTitle,
			tp.SizeId,
			tp.Price/100,
			tp.NumberInStock,
			tp.ArticleGroupId,
			tp.ArticleGroupTitle,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.ArticleCodeTitle,
			tp.BrandId
		FROM
			[import].tProduct tp

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO 
			@HYErpId,
			@HYErpIdSize,
			@Channel,			 
			@ArticleTitle,
			@SizeId,
			@Price,
			@NumberInStock,
			@VarugruppId,
			@VarugruppTitel,
			@VaruklassId,
			@VarukodId,
			@VarukodTitel,
			@HYBrandId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			SET @IsProductRegistry = 0
		
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYErpId)
			IF @ProductId IS NOT NULL
			BEGIN
				IF EXISTS (SELECT 1 FROM product.tProductRegistryProduct prp
						   WHERE prp.ProductId = @ProductId
						   AND prp.ProductRegistryId = @Channel)
				BEGIN
					SET @IsProductRegistry = 1
					GOTO PRODUCT_END
				END
			END
			
			PRODUCT_START:
			
			-- Skip processing the same @HYErpId + @Channel few times
			IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			BEGIN
				GOTO PRODUCT_END
			END ELSE BEGIN
				SET	@PrevHYErpId = @HYErpId
				SET	@PrevChannel = @Channel
			END
			
			BEGIN TRANSACTION
			
			SET @NeedToInsertProductRelationData = 0
			
			SET @HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '')
			SET @CategoryErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, '')

			SET @CategoryId = (SELECT CategoryId FROM product.tCategory WHERE ErpId = @CategoryErpId)
			
			SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)
							
			IF @ProductId IS NULL 
			BEGIN
				SET @NeedToInsertProductRelationData = 1
			
				-- Data that was sent into insert
				SET @Data = 'NEW: HYErpId ' + @HYErpId + ' @Fok ' + CAST(@Channel AS VARCHAR(10)) + 'HY-Cat ' + @CategoryErpId 
				
				-- tProduct
				INSERT INTO product.tProduct (
					ErpId,  
					IsDeleted, 
					NumberInStock, 
					CategoryId, 
					Title, 
					[Description], 
					ProductStatusId
				)
				VALUES (
					@HYErpId,
					0, --IsDeleted
					@NumberInStock, 
					@CategoryId,
					@ArticleTitle, 
					'', --Description, 
					6 --ProductStatusId = 'Ready to translate'
				)
											
				SET @ProductId = SCOPE_IDENTITY()
											
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId, 
					HYErpId, 
					BrandId, 
					IsBookable, 
					AgeFromMonth, 
					AgeToMonth, 
					IsNewFrom, 
					IsNewTo, 
					IsBatteryIncluded, 
					ExpectedBackInStock
				)
				VALUES (
					@ProductId, 
					@HYErpId,
					@BrandId,	
					0, --IsBookable, 
					0, --AgeFromMonth, 
					0, --AgeToMonth, 
					@NewFromDate,
					@NewToDate, 
					0, --IsBatteryIncluded, 
					NULL --ExpectedBackInStock
				)
				
				------------------------------------------------------------------------------------------
				-- tProductNews
				-- Flag so that the product is and has been the news
				
				IF (@ProductId NOT IN (SELECT productId FROM integration.[tProductNews]))
				BEGIN
					INSERT INTO integration.tProductNews (
						ProductId,
						ErpId
					)
					VALUES (
						@ProductId,
						@HYErpId
					)
				END
					
				------------------------------------------------------------------------------------------
				-- tProductTag
				-- tag with categoryLevel3 (sneaker boots, etc.)

				SET @TagId_1 = NULL

				SELECT @TagId_1 = TagId
				FROM lekmer.tTag
				WHERE Value = @VarukodTitel
					  AND TagGroupId = @TagGroupId_CategoryLevel3

				IF @TagId_1 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1
								   FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND Tagid = @TagId_1)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_1
					END
				END

				------------------------------------------------------------------------------------------
				-- tProductTag
				-- Tag man/kvinna/pojke/flocka/unisex

				SET @TagId_1 = NULL
				SET @TagId_2 = NULL

				IF @VarugruppTitel IN ('Kvinna', 'Man', 'Flickskor', 'Pojkskor')
				BEGIN
					SELECT @TagId_1 = TagId
					FROM lekmer.tTag
					WHERE Value = @VarugruppTitel
						  AND TagGroupId = 1000002
				END
				ELSE IF @VarugruppTitel = ('Unisex barn')
				BEGIN
					SET @TagId_1 = 1000013 -- Pojkskor
					SET @TagId_2 = 1000012 -- Flickskor
				END
				ELSE IF @VarugruppTitel = ('Unisex kvinna/man')
				BEGIN
					SET @TagId_1 = 1000009 -- Man
					SET @TagId_2 = 1000010 -- Kvinna
				END

				IF @TagId_1 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND TagId = @TagId_1)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_1
					END
				END

				IF @TagId_2 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND TagId = @TagId_2)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_2
					END
				END
				------------------------------------------------------------------------------------------
			END -- Product already exist in tLekmerProduct
			ELSE IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem prp WHERE prp.ProductId = @ProductId AND prp.PriceListId = @Channel)
			BEGIN
				SET @NeedToInsertProductRelationData = 1

				SET @Data = 'EXISTING: @HYErpId ' + @HYErpId + ' @Fok ' + CAST(@Channel AS VARCHAR(10)) + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
			END

			IF @NeedToInsertProductRelationData = 1
			BEGIN
				------------------------------------------------------------------------------------------
				-- tProductRegistryProduct

				IF @IsProductRegistry = 0
				BEGIN
					INSERT INTO product.tProductRegistryProduct (
						ProductId, 
						ProductRegistryId
					)
					VALUES (
						@ProductId,
						@Channel
					)
				END

				------------------------------------------------------------------------------------------
				-- tPriceListItem
				-- PriceListId and @Channel have same values

				IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem
							   WHERE ProductId = @ProductId
							   AND PriceListId = @Channel)
				BEGIN
					INSERT INTO product.tPriceListItem (
						PriceListId,
						ProductId,
						PriceIncludingVat,
						PriceExcludingVat,
						VatPercentage
					)
					VALUES (
						@Channel,
						@ProductId,
						@Price,
						@Price / (1.0+@Vat/100.0),
						@Vat
					)
				END

				------------------------------------------------------------------------------------------ 
				-- tTradeDoublerProductGroupMapping

				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = @HYArticleClassId
													   AND ChannelId = @Channel)

				SET @ChannelId = (SELECT CASE WHEN @Channel = 1 THEN 1
											  WHEN @Channel = 2 THEN 4
											  WHEN @Channel = 3 THEN 657
											  WHEN @Channel = 4 THEN 1000003
										 END)

				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM  lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId
										 AND tTDPGM.ChannelId = @ChannelId)
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							@ChannelId
						)
					END
				END 
			END

			COMMIT
			
			PRODUCT_END:
			
			--ProductSize
			
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [import].[pImportProductSize]
				@HYErpId,
				@HYErpIdSize,
				@Channel, -- Channel id value from HY
				@SizeId,
				@NumberInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@ProductId
			
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK

			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
			
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)
			
		END CATCH

		FETCH_NEXT: --LABEL
		FETCH NEXT FROM cur_product INTO
			 @HYErpId,
			 @HYErpIdSize,
			 @Channel,			 
			 @ArticleTitle,
			 @SizeId,
			 @Price,
			 @NumberInStock,
			 @VarugruppId,
			 @VarugruppTitel,
			 @VaruklassId,
			 @VarukodId,
			 @VarukodTitel,
			 @HYBrandId
	END

	CLOSE cur_product
	DEALLOCATE cur_product


	EXEC [import].[pUpdateProductBrand]
	EXEC [import].[pAddProductColorTags]
	-- temp lösning
	EXEC [integration].[usp_BrandRestrictionsFok]	
	EXEC [integration].[usp_REATagProduct]
	
	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]
END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO

sp_refreshview 'product.vProductSecure'
GO

ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		

GO
PRINT N'Altering [core].[vCustomChannel]'
GO


ALTER VIEW [core].[vCustomChannel]
AS
	SELECT
		vC.*,
		vLC.[Channel.TimeFormat],
		vLC.[Channel.WeekDayFormat],
		vLC.[Channel.DayFormat],
		vLC.[Channel.DateTimeFormat],
		vLC.[Channel.TimeZoneDiff]
	FROM
		[core].[vChannel] AS vC
		LEFT OUTER JOIN [lekmer].[vLekmerChannel] vLC ON vLC.[Channel.Id] = vC.[Channel.Id]


GO
PRINT N'Altering [product].[vCustomProductView]'
GO


sp_refreshview 'product.vProductView'
GO


ALTER VIEW [product].[vCustomProductView]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]




GO
PRINT N'Altering [product].[vCustomProduct]'
GO


ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]

GO
PRINT N'Creating primary key [PK_tProduct] on [import].[tProduct]'
GO
ALTER TABLE [import].[tProduct] ADD CONSTRAINT [PK_tProduct] PRIMARY KEY CLUSTERED  ([ChannelId], [HYErpIdSize])
GO
PRINT N'Creating primary key [PK_tTradeDoublerProductGroupMapping] on [integration].[tTradeDoublerProductGroupMapping]'
GO
ALTER TABLE [integration].[tTradeDoublerProductGroupMapping] ADD CONSTRAINT [PK_tTradeDoublerProductGroupMapping] PRIMARY KEY CLUSTERED  ([HYArticleClassId], [ChannelId])
GO
PRINT N'Creating primary key [PK_tTradeDoublerProductGroupMapping] on [lekmer].[tTradeDoublerProductGroupMapping]'
GO
ALTER TABLE [lekmer].[tTradeDoublerProductGroupMapping] ADD CONSTRAINT [PK_tTradeDoublerProductGroupMapping] PRIMARY KEY CLUSTERED  ([ProductId], [ChannelId])
GO
PRINT N'Creating index [IX_tBrand_ErpId] on [lekmer].[tBrand]'
GO
CREATE NONCLUSTERED INDEX [IX_tBrand_ErpId] ON [lekmer].[tBrand] ([ErpId])
GO
PRINT N'Creating index [IX_tProductSize_ErpId] on [lekmer].[tProductSize]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductSize_ErpId] ON [lekmer].[tProductSize] ([ErpId])
GO
PRINT N'Creating index [IX_tTag_Value] on [lekmer].[tTag]'
GO
CREATE NONCLUSTERED INDEX [IX_tTag_Value] ON [lekmer].[tTag] ([Value])
GO
PRINT N'Creating index [IX_tWishList_WishListKey] on [lekmer].[tWishList]'
GO
CREATE NONCLUSTERED INDEX [IX_tWishList_WishListKey] ON [lekmer].[tWishList] ([WishListKey])
GO
PRINT N'Creating index [IX_tPriceListItem_ProductId] on [product].[tPriceListItem]'
GO
CREATE NONCLUSTERED INDEX [IX_tPriceListItem_ProductId] ON [product].[tPriceListItem] ([ProductId]) INCLUDE ([PriceIncludingVat], [PriceListId])
GO
PRINT N'Creating index [IX_tProductRegistryProduct_ProductId_(ProductRegistryId)] on [product].[tProductRegistryProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct_ProductId_(ProductRegistryId)] ON [product].[tProductRegistryProduct] ([ProductId]) INCLUDE ([ProductRegistryId])
GO
PRINT N'Creating index [IX_tProductRegistryProduct_ProductRegistryId] on [product].[tProductRegistryProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct_ProductRegistryId] ON [product].[tProductRegistryProduct] ([ProductRegistryId])
GO
PRINT N'Creating index [IX_tProductRegistryProduct_ProductRegistryId_ProductId] on [product].[tProductRegistryProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct_ProductRegistryId_ProductId] ON [product].[tProductRegistryProduct] ([ProductRegistryId], [ProductId])
GO
PRINT N'Altering extended properties'
GO
EXEC sp_updateextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[34] 4[32] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 234
               Right = 230
            End
            DisplayFlags = 280
            TopColumn = 6
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 3075
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'lekmer', 'VIEW', N'vLekmerProduct', NULL, NULL
GO



--ROLLBACK
--RAISERROR('Remove ROLLBACK first!', 18, 0)
COMMIT
--/////////////////////////////////////