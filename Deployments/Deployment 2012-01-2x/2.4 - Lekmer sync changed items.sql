SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
USE [Lekmer_live]
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

--/////////////////////////////////////
BEGIN TRANSACTION


PRINT N'Dropping foreign keys from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP
CONSTRAINT [FK_tLekmerProduct_tSizeDeviation]
GO
PRINT N'Dropping constraints from [integration].[tBackofficeProductInfoImport]'
GO
ALTER TABLE [integration].[tBackofficeProductInfoImport] DROP CONSTRAINT [PK_tBackofficeProductInfoImport]
GO
PRINT N'Dropping constraints from [lekmer].[tTradeDoublerProductGroupMapping]'
GO
ALTER TABLE [lekmer].[tTradeDoublerProductGroupMapping] DROP CONSTRAINT [PK__tTradeDo__E780F84C1FD6714A]
GO
PRINT N'Dropping index [IX_tLekmerProduct] from [lekmer].[tLekmerProduct]'
GO
DROP INDEX [IX_tLekmerProduct] ON [lekmer].[tLekmerProduct]
GO
PRINT N'Dropping index [IX] from [product].[tPriceListItem]'
GO
DROP INDEX [IX] ON [product].[tPriceListItem]
GO
PRINT N'Dropping index [IX_tProductRegistryProduct] from [product].[tProductRegistryProduct]'
GO
DROP INDEX [IX_tProductRegistryProduct] ON [product].[tProductRegistryProduct]
GO
PRINT N'Altering [integration].[pReportCategorysMissing]'
GO

ALTER PROCEDURE [integration].[pReportCategorysMissing]

AS
begin
	set nocount on
	begin try
				
	select HYArticleid, ArticleClassTitle, isnull(ArticleClassId, 'NULL') as ArticleClassId, 
								ArticleGroupTitle, isnull(ArticleGroupId, 'NULL') as ArticleGroupId,
								ArticleCodeTitle, isnull(ArticleCodeId , 'NULL') as ArticleCodeId
			from integration.tempProduct
			where ArticleClassTitle = '***Missing***'
			or ArticleGroupTitle = '***Missing***'
			or ArticleCodeTitle = '***Missing***'
			

	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
PRINT N'Rebuilding [integration].[tBackofficeProductInfoImport]'
GO
CREATE TABLE [integration].[tmp_rg_xx_tBackofficeProductInfoImport]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[HYErpId] [nvarchar] (20) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ChannelNameISO] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TagIdCollection] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[UserName] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[InsertedDate] [smalldatetime] NOT NULL
)
GO
SET IDENTITY_INSERT [integration].[tmp_rg_xx_tBackofficeProductInfoImport] ON
GO
INSERT INTO [integration].[tmp_rg_xx_tBackofficeProductInfoImport]([Id], [HYErpId], [Title], [Description], [ChannelNameISO], [UserName], [InsertedDate]) SELECT [Id], [HYErpId], [Title], [Description], [ChannelNameISO], [UserName], [InsertedDate] FROM [integration].[tBackofficeProductInfoImport]
GO
SET IDENTITY_INSERT [integration].[tmp_rg_xx_tBackofficeProductInfoImport] OFF
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[integration].[tBackofficeProductInfoImport]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[integration].[tmp_rg_xx_tBackofficeProductInfoImport]', RESEED, @idVal)
GO
DROP TABLE [integration].[tBackofficeProductInfoImport]
GO
EXEC sp_rename N'[integration].[tmp_rg_xx_tBackofficeProductInfoImport]', N'tBackofficeProductInfoImport'
GO
PRINT N'Creating primary key [PK_tBackofficeProductInfoImport] on [integration].[tBackofficeProductInfoImport]'
GO
ALTER TABLE [integration].[tBackofficeProductInfoImport] ADD CONSTRAINT [PK_tBackofficeProductInfoImport] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Altering [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD
[ShowVariantRelations] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_ShowVariantRelations] DEFAULT ((0))
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP
COLUMN [OldUrl]
GO
PRINT N'Creating index [IX_tLekmerProduct_HYErpId] on [lekmer].[tLekmerProduct]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tLekmerProduct_HYErpId] ON [lekmer].[tLekmerProduct] ([HYErpId])
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO


ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	CAST((CASE WHEN EXISTS (SELECT 1 FROM lekmer.tProductSize s WHERE s.ProductId = p.ProductId) THEN 1 ELSE 0 END) AS BIT) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations'
FROM
	lekmer.tLekmerProduct AS p
GO
PRINT N'Altering [lekmer].[pVoucherDiscountActionGetById]'
GO
ALTER PROCEDURE [lekmer].[pVoucherDiscountActionGetById]
	@ActionId int
AS 
BEGIN 
	SELECT
		CCA.*,
		VA.*
	FROM
		campaign.vCustomCartAction as CCA
		left join lekmer.tVoucherAction VA on VA.CartActionId=CCA.[CartAction.Id]
	WHERE
		[CartAction.Id] = @ActionId
END 

GO
PRINT N'Altering [integration].[usp_ImportUpdateProductSizesLekmer]'
GO

ALTER PROCEDURE [integration].[usp_ImportUpdateProductSizesLekmer]
	@HYArticleNoFull NVARCHAR(50),
	@NoInStock NVARCHAR(250),
	@HYSizeValue NVARCHAR (50),
	@ProductId INT,
	@HYChannel NVARCHAR(40),
	@HYArticleNoColorSize NVARCHAR(50)
AS
BEGIN

	DECLARE 
		@Data NVARCHAR(4000),
		@SizeChar NVARCHAR(20),
		@SizeNo NVARCHAR(20),
		@SizeId INT,
		@AccessoriesSizeId INT,
		@NeedToInsertProductSize BIT
	
	IF SUBSTRING(@HYArticleNoFull, 3, 1) = 1
	IF @HYSizeValue != 'Onesize'
	IF NOT EXISTS(SELECT 1
				  FROM lekmer.tProductSize
				  WHERE ErpId = @HYArticleNoColorSize)
	BEGIN TRY
		BEGIN TRANSACTION

		SET @NeedToInsertProductSize = 0
		
		SET @SizeChar = @HYSizeValue
		SET @SizeNo = @HYSizeValue
		
		IF @SizeChar NOT IN ('XS', 'Small', 'Medium', 'Large', 'XL', 'Onesize', '**Onesize', 'Storlekslös', '**Storlekslös')
		BEGIN
			SET @Data = 'IF tProductSIZE Shoes: @HYArticleNoColorSize ' + @HYArticleNoColorSize + ' @HYChannel ' + @HYChannel + ' @SizeChar ' + @SizeChar + ' @SizeNo ' + @SizeNo
			
			SET @SizeId = (SELECT SizeId FROM lekmer.tSize WHERE EU = @SizeNo)
			
			SET @NeedToInsertProductSize = 1
		END
		ELSE IF @SizeChar NOT IN ('Onesize', '**Onesize', 'Storlekslös', '**Storlekslös')
		BEGIN
			SET @Data = 'ELSE IF tProductSIZE Accessories: @HYArticleNoColorSize ' + @HYArticleNoColorSize + ' @HYChannel ' + @HYChannel + ' @SizeChar ' + @SizeChar
			
			SET @AccessoriesSizeId = (SELECT AccessoriesSizeId FROM integration.tAccessoriesSize WHERE AccessoriesSize = @SizeChar)
			
			SET @SizeId = (SELECT SizeId FROM lekmer.tSize WHERE EU = @AccessoriesSizeId)
			
			SET @NeedToInsertProductSize = 1
		END

		IF @NeedToInsertProductSize = 1
		BEGIN
			IF NOT EXISTS (SELECT 1
						   FROM [lekmer].tProductSize ps
						   WHERE ps.ProductId = @ProductId 
						   AND ps.SizeId = @SizeId)
			BEGIN
				INSERT INTO [lekmer].tProductSize (
					ProductId,
					SizeId,
					ErpId,
					NumberInStock
				)
				VALUES (
					@ProductId,
					@SizeId,
					@HYArticleNoColorSize,
					@NoInStock
				)
			END
		END
	
		COMMIT	
	END TRY
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		
		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			@Data,
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)

	END CATCH
		
END
GO
PRINT N'Altering [integration].[usp_UpdateTCategoryLekmer]'
GO
ALTER PROCEDURE [integration].[usp_UpdateTCategoryLekmer]
AS
BEGIN
	DECLARE 
		@Seperator NVARCHAR(1),	
		@ErpCategoryPrefix NVARCHAR(2),
		@ErpClassId NVARCHAR(50),
		@ErpClassGroupId NVARCHAR(50),
		@ErpClassGroupCodeId NVARCHAR(50),
		@ClassGroupCodeId NVARCHAR(50),
		@HYErpId NVARCHAR(50),
		@ArticleClassId NVARCHAR(10),
		@ArticleClassTitle NVARCHAR(40),
		@ArticleGroupId NVARCHAR(10),
		@ArticleGroupTitle NVARCHAR(40),
		@ArticleCodeId NVARCHAR(10),
		@ArticleCodeTitle NVARCHAR(40)
	
	SET @Seperator = '-'
	SET @ErpCategoryPrefix = 'C_'
		
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT DISTINCT
			tp.HYErpId,
			tp.ArticleClassId ,
			tp.ArticleClassTitle,
			tp.ArticleGroupId,
			tp.ArticleGroupTitle,
			tp.ArticleCodeId,
			tp.ArticleCodeTitle
		FROM
			[import].tProduct tp
			LEFT JOIN lekmer.tLekmerProduct lp ON lp.HYErpId = tp.HYErpId 
			LEFT JOIN product.tProduct p ON p.ProductId = lp.ProductId
			LEFT JOIN product.tCategory c ON c.CategoryId = p.CategoryId
		WHERE
			tp.ChannelId = 1
			AND
			(
				@ErpCategoryPrefix 
					+ ISNULL(tp.ArticleClassId, '') + @Seperator 
					+ ISNULL(tp.ArticleGroupId, '') + @Seperator 
					+ ISNULL(tp.ArticleCodeId, '') <> c.ErpId
				OR
					c.ErpId is null
			)
		
	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO 
			@HYErpId,
			@ArticleClassId,
			@ArticleClassTitle,
			@ArticleGroupId,
			@ArticleGroupTitle,
			@ArticleCodeId,
			@ArticleCodeTitle
		
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @ClassGroupCodeId = @ArticleClassId + @Seperator + @ArticleGroupId + @Seperator + @ArticleCodeId
			
			BEGIN TRY
				BEGIN TRANSACTION
					SET @ErpClassId = @ErpCategoryPrefix + @ArticleClassId
					SET @ErpClassGroupId = @ErpClassId + @Seperator + @ArticleGroupId
					SET @ErpClassGroupCodeId = @ErpClassGroupId + @Seperator + @ArticleCodeId
				
					-- varuklass
					IF not exists (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassId)
						INSERT INTO 
							product.tCategory (ParentCategoryId, Title, ErpId)
						SELECT 	
							null,
							@ArticleClassTitle,
							@ErpClassId

					-- Varugrupp
					IF NOT EXISTS (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassGroupId)
						INSERT INTO 
							[product].tCategory (ParentCategoryId, Title, ErpId)
						SELECT 	
							(SELECT CategoryId FROM product.tCategory WHERE ErpId = @ErpClassId),
							@ArticleGroupTitle,		
							@ErpClassGroupId
					
					-- Varukod
					IF (NOT EXISTS (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassGroupCodeId)
						AND @ArticleCodeTitle <> '***Missing***' )
							INSERT INTO 
								[product].tCategory (ParentCategoryId, Title, ErpId)
							SELECT 	
								(SELECT CategoryId FROM product.tCategory WHERE ErpId = @ErpClassGroupId),
								@ArticleCodeTitle,		
								@ErpClassGroupCodeId

					-- ändra referensen på categoryId om den bytat
					UPDATE 
						tp
					SET 
						tp.CategoryId = (SELECT categoryId FROM product.tCategory 
										 WHERE ErpId = @ErpClassGroupCodeId)
					FROM 
						[lekmer].tLekmerProduct lp						
						INNER JOIN product.tProduct tp ON tp.ProductId = lp.ProductId				
					WHERE 
						lp.HYErpId = @HYErpId 
						AND tp.CategoryId <> (SELECT categoryId FROM product.tCategory 
											  WHERE ErpId = @ErpClassGroupCodeId)
					
				COMMIT
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0 ROLLBACK
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				VALUES(@ClassGroupCodeId, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

				CLOSE cur_product
				DEALLOCATE cur_product 
			END CATCH
		
			FETCH NEXT FROM cur_product 
				INTO 
					@HYErpId,
					@ArticleClassId,
					@ArticleClassTitle,
					@ArticleGroupId,
					@ArticleGroupTitle,
					@ArticleCodeId,
					@ArticleCodeTitle
		END
	CLOSE cur_product
	DEALLOCATE cur_product	              
END
GO
PRINT N'Altering [integration].[usp_CreateProductUrls]'
GO

ALTER PROCEDURE [integration].[usp_CreateProductUrls]
	@ProductUrlXml XML,
	@HistoryLifeIntervalTypeId INT = 3

	-- Returns.....: 0 on Valid, 1 on failure
AS
BEGIN

	/*
	<products>
		<product id="103024" title="100124-0014" languageid="1"/>
		<product id="103025" title="100124-0014" languageid="1"/>
		<product id="103026" title="100124-0014" languageid="1"/>
	</products>
	*/

	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tProductUrls') IS NOT NULL
		DROP TABLE #tProductUrls
		
	CREATE TABLE #tProductUrls
	(
		ProductId INT NOT NULL,
		Title NVARCHAR (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
		LanguageId INT NOT NULL
		CONSTRAINT PK_#tProductUrls PRIMARY KEY(ProductId, LanguageId)
	)

	BEGIN TRY
		BEGIN TRANSACTION

			-- Populate temp table
			INSERT #tProductUrls (
				ProductId,
				Title,
				LanguageId
			)
			SELECT
				T.c.value('@id[1]', 'int'),
				T.c.value('@title[1]', 'nvarchar(250)'),
				T.c.value('@languageid[1]', 'int')
				--T.c.value('@id[1]', 'int'),
				--T.c.value('@height[1]', 'int'),
				--T.c.value('@width[1]', 'int'),
				--T.c.value('@extension[1]', 'nvarchar(10)')
			FROM
				@ProductUrlXml.nodes('/products/product') T(c)

			--SELECT * FROM #tProductUrls

			--Insert into tProductUlrHistory
			INSERT INTO [lekmer].[tProductUrlHistory] (
				ProductId,
				LanguageId,
				UrlTitleOld,
				HistoryLifeIntervalTypeId
			)
			SELECT
				pu.ProductId,
				pu.LanguageId,
				pu.UrlTitle,
				@HistoryLifeIntervalTypeId
			FROM
				[lekmer].[tProductUrl] pu
				INNER JOIN #tProductUrls ipu ON ipu.ProductId = pu.ProductId AND ipu.LanguageId = pu.LanguageId
			WHERE
				pu.UrlTitle != ipu.Title

			--Remove from tProductUlrHistory
			DELETE
				puh
			FROM
				[lekmer].[tProductUrlHistory] puh				
				INNER JOIN (
					SELECT
						ipu.*
					FROM
						[lekmer].[tProductUrl] pu
						INNER JOIN #tProductUrls ipu ON ipu.ProductId = pu.ProductId AND ipu.LanguageId = pu.LanguageId
					WHERE
						pu.UrlTitle != ipu.Title
				) AS puhNew ON puhNew.ProductId = puh.ProductId AND puhNew.LanguageId = puh.LanguageId AND puhNew.Title = puh.UrlTitleOld
			
			-- Delete from tProductUrl
			DELETE
				p
			FROM
				lekmer.tProductUrl p
				INNER JOIN #tProductUrls purl ON p.ProductId = purl.ProductId AND p.LanguageId = purl.LanguageId

			-- Insert into tProductUrl
			INSERT lekmer.tProductUrl(
				ProductId,
				LanguageId,
				UrlTitle
			)
			SELECT
				ProductId,
				LanguageId,
				Title
			FROM
				#tProductUrls

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH

		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

		--RETURN @Return
	END CATCH		
END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProduct]'
GO

ALTER PROCEDURE [integration].[usp_ImportUpdateProduct]

AS
BEGIN
		--raiserror('errorMSG', 16, 1)
		-- tCategory proc kallas
		print 'Start tCategory'
		exec [integration].[usp_UpdateTCategory] -- skapar om artiklar om det behövs
		print 'End tCategory'
		print 'Start tUpdateBrand'
		exec [integration].usp_UpdateBrand
		print 'End tUpdateBrand'

		
		-- tProduct
		print 'Start update product'
		UPDATE 
			p
		SET 
			Title = tp.ArticleTitle, 
			--ItemsInPackage = null,
			EanCode = tp.EanCode, 
			--IsDeleted = 0, 
			--ColorId SAKNAS
			--SizeId SAKNAS
			NumberInStock = tp.NoInStock,
			-- Category kanske måste ändras om det skapar problem
			-- man ändrar den då till typ
			-- select catagoryId from tcategory where erpid = varukl+varugrupp+varukod 
			CategoryId = (select categoryId from product.tCategory where ErpId = 'C_'+tp.ArticleClassId+'-'+tp.ArticleGroupId+'-'+tp.ArticleCodeId)
			--CategoryId = tp.ArticleCodeId,	--varukodID det är den mest specifika!!
			--WebShopTitle = null,
			--[Description] = '',
			--ShortDescription = null,
			--MediaId = null, -- Måste sättas
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,17)	
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join [product].tProductRegistryProduct pli
					on pli.ProductId = lp.ProductId and
					pli.ProductRegistryId = substring(tp.HYarticleId, 3,1)
		WHERE 
			p.Title <> tp.ArticleTitle OR
			p.EanCode <> tp.EanCode OR
			p.NumberInStock <> tp.NoInStock
		 
		print 'End product update'
		
		---------------------------------------------
		print 'Start lekmer product update'
		exec integration.usp_ImportUpdateLekmerProduct
		print 'End lekmer product update'
	--------------------------------------		
		-- tPriceListItem - Update Prices from integration.tempProductPrice
		print 'Start price update'
		exec [integration].[usp_UpdateProductPRICE]
		print 'End price update'
	--------------------------------------
	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@EanCode nvarchar(50),
				@NoInStock nvarchar(250),
				@ArticleTitle nvarchar(250),
				@ProductId int,
				@HYBrandId nvarchar(25),
				
				@IsBookable bit,
				@ExpectedBackInStock datetime,
				@IsDeleted bit,
				@CategoryId int,
				@Description nvarchar(250),
				@Price decimal,
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10),
				
				@Data nvarchar(4000),
				@Vat decimal
				SET @Vat = 25
				SET @IsDeleted = 0
				SET @Description = ''
				SET @ExpectedBackInStock = null
				SET @IsBookable = 0
		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				------------------------------------
		
		declare cur_product cursor fast_forward for
			select
				tp.HYarticleId,
				tp.EanCode,
				tp.NoInStock,
				tp.ArticleTitle,
				(tp.Price/100),
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId,
				tp.BrandId
			from
				[integration].tempProduct tp
			where not exists(select 1
						-- parar lekmerErpId med temp tabell erpId (klipper fok) sen parar
						-- tProductregistry på lekmerProductId på det ErpId sen parar också foken med productRegProduct
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = substring(tp.HYarticleId, 5,17) and
							lp.ProductId = prp.ProductId and
							substring(tp.HYarticleId, 3,1) = prp.ProductRegistryId)
			
			
			
		--print 'Open Cursor'
		open cur_product
		fetch next from cur_product
			into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1) -- Fok här är nu 001 eller 002 osv beroende på vilket land artikeln tillhör
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)			
				
				
				-- if, kolla om eprid utan fok finns, om det gör hämta dens produktid och lägg in de produkt idet + fok
				-- i tproductRegistryProduct	
				
				if not exists(select 1 -- om erpid inte finns i tlekmerProduct finns det inte i nån fok, lägg in den då, annars lägg bara in i pReg
							from lekmer.tLekmerProduct lp
							where lp.HYErpId = @HYarticleIdNoFoK)
				begin	
					print 'insert into product.tproduct'
					INSERT INTO -- MediaId saknas
						[product].tProduct(ErpId, EanCode, IsDeleted, NumberInStock, CategoryId, Title, [Description], ProductStatusId)
					SELECT 
						@HYarticleIdNoFoK, -- @HYArticleId
						@EanCode, 
						@IsDeleted, --0
						@NoInStock, 
						(SELECT CategoryId FROM product.tCategory WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId), -- koll här
						@ArticleTitle, 
						@Description, --''
						0 --case when @NoInStock > 0 then 0 else 1 end (Ändrat alla produkter sätts som offline när de sätts in)
												
					set @ProductId = SCOPE_IDENTITY()
					
					-- Data that was sent into insert
					SET @Data = 'NEW: HYArticleId ' + @HYarticleId + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId as varchar(10))
		
					--IF @nrInStock > 0 -- om antal är mer än 0
					--begin
						--SET @IsBookable = 0
					--end
						
					print 'insert into lekmer.tlekmerProduct'		
					insert into lekmer.tLekmerProduct(ProductId, HYErpId, BrandId, IsBookable, AgeFromMonth, AgeToMonth, 
								IsNewFrom, IsNewTo, IsBatteryIncluded, ExpectedBackInStock, ShowVariantRelations)
					values(
					@ProductId, 
					@HYarticleIdNoFoK, --@HYarticleId 
					(Select b.BrandId from lekmer.tBrand b where b.ErpId = @HYBrandId),
					@IsBookable, 
					0, 
					0, 
					Convert(varchar, GETDATE(), 112),
					DATEADD(WK, 1, Convert(varchar, GETDATE(), 112)), 
					0, 
					@ExpectedBackInStock,
					0 -- ShowVariantRelations
					)
					print 'insert into product.tPriceListItem'
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, @ProductId, @Price, (@Price * ((100-@Vat)/100)), @Vat)
					print 'insert into product.tProductReg'		
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct (ProductId, ProductRegistryId)
					values(@ProductId, @Fok) -- Fok är färsäljningskanalen
		
					--commit
				end	
				else if not exists (select 1
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = @HYarticleIdNoFoK
							and lp.ProductId = prp.ProductId 
							and prp.ProductRegistryId = @Fok)
				begin
					select @ProductId = ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFoK
					set @Data = 'EXISTING: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					print 'insert into product.tproductRegProg (in else)'	
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct (ProductId, ProductRegistryId)
					values(@ProductId, @Fok)
					print 'insert into product.rPriceList (in else)'	
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFoK),
					@Price, (@Price * ((100-@Vat)/100)), @Vat)
					
					--SET @Data = 'HYArticleId ' + @HYarticleId + 'EanCode ' + @EanCode + 'CategoryId ' +cast(@CategoryId as nvarchar)
				end
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_product into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		end
		
		close cur_product
		deallocate cur_product
		


END
GO
PRINT N'Altering [integration].[pBackofficeProductImportById]'
GO


ALTER PROCEDURE [integration].[pBackofficeProductImportById]
	@HYErpId		nvarchar(20),
	@Title			nvarchar(256),
	@Description	nvarchar(max),
	@ChannelNameISO	nvarchar(10),
	@TagIds			VARCHAR(MAX),
	@UserName		nvarchar(100)
AS
begin
	set nocount on
	
	-- insert into tproducttransaltion
	declare @LanguageId int
	set @LanguageId = (
						select l.LanguageId 
						from core.tLanguage l
							inner join core.tChannel h
							on l.LanguageId = h.LanguageId
							inner join core.tCountry c
							on h.CountryId = c.CountryId
						where
							c.ISO = @ChannelNameISO
						)

	insert into product.tProductTranslation(ProductId, LanguageId)
	select
		p.ProductId,
		@LanguageId
	from
		product.tProduct p
	where
		not exists (select 1
						from product.tProductTranslation n
						where n.ProductId = p.ProductId and
						n.LanguageId = @LanguageId)
				
	begin try
		begin transaction				
			
		if @LanguageId != 1
			begin
				update pt
				set pt.Title = @Title
				from 
					lekmer.tLekmerProduct l
					inner join product.tProductTranslation pt
						on l.ProductId = pt.ProductId	
				where
					l.HYErpId = @HYErpId
					and pt.LanguageId = @LanguageId
					and (pt.Title != @Title or pt.Title is null)
				
				
				update pt
				set pt.[Description] = @Description
				from 
					lekmer.tLekmerProduct l
					inner join product.tProductTranslation pt
						on l.ProductId = pt.ProductId	
				where
					l.HYErpId = @HYErpId
					and pt.LanguageId = @LanguageId
					and (@Description is not null and @Description != '')
					and (pt.[Description] != @Description or pt.[Description] is null)
			end
		else
			begin
				update pt
				set pt.Title = @Title
				from 
					lekmer.tLekmerProduct l
					inner join product.tProduct pt
						on l.ProductId = pt.ProductId	
				where
					l.HYErpId = @HYErpId
						
				update pt
				set pt.[Description] = @Description
				from 
					lekmer.tLekmerProduct l
					inner join product.tProduct pt
						on l.ProductId = pt.ProductId	
				where
					l.HYErpId = @HYErpId
					and (@Description is not null and @Description != '')	
					and (pt.[Description] != @Description or pt.[Description] is null)
			end
			
		-- Update/Insert product tags.
		DECLARE @TagId INT
		DECLARE cur_tag CURSOR FAST_FORWARD FOR
			SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')
			
		OPEN cur_tag
		FETCH NEXT FROM cur_tag INTO @TagId
			
		WHILE @@FETCH_STATUS = 0
			BEGIN
				-- Update/Insert product tags.
				IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
						   INNER JOIN product.tProduct p ON pt.ProductId = p.ProductId	
						   INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
						   WHERE 
								lp.HYErpId = @HYErpId
								AND pt.TagId = @TagId)
					BEGIN
						INSERT INTO lekmer.tProductTag (ProductId, TagId)
						VALUES ((SELECT TOP(1) p.ProductId FROM product.tProduct p 
								INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
								WHERE lp.HYErpId = @HYErpId), @TagId)
					END
				
				FETCH NEXT FROM cur_tag INTO @TagId
			END
		CLOSE cur_tag
		DEALLOCATE cur_tag
		
		-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
		insert into integration.tBackofficeProductInfoImport
		(
			[HYErpId]
			,[Title]
			,[Description]
			,[ChannelNameISO]
			,[TagIdCollection]
			,[UserName]
			,[InsertedDate]
		)
		VALUES 
		(
			@HYErpId
			,@Title
			,@Description
			,@ChannelNameISO
			,@TagIds
			,@UserName
			,CAST(GETDATE() AS smalldatetime)
		)
		
		commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end

GO
PRINT N'Altering [lekmer].[pProductSave]'
GO

ALTER PROCEDURE [lekmer].[pProductSave]
	@ProductId				INT,
	@ItemsInPackage			INT,
	@ErpId					VARCHAR(50),
	@EANCode				VARCHAR(20),
	@Title					NVARCHAR(256),
	@WebShopTitle			NVARCHAR(256),
	@ProductStatusId		INT,
	@Description			NVARCHAR(MAX),
	@ShortDescription		NVARCHAR(MAX),
	@CategoryId				INT,
	@MediaId				INT,
	@BrandId				INT,
	@AgeFromMonth			INT,
	@AgeToMonth				INT,
	@IsBookable				BIT,
	@IsNewFrom				DATETIME,
	@IsNewTo				DATETIME,
	@Measurement			NVARCHAR(MAX),
	@BatteryTypeId			INT,
	@NumberOfBatteries		INT,
	@IsBatteryIncluded		BIT,
	@ExpectedBackInStock	DATETIME,
	@CreatedDate			DATETIME,
	@SizeDeviationId		INT,
	@ShowVariantRelations	BIT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tProduct]
	SET
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	WHERE
		[ProductId] = @ProductId
		
	UPDATE
		[lekmer].[tLekmerProduct]
	SET
		[BrandId]				= @BrandId,
		[AgeFromMonth]			= @AgeFromMonth,
		[AgeToMonth]			= @AgeToMonth,
		[IsBookable]			= @IsBookable,
		[IsNewFrom]				= @IsNewFrom,
		[IsNewTo]				= @IsNewTo,
		[Measurement]			= @Measurement,
		[BatteryTypeId]			= @BatteryTypeId,
		[NumberOfBatteries]		= @NumberOfBatteries,
		[IsBatteryIncluded]		= @IsBatteryIncluded,
		[ExpectedBackInStock]	= @ExpectedBackInStock,
		[CreatedDate]			= @CreatedDate,
		[SizeDeviationId]		= @SizeDeviationId,
		[ShowVariantRelations]	= @ShowVariantRelations
	WHERE
		[ProductId] = @ProductId
END
GO
PRINT N'Altering [lekmer].[vFilterProduct]'
GO
ALTER view [lekmer].[vFilterProduct]
as
	select
		p.ProductId 'FilterProduct.ProductId',
		p.CategoryId 'FilterProduct.CategoryId',
		COALESCE(pt.Title, p.Title) 'FilterProduct.Title',
		lp.BrandId 'FilterProduct.BrandId',
		lp.AgeFromMonth 'FilterProduct.AgeFromMonth',
		lp.AgeToMonth 'FilterProduct.AgeToMonth',
		lp.IsNewFrom 'FilterProduct.IsNewFrom',
		ch.ChannelId 'FilterProduct.ChannelId',
		ch.CurrencyId 'FilterProduct.CurrencyId',
		pmc.PriceListRegistryId 'FilterProduct.PriceListRegistryId',
		lpp.[ProductPopularity.Popularity] AS 'FilterProduct.Popularity',
		p.NumberInStock 'FilterProduct.NumberInStock'
	from
		product.tProduct p
		inner join lekmer.tLekmerProduct lp on p.ProductId = lp.ProductId

		INNER JOIN product.tProductRegistryProduct AS prp ON p.ProductId = prp.ProductId
		INNER JOIN product.tProductModuleChannel AS pmc ON prp.ProductRegistryId = pmc.ProductRegistryId
		INNER JOIN core.tChannel AS ch ON pmc.ChannelId = ch.ChannelId

		LEFT JOIN product.tProductTranslation AS pt
			ON p.ProductId = pt.ProductId
			AND ch.LanguageId = pt.LanguageId
			
		LEFT JOIN [lekmer].[vProductPopularity] lpp on pmc.[ChannelId] = lpp.[ProductPopularity.ChannelId] AND p.[ProductId] = lpp.[ProductPopularity.ProductId]

	WHERE
		p.IsDeleted = 0
		AND p.ProductStatusId = 0
GO
PRINT N'Altering [lekmer].[pFilterProductGetAll]'
GO
ALTER procedure [lekmer].[pFilterProductGetAll]
	@CustomerId int,
	@ChannelId	int
as
begin
	SET NOCOUNT ON;
	
	select
		/*Select only necessary columns (see data mapper)*/
		[FilterProduct.ProductId],
		[FilterProduct.CategoryId],
		--[FilterProduct.Title] ,
		[FilterProduct.BrandId] ,
		[FilterProduct.AgeFromMonth] ,
		[FilterProduct.AgeToMonth] ,
		[FilterProduct.IsNewFrom] ,
		--[FilterProduct.ChannelId] ,
		--[FilterProduct.CurrencyId] ,
		--[FilterProduct.PriceListRegistryId] ,
		[FilterProduct.Popularity] ,
		--[Price.PriceListId] ,
		--[Price.ProductId] ,
		[Price.PriceIncludingVat],
		--[Price.PriceExcludingVat] ,
		--[Price.VatPercentage]
		[FilterProduct.NumberInStock]
	from
		lekmer.vFilterProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[FilterProduct.ProductId]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[FilterProduct.CurrencyId],
				p.[FilterProduct.ProductId],
				p.[FilterProduct.PriceListRegistryId],
				@CustomerId
			)
	where
		[FilterProduct.ChannelId] = @ChannelId
	order by
		[FilterProduct.Title]
end
GO
PRINT N'Altering [media].[vCustomImageSecure]'
GO



ALTER view [media].[vCustomImageSecure]
as
	select
		IM.*,
		LI.Link as 'LekmerImage.Link',
		LI.Parameter  as 'LekmerImage.Parameter',
		LI.TumbnailImageUrl  as 'LekmerImage.TumbnailImageUrl',
		LI.HasImage  as 'LekmerImage.HasImage'
	from
		[media].[vImageSecure] as IM
		left join [lekmer].[tLekmerImage] as LI on LI.[MediaId]=IM.[Image.MediaId]



GO
PRINT N'Altering [media].[vCustomImage]'
GO


ALTER view [media].[vCustomImage]
as
	select
		IM.*,
		LI.Link as 'LekmerImage.Link',
		LI.Parameter  as 'LekmerImage.Parameter',
		LI.TumbnailImageUrl  as 'LekmerImage.TumbnailImageUrl',
		LI.HasImage  as 'LekmerImage.HasImage'
	from
		[media].[vImage] as IM
		left join [lekmer].[tLekmerImage] as LI on LI.[MediaId]=IM.[Image.MediaId]
GO
PRINT N'Altering [product].[vCustomProductRecord]'
GO


sp_refreshview 'product.vProductRecord'
GO


ALTER VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations]
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductHeppo]'
GO

ALTER PROCEDURE [integration].[usp_ImportUpdateProductHeppo]

AS
BEGIN
		--raiserror('errorMSG', 16, 1)
		-- tCategory proc kallas
		print 'Start tCategory'
		exec [integration].[usp_UpdateTCategoryHeppo] -- skapar om artiklar om det behövs
		print 'End tCategory'
		print 'Start tUpdateBrand'
		exec [integration].usp_UpdateBrandHeppo
		print 'End tUpdateBrand'

		
		-- tProduct
		print 'Start update product'
		UPDATE 
			p
		SET 
			Title = tp.ArticleTitle, 
			--ItemsInPackage = null,
			EanCode = tp.EanCode, 
			--IsDeleted = 0, 
			--ColorId SAKNAS
			--SizeId SAKNAS
			NumberInStock = tp.NoInStock,
			-- Category kanske måste ändras om det skapar problem
			-- man ändrar den då till typ
			-- select catagoryId from tcategory where erpid = varukl+varugrupp+varukod 
			CategoryId = (select categoryId from product.tCategory where ErpId = 'C_'+tp.ArticleClassId+'-'+tp.ArticleGroupId+'-'+tp.ArticleCodeId)
			--CategoryId = tp.ArticleCodeId,	--varukodID det är den mest specifika!!
			--WebShopTitle = null,
			--[Description] = '',
			--ShortDescription = null,
			--MediaId = null, -- Måste sättas
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,11)	-- 5,17 innan
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join [product].tProductRegistryProduct pli
					on pli.ProductId = lp.ProductId and
					pli.ProductRegistryId = substring(tp.HYarticleId, 3,1)
		WHERE 
			p.Title <> tp.ArticleTitle OR
			p.EanCode <> tp.EanCode OR
			p.NumberInStock <> tp.NoInStock
		 
		print 'End product update'
		
		---------------------------------------------
		--print 'Start lekmer product update'
		--exec integration.usp_ImportUpdateLekmerProduct
		--print 'End lekmer product update'
	--------------------------------------		
		-- tPriceListItem - Update Prices from integration.tempProductPrice
		print 'Start price update'
		exec [integration].[usp_UpdateProductPRICE]
		print 'End price update'
	--------------------------------------
	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@HYarticleIdNoFokNoSize nvarchar (50),
				@HYarticleSize nvarchar(50),
				@EanCode nvarchar(50),
				@NoInStock nvarchar(250),
				@ArticleTitle nvarchar(250),
				@ProductId int,
				@HYBrandId nvarchar(25),
				
				@IsBookable bit,
				@ExpectedBackInStock datetime,
				@IsDeleted bit,
				@CategoryId int,
				@Description nvarchar(250),
				@Price decimal,
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10),
				
				@Data nvarchar(4000),
				@Vat decimal
				SET @Vat = 25
				SET @IsDeleted = 0
				SET @Description = ''
				SET @ExpectedBackInStock = null
				SET @IsBookable = 0
		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				------------------------------------
		
		declare cur_product cursor fast_forward for
			select
				tp.HYarticleId,
				tp.EanCode,
				tp.NoInStock,
				tp.ArticleTitle,
				(tp.Price/100),
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId,
				tp.BrandId
			from
				[integration].tempProduct tp
			where not exists(select 1
						-- parar lekmerErpId med temp tabell erpId (klipper fok) sen parar
						-- tProductregistry på lekmerProductId på det ErpId sen parar också foken med productRegProduct
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = substring(tp.HYarticleId, 5,11) and
							lp.ProductId = prp.ProductId and
							substring(tp.HYarticleId, 3,1) = prp.ProductRegistryId)
			
			
			
		--print 'Open Cursor'
		open cur_product
		fetch next from cur_product
			into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1) -- Fok här är nu 001 eller 002 osv beroende på vilket land artikeln tillhör
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)			
				SET @HYarticleSize = substring(@HYarticleId, 17,3)
				SET @HYarticleIdNoFokNoSize = substring(@HYarticleId, 5,11)
				
				-- if, kolla om eprid utan fok finns, om det gör hämta dens produktid och lägg in de produkt idet + fok
				-- i tproductRegistryProduct	
				
				if not exists(select 1 -- om erpid inte finns i tlekmerProduct finns det inte i nån fok, lägg in den då, annars lägg bara in i pReg
							from lekmer.tLekmerProduct lp
							where lp.HYErpId = @HYarticleIdNoFokNoSize)
				begin	
					print 'insert into product.tproduct'
					INSERT INTO -- MediaId saknas
						[product].tProduct(ErpId, EanCode, IsDeleted, NumberInStock, CategoryId, Title, [Description], ProductStatusId)
					SELECT 
						@HYarticleIdNoFokNoSize, -- @HYArticleId
						@EanCode, 
						@IsDeleted, --0
						@NoInStock, 
						(SELECT CategoryId FROM product.tCategory WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId), -- koll här
						@ArticleTitle, 
						@Description, --''
						0 --case when @NoInStock > 0 then 0 else 1 end (Ändrat alla produkter sätts som offline när de sätts in)
												
					set @ProductId = SCOPE_IDENTITY()
					
					-- Data that was sent into insert
					SET @Data = 'NEW: HYArticleId ' + @HYarticleId + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId as varchar(10))
		
					--IF @nrInStock > 0 -- om antal är mer än 0
					--begin
						--SET @IsBookable = 0
					--end
						
					print 'insert into lekmer.tlekmerProduct'		
					insert into lekmer.tLekmerProduct(ProductId, HYErpId, BrandId, IsBookable, AgeFromMonth, AgeToMonth, 
								IsNewFrom, IsNewTo, IsBatteryIncluded, ExpectedBackInStock, ShowVariantRelations)
					values(
					@ProductId, 
					@HYarticleIdNoFokNoSize,
					(Select b.BrandId from lekmer.tBrand b where b.ErpId = @HYBrandId),
					@IsBookable, 
					0, 
					0, 
					Convert(varchar, GETDATE(), 112),
					DATEADD(WK, 1, Convert(varchar, GETDATE(), 112)), 
					0, 
					@ExpectedBackInStock,
					0 --ShowVariantRelations
					)
					print 'insert into product.tPriceListItem'
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, @ProductId, @Price, (@Price * ((100-@Vat)/100)), @Vat)
					print 'insert into product.tProductReg'		
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct (ProductId, ProductRegistryId)
					values(@ProductId, @Fok) -- Fok är färsäljningskanalen
					
				
					--commit
				end	
				else if not exists (select 1
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = @HYarticleIdNoFokNoSize -- @HYarticleNoFok innan
							and lp.ProductId = prp.ProductId 
							and prp.ProductRegistryId = @Fok)
				begin
					select @ProductId = ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize --@HYarticleIdNoFoK innan
					set @Data = 'EXISTING: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					print 'insert into product.tproductRegProg (in else)'	
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct (ProductId, ProductRegistryId)
					values(@ProductId, @Fok)
					print 'insert into product.rPriceList (in else)'	
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize), --@HYarticleIdNoFoK innan),
					@Price, (@Price * ((100-@Vat)/100)), @Vat)
					
				end
				
				
				-- placeringen funkar inte, kommer bara gå in hit om produkten inte är
				-- i alla 4 foks...
				-- om den specifika sizen av produkten inte finns i tProductSize
				/*
				if not exists (select 1
						from lekmer.tProductSize ps
						where ps.ErpId = @HYarticleIdNoFoK)
				begin
				set @Data = 'NEW tProductSIZE: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						@ProductId,
						1, --(select SizeId from lekmer.tSize where ErpId = @HYarticleSize),
						@HYarticleIdNoFoK,
						@NoInStock
					
				end
			*/
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_product into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		end
		
		close cur_product
		deallocate cur_product
		

		-- hantera storlekarna på bilderna
		print '[integration].[usp_ImportUpdateProductSizesHeppo]  körs'
		exec [integration].[usp_ImportUpdateProductSizesHeppo]
				
END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductLekmer]'
GO

ALTER PROCEDURE [integration].[usp_ImportUpdateProductLekmer]
AS
BEGIN
	SET NOCOUNT ON 
	
	-- tCategory
	EXEC [integration].[usp_UpdateTCategoryLekmer] -- creates the items if necessary
	
	-- tBrand
	EXEC [integration].[usp_UpdateBrandLekmer]
	EXEC [integration].[usp_UpdateBrandLekmerCreationDate] -- adds the date the brand was created
	
	-- tProduct
	EXEC [integration].[usp_UpdateProductLekmer]
	EXEC [integration].[usp_ImportUpdateLekmerProduct]
	
	-- tPriceListItem
	EXEC [integration].[usp_UpdateProductPRICELekmer]
	
	-- tProductSize
	EXEC [integration].[usp_UpdateSizeStockLekmer]

	--------------------------------------

	DECLARE 
		@HYArticleNoFull NVARCHAR(50),
		@HYArticleNoColorSize NVARCHAR(50),
		@HYArticleNoColor NVARCHAR(50),
		@HYSizeId NVARCHAR(50),
		@EanCode NVARCHAR(50),
		@NoInStock NVARCHAR(250),
		@ArticleTitle NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@BrandId INT,
		@HYSizeValue NVARCHAR(50),
		
		@CategoryId INT,
		@Price DECIMAL,
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@LekmerErpId NVARCHAR(25),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL,
		
		@NewFromDate DATETIME,
		@NewToDate DATETIME,

		-- Variables for Channel
		@HYChannel NVARCHAR(40),
		
		@TradeDoublerProductGroup NVARCHAR(50),
		@ChannelId INT,
		@IsProductRegistry BIT,
		@NeedToInsertProductRelationData BIT
	
	SET @Vat = 25.0
	
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 4, @NewFromDate)

	------------------------------------

	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			tp.HYarticleId,
			tp.ArticleTitle,
			tp.SizeId,
			(tp.Price/100),
			tp.NoInStock,
			tp.ArticleGroupId,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.BrandId,
			tp.EanCode,
			tp.LekmerArtNo
		FROM
			[integration].tempProduct tp
		WHERE NOT EXISTS (SELECT 1
						  FROM lekmer.tLekmerProduct lp, product.tPriceListItem prp
						  WHERE lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
							AND lp.ProductId = prp.ProductId
							AND SUBSTRING(tp.HYarticleId, 3,1) = prp.PriceListId)

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@HYArticleNoFull,
			@ArticleTitle,
			@HYSizeValue,
			@Price,
			@NoInStock,
			@VarugruppId,
			@VaruklassId,
			@VarukodId,
			@HYBrandId,
			@EanCode,
			@LekmerErpId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			SET @IsProductRegistry = 0
			SET @NeedToInsertProductRelationData = 0
			
			-- Split @HYArticleNoFull
			-- 001-0000001-1017-108
			SET @HYChannel = SUBSTRING(@HYArticleNoFull, 1, 3)
			SET @HYArticleNoColorSize = SUBSTRING(@HYArticleNoFull, 5, 17)
			SET @HYSizeId = SUBSTRING(@HYArticleNoFull, 18, 3)
			SET @HYArticleNoColor = SUBSTRING(@HYArticleNoFull, 5, 12)
		
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYArticleNoColor)
			IF @ProductId IS NOT NULL
			BEGIN
				IF EXISTS (SELECT 1 FROM product.tProductRegistryProduct
						   WHERE ProductId = @ProductId
								 AND ProductRegistryId = @HYChannel)
				BEGIN
					SET @IsProductRegistry = 1
					GOTO PRODUCT_END
				END
			END
			
			PRODUCT_START:
			
			---- Skip processing the same @HYErpId + @Channel few times
			--IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			--BEGIN
			--	GOTO PRODUCT_END
			--END ELSE BEGIN
			--	SET	@PrevHYErpId = @HYErpId
			--	SET	@PrevChannel = @Channel
			--END
			
			BEGIN TRANSACTION
			
			SET @CategoryId = (SELECT CategoryId 
							   FROM product.tCategory 
							   WHERE ErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, ''))
			
			SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)

			IF @ProductId IS NULL
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNoFull + ' EanCode ' + @EanCode
				
				-- tProduct
				INSERT INTO [product].tProduct (
					ErpId,
					EanCode,
					IsDeleted,
					NumberInStock,
					CategoryId,
					Title,
					[Description],
					ProductStatusId
				)
				VALUES (
					@HYArticleNoColor,
					@EanCode,
					0, -- IsDeleted
					@NoInStock,
					@CategoryId,
					@ArticleTitle, 
					'', -- Description
					1 -- Offline
				)
											
				SET @ProductId = SCOPE_IDENTITY()
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNoFull + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
	
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId,
					HYErpId,
					BrandId,
					IsBookable,
					AgeFromMonth,
					AgeToMonth,
					IsNewFrom,
					IsNewTo,
					IsBatteryIncluded,
					ExpectedBackInStock,
					LekmerErpId,
					ShowVariantRelations
				)
				VALUES (
					@ProductId, 
					@HYArticleNoColor,
					@BrandId,
					0, -- IsBookable, 
					0, -- AgeFromMonth
					0, -- AgeToMonth
					@NewFromDate,
					@NewToDate,
					0, -- IsBatteryIncluded
					NULL, --ExpectedBackInStock
					@LekmerErpId,
					0 -- ShowVariantRelations
				)

			END	 -- Product already exist in tLekmerProduct
			ELSE IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem prp WHERE prp.ProductId = @ProductId AND prp.PriceListId = @HYChannel)
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'EXISTING: @HYArticleNoColorSize ' + @HYArticleNoColorSize + ' @HYChannel ' + @HYChannel + ' ProductId' + CAST(@ProductId as varchar(10))
			END
			
			IF @NeedToInsertProductRelationData = 1
			BEGIN
				------------------------------------------------------------------------------------------
				-- tProductRegistryProduct
				
				IF NOT EXISTS (SELECT 1 FROM product.tProductRegistryProduct
							   WHERE ProductId = @ProductId
									 AND ProductRegistryId = @HYChannel)
				BEGIN
					INSERT INTO product.tProductRegistryProduct (
						ProductId,
						ProductRegistryId
					)
					VALUES (
						@ProductId,
						@HYChannel
					)
				END				
				
				------------------------------------------------------------------------------------------
				-- tPriceListItem
				-- PriceListId and @HYChannel have same values

				IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem
							   WHERE ProductId = @ProductId
									 AND PriceListId = @HYChannel)
				BEGIN
					INSERT INTO product.tPriceListItem (
						PriceListId,
						ProductId,
						PriceIncludingVat,
						PriceExcludingVat,
						VatPercentage
					)
					VALUES (
						@HYChannel, 
						@ProductId, 
						@Price, 
						@Price / (1.0+@Vat/100.0),
						@Vat
					)
				END

				------------------------------------------------------------------------------------------ 
				-- tTradeDoublerProductGroupMapping

				SET @ChannelId = CAST(@HYChannel AS INT)
				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '')
													   AND ChannelId = @ChannelId)
				
				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId 
										 AND tTDPGM.ChannelId = @ChannelId)
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							@ChannelId
						)
					END
				END 
			END

			COMMIT

			PRODUCT_END:
			
			------------------------------------------------------------------------------------------ 
			-- ProductSize
				
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [integration].[usp_ImportUpdateProductSizesLekmer]
				@HYArticleNoFull,
				@NoInStock,
				@HYSizeValue,
				@ProductId,
				@HYChannel,
				@HYArticleNoColorSize
			
		END TRY
		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
				
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)

		END CATCH

		FETCH NEXT FROM cur_product 
			INTO 
				@HYArticleNoFull,
				@ArticleTitle,
				@HYSizeValue,
				@Price,
				@NoInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@HYBrandId,
				@EanCode,
				@LekmerErpId

	END
	
	CLOSE cur_product
	DEALLOCATE cur_product
	
	EXEC [integration].[usp_UpdateProductBrandLekmer]
	--exec [integration].[usp_FokRestrictionsProducts] -- Outdated, soon to be removed, [usp_ChannelRestrictionsProducts] in its place
	EXEC [integration].[usp_ChannelRestrictionsProducts]
	EXEC [integration].[usp_REATagProduct]
	
	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]
END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO

sp_refreshview  'product.vProductSecure'
GO

ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		

GO
PRINT N'Altering [core].[vCustomChannel]'
GO


ALTER VIEW [core].[vCustomChannel]
AS
	SELECT
		vC.*,
		vLC.[Channel.TimeFormat],
		vLC.[Channel.WeekDayFormat],
		vLC.[Channel.DayFormat],
		vLC.[Channel.DateTimeFormat],
		vLC.[Channel.TimeZoneDiff]
	FROM
		[core].[vChannel] AS vC
		LEFT OUTER JOIN [lekmer].[vLekmerChannel] vLC ON vLC.[Channel.Id] = vC.[Channel.Id]


GO
PRINT N'Altering [product].[vCustomProductView]'
GO


sp_refreshview 'product.vProductView'
GO


ALTER VIEW [product].[vCustomProductView]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]




GO
PRINT N'Altering [product].[vCustomProduct]'
GO



ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]


GO
PRINT N'Creating primary key [PK_tTradeDoublerProductGroupMapping] on [lekmer].[tTradeDoublerProductGroupMapping]'
GO
ALTER TABLE [lekmer].[tTradeDoublerProductGroupMapping] ADD CONSTRAINT [PK_tTradeDoublerProductGroupMapping] PRIMARY KEY CLUSTERED  ([ProductId], [ChannelId])
GO
PRINT N'Creating index [IX_tBrand_ErpId] on [lekmer].[tBrand]'
GO
CREATE NONCLUSTERED INDEX [IX_tBrand_ErpId] ON [lekmer].[tBrand] ([ErpId])
GO
PRINT N'Creating index [IX_tImageRotatorGroup_BlockId_Ordinal] on [lekmer].[tImageRotatorGroup]'
GO
CREATE NONCLUSTERED INDEX [IX_tImageRotatorGroup_BlockId_Ordinal] ON [lekmer].[tImageRotatorGroup] ([BlockId], [Ordinal])
GO
PRINT N'Creating index [IX_tProductSize_ProductId] on [lekmer].[tProductSize]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductSize_ProductId] ON [lekmer].[tProductSize] ([ProductId])
GO
PRINT N'Creating index [IX_tWishList_WishListKey] on [lekmer].[tWishList]'
GO
CREATE NONCLUSTERED INDEX [IX_tWishList_WishListKey] ON [lekmer].[tWishList] ([WishListKey])
GO
PRINT N'Creating index [IX_tPriceListItem_ProductId] on [product].[tPriceListItem]'
GO
CREATE NONCLUSTERED INDEX [IX_tPriceListItem_ProductId] ON [product].[tPriceListItem] ([ProductId]) INCLUDE ([PriceIncludingVat], [PriceListId])
GO
PRINT N'Creating index [IX_tProductRegistryProduct_ProductId_(ProductRegistryId)] on [product].[tProductRegistryProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct_ProductId_(ProductRegistryId)] ON [product].[tProductRegistryProduct] ([ProductId]) INCLUDE ([ProductRegistryId])
GO
PRINT N'Creating index [IX_tProductRegistryProduct_ProductRegistryId] on [product].[tProductRegistryProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct_ProductRegistryId] ON [product].[tProductRegistryProduct] ([ProductRegistryId])
GO
PRINT N'Creating index [IX_tProductRegistryProduct_ProductRegistryId_ProductId] on [product].[tProductRegistryProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct_ProductRegistryId_ProductId] ON [product].[tProductRegistryProduct] ([ProductRegistryId], [ProductId])
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD
CONSTRAINT [FK_tLekmerProduct_tSizeDeviation] FOREIGN KEY ([SizeDeviationId]) REFERENCES [lekmer].[tSizeDeviation] ([SizeDeviationId])
GO


--ROLLBACK
--RAISERROR('Remove ROLLBACK first!', 18, 0)
COMMIT
--/////////////////////////////////////