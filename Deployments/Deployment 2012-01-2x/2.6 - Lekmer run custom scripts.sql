USE Lekmer_live
GO

PRINT 'Refresh views 1'
GO

sp_refreshview 'product.vProductImage'
GO

sp_refreshview 'product.vRelationListProduct'
GO

sp_refreshview 'product.vProductRelationList'
GO

PRINT 'Refresh views ALL'
GO

EXEC dev.pRefreshAllViews
GO

PRINT 'Create variants'
GO

EXEC integration.pCreateProductRelationVariant
GO