SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
USE [Heppo_live]
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

--/////////////////////////////////////
BEGIN TRANSACTION

-- Transfer old urls from tLekmerProduct into tProductUrlHistory
INSERT INTO lekmer.tProductUrlHistory (
	ProductId,
	LanguageId,
	UrlTitleOld,
	HistoryLifeIntervalTypeId
)
SELECT
	LP.ProductId,
	Ch.LanguageId,
	SUBSTRING(LP.OldUrl, CHARINDEX('/', LP.OldUrl) + 1, LEN(LP.OldUrl)),
	1
FROM
	lekmer.tLekmerProduct LP
	INNER JOIN product.tProductRegistryProduct AS PRP ON LP.ProductId = PRP.ProductId
	INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
	INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
WHERE
	LP.OldUrl IS NOT NULL


--ROLLBACK
--RAISERROR('Remove ROLLBACK first!', 18, 0)
COMMIT
--/////////////////////////////////////