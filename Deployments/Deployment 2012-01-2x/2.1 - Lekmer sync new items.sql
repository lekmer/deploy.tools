SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
USE [Lekmer_live]
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

--/////////////////////////////////////
BEGIN TRANSACTION

PRINT N'Creating [lekmer].[pProductGetIdAllByBlockAndProduct]'
GO
CREATE PROCEDURE [lekmer].[pProductGetIdAllByBlockAndProduct]
		@ChannelId		INT,
		@CustomerId		INT,
		@BlockId		INT,
		@ProductId		INT,
		@ShowVariants	BIT,
		@Page			INT = NULL,
		@PageSize		INT,
		@SortBy			VARCHAR(50) = NULL,
		@SortDescending BIT = NULL
AS
BEGIN
	DECLARE @RelationListVariantTypeId INT
	SET @RelationListVariantTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'Variant')
		
	DECLARE @maxCount CHAR(4)
	SET @maxCount = '2000'

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @CustomerIdString VARCHAR(10)
	
	IF (@CustomerId IS NULL)
		SET @CustomerIdString = 'null'
	ELSE
		SET @CustomerIdString = CAST(@CustomerId AS VARCHAR(10))
		
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sql = '
	declare @tResult table (ProductId int, SortBy nvarchar(max))	
	insert @tResult	(ProductId, SortBy)
	select distinct TOP ' + @maxCount + ' p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	from
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN [product].[tBlockProductRelationListItem] bprli ON rl.RelationListTypeId = bprli.RelationListTypeId
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				' + @CustomerIdString + '
			)
	WHERE
		prl.ProductId = ' + cast(@ProductId as varchar) + '
		AND rlp.ProductId <> ' + cast(@ProductId as varchar) + '
		' + CASE WHEN (@ShowVariants = 0) 
				THEN ' AND rl.RelationListTypeId <> ' + cast(@RelationListVariantTypeId as varchar) + ' '
				ELSE ' '
			END + '
		AND p.[Product.ChannelId] = ' + cast(@ChannelId as varchar) + '
		AND bprli.BlockId = ' + cast(@BlockId as varchar) + '

	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + CAST(@Page * @PageSize AS varchar(10)) + ' *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
		AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + @maxCount + ' ProductId FROM @tResult AS SearchResult'
	END
	
	PRINT @sql
			
	EXEC (@sql)
END

GO
PRINT N'Creating [lekmer].[pOrderSearch]'
GO
CREATE PROCEDURE [lekmer].[pOrderSearch]
@OrderId		INT,
@Page           INT = NULL,
@PageSize       INT,
@SortBy			VARCHAR(20) = NULL,
@SortAcsending	BIT = 0

AS
BEGIN

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN;
	END
		
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

    SET @sqlFragment = 'SELECT ROW_NUMBER() OVER (ORDER BY ' 
		+ COALESCE(@SortBy, 'vO.[Order.CreatedDate]')
		+ CASE WHEN (@SortAcsending = 0) THEN ' DESC' ELSE ' ASC' END + ') AS _Number,
		vO.*
		FROM [order].[vCustomOrder] vO 
		WHERE vO.[OrderStatus.CommonName] <> ''RejectedByPaymentProvider'''
			+ CASE WHEN (@OrderId IS NOT NULL) THEN + '
			AND vO.[Order.OrderId] LIKE ''%'' + CAST(@OrderId AS VARCHAR(10)) + ''%'' ' ELSE '' END 
			
	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE _Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND _Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@OrderId		INT',
			@OrderId

	EXEC sp_executesql @sql, 
		N'	@OrderId		INT',
			@OrderId
END
GO
PRINT N'Creating [integration].[tProductsTranslatedInHY]'
GO
CREATE TABLE [integration].[tProductsTranslatedInHY]
(
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK__tProduct__1F9F439771BF3B7D] on [integration].[tProductsTranslatedInHY]'
GO
ALTER TABLE [integration].[tProductsTranslatedInHY] ADD CONSTRAINT [PK__tProduct__1F9F439771BF3B7D] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId])
GO
PRINT N'Creating [integration].[usp_UpdateSizeStockLekmer]'
GO

CREATE PROCEDURE [integration].[usp_UpdateSizeStockLekmer]
AS
BEGIN

	UPDATE 
		ps
	SET 
		ps.NumberInStock = tp.NoInStock
	FROM
		integration.tempProduct tp
		INNER JOIN lekmer.tProductSize ps ON ps.ErpId = SUBSTRING(tp.HYarticleId, 5, 17)
	WHERE 
		ps.NumberInStock <> tp.NoInStock
		AND SUBSTRING(tp.HYarticleId, 3,1) = 1

END
GO
PRINT N'Creating [lekmer].[tLekmerChannel]'
GO
CREATE TABLE [lekmer].[tLekmerChannel]
(
[ChannelId] [int] NOT NULL,
[TimeFormat] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[WeekDayFormat] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DayFormat] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DateTimeFormat] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TimeZoneDiff] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_tLekmerChannel] on [lekmer].[tLekmerChannel]'
GO
ALTER TABLE [lekmer].[tLekmerChannel] ADD CONSTRAINT [PK_tLekmerChannel] PRIMARY KEY CLUSTERED  ([ChannelId])
GO
PRINT N'Creating [lekmer].[pLekmerChannelSave]'
GO

CREATE PROCEDURE [lekmer].[pLekmerChannelSave]
	@ChannelId				INT,
	@TimeFormat				NVARCHAR(50),
	@WeekDayFormat			NVARCHAR(50),
	@DayFormat				NVARCHAR(50),
	@DateTimeFormat			NVARCHAR(50),
	@TimeZoneDiff			INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tLekmerChannel]
	SET
		[TimeFormat] = @TimeFormat,
		[WeekDayFormat] = @WeekDayFormat,
		[DayFormat] = @DayFormat,
		[DateTimeFormat] = @DateTimeFormat,
		[TimeZoneDiff] = @TimeZoneDiff
	WHERE
		[ChannelId] = @ChannelId
		
	IF @@ROWCOUNT = 0
	
	INSERT INTO [lekmer].[tLekmerChannel] (
		[ChannelId],
		[TimeFormat],
		[WeekDayFormat],
		[DayFormat],
		[DateTimeFormat],
		[TimeZoneDiff])
	VALUES (
		@ChannelId,
		@TimeFormat,
		@WeekDayFormat,
		@DayFormat,
		@DateTimeFormat,
		@TimeZoneDiff
	)
		
	RETURN @ChannelId
END
GO
PRINT N'Creating [lekmer].[pTagGetAll]'
GO

CREATE PROCEDURE [lekmer].[pTagGetAll]
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		*
	FROM 
		lekmer.[vTagSecure]
END 

GO
PRINT N'Creating [lekmer].[tHistoryLifeIntervalType]'
GO
CREATE TABLE [lekmer].[tHistoryLifeIntervalType]
(
[HistoryLifeIntervalTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DaysActive] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_HistoryLifeIntervalType] on [lekmer].[tHistoryLifeIntervalType]'
GO
ALTER TABLE [lekmer].[tHistoryLifeIntervalType] ADD CONSTRAINT [PK_HistoryLifeIntervalType] PRIMARY KEY CLUSTERED  ([HistoryLifeIntervalTypeId])
GO
PRINT N'Creating [lekmer].[tProductSimilar]'
GO
CREATE TABLE [lekmer].[tProductSimilar]
(
[ProductId] [int] NOT NULL,
[SimilarProductId] [int] NOT NULL,
[Score] [decimal] (18, 6) NOT NULL
)
GO
PRINT N'Creating primary key [PK_tProductSimilar] on [lekmer].[tProductSimilar]'
GO
ALTER TABLE [lekmer].[tProductSimilar] ADD CONSTRAINT [PK_tProductSimilar] PRIMARY KEY CLUSTERED  ([ProductId], [SimilarProductId])
GO
PRINT N'Creating [lekmer].[vProductSimilar]'
GO


CREATE VIEW [lekmer].[vProductSimilar]
AS
SELECT     
	ps.ProductId 'ProductSimilar.ProductId',
	ps.SimilarProductId 'ProductSimilar.SimilarProductId',
	ps.Score 'ProductSimilar.Score'
FROM
	[lekmer].[tProductSimilar] ps
GO
PRINT N'Creating [lekmer].[vCustomProductSimilar]'
GO


CREATE VIEW [lekmer].[vCustomProductSimilar]
AS
	SELECT
		*
	FROM
		[lekmer].[vProductSimilar]
GO
PRINT N'Creating [lekmer].[pProductSimilarDelete]'
GO
CREATE PROCEDURE [lekmer].[pProductSimilarDelete]
	@ProductId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tProductSimilar]
	WHERE 
		[ProductId] = @ProductId
END 
GO
PRINT N'Creating [lekmer].[pProductSimilarGetAllByProductId]'
GO
CREATE PROCEDURE [lekmer].[pProductSimilarGetAllByProductId]
	@ProductId	INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vCustomProductSimilar]
	WHERE
		[ProductSimilar.ProductId] = @ProductId
	ORDER BY [ProductSimilar.Score] ASC
END 
GO
PRINT N'Creating [lekmer].[fnConvertIDListToTableWithScore]'
GO
CREATE FUNCTION [lekmer].[fnConvertIDListToTableWithScore](
	@IDList VARCHAR(MAX),
	@ScoreList VARCHAR(MAX),
	@Delimiter CHAR(1) = ';'
)
RETURNS @tbl TABLE(ErpId VARCHAR(50), Score DECIMAL(18, 6))
AS
BEGIN
	IF @IDList IS NULL OR @IDList = '' OR @ScoreList IS NULL OR @ScoreList = ''
		RETURN

	IF SUBSTRING(@IDList, LEN(@IDList), 1) <> @Delimiter
		SELECT @IDList = @IDList + @Delimiter
		
	IF SUBSTRING(@ScoreList, LEN(@ScoreList), 1) <> @Delimiter
		SELECT @ScoreList = @ScoreList + @Delimiter

	DECLARE @indBegin1 INT, @ind1 INT, @indBegin2 INT, @ind2 INT
	
	SET @indBegin1 = 1
	SET @indBegin2 = 1

	WHILE 1 = 1
	BEGIN
		SELECT @ind1 = CHARINDEX(@Delimiter, @IDList, @indBegin1)
		SELECT @ind2 = CHARINDEX(@Delimiter, @ScoreList, @indBegin2)

		IF @ind1 = 0 OR @ind2 = 0
			BREAK
		
		INSERT
			@tbl
		VALUES
		(
			CAST(SUBSTRING(@IDList, @indBegin1, @ind1 - @indBegin1) AS VARCHAR),
			CAST(REPLACE(SUBSTRING(@ScoreList, @indBegin2, @ind2 - @indBegin2), ',', '.') AS DECIMAL(18, 6))
		)

		SET @indBegin1 = @ind1 + 1
		SET @indBegin2 = @ind2 + 1
	END

	RETURN
END
GO
PRINT N'Creating [lekmer].[tBlockProductSimilarList]'
GO
CREATE TABLE [lekmer].[tBlockProductSimilarList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[TotalProductCount] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_tBlockProductSimilarList] on [lekmer].[tBlockProductSimilarList]'
GO
ALTER TABLE [lekmer].[tBlockProductSimilarList] ADD CONSTRAINT [PK_tBlockProductSimilarList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
PRINT N'Creating [lekmer].[pBlockProductSimilarListSave]'
GO
CREATE PROCEDURE [lekmer].[pBlockProductSimilarListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@TotalProductCount INT
AS
BEGIN
	UPDATE lekmer.tBlockProductSimilarList
	SET
		ColumnCount = @ColumnCount,
		[RowCount] = @RowCount,
		TotalProductCount = @TotalProductCount
	WHERE
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT lekmer.tBlockProductSimilarList (
		BlockId,
		ColumnCount,
		[RowCount],
		TotalProductCount
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@TotalProductCount
	)
END
GO
PRINT N'Creating [lekmer].[pBlockProductSimilarListDelete]'
GO
CREATE PROCEDURE [lekmer].[pBlockProductSimilarListDelete]
	@BlockId INT
AS
BEGIN
	DELETE lekmer.tBlockProductSimilarList
	WHERE BlockId = @BlockId
END
GO
PRINT N'Creating [lekmer].[vBlockProductSimilarList]'
GO

CREATE VIEW [lekmer].[vBlockProductSimilarList]
AS
	SELECT
		[BlockId] as 'BlockProductSimilarList.BlockId',
		[ColumnCount] as 'BlockProductSimilarList.ColumnCount',
		[RowCount] as 'BlockProductSimilarList.RowCount',
		[TotalProductCount] as 'BlockProductSimilarList.TotalProductCount'
	FROM
		lekmer.tBlockProductSimilarList
GO
PRINT N'Creating [lekmer].[vCustomBlockProductSimilarList]'
GO

CREATE VIEW [lekmer].[vCustomBlockProductSimilarList]
AS
	SELECT
		*
	FROM
		[lekmer].[vBlockProductSimilarList]
GO
PRINT N'Creating [integration].[pCreateProductRelationVariant]'
GO
CREATE PROCEDURE [integration].[pCreateProductRelationVariant]
AS
BEGIN
	SET NOCOUNT ON 
	
	DECLARE @RelationListTypeId INT
	SELECT @RelationListTypeId = RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'Variant'

	DECLARE @VariantsCount INT

	DECLARE @RelationListId INT
	DECLARE @VariantErpId VARCHAR(50)

	DECLARE @ProductId INT
	DECLARE @ProductErpId VARCHAR(50)

	-- Delete all Product - Relation List where Relation List Type = 'Variant'
	DELETE prl FROM product.tProductRelationList prl
	INNER JOIN product.tRelationList rl ON prl.RelationListId = rl.RelationListId
	WHERE rl.RelationListTypeId = @RelationListTypeId

	-- Delete all Relation List - Product where Relation List Type = 'Variant'
	DELETE rlp FROM product.tRelationListProduct rlp
	INNER JOIN product.tRelationList rl ON rlp.RelationListId = rl.RelationListId
	WHERE rl.RelationListTypeId = @RelationListTypeId

	-- Delete all Relation List where Relation List Type = 'Variant'
	DELETE FROM product.tRelationList WHERE RelationListTypeId = @RelationListTypeId

	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			lp.ProductId,
			lp.HYErpId,
			SUBSTRING(lp.HYErpId, 1, CHARINDEX('-', lp.HYErpId) - 1)
		FROM
			lekmer.tLekmerProduct lp
		ORDER BY lp.HYErpId

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@ProductId,
			@ProductErpId,
			@VariantErpId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			-- Check if product has variants			
			SET @VariantsCount = (SELECT COUNT(ProductId) FROM lekmer.tLekmerProduct WHERE HYErpId LIKE @VariantErpId + '%')
			IF @VariantsCount > 1
			BEGIN
				IF NOT EXISTS ( SELECT *
							FROM product.tProductRelationList prl
							INNER JOIN product.tRelationList rl ON prl.RelationListId = rl.RelationListId
							WHERE prl.ProductId = @ProductId
								  AND rl.RelationListTypeId = @RelationListTypeId
								  AND rl.Title = @VariantErpId )
				BEGIN
					BEGIN TRANSACTION
						SET @RelationListId = ( SELECT RelationListId 
												FROM product.tRelationList rl
												WHERE rl.Title = @VariantErpId
													  AND rl.RelationListTypeId = @RelationListTypeId )

						IF @RelationListId IS NULL
						BEGIN
							EXEC @RelationListId = [product].[pRelationListSave] NULL, @VariantErpId, @RelationListTypeId
						END
						
						EXEC [product].[pProductRelationListSave] @ProductId, @RelationListId
						
						EXEC [product].[pRelationListProductSave] @RelationListId, @ProductId
					COMMIT
				END
			END
		END TRY
		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				NULL,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
		END CATCH

		FETCH NEXT FROM cur_product 
			INTO 
				@ProductId,
				@ProductErpId,
				@VariantErpId
	END

	CLOSE cur_product
	DEALLOCATE cur_product
END
GO
PRINT N'Creating [lekmer].[vLekmerChannel]'
GO



CREATE VIEW [lekmer].[vLekmerChannel]
AS
SELECT
	[ChannelId]		AS [Channel.Id],
	[TimeFormat]	AS [Channel.TimeFormat],
	[WeekDayFormat]	AS [Channel.WeekDayFormat],
	[DayFormat]		AS [Channel.DayFormat],
	[DateTimeFormat]AS [Channel.DateTimeFormat],
	[TimeZoneDiff]	AS [Channel.TimeZoneDiff]
FROM
	[lekmer].[tLekmerChannel]


GO
PRINT N'Creating [integration].[pTranslateProductTitlesInHY]'
GO

CREATE PROCEDURE [integration].[pTranslateProductTitlesInHY]
	@ISO	nvarchar(10)
AS
begin
	set nocount on				
		
		declare @channel int
		set @channel = (
							select ChannelId from core.tChannel c
							inner join core.tLanguage l
								on c.LanguageId = l.LanguageId
							where l.ISO = @ISO)
		select 
			l.HYErpId, 
			t.ProductId, 
			t.LanguageId, 
			t.Title,
			@channel 
		from lekmer.tLekmerProduct l
				inner join product.tProductTranslation t
					on l.ProductId = t.ProductId 
		where 
			t.LanguageId = (select LanguageId from core.tLanguage where ISO = @ISO) 
			and t.Title Is not NULL
			and t.Title != ''
			and not exists 
							(
								select 1 
								from integration.[tProductsTranslatedInHY] i
								where i.ProductId = t.ProductId
								and i.languageId = t.LanguageId					
							)		 
end
GO
PRINT N'Creating [integration].[usp_UpdateProductLekmer]'
GO

CREATE PROCEDURE [integration].[usp_UpdateProductLekmer]
AS
BEGIN
	SET NOCOUNT ON 
	
	UPDATE 
		p
	SET 
		EanCode = tp.EanCode, 
		NumberInStock = tp.NoInStock,
		CategoryId = (SELECT categoryId 
					  FROM product.tCategory 
					  WHERE ErpId = 'C_' + ISNULL(tp.ArticleClassId, '') + '-' + ISNULL(tp.ArticleGroupId, '') + '-' + ISNULL(tp.ArticleCodeId, ''))
	FROM 
		[integration].tempProduct tp
		INNER JOIN [lekmer].tLekmerProduct lp ON lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
		INNER JOIN product.tProduct p ON p.ProductId = lp.ProductId
		INNER JOIN [product].tPriceListItem pli ON pli.ProductId = lp.ProductId AND pli.PriceListId = 1
	WHERE 
		SUBSTRING(tp.HYarticleId, 3,1) = 1 
		AND (
			p.EanCode <> tp.EanCode OR
			p.NumberInStock <> tp.NoInStock
		)
END
GO
PRINT N'Creating [lekmer].[tProductUrlHistory]'
GO
CREATE TABLE [lekmer].[tProductUrlHistory]
(
[ProductUrlHistoryId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[UrlTitleOld] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_tProductUrlHistory_CreationDate] DEFAULT (getdate()),
[LastUsageDate] [datetime] NOT NULL CONSTRAINT [DF_tProductUrlHistory_LastUsageDate] DEFAULT (getdate()),
[UsageAmount] [int] NOT NULL CONSTRAINT [DF_tProductUrlHistory_UsageAmount] DEFAULT ((0)),
[HistoryLifeIntervalTypeId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_tProductUrlHistory] on [lekmer].[tProductUrlHistory]'
GO
ALTER TABLE [lekmer].[tProductUrlHistory] ADD CONSTRAINT [PK_tProductUrlHistory] PRIMARY KEY CLUSTERED  ([ProductUrlHistoryId])
GO
PRINT N'Creating index [IX_tProductUrlHistory_ProductId_LanguageId_UrlTitleOld] on [lekmer].[tProductUrlHistory]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tProductUrlHistory_ProductId_LanguageId_UrlTitleOld] ON [lekmer].[tProductUrlHistory] ([ProductId], [LanguageId], [UrlTitleOld])
GO
PRINT N'Creating index [IX_tProductUrlHistory_UrlTitleOld] on [lekmer].[tProductUrlHistory]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductUrlHistory_UrlTitleOld] ON [lekmer].[tProductUrlHistory] ([UrlTitleOld])
GO
PRINT N'Creating [lekmer].[pProductUrlHistorySave]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistorySave]
	@ProductId					INT,
	@LanguageId					INT,
	@UrlTitleOld				NVARCHAR(256),
	@HistoryLifeIntervalTypeId	INT
AS 
BEGIN 
	INSERT INTO lekmer.tProductUrlHistory (
		ProductId,
		LanguageId,
		UrlTitleOld,
		HistoryLifeIntervalTypeId
	)
	VALUES (
		@ProductId,
		@LanguageId,
		@UrlTitleOld,
		@HistoryLifeIntervalTypeId
	)
	
	RETURN @@IDENTITY
END 
GO
PRINT N'Creating [lekmer].[tContentPageUrlHistory]'
GO
CREATE TABLE [lekmer].[tContentPageUrlHistory]
(
[ContentPageUrlHistoryId] [int] NOT NULL IDENTITY(1, 1),
[ContentPageId] [int] NOT NULL,
[UrlTitleOld] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_tContentPageUrlHistory_CreationDate] DEFAULT (getdate()),
[LastUsageDate] [datetime] NOT NULL CONSTRAINT [DF_tContentPageUrlHistory_LastUsageDate] DEFAULT (getdate()),
[UsageAmount] [int] NOT NULL CONSTRAINT [DF_tContentPageUrlHistory_UsageAmount] DEFAULT ((0)),
[HistoryLifeIntervalTypeId] [int] NOT NULL
)
GO
PRINT N'Creating primary key [PK_tContentPageUrlHistory] on [lekmer].[tContentPageUrlHistory]'
GO
ALTER TABLE [lekmer].[tContentPageUrlHistory] ADD CONSTRAINT [PK_tContentPageUrlHistory] PRIMARY KEY CLUSTERED  ([ContentPageUrlHistoryId])
GO
PRINT N'Creating index [IX_tContentPageUrlHistory_ContentPageId_UrlTitleOld] on [lekmer].[tContentPageUrlHistory]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tContentPageUrlHistory_ContentPageId_UrlTitleOld] ON [lekmer].[tContentPageUrlHistory] ([ContentPageId], [UrlTitleOld])
GO
PRINT N'Creating index [IX_tContentPageUrlHistory_UrlTitleOld] on [lekmer].[tContentPageUrlHistory]'
GO
CREATE NONCLUSTERED INDEX [IX_tContentPageUrlHistory_UrlTitleOld] ON [lekmer].[tContentPageUrlHistory] ([UrlTitleOld])
GO
PRINT N'Creating [lekmer].[pContentPageUrlHistorySave]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistorySave]
	@ContentPageId				INT,
	@UrlTitleOld				NVARCHAR(256),
	@HistoryLifeIntervalTypeId	INT
AS 
BEGIN 
	INSERT INTO lekmer.tContentPageUrlHistory (
		ContentPageId,
		UrlTitleOld,
		HistoryLifeIntervalTypeId
	)
	VALUES (
		@ContentPageId,
		@UrlTitleOld,
		@HistoryLifeIntervalTypeId
	)
	
	RETURN @@IDENTITY
END
GO
PRINT N'Creating [lekmer].[pContentPageUrlHistoryUpdate]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryUpdate]
	@ContentPageUrlHistoryId	INT
AS 
BEGIN 
	UPDATE 
		lekmer.tContentPageUrlHistory
	SET 
		UsageAmount = UsageAmount + 1,
		LastUsageDate = GETDATE()
	WHERE
		ContentPageUrlHistoryId = @ContentPageUrlHistoryId
END 
GO
PRINT N'Creating [lekmer].[pProductUrlHistoryUpdate]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryUpdate]
	@ProductUrlHistoryId	INT
AS 
BEGIN 
	UPDATE 
		lekmer.tProductUrlHistory
	SET 
		UsageAmount = UsageAmount + 1,
		LastUsageDate = GETDATE()
	WHERE
		ProductUrlHistoryId = @ProductUrlHistoryId
END 
GO
PRINT N'Creating [lekmer].[pContentPageUrlHistoryDeleteById]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryDeleteById]
	@ContentPageUrlHistoryId INT
AS
BEGIN
	DELETE [lekmer].[tContentPageUrlHistory]
	WHERE ContentPageUrlHistoryId = @ContentPageUrlHistoryId
END
GO
PRINT N'Creating [lekmer].[pProductUrlHistoryDeleteById]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryDeleteById]
	@ProductUrlHistoryId INT
AS
BEGIN
	DELETE [lekmer].[tProductUrlHistory]
	WHERE ProductUrlHistoryId = @ProductUrlHistoryId
END
GO
PRINT N'Creating [lekmer].[pProductUrlHistoryDeleteByProductId]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryDeleteByProductId]
	@ProductId INT
AS
BEGIN
	DELETE [lekmer].[tProductUrlHistory]
	WHERE ProductId = @ProductId
END
GO
PRINT N'Creating [lekmer].[pContentPageUrlHistoryDeleteByContentPageId]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryDeleteByContentPageId]
	@ContentPageId INT
AS
BEGIN
	DELETE [lekmer].[tContentPageUrlHistory]
	WHERE ContentPageId = @ContentPageId
END
GO
PRINT N'Creating [lekmer].[pProductUrlHistoryDeleteByProductIdAndUrlTitle]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryDeleteByProductIdAndUrlTitle]
	@ProductId		INT,
	@LanguageId		INT,
	@UrlTitleOld	NVARCHAR(256)
AS
BEGIN
	DELETE 
		[lekmer].[tProductUrlHistory]
	WHERE 
		ProductId = @ProductId
		AND LanguageId = @LanguageId
		AND UrlTitleOld = @UrlTitleOld
END
GO
PRINT N'Creating [lekmer].[pProductUrlHistoryDeleteExpiredItems]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryDeleteExpiredItems]
AS
BEGIN
	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = GETDATE()

	DELETE 
		puh 
	FROM
		[lekmer].[tProductUrlHistory] puh
		INNER JOIN [lekmer].[tHistoryLifeIntervalType] hlit ON puh.HistoryLifeIntervalTypeId = hlit.HistoryLifeIntervalTypeId
	WHERE 
		@CurrentDate > DATEADD(DAY, hlit.DaysActive, puh.CreationDate)
END
GO
PRINT N'Creating [lekmer].[pContentPageUrlHistoryDeleteExpiredItems]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryDeleteExpiredItems]
AS
BEGIN
	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = GETDATE()

	DELETE 
		cpuh 
	FROM
		[lekmer].[tContentPageUrlHistory] cpuh
		INNER JOIN [lekmer].[tHistoryLifeIntervalType] hlit ON cpuh.HistoryLifeIntervalTypeId = hlit.HistoryLifeIntervalTypeId
	WHERE 
		@CurrentDate > DATEADD(DAY, hlit.DaysActive, cpuh.CreationDate)
END
GO
PRINT N'Creating [lekmer].[vProductUrlHistory]'
GO

CREATE view [lekmer].[vProductUrlHistory]
AS
	SELECT
		[ProductUrlHistoryId] AS 'ProductUrlHistory.Id',
		[ProductId] AS 'ProductUrlHistory.ProductId',
		[LanguageId] AS 'ProductUrlHistory.LanguageId',
		[UrlTitleOld] AS 'ProductUrlHistory.UrlTitleOld',
		[CreationDate] AS 'ProductUrlHistory.CreationDate',
		[LastUsageDate] AS 'ProductUrlHistory.LastUsageDate',
		[UsageAmount] AS 'ProductUrlHistory.UsageAmount',
		[HistoryLifeIntervalTypeId] AS 'ProductUrlHistory.HistoryLifeIntervalTypeId'
	FROM
		[lekmer].[tProductUrlHistory]
GO
PRINT N'Creating [lekmer].[vCustomProductUrlHistory]'
GO

CREATE view [lekmer].[vCustomProductUrlHistory]
AS
	SELECT
		*
	FROM
		[lekmer].[vProductUrlHistory]
GO
PRINT N'Creating [lekmer].[vContentPageUrlHistory]'
GO


CREATE view [lekmer].[vContentPageUrlHistory]
AS
	SELECT
		[ContentPageUrlHistoryId] AS 'ContentPageUrlHistory.Id',
		[ContentPageId] AS 'ContentPageUrlHistory.ContentPageId',
		[UrlTitleOld] AS 'ContentPageUrlHistory.UrlTitleOld',
		[CreationDate] AS 'ContentPageUrlHistory.CreationDate',
		[LastUsageDate] AS 'ContentPageUrlHistory.LastUsageDate',
		[UsageAmount] AS 'ContentPageUrlHistory.UsageAmount',
		[HistoryLifeIntervalTypeId] AS 'ContentPageUrlHistory.HistoryLifeIntervalTypeId'
	FROM
		[lekmer].[tContentPageUrlHistory]

GO
PRINT N'Creating [lekmer].[vCustomContentPageUrlHistory]'
GO


CREATE view [lekmer].[vCustomContentPageUrlHistory]
AS
	SELECT
		*
	FROM
		[lekmer].[vContentPageUrlHistory]

GO
PRINT N'Creating [lekmer].[pContentPageUrlHistoryCleanUp]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryCleanUp]
	@MaxCount INT
AS
BEGIN
	DECLARE 
		@ContentPageId INT,
		@Difference INT,
		@UrlHistoryCount INT

	DECLARE cur_urlHistory CURSOR FAST_FORWARD FOR
		SELECT
			cpuh.ContentPageId
		FROM
			[lekmer].[tContentPageUrlHistory] cpuh
		GROUP BY ContentPageId

	OPEN cur_urlHistory
	FETCH NEXT FROM cur_urlHistory
		INTO
			@ContentPageId
			
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @UrlHistoryCount = (SELECT COUNT(1) FROM [lekmer].[tContentPageUrlHistory]
								WHERE ContentPageId = @ContentPageId)
									  
		SET @Difference = @UrlHistoryCount - @MaxCount
		
		IF @Difference > 0
		BEGIN
			DELETE UrlHistory FROM 
			(SELECT TOP(@Difference) * 
			 FROM [lekmer].[tContentPageUrlHistory] 
			 WHERE ContentPageId = @ContentPageId
			 ORDER BY LastUsageDate ASC) UrlHistory
		END
	
		FETCH NEXT FROM cur_urlHistory
		INTO
			@ContentPageId
	END
	
	CLOSE cur_urlHistory
	DEALLOCATE cur_urlHistory
END
GO
PRINT N'Creating [lekmer].[pProductUrlHistoryCleanUp]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryCleanUp]
	@MaxCount INT
AS
BEGIN
	DECLARE 
		@ProductId INT,
		@LanguageId INT,
		@Difference INT,
		@UrlHistoryCount INT

	DECLARE cur_urlHistory CURSOR FAST_FORWARD FOR
		SELECT
			puh.ProductId,
			puh.LanguageId
		FROM
			[lekmer].[tProductUrlHistory] puh
			GROUP BY ProductId, LanguageId

	OPEN cur_urlHistory
	FETCH NEXT FROM cur_urlHistory
		INTO
			@ProductId,
			@LanguageId
			
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @UrlHistoryCount = (SELECT COUNT(1) FROM [lekmer].[tProductUrlHistory] 
								WHERE ProductId = @ProductId
									  AND LanguageId = @LanguageId)
									  
		SET @Difference = @UrlHistoryCount - @MaxCount
		
		IF @Difference > 0
		BEGIN
			DELETE UrlHistory FROM 
			(SELECT TOP(@Difference) * 
			 FROM [lekmer].[tProductUrlHistory] 
			 WHERE ProductId = @ProductId
				   AND LanguageId = @LanguageId
			 ORDER BY LastUsageDate ASC) UrlHistory
		END
	
		FETCH NEXT FROM cur_urlHistory
		INTO
			@ProductId,
			@LanguageId
	END
	
	CLOSE cur_urlHistory
	DEALLOCATE cur_urlHistory
END
GO
PRINT N'Creating [lekmer].[pProductUrlHistoryGetByUrlTitle]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryGetByUrlTitle]
	@LanguageId	INT,
	@UrlTitleOld NVARCHAR(256)
AS 
BEGIN 
	SELECT 
		CPUH.*
	FROM 
		[lekmer].[vCustomProductUrlHistory] CPUH
	WHERE
		CPUH.[ProductUrlHistory.LanguageId] = @LanguageId
		AND
		CPUH.[ProductUrlHistory.UrlTitleOld] = @UrlTitleOld
END
GO
PRINT N'Creating [lekmer].[pContentPageUrlHistoryGetByUrlTitle]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryGetByUrlTitle]
	@ChannelId INT,
	@UrlTitleOld NVARCHAR(256)
AS 
BEGIN 
	SELECT 
		CCPUH.*
	FROM 
		[lekmer].[vCustomContentPageUrlHistory] CCPUH
		LEFT OUTER JOIN [sitestructure].[tContentPage] AS CP ON CP.ContentNodeId = CCPUH.[ContentPageUrlHistory.ContentPageId]
		LEFT OUTER JOIN [sitestructure].[tSiteStructureModuleChannel] AS MCH ON MCH.SiteStructureRegistryId = CP.SiteStructureRegistryId
		LEFT OUTER JOIN [core].[tChannel] AS CH ON MCH.ChannelId = CH.ChannelId
	WHERE
		CH.ChannelId = @ChannelId
		AND
		CCPUH.[ContentPageUrlHistory.UrlTitleOld] = @UrlTitleOld
END
GO
PRINT N'Creating [lekmer].[pProductUrlHistoryGetAllByLanguageId]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryGetAllByLanguageId]
	@LanguageId	INT
AS 
BEGIN 
	SELECT 
		CPUH.*
	FROM 
		[lekmer].[vCustomProductUrlHistory] CPUH
	WHERE
		CPUH.[ProductUrlHistory.LanguageId] = @LanguageId
END
GO
PRINT N'Creating [lekmer].[pContentPageUrlHistoryGetAll]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryGetAll]
	@ChannelId INT
AS 
BEGIN 
	SELECT 
		CCPUH.*
	FROM 
		[lekmer].[vCustomContentPageUrlHistory] CCPUH
		LEFT OUTER JOIN [sitestructure].[tContentPage] AS CP ON CP.ContentNodeId = CCPUH.[ContentPageUrlHistory.ContentPageId]
		LEFT OUTER JOIN [sitestructure].[tSiteStructureModuleChannel] AS MCH ON MCH.SiteStructureRegistryId = CP.SiteStructureRegistryId
		LEFT OUTER JOIN [core].[tChannel] AS CH ON MCH.ChannelId = CH.ChannelId
	WHERE
		CH.ChannelId = @ChannelId
END
GO
PRINT N'Creating [lekmer].[pProductUrlHistoryGetByProductId]'
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryGetByProductId]
	@ProductId	INT
AS 
BEGIN 
	SELECT 
		CPUH.*
	FROM 
		[lekmer].[vCustomProductUrlHistory] CPUH
	WHERE
		CPUH.[ProductUrlHistory.ProductId] = @ProductId
END
GO
PRINT N'Creating [lekmer].[pContentPageUrlHistoryGetByPageId]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryGetByPageId]
	@ContentPageId	INT
AS 
BEGIN 
	SELECT 
		CCPUH.*
	FROM 
		[lekmer].[vCustomContentPageUrlHistory] CCPUH
	WHERE
		CCPUH.[ContentPageUrlHistory.ContentPageId] = @ContentPageId
END
GO
PRINT N'Creating [lekmer].[pContentPageUrlHistoryDeleteByContentPageIdAndUrlTitle]'
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryDeleteByContentPageIdAndUrlTitle]
	@ContentPageId	INT,
	@UrlTitleOld	NVARCHAR(256)
AS
BEGIN
	DELETE 
		[lekmer].[tContentPageUrlHistory]
	WHERE 
		ContentPageId = @ContentPageId
		AND UrlTitleOld = @UrlTitleOld
END
GO
PRINT N'Creating [lekmer].[pProductSimilarSave]'
GO
CREATE PROCEDURE [lekmer].[pProductSimilarSave]
	@HYProductErpId			VARCHAR(50),
	@ProductSimilarIdList	VARCHAR(MAX),
	@ScoreList				VARCHAR(MAX),
	@Separator				CHAR(1)
AS
BEGIN
	DECLARE @ProductId INT
	SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] 
					  WHERE HYErpId = @HYProductErpId)

	EXEC [lekmer].[pProductSimilarDelete] @ProductId

	INSERT INTO [lekmer].[tProductSimilar] (
		ProductId,
		SimilarProductId,
		Score
	)
	SELECT
		@ProductId,
		lp.ProductId,
		idList.Score
	FROM [lekmer].[fnConvertIDListToTableWithScore] (@ProductSimilarIdList, @ScoreList, @Separator) idList
	INNER JOIN [lekmer].[tLekmerProduct] lp ON idList.ErpId = lp.HYErpId
	
	RETURN @ProductId
END
GO
PRINT N'Creating [lekmer].[pBlockProductSimilarListGetByIdSecure]'
GO

CREATE PROCEDURE [lekmer].[pBlockProductSimilarListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		psl.*,
		b.*
	FROM 
		lekmer.vCustomBlockProductSimilarList AS psl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON psl.[BlockProductSimilarList.BlockId] = b.[Block.BlockId]
	WHERE
		psl.[BlockProductSimilarList.BlockId] = @BlockId
END
GO
PRINT N'Creating [lekmer].[pBlockProductSimilarListGetById]'
GO

CREATE PROCEDURE [lekmer].[pBlockProductSimilarListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		psl.*,
		b.*
	FROM 
		[lekmer].[vCustomBlockProductSimilarList] AS psl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON psl.[BlockProductSimilarList.BlockId] = b.[Block.BlockId]
	WHERE
		psl.[BlockProductSimilarList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
PRINT N'Creating [integration].[tProductTranslationDescriptionDK]'
GO
CREATE TABLE [integration].[tProductTranslationDescriptionDK]
(
[HYId] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
PRINT N'Creating [integration].[tProductTranslationDescriptionNO]'
GO
CREATE TABLE [integration].[tProductTranslationDescriptionNO]
(
[HYId] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockProductSimilarList]'
GO
ALTER TABLE [lekmer].[tBlockProductSimilarList] ADD
CONSTRAINT [FK_tBlockProductSimilarList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
PRINT N'Adding foreign keys to [lekmer].[tContentPageUrlHistory]'
GO
ALTER TABLE [lekmer].[tContentPageUrlHistory] ADD
CONSTRAINT [FK_tContentPageUrlHistory_tContentPage] FOREIGN KEY ([ContentPageId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId]),
CONSTRAINT [FK_tContentPageUrlHistory_tHistoryLifeIntervalType] FOREIGN KEY ([HistoryLifeIntervalTypeId]) REFERENCES [lekmer].[tHistoryLifeIntervalType] ([HistoryLifeIntervalTypeId])
GO
PRINT N'Adding foreign keys to [lekmer].[tProductUrlHistory]'
GO
ALTER TABLE [lekmer].[tProductUrlHistory] ADD
CONSTRAINT [FK_tProductUrlHistory_tHistoryLifeIntervalType] FOREIGN KEY ([HistoryLifeIntervalTypeId]) REFERENCES [lekmer].[tHistoryLifeIntervalType] ([HistoryLifeIntervalTypeId]),
CONSTRAINT [FK_tProductUrlHistory_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]),
CONSTRAINT [FK_tProductUrlHistory_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerChannel]'
GO
ALTER TABLE [lekmer].[tLekmerChannel] ADD
CONSTRAINT [FK_tLekmerChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
PRINT N'Adding foreign keys to [lekmer].[tProductSimilar]'
GO
ALTER TABLE [lekmer].[tProductSimilar] ADD
CONSTRAINT [FK_tProductSimilar_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]),
CONSTRAINT [FK_tProductSimilar_tLekmerProduct1] FOREIGN KEY ([SimilarProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO

--ROLLBACK
--RAISERROR('Remove ROLLBACK first!', 18, 0)
COMMIT
--/////////////////////////////////////