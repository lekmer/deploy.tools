SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
USE [Voucher_live]
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

--/////////////////////////////////////
BEGIN TRANSACTION

PRINT N'Rename product.tVoucherLog.Used to UsageDate'
GO
EXEC sp_rename
    @objname = 'product.tVoucherLog.Used',
    @newname = 'UsageDate',
    @objtype = 'COLUMN'
GO

PRINT N'Dropping foreign keys from [product].[tChannelGroup]'
GO
ALTER TABLE [product].[tChannelGroup] DROP
CONSTRAINT [fk_siteId]
GO
PRINT N'Dropping foreign keys from [product].[tVoucher]'
GO
ALTER TABLE [product].[tVoucher] DROP
CONSTRAINT [FK__tVoucher__ValidT__5070F446],
CONSTRAINT [fk_voucherInfoId]
GO
PRINT N'Dropping foreign keys from [product].[tVoucherCustomer]'
GO
ALTER TABLE [product].[tVoucherCustomer] DROP
CONSTRAINT [FK__tVoucherC__Custo__619B8048],
CONSTRAINT [FK__tVoucherC__Vouch__60A75C0F]
GO
PRINT N'Dropping foreign keys from [product].[tVoucherInfo]'
GO
ALTER TABLE [product].[tVoucherInfo] DROP
CONSTRAINT [fk_discountTypeId]
GO
PRINT N'Dropping constraints from [product].[tChannelGroup]'
GO
ALTER TABLE [product].[tChannelGroup] DROP CONSTRAINT [PK__tChannel__F58D816B74AE54BC]
GO
PRINT N'Dropping constraints from [product].[tCustomer]'
GO
ALTER TABLE [product].[tCustomer] DROP CONSTRAINT [PK__tCustome__A4AE64D85AEE82B9]
GO
PRINT N'Dropping constraints from [product].[tDiscountType]'
GO
ALTER TABLE [product].[tDiscountType] DROP CONSTRAINT [PK__tDiscoun__6CCE1DB6534D60F1]
GO
PRINT N'Dropping constraints from [product].[tSite]'
GO
ALTER TABLE [product].[tSite] DROP CONSTRAINT [PK__tSite__B9DCB963571DF1D5]
GO
PRINT N'Dropping constraints from [product].[tTmpV]'
GO
ALTER TABLE [product].[tTmpV] DROP CONSTRAINT [PK__tTmpV__3AEE79210C85DE4D]
GO
PRINT N'Dropping constraints from [product].[tVoucher]'
GO
ALTER TABLE [product].[tVoucher] DROP CONSTRAINT [PK__tVoucher__3AEE79214E88ABD4]
GO
PRINT N'Dropping constraints from [product].[tVoucherCreationLog]'
GO
ALTER TABLE [product].[tVoucherCreationLog] DROP CONSTRAINT [PK__tVoucher__03DEDB217F2BE32F]
GO
PRINT N'Dropping constraints from [product].[tVoucherCustomer]'
GO
ALTER TABLE [product].[tVoucherCustomer] DROP CONSTRAINT [PK__tVoucher__A700834A5EBF139D]
GO
PRINT N'Dropping constraints from [product].[tVoucherErrorLog]'
GO
ALTER TABLE [product].[tVoucherErrorLog] DROP CONSTRAINT [PK__integrat__3214EC0737088A03]
GO
PRINT N'Dropping constraints from [product].[tVoucherInfo]'
GO
ALTER TABLE [product].[tVoucherInfo] DROP CONSTRAINT [PK__tVoucher__05946CDB4AB81AF0]
GO
PRINT N'Dropping constraints from [product].[tVoucherLog]'
GO
ALTER TABLE [product].[tVoucherLog] DROP CONSTRAINT [PK__tVoucher__3FBEAD326D0D32F4]
GO
PRINT N'Dropping constraints from [user].[tVoucherCreationRestrictions]'
GO
ALTER TABLE [user].[tVoucherCreationRestrictions] DROP CONSTRAINT [PK__tVoucher__1788CC4C07C12930]
GO
PRINT N'Dropping constraints from [product].[tOldSystemVoucherCodes]'
GO
ALTER TABLE [product].[tOldSystemVoucherCodes] DROP CONSTRAINT [PK__tOldSyst__3214EC071FCDBCEB]
GO
PRINT N'Dropping index [IX_tVoucher_1] from [product].[tVoucher]'
GO
DROP INDEX [IX_tVoucher_1] ON [product].[tVoucher]
GO
PRINT N'Dropping [backoffice].[pVoucherCheckOLD]'
GO
DROP PROCEDURE [backoffice].[pVoucherCheckOLD]
GO
PRINT N'Dropping [backoffice].[pVoucherConsumeOLD]'
GO
DROP PROCEDURE [backoffice].[pVoucherConsumeOLD]
GO
PRINT N'Dropping [product].[tOldSystemVoucherCodes]'
GO
DROP TABLE [product].[tOldSystemVoucherCodes]
GO
PRINT N'Rebuilding [product].[tVoucherLog]'
GO
CREATE TABLE [product].[tmp_rg_xx_tVoucherLog]
(
[VoucherUsedId] [int] NOT NULL IDENTITY(1, 1),
[VoucherId] [int] NULL,
[UsageDate] [datetime] NOT NULL,
[OrderId] [int] NULL,
[AmountUsed] [decimal] (18, 0) NULL
)
GO
SET IDENTITY_INSERT [product].[tmp_rg_xx_tVoucherLog] ON
GO

INSERT INTO [product].[tmp_rg_xx_tVoucherLog](
	[VoucherUsedId], 
	[VoucherId],
	[UsageDate], 
	[OrderId]
)
SELECT 
	[VoucherUsedId], 
	[VoucherId], 
	[UsageDate],
	[OrderId] 
FROM
	[product].[tVoucherLog]


GO
SET IDENTITY_INSERT [product].[tmp_rg_xx_tVoucherLog] OFF
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[product].[tVoucherLog]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[product].[tmp_rg_xx_tVoucherLog]', RESEED, @idVal)
GO
DROP TABLE [product].[tVoucherLog]
GO
EXEC sp_rename N'[product].[tmp_rg_xx_tVoucherLog]', N'tVoucherLog'
GO
PRINT N'Creating primary key [PK_tVoucherLog] on [product].[tVoucherLog]'
GO
ALTER TABLE [product].[tVoucherLog] ADD CONSTRAINT [PK_tVoucherLog] PRIMARY KEY CLUSTERED  ([VoucherUsedId])
GO
PRINT N'Altering [product].[tVoucher]'
GO
ALTER TABLE [product].[tVoucher] ADD
[AmountLeft] [decimal] (18, 0) NULL
GO
PRINT N'Creating primary key [PK_tVoucher] on [product].[tVoucher]'
GO
ALTER TABLE [product].[tVoucher] ADD CONSTRAINT [PK_tVoucher] PRIMARY KEY CLUSTERED  ([VoucherId])
GO
PRINT N'Creating index [IX_tVoucher_VoucherCode] on [product].[tVoucher]'
GO
CREATE NONCLUSTERED INDEX [IX_tVoucher_VoucherCode] ON [product].[tVoucher] ([VoucherCode]) INCLUDE ([IsActive], [QuantityLeft], [Reserved], [ValidTo])
GO
PRINT N'Altering [backoffice].[pReportLekmerGetUsedVocherInfo]'
GO

ALTER PROCEDURE [backoffice].[pReportLekmerGetUsedVocherInfo]
	@UsedFrom		datetime = NULL,
	@UsedTo			datetime = NULL,	
	@ChannelGroupId	int = NULL,
	@CreatedBy		nvarchar(50) = NULL,
	@BatchId		int = NULL
AS
begin
	set nocount on
		
		select 		
			vi.VoucherInfoId as BatchId,  
			vi.Quantity, 			
			DiscountValue,
			vi.DiscountTitle,
			vi.[Description],
			c.[Description] as Channel,
			vi.OriginalStartQuantityPerCode, 
			ISNULL(vi.CreatedBy, '') AS CreatedBy,
			d.DiscountType, 
			(vi.Quantity * vi.OriginalStartQuantityPerCode) as TotalAmountOfUsableCodes,			
			(case when vi.DiscountTypeId = 1 then vi.DiscountValue 
					else ((vi.Quantity * vi.OriginalStartQuantityPerCode) * vi.DiscountValue)
					 end) as 'TotalDiscountForBatch (local currency or %)',
			z.TotalUsage as NoOfVouchersUsed,
			(case when vi.DiscountTypeId = 1 then vi.DiscountValue
					else (z.TotalUsage * vi.DiscountValue)
					 end) as 'DiscountUsed (local currency or %)'
		from 
			product.tVoucherInfo vi
			inner join  		
			(			
				select 
					v.VoucherInfoId,
					SUM(l2.[Count]) as TotalUsage
				from
					product.tVoucher v
					inner join 
					(
						select
							l.VoucherId,
							COUNT(*) as 'Count'
						from
							product.tVoucherLog l
						where
							(@UsedFrom is null or l.UsageDate > @UsedFrom)
							and
							(@UsedTo is null or l.UsageDate < @UsedTo)
						group by
							l.VoucherId
					) l2
					on v.VoucherId = l2.VoucherId				
				group by
					v.VoucherInfoId			
			) z
			
				on vi.VoucherInfoId = z.VoucherInfoId
			inner join product.tChannelGroup c
				on c.ChannelGroupId = vi.ChannelGroupId
			inner join product.tDiscountType d
				on vi.DiscountTypeId = d.DiscountTypeId									
			
		where
			(@BatchId is null or vi.VoucherInfoId = @BatchId) 
			and
			(@CreatedBy is null or vi.CreatedBy = @CreatedBy)
			and
			(vi.ChannelGroupId between 1 and 4
				or vi.ChannelGroupId = 13)
		order by
			vi.VoucherInfoId
	 
end
GO
PRINT N'Altering [backoffice].[pReportHeppoGetUsedVocherInfo]'
GO

ALTER PROCEDURE [backoffice].[pReportHeppoGetUsedVocherInfo]
	@UsedFrom		datetime = NULL,
	@UsedTo			datetime = NULL,	
	@ChannelGroupId	int = NULL,
	@CreatedBy		nvarchar(50) = NULL,
	@BatchId		int = NULL
AS
begin
	set nocount on
		
		select 		
			vi.VoucherInfoId as BatchId,  
			vi.Quantity, 			
			DiscountValue,
			vi.DiscountTitle,
			vi.[Description],
			c.[Description] as Channel,
			vi.OriginalStartQuantityPerCode, 
			ISNULL(vi.CreatedBy, '') AS CreatedBy,
			d.DiscountType, 
			(vi.Quantity * vi.OriginalStartQuantityPerCode) as TotalAmountOfUsableCodes,			
			(case when vi.DiscountTypeId = 1 then vi.DiscountValue 
					else ((vi.Quantity * vi.OriginalStartQuantityPerCode) * vi.DiscountValue)
					 end) as 'TotalDiscountForBatch (local currency or %)',
			z.TotalUsage as NoOfVouchersUsed,
			(case when vi.DiscountTypeId = 1 then vi.DiscountValue
					else (z.TotalUsage * vi.DiscountValue)
					 end) as 'DiscountUsed (local currency or %)'
		from 
			product.tVoucherInfo vi
			inner join  		
			(			
				select 
					v.VoucherInfoId,
					SUM(l2.[Count]) as TotalUsage
				from
					product.tVoucher v
					inner join 
					(
						select
							l.VoucherId,
							COUNT(*) as 'Count'
						from
							product.tVoucherLog l
						where
							(@UsedFrom is null or l.UsageDate > @UsedFrom)
							and
							(@UsedTo is null or l.UsageDate < @UsedTo)
						group by
							l.VoucherId
					) l2
					on v.VoucherId = l2.VoucherId				
				group by
					v.VoucherInfoId			
			) z
			
				on vi.VoucherInfoId = z.VoucherInfoId
			inner join product.tChannelGroup c
				on c.ChannelGroupId = vi.ChannelGroupId
			inner join product.tDiscountType d
				on vi.DiscountTypeId = d.DiscountTypeId									
			
		where
			(@BatchId is null or vi.VoucherInfoId = @BatchId) 
			and
			(@CreatedBy is null or vi.CreatedBy = @CreatedBy)
			and
			(vi.ChannelGroupId between 6 and 9
				or vi.ChannelGroupId = 12)
		order by
			vi.VoucherInfoId
	 /*
	 exec [backoffice].[pReportHeppoGetUsedVocherInfo]
--'2010-07-08 00:00:00.000', 
--'2011-08-10 00:00:00.000',
NULL,
NULL,
NULL, NULL, NULL
	 */
end
GO
PRINT N'Altering [backoffice].[pVoucherConsume]'
GO

ALTER PROCEDURE [backoffice].[pVoucherConsume]
	@VoucherCode	NVARCHAR (50),
	@OrderId		INT,
	@UsedAmount		DECIMAL(18,0) = NULL
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION

			-- Voucher 'GiftCard'
			IF EXISTS (SELECT 1
					   FROM product.tVoucher v
					   INNER JOIN product.tVoucherInfo vi on vi.VoucherInfoId = v.VoucherInfoId
					   WHERE v.VoucherCode = @VoucherCode
					   AND vi.DiscountTypeId = 3)
				BEGIN
					DECLARE @AmountLeft DECIMAL(18,0)
					SELECT @AmountLeft = v.AmountLeft - @UsedAmount FROM product.tVoucher v WHERE v.VoucherCode = @VoucherCode
										
					UPDATE 
						v 
					SET 
						v.AmountLeft = @AmountLeft
						,v.QuantityLeft = (CASE WHEN @AmountLeft <= 0 THEN v.QuantityLeft - 1 ELSE v.QuantityLeft END)
					FROM
						product.tVoucher v
					WHERE
						v.VoucherCode = @VoucherCode
				END
			-- Voucher other
			ELSE
				BEGIN
					UPDATE 
						v 
					SET 
						v.QuantityLeft = (v.QuantityLeft - 1)
					FROM
						product.tVoucher v
					WHERE
						v.VoucherCode = @VoucherCode
				END
			
			SELECT 
				vi.DiscountValue, 
				dt.DiscountTypeId,
				vi.VoucherInfoId,
				v.AmountLeft
			FROM 
				product.tVoucher v 
				INNER JOIN product.tVoucherInfo vi ON v.VoucherInfoId = vi.VoucherInfoId
				INNER JOIN product.tDiscountType dt ON vi.DiscountTypeId = dt.DiscountTypeId
			WHERE 
				v.VoucherCode = @VoucherCode 
		
			INSERT INTO product.tVoucherLog(VoucherId, UsageDate, OrderId, AmountUsed)
			SELECT
				(SELECT VoucherId FROM product.tVoucher WHERE VoucherCode = @VoucherCode),
				GETDATE(),
				@OrderId,
				@UsedAmount
									
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		if @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
PRINT N'Altering [backoffice].[pVoucherBatch]'
GO

ALTER PROCEDURE [backoffice].[pVoucherBatch]
	@ValidFrom		datetime,
	@ValidTo		datetime,
	@Prefix			varchar (32),
	@Suffix			varchar (32),
	@Quantity		int,
	@DiscountTypeId	int,
	@DiscountValue	decimal,
	@IsActive		bit,
	@ChannelGroupId	int,
	@DiscountTitle	nvarchar (250),
	@Description	nvarchar (250),
	@NbrOfToken		int,
	@OriginalStartQuantityPerCode	int,
	@ValidInDays	int,
	@CreatedBy		nvarchar(250) = null
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		
		-- Voucher 'GiftCard'
		IF (@DiscountTypeId = 3 AND @OriginalStartQuantityPerCode > 1)
		BEGIN
			RAISERROR ('The voucher info with discount type = "GiftCard" should has the @OriginalStartQuantityPerCode = 1.', 16, 1)
		END
		
		declare @Return int
		set @Return = 1
		
		insert into product.tVoucherInfo(ValidFrom, ValidTo, Prefix, Suffix, Quantity, DiscountTypeId,
			DiscountValue, IsActive, ChannelGroupId, DiscountTitle, [Description], NbrOfToken, OriginalStartQuantityPerCode,
			ValidInDays, CreatedBy)
		select
			(CAST(@ValidFrom AS datetime)),
			(CAST(@ValidTo AS datetime)),
			@Prefix,
			@Suffix,
			@Quantity,
			@DiscountTypeId,
			@DiscountValue,
			@IsActive,
			@ChannelGroupId,
			@DiscountTitle,
			@Description,
			@NbrOfToken,
			@OriginalStartQuantityPerCode,
			@ValidInDays,
			@CreatedBy
		
		set @Return = 0

	commit transaction
	return @Return
	end try
	begin catch
		set @Return = 5
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', @ErrMsg, GETDATE(), @SP)
		
		RETURN @Return
	END CATCH		 
END
GO
PRINT N'Altering [backoffice].[pVoucherConsumeValueReturned]'
GO

ALTER PROCEDURE [backoffice].[pVoucherConsumeValueReturned]
	@VoucherCode	nvarchar (50)
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		
		--declare @Return int
		--set @Return = 1			
		
		--Set @Return = 
		create table #tTemp
		(
			DiscountValue	decimal,
			DiscountTypeId	int
		)
		insert into #tTemp
		select vi.DiscountValue, dt.DiscountTypeId
		from product.tVoucher v 
				inner join
					product.tVoucherInfo vi
						on v.VoucherInfoId = vi.VoucherInfoId
				inner join product.tDiscountType dt
						on vi.DiscountTypeId = dt.DiscountTypeId
		where v.VoucherCode = @VoucherCode 
		if exists (select 1 from #tTemp)
		begin
			select DiscountValue, DiscountTypeId
			from #tTemp
		end
		--set @Return = @VoucherCode
	commit transaction
	--return @Return
	end try
	begin catch
		--set @Return = 5
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		--return @Return
	end catch		 
end
GO
PRINT N'Altering [backoffice].[pVoucherGenerateCodes]'
GO
ALTER procedure [backoffice].[pVoucherGenerateCodes]
	@VoucherInfoId	int,
	@nbrOfVouchers	int,
	@customers		xml = null,
	@ValidTo		smalldatetime = null
as
begin
	set nocount on
	begin try
		begin transaction
		
		declare
		@nbrOfToken			int,
		@suffix				varchar(32),
		@prefix				varchar(32),
		@DiscountValue		decimal(18,0),
		@OriginalQuantity	int

		select
			@nbrOfToken = NbrOfToken,
			@suffix = Suffix,
			@prefix = Prefix,
			@DiscountValue = DiscountValue,
			@OriginalQuantity = OriginalStartQuantityPerCode
		from
			product.tVoucherInfo
		where
			VoucherInfoId = @VoucherInfoId

		set @nbrOfToken = @nbrOfToken - 2 -- reduce length by two characters since we add a 2 digit checksum.

		if @nbrOfVouchers is null and @customers is null
		begin
			raiserror ('Either @nbrOfVouchers or @customers must have a non null value.', 16, 1)
		end
		

		if @nbrOfVouchers is null -- Customer
		begin
			select
				@nbrOfVouchers = count(*)
			from
				@customers.nodes('customers/customer') as T(c)
		end


		declare @codes table (code varchar(32) COLLATE Finnish_Swedish_CS_AS)

		declare
			@voucherCount	int,
			@tokenCount		int,
			@validTokens	varchar(100),
			@lenValidTokens	int,
			@pos			int,
			@sum			int,
			@token			char,
			@code			varchar(32)

		-- Only these character can be used for the codes
		set @validTokens = '34679abcdefghijkmnpqrtwxyACDEFGHJKLMNPQRTWXY'
		set @lenValidTokens = len(@validTokens)

		-- Generate codes in a loop, generate some extra to get enough unique codes
		set @voucherCount = 0
		while @voucherCount < @nbrOfVouchers + @nbrOfVouchers / 10 + 10
		begin
			-- Generate code
			set @tokenCount = 0
			set @code = ''
			set @sum = 0
			while @tokenCount < @nbrOfToken
			begin
				set @pos = convert(int, rand() * @lenValidTokens)
				set @token = substring(@validTokens, @pos + 1, 1)
				set @sum = @sum + ascii(@token)
				set @code = @code + @token

				set @tokenCount = @tokenCount + 1
			end

			-- Append checksum
			set @sum = @sum % 100
			if @sum < 10 set @code = @code + '0'
			set @code = @code + convert(varchar(2), @sum)

			-- Build the final code
			set @code = @prefix + @code + @suffix

			-- Insert the code in our temporary amount
			insert into
				@codes
				(code)
			values
				(@code)

			set @voucherCount = @voucherCount + 1
		end
		
		
		-- Valid-to-date
		if @ValidTo is null
		begin
			declare @ValidInDays int
			select
				@ValidInDays = ValidInDays
			from
				[product].tVoucherInfo
			where
				VoucherInfoId = @VoucherInfoId
			
			-- If @ValidTo is null then set to today + ValidInDays in tVoucherInfo (23:59).
			-- If ValidInDays is null as well then set ValidTo = null for the generated vouchers.
			if @ValidInDays is not null
			begin
				set @ValidTo = cast((convert(varchar(8), dateadd(day, @ValidInDays, getdate()), 112) + ' 23:59') as smalldatetime)
			end
		end

		-- Table to save generated id's in.
		declare @Vouchers table (VoucherId int)

		-- Insert the generated codes
		insert into
			[product].[tVoucher]
			(VoucherInfoId, VoucherCode, ValidTo, QuantityLeft, AmountLeft) -- lägg till id
		output inserted.VoucherId into @Vouchers
		-- Pick the required number of codes
		-- Distinct avoids picking duplicates in the generated amount
		select distinct
			top (@nbrOfVouchers)
			@VoucherInfoId,
			code,
			@ValidTo,
			@OriginalQuantity,
			@DiscountValue
		from
			@codes
		-- Only insert codes that doesn't exist
		where
			code not in (
				select
					VoucherCode
				from
					[product].[tVoucher]
			)


		-- Handle customers
		if @customers is not null
		begin
			declare @CustomerIds table (CustomerId int)

			insert into
				@CustomerIds
				(CustomerId)
			select
				T.c.value('@id', 'int') as CustomerId
			from
				@customers.nodes('customers/customer') as T(c)


			-- Check that input xml contains the right number of rows.
			if @@rowcount <> @nbrOfVouchers
			begin
				raiserror ('Supplied number of customers must match the number of codes to generate.', 16, 1)
			end

			-- Table to save generated codes for customers
			declare @VoucherCustomers table (VoucherId int, CustomerId int)

			insert into
				product.tVoucherCustomer
				(VoucherId, CustomerId)  -- (VoucherId, CustomerId, IsUsed)
			output inserted.VoucherId, inserted.CustomerId into @VoucherCustomers -- inserted.VoucherId, inserted.CustomerId into @VoucherCustomers
			select
				V.VoucherId,
				C.CustomerId
				--0
			from (select
					VoucherId,
					row_number() over (order by VoucherId) as Row
				from @Vouchers) as V
				inner join (select
						CustomerId,
						row_number() over (order by CustomerId) as Row
					from @CustomerIds) as C
					on V.Row = C.Row
		end
		

		-- Verify usage count
		declare @Quantity int
		
		select @Quantity = Quantity from product.tVoucherInfo where VoucherInfoId = @VoucherInfoId

		if @Quantity > 0
		BEGIN
			IF ((SELECT COUNT(*) FROM product.tVoucher WHERE VoucherInfoId = @VoucherInfoId) > @Quantity)
			BEGIN
				PRINT 'The number of codes may not exceed the quantity value.'
				RAISERROR ('The number of codes may not exceed the quantity value.', 16, 1)
			END
		END

		
		-- Return info about created codes
		if @customers is not null
		begin
			-- Return info about created codes for customers
			select
				V.VoucherCode,
				VC.CustomerId
			from product.tVoucher V
				inner join @VoucherCustomers VC on V.VoucherId = VC.VoucherId
		end
		else
		begin
			-- Return info about created codes for customers
			select
				V.VoucherCode,
				null as CustomerId
			from product.tVoucher V
				inner join @Vouchers VC on V.VoucherId = VC.VoucherId
		end


		COMMIT TRANSACTION

		RETURN 0 
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()
		
		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', @ErrMsg, GETDATE(), @SP)
		
		--exec generic.pErrorRaise @ErrMsg, @SP, @Severity, @State      
		
		RETURN 1
	END CATCH		
END




--exec [backoffice].[pVoucherGenerateCodes] 3,5,null,null 
--exec [backoffice].[pVoucherGenerateCodes] 3,null,'<customers><customer id="1"/></customers>',null 

/*
insert into product.tDiscountType (DiscountType) values ('Test discount')
insert into product.tSite (SiteTitle, Description) values ('LekMer.se', '')

exec [backoffice].[pVoucherGenerateCodes] 3,5,null,null 


insert into product.tVoucherInfo (ValidFrom, ValidTo,
 Prefix, Suffix, Quantity, DiscountTypeId, 
 DiscountValue, IsActive, SiteId, Description, 
 DiscountTitle, NbrOfToken, OriginalStartQuantityPerCode)
values (
	'2010-08-12 00:00:00',
	'',
	'',
	'',
	100, -- Quantity hur många koder som ska skapas
	1,
	20,
	1,
	6,
	'Grand Opening',
	'Fredagen innan Grand Opening',
	8,
	1,
	8
)

exec [backoffice].[pVoucherGenerateCodes] 15,100,null,null 

insert into product.tVoucherInfo (ValidFrom, --ValidTo
 Prefix, Suffix, Quantity, DiscountTypeId, 
 DiscountValue, IsActive, ChannelGroupId, Description, 
 DiscountTitle, NbrOfToken, OriginalStartQuantityPerCode, ValidInDays, CreatedBy)
values (
	'2011-03-08 00:00:00',
	--'',
	'',
	'',
	100, -- Quantity hur många koder som ska skapas
	1,
	20,
	1,
	6,
	'Grand Opening',
	'Fredagen innan Grand Opening',
	8,
	1,
	8,
	'dino.miralem@it.cdon.com'
)
*/
GO
PRINT N'Altering [backoffice].[pVoucherCheck]'
GO

ALTER PROCEDURE [backoffice].[pVoucherCheck]
	@VoucherCode			NVARCHAR (50),
	@SiteApplicationName	NVARCHAR(40)
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		DECLARE @SiteId INT
		SET @SiteId = 0
		SELECT @SiteId = siteId FROM product.tSite WHERE SiteTitle = @SiteApplicationName

		IF @SiteId != 0
			BEGIN
				IF EXISTS 
				(SELECT * FROM product.tVoucher v
					INNER JOIN product.tVoucherInfo vi on v.VoucherInfoId = vi.VoucherInfoId
					INNER JOIN product.tChannelGroup chg on chg.ChannelGroupId = vi.ChannelGroupId
				 WHERE
					VoucherCode = @VoucherCode
					AND (QuantityLeft > 0 ) 
					AND ((vi.ValidTo IS NULL OR GETDATE() < (vi.ValidTo)) 
						  AND (v.ValidTo IS NULL OR GETDATE() < (v.ValidTo)))										
					AND (vi.IsActive = 1)
					AND (v.IsActive = 1 OR v.IsActive IS NULL)	
					AND (vi.ValidFrom < GETDATE())
					AND (v.Reserved = 0 OR v.Reserved IS NULL)
					AND chg.SiteId = @SiteId
					-- Voucher 'GiftCard'
					AND (vi.DiscountTypeId <> 3 OR v.AmountLeft > 0)
				 )
					BEGIN
						SELECT 
							vi.DiscountValue, 
							vi.DiscountTypeId,
							vi.VoucherInfoId,
							v.AmountLeft
						FROM 
							product.tVoucher v 
							INNER JOIN product.tVoucherInfo vi ON v.VoucherInfoId = vi.VoucherInfoId
						WHERE 
							v.VoucherCode = @VoucherCode 
					END				
			END
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
					
	END CATCH	 
END
GO
PRINT N'Creating primary key [PK_tChannelGroup] on [product].[tChannelGroup]'
GO
ALTER TABLE [product].[tChannelGroup] ADD CONSTRAINT [PK_tChannelGroup] PRIMARY KEY CLUSTERED  ([ChannelGroupId], [SiteId])
GO
PRINT N'Creating primary key [PK_tCustomer] on [product].[tCustomer]'
GO
ALTER TABLE [product].[tCustomer] ADD CONSTRAINT [PK_tCustomer] PRIMARY KEY CLUSTERED  ([CustomerId])
GO
PRINT N'Creating primary key [PK_tDiscountType] on [product].[tDiscountType]'
GO
ALTER TABLE [product].[tDiscountType] ADD CONSTRAINT [PK_tDiscountType] PRIMARY KEY CLUSTERED  ([DiscountTypeId])
GO
PRINT N'Creating primary key [PK_tSite] on [product].[tSite]'
GO
ALTER TABLE [product].[tSite] ADD CONSTRAINT [PK_tSite] PRIMARY KEY CLUSTERED  ([SiteId])
GO
PRINT N'Creating primary key [PK_tTmpV] on [product].[tTmpV]'
GO
ALTER TABLE [product].[tTmpV] ADD CONSTRAINT [PK_tTmpV] PRIMARY KEY CLUSTERED  ([VoucherId])
GO
PRINT N'Creating primary key [PK_tVoucherCreationLog] on [product].[tVoucherCreationLog]'
GO
ALTER TABLE [product].[tVoucherCreationLog] ADD CONSTRAINT [PK_tVoucherCreationLog] PRIMARY KEY CLUSTERED  ([CreationId])
GO
PRINT N'Creating primary key [PK_tVoucherCustomer] on [product].[tVoucherCustomer]'
GO
ALTER TABLE [product].[tVoucherCustomer] ADD CONSTRAINT [PK_tVoucherCustomer] PRIMARY KEY CLUSTERED  ([CustomerId], [VoucherId])
GO
PRINT N'Creating primary key [PK_tVoucherErrorLog] on [product].[tVoucherErrorLog]'
GO
ALTER TABLE [product].[tVoucherErrorLog] ADD CONSTRAINT [PK_tVoucherErrorLog] PRIMARY KEY CLUSTERED  ([Id])
GO
PRINT N'Creating primary key [PK_tVoucherInfo] on [product].[tVoucherInfo]'
GO
ALTER TABLE [product].[tVoucherInfo] ADD CONSTRAINT [PK_tVoucherInfo] PRIMARY KEY CLUSTERED  ([VoucherInfoId])
GO
PRINT N'Creating primary key [PK_tVoucherCreationRestrictions] on [user].[tVoucherCreationRestrictions]'
GO
ALTER TABLE [user].[tVoucherCreationRestrictions] ADD CONSTRAINT [PK_tVoucherCreationRestrictions] PRIMARY KEY CLUSTERED  ([UserId])
GO
PRINT N'Adding foreign keys to [product].[tChannelGroup]'
GO
ALTER TABLE [product].[tChannelGroup] ADD
CONSTRAINT [FK_tChannelGroup_tSite] FOREIGN KEY ([SiteId]) REFERENCES [product].[tSite] ([SiteId])
GO
PRINT N'Adding foreign keys to [product].[tVoucher]'
GO
ALTER TABLE [product].[tVoucher] ADD
CONSTRAINT [FK_tVoucher_tVoucherInfo] FOREIGN KEY ([VoucherInfoId]) REFERENCES [product].[tVoucherInfo] ([VoucherInfoId])
GO
PRINT N'Adding foreign keys to [product].[tVoucherCustomer]'
GO
ALTER TABLE [product].[tVoucherCustomer] ADD
CONSTRAINT [FK_tVoucherCustomer_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [product].[tCustomer] ([CustomerId]),
CONSTRAINT [FK_tVoucherCustomer_tVoucher] FOREIGN KEY ([VoucherId]) REFERENCES [product].[tVoucher] ([VoucherId])
GO
PRINT N'Adding foreign keys to [product].[tVoucherInfo]'
GO
ALTER TABLE [product].[tVoucherInfo] ADD
CONSTRAINT [FK_tVoucherInfo_tDiscountType] FOREIGN KEY ([DiscountTypeId]) REFERENCES [product].[tDiscountType] ([DiscountTypeId])
GO


-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Add row to [product].[tDiscountType]
SET IDENTITY_INSERT [product].[tDiscountType] ON
INSERT INTO [product].[tDiscountType] ([DiscountTypeId], [DiscountType]) VALUES (3, N'GiftCard')
SET IDENTITY_INSERT [product].[tDiscountType] OFF
GO




--ROLLBACK
--RAISERROR('Remove ROLLBACK first!', 18, 0)
COMMIT
--/////////////////////////////////////