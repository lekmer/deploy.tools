/****** Object:  Table [template].[tAlias]    Script Date: 05/07/2013 18:13:14 ******/
SET IDENTITY_INSERT [template].[tAlias] ON
INSERT [template].[tAlias] ([AliasId], [AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description]) VALUES (1000889, 1000017, N'Order.Klarna.DeniedResponseCode', 1, N'', N'error message for denied response code')
INSERT [template].[tAlias] ([AliasId], [AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description]) VALUES (1000890, 1000017, N'Order.Klarna.PendingResponseCode', 1, N'', N'error message for pending response code')
SET IDENTITY_INSERT [template].[tAlias] OFF

/****** Object:  Table [template].[tAliasTranslation]    Script Date: 05/07/2013 18:13:14 ******/
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000889, 1, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000889, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000889, 1000001, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000889, 1000002, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000889, 1000003, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000889, 1000005, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000890, 1, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000890, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000890, 1000001, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000890, 1000002, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000890, 1000003, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (1000890, 1000005, NULL)