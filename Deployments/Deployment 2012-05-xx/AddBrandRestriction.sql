BEGIN TRANSACTION

DECLARE @ProductRegistryRestrictionBrand TABLE (BrandId INT, ProductRegistryId INT)
INSERT INTO @ProductRegistryRestrictionBrand (BrandId, ProductRegistryId) 
VALUES 
	(1000139, 2), --lloyd
	(1000144, 3)

INSERT INTO @ProductRegistryRestrictionBrand (BrandId, ProductRegistryId) 
SELECT 1000218, pr.ProductRegistryId FROM [product].[tProductRegistry] pr
WHERE pr.ProductRegistryId <> 1


DECLARE @ProductRegistryRestrictionProduct TABLE (ProductId INT, ProductRegistryId INT)
INSERT INTO @ProductRegistryRestrictionProduct (ProductId, ProductRegistryId) 
VALUES 
	(1004337, 2),
	(1004340, 2),			
	(1004341, 2),
	(1018932, 2),
	(1021365, 2),
	(1021366, 2),
	(1021368, 2)

----------------------------------------------------------------------------
--- Insert tProductRegistryRestrictionBrand
----------------------------------------------------------------------------
INSERT INTO	[lekmer].[tProductRegistryRestrictionBrand] (
	ProductRegistryId,
	BrandId,
	RestrictionReason,
	UserId,
	CreatedDate
)
SELECT
	ProductRegistryId,
	BrandId,
	'Brand Restriction',
	NULL,
	GETDATE()
FROM
	@ProductRegistryRestrictionBrand
WHERE NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionBrand] prrb
				  WHERE BrandId = prrb.BrandId AND prrb.ProductRegistryId = ProductRegistryId)



----------------------------------------------------------------------------
--- Insert tProductRegistryRestrictionProduct
----------------------------------------------------------------------------
INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
	ProductRegistryId,
	ProductId,
	RestrictionReason,
	UserId,
	CreatedDate
)
SELECT
	ProductRegistryId,
	ProductId,
	'Brand Restriction',
	NULL,
	GETDATE()
FROM
	@ProductRegistryRestrictionProduct
WHERE NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
				  WHERE ProductId = prrp.ProductId AND prrp.ProductRegistryId = ProductRegistryId)


--------------------------------------------------------------------------------
---- Delete items from tProductRegistryProduct.
--------------------------------------------------------------------------------
-- By Product restroction
DELETE prp
FROM [product].[tProductRegistryProduct] prp
INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = prp.ProductRegistryId 
																 AND prrp.ProductId = prp.ProductId
-- By Brand restroction
DELETE prp 
FROM [product].[tProductRegistryProduct] prp
	 INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = prp.[ProductId]
	 INNER JOIN [lekmer].[tProductRegistryRestrictionBrand] prrb ON prrb.[BrandId] = lp.[BrandId] 
																	AND prrb.[ProductRegistryId] = prp.[ProductRegistryId]

COMMIT TRANSACTION