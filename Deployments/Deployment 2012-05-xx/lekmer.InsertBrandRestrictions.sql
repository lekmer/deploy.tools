DECLARE @ProductRegistryRestrictionBrand TABLE (BrandTitle NVARCHAR(500), ProductRegistryId INT)

INSERT INTO @ProductRegistryRestrictionBrand (BrandTitle, ProductRegistryId)
VALUES
	('Bobux', 6), ('Bobux', 7),
	('Lloyd', 6), ('Lloyd', 7),
	('Magnanni', 6), ('Magnanni', 7),
	('Rivieras', 6), ('Rivieras', 7),
	('Playboy', 6), ('Playboy', 7),
	('Saucony', 6), ('Saucony', 7),
	('FitFlop', 6), ('FitFlop', 7),
	('Fred Perry', 6), ('Fred Perry', 7),
	('HUB', 6), ('HUB', 7),
	('H by Hudson', 6), ('H by Hudson', 7),
	('Falke', 6), ('Falke', 7),
	('Burlington', 6), ('Burlington', 7),
	('Tommy Hilfiger', 6), ('Tommy Hilfiger', 7),
	('R''Belle', 6), ('R''Belle', 7),
	('Scotch Shrunk', 6), ('Scotch Shrunk', 7),
	('Esprit', 6), ('Esprit', 7),
	('Diesel', 6), ('Diesel', 7),
	('Helly Hansen', 6), ('Helly Hansen', 7),
	('Vibram 5-fingers', 6), ('Vibram 5-fingers', 7),
	('Paraboot', 6), ('Paraboot', 7),
	('Sanders', 6), ('Sanders', 7),
	('Aigle', 6), ('Aigle', 7),
	('Harley Davidson', 6), ('Harley Davidson', 7),
	('Wolverine', 6), ('Wolverine', 7),
	('Minnetonka', 6), ('Minnetonka', 7),
	('Xti', 6),
	('Alfiere', 6)

INSERT INTO [lekmer].[tProductRegistryRestrictionBrand] (
	ProductRegistryId,
	BrandId,
	RestrictionReason,
	UserId,
	CreatedDate
)
SELECT
	prrb.ProductRegistryId,
	b.BrandId,
	'Brand restriction',
	NULL,
	GETDATE()
FROM
	[lekmer].[tBrand] b
	INNER JOIN @ProductRegistryRestrictionBrand prrb ON prrb.BrandTitle = b.Title
	
	
--------------------------------------------------------------------------------
---- Delete items from tProductRegistryProduct.
--------------------------------------------------------------------------------
-- By Brand restroction
DELETE prp 
FROM [product].[tProductRegistryProduct] prp
	 INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = prp.[ProductId]
	 INNER JOIN [lekmer].[tProductRegistryRestrictionBrand] prrb ON prrb.[BrandId] = lp.[BrandId] 
																	AND prrb.[ProductRegistryId] = prp.[ProductRegistryId]