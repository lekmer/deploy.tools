/*
Run this script on:

        10.150.43.52.Lekmer_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 011\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 14.05.2012 9:32:36

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [campaignlek]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating types'
GO
CREATE TYPE [lekmer].[ProductRegistryProducts] AS TABLE
(
[ProductId] [int] NOT NULL,
[ProductRegistryId] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([ProductId], [ProductRegistryId])
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountAction]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountAction]
(
[ProductActionId] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL CONSTRAINT [DF_tFixedDiscountAction_IncludeAllProducts] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountAction] on [campaignlek].[tFixedDiscountAction]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountAction] ADD CONSTRAINT [PK_tFixedDiscountAction] PRIMARY KEY CLUSTERED  ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartContainsConditionExcludeBrand]'
GO
CREATE TABLE [addon].[tCartContainsConditionExcludeBrand]
(
[ConditionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartContainsConditionExcludeBrand] on [addon].[tCartContainsConditionExcludeBrand]'
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeBrand] ADD CONSTRAINT [PK_tCartContainsConditionExcludeBrand] PRIMARY KEY CLUSTERED  ([ConditionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartContainsConditionIncludeBrand]'
GO
CREATE TABLE [addon].[tCartContainsConditionIncludeBrand]
(
[ConditionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartContainsConditionIncludeBrand] on [addon].[tCartContainsConditionIncludeBrand]'
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeBrand] ADD CONSTRAINT [PK_tCartContainsConditionIncludeBrand] PRIMARY KEY CLUSTERED  ([ConditionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCartValueRangeCondition]'
GO
CREATE TABLE [campaignlek].[tCartValueRangeCondition]
(
[ConditionId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartValueRangeCondition] on [campaignlek].[tCartValueRangeCondition]'
GO
ALTER TABLE [campaignlek].[tCartValueRangeCondition] ADD CONSTRAINT [PK_tCartValueRangeCondition] PRIMARY KEY CLUSTERED  ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCartValueRangeConditionCurrency]'
GO
CREATE TABLE [campaignlek].[tCartValueRangeConditionCurrency]
(
[ConditionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValueFrom] [decimal] (16, 2) NOT NULL,
[MonetaryValueTo] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartValueRangeConditionCurrency] on [campaignlek].[tCartValueRangeConditionCurrency]'
GO
ALTER TABLE [campaignlek].[tCartValueRangeConditionCurrency] ADD CONSTRAINT [PK_tCartValueRangeConditionCurrency] PRIMARY KEY CLUSTERED  ([ConditionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountActionCurrency]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountActionCurrency]
(
[ProductActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountActionCurrency] on [campaignlek].[tFixedDiscountActionCurrency]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionCurrency] ADD CONSTRAINT [PK_tFixedDiscountActionCurrency] PRIMARY KEY CLUSTERED  ([ProductActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountActionExcludeBrand]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountActionExcludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountActionExcludeBrand] on [campaignlek].[tFixedDiscountActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeBrand] ADD CONSTRAINT [PK_tFixedDiscountActionExcludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountActionExcludeCategory]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountActionExcludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountActionExcludeCategory] on [campaignlek].[tFixedDiscountActionExcludeCategory]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeCategory] ADD CONSTRAINT [PK_tFixedDiscountActionExcludeCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountActionExcludeProduct]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountActionExcludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountActionExcludeProduct] on [campaignlek].[tFixedDiscountActionExcludeProduct]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeProduct] ADD CONSTRAINT [PK_tFixedDiscountActionExcludeProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountActionIncludeBrand]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountActionIncludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountActionIncludeBrand] on [campaignlek].[tFixedDiscountActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeBrand] ADD CONSTRAINT [PK_tFixedDiscountActionIncludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountActionIncludeCategory]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountActionIncludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountActionIncludeCategory] on [campaignlek].[tFixedDiscountActionIncludeCategory]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeCategory] ADD CONSTRAINT [PK_tFixedDiscountActionIncludeCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountActionIncludeProduct]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountActionIncludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountActionIncludeProduct] on [campaignlek].[tFixedDiscountActionIncludeProduct]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeProduct] ADD CONSTRAINT [PK_tFixedDiscountActionIncludeProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tPercentageDiscountCartAction]'
GO
CREATE TABLE [campaignlek].[tPercentageDiscountCartAction]
(
[CartActionId] [int] NOT NULL,
[DiscountAmount] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPercentageDiscountAction] on [campaignlek].[tPercentageDiscountCartAction]'
GO
ALTER TABLE [campaignlek].[tPercentageDiscountCartAction] ADD CONSTRAINT [PK_tPercentageDiscountAction] PRIMARY KEY CLUSTERED  ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]'
GO
CREATE TABLE [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPercentagePriceDiscountActionExcludeBrand] on [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionExcludeBrand] ADD CONSTRAINT [PK_tPercentagePriceDiscountActionExcludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]'
GO
CREATE TABLE [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPercentagePriceDiscountActionIncludeBrand] on [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionIncludeBrand] ADD CONSTRAINT [PK_tPercentagePriceDiscountActionIncludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tProductRegistryRestrictionBrand]'
GO
CREATE TABLE [lekmer].[tProductRegistryRestrictionBrand]
(
[ProductRegistryId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[RestrictionReason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductRegistryRestrictionBrand] on [lekmer].[tProductRegistryRestrictionBrand]'
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionBrand] ADD CONSTRAINT [PK_tProductRegistryRestrictionBrand] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tProductRegistryRestrictionCategory]'
GO
CREATE TABLE [lekmer].[tProductRegistryRestrictionCategory]
(
[ProductRegistryId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[RestrictionReason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductRegistryRestrictionCategory] on [lekmer].[tProductRegistryRestrictionCategory]'
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionCategory] ADD CONSTRAINT [PK_tProductRegistryRestrictionCategory] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tProductRegistryRestrictionProduct]'
GO
CREATE TABLE [lekmer].[tProductRegistryRestrictionProduct]
(
[ProductRegistryId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[RestrictionReason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductRegistryRestrictionProduct] on [lekmer].[tProductRegistryRestrictionProduct]'
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionProduct] ADD CONSTRAINT [PK_tProductRegistryRestrictionProduct] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[fnGetPriceListIdOfItemWithLowestPrice]'
GO

ALTER FUNCTION [product].[fnGetPriceListIdOfItemWithLowestPrice]
(
	@CurrencyId INT,
	@ProductId INT,
	@PriceListRegistryId INT,
	@CustomerId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @PriceListId INT
	
	DECLARE @DateNow DATETIME
	SET @DateNow = GETDATE()
	
	SELECT TOP(1)
		@PriceListId = pli.PriceListId		
	FROM
		product.tPriceListItem AS pli
		INNER JOIN product.tPriceList AS pl ON pl.PriceListId = pli.PriceListId
		LEFT JOIN product.tPriceListCustomerGroup AS cgp ON cgp.PriceListId = pl.PriceListId
	WHERE
		pl.PriceListStatusId = 0
		AND pl.PriceListRegistryId = @PriceListRegistryId
		AND pl.CurrencyId = @CurrencyId
		AND (pl.StartDateTime IS NULL OR pl.StartDateTime <= @DateNow)
		AND (pl.EndDateTime   IS NULL OR @DateNow <= pl.EndDateTime)
		AND pli.ProductId = @ProductId
		AND ( -- if customer is not defined then should be available all pricelists that are not assigned to any customer group
			cgp.CustomerGroupId IS NULL
			OR
			(
				@CustomerId IS NOT NULL
				AND
				cgp.CustomerGroupId IN (
					SELECT
						cg.CustomerGroupId
					FROM
						customer.tCustomerGroup AS cg
						INNER JOIN customer.tCustomerGroupCustomer AS cgc ON cgc.CustomerGroupId = cg.CustomerGroupId
					WHERE
						cg.StatusId = 0
						AND cgc.CustomerId = @CustomerId
				)
			)
		)
	ORDER BY
		pli.PriceIncludingVat

	RETURN @PriceListId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeBrandDeleteAll]'
GO
CREATE PROCEDURE [addon].[pCartContainsConditionExcludeBrandDeleteAll]
	@ConditionId int
AS 
BEGIN 
	DELETE
		[addon].[tCartContainsConditionExcludeBrand]
	WHERE
		ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeBrandDeleteAll]'
GO
CREATE PROCEDURE [addon].[pCartContainsConditionIncludeBrandDeleteAll]
	@ConditionId INT
AS
BEGIN
	DELETE
		[addon].[tCartContainsConditionIncludeBrand]
	WHERE
		ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeBrandGetIdAll]'
GO
CREATE PROCEDURE [addon].[pCartContainsConditionExcludeBrandGetIdAll]
	@ConditionId	INT
AS
BEGIN
	SELECT 
		ceb.[BrandId]
	FROM 
		[addon].[tCartContainsConditionExcludeBrand] ceb 
	WHERE 
		ceb.[ConditionId] = @ConditionId 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionExcludeBrandInsert]'
GO
CREATE PROCEDURE [addon].[pCartContainsConditionExcludeBrandInsert]
	@ConditionId	INT,
	@BrandId		INT
AS
BEGIN
	INSERT [addon].[tCartContainsConditionExcludeBrand] (
		ConditionId,
		BrandId
	)
	VALUES (
		@ConditionId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeBrandGetIdAll]'
GO
CREATE PROCEDURE [addon].[pCartContainsConditionIncludeBrandGetIdAll]
	@ConditionId	INT
AS
BEGIN
	SELECT
		cib.[BrandId]
	FROM
		[addon].[tCartContainsConditionIncludeBrand] cib
	WHERE
		cib.[ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartContainsConditionIncludeBrandInsert]'
GO
CREATE PROCEDURE [addon].[pCartContainsConditionIncludeBrandInsert]
	@ConditionId	INT,
	@BrandId		INT
AS
BEGIN
	INSERT [addon].[tCartContainsConditionIncludeBrand] (
		ConditionId,
		BrandId
	)
	VALUES (
		@ConditionId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pPercentagePriceDiscountActionExcludeBrandDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pPercentagePriceDiscountActionExcludeBrandDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tPercentagePriceDiscountActionExcludeBrand]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pPercentagePriceDiscountActionIncludeBrandDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pPercentagePriceDiscountActionIncludeBrandDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tPercentagePriceDiscountActionIncludeBrand]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartValueRangeConditionCurrencyDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionCurrencyDeleteAll]
	@ConditionId INT
AS 
BEGIN
	DELETE FROM [campaignlek].[tCartValueRangeConditionCurrency]
	WHERE ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartValueRangeConditionCurrencySave]'
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionCurrencySave]
	@ConditionId	INT,
	@CurrencyId		INT,
	@ValueFrom		DECIMAL(16,2),
	@ValueTo		DECIMAL(16,2)
AS 
BEGIN 
	INSERT [campaignlek].[tCartValueRangeConditionCurrency] ( 
		ConditionId,
		CurrencyId,
		MonetaryValueFrom,
		MonetaryValueTo
	)
	VALUES (
		@ConditionId,
		@CurrencyId,
		@ValueFrom,
		@ValueTo
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartValueRangeConditionDelete]'
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionDelete]
	@ConditionId INT
AS 
BEGIN
	EXEC [campaignlek].[pCartValueRangeConditionCurrencyDeleteAll] @ConditionId
	
	DELETE FROM [campaignlek].[tCartValueRangeCondition]
	WHERE ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartValueRangeConditionSave]'
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionSave]
	@ConditionId INT
AS 
BEGIN
		IF EXISTS (SELECT 1 FROM [campaignlek].[tCartValueRangeCondition] WHERE ConditionId = @ConditionId)
			RETURN
		
		INSERT [campaignlek].[tCartValueRangeCondition] ( 
			ConditionId
		)
		VALUES (
			@ConditionId
		)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionCurrencyDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionCurrencyDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountActionCurrency]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionCurrencyInsert]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionCurrencyInsert]
	@ProductActionId	INT,
	@CurrencyId			INT,
	@Value				DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionCurrency] (
		ProductActionId,
		CurrencyId,
		Value
	)
	VALUES (
		@ProductActionId,
		@CurrencyId,
		@Value
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeProductDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeProductDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountActionExcludeProduct]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeProductDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeProductDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountActionIncludeProduct]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeCategoryDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeCategoryDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountActionExcludeCategory]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeCategoryDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeCategoryDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountActionIncludeCategory]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeBrandDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeBrandDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountActionExcludeBrand]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeBrandDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeBrandDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountActionIncludeBrand]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeBrandGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tFixedDiscountActionExcludeBrand]
	WHERE 
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeBrandInsert]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeBrandInsert]
	@ProductActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionExcludeBrand] (
		ProductActionId,
		BrandId
	)
	VALUES (
		@ProductActionId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeCategoryGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		CategoryId
	FROM 
		[campaignlek].[tFixedDiscountActionExcludeCategory]
	WHERE 
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeCategoryInsert]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeCategoryInsert]
	@ProductActionId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionExcludeCategory] (
		ProductActionId,
		CategoryId
	)
	VALUES (
		@ProductActionId,
		@CategoryId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeProductInsert]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeProductInsert]
	@ProductActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionExcludeProduct] (
		ProductActionId,
		ProductId
	)
	VALUES (
		@ProductActionId,
		@ProductId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeBrandGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tFixedDiscountActionIncludeBrand]
	WHERE 
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeBrandInsert]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeBrandInsert]
	@ProductActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionIncludeBrand] (
		ProductActionId,
		BrandId
	)
	VALUES (
		@ProductActionId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeCategoryGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		CategoryId
	FROM 
		[campaignlek].[tFixedDiscountActionIncludeCategory]
	WHERE 
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeCategoryInsert]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeCategoryInsert]
	@ProductActionId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionIncludeCategory] (
		ProductActionId,
		CategoryId
	)
	VALUES (
		@ProductActionId,
		@CategoryId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeProductInsert]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeProductInsert]
	@ProductActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionIncludeProduct] (
		ProductActionId,
		ProductId
	)
	VALUES (
		@ProductActionId,
		@ProductId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionSave]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT
AS
BEGIN
	UPDATE
		[campaignlek].[tFixedDiscountAction]
	SET
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		ProductActionId = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tFixedDiscountAction] (
			ProductActionId,
			IncludeAllProducts
		)
		VALUES (
			@ProductActionId,
			@IncludeAllProducts
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pPercentageDiscountCartActionDelete]'
GO
CREATE PROCEDURE [campaignlek].[pPercentageDiscountCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	DELETE FROM [campaignlek].[tPercentageDiscountCartAction]
	WHERE CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pPercentageDiscountCartActionSave]'
GO
CREATE PROCEDURE [campaignlek].[pPercentageDiscountCartActionSave]
	@CartActionId INT,
	@DiscountAmount DECIMAL(16,2)
AS
BEGIN
	UPDATE
		[campaignlek].[tPercentageDiscountCartAction]
	SET
		DiscountAmount = @DiscountAmount
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tPercentageDiscountCartAction] (
			CartActionId,
			DiscountAmount
		)
		VALUES (
			@CartActionId,
			@DiscountAmount
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pPercentagePriceDiscountActionExcludeBrandGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pPercentagePriceDiscountActionExcludeBrandGetIdAll]
	@ActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tPercentagePriceDiscountActionExcludeBrand]
	WHERE 
		ProductActionId = @ActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pPercentagePriceDiscountActionExcludeBrandInsert]'
GO
CREATE PROCEDURE [campaignlek].[pPercentagePriceDiscountActionExcludeBrandInsert]
	@ProductActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tPercentagePriceDiscountActionExcludeBrand] (
		ProductActionId,
		BrandId
	)
	VALUES (
		@ProductActionId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pPercentagePriceDiscountActionIncludeBrandGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pPercentagePriceDiscountActionIncludeBrandGetIdAll]
	@ActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tPercentagePriceDiscountActionIncludeBrand]
	WHERE 
		ProductActionId = @ActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pPercentagePriceDiscountActionIncludeBrandInsert]'
GO
CREATE PROCEDURE [campaignlek].[pPercentagePriceDiscountActionIncludeBrandInsert]
	@ProductActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tPercentagePriceDiscountActionIncludeBrand] (
		ProductActionId,
		BrandId
	)
	VALUES (
		@ProductActionId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tProductPrice]'
GO
CREATE TABLE [export].[tProductPrice]
(
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[PriceIncludingVat] [decimal] (16, 2) NOT NULL,
[PriceExcludingVat] [decimal] (16, 2) NOT NULL,
[DiscountPriceIncludingVat] [decimal] (16, 2) NULL,
[DiscountPriceExcludingVat] [decimal] (16, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductPrice] on [export].[tProductPrice]'
GO
ALTER TABLE [export].[tProductPrice] ADD CONSTRAINT [PK_tProductPrice] PRIMARY KEY CLUSTERED  ([ChannelId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[vProductPrice]'
GO

CREATE VIEW [export].[vProductPrice]
AS
	SELECT
		pp.[ChannelId] 'ProductPrice.ChannelId',
		pp.[ProductId] 'ProductPrice.ProductId',
		pp.[PriceIncludingVat] 'ProductPrice.PriceIncludingVat',
		pp.[PriceExcludingVat] 'ProductPrice.PriceExcludingVat',
		pp.[DiscountPriceIncludingVat] 'ProductPrice.DiscountPriceIncludingVat',
		pp.[DiscountPriceExcludingVat] 'ProductPrice.DiscountPriceExcludingVat'
	FROM 
		export.tProductPrice  AS pp
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pProductPriceGetAll]'
GO
CREATE PROCEDURE [export].[pProductPriceGetAll]
	@ChannelId INT
AS 
BEGIN 
	SELECT
		*
	FROM
		[export].[vProductPrice] pp
	WHERE
		pp.[ProductPrice.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pProductPriceSave]'
GO
CREATE PROCEDURE [export].[pProductPriceSave]
	@ChannelId	INT,
	@ProductId	INT,
	@PriceIncludingVat DECIMAL (16, 2),
	@PriceExcludingVat DECIMAL (16, 2),
	@DiscountPriceIncludingVat DECIMAL (16, 2),
	@DiscountPriceExcludingVat DECIMAL (16, 2)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[export].[tProductPrice]
	SET
		[PriceIncludingVat] = @PriceIncludingVat,
		[PriceExcludingVat] = @PriceExcludingVat,
		[DiscountPriceIncludingVat] = @DiscountPriceIncludingVat,
		[DiscountPriceExcludingVat] = @DiscountPriceExcludingVat
	WHERE
		[ChannelId] = @ChannelId
		AND
		[ProductId] = @ProductId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [export].[tProductPrice]
		(
			[ChannelId],
			[ProductId],
			[PriceIncludingVat],
			[PriceExcludingVat],
			[DiscountPriceIncludingVat],
			[DiscountPriceExcludingVat]
		)
		VALUES
		(
			@ChannelId,
			@ProductId,
			@PriceIncludingVat,
			@PriceExcludingVat,
			@DiscountPriceIncludingVat,
			@DiscountPriceExcludingVat
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionCategoryGetAll]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionCategoryGetAll]
AS
BEGIN
	SELECT DISTINCT src.CategoryId, rc.ProductRegistryId
	FROM [lekmer].[tProductRegistryRestrictionCategory] rc
		 CROSS APPLY [product].[fnGetSubCategories] (rc.CategoryId) src
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionBrandDelete]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionBrandDelete]
	@BrandId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tProductRegistryRestrictionBrand]
	WHERE 
		[BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryGetAll]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryGetAll]
AS
BEGIN
	SELECT
		*
	FROM 
		[product].[tProductRegistry]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionBrandGetAll]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionBrandGetAll]
AS
BEGIN
	SELECT BrandId, ProductRegistryId 
	FROM [lekmer].[tProductRegistryRestrictionBrand]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionProductGetAll]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionProductGetAll]
AS
BEGIN
	SELECT ProductId, ProductRegistryId 
	FROM [lekmer].[tProductRegistryRestrictionProduct]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryProductDeleteByProduct]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductDeleteByProduct]
	@ProductId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN 
	DELETE 
		[product].[tProductRegistryProduct]
	WHERE 
		[ProductId] = @ProductId
		AND [ProductRegistryId] IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionBrandGetAllByBrand]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionBrandGetAllByBrand]
	@BrandId		INT
AS
BEGIN
	SELECT
		*
	FROM 
		[lekmer].[tProductRegistryRestrictionBrand]
	WHERE
		[BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionBrandSave]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionBrandSave]
	@ProductRegistryId	INT,
	@BrandId			INT,
	@RestrictionReason	NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME
AS
BEGIN
	INSERT INTO [lekmer].[tProductRegistryRestrictionBrand] (
		ProductRegistryId,
		BrandId,
		RestrictionReason,
		UserId,
		CreatedDate
	)
	VALUES (
		@ProductRegistryId,
		@BrandId,
		@RestrictionReason,
		@UserId,
		@CreatedDate
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionCategoryDelete]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionCategoryDelete]
	@CategoryId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tProductRegistryRestrictionCategory]
	WHERE 
		[CategoryId] = @CategoryId
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionCategoryGetAllByCategory]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionCategoryGetAllByCategory]
	@CategoryId		INT
AS
BEGIN
	SELECT
		*
	FROM 
		[lekmer].[tProductRegistryRestrictionCategory]
	WHERE
		[CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionCategorySave]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionCategorySave]
	@ProductRegistryId	INT,
	@CategoryId			INT,
	@RestrictionReason	NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME
AS
BEGIN
	INSERT INTO [lekmer].[tProductRegistryRestrictionCategory] (
		ProductRegistryId,
		CategoryId,
		RestrictionReason,
		UserId,
		CreatedDate
	)
	VALUES (
		@ProductRegistryId,
		@CategoryId,
		@RestrictionReason,
		@UserId,
		@CreatedDate
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionProductDelete]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionProductDelete]
	@ProductId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tProductRegistryRestrictionProduct]
	WHERE 
		[ProductId] = @ProductId
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionProductGetAllByProduct]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionProductGetAllByProduct]
	@ProductId		INT
AS
BEGIN
	SELECT
		*
	FROM 
		[lekmer].[tProductRegistryRestrictionProduct]
	WHERE
		[ProductId] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryRestrictionProductSave]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionProductSave]
	@ProductRegistryId	INT,
	@ProductId			INT,
	@RestrictionReason	NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME
AS
BEGIN
	INSERT INTO [lekmer].[tProductRegistryRestrictionProduct] (
		ProductRegistryId,
		ProductId,
		RestrictionReason,
		UserId,
		CreatedDate
	)
	VALUES (
		@ProductRegistryId,
		@ProductId,
		@RestrictionReason,
		@UserId,
		@CreatedDate
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[fnGetCartContainsConditionAffectedProducts]'
GO
CREATE FUNCTION [integration].[fnGetCartContainsConditionAffectedProducts](
	@ConditionId INT
)
RETURNS @tAffectedProducts TABLE (ProductId INT)
AS 
BEGIN
	INSERT INTO @tAffectedProducts (ProductId)
	
	      -- include products from categories without subcategories
	
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN addon.tCartContainsConditionIncludeCategory ic ON ic.CategoryId = p.CategoryId
		WHERE
			ic.ConditionId = @ConditionId
			AND
			ic.IncludeSubcategories = 0
		
	UNION -- include products from categories with subcategories
	
		SELECT
			ProductId
		FROM
			addon.tCartContainsConditionIncludeCategory ic
			CROSS APPLY [product].[fnGetSubCategories] (ic.CategoryId) sc
			INNER JOIN product.tProduct p ON p.CategoryId = sc.CategoryId
		WHERE
			ic.ConditionId = @ConditionId
			AND
			ic.IncludeSubcategories = 1
		
	UNION -- include products
	
		SELECT
			ProductId
		FROM
			addon.tCartContainsConditionIncludeProduct ip
		WHERE
			ip.ConditionId = @ConditionId
			
	EXCEPT -- exclude products from categories without subcategories
		
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN addon.tCartContainsConditionExcludeCategory ec ON ec.CategoryId = p.CategoryId
		WHERE
			ec.ConditionId = @ConditionId
			AND
			ec.IncludeSubcategories = 0
		
	EXCEPT -- exclude products from categories with subcategories
	
		SELECT
			ProductId
		FROM
			addon.tCartContainsConditionExcludeCategory ec
			CROSS APPLY [product].[fnGetSubCategories] (ec.CategoryId) sc
			INNER JOIN product.tProduct p ON p.CategoryId = sc.CategoryId
		WHERE
			ec.ConditionId = @ConditionId
			AND
			ec.IncludeSubcategories = 1
		
	EXCEPT -- exclude products
	
		SELECT
			ProductId
		FROM
			addon.tCartContainsConditionExcludeProduct ep
		WHERE
			ep.ConditionId = @ConditionId

	RETURN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[fnGetCartItemPriceActionAffectedProducts]'
GO
CREATE FUNCTION [integration].[fnGetCartItemPriceActionAffectedProducts](
	@CartActionId INT
)
RETURNS @tAffectedProducts TABLE (ProductId INT)
AS 
BEGIN
	INSERT INTO @tAffectedProducts (ProductId)
	
	      -- include products from categories
	
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN addon.tCartItemPriceActionIncludeCategory ic ON ic.CategoryId = p.CategoryId
		WHERE
			ic.CartActionId = @CartActionId

	UNION -- include products
	
		SELECT
			ProductId
		FROM
			addon.tCartItemPriceActionIncludeProduct ip
		WHERE
			ip.CartActionId = @CartActionId

	EXCEPT -- exclude products from categories
		
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN addon.tCartItemPriceActionExcludeCategory ec ON ec.CategoryId = p.CategoryId
		WHERE
			ec.CartActionId = @CartActionId

	EXCEPT -- exclude products
	
		SELECT
			ProductId
		FROM
			addon.tCartItemPriceActionExcludeProduct ep
		WHERE
			ep.CartActionId = @CartActionId

	RETURN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[fnGetPercentagePriceDiscountActionAffectedProducts]'
GO
CREATE FUNCTION [integration].[fnGetPercentagePriceDiscountActionAffectedProducts](
	@ProductActionId INT
)
RETURNS @tAffectedProducts TABLE (ProductId INT)
AS 
BEGIN
	INSERT INTO @tAffectedProducts (ProductId)
	
	      -- include products from categories
	
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN campaign.tPercentagePriceDiscountActionIncludeCategory ic ON ic.CategoryId = p.CategoryId
		WHERE
			ic.ProductActionId = @ProductActionId

	UNION -- include products
	
		SELECT
			ProductId
		FROM
			campaign.tPercentagePriceDiscountActionIncludeProduct ip
		WHERE
			ip.ProductActionId = @ProductActionId

	EXCEPT -- exclude products from categories
		
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN campaign.tPercentagePriceDiscountActionExcludeCategory ec ON ec.CategoryId = p.CategoryId
		WHERE
			ec.ProductActionId = @ProductActionId

	EXCEPT -- exclude products
	
		SELECT
			ProductId
		FROM
			campaign.tPercentagePriceDiscountActionExcludeProduct ep
		WHERE
			ep.ProductActionId = @ProductActionId

	RETURN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartContainsConditionDelete]'
GO
ALTER PROCEDURE [addon].[pCartContainsConditionDelete]
	@ConditionId INT
AS
BEGIN
	EXEC [addon].[pCartContainsConditionIncludeProductDeleteAll] @ConditionId
	EXEC [addon].[pCartContainsConditionExcludeProductDeleteAll] @ConditionId
	
	EXEC [addon].[pCartContainsConditionIncludeCategoryDeleteAll] @ConditionId
	EXEC [addon].[pCartContainsConditionExcludeCategoryDeleteAll] @ConditionId
	
	EXEC [addon].[pCartContainsConditionIncludeBrandDeleteAll] @ConditionId
	EXEC [addon].[pCartContainsConditionExcludeBrandDeleteAll] @ConditionId
	
	DELETE [addon].[tCartContainsCondition]
	WHERE ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pPercentagePriceDiscountActionDelete]'
GO
ALTER PROCEDURE [campaign].[pPercentagePriceDiscountActionDelete]
	@ProductActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pPercentagePriceDiscountActionIncludeBrandDeleteAll] @ProductActionId
	EXEC [campaignlek].[pPercentagePriceDiscountActionExcludeBrandDeleteAll] @ProductActionId

	EXEC [campaign].[pPercentagePriceDiscountActionIncludeCategoryDeleteAll] @ProductActionId
	EXEC [campaign].[pPercentagePriceDiscountActionExcludeCategoryDeleteAll] @ProductActionId

	EXEC [campaign].[pPercentagePriceDiscountActionIncludeProductDeleteAll] @ProductActionId
	EXEC [campaign].[pPercentagePriceDiscountActionExcludeProductDeleteAll] @ProductActionId

	DELETE FROM [campaign].[tPercentagePriceDiscountAction]
	WHERE ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionDelete]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionDelete]
	@ProductActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pFixedDiscountActionIncludeBrandDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedDiscountActionExcludeBrandDeleteAll] @ProductActionId

	EXEC [campaignlek].[pFixedDiscountActionIncludeCategoryDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedDiscountActionExcludeCategoryDeleteAll] @ProductActionId

	EXEC [campaignlek].[pFixedDiscountActionIncludeProductDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedDiscountActionExcludeProductDeleteAll] @ProductActionId
	
	EXEC [campaignlek].[pFixedDiscountActionCurrencyDeleteAll] @ProductActionId

	DELETE FROM [campaignlek].[tFixedDiscountAction]
	WHERE ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeProductGetIdAllSecure]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeProductGetIdAllSecure]
	@ProductActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tFixedDiscountActionExcludeProduct] fdaep
		INNER JOIN [product].[tProduct] p ON fdaep.[ProductId] = p.[ProductId]
	WHERE
		fdaep.[ProductActionId] = @ProductActionId
		AND p.[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeProductGetIdAllSecure]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeProductGetIdAllSecure]
	@ProductActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tFixedDiscountActionIncludeProduct] fdaip
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = fdaip.[ProductId]
	WHERE
		fdaip.[ProductActionId] = @ProductActionId
		AND p.[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pProductPriceDeleteByIdList]'
GO

CREATE PROCEDURE [export].[pProductPriceDeleteByIdList]
	@ChannelId	INT,
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE
		pp
	FROM
		[export].[tProductPrice] pp
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) AS pl ON pl.[ID] = pp.[ProductId]
	WHERE 
		pp.[ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pTagProductSale]'
GO
CREATE PROCEDURE [integration].[pTagProductSale]

AS
BEGIN
	DECLARE @SalesTagId INT
		SET @SalesTagId = (SELECT TagId FROM lekmer.tTag WHERE Value = 'Sale')

	DECLARE @ActiveCampaigns TABLE 
	(
		CampaignId INT PRIMARY KEY WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO @ActiveCampaigns (CampaignId)
	SELECT
		c.CampaignId
	FROM 
		campaign.tCampaign c
	WHERE
		c.EndDate > GETDATE()
		AND c.StartDate < GETDATE()
		AND CampaignStatusId = 0

	DECLARE @ProductsToBeTaged TABLE 
	(
		ProductId INT PRIMARY KEY WITH (IGNORE_DUP_KEY = ON)
	)
	
	DECLARE @IncludeAllProducts BIT
	
	/*--------------------------------------------*/
	/*CartContainsCondition*/

	DECLARE @CartContainsConditionId INT
	
	DECLARE CartContainsCondition_cursor CURSOR FOR 
	SELECT
		con.ConditionId,
		ccc.IncludeAllProducts
	FROM
		addon.tCartContainsCondition ccc
		INNER JOIN campaign.tCondition con ON con.ConditionId = ccc.ConditionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = con.CampaignId

	OPEN CartContainsCondition_cursor

	FETCH NEXT FROM CartContainsCondition_cursor INTO @CartContainsConditionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetCartContainsConditionAffectedProducts (@CartContainsConditionId)
	
		FETCH NEXT FROM CartContainsCondition_cursor INTO @CartContainsConditionId, @IncludeAllProducts
	END

	CLOSE CartContainsCondition_cursor;
	DEALLOCATE CartContainsCondition_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*CartItemPriceAction*/

	DECLARE @CartItemPriceActionId INT
	
	DECLARE CartItemPriceAction_cursor CURSOR FOR 
	SELECT
		ca.CartActionId,
		cipa.IncludeAllProducts
	FROM
		addon.tCartItemPriceAction cipa
		INNER JOIN campaign.tCartAction ca ON ca.CartActionId = cipa.CartActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = ca.CampaignId

	OPEN CartItemPriceAction_cursor

	FETCH NEXT FROM CartItemPriceAction_cursor INTO @CartItemPriceActionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetCartItemPriceActionAffectedProducts (@CartItemPriceActionId)
	
		FETCH NEXT FROM CartItemPriceAction_cursor INTO @CartItemPriceActionId, @IncludeAllProducts
	END

	CLOSE CartItemPriceAction_cursor;
	DEALLOCATE CartItemPriceAction_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*PercentagePriceDiscountAction*/

	DECLARE @PercentagePriceDiscountActionId INT
	
	DECLARE PercentagePriceDiscountAction_cursor CURSOR FOR 
	SELECT
		pa.ProductActionId,
		ppda.IncludeAllProducts
	FROM
		campaign.tPercentagePriceDiscountAction ppda
		INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ppda.ProductActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = pa.CampaignId

	OPEN PercentagePriceDiscountAction_cursor

	FETCH NEXT FROM PercentagePriceDiscountAction_cursor INTO @PercentagePriceDiscountActionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetPercentagePriceDiscountActionAffectedProducts (@PercentagePriceDiscountActionId)
	
		FETCH NEXT FROM PercentagePriceDiscountAction_cursor INTO @PercentagePriceDiscountActionId, @IncludeAllProducts
	END

	CLOSE PercentagePriceDiscountAction_cursor;
	DEALLOCATE PercentagePriceDiscountAction_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*ProductDiscountAction*/
	
	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT 
		ProductId
	FROM 
		lekmer.tProductDiscountActionItem pdai
		INNER JOIN lekmer.tProductDiscountAction pda ON pda.ProductActionId = pdai.ProductActionId
		INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = pda.ProductActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = pa.CampaignId

	/*--------------------------------------------*/

UpdateTags:
	
	BEGIN TRY
	BEGIN TRANSACTION

		DELETE FROM lekmer.tProductTag WHERE TagId = @SalesTagId

		INSERT INTO lekmer.tProductTag(ProductId, TagId)
		SELECT
			p.ProductId,
			@SalesTagId
		FROM
			@ProductsToBeTaged p												
				
	COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH

	RETURN

	/*--------------------------------------------*/

AllProductsAffected:

	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT ProductId FROM product.tProduct

	GOTO UpdateTags

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryProductAdd]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductAdd]
	@ProductRegistryProducts [lekmer].[ProductRegistryProducts] READONLY
AS
BEGIN
	DECLARE @ProductRestriction		 TABLE(ProductId INT,  ProductRegistryId INT)
	DECLARE @BrandRestriction		 TABLE(BrandId INT,	   ProductRegistryId INT)
	DECLARE @CategoryRestriction	 TABLE(CategoryId INT, ProductRegistryId INT)


	-- Get All Product Restrictions.
	INSERT INTO @ProductRestriction (ProductId, ProductRegistryId)
	EXEC [lekmer].[pProductRegistryRestrictionProductGetAll]

	-- Get All Brand Restrictions.
	INSERT INTO @BrandRestriction (BrandId, ProductRegistryId)
	EXEC [lekmer].[pProductRegistryRestrictionBrandGetAll]

	-- Get All Category Restrictions.	
	INSERT INTO @CategoryRestriction (CategoryId, ProductRegistryId)
	EXEC [lekmer].[pProductRegistryRestrictionCategoryGetAll]


	DECLARE @ProductIdsToInsert TABLE(ProductId INT, ProductRegistryId INT)
	INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
	SELECT ProductId, ProductRegistryId FROM @ProductRegistryProducts


	-- Remove products that have any restrictions.
	DELETE pId FROM @ProductIdsToInsert pId
	INNER JOIN product.tProduct p ON p.ProductId = pId.ProductId
	INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
	WHERE EXISTS (SELECT 1 FROM @ProductRestriction WHERE ProductId = pId.ProductId AND ProductRegistryId = pId.ProductRegistryId)
		  OR EXISTS (SELECT 1 FROM @BrandRestriction WHERE BrandId = lp.BrandId AND ProductRegistryId = pId.ProductRegistryId)
		  OR EXISTS (SELECT 1 FROM @CategoryRestriction WHERE CategoryId = p.CategoryId AND ProductRegistryId = pId.ProductRegistryId)
		  OR EXISTS (SELECT 1 FROM product.tProductRegistryProduct WHERE ProductId = pId.ProductId AND ProductRegistryId = pId.ProductRegistryId)


	-- Add product to tProductRegistryProduct table.
	INSERT INTO product.tProductRegistryProduct (ProductId, ProductRegistryId)
	SELECT ProductId, ProductRegistryId FROM @ProductIdsToInsert
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_FokRestrictionsProducts]'
GO
ALTER PROCEDURE [integration].[usp_FokRestrictionsProducts]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION
		--------------------------------------------------
		--- Update tProductTranslation with new products
		--------------------------------------------------
		-- NO
		INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
		SELECT p.productid, 1000001 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT productid FROM [product].[tProductTranslation] WHERE LanguageId = 1000001)
		
		-- DK
		INSERT INTO product.tProductTranslation (ProductId, LanguageId)
		SELECT p.productid, 1000002 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000002)

		-- FI		
		INSERT INTO product.tProductTranslation(ProductId, LanguageId)
		SELECT p.productid, 1000003 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000003)



		----------------------------------------------------------------------------
		--- Insert not translated products into the tProductRegistryRestrictionProduct
		----------------------------------------------------------------------------
		-- NO
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			2,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000001 -- NO
			AND Title IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 2)

		-- DK
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			3,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000002 -- DK
			AND Title IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 3)

		-- FI
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			4,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000003 -- FI
			AND Title IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 4)



		----------------------------------------------------------------------------
		--- Insert tProductRegistryRestrictionBrand
		----------------------------------------------------------------------------
		--------------------- BRANDS ALLA KANALER (NO, DK, FI) ---------------------
		DECLARE @RestrictionBrand TABLE (BrandId INT, ProductRegistryId INT)
		INSERT INTO @RestrictionBrand (BrandId, ProductRegistryId) 
		VALUES 
			(1000232, 2), (1000235, 2), (1000347, 2),
			(1000232, 3), (1000235, 3),	(1000347, 3), (1000357, 3),
			(1000232, 4), (1000235, 4),	(1000347, 4)
	
		INSERT INTO	[lekmer].[tProductRegistryRestrictionBrand] (
			ProductRegistryId,
			BrandId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			ProductRegistryId,
			BrandId,
			'Brand Restriction',
			NULL,
			GETDATE()
		FROM
			@RestrictionBrand
		WHERE NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionBrand] prrb
						  WHERE BrandId = prrb.BrandId AND prrb.ProductRegistryId = ProductRegistryId)



		----------------------------------------------------------------------------
		--- Insert tProductRegistryRestrictionCategory
		----------------------------------------------------------------------------
		DECLARE @RestrictionCategory TABLE (CategoryId INT, ProductRegistryId INT)
		INSERT INTO @RestrictionCategory (CategoryId, ProductRegistryId) 
		VALUES 
			(1000842, 2), (1000842, 3), (1000842, 4), --böcker och tidningar  -- C_30-119
			(1000985, 2), (1000985, 3), (1000985, 4), --böcker och tidningar  -- C_30-124
			(1000590, 2), (1000590, 3), (1000590, 4), --Barnspel
			(1000989, 2), (1000989, 3), (1000989, 4), --Vuxenspel
			(1000996, 2), (1000996, 3), (1000996, 4), --Familjespel
			(1000997, 2), (1000997, 3), (1000997, 4), --Tv-spel och datorspel
			(1000999, 2), (1000999, 3), (1000999, 4), --Utomhusspel
			(1001000, 2), (1001000, 3), (1001000, 4), --Strategispel
			(1001001, 2), (1001001, 3), (1001001, 4), --Pocketspel
			(1001002, 2), (1001002, 3), (1001002, 4), --Klassiker
			(1001005, 2), (1001005, 3), (1001005, 4), --Resespel
			(1001006, 2), (1001006, 3), (1001006, 4), --Memoryspel
			(1001008, 2), (1001008, 3), (1001008, 4), --Brädspel
			(1001009, 2), (1001009, 3), (1001009, 4), --Barnpussel
			(1001010, 2), (1001010, 3), (1001010, 4), --Startegispel
			(1001014, 2), (1001014, 3), (1001014, 4)  --Strategispel
			
		INSERT INTO	[lekmer].[tProductRegistryRestrictionCategory] (
			ProductRegistryId,
			CategoryId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			ProductRegistryId,
			CategoryId,
			'Category Restriction',
			NULL,
			GETDATE()
		FROM
			@RestrictionCategory
		WHERE NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionCategory] prrc
						  WHERE CategoryId = prrc.CategoryId AND prrc.ProductRegistryId = ProductRegistryId)		



		---------------------------------------------------------------------
		---- Get Product Ids that need add to product.tProductRegistryProduct.
		----------------------------------------------------------------------
		DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]

		-- NO
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 2 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 2)

		-- DK
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 3 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 3)
		
		-- FI
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 4 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 4)

		
																
		--------------------------------------------------------------------------------
		---- Delete items from tProductRegistryProduct and from list products to insert.
		--------------------------------------------------------------------------------
		-- By Product restroction
		DELETE prp
		FROM [product].[tProductRegistryProduct] prp
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = prp.ProductRegistryId 
																		 AND prrp.ProductId = prp.ProductId
		-- By Brand restroction
		DELETE prp 
		FROM [product].[tProductRegistryProduct] prp
			 INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = prp.[ProductId]
			 INNER JOIN [lekmer].[tProductRegistryRestrictionBrand] prrb ON prrb.[BrandId] = lp.[BrandId] 
																			AND prrb.[ProductRegistryId] = prp.[ProductRegistryId]
		-- By Category restroction
		DECLARE @CategoryRestriction TABLE(CategoryId INT, ProductRegistryId INT)
		INSERT INTO @CategoryRestriction (CategoryId, ProductRegistryId)
		EXEC [lekmer].[pProductRegistryRestrictionCategoryGetAll]
	
		DELETE prp 
		FROM [product].[tProductRegistryProduct] prp
			 INNER JOIN [product].[tProduct] p ON p.[ProductId] = prp.[ProductId]
			 INNER JOIN @CategoryRestriction c ON c.[CategoryId] = p.[CategoryId]
											   AND c.[ProductRegistryId] = prp.[ProductRegistryId]


		DELETE pIds
		FROM @ProductIdsToInsert pIds
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = pIds.ProductRegistryId 
																		 AND prrp.ProductId = pIds.ProductId


		EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert	
				
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			'',
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)
	END CATCH
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryProductInsert]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductInsert]
	@ProductId			INT,
	@BrandId			INT,
	@CategoryId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN
	-- Get Product Ids that need add to product.tProductRegistryProduct.
	DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]

	IF @ProductId IS NOT NULL
	BEGIN
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT @ProductId, ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter)
	END

	IF @BrandId IS NOT NULL
	BEGIN
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
			SELECT p.ProductId, pr.ID
			FROM (SELECT ProductId FROM lekmer.tLekmerProduct WHERE BrandId = @BrandId) p,
				 [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	IF @CategoryId IS NOT NULL
	BEGIN
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
			SELECT p.ProductId, pr.ID
			FROM (SELECT ProductId FROM product.tProduct WHERE CategoryId IN (SELECT CategoryId FROM [product].[fnGetSubCategories] (@CategoryId))) p,
				 [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END


	EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandDelete]'
GO

ALTER PROCEDURE [lekmer].[pBrandDelete]
	@BrandId	INT
AS
BEGIN
	EXEC [lekmer].[pProductRegistryRestrictionBrandDelete] @BrandId

	DELETE FROM
		[review].[tBlockBestRatedProductListBrand]
	WHERE 
		[BrandId] = @BrandId

	DELETE FROM 
		[review].[tBlockLatestFeedbackListBrand]
	WHERE 
		[BrandId] = @BrandId

	DELETE FROM 
		[lekmer].[tBlockBrandListBrand]
	WHERE 
		[BrandId] = @BrandId
	
	DELETE FROM 
		[lekmer].[tBlockBrandProductListBrand]
	WHERE 
		[BrandId] = @BrandId
		
	DELETE FROM
		[lekmer].[tBlockProductFilterBrand]
	WHERE 
		[BrandId] = @BrandId	
	
	UPDATE 
		[lekmer].[tLekmerProduct]
	SET 
		[BrandId] = NULL 
	WHERE 
		[BrandId] = @BrandId

	DELETE FROM 
		[lekmer].[tBrandTranslation]
	WHERE
		[BrandId] = @BrandId
			
	DELETE FROM 
		[lekmer].[tBrand]
	WHERE 
		[BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetIdAllWithoutStatusFilter]'
GO


CREATE PROCEDURE [lekmer].[pProductGetIdAllWithoutStatusFilter]
	@ChannelId INT,
	@CustomerId INT,
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	DECLARE @sqlCountFilter NVARCHAR(MAX)

	IF @CustomerId is not null
		BEGIN
			RAISERROR ('Current version doesn''t support CustomerId parameter.',10,1)
		END


	SELECT COUNT(*)
		FROM
			product.tProduct AS p
			INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
			INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
			INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
		WHERE
			Ch.ChannelId = @ChannelId
			and P.IsDeleted = 0


	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY p.[ProductId]) AS Number,
			p.[ProductId]
		FROM
			product.tProduct AS p
			INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
			INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
			INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
		WHERE
			Ch.ChannelId = @ChannelId
			and P.IsDeleted = 0
	) As Result
	WHERE Number > (@Page - 1) * @PageSize
		AND Number <= @Page * @PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryProductDeleteByBrand]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductDeleteByBrand]
	@BrandId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN 
	DELETE prp 
	FROM [product].[tProductRegistryProduct] prp
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = prp.[ProductId]
	WHERE lp.[BrandId] = @BrandId
		  AND prp.[ProductRegistryId] IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductRegistryProductDeleteByCategory]'
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductDeleteByCategory]
	@CategoryId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN 
	DELETE prp 
	FROM [product].[tProductRegistryProduct] prp
		 INNER JOIN [product].[tProduct] p ON p.[ProductId] = prp.[ProductId]
	WHERE p.[CategoryId] IN (SELECT CategoryId FROM [product].[fnGetSubCategories] (@CategoryId))
		  AND prp.[ProductRegistryId] IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCartValueRangeConditionCurrency]'
GO
CREATE VIEW [campaignlek].[vCartValueRangeConditionCurrency]
AS
	SELECT
		cvrcc.[ConditionId] AS 'CartValueRangeConditionCurrency.ConditionId',
		cvrcc.[CurrencyId] AS 'CartValueRangeConditionCurrency.CurrencyId',
		cvrcc.[MonetaryValueFrom] AS 'CartValueRangeConditionCurrency.MonetaryValueFrom',
		cvrcc.[MonetaryValueTo] AS 'CartValueRangeConditionCurrency.MonetaryValueTo',
		c.*
	FROM
		[campaignlek].[tCartValueRangeConditionCurrency] cvrcc
		INNER JOIN [core].[vCustomCurrency] c ON cvrcc.[CurrencyId] = c.[Currency.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartValueRangeConditionCurrencyGetByCondition]'
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionCurrencyGetByCondition]
	@ConditionId	INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartValueRangeConditionCurrency] cvrcc
	WHERE 
		cvrcc.[CartValueRangeConditionCurrency.ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCartValueRangeCondition]'
GO
CREATE VIEW [campaignlek].[vCartValueRangeCondition]
AS
	SELECT
		c.*
	FROM
		[campaignlek].[tCartValueRangeCondition] cvrc
		INNER JOIN [campaign].[vCustomCondition] c ON c.[Condition.Id] = cvrc.[ConditionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartValueRangeConditionGetById]'
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionGetById]
	@ConditionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartValueRangeCondition] cvrc
	WHERE
		cvrc.[Condition.Id] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vFixedDiscountActionCurrency]'
GO



CREATE VIEW [campaignlek].[vFixedDiscountActionCurrency]
AS
	SELECT
		fdac.[ProductActionId] AS 'FixedDiscountActionCurrency.ProductActionId',
		fdac.[CurrencyId] AS 'FixedDiscountActionCurrency.CurrencyId',
		fdac.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tFixedDiscountActionCurrency] fdac
		INNER JOIN [core].[vCustomCurrency] c ON fdac.[CurrencyId] = c.[Currency.Id]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionCurrencyGetByAction]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionCurrencyGetByAction]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedDiscountActionCurrency]
	WHERE 
		[FixedDiscountActionCurrency.ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vFixedDiscountAction]'
GO

CREATE VIEW [campaignlek].[vFixedDiscountAction]
AS
	SELECT
		fda.[ProductActionId] AS 'FixedDiscountAction.ProductActionId',
		fda.[IncludeAllProducts] AS 'FixedDiscountAction.IncludeAllProducts',
		pa.*
	FROM
		[campaignlek].[tFixedDiscountAction] fda
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = fda.[ProductActionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionGetById]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionGetById]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedDiscountAction]
	WHERE
		[FixedDiscountAction.ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pPercentageDiscountCartActionGetById]'
GO
CREATE PROCEDURE [campaignlek].[pPercentageDiscountCartActionGetById]
	@CartActionId INT
AS 
BEGIN 
	SELECT
		ca.*,
		pdca.*
	FROM
		[campaign].[vCustomCartAction] AS ca
		LEFT JOIN [campaignlek].[tPercentageDiscountCartAction] pdca ON pdca.[CartActionId] = ca.[CartAction.Id]
	WHERE
		ca.[CartAction.Id] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ChannelRestrictionsProducts]'
GO
ALTER PROCEDURE [integration].[usp_ChannelRestrictionsProducts]
AS
BEGIN
	SET NOCOUNT ON
	
	BEGIN TRY
		BEGIN TRANSACTION
		--------------------------------------------------
		--- Update tProductTranslation with new products
		--------------------------------------------------
		-- NO
		INSERT INTO product.tProductTranslation (ProductId, LanguageId)
		SELECT p.productid, 1000001 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000001)

		-- DK
		INSERT INTO product.tProductTranslation (ProductId, LanguageId)
		SELECT p.productid, 1000002 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000002)

		-- FI
		INSERT INTO product.tProductTranslation(ProductId, LanguageId)
		SELECT p.productid, 1000003 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000003)


		----------------------------------------------------------------------------
		--- Insert not translated products in the tProductRegistryRestrictionProduct
		----------------------------------------------------------------------------
		-- NO
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			2,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000001 -- NO
			AND Title IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 2)

		-- DK
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			3,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000002 -- DK
			AND Title IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 3)

		-- FI
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			4,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000003 -- FI
			AND Title IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 4)


		----------------------------------------------------------------------
		---- Get Product Ids that need add to product.tProductRegistryProduct.
		----------------------------------------------------------------------
		DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]

		-- NO
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 2 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 2)

		-- DK
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 3 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 3)
		
		-- FI
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 4 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 4)


		--------------------------------------------------------------------------------
		---- Delete items from tProductRegistryProduct and from list products to insert.
		--------------------------------------------------------------------------------
		DELETE prp
		FROM [product].[tProductRegistryProduct] prp
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = prp.ProductRegistryId 
																		 AND prrp.ProductId = prp.ProductId

		DELETE pIds
		FROM @ProductIdsToInsert pIds
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = pIds.ProductRegistryId 
																		 AND prrp.ProductId = pIds.ProductId


		EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
		
	COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			'',
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)
	END CATCH
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductHeppo]'
GO

ALTER PROCEDURE [integration].[usp_ImportUpdateProductHeppo]

AS
BEGIN
		--raiserror('errorMSG', 16, 1)
		-- tCategory proc kallas
		print 'Start tCategory'
		exec [integration].[usp_UpdateTCategoryHeppo] -- skapar om artiklar om det behövs
		print 'End tCategory'
		print 'Start tUpdateBrand'
		exec [integration].usp_UpdateBrandHeppo
		print 'End tUpdateBrand'

		
		-- tProduct
		print 'Start update product'
		UPDATE 
			p
		SET 
			Title = tp.ArticleTitle, 
			--ItemsInPackage = null,
			EanCode = tp.EanCode, 
			--IsDeleted = 0, 
			--ColorId SAKNAS
			--SizeId SAKNAS
			NumberInStock = tp.NoInStock,
			-- Category kanske måste ändras om det skapar problem
			-- man ändrar den då till typ
			-- select catagoryId from tcategory where erpid = varukl+varugrupp+varukod 
			CategoryId = (select categoryId from product.tCategory where ErpId = 'C_'+tp.ArticleClassId+'-'+tp.ArticleGroupId+'-'+tp.ArticleCodeId)
			--CategoryId = tp.ArticleCodeId,	--varukodID det är den mest specifika!!
			--WebShopTitle = null,
			--[Description] = '',
			--ShortDescription = null,
			--MediaId = null, -- Måste sättas
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,11)	-- 5,17 innan
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join [product].tProductRegistryProduct pli
					on pli.ProductId = lp.ProductId and
					pli.ProductRegistryId = substring(tp.HYarticleId, 3,1)
		WHERE 
			p.Title <> tp.ArticleTitle OR
			p.EanCode <> tp.EanCode OR
			p.NumberInStock <> tp.NoInStock
		 
		print 'End product update'
		
		---------------------------------------------
		--print 'Start lekmer product update'
		--exec integration.usp_ImportUpdateLekmerProduct
		--print 'End lekmer product update'
	--------------------------------------		
		-- tPriceListItem - Update Prices from integration.tempProductPrice
		print 'Start price update'
		exec [integration].[usp_UpdateProductPRICE]
		print 'End price update'
	--------------------------------------
	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@HYarticleIdNoFokNoSize nvarchar (50),
				@HYarticleSize nvarchar(50),
				@EanCode nvarchar(50),
				@NoInStock nvarchar(250),
				@ArticleTitle nvarchar(250),
				@ProductId int,
				@HYBrandId nvarchar(25),
				
				@IsBookable bit,
				@ExpectedBackInStock datetime,
				@IsDeleted bit,
				@CategoryId int,
				@Description nvarchar(250),
				@Price decimal,
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10),
				
				@Data nvarchar(4000),
				@Vat decimal
				SET @Vat = 25
				SET @IsDeleted = 0
				SET @Description = ''
				SET @ExpectedBackInStock = null
				SET @IsBookable = 0
		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				------------------------------------
		
		declare cur_product cursor fast_forward for
			select
				tp.HYarticleId,
				tp.EanCode,
				tp.NoInStock,
				tp.ArticleTitle,
				(tp.Price/100),
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId,
				tp.BrandId
			from
				[integration].tempProduct tp
			where not exists(select 1
						-- parar lekmerErpId med temp tabell erpId (klipper fok) sen parar
						-- tProductregistry på lekmerProductId på det ErpId sen parar också foken med productRegProduct
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = substring(tp.HYarticleId, 5,11) and
							lp.ProductId = prp.ProductId and
							substring(tp.HYarticleId, 3,1) = prp.ProductRegistryId)
			
			
			
		--print 'Open Cursor'
		open cur_product
		fetch next from cur_product
			into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1) -- Fok här är nu 001 eller 002 osv beroende på vilket land artikeln tillhör
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)			
				SET @HYarticleSize = substring(@HYarticleId, 17,3)
				SET @HYarticleIdNoFokNoSize = substring(@HYarticleId, 5,11)
				
				-- if, kolla om eprid utan fok finns, om det gör hämta dens produktid och lägg in de produkt idet + fok
				-- i tproductRegistryProduct	
				
				if not exists(select 1 -- om erpid inte finns i tlekmerProduct finns det inte i nån fok, lägg in den då, annars lägg bara in i pReg
							from lekmer.tLekmerProduct lp
							where lp.HYErpId = @HYarticleIdNoFokNoSize)
				begin	
					print 'insert into product.tproduct'
					INSERT INTO -- MediaId saknas
						[product].tProduct(ErpId, EanCode, IsDeleted, NumberInStock, CategoryId, Title, [Description], ProductStatusId)
					SELECT 
						@HYarticleIdNoFokNoSize, -- @HYArticleId
						@EanCode, 
						@IsDeleted, --0
						@NoInStock, 
						(SELECT CategoryId FROM product.tCategory WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId), -- koll här
						@ArticleTitle, 
						@Description, --''
						0 --case when @NoInStock > 0 then 0 else 1 end (Ändrat alla produkter sätts som offline när de sätts in)
												
					set @ProductId = SCOPE_IDENTITY()
					
					-- Data that was sent into insert
					SET @Data = 'NEW: HYArticleId ' + @HYarticleId + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId as varchar(10))
		
					--IF @nrInStock > 0 -- om antal är mer än 0
					--begin
						--SET @IsBookable = 0
					--end
						
					print 'insert into lekmer.tlekmerProduct'		
					insert into lekmer.tLekmerProduct(ProductId, HYErpId, BrandId, IsBookable, AgeFromMonth, AgeToMonth, 
								IsNewFrom, IsNewTo, IsBatteryIncluded, ExpectedBackInStock, ShowVariantRelations)
					values(
					@ProductId, 
					@HYarticleIdNoFokNoSize,
					(Select b.BrandId from lekmer.tBrand b where b.ErpId = @HYBrandId),
					@IsBookable, 
					0, 
					0, 
					Convert(varchar, GETDATE(), 112),
					DATEADD(WK, 1, Convert(varchar, GETDATE(), 112)), 
					0, 
					@ExpectedBackInStock,
					0 --ShowVariantRelations
					)
					print 'insert into product.tPriceListItem'
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, @ProductId, @Price, (@Price * ((100-@Vat)/100)), @Vat)
					print 'insert into product.tProductReg'		
					-- tProductRegistryProduct
					EXEC [lekmer].[pProductRegistryProductInsert]  @ProductId, NULL,NULL, @Fok, ','
					
				
					--commit
				end	
				else if not exists (select 1
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = @HYarticleIdNoFokNoSize -- @HYarticleNoFok innan
							and lp.ProductId = prp.ProductId 
							and prp.ProductRegistryId = @Fok)
				begin
					select @ProductId = ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize --@HYarticleIdNoFoK innan
					set @Data = 'EXISTING: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					print 'insert into product.tproductRegProg (in else)'	
					-- tProductRegistryProduct
					EXEC [lekmer].[pProductRegistryProductInsert]  @ProductId, NULL,NULL, @Fok, ','
					
					print 'insert into product.rPriceList (in else)'	
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize), --@HYarticleIdNoFoK innan),
					@Price, (@Price * ((100-@Vat)/100)), @Vat)
					
				end
				
				
				-- placeringen funkar inte, kommer bara gå in hit om produkten inte är
				-- i alla 4 foks...
				-- om den specifika sizen av produkten inte finns i tProductSize
				/*
				if not exists (select 1
						from lekmer.tProductSize ps
						where ps.ErpId = @HYarticleIdNoFoK)
				begin
				set @Data = 'NEW tProductSIZE: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						@ProductId,
						1, --(select SizeId from lekmer.tSize where ErpId = @HYarticleSize),
						@HYarticleIdNoFoK,
						@NoInStock
					
				end
			*/
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_product into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		end
		
		close cur_product
		deallocate cur_product
		

		-- hantera storlekarna på bilderna
		print '[integration].[usp_ImportUpdateProductSizesHeppo]  körs'
		exec [integration].[usp_ImportUpdateProductSizesHeppo]
				
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionExcludeProductGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@ProductActionId	INT
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		[campaignlek].[tFixedDiscountActionExcludeProduct] fdaep
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = fdaep.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		fdaep.[ProductActionId] = @ProductActionId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountActionIncludeProductGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@ProductActionId	INT
AS
BEGIN
	SELECT
		p.[Product.Id]
	FROM
		[campaignlek].[tFixedDiscountActionIncludeProduct] fdaip
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = fdaip.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		fdaip.[ProductActionId] = @ProductActionId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetAllByIdListWithoutStatusFilter]'
GO
CREATE PROCEDURE [lekmer].[pProductGetAllByIdListWithoutStatusFilter]
	@ChannelId		INT,
	@CustomerId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[p].*,
		[pli].*
	FROM
		[lekmer].[vCustomProductWithoutStatusFilter] p
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl 
			ON pl.Id = p.[Product.Id]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartContainsConditionExcludeBrand]'
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeBrand] ADD
CONSTRAINT [FK_tCartContainsConditionExcludeBrand_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId]),
CONSTRAINT [FK_tCartContainsConditionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartContainsConditionIncludeBrand]'
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeBrand] ADD
CONSTRAINT [FK_tCartContainsConditionIncludeBrand_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId]),
CONSTRAINT [FK_tCartContainsConditionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartValueRangeCondition]'
GO
ALTER TABLE [campaignlek].[tCartValueRangeCondition] ADD
CONSTRAINT [FK_tCartValueRangeCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartValueRangeConditionCurrency]'
GO
ALTER TABLE [campaignlek].[tCartValueRangeConditionCurrency] ADD
CONSTRAINT [FK_tCartValueRangeConditionCurrency_tCartValueRangeCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaignlek].[tCartValueRangeCondition] ([ConditionId]),
CONSTRAINT [FK_tCartValueRangeConditionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountAction]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountAction] ADD
CONSTRAINT [FK_tFixedDiscountAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountActionCurrency]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionCurrency] ADD
CONSTRAINT [FK_tFixedDiscountActionCurrency_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId]),
CONSTRAINT [FK_tFixedDiscountActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeBrand] ADD
CONSTRAINT [FK_tFixedDiscountActionExcludeBrand_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId]),
CONSTRAINT [FK_tFixedDiscountActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountActionExcludeCategory]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeCategory] ADD
CONSTRAINT [FK_tFixedDiscountActionExcludeCategory_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId]),
CONSTRAINT [FK_tFixedDiscountActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountActionExcludeProduct]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeProduct] ADD
CONSTRAINT [FK_tFixedDiscountActionExcludeProduct_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId]),
CONSTRAINT [FK_tFixedDiscountActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeBrand] ADD
CONSTRAINT [FK_tFixedDiscountActionIncludeBrand_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId]),
CONSTRAINT [FK_tFixedDiscountActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountActionIncludeCategory]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeCategory] ADD
CONSTRAINT [FK_tFixedDiscountActionIncludeCategory_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId]),
CONSTRAINT [FK_tFixedDiscountActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountActionIncludeProduct]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeProduct] ADD
CONSTRAINT [FK_tFixedDiscountActionIncludeProduct_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId]),
CONSTRAINT [FK_tFixedDiscountActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tPercentageDiscountCartAction]'
GO
ALTER TABLE [campaignlek].[tPercentageDiscountCartAction] ADD
CONSTRAINT [FK_tPercentageDiscountAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionExcludeBrand] ADD
CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeBrand_tPercentagePriceDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tPercentagePriceDiscountAction] ([ProductActionId]),
CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionIncludeBrand] ADD
CONSTRAINT [FK_tPercentagePriceDiscountActionIncludeBrand_tPercentagePriceDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tPercentagePriceDiscountAction] ([ProductActionId]),
CONSTRAINT [FK_tPercentagePriceDiscountActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductRegistryRestrictionBrand]'
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionBrand] ADD
CONSTRAINT [FK_tProductRegistryRestrictionBrand_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId]),
CONSTRAINT [FK_tProductRegistryRestrictionBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductRegistryRestrictionCategory]'
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionCategory] ADD
CONSTRAINT [FK_tProductRegistryRestrictionCategory_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId]),
CONSTRAINT [FK_tProductRegistryRestrictionCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductRegistryRestrictionProduct]'
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionProduct] ADD
CONSTRAINT [FK_tProductRegistryRestrictionProduct_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId]),
CONSTRAINT [FK_tProductRegistryRestrictionProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
