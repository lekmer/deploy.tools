/*
Run this script on:

10.150.43.52.Lekmer_live – this database will be modified

to synchronize its data with:

D:\Projects\lmheppo\Scensum\trunk\Release\Release 011\DB\LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 9.0.0 from Red Gate Software Ltd at 14.05.2012 9:36:08

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Add row to [campaign].[tConditionType]
SET IDENTITY_INSERT [campaign].[tConditionType] ON
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (1000002, N'Cart value range', N'CartValueRange')
SET IDENTITY_INSERT [campaign].[tConditionType] OFF

-- Add row to [campaign].[tProductActionType]
SET IDENTITY_INSERT [campaign].[tProductActionType] ON
INSERT INTO [campaign].[tProductActionType] ([ProductActionTypeId], [Title], [CommonName]) VALUES (1000002, N'Fixed discount', N'FixedDiscount')
SET IDENTITY_INSERT [campaign].[tProductActionType] OFF
COMMIT TRANSACTION
GO
