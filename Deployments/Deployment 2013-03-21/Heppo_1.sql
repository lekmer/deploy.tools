SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [export].[pCdonExportRestrictedProductsCount]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictedProductsCount]
AS
BEGIN
	DECLARE @RestrictedProducts TABLE (ProductId INT)
	DECLARE @IncludedProducts TABLE (ProductId INT)
	DECLARE @RestrictedProductsByChannel TABLE (ProductId INT, ChannelId INT)

	DECLARE @tmpChannelId INT
	DECLARE @tmpChannels TABLE (ChannelId INT)
	INSERT INTO @tmpChannels ([channelId])
	SELECT [ChannelId] FROM [core].[tChannel]

	WHILE ((SELECT count(*) FROM @tmpChannels) > 0)
	BEGIN
		DELETE @RestrictedProducts
		DELETE @IncludedProducts

		SET @tmpChannelId = (SELECT TOP 1 [ChannelId] FROM @tmpChannels)
			
		INSERT INTO @RestrictedProducts ([ProductId])
		(
			SELECT DISTINCT [p].[ProductId]
			FROM [lekmer].[tLekmerProduct] p
			WHERE [p].[BrandId] IN (SELECT [rb].[BrandId] FROM [export].[tCdonExportRestrictionBrand] rb
									INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rb].[ProductRegistryId]
									WHERE [pmc].[ChannelId] = @tmpChannelId)

			UNION

			SELECT DISTINCT [p].[ProductId]
			FROM [lekmer].[tLekmerProduct] p
			WHERE [p].[ProductId] IN (SELECT [rp].[ProductId] FROM [export].[tCdonExportRestrictionProduct] rp
									  INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rp].[ProductRegistryId]
									  WHERE [pmc].[ChannelId] = @tmpChannelId)

			UNION

			SELECT DISTINCT [p].[ProductId]
			FROM [product].[tProduct] p 
			WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [export].[tCdonExportRestrictionCategory] rc
									   CROSS APPLY [product].[fnGetSubCategories] (rc.CategoryId) src
									   INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
									   WHERE [pmc].[ChannelId] = @tmpChannelId)
		)

		INSERT INTO @IncludedProducts ([ProductId])
		(
			SELECT DISTINCT [p].[ProductId]
			FROM [lekmer].[tLekmerProduct] p 
			WHERE [p].[BrandId] IN (SELECT [ib].[BrandId] FROM [export].[tCdonExportIncludeBrand] ib
									INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ib].[ProductRegistryId]
									WHERE [pmc].[ChannelId] = @tmpChannelId)

			UNION

			SELECT DISTINCT [p].[ProductId]
			FROM [lekmer].[tLekmerProduct] p
			WHERE [p].[ProductId] IN (SELECT [ip].[ProductId] FROM [export].[tCdonExportIncludeProduct] ip
									  INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ip].[ProductRegistryId]
									  WHERE [pmc].[ChannelId] = @tmpChannelId)

			UNION

			SELECT DISTINCT [p].[ProductId]
			FROM [product].[tProduct] p 
			WHERE [p].[CategoryId] IN (SELECT DISTINCT [sic].[CategoryId] FROM [export].[tCdonExportIncludeCategory] ic
									   CROSS APPLY [product].[fnGetSubCategories] (ic.CategoryId) sic
									   INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ic].[ProductRegistryId]
									   WHERE [pmc].[ChannelId] = @tmpChannelId)
		)
		
		INSERT INTO @RestrictedProductsByChannel ([ProductId], [ChannelId])
		(
			SELECT [a0].[ProductId], @tmpChannelId 
			FROM (	SELECT [ProductId] FROM @RestrictedProducts
					EXCEPT
					SELECT [ProductId] FROM @IncludedProducts) a0
		)
			
		DELETE @tmpChannels WHERE [ChannelId] = @tmpChannelId
	END

	SELECT COUNT([ProductId]), [ChannelId] FROM @RestrictedProductsByChannel GROUP BY [ChannelId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
