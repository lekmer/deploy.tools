SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tSizeTableIncludeProduct]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] DROP CONSTRAINT [FK_tSizeTableIncludeProduct_tSizeTable]
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] DROP CONSTRAINT [FK_tSizeTableIncludeProduct_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tSizeTableIncludeCategory]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategory] DROP CONSTRAINT [FK_tSizeTableIncludeCategory_tSizeTable]
ALTER TABLE [lekmer].[tSizeTableIncludeCategory] DROP CONSTRAINT [FK_tSizeTableIncludeCategory_tCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tSizeTableIncludeBrand]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeBrand] DROP CONSTRAINT [FK_tSizeTableIncludeBrand_tSizeTable]
ALTER TABLE [lekmer].[tSizeTableIncludeBrand] DROP CONSTRAINT [FK_tSizeTableIncludeBrand_tBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tSizeTableIncludeProduct]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] DROP CONSTRAINT [PK_tSizeTableIncludeProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tSizeTableIncludeCategory]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategory] DROP CONSTRAINT [PK_tSizeTableIncludeCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tSizeTableIncludeBrand]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeBrand] DROP CONSTRAINT [PK_tSizeTableIncludeBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pSizeTableGetByProductSecure]'
GO
DROP PROCEDURE [lekmer].[pSizeTableGetByProductSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pSizeTableGetByBrand]'
GO
DROP PROCEDURE [lekmer].[pSizeTableGetByBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pSizeTableIncludeBrandDelete]'
GO
DROP PROCEDURE [lekmer].[pSizeTableIncludeBrandDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pSizeTableIncludeCategoryDelete]'
GO
DROP PROCEDURE [lekmer].[pSizeTableIncludeCategoryDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pSizeTableGetByCategory]'
GO
DROP PROCEDURE [lekmer].[pSizeTableGetByCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pSizeTableIncludesClear]'
GO
DROP PROCEDURE [lekmer].[pSizeTableIncludesClear]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[tSizeTableIncludeBrand]'
GO
DROP TABLE [lekmer].[tSizeTableIncludeBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[tSizeTableIncludeCategory]'
GO
DROP TABLE [lekmer].[tSizeTableIncludeCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableIncludeCategoryBrand]'
GO
CREATE TABLE [lekmer].[tSizeTableIncludeCategoryBrand]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[SizeTableId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[BrandId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableIncludeCategoryBrand] on [lekmer].[tSizeTableIncludeCategoryBrand]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategoryBrand] ADD CONSTRAINT [PK_tSizeTableIncludeCategoryBrand] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tSizeTableIncludeProduct]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tSizeTableIncludeProduct]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[SizeTableId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tSizeTableIncludeProduct]([SizeTableId], [ProductId]) SELECT [SizeTableId], [ProductId] FROM [lekmer].[tSizeTableIncludeProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tSizeTableIncludeProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tSizeTableIncludeProduct]', N'tSizeTableIncludeProduct'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableIncludeProduct] on [lekmer].[tSizeTableIncludeProduct]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] ADD CONSTRAINT [PK_tSizeTableIncludeProduct] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableGetByProduct]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableGetByProduct]
	@ProductId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SizeTableId INT
	SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeProduct] WHERE [ProductId] = @ProductId ORDER BY Id DESC)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	/*--------------------------------------------------------------------------------------------------------------------------*/
	
	DECLARE @BrandId INT, @CategoryThirdLevelId INT, @CategorySecondLevelId INT, @CategoryFirstLevelId INT
	
	SET @BrandId = (SELECT [BrandId] FROM [lekmer].[tLekmerProduct] WHERE [ProductId] = @ProductId)
	IF (@BrandId IS NOT NULL)
	BEGIN
		SET @CategoryThirdLevelId = (SELECT [CategoryId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId)
		-- Size table include category 3 level
		SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand]
							WHERE [CategoryId] = @CategoryThirdLevelId AND [BrandId] = @BrandId 
							ORDER BY Id DESC)
		IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
			RETURN @SizeTableId

		SET @CategorySecondLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
		-- Size table include category 2 level
		SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] 
							WHERE [CategoryId] = @CategorySecondLevelId AND [BrandId] = @BrandId
							ORDER BY Id DESC)
		IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
			RETURN @SizeTableId

		SET @CategoryFirstLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategorySecondLevelId)
		-- Size table include category 1 level
		SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] 
							WHERE [CategoryId] = @CategoryFirstLevelId AND [BrandId] = @BrandId
							ORDER BY Id DESC)
		IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
			RETURN @SizeTableId
	END
	
	/*--------------------------------------------------------------------------------------------------------------------------*/

	IF (@CategoryThirdLevelId IS NULL)
		SET @CategoryThirdLevelId = (SELECT [CategoryId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId)
	-- Size table include category 3 level
	SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand]
						WHERE [CategoryId] = @CategoryThirdLevelId AND [BrandId] IS NULL 
						ORDER BY Id DESC)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	IF (@CategorySecondLevelId IS NULL)
		SET @CategorySecondLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	-- Size table include category 2 level
	SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] 
						WHERE [CategoryId] = @CategorySecondLevelId AND [BrandId] IS NULL
						ORDER BY Id DESC)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	IF (@CategoryFirstLevelId IS NULL)
		SET @CategoryFirstLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategorySecondLevelId)
	-- Size table include category 1 level
	SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] 
						WHERE [CategoryId] = @CategoryFirstLevelId AND [BrandId] IS NULL
						ORDER BY Id DESC)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeGetAllByProductIdList]'
GO
ALTER PROCEDURE [lekmer].[pProductSizeGetAllByProductIdList]
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[ps].*
	FROM 
		[lekmer].[vProductSize] ps
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl 
			ON [pl].[Id] = [ps].[ProductSize.ProductId]
	ORDER BY
		[ps].[ProductSize.ProductId],
		[ps].[Size.Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductSizeGetAllByProduct]
	@ProductId	INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vProductSize]
	WHERE
		[ProductSize.ProductId] = @ProductId
	ORDER BY
		[Size.Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableIncludesGetAll]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableIncludesGetAll]
	@SizeTableId	INT
AS
BEGIN
	SELECT [ProductId] FROM [lekmer].[tSizeTableIncludeProduct] WHERE [SizeTableId] = @SizeTableId
	SELECT [Id], [CategoryId], [BrandId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableIncludeCategoryBrandDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludeCategoryBrandDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tSizeTableIncludeCategoryBrand]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableIncludesSave]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableIncludesSave]
	@SizeTableId	INT,
	@ProductIds		VARCHAR(MAX),
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	-- Delete existing includes for size table.
	EXEC [lekmer].[pSizeTableIncludeProductDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeCategoryBrandDelete] @SizeTableId	

	-- Insert size table Product includes.
	INSERT [lekmer].[tSizeTableIncludeProduct] ([SizeTableId], [ProductId])
	SELECT @SizeTableId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p
	
	-- Size table Category includes.
	DECLARE @tCategory TABLE (CategoryId INT, Ordinal INT)
	INSERT INTO @tCategory ([CategoryId], [Ordinal])
	SELECT [Id], [Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter)

	-- Size table Brand includes.
	DECLARE @tBrand TABLE (BrandId INT, Ordinal INT)
	INSERT INTO @tBrand ([BrandId], [Ordinal])
	SELECT (CASE WHEN [Id] > 0 THEN [Id] ELSE NULL END), [Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal] (@BrandIds, @Delimiter)

	INSERT INTO [lekmer].[tSizeTableIncludeCategoryBrand] ([SizeTableId], [CategoryId], [BrandId])
	SELECT @SizeTableId, [c].[CategoryId], [b].[BrandId]
	FROM @tCategory c
	INNER JOIN @tBrand b ON [b].[Ordinal] = [c].[Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pContentPageUrlHistoryDeleteExpiredItems]'
GO

ALTER PROCEDURE [lekmer].[pContentPageUrlHistoryDeleteExpiredItems]
AS
BEGIN
	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = GETDATE()

	DELETE 
		cpuh 
	FROM
		[lekmer].[tContentPageUrlHistory] cpuh
		INNER JOIN [lekmer].[tHistoryLifeIntervalType] hlit ON cpuh.HistoryLifeIntervalTypeId = hlit.HistoryLifeIntervalTypeId
	WHERE 
		@CurrentDate > DATEADD(DAY, hlit.DaysActive, cpuh.LastUsageDate)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductUrlHistoryDeleteExpiredItems]'
GO

ALTER PROCEDURE [lekmer].[pProductUrlHistoryDeleteExpiredItems]
AS
BEGIN
	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = GETDATE()

	DELETE 
		puh 
	FROM
		[lekmer].[tProductUrlHistory] puh
		INNER JOIN [lekmer].[tHistoryLifeIntervalType] hlit ON puh.HistoryLifeIntervalTypeId = hlit.HistoryLifeIntervalTypeId
	WHERE 
		@CurrentDate > DATEADD(DAY, hlit.DaysActive, puh.LastUsageDate)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableDelete]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pSizeTableIncludeProductDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeCategoryBrandDelete] @SizeTableId	
	EXEC [lekmer].[pSizeTableTranslationDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableRowDelete] @SizeTableId
	
	EXEC [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTable] @SizeTableId
	EXEC [lekmer].[pSizeTableMediaDeleteAllBySizeTable] @SizeTableId
		
	DELETE FROM [lekmer].[tSizeTable]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableIncludeCategoryBrand]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategoryBrand] ADD CONSTRAINT [FK_tSizeTableIncludeCategoryBrand_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
ALTER TABLE [lekmer].[tSizeTableIncludeCategoryBrand] ADD CONSTRAINT [FK_tSizeTableIncludeCategoryBrand_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
ALTER TABLE [lekmer].[tSizeTableIncludeCategoryBrand] ADD CONSTRAINT [FK_tSizeTableIncludeCategoryBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableIncludeProduct]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] ADD CONSTRAINT [FK_tSizeTableIncludeProduct_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] ADD CONSTRAINT [FK_tSizeTableIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
