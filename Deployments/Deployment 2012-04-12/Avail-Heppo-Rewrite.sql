/*
Run this script on:

        (local).Heppo_212_0    -  This database will be modified

to synchronize it with:

        (local).Heppo_212_1

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 10.04.2012 15:49:46

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [export]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [generic].[fGetCategoryUrl]'
GO
DROP FUNCTION [generic].[fGetCategoryUrl]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [generic].[fnGetCategoryUrlForProduct]'
GO
DROP FUNCTION [generic].[fnGetCategoryUrlForProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [generic].[fGetCategoryProductUrl]'
GO
CREATE FUNCTION [generic].[fGetCategoryProductUrl]
(
	@ApplicationName VARCHAR(100),
	@CategoryPath VARCHAR(1000),
	@LanguageId INT,
	@ProductId INT
)
RETURNS VARCHAR(1000)
AS
BEGIN
	DECLARE @ProductUrlTitle VARCHAR(1000)

	SELECT	@ProductUrlTitle = UrlTitle 
	FROM	lekmer.tProductUrl 
	WHERE	ProductId = @ProductId AND LanguageId = @LanguageId

	SET @ProductUrlTitle = REPLACE(@ProductUrlTitle, ' ', '-')

	RETURN 'http://' + @ApplicationName + '/' + @CategoryPath + @ProductUrlTitle
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tAvailProductSitePrice]'
GO
CREATE TABLE [export].[tAvailProductSitePrice]
(
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SitePrice] [decimal] (16, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAvailProductSitePrice] on [export].[tAvailProductSitePrice]'
GO
ALTER TABLE [export].[tAvailProductSitePrice] ADD CONSTRAINT [PK_tAvailProductSitePrice] PRIMARY KEY CLUSTERED  ([ChannelId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tAvailProductList]'
GO
CREATE TABLE [export].[tAvailProductList]
(
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAvailProductList] on [export].[tAvailProductList]'
GO
ALTER TABLE [export].[tAvailProductList] ADD CONSTRAINT [PK_tAvailProductList] PRIMARY KEY CLUSTERED  ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tAvailProductData]'
GO
CREATE TABLE [export].[tAvailProductData]
(
[ProductId] [int] NOT NULL,
[PriceSE] [decimal] (16, 2) NOT NULL,
[PriceDK] [decimal] (16, 2) NOT NULL,
[PriceNO] [decimal] (16, 2) NOT NULL,
[PriceFI] [decimal] (16, 2) NOT NULL,
[PriceFR] [decimal] (16, 2) NOT NULL,
[PriceNL] [decimal] (16, 2) NOT NULL,
[DiscountPriceSE] [decimal] (16, 2) NOT NULL,
[DiscountPriceDK] [decimal] (16, 2) NOT NULL,
[DiscountPriceNO] [decimal] (16, 2) NOT NULL,
[DiscountPriceFI] [decimal] (16, 2) NOT NULL,
[DiscountPriceFR] [decimal] (16, 2) NOT NULL,
[DiscountPriceNL] [decimal] (16, 2) NOT NULL,
[CategoryUrlSE] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlNO] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlDA] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlFI] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlFR] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlNL] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ImageUrlSE] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlNO] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlDA] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlFI] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlFR] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlNL] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[TitleSE] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleNO] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleDA] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleFI] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleFR] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleNL] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[NumberInStock] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAvailProductData] on [export].[tAvailProductData]'
GO
ALTER TABLE [export].[tAvailProductData] ADD CONSTRAINT [PK_tAvailProductData] PRIMARY KEY CLUSTERED  ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tAvailCategoryPath]'
GO
CREATE TABLE [export].[tAvailCategoryPath]
(
[CategoryId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[CategoryPath] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAvailCategoryPath] on [export].[tAvailCategoryPath]'
GO
ALTER TABLE [export].[tAvailCategoryPath] ADD CONSTRAINT [PK_tAvailCategoryPath] PRIMARY KEY CLUSTERED  ([CategoryId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [generic].[fGetImageUrl]'
GO
ALTER FUNCTION [generic].[fGetImageUrl]
(
	@ProductId INT,
	@ApplicationName VARCHAR(100)
)
RETURNS VARCHAR(1000)
AS
BEGIN
	DECLARE @MediaId INT

	SELECT @MediaId = MediaId FROM (
		SELECT TOP 1 MediaId
		FROM product.tProductImage
		WHERE ProductImageGroupId = 1 AND ProductId = @ProductId
		ORDER BY ordinal) t

	RETURN 'http://' + @ApplicationName + '/mediaarchive/' + CAST(@MediaId AS VARCHAR) + '/' + CAST(@MediaId AS VARCHAR) + '.jpg'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [generic].[fGetCategoryUrlForProduct]'
GO
CREATE FUNCTION [generic].[fGetCategoryUrlForProduct](
	@CategoryId INT,
	@LanguageId INT
)
RETURNS VARCHAR(1000)
AS
BEGIN

	DECLARE @CategoryPath VARCHAR(1000);

	WITH CategoryList (CategoryId, Lvl)
	AS
	(
		SELECT
			c.CategoryId,
			0
		FROM
			product.tCategory c
		WHERE
			CategoryId = @CategoryId

		UNION ALL

		SELECT
			cc.ParentCategoryId,
			Lvl+1
		FROM
			product.tCategory cc
			INNER JOIN CategoryList cl ON cc.CategoryId = cl.CategoryId
		WHERE
			cc.ParentCategoryId IS NOT NULL
	)

	SELECT @CategoryPath = (
		SELECT generic.fCleanCategory(COALESCE(ct.Title, cc.Title)) + '/'
		FROM
			CategoryList cl
			INNER JOIN product.tCategory cc ON cc.CategoryId = cl.CategoryId
			LEFT OUTER JOIN product.tCategoryTranslation ct ON ct.CategoryId = cc.CategoryId AND ct.LanguageId = @LanguageId
		ORDER BY
			cl.lvl DESC
		FOR XML PATH (''))

	SET @CategoryPath = REPLACE(@CategoryPath, ' ', '')

	RETURN @CategoryPath
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pAvailGetValidData]'
GO

ALTER PROCEDURE [lekmer].[pAvailGetValidData] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	p.ProductId
	FROM	product.tProduct p
	WHERE	p.ProductStatusId = 0 AND p.NumberInStock > 0
		AND NOT EXISTS ( SELECT 1 FROM lekmer.tProductSize ps WHERE ps.ProductId = p.ProductId )
		
	UNION ALL
		
	SELECT	p.ProductId
	FROM	product.tProduct p
	WHERE	p.ProductStatusId = 0
		AND EXISTS ( SELECT 1 FROM lekmer.tProductSize ps WHERE ps.ProductId = p.ProductId AND ps.NumberInStock > 0 )
			
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[GetOrderDataForAvailExport]'
GO

ALTER PROCEDURE [lekmer].[GetOrderDataForAvailExport] 
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		COALESCE(lo.CustomerIdentificationKey, CAST(o.CustomerId AS VARCHAR(50))) AS CustomerId,
		p.ProductId,
		o.OrderId
	FROM
		[order].[tOrder] o
		INNER JOIN [lekmer].[tLekmerOrder] lo ON o.OrderId = lo.OrderId
		INNER JOIN [order].[tOrderItem] oi ON o.OrderId = oi.OrderId
		INNER JOIN [order].[tOrderItemProduct] oip ON oi.OrderItemId = oip.OrderItemId
		INNER JOIN [product].[tProduct] p ON oip.ErpId = p.ErpId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pAvailPrepareProductData]'
GO
CREATE PROCEDURE [lekmer].[pAvailPrepareProductData]
AS
BEGIN
	SET NOCOUNT ON;
	
	-- Empty export tables
	TRUNCATE TABLE export.tAvailProductList
	TRUNCATE TABLE export.tAvailProductSitePrice
	TRUNCATE TABLE export.tAvailCategoryPath
	TRUNCATE TABLE export.tAvailProductData
	
	-- Fill table with products list
	INSERT INTO export.tAvailProductList
	SELECT ProductId FROM product.tProduct
	
	-- Fill table with site prices
	INSERT INTO export.tAvailProductSitePrice
	EXEC [lekmer].[pAvailGetProductDiscountPriceOnSite]
	
	DECLARE
		@ProductId INT,
		@CategoryId INT,
		@Price_SE DECIMAL(16, 2),
		@Price_DK DECIMAL(16, 2),
		@Price_NO DECIMAL(16, 2),
		@Price_FI DECIMAL(16, 2),
		@Price_FR DECIMAL(16, 2),
		@Price_NL DECIMAL(16, 2),
		@DiscountPrice_SE DECIMAL(16, 2),
		@DiscountPrice_DK DECIMAL(16, 2),
		@DiscountPrice_NO DECIMAL(16, 2),
		@DiscountPrice_FI DECIMAL(16, 2),
		@DiscountPrice_FR DECIMAL(16, 2),
		@DiscountPrice_NL DECIMAL(16, 2),
		@CategoryUrl_SE VARCHAR(1000),
		@CategoryUrl_NO VARCHAR(1000),
		@CategoryUrl_DA VARCHAR(1000),
		@CategoryUrl_FI VARCHAR(1000),
		@CategoryUrl_FR VARCHAR(1000),
		@CategoryUrl_NL VARCHAR(1000),
		@ImageUrl_SE VARCHAR(1000),
		@ImageUrl_NO VARCHAR(1000),
		@ImageUrl_DA VARCHAR(1000),
		@ImageUrl_FI VARCHAR(1000),
		@ImageUrl_FR VARCHAR(1000),
		@ImageUrl_NL VARCHAR(1000),
		@Title    NVARCHAR(256),
		@Title_SE NVARCHAR(256),
		@Title_NO NVARCHAR(256),
		@Title_DA NVARCHAR(256),
		@Title_FI NVARCHAR(256),
		@Title_FR NVARCHAR(256),
		@Title_NL NVARCHAR(256),
		@NumberInStock INT,
		@NumberInStock_Size INT
	
	DECLARE
		    @Channel_SE INT,          @Channel_NO INT,          @Channel_DA INT,        
		 @LanguageId_SE INT,       @LanguageId_NO INT,       @LanguageId_DA INT,        
		@PriceListId_SE INT,      @PriceListId_NO INT,      @PriceListId_DA INT,        
		    @AppName_SE VARCHAR(100), @AppName_NO VARCHAR(100), @AppName_DA VARCHAR(100),
		    
		    @Channel_FI INT,          @Channel_FR INT,          @Channel_NL INT,
		 @LanguageId_FI INT,       @LanguageId_FR INT,       @LanguageId_NL INT,
		@PriceListId_FI INT,      @PriceListId_FR INT,      @PriceListId_NL INT,
		    @AppName_FI VARCHAR(100), @AppName_FR VARCHAR(100), @AppName_NL VARCHAR(100)
		
	SELECT @Channel_SE = ChannelId, @AppName_SE = ApplicationName FROM core.tChannel WHERE CommonName = 'Sweden'
	SELECT @Channel_NO = ChannelId, @AppName_NO = ApplicationName FROM core.tChannel WHERE CommonName = 'Norway'
	SELECT @Channel_DA = ChannelId, @AppName_DA = ApplicationName FROM core.tChannel WHERE CommonName = 'Denmark'
	SELECT @Channel_FI = ChannelId, @AppName_FI = ApplicationName FROM core.tChannel WHERE CommonName = 'Finland'
	SELECT @Channel_FR = ChannelId, @AppName_FR = ApplicationName FROM core.tChannel WHERE CommonName = 'France'
	SELECT @Channel_NL = ChannelId, @AppName_NL = ApplicationName FROM core.tChannel WHERE CommonName = 'Netherlands'

	SELECT @LanguageId_SE = LanguageId FROM core.tLanguage WHERE Title = 'Swedish'
	SELECT @LanguageId_NO = LanguageId FROM core.tLanguage WHERE Title = 'Norwegian'
	SELECT @LanguageId_DA = LanguageId FROM core.tLanguage WHERE Title = 'Danish'
	SELECT @LanguageId_FI = LanguageId FROM core.tLanguage WHERE Title = 'Finish'
	SELECT @LanguageId_FR = LanguageId FROM core.tLanguage WHERE Title = 'French'
	SELECT @LanguageId_NL = LanguageId FROM core.tLanguage WHERE Title = 'Dutch'

	SELECT @PriceListId_SE = PriceListId FROM product.tPriceList WHERE CommonName = 'Sweden'
	SELECT @PriceListId_DA = PriceListId FROM product.tPriceList WHERE CommonName = 'Denmark'
	SELECT @PriceListId_NO = PriceListId FROM product.tPriceList WHERE CommonName = 'Norway'
	SELECT @PriceListId_FI = PriceListId FROM product.tPriceList WHERE CommonName = 'Finland'
	SELECT @PriceListId_FR = PriceListId FROM product.tPriceList WHERE CommonName = 'France'
	SELECT @PriceListId_NL = PriceListId FROM product.tPriceList WHERE CommonName = 'Netherlands'

	-- Fill table with categories path
	INSERT INTO export.tAvailCategoryPath
		SELECT CategoryId, @LanguageId_SE, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_SE)) FROM product.tCategory UNION ALL
		SELECT CategoryId, @LanguageId_NO, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_NO)) FROM product.tCategory UNION ALL
		SELECT CategoryId, @LanguageId_DA, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_DA)) FROM product.tCategory UNION ALL
		SELECT CategoryId, @LanguageId_FI, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_FI)) FROM product.tCategory UNION ALL
		SELECT CategoryId, @LanguageId_FR, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_FR)) FROM product.tCategory UNION ALL
		SELECT CategoryId, @LanguageId_NL, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_NL)) FROM product.tCategory

	------------------------------------------------------------------------------
	
	DECLARE product_cursor CURSOR FOR 
	SELECT ProductId FROM export.tAvailProductList

	OPEN product_cursor

	FETCH NEXT FROM product_cursor INTO @ProductId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		-- Get product information
		SELECT
			@CategoryId = p.CategoryId,
			@Title = p.Title,
			@NumberInStock = p.NumberInStock
		FROM
			product.tProduct p
		WHERE
			p.ProductId = @ProductId


		-- Get stock number
		SET @NumberInStock_Size = (SELECT SUM(ps.NumberInStock) FROM lekmer.tProductSize ps WHERE ps.ProductId = @ProductId)
		IF @NumberInStock_Size IS NOT NULL
		BEGIN
			SET @NumberInStock = @NumberInStock_Size
		END


		-- Get price info
		SELECT
			@Price_SE = 0,
			@Price_NO = 0,
			@Price_DK = 0,
			@Price_FI = 0,
			@Price_FR = 0,
			@Price_NL = 0
		
		SELECT @Price_SE = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_SE
		SELECT @Price_NO = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_NO
		SELECT @Price_DK = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_DA
		SELECT @Price_FI = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_FI
		SELECT @Price_FR = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_FR
		SELECT @Price_NL = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_NL


		-- Get discount price info
		SET @DiscountPrice_SE = (SELECT SitePrice FROM export.tAvailProductSitePrice WHERE ProductId = @ProductId AND ChannelId = @Channel_SE)
		SET @DiscountPrice_NO = (SELECT SitePrice FROM export.tAvailProductSitePrice WHERE ProductId = @ProductId AND ChannelId = @Channel_NO)
		SET @DiscountPrice_DK = (SELECT SitePrice FROM export.tAvailProductSitePrice WHERE ProductId = @ProductId AND ChannelId = @Channel_DA)
		SET @DiscountPrice_FI = (SELECT SitePrice FROM export.tAvailProductSitePrice WHERE ProductId = @ProductId AND ChannelId = @Channel_FI)
		SET @DiscountPrice_FR = (SELECT SitePrice FROM export.tAvailProductSitePrice WHERE ProductId = @ProductId AND ChannelId = @Channel_FR)
		SET @DiscountPrice_NL = (SELECT SitePrice FROM export.tAvailProductSitePrice WHERE ProductId = @ProductId AND ChannelId = @Channel_NL)


		-- Get category urls
		SET @CategoryUrl_SE = (SELECT generic.fGetCategoryProductUrl(@AppName_SE, CategoryPath, @LanguageId_SE, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_SE)
		SET @CategoryUrl_NO = (SELECT generic.fGetCategoryProductUrl(@AppName_NO, CategoryPath, @LanguageId_NO, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_NO)
		SET @CategoryUrl_DA = (SELECT generic.fGetCategoryProductUrl(@AppName_DA, CategoryPath, @LanguageId_DA, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_DA)
		SET @CategoryUrl_FI = (SELECT generic.fGetCategoryProductUrl(@AppName_FI, CategoryPath, @LanguageId_FI, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_FI)
		SET @CategoryUrl_FR = (SELECT generic.fGetCategoryProductUrl(@AppName_FR, CategoryPath, @LanguageId_FR, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_FR)
		SET @CategoryUrl_NL = (SELECT generic.fGetCategoryProductUrl(@AppName_NL, CategoryPath, @LanguageId_NL, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_NL)
		

		-- Get image urls
		SELECT
			@ImageUrl_SE = generic.fGetImageUrl(@ProductId, @AppName_SE),
			@ImageUrl_NO = generic.fGetImageUrl(@ProductId, @AppName_NO),
			@ImageUrl_DA = generic.fGetImageUrl(@ProductId, @AppName_DA),
			@ImageUrl_FI = generic.fGetImageUrl(@ProductId, @AppName_FI),
			@ImageUrl_FR = generic.fGetImageUrl(@ProductId, @AppName_FR),
			@ImageUrl_NL = generic.fGetImageUrl(@ProductId, @AppName_NL)


		-- Get titles
		SET @Title_SE = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_SE)
		SET @Title_NO = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_NO)
		SET @Title_DA = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_DA)
		SET @Title_FI = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_FI)
		SET @Title_FR = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_FR)
		SET @Title_NL = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_NL)


		-- Insert avail product info
		INSERT INTO export.tAvailProductData (
			[ProductId],
			[PriceSE],
			[PriceDK],
			[PriceNO],
			[PriceFI],
			[PriceFR],
			[PriceNL],
			[DiscountPriceSE],
			[DiscountPriceDK],
			[DiscountPriceNO],
			[DiscountPriceFI],
			[DiscountPriceFR],
			[DiscountPriceNL],
			[CategoryUrlSE],
			[CategoryUrlNO],
			[CategoryUrlDA],
			[CategoryUrlFI],
			[CategoryUrlFR],
			[CategoryUrlNL],
			[ImageUrlSE],
			[ImageUrlNO],
			[ImageUrlDA],
			[ImageUrlFI],
			[ImageUrlFR],
			[ImageUrlNL],
			[TitleSE],
			[TitleNO],
			[TitleDA],
			[TitleFI],
			[TitleFR],
			[TitleNL],
			[NumberInStock]
		) VALUES (
			@ProductId,--[ProductId],
			@Price_SE,--[PriceSE],
			@Price_DK,--[PriceDK],
			@Price_NO,--[PriceNO],
			@Price_FI,--[PriceFI],
			@Price_FR,--[PriceFR],
			@Price_NL,--[PriceNL],
			COALESCE(@DiscountPrice_SE, @Price_SE),--[DiscountPriceSE],
			COALESCE(@DiscountPrice_DK, @Price_DK),--[DiscountPriceDK],
			COALESCE(@DiscountPrice_NO, @Price_NO),--[DiscountPriceNO],
			COALESCE(@DiscountPrice_FI, @Price_FI),--[DiscountPriceFI],
			COALESCE(@DiscountPrice_FR, @Price_FR),--[DiscountPriceFR],
			COALESCE(@DiscountPrice_NL, @Price_NL),--[DiscountPriceNL],
			@CategoryUrl_SE,--[CategoryUrlSE],
			@CategoryUrl_NO,--[CategoryUrlNO],
			@CategoryUrl_DA,--[CategoryUrlDA],
			@CategoryUrl_FI,--[CategoryUrlFI],
			@CategoryUrl_FR,--[CategoryUrlFR],
			@CategoryUrl_NL,--[CategoryUrlNL],
			@ImageUrl_SE,--[ImageUrlSE],
			@ImageUrl_NO,--[ImageUrlNO],
			@ImageUrl_DA,--[ImageUrlDA],
			@ImageUrl_FI,--[ImageUrlFI],
			@ImageUrl_FR,--[ImageUrlFR],
			@ImageUrl_NL,--[ImageUrlNL],
			COALESCE(@Title_SE, @Title),--[TitleSE],
			COALESCE(@Title_NO, @Title),--[TitleNO],
			COALESCE(@Title_DA, @Title),--[TitleDA],
			COALESCE(@Title_FI, @Title),--[TitleFI],
			COALESCE(@Title_FR, @Title),--[TitleFR],
			COALESCE(@Title_NL, @Title),--[TitleNL],
			@NumberInStock--[NumberInStock]
		)

		FETCH NEXT FROM product_cursor INTO @ProductId
	END

	CLOSE product_cursor;
	DEALLOCATE product_cursor;

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pAvailGetProductData]'
GO
ALTER PROCEDURE [lekmer].[pAvailGetProductData]
AS
BEGIN
	SET NOCOUNT ON;
	
	EXEC [lekmer].[pAvailPrepareProductData]
	
	SELECT * FROM export.tAvailProductData
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tProductSize_NumberInStock(ProductId)] on [lekmer].[tProductSize]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductSize_NumberInStock(ProductId)] ON [lekmer].[tProductSize] ([NumberInStock]) INCLUDE ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrderItemProduct_ErpId] on [order].[tOrderItemProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrderItemProduct_ErpId] ON [order].[tOrderItemProduct] ([ErpId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
