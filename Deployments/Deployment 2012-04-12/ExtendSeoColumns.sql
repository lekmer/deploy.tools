/*
Run this script on:

        (local).Heppo_212_0    -  This database will be modified

to synchronize it with:

        (local).Heppo_212_1

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 05.04.2012 14:17:50

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [product].[tProductSeoSettingTranslation]'
GO
ALTER TABLE [product].[tProductSeoSettingTranslation] ALTER COLUMN [Keywords] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[tProductSeoSetting]'
GO
ALTER TABLE [product].[tProductSeoSetting] ALTER COLUMN [Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [product].[tProductSeoSetting] ALTER COLUMN [Description] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [product].[tProductSeoSetting] ALTER COLUMN [Keywords] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductSeoSettingSave]'
GO

/*
*****************  Version 1  *****************
User: Roman G.		Date: 15.09.2008		Time: 17:00
Description:
      Save/Update SEOSettings for Product.
/***********************  Version 2  **********************
User: Roman D.		Date: 16/01/2009		Time: 14:35
Description: delete try - cath  block and error logging 
**********************************************************/
*/
ALTER PROCEDURE [product].[pProductSeoSettingSave]
	@ProductId		INT,
	@Title			NVARCHAR(500),
	@Description	NVARCHAR(500),
	@Keywords		NVARCHAR(500)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[product].[tProductSeoSetting]
	SET
		[Title] = @Title,
		[Description] = @Description,
		[Keywords] = @Keywords
	WHERE
		[ProductId] = @ProductId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT [product].[tProductSeoSetting]
		(				
			[ProductId],
			[Title],
			[Description],
			[Keywords]
		)
		VALUES
		(
			@ProductId,
			@Title,
			@Description,
			@Keywords
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [test].[pProductSeoSettingInsert]'
GO

ALTER PROCEDURE [test].[pProductSeoSettingInsert]
	@ProductId INT,
	@Title NVARCHAR(500),
	@Description NVARCHAR(500),
	@Keywords NVARCHAR(500)
AS
BEGIN
	INSERT INTO product.tProductSeoSetting
	(
		ProductId,
		Title,
		[Description],
		Keywords
	)
	VALUES
	(
		@ProductId,
		@Title,
		@Description,
		@Keywords
	)

	INSERT INTO product.tProductSeoSettingTranslation
	(
		ProductId,
		LanguageId,
		Title,
		[Description],
		Keywords
	)
	SELECT
		@ProductId,
		LanguageId,
		@Title,
		@Description,
		@Keywords
	FROM
		core.tLanguage
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[tContentPageSeoSettingTranslation]'
GO
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ALTER COLUMN [Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ALTER COLUMN [Description] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ALTER COLUMN [Keywords] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[tContentPageSeoSetting]'
GO
ALTER TABLE [sitestructure].[tContentPageSeoSetting] ALTER COLUMN [Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [sitestructure].[tContentPageSeoSetting] ALTER COLUMN [Description] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [sitestructure].[tContentPageSeoSetting] ALTER COLUMN [Keywords] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[pContentPageSeoSettingSave]'
GO

/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 17:00
Description:	Created
*/

ALTER PROCEDURE [sitestructure].[pContentPageSeoSettingSave]
	@ContentNodeId	INT,
	@Title			NVARCHAR(500),
	@Description	NVARCHAR(500),
	@Keywords		NVARCHAR(500)
AS
BEGIN
	UPDATE
		[sitestructure].[tContentPageSeoSetting]
	SET
		[Title]			= @Title,
		[Description]	= @Description,
		[Keywords]		= @Keywords
	WHERE
		[ContentNodeId] = @ContentNodeId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [sitestructure].[tContentPageSeoSetting]
		(
			[ContentNodeId],
			[Title],
			[Description],
			[Keywords]
		)
		VALUES
		(
			@ContentNodeId,
			@Title,
			@Description,
			@Keywords
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_GenerateSeoSettingsSeoTranslation]'
GO

ALTER PROCEDURE [integration].[usp_GenerateSeoSettingsSeoTranslation]

AS
begin
	set nocount on
	begin try
		begin transaction
		
		declare @Data nvarchar (500)
				
		-- DEFAULT --
		INSERT INTO product.tProductSeoSetting(ProductId)
		SELECT
			p.ProductId
		FROM
			product.tProduct p
		WHERE
			p.ProductId not in (select ProductId 
									from product.tProductSeoSetting)
		
		UPDATE
			pss
		SET
			pss.Title = (case when len('Skor | ' + b.Title + ':' + p.Title  + ' | Heppo.se') > 66 then 
			SUBSTRING(('Skor | ' + b.Title + ':' + p.Title  + ' | Heppo.se'), 8, 66) else 
			('Skor | ' + b.Title + ':' + p.Title  + ' | Heppo.se') end),
			
			--pss.[Description] = (case when len('Köp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens största skobutiker på nätet. Gratis frakt & fria byten med 30 dagar öppet köp.') > 160 then
			--SUBSTRING(('Köp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens största skobutiker på nätet. Gratis frakt & fria byten med 30 dagar öppet köp.'), 1, 160) else 
			--('Köp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens största skobutiker på nätet. Gratis frakt & fria byten med 30 dagar öppet köp.') end),
			pss.[Description] = 'Köp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens största skobutiker på nätet. Gratis frakt & fria byten med 30 dagar öppet köp.',
			
			pss.Keywords = 'Skor, ' + b.Title + ' ' + p.Title + ' ' + 
			(select top 1(Title) from product.tCategory where ParentCategoryId = c.ParentCategoryId) + ' ' 
			+ c.Title
			--select *
		FROM
			product.tProductSeoSetting pss
				inner join lekmer.tLekmerProduct l
					on pss.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
		WHERE
			(pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = '')
			or (pss.Keywords is null or pss.Keywords = '')
			
		
		-- SWEDEN --
		declare @languageIdSV int
		set @languageIdSV = (select LanguageId from core.tLanguage where ISO = 'SV')
		
		insert into product.tProductSeoSettingTranslation(ProductId, LanguageId)
		select
			p.ProductId,
			@languageIdSV
		from
			product.tProduct p
		WHERE
			not exists (select 1
							from product.tProductSeoSettingTranslation n
							where n.ProductId = p.ProductId and
							n.LanguageId = @languageIdSV)
		
		update
			psst
		set
			psst.Title = (case when len('Skor | ' + b.Title + ':' + p.Title  + ' | Heppo.se') > 66 then 
			SUBSTRING(('Skor | ' + b.Title + ':' + p.Title  + ' | Heppo.se'), 8, 66) else 
			('Skor | ' + b.Title + ':' + p.Title  + ' | Heppo.se') end),
			
			--psst.[Description] = (case when len('Köp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens största skobutiker på nätet. Gratis frakt & fria byten med 30 dagar öppet köp.') > 160 then
			--SUBSTRING(('Köp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens största skobutiker på nätet. Gratis frakt & fria byten med 30 dagar öppet köp.'), 1, 160) else 
			--('Köp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens största skobutiker på nätet. Gratis frakt & fria byten med 30 dagar öppet köp.') end),
			psst.[Description] = 'Köp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens största skobutiker på nätet. Gratis frakt & fria byten med 30 dagar öppet köp.',
			
			
			psst.Keywords = 'Skor, ' + b.Title + ' ' + p.Title + ' ' + 
			(select top 1(Title) from product.tCategory where ParentCategoryId = c.ParentCategoryId) + ' ' 
			+ c.Title
		from
			product.tProductSeoSettingTranslation psst
				inner join lekmer.tLekmerProduct l
					on psst.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
		where 
			LanguageId = (select LanguageId from core.tLanguage where ISO = 'SV')
			AND 
				(
				(psst.Title is null or psst.Title = '')
				or (psst.[Description] is null or psst.[Description] = '')
				or (psst.Keywords is null or psst.Keywords = '')
				)
		
		
		-- NORWAY --
		declare @languageIdNO int
		set @languageIdNO = (select LanguageId from core.tLanguage where ISO = 'NO')
		
		insert into product.tProductSeoSettingTranslation(ProductId, LanguageId)
		select
			p.ProductId,
			@languageIdNO
		from
			product.tProduct p
		WHERE
			not exists (select 1
							from product.tProductSeoSettingTranslation n
							where n.ProductId = p.ProductId and
							n.LanguageId = @languageIdNO)
						
																											
		update
			psst
		set		
			psst.Title = 'Sko | ' + b.Title + ':' + p.Title  + ' | Heppo.no',
			psst.[Description] = 'Kjøp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens største skobutikker på nettet. Gratis frakt og fri bytterett med 30 dagers åpent kjøp.',
			psst.Keywords = 'Sko, ' + b.Title + ' ' + p.Title + ' ' + 
			(select top 1(Title) from product.tCategory where ParentCategoryId = c.ParentCategoryId) + ' ' 
			+ c.Title
		from
			product.tProductSeoSettingTranslation psst
				inner join lekmer.tLekmerProduct l
					on psst.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
		where 
			LanguageId = (select LanguageId from core.tLanguage where ISO = 'NO')
			AND 
				(
				(psst.Title is null or psst.Title = '')
				or (psst.[Description] is null or psst.[Description] = '')
				or (psst.Keywords is null or psst.Keywords = '')
				)
		
		
		-- NORWAY --
		declare @languageIdDA int
		set @languageIdDA = (select LanguageId from core.tLanguage where ISO = 'DA')
		
		insert into product.tProductSeoSettingTranslation(ProductId, LanguageId)
		select
			p.ProductId,
			@languageIdDA
		from
			product.tProduct p
		WHERE
			not exists (select 1
							from product.tProductSeoSettingTranslation n
							where n.ProductId = p.ProductId and
							n.LanguageId = @languageIdDA)
							
		update
			psst
		set
			psst.Title = 'Sko | ' + b.Title + ':' + p.Title  + ' | Heppo.dk',
			psst.[Description] = 'Køb ' + b.Title + ' : ' + p.Title + '  hos en af Nordens største skobutikker på nettet. Gratis levering & fri ombytning med 30 dages åbent køb.',
			psst.Keywords = 'Sko, ' + b.Title + ' ' + p.Title + ' ' + 
			(select top 1(Title) from product.tCategory where ParentCategoryId = c.ParentCategoryId) + ' ' 
			+ c.Title
		from
			product.tProductSeoSettingTranslation psst
				inner join lekmer.tLekmerProduct l
					on psst.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
		where 
			LanguageId = (select LanguageId from core.tLanguage where ISO = 'DA')
			AND 
				(
				(psst.Title is null or psst.Title = '')
				or (psst.[Description] is null or psst.[Description] = '')
				or (psst.Keywords is null or psst.Keywords = '')
				)
		
		
		-- NORWAY --
		declare @languageIdFI int
		set @languageIdFI = (select LanguageId from core.tLanguage where ISO = 'FI')
		
		insert into product.tProductSeoSettingTranslation(ProductId, LanguageId)
		select
			p.ProductId,
			@languageIdFI
		from
			product.tProduct p
		WHERE
			not exists (select 1
							from product.tProductSeoSettingTranslation n
							where n.ProductId = p.ProductId and
							n.LanguageId = @languageIdFI)
		
		
		update
			psst
		set
			psst.Title = 'Kengät | ' + b.Title + ':' + p.Title  + ' | Heppo.fi',
			psst.[Description] = 'Osta ' + b.Title + ' : ' + p.Title + ' pohjoismaiden suurimmasta online-kenkäkaupasta. Ilmainen toimitus ja 30 päivän vaihto-oikeus.',
			psst.Keywords = 'Kengät ' + b.Title + ' ' + p.Title + ' ' + 
			(select top 1(Title) from product.tCategory where ParentCategoryId = c.ParentCategoryId) + ' ' 
			+ c.Title
		from
			product.tProductSeoSettingTranslation psst
				inner join lekmer.tLekmerProduct l
					on psst.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
		where 
			LanguageId = (select LanguageId from core.tLanguage where ISO = 'FI')
			AND 
				(
				(psst.Title is null or psst.Title = '')
				or (psst.[Description] is null or psst.[Description] = '')
				or (psst.Keywords is null or psst.Keywords = '')
				)
					
					
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
