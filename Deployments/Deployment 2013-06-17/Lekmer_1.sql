SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[tLekmerBlock]'
GO
ALTER TABLE [lekmer].[tLekmerBlock] ADD
[ShowOnDesktop] [bit] NULL,
[ShowOnMobile] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[vCustomBlockSecure]'
GO


ALTER VIEW [sitestructure].[vCustomBlockSecure]
AS
SELECT
	[b].*,
	[l].[StartDate] AS 'Block.StartDate',
	[l].[EndDate] AS 'Block.EndDate',
	[l].[StartDailyIntervalMinutes] AS 'Block.StartDailyIntervalMinutes',
	[l].[EndDailyIntervalMinutes] AS 'Block.EndDailyIntervalMinutes',
	[l].[ShowOnDesktop] AS 'Block.ShowOnDesktop',
	[l].[ShowOnMobile] AS 'Block.ShowOnMobile'
FROM
	[sitestructure].[vBlockSecure] b
	LEFT JOIN [lekmer].[tLekmerBlock] l ON b.[Block.BlockId] = l.[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[vCustomBlock]'
GO

ALTER VIEW [sitestructure].[vCustomBlock]
AS
SELECT
	[b].*,
	[l].[StartDate] AS 'Block.StartDate',
	[l].[EndDate] AS 'Block.EndDate',
	[l].[StartDailyIntervalMinutes] AS 'Block.StartDailyIntervalMinutes',
	[l].[EndDailyIntervalMinutes] AS 'Block.EndDailyIntervalMinutes',
	[l].[ShowOnDesktop] AS 'Block.ShowOnDesktop',
	[l].[ShowOnMobile] AS 'Block.ShowOnMobile'
FROM
	[sitestructure].[vBlock] b
	LEFT JOIN [lekmer].[tLekmerBlock] l ON b.[Block.BlockId] = l.[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [media].[tImageSize]'
GO
ALTER TABLE [media].[tImageSize] ADD
[MobileWidth] [int] NULL,
[MobileHeight] [int] NULL,
[MobileQuality] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [media].[vImageSize]'
GO


ALTER VIEW [media].[vImageSize]
AS
SELECT 
	 [ImageSizeId] 'ImageSize.Id',
	 [Title] 'ImageSize.Title',
	 [CommonName] 'ImageSize.CommonName',
	 [Width] 'ImageSize.Width',
	 [Height] 'ImageSize.Height',
	 [Quality] 'ImageSize.Quality',
	 [MobileWidth] 'ImageSize.MobileWidth',
	 [MobileHeight] 'ImageSize.MobileHeight',
	 [MobileQuality] 'ImageSize.MobileQuality'
FROM   
	[media].[tImageSize]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pLekmerBlockSave]'
GO
ALTER PROCEDURE [lekmer].[pLekmerBlockSave]
	@BlockId INT,
	@StartDate DATETIME = NULL,
	@EndDate DATETIME = NULL,
	@StartDailyIntervalMinutes INT = NULL,
	@EndDailyIntervalMinutes INT = NULL,
	@ShowOnDesktop BIT = NULL,
	@ShowOnMobile BIT = NULL
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tLekmerBlock]
	SET	
		[StartDate] = @StartDate,
		[EndDate] = @EndDate,
		[StartDailyIntervalMinutes] = @StartDailyIntervalMinutes,
		[EndDailyIntervalMinutes] = @EndDailyIntervalMinutes,
		[ShowOnDesktop] = @ShowOnDesktop,
		[ShowOnMobile] = @ShowOnMobile
	WHERE
		[BlockId] = @BlockId
  
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[lekmer].[tLekmerBlock]
				( [BlockId],
				  [StartDate],
				  [EndDate],
				  [StartDailyIntervalMinutes],
				  [EndDailyIntervalMinutes],
				  [ShowOnDesktop],
				  [ShowOnMobile]
				)
		VALUES
				( @BlockId,
				  @StartDate,
				  @EndDate,
				  @StartDailyIntervalMinutes,
				  @EndDailyIntervalMinutes,
				  @ShowOnDesktop,
				  @ShowOnMobile
				)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
