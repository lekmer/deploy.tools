--begin tran

DECLARE @tTemplateIdsForChanging TABLE (DevTemplateId INT, LiveTemplateId INT)

INSERT INTO @tTemplateIdsForChanging ([DevTemplateId], [LiveTemplateId])
VALUES (305,305),(375,375),(542,542),(1000205,1000205),(1000279,1000279),(1000415,1000340),(1000416,1000341),(1000417,1000342),(1000418,1000343),(1000419,1000344),(1000420,1000345),
(1000421,1000346),(1000422,1000347),(1000423,1000348),(1000424,1000349),(1000425,1000350),(1000426,1000351),(1000429,1000352),(1000428,1000353),(1000430,1000354),(1000427,1000355)

------------------------------------------------------------------------------------------

DECLARE 
		@DevTemplateId INT,
		@LiveTemplateId INT

DECLARE cur_templates CURSOR FAST_FORWARD FOR
	SELECT 
		[DevTemplateId],
		[LiveTemplateId]
	FROM
		@tTemplateIdsForChanging
	
OPEN cur_templates
FETCH NEXT FROM cur_templates
	INTO 
		@DevTemplateId,
		@LiveTemplateId
	
WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE tfLive
		SET tfLive.[Content] = ( SELECT [tfDev].[Content] FROM [Lekmer_dev].[template].[tTemplateFragment] tfDev
								 WHERE tfDev.[TemplateId] = @DevTemplateId AND tfDev.[ModelFragmentId] = [tfLive].ModelFragmentId),
			tfLive.[AlternateContent] = ( SELECT [tfDev].[AlternateContent] FROM [Lekmer_dev].[template].[tTemplateFragment] tfDev
								 WHERE tfDev.[TemplateId] = @DevTemplateId AND tfDev.[ModelFragmentId] = [tfLive].ModelFragmentId)
		FROM [Lekmer].[template].[tTemplateFragment] tfLive
		WHERE tfLive.[TemplateId] = @LiveTemplateId
		
		FETCH NEXT FROM cur_templates
		INTO 
			@DevTemplateId,
			@LiveTemplateId
	END
CLOSE cur_templates
DEALLOCATE cur_templates

--rollback