--DROP TABLE [dbo].[TemplateMappingTemp]
--DROP TABLE [dbo].[TemplateMappingLiteTemp]

/****** Object:  Table [dbo].[TemplateMappingTemp]    Script Date: 05/10/2013 17:32:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemplateMappingTemp](
	[OldTemplateId] [nvarchar](255) NULL,
	[NewTemplateId] [nvarchar](255) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000207', N'1000386')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000273', N'1000386')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000004', N'1000387')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000272', N'1000387')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000182', N'1000433')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000181', N'1000367')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000180', N'1000374')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000309', N'1000428')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000310', N'1000429')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000312', N'1000430')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000311', N'1000431')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000308', N'1000412')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000239', N'1000403')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000240', N'1000405')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000281', N'1000362')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000241', N'1000391')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'24', N'1000365')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000121', N'1000413')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000038', N'1000417')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000073', N'1000364')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000226', N'1000401')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000303', N'1000401')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000243', N'1000396')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000144', N'1000415')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000032', N'1000409')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000027', N'1000402')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000307', N'1000402')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000035', N'1000398')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000227', N'1000398')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000302', N'1000398')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'328', N'1000399')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000225', N'1000399')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000301', N'1000399')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000021', N'1000400')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'840', N'1000394')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000208', N'1000392')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000220', N'1000392')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'535', N'1000375')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'200', N'1000368')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000291', N'1000359')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'37', N'1000360')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'346', N'1000395')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000275', N'1000435')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000249', N'1000371')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000305', N'1000363')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000194', N'1000366')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000231', N'1000411')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'112', N'1000389')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'259', N'1000370')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000002', N'1000388')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000246', N'1000434')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000049', N'1000397')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000304', N'1000397')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000059', N'1000410')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000233', N'1000372')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'28', N'1000361')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000276', N'1000393')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000193', N'1000414')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000016', N'1000427')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000081', N'1000390')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000103', N'1000408')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000097', N'1000422')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000095', N'1000423')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000086', N'1000424')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000104', N'1000419')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000105', N'1000420')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000106', N'1000421')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000294', N'1000407')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000082', N'1000404')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000357', N'1000436')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000017', N'1000369')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000261', N'1000379')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000296', N'1000384')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000298', N'1000381')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000295', N'1000382')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000242', N'1000406')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000108', N'1000416')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000229', N'1000380')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000221', N'1000425')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000237', N'1000385')
INSERT [dbo].[TemplateMappingTemp] ([OldTemplateId], [NewTemplateId]) VALUES (N'1000297', N'1000383')



DECLARE 
		@OldTemplateId INT,
		@NewTemplateId INT,
		@OldModelId INT,
		@NewModelId INT

DECLARE cur_templates CURSOR FAST_FORWARD FOR
	SELECT 
		[OldTemplateId],
		[NewTemplateId] 
	FROM
		[dbo].[TemplateMappingTemp]
	
OPEN cur_templates
FETCH NEXT FROM cur_templates
	INTO 
		@OldTemplateId,
		@NewTemplateId
	
WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @OldModelId = (select ModelId from template.tTemplate where TemplateId = @OldTemplateId)
		SET @NewModelId = (select ModelId from template.tTemplate where TemplateId = @NewTemplateId)
		
		if (@OldModelId <> @NewModelId)
			Print 'false'
		
		FETCH NEXT FROM cur_templates
		INTO 
			@OldTemplateId,
			@NewTemplateId
	END
CLOSE cur_templates
DEALLOCATE cur_templates

--------------------------------------------------------------------------------------------------------

select t.ModelId, t2.ModelId from [dbo].[TemplateMappingTemp] tm
Left join template.tTemplate t on t.TemplateId = tm.OldTemplateId
left join template.tTemplate t2 on t2.TemplateId = tm.NewTemplateId
where t.ModelId IS NULL OR t2.ModelId IS NULL OR t.ModelId <> t2.ModelId

--------------------------------------------------------------------------------------------------------

/****** Object:  Table [dbo].[TemplateMappingLiteTemp]    Script Date: 05/10/2013 17:32:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemplateMappingLiteTemp](
	[TemplateIdTest] [float] NULL,
	[TemplateIdLive] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000334, 1000359)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000335, 1000360)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000336, 1000361)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000337, 1000362)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000338, 1000363)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000339, 1000364)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000340, 1000365)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000341, 1000366)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000342, 1000367)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000343, 1000368)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000344, 1000369)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000345, 1000370)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000346, 1000371)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000347, 1000372)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000348, 1000373)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000349, 1000374)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000350, 1000375)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000351, 1000376)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000352, 1000377)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000353, 1000378)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000354, 1000379)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000355, 1000380)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000356, 1000381)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000357, 1000382)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000358, 1000383)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000359, 1000384)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000360, 1000385)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000361, 1000386)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000362, 1000387)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000363, 1000388)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000364, 1000389)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000365, 1000390)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000366, 1000391)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000367, 1000392)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000368, 1000393)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000369, 1000394)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000370, 1000395)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000371, 1000396)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000372, 1000397)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000373, 1000398)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000374, 1000399)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000375, 1000400)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000376, 1000401)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000377, 1000402)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000378, 1000403)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000379, 1000404)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000380, 1000405)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000381, 1000406)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000382, 1000407)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000383, 1000408)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000384, 1000409)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000385, 1000410)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000386, 1000411)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000387, 1000412)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000388, 1000413)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000389, 1000414)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000390, 1000415)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000391, 1000416)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000392, 1000417)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000393, 1000418)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000394, 1000419)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000395, 1000420)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000396, 1000421)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000397, 1000422)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000398, 1000423)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000399, 1000424)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000400, 1000425)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000402, 1000426)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000406, 1000427)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000407, 1000428)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000408, 1000429)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000409, 1000430)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000410, 1000431)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000411, 1000432)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000412, 1000433)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000413, 1000434)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000414, 1000435)
INSERT [dbo].[TemplateMappingLiteTemp] ([TemplateIdTest], [TemplateIdLive]) VALUES (1000431, 1000436)