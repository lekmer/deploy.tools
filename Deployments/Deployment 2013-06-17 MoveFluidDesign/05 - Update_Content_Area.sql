--begin tran

DECLARE @tCNIds TABLE (Id INT)
INSERT INTO @tCNIds
select cp.ContentNodeId 
from sitestructure.tContentPage cp
where cp.TemplateId in (	select Distinct TemplateId 
							from sitestructure.tContentArea 
							where TemplateId IN (SELECT distinct NewTemplateId FROM [dbo].[TemplateMappingTemp]))


DECLARE @CNId INT

DECLARE cur_cn CURSOR FAST_FORWARD FOR
SELECT Id FROM @tCNIds
OPEN cur_cn
FETCH NEXT FROM cur_cn
INTO @CNId
WHILE @@FETCH_STATUS = 0
BEGIN
	
	
	UPDATE b SET
	b.ContentAreaId = CASE WHEN
					  (
						SELECT ca.ContentAreaId FROM [sitestructure].[tContentArea] ca
						WHERE ca.CommonName = (SELECT caOld.CommonName FROM [sitestructure].[tContentArea] caOld WHERE caOld.ContentAreaId = b.ContentAreaId)
						AND TemplateId = (SELECT TemplateId from sitestructure.tContentPage where ContentNodeId = @CNId)
					  ) IS NOT NULL 
					  THEN 
					  (
					    SELECT ca.ContentAreaId FROM [sitestructure].[tContentArea] ca
						WHERE ca.CommonName = (SELECT caOld.CommonName FROM [sitestructure].[tContentArea] caOld WHERE caOld.ContentAreaId = b.ContentAreaId)
						AND TemplateId = (SELECT TemplateId from sitestructure.tContentPage where ContentNodeId = @CNId)
					  ) 
					  ELSE 
					  (
					    SELECT top(1) ca.ContentAreaId FROM [sitestructure].[tContentArea] ca
						WHERE TemplateId = (SELECT TemplateId from sitestructure.tContentPage where ContentNodeId = @CNId)
					  )
					  END
	FROM sitestructure.tBlock b
	WHERE b.ContentNodeId = @CNId
	--AND b.TemplateId IN (SELECT distinct NewTemplateId FROM [dbo].[TemplateMappingTemp])
	--AND b.ContentNodeId <> 559 AND b.ContentNodeId <> 1009829 AND b.ContentNodeId <> 1009909 AND b.ContentNodeId <> 1013511 AND b.ContentNodeId <> 1013776

	FETCH NEXT FROM cur_cn
	INTO @CNId
END

CLOSE cur_cn
DEALLOCATE cur_cn

--ROLLBACK