--begin tran

DECLARE @tNewTemplateIds TABLE (TemplateId INT)

INSERT INTO @tNewTemplateIds
SELECT [TemplateId]
FROM [Lekmer_dev].[template].[tTemplate]
WHERE [Title] LIKE '%2013%'
AND [Title] NOT LIKE '%css%'

INSERT INTO @tNewTemplateIds
SELECT [TemplateId]
FROM [Lekmer_dev].[template].[tTemplate]
WHERE [Title] = 'Review list most helpful rating - product page - 2'

SELECT * FROM @tNewTemplateIds

------------------------------------------------------------------------------------------

DECLARE @Id INT
DECLARE @NewTemplateId INT

DECLARE cur_Templates CURSOR FAST_FORWARD FOR
SELECT [TemplateId] FROM @tNewTemplateIds GROUP BY [TemplateId]

OPEN cur_Templates
FETCH NEXT FROM cur_Templates
INTO @Id
WHILE @@FETCH_STATUS = 0

BEGIN
	INSERT INTO [Lekmer].[template].[tTemplate] ([ModelId],[Title],[UseAlternate],[ImageFileName])
	SELECT [ModelId], [Title], [UseAlternate], [ImageFileName] FROM [Lekmer_dev].[template].[tTemplate] WHERE TemplateId = @Id
	
	SET @NewTemplateId = (SELECT IDENT_CURRENT('[Lekmer].[template].[tTemplate]'))
	
	INSERT INTO [Lekmer].[template].[tTemplateSetting] ([TemplateId],[ModelSettingId],[Value],[AlternateValue])
	SELECT @NewTemplateId, ModelSettingId, Value, AlternateValue FROM [Lekmer_dev].[template].[tTemplateSetting] WHERE TemplateId = @Id
	
	INSERT INTO [Lekmer].[template].[tTemplateFragment] ([TemplateId],[ModelFragmentId],[Content],[AlternateContent])
	SELECT @NewTemplateId, ModelFragmentId, Content, AlternateContent FROM [Lekmer_dev].[template].[tTemplateFragment] WHERE TemplateId = @Id
	
	INSERT INTO [Lekmer].[sitestructure].[tContentArea] ([TemplateId],[Title],[CommonName])
	SELECT @NewTemplateId, Title, CommonName FROM [Lekmer_dev].[sitestructure].[tContentArea] WHERE TemplateId = @Id

	FETCH NEXT FROM cur_Templates
	INTO @Id
END

CLOSE cur_Templates
DEALLOCATE cur_Templates

--rollback