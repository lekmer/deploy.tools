--begin tran

INSERT INTO [Lekmer].[template].[tInclude] (
	[IncludeFolderId],
	[CommonName],
	[Content],
	[Description]
)
SELECT 
	[IncludeFolderId],
	[CommonName],
	[Content],
	[Description]
FROM
	[Lekmer_dev].[template].[tInclude]
WHERE 
	[CommonName] LIKE '%2013%' 
ORDER BY
	[CommonName]

--rollback