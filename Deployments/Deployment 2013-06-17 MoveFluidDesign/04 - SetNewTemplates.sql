DECLARE 
		@OldTemplateId INT,
		@NewTemplateId INT

DECLARE cur_templates CURSOR FAST_FORWARD FOR
	SELECT 
		[OldTemplateId],
		[NewTemplateId]
	FROM
		[dbo].[TemplateMappingTemp]
	
OPEN cur_templates
FETCH NEXT FROM cur_templates
	INTO 
		@OldTemplateId,
		@NewTemplateId
	
WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRANSACTION
		
		UPDATE [campaignlek].[tGiftCardViaMailCartAction]
		SET TemplateId = @NewTemplateId
		WHERE [TemplateId] = @OldTemplateId

		UPDATE [campaignlek].[tGiftCardViaMailProductAction]
		SET TemplateId = @NewTemplateId
		WHERE [TemplateId] = @OldTemplateId

		UPDATE [lekmer].[tBlockImageRotator]
		SET [SecondaryTemplateId] = @NewTemplateId
		WHERE [SecondaryTemplateId] = @OldTemplateId

		UPDATE [lekmer].[tBlockProductFilter]
		SET [SecondaryTemplateId] = @NewTemplateId
		WHERE [SecondaryTemplateId] = @OldTemplateId

		UPDATE [productlek].[tBlockProductSearchEsalesResult]
		SET [SecondaryTemplateId] = @NewTemplateId
		WHERE [SecondaryTemplateId] = @OldTemplateId

		UPDATE [sitestructure].[tBlock]
		SET TemplateId = @NewTemplateId
		WHERE [TemplateId] = @OldTemplateId
		
		--UPDATE [sitestructure].[tContentArea] 
		--SET TemplateId = @NewTemplateId
		--WHERE [TemplateId] = @OldTemplateId

		UPDATE [sitestructure].[tContentPage]
		SET TemplateId = @NewTemplateId
		WHERE [TemplateId] = @OldTemplateId
		UPDATE [sitestructure].[tContentPage]
		SET [MasterTemplateId] = @NewTemplateId
		WHERE [MasterTemplateId] = @OldTemplateId

		UPDATE [template].[tModel]
		SET [DefaultTemplateId] = @NewTemplateId
		WHERE [DefaultTemplateId] = @OldTemplateId

		UPDATE [template].[tThemeModel]
		SET [TemplateId] = @NewTemplateId
		WHERE [TemplateId] = @OldTemplateId
		
		UPDATE template.tTemplateFragment
		SET [Content] = (SELECT REPLACE([Content], 'TemplateId=' + CAST(@OldTemplateId AS NVARCHAR), 'TemplateId=9GROM9' + CAST(@NewTemplateId AS NVARCHAR)))
		WHERE EXISTS (select * from template.tTemplateFragment where Content like '%TemplateId=' + CAST(@OldTemplateId AS NVARCHAR) + '%')
		AND TemplateId NOT IN (SELECT NewTemplateId from [dbo].[TemplateMappingTemp])
		
		UPDATE template.[tInclude]
		SET [Content] = (SELECT REPLACE([Content], 'TemplateId=' + CAST(@OldTemplateId AS NVARCHAR), 'TemplateId=9GROM9' + CAST(@NewTemplateId AS NVARCHAR)))
		WHERE EXISTS (select * from template.[tInclude] where Content like '%TemplateId=' + CAST(@OldTemplateId AS NVARCHAR) + '%')
		AND IncludeId NOT IN (select IncludeId from template.tInclude where CommonName like '%2013%')
		
		
		--UPDATE template.tTemplateFragment
		--SET [Content] = (SELECT REPLACE([Content], 'TemplateId=' + CAST(@NewTempTemplateId AS NVARCHAR), 'TemplateId=' + CAST(@NewTemplateId AS NVARCHAR)))
		--WHERE EXISTS (select * from template.tTemplateFragment where Content like '%TemplateId=' + CAST(@NewTempTemplateId AS NVARCHAR) + '%')
		--AND TemplateId IN (SELECT NewTemplateId from [dbo].[TemplateMappingTemp])
		
		--UPDATE template.[tInclude]
		--SET [Content] = (SELECT REPLACE([Content], 'TemplateId=' + CAST(@NewTempTemplateId AS NVARCHAR), 'TemplateId=' + CAST(@NewTemplateId AS NVARCHAR)))
		--WHERE EXISTS (select * from template.[tInclude] where Content like '%TemplateId=' + CAST(@NewTempTemplateId AS NVARCHAR) + '%')
		--AND IncludeId IN (select IncludeId from template.tInclude where CommonName like '%2013%')
		

		COMMIT
		
		FETCH NEXT FROM cur_templates
		INTO 
			@OldTemplateId,
			@NewTemplateId
	END
CLOSE cur_templates
DEALLOCATE cur_templates