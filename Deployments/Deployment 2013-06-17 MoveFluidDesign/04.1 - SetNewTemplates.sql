DECLARE 
		@TemplateIdTestId INT,
		@TemplateIdLiveId INT

DECLARE cur_templates CURSOR FAST_FORWARD FOR
	SELECT 
		[TemplateIdTest],
		[TemplateIdLive]
	FROM
		[dbo].[TemplateMappingLiteTemp]
	
OPEN cur_templates
FETCH NEXT FROM cur_templates
	INTO 
		@TemplateIdTestId,
		@TemplateIdLiveId
	
WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRANSACTION
		
		-- Check if TEST ID not included in LIVE ID
		UPDATE template.tTemplateFragment
		SET [Content] = (SELECT REPLACE([Content], 'TemplateId=' + CAST(@TemplateIdTestId AS NVARCHAR), 'TemplateId=9GROM9' + CAST(@TemplateIdLiveId AS NVARCHAR)))
		WHERE EXISTS (select * from template.tTemplateFragment where Content like '%TemplateId=' + CAST(@TemplateIdTestId AS NVARCHAR) + '%')
		AND TemplateId IN (SELECT NewTemplateId from [dbo].[TemplateMappingTemp])
		
		UPDATE template.[tInclude]
		SET [Content] = (SELECT REPLACE([Content], 'TemplateId=' + CAST(@TemplateIdTestId AS NVARCHAR), 'TemplateId=9GROM9' + CAST(@TemplateIdLiveId AS NVARCHAR)))
		WHERE EXISTS (select * from template.[tInclude] where Content like '%TemplateId=' + CAST(@TemplateIdTestId AS NVARCHAR) + '%')
		AND IncludeId IN (select IncludeId from template.tInclude where CommonName like '%2013%')
		

		COMMIT
		
		FETCH NEXT FROM cur_templates
		INTO 
			@TemplateIdTestId,
			@TemplateIdLiveId
	END
CLOSE cur_templates
DEALLOCATE cur_templates


UPDATE template.tTemplateFragment
SET [Content] = (SELECT REPLACE([Content], 'TemplateId=9GROM9', 'TemplateId='))

UPDATE template.[tInclude]
SET [Content] = (SELECT REPLACE([Content], 'TemplateId=9GROM9', 'TemplateId='))