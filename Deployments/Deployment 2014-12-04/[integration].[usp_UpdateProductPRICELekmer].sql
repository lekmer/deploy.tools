ALTER PROCEDURE [integration].[usp_UpdateProductPRICELekmer]
AS
BEGIN

	IF OBJECT_ID('tempdb..#tProductPrice') IS NOT NULL
		DROP TABLE #tProductPrice
		
	CREATE TABLE #tProductPrice
	(
		ProductId INT NOT NULL,
		PriceListId INT NOT NULL,
		PriceIncludingVat DECIMAL(16,2) NOT NULL,
		PriceExcludingVat DECIMAL(16,2) NOT NULL,
		VatPercentage DECIMAL(16,2) NOT NULL
		CONSTRAINT PK_#tProductPrice PRIMARY KEY(ProductId, PricelistId) WITH (IGNORE_DUP_KEY=ON)
	)
	
	BEGIN TRY

		PRINT 'Update prices prepare.';

		WITH #tempProductPrices AS
		(
			SELECT 
				lc.[ChannelId],
				SUBSTRING([HYarticleId], 5, 12) AS ProductHYID, -- '8065259-0000'
				CAST([Price]/100.0 AS DECIMAL(16,2)) AS PriceIncludingVat,
				CAST((([Price]/100.0) / (1.0+(ISNULL([tpp].[VatPercentage]/100.0, 25.0)/100.0))) AS DECIMAL(16,2)) AS PriceExcludingVat,
				CAST([tpp].[VatPercentage]/100.0 AS DECIMAL(16,2)) AS VatPercentage
			FROM
				[integration].[tempProductPrice] tpp
				LEFT OUTER JOIN [lekmer].[tLekmerChannel] lc ON lc.[ErpId] = LEFT([HYarticleId], 3) -- '001'
		),
		#stageProductPrices AS
		(
			SELECT
				[ChannelId],
				[ProductHYID],
				(SELECT MAX(Price) FROM (VALUES (pp.[PriceIncludingVat]), (0.02)) AS AllPrices(Price)) AS 'PriceIncludingVat',
				(SELECT MAX(Price) FROM (VALUES (pp.[PriceExcludingVat]), (0.01)) AS AllPrices(Price)) AS 'PriceExcludingVat',
				[VatPercentage]
			FROM
				[#tempProductPrices] pp
		)
		INSERT [#tProductPrice]
		(
			[ProductId],
			[PriceListId],
			[PriceIncludingVat],
			[PriceExcludingVat],
			[VatPercentage]
		)		
		SELECT
			DISTINCT
			pli.[ProductId],
			pli.[PriceListId],
			t.[PriceIncludingVat],
			t.[PriceExcludingVat],
			t.[VatPercentage]
		FROM 
			[#stageProductPrices] t
			INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[HYErpId] = t.[ProductHYID]
			INNER JOIN [product].[tPriceListItem] pli ON pli.[ProductId] = lp.[ProductId]
			INNER JOIN [product].[tPriceList] pl ON pl.[PriceListId] = pli.[PriceListId]
			INNER JOIN [product].[tProductModuleChannel] pmc ON pmc.[PriceListRegistryId] = pl.[PriceListRegistryId] AND pmc.[ChannelId] = t.[ChannelId]
		WHERE
			pli.[PriceIncludingVat] <> t.[PriceIncludingVat]
			OR
			pli.[PriceExcludingVat] <> t.[PriceExcludingVat]
		;

		PRINT 'Update prices.'

		BEGIN TRANSACTION
		
		UPDATE
			ppli
		SET
			ppli.[PriceIncludingVat] = t.[PriceIncludingVat],
			ppli.[PriceExcludingVat] = t.[PriceExcludingVat],
			ppli.[VatPercentage] = t.[VatPercentage]
		FROM
			product.[tPriceListItem] ppli
			INNER JOIN #tProductPrice t ON ppli.[ProductId] = t.[ProductId] AND ppli.[PriceListId] = t.[PriceListId]

		COMMIT
		
		PRINT 'Update prices done.'		
		
		IF OBJECT_ID('tempdb..#tProductPrice') IS NOT NULL
			DROP TABLE #tProductPrice
	
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH

END