/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 2/14/2014 3:04:20 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tEntity]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tFunctionType]

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]

-- Drop constraint FK_tModelFragmentRegion_tModel from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraint FK_tModelFragment_tModel from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraint FK_tModelSetting_tModel from [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModel]

-- Delete rows from [template].[tModelFragmentFunction]
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=101
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=104
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=196
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=197
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000043
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000044
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000046
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000047
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000504
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000505
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000507
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000508
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000613
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000614
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000622
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000623
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000631
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000632
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000641
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000642
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000643
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000644
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000738
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000739
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000740
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000741
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000742
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000743
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000744
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000745
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001356
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001357
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001358
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001359
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001360
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001361
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001364
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001365
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001366
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001367
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001368
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001369
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001370
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001371
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001395
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001396
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001397
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001398
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001399
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001400
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001407
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001408
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001409
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001410
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001411
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001412
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001421
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001422
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001423
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001424
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001425
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001426
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001427
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001428
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001443
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001444
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001445
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001446
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001447
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001448
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001455
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001456
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001457
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001458
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001459
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001460
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001461
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001462
-- Operation applied to 78 rows out of 78

-- Update rows in [template].[tModel]
UPDATE [template].[tModel] SET [Title]=N'Block save cart form', [CommonName]=N'BlockSaveCartForm' WHERE [ModelId]=1000065
UPDATE [template].[tModel] SET [Title]=N'Block saved cart', [CommonName]=N'BlockSavedCart' WHERE [ModelId]=1000067
-- Operation applied to 2 rows out of 2

-- Add rows to [template].[tEntity]
SET IDENTITY_INSERT [template].[tEntity] ON
INSERT INTO [template].[tEntity] ([EntityId], [CommonName], [Title]) VALUES (1000016, N'CartPrice', N'Cart price')
INSERT INTO [template].[tEntity] ([EntityId], [CommonName], [Title]) VALUES (1000017, N'CartItemPrice', N'Cart item price')
INSERT INTO [template].[tEntity] ([EntityId], [CommonName], [Title]) VALUES (1000018, N'CartItemFreePrice', N'Cart item free price')
SET IDENTITY_INSERT [template].[tEntity] OFF
-- Operation applied to 3 rows out of 3

-- Add rows to [template].[tEntityFunction]
SET IDENTITY_INSERT [template].[tEntityFunction] ON
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000238, 1000016, 1, N'Cart.PriceIncludingVatSummary', N'Channel decimals, possible rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000239, 1000016, 1, N'Cart.PriceExcludingVatSummary', N'Channel decimals, possible rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000240, 1000016, 1, N'Cart.VatSummary', N'Channel decimals, possible rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000241, 1000016, 1, N'Cart.PriceIncludingVatSummary_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000242, 1000016, 1, N'Cart.PriceExcludingVatSummary_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000243, 1000016, 1, N'Cart.VatSummary_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000244, 1000017, 1, N'Product.CampaignInfo.Price_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000245, 1000017, 1, N'Product.Price_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000246, 1000017, 1, N'CartItem.PriceIncludingVatSummary', N'Channel decimals, possible rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000247, 1000017, 1, N'CartItem.PriceExcludingVatSummary', N'Channel decimals, possible rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000248, 1000017, 1, N'CartItem.VatSummary', N'Channel decimals, possible rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000249, 1000017, 1, N'CartItem.PriceIncludingVatSummary_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000250, 1000017, 1, N'CartItem.PriceExcludingVatSummary_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000251, 1000017, 1, N'CartItem.VatSummary_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000252, 1000018, 1, N'Product.CampaignInfo.Price_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000253, 1000018, 1, N'Product.Price_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000254, 1000018, 1, N'CartItemOption.PriceIncludingVatSummary', N'Channel decimals, possible rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000255, 1000018, 1, N'CartItemOption.PriceExcludingVatSummary', N'Channel decimals, possible rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000257, 1000018, 1, N'CartItemOption.VatSummary', N'Channel decimals, possible rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000258, 1000018, 1, N'CartItemOption.PriceIncludingVatSummary_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000259, 1000018, 1, N'CartItemOption.PriceExcludingVatSummary_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000260, 1000018, 1, N'CartItemOption.VatSummary_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000273, 1, 1, N'Product.Price_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000274, 5, 1, N'Product.Price_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000275, 1, 1, N'Product.CampaignInfo.Price_Formatted', N'Two or less decimals, without rounding')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000276, 5, 1, N'Product.CampaignInfo.Price_Formatted', N'Two or less decimals, without rounding')
SET IDENTITY_INSERT [template].[tEntityFunction] OFF
-- Operation applied to 26 rows out of 26

-- Add rows to [template].[tModelFragmentEntity]
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (35, 1000016)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (37, 1000017)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1000061, 1000016)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1000062, 1000017)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001178, 1000018)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001179, 1000016)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001180, 1000017)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001356, 1000016)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001357, 1000017)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001364, 1000016)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001366, 1000017)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001382, 1000016)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001386, 1000016)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001387, 1000017)
-- Operation applied to 18 rows out of 21

-- Add constraints to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])

-- Add constraint FK_tModelFragmentRegion_tModel to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] WITH CHECK ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraint FK_tModelFragment_tModel to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] WITH CHECK ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraint FK_tModelSetting_tModel to [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] WITH CHECK ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
COMMIT TRANSACTION
GO
