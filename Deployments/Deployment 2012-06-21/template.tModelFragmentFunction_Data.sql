SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000590, 139, 1, N'FixedDiscount', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000591, 139, 1, N'PercentageDiscount', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000592, 139, 1, N'VoucherDiscount', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000593, 139, 2, N'IsFixedDiscountApplied', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000594, 139, 2, N'IsPercentageDiscountApplied', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000595, 139, 2, N'IsVoucherUsed', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000596, 139, 2, N'IsVoucherWithoutDiscount', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF