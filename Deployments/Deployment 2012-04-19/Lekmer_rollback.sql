SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [lekmer].[pProductGetViewAllByIdList]'
GO
DROP PROCEDURE [lekmer].[pProductGetViewAllByIdList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pProductGetAllByIdList]'
GO
DROP PROCEDURE [lekmer].[pProductGetAllByIdList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [integration].[pCreateProductImageReferences]'
GO
DROP PROCEDURE [integration].[pCreateProductImageReferences]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[fProductHasSizes]'
GO
DROP FUNCTION [product].[fProductHasSizes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO


ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	CAST((CASE WHEN EXISTS (SELECT 1 FROM lekmer.tProductSize s WHERE s.ProductId = p.ProductId) THEN 1 ELSE 0 END) AS BIT) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations'
FROM
	lekmer.tLekmerProduct AS p
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
