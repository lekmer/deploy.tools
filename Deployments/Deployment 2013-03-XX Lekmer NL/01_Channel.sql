BEGIN TRANSACTION

--[core].[tCountry]
SET IDENTITY_INSERT [core].[tCountry] ON
INSERT INTO [core].[tCountry] ([CountryId],[ISO],[IsInEU],[PhonePrefix],[Title])
VALUES (1000005, 'NL', 1, '+31 ', N'Nederland')
SET IDENTITY_INSERT [core].[tCountry] OFF

-- [core].[tLanguage]
SET IDENTITY_INSERT [core].[tLanguage] ON
INSERT INTO [core].[tLanguage] ([LanguageId],[Title],[ISO])
VALUES (1000005, N'Dutch', 'NL')
SET IDENTITY_INSERT [core].[tLanguage] OFF

-- [core].[tCurrency]
SET IDENTITY_INSERT [core].[tCurrency] ON
INSERT INTO [core].[tCurrency] ([CurrencyId],[ISO],[Title],[PriceFormat],[NumberOfDecimals])
VALUES (1000005, 'EUR', N'EUR', '� {0}', 2)
SET IDENTITY_INSERT [core].[tCurrency] OFF

-- [core].[tCulture]
SET IDENTITY_INSERT [core].[tCulture] ON
INSERT INTO [core].[tCulture] ([CultureId],[CommonName],[Title])
VALUES (1000005, 'nl-NL', N'Dutch (Netherlands)')
SET IDENTITY_INSERT [core].[tCulture] OFF

-- [core].[tChannel]
SET IDENTITY_INSERT [core].[tChannel] ON
INSERT INTO [core].[tChannel] ([ChannelId],[Title],[CountryId],[ApplicationName],[AlternateLayoutRatio],[LanguageId],[CurrencyId],[CultureId],[CommonName])
VALUES (1000005, N'Lekmer.nl', 1000005, 'lekmer.nl', 0.00, 1000005, 1000005, 1000005, 'Netherlands')
SET IDENTITY_INSERT [core].[tChannel] OFF

-- [lekmer].[tLekmerChannel]
INSERT INTO [lekmer].[tLekmerChannel] ([ChannelId],[TimeFormat],[WeekDayFormat],[DayFormat],[DateTimeFormat],[TimeZoneDiff])
VALUES (1000005, N'HH:mm', N'ddd', N'dd', N'yyyy-MM-dd', 1)

TODO: Check sub domain configuration before running
-- [corelek].[tSubDomainChannel]
INSERT INTO corelek.tSubDomainChannel (ChannelId,SubDomainId)
SELECT 1000005, sd.SubDomainId FROM corelek.tSubDomain sd WHERE CHARINDEX('lekmer.com', sd.DomainUrl) > 0

ROLLBACK TRANSACTION