ALTER PROCEDURE [backoffice].[pReportLekmerGetUsedVocherInfo]
	@UsedFrom		DATETIME = NULL,
	@UsedTo			DATETIME = NULL,	
	@ChannelGroupId	INT = NULL,
	@CreatedBy		NVARCHAR(50) = NULL,
	@BatchId		INT = NULL
AS
BEGIN
	SET NOCOUNT ON

	(SELECT
		vi.VoucherInfoId AS BatchId,
		vi.Quantity,
		DiscountValue,
		vi.DiscountTitle,
		vi.[Description],
		c.[Description] AS Channel,
		vi.OriginalStartQuantityPerCode,
		ISNULL(vi.CreatedBy, '') AS CreatedBy,
		d.DiscountType,
		(vi.Quantity * vi.OriginalStartQuantityPerCode) AS TotalAmountOfUsableCodes,
		(CASE WHEN vi.DiscountTypeId = 1
			  THEN vi.DiscountValue
			  ELSE ((vi.Quantity * vi.OriginalStartQuantityPerCode) * vi.DiscountValue)
		 END) AS 'TotalDiscountForBatch (local currency or %)',
		z.TotalUsage AS NoOfVouchersUsed,
		(CASE WHEN vi.DiscountTypeId = 1
			  THEN vi.DiscountValue
			  ELSE (z.TotalUsage * vi.DiscountValue)
		 END) AS 'DiscountUsed (local currency or %)'
	FROM
		product.tVoucherInfo vi
		INNER JOIN
		(
			SELECT
				v.VoucherInfoId,
				SUM(l2.[Count]) AS TotalUsage
			FROM
				product.tVoucher v
				INNER JOIN
				(
					SELECT l.VoucherId, COUNT(*) as 'Count'
					FROM product.tVoucherLog l
					WHERE
						(@UsedFrom IS NULL OR l.UsageDate > @UsedFrom)
						AND (@UsedTo IS NULL OR l.UsageDate < @UsedTo)
					GROUP BY l.VoucherId
				) l2 ON v.VoucherId = l2.VoucherId
			GROUP BY v.VoucherInfoId
		) z ON vi.VoucherInfoId = z.VoucherInfoId
		INNER JOIN product.tChannelGroup c ON c.ChannelGroupId = vi.ChannelGroupId
		INNER JOIN product.tDiscountType d ON vi.DiscountTypeId = d.DiscountTypeId
	WHERE
		(@BatchId IS NULL OR vi.VoucherInfoId = @BatchId)
		AND (@CreatedBy IS NULL OR vi.CreatedBy = @CreatedBy)
		AND vi.ChannelGroupId IN (1,2,3,4,13,16)
	)

	UNION ALL

	(SELECT
			vi.VoucherInfoId AS BatchId,
			vi.Quantity,
			DiscountValue,
			vi.DiscountTitle,
			vi.[Description],
			c.[Description] AS Channel,
			vi.OriginalStartQuantityPerCode,
			ISNULL(vi.CreatedBy, '') AS CreatedBy,
			d.DiscountType,
			(vi.Quantity * vi.OriginalStartQuantityPerCode) as TotalAmountOfUsableCodes,
			(CASE WHEN vi.DiscountTypeId = 1
				  THEN vi.DiscountValue
				  ELSE ((vi.Quantity * vi.OriginalStartQuantityPerCode) * vi.DiscountValue)
			 END) AS 'TotalDiscountForBatch (local currency or %)',
			z.TotalUsage AS NoOfVouchersUsed,
			(CASE WHEN vi.DiscountTypeId = 1
				  THEN vi.DiscountValue
				  ELSE (z.TotalUsage * vi.DiscountValue)
			 END) AS 'DiscountUsed (local currency or %)'
		FROM
			archive.tVoucherInfo vi
			INNER JOIN
			(
				SELECT
					v.VoucherInfoId,
					SUM(l2.[Count]) AS TotalUsage
				FROM
					archive.tVoucher v
					INNER JOIN
					(
						SELECT l.VoucherId, COUNT(*) AS 'Count'
						FROM product.tVoucherLog l
						WHERE
							(@UsedFrom IS NULL OR l.UsageDate > @UsedFrom)
							AND (@UsedTo IS NULL OR l.UsageDate < @UsedTo)
						GROUP BY l.VoucherId
					) l2 ON v.VoucherId = l2.VoucherId
				GROUP BY v.VoucherInfoId
			) z ON vi.VoucherInfoId = z.VoucherInfoId
			INNER JOIN product.tChannelGroup c ON c.ChannelGroupId = vi.ChannelGroupId
			INNER JOIN product.tDiscountType d ON vi.DiscountTypeId = d.DiscountTypeId
		WHERE
			(@BatchId IS NULL OR vi.VoucherInfoId = @BatchId)
			AND (@CreatedBy IS NULL OR vi.CreatedBy = @CreatedBy)
			AND vi.ChannelGroupId IN (1,2,3,4,13,16)
	 )
	 ORDER BY BatchId
END