-- [order].[tChannelPaymentType]
INSERT INTO [order].[tChannelPaymentType] ([ChannelId],[PaymentTypeId])
VALUES (1000005, 1),(1000005, 1000002),(1000005, 1000003)


-- [lekmer].[tPaymentTypePrice]
INSERT INTO [lekmer].[tPaymentTypePrice] ([CountryId],[CurrencyId],[PaymentTypeId],[PaymentCost])
VALUES (1000005, 1000005, 1, 0.00), (1000005, 1000005, 1000002, 0.00), (1000005, 1000005, 1000003, 0.00)


-- [order].[tChannelDeliveryMethod]
INSERT INTO [order].[tChannelDeliveryMethod] ([ChannelId],[DeliveryMethodId])
VALUES (1000005, 1)


-- [order].[tDeliveryMethodPrice]
INSERT INTO [order].[tDeliveryMethodPrice] ([CountryId],[CurrencyId],[DeliveryMethodId],[FreightCost])
VALUES (1000005, 1000005, 1, 4.90)