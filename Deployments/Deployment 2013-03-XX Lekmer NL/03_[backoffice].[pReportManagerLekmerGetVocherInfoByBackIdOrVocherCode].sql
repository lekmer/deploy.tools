ALTER PROCEDURE [backoffice].[pReportManagerLekmerGetVocherInfoByBackIdOrVocherCode]
	@ValidFrom		DATETIME = NULL,
	@ValidTo		DATETIME = NULL,
	@ChannelGroupId	INT = NULL,
	@CreatedBy		NVARCHAR(50) = NULL,
	@BatchId		INT = NULL,
	@VoucherCode	NVARCHAR(50) = NULL,
	@OrderId		INT = NULL
AS
BEGIN
	SET NOCOUNT ON

		-- ställ om till dagar 2359

		--set @ChannelGroupId = coalesce(@ChannelGroupId, 12)
		--set @ValidFrom = coalesce(@ValidFrom, '2009-08-12 00:00:00.000')
		--set @ValidTo = coalesce(@ValidTo, '2099-08-12 00:00:00.000')
		--set @BatchId = coalesce(@BatchId, )

		(SELECT
			vi.VoucherInfoId,
			vi.ValidFrom,
			COALESCE(vi.ValidTo, v.ValidTo) AS ValidTo,
			vi.Quantity,
			d.DiscountType,
			DiscountValue,
			vi.DiscountTitle AS CustomerNo,
			vi.[Description],
			vi.IsActive,
			c.[Description] AS Channel,
			OriginalStartQuantityPerCode,
			vi.CreatedBy,
			v.VoucherCode,
			v.QuantityLeft,
			l.OrderId
		FROM
			product.tVoucherInfo vi
			INNER JOIN product.tVoucher v ON vi.VoucherInfoId = v.VoucherInfoId
			INNER JOIN product.tChannelGroup c ON c.ChannelGroupId = vi.ChannelGroupId
			INNER JOIN product.tDiscountType d ON vi.DiscountTypeId = d.DiscountTypeId
			LEFT JOIN product.tVoucherLog l ON l.VoucherId = v.VoucherId
		WHERE
			--vi.ChannelGroupId = ( select SiteId from product.tChannelGroup where ChannelGroupId = 12)
			--and (vi.ValidFrom > @ValidFrom)
			--and (v.ValidTo < @ValidTo)
			(@BatchId IS NULL OR vi.VoucherInfoId = @BatchId) 
			AND (@VoucherCode IS NULL OR v.VoucherCode = @VoucherCode)
			AND (@ValidFrom IS NULL OR vi.ValidFrom > @ValidFrom)
			AND (@ValidTo IS NULL OR ((v.ValidTo < @ValidTo) or (vi.ValidTo < @ValidTo)))
			AND (@CreatedBy IS NULL OR vi.CreatedBy = @CreatedBy)
			AND (@OrderId IS NULL OR l.OrderId = @OrderId)
			AND vi.ChannelGroupId IN (1,2,3,4,13,16)
		)
		
	UNION ALL

		(SELECT
			vi.VoucherInfoId,
			vi.ValidFrom,
			coalesce(vi.ValidTo, v.ValidTo) AS ValidTo,
			vi.Quantity,
			d.DiscountType,
			DiscountValue,
			vi.DiscountTitle AS CustomerNo,
			vi.[Description],
			vi.IsActive,
			c.[Description] AS Channel,
			OriginalStartQuantityPerCode,
			vi.CreatedBy,
			v.VoucherCode,
			v.QuantityLeft,
			l.OrderId
		FROM
			archive.tVoucherInfo vi
			INNER JOIN archive.tVoucher v ON vi.VoucherInfoId = v.VoucherInfoId
			INNER JOIN product.tChannelGroup c ON c.ChannelGroupId = vi.ChannelGroupId
			INNER JOIN product.tDiscountType d ON vi.DiscountTypeId = d.DiscountTypeId
			LEFT JOIN product.tVoucherLog l ON l.VoucherId = v.VoucherId
		WHERE
			--vi.ChannelGroupId = ( select SiteId from product.tChannelGroup where ChannelGroupId = 12)
			--and (vi.ValidFrom > @ValidFrom)
			--and (v.ValidTo < @ValidTo)

			(@BatchId IS NULL OR vi.VoucherInfoId = @BatchId)
			AND (@VoucherCode IS NULL OR v.VoucherCode = @VoucherCode)
			AND (@ValidFrom IS NULL OR vi.ValidFrom > @ValidFrom)
			AND (@ValidTo IS NULL OR ((v.ValidTo < @ValidTo) or (vi.ValidTo < @ValidTo)))
			AND (@CreatedBy IS NULL OR vi.CreatedBy = @CreatedBy)
			AND (@OrderId IS NULL OR l.OrderId = @OrderId)
			AND vi.ChannelGroupId IN (1,2,3,4,13,16)
		)
END