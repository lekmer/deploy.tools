ALTER PROCEDURE [integration].[usp_UpdateProductPRICE]
AS
BEGIN
	declare @Vat decimal
	SET @Vat = 25
	
	DECLARE 
		@HYChannelNL NVARCHAR(40),
		@ChannelIdNL INT
	
	TODO:
	SET @HYChannelNL = '006'
	SET @ChannelIdNL = 1000005
	
	begin try
	begin transaction
-- kalla sp som skapar prislista
		-- tProduct
		
		UPDATE
		--product.tPriceListItem 
			pli
		SET 
			PriceIncludingVat = (t.price/100),
			PriceExcludingVat = ((t.price/100) * ((100 - @Vat)/100)),
			VatPercentage = @Vat
		FROM 
			[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYarticleId, 5,17)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId
					   AND (pli.PriceListId = left(t.HYarticleId, 3) 
							OR (pli.PriceListId = @ChannelIdNL AND t.HYarticleId = @HYChannelNL)) --Dangerous!
			/*[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = t.HYarticleId		
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId*/	

		WHERE 		
			pli.PriceIncludingVat <> (t.price/100)
		
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
        /*delete pli -- funkar på update också
		from product.tPriceListItem pli
        inner join lekmer.tLekmerProduct ll on pli.ProductId = ll.ProductId*/
END

