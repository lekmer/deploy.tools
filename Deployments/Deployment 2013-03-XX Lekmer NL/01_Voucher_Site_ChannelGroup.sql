-- [product].[tSite]
SET IDENTITY_INSERT [product].[tSite] ON
INSERT INTO [product].[tSite] ([SiteId],[SiteTitle],[Description])
VALUES (14, N'lekmer.nl', NULL)
SET IDENTITY_INSERT [product].[tSite] OFF


-- [product].[tChannelGroup]
INSERT INTO [product].[tChannelGroup] ([ChannelGroupId],[SiteId],[Description])
VALUES (16, 14, N'lekmer.nl')
INSERT INTO [product].[tChannelGroup] ([ChannelGroupId],[SiteId],[Description])
VALUES (13, 14, N'lekmer all channels')