CREATE PROCEDURE [integration].[pGenerateSeoContentPageSettingsNL]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION
TODO: correct CONTENT NODE ids
		------------------------------------------------------------
		-- <Leksaker Kategori Actionfigurer - nivå 2 och nivå 3>
		------------------------------------------------------------
		-- Insert new contentnodeIds in tContentPageSeoSetting
		INSERT INTO sitestructure.tContentPageSeoSetting (ContentNodeId)
		SELECT
			n.ContentNodeId
		FROM
			sitestructure.tContentNode n --3
		WHERE
			n.SiteStructureRegistryId = 1000005
			AND n.ContentNodeTypeId = 3 -- detta är contentpages
			AND n.ContentNodeId NOT IN (SELECT ContentNodeId FROM sitestructure.tContentPageSeoSetting)

		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Legetøj online fra Lekmer.nl – legetøj på nettet.',
			cps.[Description] = 'Køb ' + cn.Title + ' på nettet. Vi tilbyder et stort udvalg af legetøj fra '
								+ 'kendte varemærker leverer indenfor 3-5 dage - Lekmer.nl din legetøjsbutik.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND cn.ParentContentNodeId = 1005996 /*--TODO:???*/
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		------------------------------------------------------------
		-- <Leksaker Underkategori Starwars - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = x.Title + ' ' + cn.Title + ' Legetøj online fra Lekmer.nl – legetøj på nettet.',
			cps.[Description] = 'Køb ' + x.Title + ' ' + cn.Title + ' på nettet. Vi tilbyder et stort udvalg af legetøj fra '
			+ 'kende varemærker leverer indenfor 3-5 dage - Lekmer.nl din legetøjsbutik.'
		FROM
				sitestructure.tContentPageSeoSetting cps
				INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
				INNER JOIN (SELECT ContentNodeId, Title
							FROM sitestructure.tContentNode
							WHERE ParentContentNodeId = 1005996/*--TODO:???*/) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		------------------------------------------------------------
		-- <Start Barn & Baby Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = 'Barn og Baby ' + cn.Title + ' produkter online fra Lekmer.nl – Køb børneartikler på nettet.',
			cps.[Description] = 'Køb Barn og Baby produkter på nettet. Få dine barn- og babyprodukter leveret indenfor 3-5 dage fra Lekmer.nl.'
		FROM
				sitestructure.tContentPageSeoSetting cps
				INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND cn.ParentContentNodeId = 1005875/*--TODO:???*/
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		------------------------------------------------------------
		-- <Start Barn & Baby Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Barn og Baby produkter online fra Lekmer.nl – Køb børneartikler på nettet.',
			cps.[Description] = 'Køb ' + cn.Title  + ' Barn og Baby produkter på nettet. Få dine barn- og babyprodukter leveret '
			+ 'indenfor 3-5 dage fra Lekmer.nl.'
		FROM
				sitestructure.tContentPageSeoSetting cps
				INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
				INNER JOIN (SELECT ContentNodeId, Title
							FROM sitestructure.tContentNode
							WHERE ParentContentNodeId = 1005875/*--TODO:???*/) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		------------------------------------------------------------
		-- <Start Barnkläder Kategori Överkategori - nivå 2 >
		------------------------------------------------------------										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Børnetøj online fra Lekmer.nl – Børn og Babytøj på nettet.',
			cps.[Description] = 'Køb ' + cn.Title + ' på nettet. Stort udvalg af cool tøj til børn og babyer – '
			+ 'handle online dit børnetøj og få dem leveret indenfor 3-5 dage fra Lekmer.nl.'
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn on cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			and cn.ParentContentNodeId = 1007349/*--TODO:???*/
			AND ((cps.Title is null or cps.Title = '') or (cps.[Description] is null or cps.[Description] = ''))

		------------------------------------------------------------
		-- <Start Barnkläder Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Børnetøj online fra Lekmer.nl – Barn og babytøj på nettet.',
			cps.[Description] = 'Køb ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dit børnetøj leveret indenfor 3-5 dage fra Lekmer.nl.'
		FROM
				sitestructure.tContentPageSeoSetting cps
				INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
				INNER JOIN (SELECT ContentNodeId, Title
							FROM sitestructure.tContentNode
							WHERE ParentContentNodeId = 1007349/*--TODO:???*/) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		------------------------------------------------------------
		-- <Start Inredning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Indretning online fra Lekmer.nl Børneværelses indretning på nettet.',
			cps.[Description] = 'Køb ' + cn.Title + ' på nettet. Få dit børneværelse leveret indenfor 3-5 dage fra Lekmer.nl.'
		FROM
				sitestructure.tContentPageSeoSetting cps
				INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND cn.ParentContentNodeId = 1005944/*--TODO:???*/
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		------------------------------------------------------------
		-- <Start Inredning Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Indretning online från Lekmer.nl – Børneværelse på nettet.',
			cps.[Description] = 'Køb ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dit børneværelse leveret indenfor 3-5 dage fra Lekmer.nl.'
		FROM
				sitestructure.tContentPageSeoSetting cps
				INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
				INNER JOIN (SELECT ContentNodeId, Title
							FROM sitestructure.tContentNode
							WHERE ParentContentNodeId = 1005944/*--TODO:???*/) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		------------------------------------------------------------
		-- <Start Underhållning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' og Underholdning og børnespil online fra Lekmer.nl – Tv-spil på nettet.',
			cps.[Description] = 'Køb ' + cn.Title + ' på nettet. Få dine tv-spil leveret indenfor 3-5 dage fra Lekmer.nl.'
		FROM
				sitestructure.tContentPageSeoSetting cps
				INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND cn.ParentContentNodeId = 1006393/*--TODO:???*/
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		------------------------------------------------------------
		-- <Start Underhållning Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' ',
			cps.[Description] = 'Køb ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dine tv-spil leveret indenfor 3-5 dage fra Lekmer.nl.'
		FROM
				sitestructure.tContentPageSeoSetting cps
				INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
				INNER JOIN (SELECT ContentNodeId, Title
							FROM sitestructure.tContentNode
							WHERE ParentContentNodeId = 1006393/*--TODO:???*/) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1000005
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- IF TRANSACTION IS ACTIVE, ROLL IT BACK.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] (Data, [Message], [Date], OcuredInProcedure)
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END