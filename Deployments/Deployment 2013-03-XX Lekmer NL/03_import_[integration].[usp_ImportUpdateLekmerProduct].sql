ALTER PROCEDURE [integration].[usp_ImportUpdateLekmerProduct]
AS
BEGIN
	DECLARE 
		@HYChannelNL NVARCHAR(40),
		@ChannelIdNL INT
	
	TODO:
	SET @HYChannelNL = '006'
	SET @ChannelIdNL = 1000005

	-- Update tLekmerProduct
	UPDATE 
		[lp]
	SET 
		[lp].[ExpectedBackInStock] =  NULL
	FROM 
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tLekmerProduct] lp				ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
		INNER JOIN [product].[tProduct] p					ON [p].[ProductId] = [lp].[ProductId]
		INNER JOIN [product].[tProductRegistryProduct] pli	ON [pli].[ProductId] = [lp].[ProductId]
																AND ([pli].[ProductRegistryId] = SUBSTRING([tp].[HYarticleId], 3, 1)
																	 OR ([pli].[ProductRegistryId] = @ChannelIdNL AND LEFT([tp].[HYarticleId], 3) = @HYChannelNL))
	WHERE 
		[lp].[ExpectedBackInStock] <> LEN([lp].[ExpectedBackInStock])
		AND [tp].[NoInStock] > 0
	
	-- Update tLekmerProduct
	UPDATE 
		[lp]
	SET 
		[lp].[LekmerErpId] = [tp].[LekmerArtNo],
		[lp].[Weight] = CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
	FROM 
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
	WHERE
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND
			(
				[lp].[LekmerErpId] IS NULL
				OR [lp].[LekmerErpId] <> [tp].[LekmerArtNo]
				OR [lp].[Weight] IS NULL
				OR [lp].[Weight] <> CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
			)
			
END
