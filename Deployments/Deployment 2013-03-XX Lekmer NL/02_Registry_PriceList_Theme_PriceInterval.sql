BEGIN TRANSACTION

-- Registries
-- [sitestructure].[tSiteStructureRegistry]
SET IDENTITY_INSERT [sitestructure].[tSiteStructureRegistry] ON
INSERT INTO [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId],[Title],[CommonName],[StartContentNodeId])
VALUES (1000005, N'Lekmer.nl', 'Nederland', NULL)
SET IDENTITY_INSERT [sitestructure].[tSiteStructureRegistry] OFF

INSERT INTO [sitestructure].[tSiteStructureModuleChannel] ([ChannelId],[SiteStructureRegistryId]) VALUES (1000005, 1000005)


-- [campaign].[tCampaignRegistry]
SET IDENTITY_INSERT [campaign].[tCampaignRegistry] ON
INSERT INTO [campaign].[tCampaignRegistry] ([CampaignRegistryId],[Title],[CommonName])
VALUES (1000005, N'Lekmer.nl', 'Nederland')
SET IDENTITY_INSERT [campaign].[tCampaignRegistry] OFF

INSERT INTO [campaign].[tCampaignModuleChannel] ([ChannelId],[CampaignRegistryId]) VALUES (1000005, 1000005)


-- [customer].[tCustomerRegistry]
SET IDENTITY_INSERT [customer].[tCustomerRegistry] ON
INSERT INTO [customer].[tCustomerRegistry] ([CustomerRegistryId],[Title])
VALUES (1000005, N'Lekmer.nl')
SET IDENTITY_INSERT [customer].[tCustomerRegistry] OFF

INSERT INTO [customer].[tCustomerModuleChannel] ([ChannelId],[CustomerRegistryId]) VALUES (1000005, 1000005)


-- [product].[tPriceListRegistry]
SET IDENTITY_INSERT [product].[tPriceListRegistry] ON
INSERT INTO [product].[tPriceListRegistry] ([PriceListRegistryId],[Title])
VALUES (1000005, N'Lekmer.nl')
SET IDENTITY_INSERT [product].[tPriceListRegistry] OFF


-- [product].[tPriceList]
SET IDENTITY_INSERT [product].[tPriceList] ON
INSERT INTO [product].[tPriceList] ([PriceListId],[CurrencyId],[ErpId],[Title],[CommonName],[StartDateTime],[EndDateTime],[PriceListFolderId],[PriceListStatusId],[PriceListRegistryId])
VALUES (1000005, 1000005, '6', N'Lekmer.nl', 'Nederland', NULL, NULL, 1, 0, 1000005)
SET IDENTITY_INSERT [product].[tPriceList] OFF


-- [product].[tProductRegistry]
SET IDENTITY_INSERT [product].[tProductRegistry] ON
INSERT INTO [product].[tProductRegistry] ([ProductRegistryId],[Title])
VALUES (1000005, N'Lekmer.nl')
SET IDENTITY_INSERT [product].[tProductRegistry] OFF

INSERT INTO [product].[tProductModuleChannel] ([ChannelId],[ProductTemplateContentNodeId],[PriceListRegistryId],[ProductRegistryId])
VALUES (1000005, NULL, 1000005, 1000005)


-- [review].[tRatingRegistry]
SET IDENTITY_INSERT [review].[tRatingRegistry] ON
INSERT INTO [review].[tRatingRegistry] ([RatingRegistryId],[CommonName],[Title])
VALUES (1000005, 'Netherlands', N'lekmer.nl')
SET IDENTITY_INSERT [review].[tRatingRegistry] OFF

INSERT INTO [review].[tRatingModuleChannel] ([ChannelId],[RatingRegistryId]) VALUES (1000005, 1000005), (1000005, 6)


-- [order].[tOrderModuleChannel]
INSERT INTO [order].[tOrderModuleChannel] ([ChannelId],[OrderTemplateContentNodeId])
VALUES (1000005, NULL)


-- [template].[tTheme]
SET IDENTITY_INSERT [template].[tTheme] ON
INSERT INTO [template].[tTheme] ([ThemeId],[Title]) 
VALUES(1000005, N'Lekmer - NL')
SET IDENTITY_INSERT [template].[tTheme] OFF


-- [template].[tTemplateModuleChannel]
INSERT INTO [template].[tTemplateModuleChannel] ([ChannelId],[ThemeId]) VALUES (1000005,1000005)


---- [lekmer].[tBrandSiteStructureRegistry]
--INSERT INTO [lekmer].[tBrandSiteStructureRegistry] ([BrandId],[SiteStructureRegistryId],[ContentNodeId])
--SELECT [BrandId], 1000005, NULL FROM [lekmer].[tBrandSiteStructureRegistry] WHERE [SiteStructureRegistryId] = 1


-- [product].[tCategorySiteStructureRegistry]
INSERT INTO [product].[tCategorySiteStructureRegistry] ([CategoryId],[SiteStructureRegistryId],[ProductParentContentNodeId],[ProductTemplateContentNodeId])
SELECT [CategoryId], 1000005, NULL, NULL FROM [product].[tCategorySiteStructureRegistry] WHERE [SiteStructureRegistryId] = 1


-- [lekmer].[tPriceInterval]
INSERT INTO [lekmer].[tPriceInterval] ([From],[To],[Title],[CurrencyId])
SELECT [From], [To], [Title], 1000005 FROM [lekmer].[tPriceInterval] WHERE [CurrencyId] = 1000003


TODO: NEED uncoment and set correct content node ids
---- [lekmer].[tPriceIntervalSiteStructureRegistry] - LOCAL
--INSERT INTO [lekmer].[tPriceIntervalSiteStructureRegistry] ([PriceIntervalId],[SiteStructureRegistryId],[ContentNodeId])
--VALUES (1000001, 1000005, 1014325),(1000002, 1000005, 1014658),(1000003, 1000005, 1014771),(1000004, 1000005, 1015056),
--	   (1000005, 1000005, 1015133),(1000006, 1000005, 1015321),(1000007, 1000005, 1015374),(1000008, 1000005, 1015478)

---- [lekmer].[tPriceIntervalSiteStructureRegistry] - DEV
--INSERT INTO [lekmer].[tPriceIntervalSiteStructureRegistry] ([PriceIntervalId],[SiteStructureRegistryId],[ContentNodeId])
--VALUES (1000001, 1000005, 1011950),(1000002, 1000005, 1012283),(1000003, 1000005, 1012397),(1000004, 1000005, 1012682),
--	   (1000005, 1000005, 1012758),(1000006, 1000005, 1012946),(1000007, 1000005, 1013000),(1000008, 1000005, 1013102)



-- [product].[tProductSiteStructureRegistry]
INSERT INTO [product].[tProductSiteStructureRegistry] ([ProductId],[SiteStructureRegistryId],[ParentContentNodeId],[TemplateContentNodeId])
SELECT [ProductId], 1000005, NULL, NULL FROM [product].[tProductSiteStructureRegistry] WHERE [SiteStructureRegistryId] = 1


-- [product].[tProductRegistryProduct]
INSERT INTO [product].[tProductRegistryProduct] ([ProductRegistryId],[ProductId])
SELECT 1000005, [ProductId] FROM [product].[tProductRegistryProduct] WHERE [ProductRegistryId] = 1


-- [product].[tPriceListItem] 
INSERT INTO [product].[tPriceListItem] ([PriceListId],[ProductId],[PriceIncludingVat],[PriceExcludingVat],[VatPercentage])
SELECT 1000005, [ProductId], [PriceIncludingVat], [PriceExcludingVat], [VatPercentage] FROM [product].[tPriceListItem] WHERE [PriceListId] = 4

ROLLBACK TRANSACTION