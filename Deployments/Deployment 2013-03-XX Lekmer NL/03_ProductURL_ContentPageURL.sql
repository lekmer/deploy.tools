-- [lekmer].[tProductUrl]
INSERT INTO [lekmer].[tProductUrl]
SELECT [ProductId], 1000005, [UrlTitle] FROM [lekmer].[tProductUrl] WHERE [LanguageId] = 1


-- [lekmer].[tProductUrlHistory]
DELETE [lekmer].[tProductUrlHistory] WHERE [LanguageId] = 1000005


-- [lekmer].[tContentPageUrlHistory]
DELETE [lekmer].[tContentPageUrlHistory]
FROM [lekmer].[tContentPageUrlHistory] h
	 INNER JOIN [sitestructure].[tContentPage] cp ON [h].[ContentPageId] = [cp].[ContentNodeId]
WHERE cp.[SiteStructureRegistryId] = 1000005