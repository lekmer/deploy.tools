
ALTER PROCEDURE [integration].[usp_UpdateProductPRICELekmer]

AS
BEGIN
	
	if object_id('tempdb..#tProductPriceIncludingVatLess') is not null
		drop table #tProductPriceIncludingVatLess
		
	if object_id('tempdb..#tProductPriceIncludingVatMore') is not null
		drop table #tProductPriceIncludingVatMore
		
	create table #tProductPriceIncludingVatLess
	(
		ProductId int not null,
		PriceListId int not null,
		PriceIncludingVat decimal(16,2) not null,
		PriceExcludingVat decimal(16,2) not null,
		VatPercentage decimal(16,2) not null
		constraint PK_#tProductPriceIncludingVatLess primary key(ProductId, PricelistId)
	)
	
	create table #tProductPriceIncludingVatMore
	(
		ProductId int not null,
		PriceListId int not null,
		PriceIncludingVat decimal(16,2) not null,
		PriceExcludingVat decimal(16,2) not null,
		VatPercentage decimal(16,2) not null
		constraint PK_#tProductPriceIncludingVatMore primary key(ProductId, PricelistId)
	)
	
	begin try
	begin transaction
		
	declare @Vat decimal
		set @Vat = 25.0	
		
		insert #tProductPriceIncludingVatLess(ProductId, PriceListId, PriceIncludingVat,
		PriceExcludingVat, VatPercentage)		
		select distinct
			pli.productid,
			pli.pricelistid,
			cast(t.price/100.0 as decimal(16,2)), -- PriceIncludingVat
			cast(((t.price/100.0) / (1.0+(25.0/100.0))) as decimal(16,2)), -- PRiceExclusingvat
			@Vat
			--select *
		from 
		[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYarticleId, 5,12)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId and
					pli.PriceListId = case left(t.HYarticleId, 3)
										when '001' then 1
										when '002' then 2
										when '003' then 3
										when '004' then 4
										when '006' then 1000005 
										TODO:
									  end
		WHERE
			pli.PriceIncludingVat <> cast(t.price/100.0 as decimal(16,2))
			and cast(t.price/100.0 as decimal(16,2)) < pli.PriceIncludingVat
			
		
		-- UPDATE 
		Update
			ppli
		set
			PriceExcludingVat = t.PriceExcludingVat
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatLess t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid

			
		-- UPDATE  (REAL)
		Update
			ppli
		set
			ppli.PriceIncludingVat = t.PriceIncludingVat,
			--ppli.PriceExcludingVat = t.PriceExcludingVat, 
			ppli.VatPercentage = t.VatPercentage
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatLess t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid
					
		
		
		--- DEL 2  om nya priser är mer än nuvarande priset
		insert #tProductPriceIncludingVatMore(ProductId, PriceListId, PriceIncludingVat,
		PriceExcludingVat, VatPercentage)		
		select distinct
			pli.productid,
			pli.pricelistid,
			cast(t.price/100.0 as decimal(16,2)), -- PriceIncludingVat
			cast(((t.price/100.0) / (1.0+(25.0/100.0))) as decimal(16,2)), -- PRiceExclusingvat
			@Vat 
		from 
		[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYarticleId, 5,12)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId and
					pli.PriceListId = case left(t.HYarticleId, 3)
										when '001' then 1
										when '002' then 2
										when '003' then 3
										when '004' then 4
										when '006' then 1000005
										TODO:
									  end
		WHERE
			pli.PriceIncludingVat <> cast(t.price/100.0 as decimal(16,2))
			and cast(t.price/100.0 as decimal(16,2)) > pli.PriceIncludingVat	
			
		
		-- UPDATE 
		Update
			ppli
		set
			ppli.PriceIncludingVat = t.PriceIncludingVat
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatMore t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid

			
		-- UPDATE  (REAL)
		Update
			ppli
		set
			ppli.PriceExcludingVat = t.PriceExcludingVat, 
			ppli.VatPercentage = t.VatPercentage
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatMore t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid
		/*
		declare @Vat decimal
		set @Vat = 25.0
		UPDATE
			pli
		SET 
			PriceIncludingVat = (t.price/100.0),
			PriceExcludingVat = ((t.price/100.0) / (1.0+(@Vat/100.0))), --case för moms HYarticleid
														-- lägg till en case sats som kollar i vilken
														-- fok det rör sig om
			VatPercentage = @Vat
		FROM 
			[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYarticleId, 5,17)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId and
					pli.PriceListId = case left(t.HYarticleId, 3)
										when '001' then 1
										when '002' then 2
										when '003' then 3
										when '004' then 4
									  end

		WHERE 		
			pli.PriceIncludingVat <> (t.price/100.0)
		*/
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
        /*delete pli -- funkar på update också
		from product.tPriceListItem pli
        inner join lekmer.tLekmerProduct ll on pli.ProductId = ll.ProductId*/
END

