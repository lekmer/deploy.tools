SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [orderlek].[tDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tDeliveryMethod] DROP CONSTRAINT [FK_tDeliveryMethod(lek)_tDeliveryMethod]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [orderlek].[tDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tDeliveryMethod] DROP CONSTRAINT [PK_tDeliveryMethod]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [orderlek].[tDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tDeliveryMethod] DROP CONSTRAINT [DF_tDeliveryMethod_IsCompany]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [orderlek].[tDeliveryMethod]'
GO
CREATE TABLE [orderlek].[tmp_rg_xx_tDeliveryMethod]
(
[DeliveryMethodId] [int] NOT NULL,
[Priority] [int] NOT NULL,
[IsCompany] [bit] NOT NULL CONSTRAINT [DF_tDeliveryMethod_IsCompany] DEFAULT ((0)),
[DeliveryMethodTypeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [orderlek].[tmp_rg_xx_tDeliveryMethod]([DeliveryMethodId], [Priority], [IsCompany], [DeliveryMethodTypeId]) SELECT [DeliveryMethodId], [Priority], [IsCompany], 1 FROM [orderlek].[tDeliveryMethod]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [orderlek].[tDeliveryMethod]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[orderlek].[tmp_rg_xx_tDeliveryMethod]', N'tDeliveryMethod'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tDeliveryMethod] on [orderlek].[tDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tDeliveryMethod] ADD CONSTRAINT [PK_tDeliveryMethod] PRIMARY KEY CLUSTERED  ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD
[OptionalDeliveryMethodId] [int] NULL,
[OptionalFreightCost] [decimal] (16, 2) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pOrderSave]'
GO
ALTER PROCEDURE [lekmer].[pOrderSave]
	@OrderId					INT,
	@PaymentCost				DECIMAL(16,2),
	@CustomerIdentificationKey	NCHAR(50),
	@AlternateAddressId			INT = NULL,
	@CivicNumber				NVARCHAR(50) = NULL,
	@OptionalDeliveryMethodId	INT,
	@OptionalFreightCost		DECIMAL(16, 2)
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		[PaymentCost] = @PaymentCost,
		[CustomerIdentificationKey] = @CustomerIdentificationKey,
		[AlternateAddressId] = @AlternateAddressId,
		[CivicNumber] = @CivicNumber,
		[OptionalDeliveryMethodId] = @OptionalDeliveryMethodId,
		[OptionalFreightCost] = @OptionalFreightCost
	WHERE 
		[OrderId] = @OrderId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tLekmerOrder]
		(
			[OrderId],
			[PaymentCost],
			[CustomerIdentificationKey],
			[AlternateAddressId],
			[CivicNumber],
			[OptionalDeliveryMethodId],
			[OptionalFreightCost]
		)
		VALUES
		(
			@OrderId,
			@PaymentCost,
			@CustomerIdentificationKey,
			@AlternateAddressId,
			@CivicNumber,
			@OptionalDeliveryMethodId,
			@OptionalFreightCost
		)
	END
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrder]'
GO

ALTER VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		lo.[PaymentCost] AS [Lekmer.PaymentCost],
		lo.[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey],
		lo.[FeedbackToken] AS [Lekmer.FeedbackToken],
		lo.[AlternateAddressId] AS [Lekmer.AlternateAddressId],
		[lo].[NeedSendInsuranceInfo] AS [Lekmer.NeedSendInsuranceInfo],
		[lo].[CivicNumber] AS [Lekmer.CivicNumber],
		[lo].[OptionalDeliveryMethodId] AS [Lekmer.OptionalDeliveryMethodId],
		[lo].[OptionalFreightCost] AS [Lekmer.OptionalFreightCost]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[vDeliveryMethod]'
GO

ALTER VIEW [orderlek].[vDeliveryMethod]
AS
	SELECT
		[DeliveryMethodId] AS 'DeliveryMethod.DeliveryMethodId',
		[Priority] AS 'DeliveryMethod.Priority',
		[IsCompany] AS 'DeliveryMethod.IsCompany',
		[DeliveryMethodTypeId] AS 'DeliveryMethod.DeliveryMethodTypeId'
	FROM
		[orderlek].[tDeliveryMethod]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomDeliveryMethod]'
GO

ALTER VIEW [order].[vCustomDeliveryMethod]
AS
	SELECT
		[dm].*,
		CAST((CASE WHEN [cddm].[ChannelDefaultDeliveryMethod.DeliveryMethodId] IS NULL THEN 0 ELSE 1 END) AS BIT) AS 'DeliveryMethod.ChannelDefault',
		[ldm].[DeliveryMethod.Priority],
		[ldm].[DeliveryMethod.IsCompany],
		[ldm].[DeliveryMethod.DeliveryMethodTypeId]
	FROM
		[order].[vDeliveryMethod] dm
		LEFT JOIN [orderlek].[vDeliveryMethod] ldm ON [ldm].[DeliveryMethod.DeliveryMethodId] = [dm].[DeliveryMethod.DeliveryMethodId]
		LEFT JOIN [orderlek].[vChannelDefaultDeliveryMethod] cddm 
			ON [cddm].[ChannelDefaultDeliveryMethod.ChannelId] = [dm].[DeliveryMethod.ChannelId]
			AND [cddm].[ChannelDefaultDeliveryMethod.DeliveryMethodId] = [dm].[DeliveryMethod.DeliveryMethodId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pOrder]'
GO

ALTER PROCEDURE [integration].[pOrder]
	@OrderId int
AS
begin
	set nocount on
	set transaction isolation level read uncommitted
	begin try
		
		--set statistics io on
		--set statistics time on
		
		-- Order
		select 		
			o.OrderId,
            o.FreightCost,
            u.OptionalFreightCost,
            o.ChannelId as Channel,   
            z.OrderPaymentId, 
            v.ErpId as PaymentMethod, 
            z.ReferenceId, 
            z.Price,
            --u.VoucherDiscount as VoucherAmount
            op.ItemsActualPriceIncludingVat - z.Price as VoucherAmount,
			lci.IsCompany -- NEW
		from
			[order].tPaymentType v
			inner join [order].tOrderPayment z
				on z.PaymentTypeId = v.PaymentTypeId
			inner join [order].torder o
				on z.OrderId = o.OrderId
			inner join customerlek.tCustomerInformation lci
				on lci.CustomerId = o.CustomerId
			left join lekmer.tlekmerOrder u
				on o.OrderId = u.OrderId
			cross apply (select sum(oi.ActualPriceIncludingVat * oi.Quantity) as 'ItemsActualPriceIncludingVat' from [order].tOrderItem oi where oi.OrderId = o.OrderId) as op	
			where
				o.OrderStatusId = 2 and
				o.OrderId = @OrderId
		
		-- OrderRow // Old
		--select
		--	o.OrderItemId as OrderRowId, 
		--	p.Title, 
		--	o.Quantity, 
		--	p.ProductId,
		--	o.ActualPriceIncludingVat as Price, 
		--	o.OriginalPriceIncludingVat as OrdinaryPrice, 
		--	coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		--from
		--	[order].tOrderItem o
		--	left join [order].tOrderItemProduct p 
		--		on o.OrderItemId = p.OrderItemId
		--	left join lekmer.tOrderItemSize ois
		--		on ois.OrderItemId = o.OrderItemId
		--where
		--		o.OrderId = @OrderId 
				
		-- OrderRow
		select
			o.OrderItemId as OrderRowId, 
			p.Title, 
			o.Quantity, 
			p.ProductId,
			o.ActualPriceIncludingVat as Price, 
			o.OriginalPriceIncludingVat as OrdinaryPrice, 
			coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
			inner join lekmer.tLekmerProduct l
				on p.ProductId = l.ProductId
		where
			o.OrderId = @OrderId 
			and l.ProductTypeId != 2 -- package article
		UNION ALL
		select
			poi.OrderItemId as OrderRowId,
			poip.Title,
			poi.Quantity,
			poip.ProductId,
			poi.PackagePriceIncludingVat as Price,
			poi.OriginalPriceIncludingVat as OrdinaryPrice,
			coalesce(poip.[SizeErpId], poip.ErpId) as HyErpId
		from
			orderlek.tPackageOrderItem poi
			left join orderlek.tPackageOrderItemProduct poip
				on poi.PackageOrderItemId = poip.PackageOrderItemId
		where
			poi.OrderId = @OrderId
			
		-- Customer
		select 		
            c.CustomerId, 
			c.FirstName, 
			c.LastName,
            c.CivicNumber, 
            c.PhoneNumber,
            c.CellPhoneNumber as MobilePhoneNumber, 
            o.Email,
			case g.IsCompany when 1 then '05' else isnull(gt.ErpId, '00') end as Gender --NEW
		from	
			[order].torder o
			inner join [customer].tCustomerInformation c
				on c.CustomerId = o.CustomerId
			left join customerlek.tCustomerInformation g
				on c.CustomerId = g.CustomerId
			left join customerlek.tGenderType gt
				on g.GenderTypeId = gt.GenderTypeId
		where
			o.OrderId = @OrderId
		
		-- BillingAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.BillingAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- DeliveryAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.DeliveryAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- AlternativeAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension
		from	
			[order].tOrder o
			inner join [lekmer].tLekmerOrder lo on lo.OrderId = o.OrderId
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = lo.AlternateAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId


		-- DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join [order].tDeliveryMethod d
				on o.DeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId
			
		-- Optional DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join lekmer.tLekmerOrder lo
				on lo.OrderId = o.OrderId
			inner join [order].tDeliveryMethod d
				on lo.OptionalDeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId

	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pDeliveryMethodGetAllByPaymentType]'
GO
ALTER PROCEDURE [order].[pDeliveryMethodGetAllByPaymentType]
	@ChannelId INT,
	@PaymentTypeId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dm.*
	FROM 
		[order].[vCustomDeliveryMethod] dm
		INNER JOIN [order].[tPaymentTypeDeliveryMethod] ptdm ON ptdm.DeliveryMethodId = dm.[DeliveryMethod.DeliveryMethodId]
	WHERE
		ptdm.PaymentTypeId = @PaymentTypeId
		AND dm.[DeliveryMethod.ChannelId] = @ChannelId
		AND dm.[DeliveryMethod.DeliveryMethodTypeId] = 1 -- Base
	ORDER BY
		dm.[DeliveryMethod.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pDeliveryMethodGetOptionalByDeliveryMethod]'
GO
CREATE PROCEDURE [orderlek].[pDeliveryMethodGetOptionalByDeliveryMethod]
	@ChannelId INT,
	@DeliveryMethodId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1
		dm.*
	FROM 
		[order].[vCustomDeliveryMethod] dm
		INNER JOIN [orderlek].[tDeliveryMethodDependency] dmd ON dmd.[OptionalDeliveryMethodId] = dm.[DeliveryMethod.DeliveryMethodId]
	WHERE
		dmd.[BaseDeliveryMethodId] = @DeliveryMethodId
		AND dm.[DeliveryMethod.ChannelId] = @ChannelId
		AND dm.[DeliveryMethod.DeliveryMethodTypeId] = 2 -- Optional
	ORDER BY
		dm.[DeliveryMethod.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tDeliveryMethod]'
GO
ALTER TABLE [orderlek].[tDeliveryMethod] ADD CONSTRAINT [FK_tDeliveryMethod(lek)_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
