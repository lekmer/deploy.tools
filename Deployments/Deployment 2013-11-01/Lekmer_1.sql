SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [orderlek].[tDeliveryMethodType]'
GO
CREATE TABLE [orderlek].[tDeliveryMethodType]
(
[DeliveryMethodTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tDeliveryMethodType] on [orderlek].[tDeliveryMethodType]'
GO
ALTER TABLE [orderlek].[tDeliveryMethodType] ADD CONSTRAINT [PK_tDeliveryMethodType] PRIMARY KEY CLUSTERED  ([DeliveryMethodTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tDeliveryMethodDependency]'
GO
CREATE TABLE [orderlek].[tDeliveryMethodDependency]
(
[DeliveryMethodDependencyId] [int] NOT NULL IDENTITY(1, 1),
[BaseDeliveryMethodId] [int] NOT NULL,
[OptionalDeliveryMethodId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tDeliveryMethodDependency] on [orderlek].[tDeliveryMethodDependency]'
GO
ALTER TABLE [orderlek].[tDeliveryMethodDependency] ADD CONSTRAINT [PK_tDeliveryMethodDependency] PRIMARY KEY CLUSTERED  ([DeliveryMethodDependencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vDeliveryMethodDependency]'
GO


CREATE VIEW [orderlek].[vDeliveryMethodDependency]
AS
SELECT
	[DeliveryMethodDependencyId] AS 'DeliveryMethodDependency.Id',
	[BaseDeliveryMethodId] AS 'DeliveryMethodDependency.BaseDeliveryMethodId',
	[OptionalDeliveryMethodId] AS 'DeliveryMethodDependency.OptionalDeliveryMethodId'
FROM
	[orderlek].[tDeliveryMethodDependency]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vDeliveryMethodType]'
GO

CREATE VIEW [orderlek].[vDeliveryMethodType]
AS
SELECT
	[DeliveryMethodTypeId] AS 'DeliveryMethodType.Id',
	[Title] AS 'DeliveryMethodType.Title',
	[CommonName] AS 'DeliveryMethodType.CommonName'
FROM
	[orderlek].[tDeliveryMethodType]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tDeliveryMethodDependency]'
GO
ALTER TABLE [orderlek].[tDeliveryMethodDependency] ADD CONSTRAINT [FK_tDeliveryMethodDependency_tDeliveryMethod_Base] FOREIGN KEY ([DeliveryMethodDependencyId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
ALTER TABLE [orderlek].[tDeliveryMethodDependency] ADD CONSTRAINT [FK_tDeliveryMethodDependency_tDeliveryMethod_Optional] FOREIGN KEY ([OptionalDeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
