SET IDENTITY_INSERT [orderlek].[tDeliveryMethodType] ON
INSERT [orderlek].[tDeliveryMethodType] ([DeliveryMethodTypeId], [Title], [CommonName]) VALUES (1, N'Base', N'Base')
INSERT [orderlek].[tDeliveryMethodType] ([DeliveryMethodTypeId], [Title], [CommonName]) VALUES (2, N'Optional', N'Optional')
SET IDENTITY_INSERT [orderlek].[tDeliveryMethodType] OFF
