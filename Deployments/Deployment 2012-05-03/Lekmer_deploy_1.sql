/*
Run this script on a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 008\DB\LekmerDB    -  This database will be modified. The scripts folder will not be modified.

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 009\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 03.05.2012 9:09:54

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] DROP
CONSTRAINT [FK_tProductPopularity_tChannel],
CONSTRAINT [FK_tProductPopularity_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [review].[tRatingReviewFeedbackLike]'
GO
ALTER TABLE [review].[tRatingReviewFeedbackLike] DROP
CONSTRAINT [FK_tRatingReviewFeedbackLike_tRatingReviewFeedback],
CONSTRAINT [FK_tRatingReviewFeedbackLike_tRatingReviewUser]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] DROP CONSTRAINT [PK_tProductPopularity]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [review].[tRatingReviewFeedbackLike]'
GO
ALTER TABLE [review].[tRatingReviewFeedbackLike] DROP CONSTRAINT [PK_tRatingReviewFeedbackLike]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [review].[pRatingReviewFeedback_GetMostHelpful]'
GO
DROP PROCEDURE [review].[pRatingReviewFeedback_GetMostHelpful]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [review].[pRatingReviewFeedback_RemoveInappropriateFlag]'
GO
DROP PROCEDURE [review].[pRatingReviewFeedback_RemoveInappropriateFlag]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tProductPopularity]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tProductPopularity]
(
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Popularity] [int] NOT NULL,
[SalesAmount] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tProductPopularity]([ChannelId], [ProductId], [Popularity]) SELECT [ChannelId], [ProductId], [Popularity] FROM [lekmer].[tProductPopularity]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tProductPopularity]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tProductPopularity]', N'tProductPopularity'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductPopularity] on [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] ADD CONSTRAINT [PK_tProductPopularity] PRIMARY KEY CLUSTERED  ([ChannelId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tBlockProductLatestFeedbackList]'
GO
CREATE TABLE [review].[tBlockProductLatestFeedbackList]
(
[BlockId] [int] NOT NULL,
[NumberOfItems] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockProductLatestFeedbackList] on [review].[tBlockProductLatestFeedbackList]'
GO
ALTER TABLE [review].[tBlockProductLatestFeedbackList] ADD CONSTRAINT [PK_tBlockProductLatestFeedbackList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [review].[tRatingReviewFeedbackLike]'
GO
CREATE TABLE [review].[tmp_rg_xx_tRatingReviewFeedbackLike]
(
[RatingReviewFeedbackLikeId] [int] NOT NULL IDENTITY(1, 1),
[RatingReviewFeedbackId] [int] NOT NULL,
[IPAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[RatingReviewUserId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [review].[tmp_rg_xx_tRatingReviewFeedbackLike] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [review].[tmp_rg_xx_tRatingReviewFeedbackLike]([RatingReviewFeedbackLikeId], [RatingReviewFeedbackId], [IPAddress], [CreatedDate], [RatingReviewUserId]) SELECT [RatingReviewFeedbackLikeId], [RatingReviewFeedbackId], [IPAddress], [CreatedDate], [RatingReviewUserId] FROM [review].[tRatingReviewFeedbackLike]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [review].[tmp_rg_xx_tRatingReviewFeedbackLike] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [review].[tRatingReviewFeedbackLike]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[review].[tmp_rg_xx_tRatingReviewFeedbackLike]', N'tRatingReviewFeedbackLike'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingReviewFeedbackLike] on [review].[tRatingReviewFeedbackLike]'
GO
ALTER TABLE [review].[tRatingReviewFeedbackLike] ADD CONSTRAINT [PK_tRatingReviewFeedbackLike] PRIMARY KEY CLUSTERED  ([RatingReviewFeedbackLikeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[tReview]'
GO
ALTER TABLE [review].[tReview] ADD
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vProductPopularity]'
GO

ALTER VIEW [lekmer].[vProductPopularity]
AS
SELECT 
	pp.[ProductId] AS 'ProductPopularity.ProductId',
	pp.[ChannelId] AS 'ProductPopularity.ChannelId',
	pp.[Popularity] AS 'ProductPopularity.Popularity',
	pp.[SalesAmount] AS 'ProductPopularity.SalesAmount'
FROM 
	lekmer.[tProductPopularity] AS pp
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductPopularityGetAll]'
GO

CREATE PROCEDURE [lekmer].[pProductPopularityGetAll]
	@ChannelId	INT
AS	
BEGIN
	SET NOCOUNT ON	
	
	SELECT
		pp.*
	FROM
		[lekmer].[vProductPopularity] pp
	WHERE
		pp.[ProductPopularity.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockProductLatestFeedbackListDelete]'
GO

CREATE PROCEDURE [review].[pBlockProductLatestFeedbackListDelete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockProductLatestFeedbackList]
	WHERE BlockId = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockProductLatestFeedbackList]'
GO

CREATE VIEW [review].[vBlockProductLatestFeedbackList]
AS
	SELECT
		[BlockId] AS 'BlockProductLatestFeedbackList.BlockId',
		[NumberOfItems] AS 'BlockProductLatestFeedbackList.NumberOfItems'
	FROM
		[review].[tBlockProductLatestFeedbackList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockProductLatestFeedbackListSave]'
GO

CREATE PROCEDURE [review].[pBlockProductLatestFeedbackListSave]
	@BlockId INT,
	@NumberOfItems INT
AS
BEGIN
	UPDATE [review].[tBlockProductLatestFeedbackList]
	SET
		[NumberOfItems] = @NumberOfItems
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockProductLatestFeedbackList] (
		[BlockId],
		[NumberOfItems]
	)
	VALUES (
		@BlockId,
		@NumberOfItems
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_IncreaseLikeHit]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_IncreaseLikeHit]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE [review].[tRatingReviewFeedback]
	SET [LikeHit] = [LikeHit] + 1
	WHERE [RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_GetById]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_GetById]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
	WHERE 
		rrf.[RatingReviewFeedback.RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_UpdateInappropriateFlag]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_UpdateInappropriateFlag]
	@RatingReviewFeedbackId	INT,
	@InappropriateFlag BIT
AS    
BEGIN
	UPDATE 
		[review].[tRatingReviewFeedback]
	SET 
		Impropriate = @InappropriateFlag
	WHERE 
		RatingReviewFeedbackId = @RatingReviewFeedbackId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[vReview]'
GO


ALTER VIEW [review].[vReview]
AS
	SELECT
		r.[ReviewId] 'Review.ReviewId',
		r.[RatingReviewFeedbackId] 'Review.RatingReviewFeedbackId',
		r.[AuthorName] 'Review.AuthorName',
		r.[Title] 'Review.Title',
		r.[Message] 'Review.Message',
		r.[Email] 'Review.Email'
	FROM 
		[review].[tReview] r

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[pReviewInsert]'
GO

ALTER PROCEDURE [review].[pReviewInsert]
	@RatingReviewFeedbackId INT,
	@AuthorName NVARCHAR(50),
	@Title NVARCHAR(50),
	@Message NVARCHAR(3000),
	@Email VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tReview] (
		[RatingReviewFeedbackId],
		[AuthorName],
		[Title],
		[Message],
		[Email]
	)
	VALUES (
		@RatingReviewFeedbackId,
		@AuthorName,
		@Title,
		@Message,
		@Email
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedbackLikeInsert]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedbackLikeInsert]
	@RatingReviewFeedbackId INT,
	@IPAddress VARCHAR(50),
	@CreatedDate DATETIME,
	@RatingReviewUserId INT
AS
BEGIN
	SET NOCOUNT ON
	
	IF EXISTS
	(
		SELECT 
			1 
		FROM 
			[review].[tRatingReviewFeedbackLike]
		WHERE 
			[RatingReviewFeedbackId] = @RatingReviewFeedbackId
			AND [RatingReviewUserId] = @RatingReviewUserId
	)
	
	RETURN -1
	
	EXEC [review].[pRatingReviewFeedback_IncreaseLikeHit] @RatingReviewFeedbackId
	
	INSERT INTO [review].[tRatingReviewFeedbackLike] (
		[RatingReviewFeedbackId],
		[IPAddress],
		[CreatedDate],
		[RatingReviewUserId]
	)
	VALUES (
		@RatingReviewFeedbackId,
		@IPAddress,
		@CreatedDate,
		@RatingReviewUserId
	)
	
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[pRatingReviewFeedback_Delete]'
GO
ALTER PROCEDURE [review].[pRatingReviewFeedback_Delete]
	@RatingReviewFeedbackId	INT
AS    
BEGIN
	SET NOCOUNT ON
	
	EXEC [review].[pRatingReviewFeedbackLikeDelete] @RatingReviewFeedbackId
	EXEC [review].[pRatingItemProductVoteDelete] @RatingReviewFeedbackId
	EXEC [review].[pReviewDeleteAll] @RatingReviewFeedbackId
	
	DELETE FROM [review].[tRatingReviewFeedback]
	WHERE RatingReviewFeedbackId = @RatingReviewFeedbackId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_GetMostHelpfulByProduct]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_GetMostHelpfulByProduct]
	@ChannelId				INT,
	@ProductId				INT,
	@RatingId				INT,
	@RatingItemIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@RatingReviewStatusId	INT,
	@IsImpropriate			BIT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		TOP(1) rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
		INNER JOIN [review].[vRatingItemProductVote] ripv 
			ON ripv.[RatingItemProductVote.RatingReviewFeedbackId] = rrf.[RatingReviewFeedback.RatingReviewFeedbackId]
		INNER JOIN [generic].[fnConvertIDListToTable](@RatingItemIds, @Delimiter) ri 
			ON ri.[ID] = ripv.[RatingItemProductVote.RatingItemId]
	WHERE 
		rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
		AND rrf.[RatingReviewFeedback.ProductId] = @ProductId
		AND rrf.[RatingReviewFeedback.RatingReviewStatusId] = @RatingReviewStatusId
		AND rrf.[RatingReviewFeedback.Impropriate] = @IsImpropriate
		AND ripv.[RatingItemProductVote.RatingId] = @RatingId
	ORDER BY
		rrf.[RatingReviewFeedback.LikeHit] DESC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockProductLatestFeedbackListGetById]'
GO

CREATE PROCEDURE [review].[pBlockProductLatestFeedbackListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		bplfl.*,
		b.*
	FROM 
		[review].[vBlockProductLatestFeedbackList] AS bplfl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON bplfl.[BlockProductLatestFeedbackList.BlockId] = b.[Block.BlockId]
	WHERE
		bplfl.[BlockProductLatestFeedbackList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockProductLatestFeedbackListGetByIdSecure]'
GO

CREATE PROCEDURE [review].[pBlockProductLatestFeedbackListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		bplfl.*,
		b.*
	FROM 
		[review].[vBlockProductLatestFeedbackList] AS bplfl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON bplfl.[BlockProductLatestFeedbackList.BlockId] = b.[Block.BlockId]
	WHERE
		bplfl.[BlockProductLatestFeedbackList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductUpdatePopularity]'
GO

ALTER PROCEDURE [lekmer].[pProductUpdatePopularity]
	@ChannelId INT,
	@DateTimeFrom DATETIME,
	@DateTimeTo DATETIME
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ProductPopularity TABLE(ProductId INT, Popularity INT, SalesAmount INT)
	
	INSERT @ProductPopularity
	(
		ProductId,
		Popularity,
		SalesAmount
	)
	SELECT
		p.[ProductId],
		SUM(oi.[Quantity] * oi.[ActualPriceIncludingVat]),
		SUM(oi.[Quantity])
	FROM
		product.tProduct p WITH(NOLOCK)
		INNER JOIN [order].tOrderItemProduct oip WITH(NOLOCK) on oip.ProductId = p.ProductId
		INNER JOIN [order].tOrderItem oi WITH(NOLOCK) on oi.OrderItemId = oip.OrderItemId
		INNER JOIN [order].tOrder o WITH(NOLOCK) on o.OrderId = oi.OrderId
	WHERE
		o.ChannelId = @ChannelId
		AND o.CreatedDate BETWEEN ISNULL(@DateTimeFrom, o.CreatedDate) AND ISNULL(@DateTimeTo, o.CreatedDate)
		AND o.OrderStatusId = 4
	GROUP BY
		p.ProductId

	DELETE	[lekmer].[tProductPopularity]
	WHERE	@ChannelId = [ChannelId]

	INSERT INTO [lekmer].[tProductPopularity]
	(
		ChannelId,
		ProductId,
		Popularity,
		SalesAmount
	)
	SELECT
		@ChannelId,
		p.[Product.Id],
		pp.[Popularity],
		pp.[SalesAmount]
	FROM
		@ProductPopularity pp
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = pp.ProductId
		INNER JOIN [product].[vCustomPriceListItem] pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				NULL
			)
	WHERE
		p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pUpdateProductPopularityAllChannels]'
GO

ALTER PROCEDURE [lekmer].[pUpdateProductPopularityAllChannels]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @DateTo AS DATETIME;
	DECLARE @DateFrom AS DATETIME;
	DECLARE @ChannelId AS INT;
	DECLARE @ChannelTitle AS VARCHAR(80);

	SET @DateTo = GETDATE();
	SET @DateFrom = DATEADD(d, -30, @DateTo);

	DECLARE @cursor CURSOR
	SET @cursor = CURSOR FOR SELECT [ChannelId], [Title] FROM [core].[tChannel];
  
	OPEN @cursor

	FETCH NEXT FROM @cursor 
	INTO @ChannelId, @ChannelTitle;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC [lekmer].[pProductUpdatePopularity]
			@ChannelId = @ChannelId,
			@DateTimeFrom = @DateFrom,
			@DateTimeTo = @DateTo;

		FETCH NEXT FROM @cursor 
		INTO @ChannelId, @ChannelTitle;
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_GetLatestByProduct]'
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_GetLatestByProduct]
	@ChannelId				INT,
	@ProductIds				VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@NumberOfItems			INT,
	@RatingReviewStatusId	INT,
	@IsImpropriate			BIT,
	@CustomerId				INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		TOP(@NumberOfItems) rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
		INNER JOIN [product].[vCustomProduct] p 
			ON p.[Product.Id] = rrf.[RatingReviewFeedback.ProductId] 
			AND p.[Product.ChannelId] = rrf.[RatingReviewFeedback.ChannelId]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) pl ON pl.[ID] = p.[Product.Id]
	WHERE 
		rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
		AND rrf.[RatingReviewFeedback.RatingReviewStatusId] = @RatingReviewStatusId
		AND rrf.[RatingReviewFeedback.Impropriate] = @IsImpropriate
	ORDER BY rrf.[RatingReviewFeedback.CreatedDate] DESC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_GetMostHelpfulByProductAndVariants]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_GetMostHelpfulByProductAndVariants]
	@ChannelId				INT,
	@ProductIds				VARCHAR(MAX),
	@RatingId				INT,
	@RatingItemIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@RatingReviewStatusId	INT,
	@IsImpropriate			BIT,
	@CustomerId				INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		TOP(1) rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
		INNER JOIN [review].[vRatingItemProductVote] ripv 
			ON ripv.[RatingItemProductVote.RatingReviewFeedbackId] = rrf.[RatingReviewFeedback.RatingReviewFeedbackId]
		INNER JOIN [generic].[fnConvertIDListToTable](@RatingItemIds, @Delimiter) ri 
			ON ri.[ID] = ripv.[RatingItemProductVote.RatingItemId]
		INNER JOIN [product].[vCustomProduct] p 
			ON p.[Product.Id] = rrf.[RatingReviewFeedback.ProductId] 
			AND p.[Product.ChannelId] = rrf.[RatingReviewFeedback.ChannelId]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) pl ON pl.[ID] = p.[Product.Id]
	WHERE 
		rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
		AND rrf.[RatingReviewFeedback.RatingReviewStatusId] = @RatingReviewStatusId
		AND rrf.[RatingReviewFeedback.Impropriate] = @IsImpropriate
		AND ripv.[RatingItemProductVote.RatingId] = @RatingId
	ORDER BY
		rrf.[RatingReviewFeedback.LikeHit] DESC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] ADD
CONSTRAINT [FK_tProductPopularity_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId]),
CONSTRAINT [FK_tProductPopularity_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tBlockProductLatestFeedbackList]'
GO
ALTER TABLE [review].[tBlockProductLatestFeedbackList] ADD
CONSTRAINT [FK_tBlockProductLatestFeedbackList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingReviewFeedbackLike]'
GO
ALTER TABLE [review].[tRatingReviewFeedbackLike] ADD
CONSTRAINT [FK_tRatingReviewFeedbackLike_tRatingReviewFeedback] FOREIGN KEY ([RatingReviewFeedbackId]) REFERENCES [review].[tRatingReviewFeedback] ([RatingReviewFeedbackId]),
CONSTRAINT [FK_tRatingReviewFeedbackLike_tRatingReviewUser] FOREIGN KEY ([RatingReviewUserId]) REFERENCES [review].[tRatingReviewUser] ([RatingReviewUserId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
