SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [review].[pRatingItemProductScoreGetSummaryByRatingAndProduct]'
GO
ALTER PROCEDURE [review].[pRatingItemProductScoreGetSummaryByRatingAndProduct]
	@ChannelId INT,
	@RatingId INT,
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT 
		[ri].[RatingItem.RatingId] 'RatingItemProductScoreSummary.RatingId',
		[ri].[RatingItem.RatingItemId] 'RatingItemProductScoreSummary.RatingItemId',
		[ri].[RatingItem.Score] 'RatingItemProductScoreSummary.RatingItemScore',
		ISNULL(SUM([ps].[RatingItemProductScore.HitCount]), 0) 'RatingItemProductScoreSummary.TotalHitCount'
	FROM 
		[review].[vRatingItemSecure] ri
		LEFT JOIN [review].[vRatingItemProductScore] ps ON
			[ps].[RatingItemProductScore.RatingId] = [ri].[RatingItem.RatingId]
			AND [ps].[RatingItemProductScore.RatingItemId] = [ri].[RatingItem.RatingItemId]
			AND [ps].[RatingItemProductScore.ProductId] = @ProductId
			AND [ps].[RatingItemProductScore.ChannelId] = @ChannelId
	WHERE
		 [ri].[RatingItem.RatingId] = @RatingId
	GROUP BY
		[ri].[RatingItem.RatingId],
		[ri].[RatingItem.RatingItemId],
		[ri].[RatingItem.Score]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pLekmerOrderGetToSendPacsoftInfo]'
GO
CREATE PROCEDURE [orderlek].[pLekmerOrderGetToSendPacsoftInfo]
	@ChannelId			INT,
	@HyId				VARCHAR(50),
	@SupplierId			NVARCHAR(500),
	@DaysAfterPurchase	INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CurrentDate DATETIME, @DateFrom DATETIME

	SET @CurrentDate = CONVERT (DATE, GETDATE())
	SET @DateFrom = (SELECT DATEADD(DAY, @DaysAfterPurchase * -1, @CurrentDate))
	
	SELECT
		[o].*
	FROM
		[order].[vCustomOrder] o
		INNER JOIN [order].[tOrderItem] oi ON [oi].[OrderId] = [o].[Order.OrderId]
		INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[OrderItemId] = [oi].[OrderItemId]
		INNER JOIN [lekmer].[tLekmerProduct] p ON [p].[ProductId] = [oip].[ProductId]
	WHERE
		[o].[Order.ChannelId] = @ChannelId
		AND [o].[OrderStatus.Id] = 4
		AND [o].[Order.CreatedDate] >= @DateFrom
		AND [o].[Order.CreatedDate] < @CurrentDate
		AND [p].[HYErpId] = @HyId
		AND [p].[SupplierId] = @SupplierId

	UNION

	SELECT
		[o].*
	FROM
		[order].[vCustomOrder] o
		INNER JOIN [orderlek].[tPackageOrderItem] oi ON [oi].[OrderId] = [o].[Order.OrderId]
		INNER JOIN [orderlek].[tPackageOrderItemProduct] oip ON [oip].[OrderItemId] = [oi].[OrderItemId]
		INNER JOIN [lekmer].[tLekmerProduct] p ON [p].[ProductId] = [oip].[ProductId]
	WHERE
		[o].[Order.ChannelId] = @ChannelId
		AND [o].[OrderStatus.Id] = 4
		AND [o].[Order.CreatedDate] >= @DateFrom
		AND [o].[Order.CreatedDate] < @CurrentDate
		AND [p].[HYErpId] = @HyId
		AND [p].[SupplierId] = @SupplierId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
