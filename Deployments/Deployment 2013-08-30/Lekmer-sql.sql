SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [media].[vCustomMedia]'
GO

ALTER VIEW [media].[vCustomMedia]
AS
SELECT
	*
FROM
	[media].[vMedia]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [media].[vImage]'
GO

ALTER VIEW [media].[vImage]
AS
SELECT
	i.MediaId AS 'Image.MediaId',
	i.Width AS 'Image.Width',
	i.Height AS 'Image.Height',
	COALESCE(it.AlternativeText, i.AlternativeText) AS 'Image.AlternativeText',
	m.*,
	mf.Extension AS 'MediaFormat.Extension',
	mf.CommonName AS 'MediaFormat.CommonName',
	l.LanguageId AS 'Image.LanguageId'
FROM
	media.tImage i
	CROSS JOIN core.tLanguage l
	INNER JOIN media.vCustomMedia m ON i.MediaId = m.[Media.Id]
	INNER JOIN media.tMediaFormat mf ON m.[Media.FormatId] = mf.MediaFormatId
	LEFT JOIN media.tImageTranslation it ON it.MediaId = i.MediaId AND it.LanguageId = l.LanguageId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [media].[vImageSecure]'
GO

ALTER VIEW [media].[vImageSecure]
AS
SELECT
	i.MediaId AS 'Image.MediaId',
	i.Width AS 'Image.Width',
	i.Height AS 'Image.Height',
	i.AlternativeText AS 'Image.AlternativeText',
	m.*,
	media.tMediaFormat.Extension AS 'MediaFormat.Extension',
	media.tMediaFormat.CommonName AS 'MediaFormat.CommonName'
FROM
	media.tImage i
	INNER JOIN media.vCustomMedia m ON i.MediaId = m.[Media.Id]
	INNER JOIN media.tMediaFormat ON m.[Media.FormatId] = media.tMediaFormat.MediaFormatId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pImageGetAllBySizeTable]'
GO
CREATE PROCEDURE [lekmer].[pImageGetAllBySizeTable]
	@ChannelId INT,
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[i].*
	FROM   
		[media].[vCustomImage] i
		INNER JOIN [core].[tChannel] c ON [c].[LanguageId] = [i].[Image.LanguageId]
		INNER JOIN [lekmer].[tSizeTableMedia] stm ON [stm].[MediaId] = [i].[Media.Id]
	WHERE  
		[c].[ChannelId] = @ChannelId
		AND
		[stm].[SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [media].[vCustomMediaFormat]'
GO


ALTER VIEW [media].[vCustomMediaFormat]
AS
SELECT
	*
FROM
	[media].[vMediaFormat]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pMediaItemGetAllBySizeTable]'
GO
ALTER PROCEDURE [lekmer].[pMediaItemGetAllBySizeTable]
	@SizeTableId INT
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		m.*
	FROM
		[lekmer].[tSizeTableMedia] stm
		INNER JOIN [media].[vCustomMedia] m ON [m].[Media.Id] = [stm].[MediaId]
	WHERE
		stm.[SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
