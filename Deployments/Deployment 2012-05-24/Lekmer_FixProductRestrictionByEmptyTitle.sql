/*
Run this script on:

        (local).Lekmer_212_0    -  This database will be modified

to synchronize it with:

        (local).Lekmer_212_1

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 23.05.2012 18:20:45

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [integration].[usp_FokRestrictionsProducts]'
GO
ALTER PROCEDURE [integration].[usp_FokRestrictionsProducts]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION
		--------------------------------------------------
		--- Update tProductTranslation with new products
		--------------------------------------------------
		-- NO
		INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
		SELECT p.productid, 1000001 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT productid FROM [product].[tProductTranslation] WHERE LanguageId = 1000001)
		
		-- DK
		INSERT INTO product.tProductTranslation (ProductId, LanguageId)
		SELECT p.productid, 1000002 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000002)

		-- FI		
		INSERT INTO product.tProductTranslation(ProductId, LanguageId)
		SELECT p.productid, 1000003 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000003)


		----------------------------------------------------------------------------
		--- Remove product restriction with reason = 'No Translation'
		----------------------------------------------------------------------------
		DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
		WHERE RestrictionReason = 'No Translation'
		AND UserId IS NULL


		----------------------------------------------------------------------------
		--- Insert not translated products into the tProductRegistryRestrictionProduct
		----------------------------------------------------------------------------
		-- NO
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			2,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000001 -- NO
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 2)

		-- DK
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			3,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000002 -- DK
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 3)

		-- FI
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			4,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000003 -- FI
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 4)



		----------------------------------------------------------------------------
		--- Insert tProductRegistryRestrictionBrand
		----------------------------------------------------------------------------
		--------------------- BRANDS ALLA KANALER (NO, DK, FI) ---------------------
		DECLARE @RestrictionBrand TABLE (BrandId INT, ProductRegistryId INT)
		INSERT INTO @RestrictionBrand (BrandId, ProductRegistryId) 
		VALUES 
			(1000232, 2), (1000235, 2), (1000347, 2),
			(1000232, 3), (1000235, 3),	(1000347, 3), (1000357, 3),
			(1000232, 4), (1000235, 4),	(1000347, 4)
	
		INSERT INTO	[lekmer].[tProductRegistryRestrictionBrand] (
			ProductRegistryId,
			BrandId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			ProductRegistryId,
			BrandId,
			'Brand Restriction',
			NULL,
			GETDATE()
		FROM
			@RestrictionBrand
		WHERE NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionBrand] prrb
						  WHERE BrandId = prrb.BrandId AND prrb.ProductRegistryId = ProductRegistryId)



		----------------------------------------------------------------------------
		--- Insert tProductRegistryRestrictionCategory
		----------------------------------------------------------------------------
		DECLARE @RestrictionCategory TABLE (CategoryId INT, ProductRegistryId INT)
		INSERT INTO @RestrictionCategory (CategoryId, ProductRegistryId) 
		VALUES 
			(1000842, 2), (1000842, 3), (1000842, 4), --böcker och tidningar  -- C_30-119
			(1000985, 2), (1000985, 3), (1000985, 4), --böcker och tidningar  -- C_30-124
			(1000590, 2), (1000590, 3), (1000590, 4), --Barnspel
			(1000989, 2), (1000989, 3), (1000989, 4), --Vuxenspel
			(1000996, 2), (1000996, 3), (1000996, 4), --Familjespel
			(1000997, 2), (1000997, 3), (1000997, 4), --Tv-spel och datorspel
			(1000999, 2), (1000999, 3), (1000999, 4), --Utomhusspel
			(1001000, 2), (1001000, 3), (1001000, 4), --Strategispel
			(1001001, 2), (1001001, 3), (1001001, 4), --Pocketspel
			(1001002, 2), (1001002, 3), (1001002, 4), --Klassiker
			(1001005, 2), (1001005, 3), (1001005, 4), --Resespel
			(1001006, 2), (1001006, 3), (1001006, 4), --Memoryspel
			(1001008, 2), (1001008, 3), (1001008, 4), --Brädspel
			(1001009, 2), (1001009, 3), (1001009, 4), --Barnpussel
			(1001010, 2), (1001010, 3), (1001010, 4), --Startegispel
			(1001014, 2), (1001014, 3), (1001014, 4)  --Strategispel
			
		INSERT INTO	[lekmer].[tProductRegistryRestrictionCategory] (
			ProductRegistryId,
			CategoryId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			ProductRegistryId,
			CategoryId,
			'Category Restriction',
			NULL,
			GETDATE()
		FROM
			@RestrictionCategory
		WHERE NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionCategory] prrc
						  WHERE CategoryId = prrc.CategoryId AND prrc.ProductRegistryId = ProductRegistryId)		



		---------------------------------------------------------------------
		---- Get Product Ids that need add to product.tProductRegistryProduct.
		----------------------------------------------------------------------
		DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]

		-- NO
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 2 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 2)

		-- DK
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 3 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 3)
		
		-- FI
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 4 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 4)

		
																
		--------------------------------------------------------------------------------
		---- Delete items from tProductRegistryProduct and from list products to insert.
		--------------------------------------------------------------------------------
		-- By Product restroction
		DELETE prp
		FROM [product].[tProductRegistryProduct] prp
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = prp.ProductRegistryId 
																		 AND prrp.ProductId = prp.ProductId
		-- By Brand restroction
		DELETE prp 
		FROM [product].[tProductRegistryProduct] prp
			 INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = prp.[ProductId]
			 INNER JOIN [lekmer].[tProductRegistryRestrictionBrand] prrb ON prrb.[BrandId] = lp.[BrandId] 
																			AND prrb.[ProductRegistryId] = prp.[ProductRegistryId]
		-- By Category restroction
		DECLARE @CategoryRestriction TABLE(CategoryId INT, ProductRegistryId INT)
		INSERT INTO @CategoryRestriction (CategoryId, ProductRegistryId)
		EXEC [lekmer].[pProductRegistryRestrictionCategoryGetAll]
	
		DELETE prp 
		FROM [product].[tProductRegistryProduct] prp
			 INNER JOIN [product].[tProduct] p ON p.[ProductId] = prp.[ProductId]
			 INNER JOIN @CategoryRestriction c ON c.[CategoryId] = p.[CategoryId]
											   AND c.[ProductRegistryId] = prp.[ProductRegistryId]


		DELETE pIds
		FROM @ProductIdsToInsert pIds
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = pIds.ProductRegistryId 
																		 AND prrp.ProductId = pIds.ProductId


		EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert	
				
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			'',
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)
	END CATCH
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pBackofficeProductImportById]'
GO
ALTER PROCEDURE [integration].[pBackofficeProductImportById]
	@HYErpId		NVARCHAR(20),
	@Title			NVARCHAR(256),
	@Description	NVARCHAR(MAX),
	@ChannelNameISO	NVARCHAR(10),
	@TagIds			VARCHAR(MAX),
	@UserName		NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Get language id by Channel
	DECLARE @LanguageId INT
	DECLARE @ChannelId INT
	
	SELECT 
		@LanguageId = l.LanguageId,
		@ChannelId = h.ChannelId
	FROM [core].[tLanguage] l
		 INNER JOIN [core].[tChannel] h ON l.LanguageId = h.LanguageId
		 INNER JOIN [core].[tCountry] c ON h.CountryId = c.CountryId
	WHERE
		c.ISO = @ChannelNameISO
	
	-- Get product id by HYErpId
	DECLARE @ProductId INT
	SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] WHERE HYErpId = @HYErpId)

	-- insert into tProductTransaltion empty rows
	INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
	SELECT
		p.ProductId,
		@LanguageId
	FROM
		[product].[tProduct] p
	WHERE
		NOT EXISTS (SELECT 1 FROM [product].[tProductTranslation] n
					WHERE n.ProductId = p.ProductId 
					AND n.LanguageId = @LanguageId)
				
	BEGIN TRY
		BEGIN TRANSACTION				
			
		IF @LanguageId != 1
			BEGIN
				UPDATE [product].[tProductTranslation]
				SET Title = @Title
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (Title != @Title OR Title IS NULL)
	
	
				/*BEGIN*/
				-- Remove Restriction from [lekmer].[tProductRegistryRestrictionProduct] if it was added due to empty title.
				-- Get product id by HYErpId
				DECLARE @ProductRegistryId INT
				SET @ProductRegistryId = (SELECT ProductRegistryId FROM [product].[tProductModuleChannel] WHERE ChannelId = @ChannelId)
	
				-- Insert product to tProductRegistryproduct
				IF @Title IS NOT NULL
				BEGIN
					DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
					WHERE
						ProductRegistryId = @ProductRegistryId
						AND ProductId = @ProductId
						AND RestrictionReason = 'No Translation'
						AND UserId IS NULL
						
					DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]
					INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
					VALUES (@ProductId, @ProductRegistryId)
					
					EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
				END
				/*END*/
				
					
				UPDATE [product].[tProductTranslation]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END
		ELSE
			BEGIN
				UPDATE [product].[tProduct]
				SET Title = @Title
				WHERE
					ProductId = @ProductId
					
				UPDATE [product].[tProduct]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END
			
		-- Update/Insert product tags.
		DECLARE @TagId INT
		DECLARE cur_tag CURSOR FAST_FORWARD FOR
			SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')
			
		OPEN cur_tag
		FETCH NEXT FROM cur_tag INTO @TagId
			
		WHILE @@FETCH_STATUS = 0
			BEGIN
				-- Update/Insert product tags.
				IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
						   INNER JOIN product.tProduct p ON pt.ProductId = p.ProductId	
						   INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
						   WHERE 
								lp.HYErpId = @HYErpId
								AND pt.TagId = @TagId)
					BEGIN
						INSERT INTO lekmer.tProductTag (ProductId, TagId)
						VALUES ((SELECT TOP(1) p.ProductId FROM product.tProduct p 
								INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
								WHERE lp.HYErpId = @HYErpId), @TagId)
					END
				
				FETCH NEXT FROM cur_tag INTO @TagId
			END
		CLOSE cur_tag
		DEALLOCATE cur_tag
		
		-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
		INSERT INTO integration.tBackofficeProductInfoImport
		(
			[HYErpId]
			,[Title]
			,[Description]
			,[ChannelNameISO]
			,[TagIdCollection]
			,[UserName]
			,[InsertedDate]
		)
		VALUES 
		(
			@HYErpId
			,@Title
			,@Description
			,@ChannelNameISO
			,@TagIds
			,@UserName
			,CAST(GETDATE() AS SMALLDATETIME)
		)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ChannelRestrictionsProducts]'
GO
ALTER PROCEDURE [integration].[usp_ChannelRestrictionsProducts]
AS
BEGIN
	SET NOCOUNT ON
	
	BEGIN TRY
		BEGIN TRANSACTION
		--------------------------------------------------
		--- Update tProductTranslation with new products
		--------------------------------------------------
		-- NO
		INSERT INTO product.tProductTranslation (ProductId, LanguageId)
		SELECT p.productid, 1000001 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000001)

		-- DK
		INSERT INTO product.tProductTranslation (ProductId, LanguageId)
		SELECT p.productid, 1000002 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000002)

		-- FI
		INSERT INTO product.tProductTranslation(ProductId, LanguageId)
		SELECT p.productid, 1000003 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000003)


		----------------------------------------------------------------------------
		--- Remove product restriction with reason = 'No Translation'
		----------------------------------------------------------------------------
		DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
		WHERE RestrictionReason = 'No Translation'
		AND UserId IS NULL
		

		----------------------------------------------------------------------------
		--- Insert not translated products in the tProductRegistryRestrictionProduct
		----------------------------------------------------------------------------
		-- NO
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			2,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000001 -- NO
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 2)

		-- DK
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			3,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000002 -- DK
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 3)

		-- FI
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			4,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000003 -- FI
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 4)


		----------------------------------------------------------------------
		---- Get Product Ids that need add to product.tProductRegistryProduct.
		----------------------------------------------------------------------
		DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]

		-- NO
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 2 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 2)

		-- DK
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 3 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 3)
		
		-- FI
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 4 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 4)


		--------------------------------------------------------------------------------
		---- Delete items from tProductRegistryProduct and from list products to insert.
		--------------------------------------------------------------------------------
		DELETE prp
		FROM [product].[tProductRegistryProduct] prp
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = prp.ProductRegistryId 
																		 AND prrp.ProductId = prp.ProductId

		DELETE pIds
		FROM @ProductIdsToInsert pIds
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = pIds.ProductRegistryId 
																		 AND prrp.ProductId = pIds.ProductId


		EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
		
	COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			'',
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)
	END CATCH
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
