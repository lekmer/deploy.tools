BEGIN TRANSACTION
DELETE
	prrp
FROM
	[lekmer].[tProductRegistryRestrictionProduct] prrp
WHERE
	prrp.RestrictionReason = 'No Translation'
	AND prrp.UserId IS NULL
	AND EXISTS (SELECT 1 FROM [product].[tProductTranslation] pt
				WHERE pt.ProductId = prrp.ProductId 
					  AND pt.Title IS NOT NULL)
ROLLBACK TRANSACTION