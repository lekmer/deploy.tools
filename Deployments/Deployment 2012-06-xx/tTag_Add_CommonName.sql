ALTER TABLE [lekmer].[tTag]
ADD CommonName VARCHAR(255)

UPDATE [lekmer].[tTag]
SET CommonName = TagId

ALTER TABLE [lekmer].[tTag]
ALTER COLUMN CommonName VARCHAR(255) NOT NULL