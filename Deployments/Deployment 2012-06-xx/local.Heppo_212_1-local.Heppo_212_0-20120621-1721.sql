/*
Run this script on:

(local).Heppo_212_0    -  This database will be modified

to synchronize it with:

(local).Heppo_212_1

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 9.0.0 from Red Gate Software Ltd at 21.06.2012 17:21:18

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Update rows in [template].[tModelFragmentFunction]
UPDATE [template].[tModelFragmentFunction] SET [Description]=N'Indicates if valid voucher was used and accepted by system. If true, voucher could be still not applied.' WHERE [FunctionId]=1000230
UPDATE [template].[tModelFragmentFunction] SET [Description]=N'Indicates if valid voucher was used and accepted by system. If true, voucher could be still not applied.' WHERE [FunctionId]=1000595
UPDATE [template].[tModelFragmentFunction] SET [Description]=N'Indicates if valid voucher was used and accepted by system. If true, voucher could be still not applied.' WHERE [FunctionId]=1000601
-- Operation applied to 3 rows out of 3

-- Add rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000604, 84, 2, N'IsVoucherWithoutDiscount', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000605, 84, 2, N'IsVoucherApplied', N'Indicates if voucher discount was applied to cart/order. True also for vouchers with zero discount.')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000606, 139, 2, N'IsVoucherApplied', N'Indicates if voucher discount was applied to cart/order. True also for vouchers with zero discount.')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000607, 1000061, 2, N'IsVoucherUsed', N'Indicates if valid voucher was used and accepted by system. If true, voucher could be still not applied.')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000608, 1000061, 2, N'IsVoucherApplied', N'Indicates if voucher discount was applied to cart/order. True also for vouchers with zero discount.')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000609, 1000061, 1, N'UsedVoucherInfo', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000610, 1000075, 2, N'IsVoucherApplied', N'Indicates if voucher discount was applied to cart/order. True also for vouchers with zero discount.')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
-- Operation applied to 7 rows out of 7

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
COMMIT TRANSACTION
GO
