IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[integration].[tTmpHyColor]') AND type in (N'U'))
	DROP TABLE [integration].[tTmpHyColor]
GO

CREATE TABLE [integration].[tTmpHyColor] (
	HyErpId	NVARCHAR(5),
	Value	NVARCHAR (50),
	LanguageIso	CHAR(2)
)
GO

-- SE
INSERT INTO integration.tTmpHyColor (HyErpId, Value, LanguageIso)
VALUES ('0000','**Färglös','SV'),('1000','Okänd','SV'),('1001','Svart','SV'),('1002','Vit','SV'),('1003','Blå','SV'),('1004','Baby blå','SV'),('1005','Beige','SV'),('1006','Guld','SV'),('1007','Ljusblå','SV'),('1008','Lila','SV'),('1010','Silver','SV'),('1011','Röd','SV'),('1012','Grön','SV'),('1013','Gul','SV'),('1014','Brun','SV'),('1015','Naturvit','SV'),('1016','Marinblå','SV'),('1017','Rosa','SV'),('1018','Denim','SV'),('1019','Grå','SV'),('1020','Ljusrosa','SV'),('1021','Turkos','SV'),('1022','Mönstrad','SV'),('1023','Mörk denim','SV'),('1024','Ljus denim','SV'),('1025','Orange','SV'),('1026','Lime','SV'),('1027','Khaki','SV'),('1028','Blandade färger','SV')
GO
-- NO
INSERT INTO integration.tTmpHyColor (HyErpId, Value, LanguageIso)
VALUES ('0000','**Fargeløs','NO'),('1000','Okänd','NO'),('1001','Svart','NO'),('1002','Vit','NO'),('1003','Blå','NO'),('1004','Baby blå','NO'),('1005','Beige','NO'),('1006','Guld','NO'),('1007','Ljusblå','NO'),('1008','Lila','NO'),('1010','Silver','NO'),('1011','Röd','NO'),('1012','Grön','NO'),('1013','Gul','NO'),('1014','Brun','NO'),('1015','Naturvit','NO'),('1016','Marinblå','NO'),('1017','Rosa','NO'),('1018','Denim','NO'),('1019','Grå','NO'),('1020','Ljusrosa','NO'),('1021','Turquoise','NO'),('1022','Mönstrad','NO'),('1023','Mörk denim','NO'),('1024','Ljus denim','NO'),('1025','Orange','NO'),('1026','Lime','NO'),('1027','Khaki','NO'),('1028','Blandade färger','NO')
GO
-- DK
INSERT INTO integration.tTmpHyColor (HyErpId, Value, LanguageIso)
VALUES ('0000','**Uden farve','DA'),('1000','Okänd','DA'),('1001','Svart','DA'),('1002','Vit','DA'),('1003','Blå','DA'),('1004','Baby blå','DA'),('1005','Beige','DA'),('1006','Guld','DA'),('1007','Ljusblå','DA'),('1008','Lila','DA'),('1010','Silver','DA'),('1011','Röd','DA'),('1012','Grön','DA'),('1013','Gul','DA'),('1014','Brun','DA'),('1015','Naturvit','DA'),('1016','Marinblå','DA'),('1017','Rosa','DA'),('1018','Denim','DA'),('1019','Grå','DA'),('1020','Ljusrosa','DA'),('1021','Turquoise','DA'),('1022','Mönstrad','DA'),('1023','Mörk denim','DA'),('1024','Ljus denim','DA'),('1025','Orange','DA'),('1026','Lime','DA'),('1027','Khaki','DA'),('1028','Blandade färger','DA')
GO
-- FI
INSERT INTO integration.tTmpHyColor (HyErpId, Value, LanguageIso)
VALUES ('0000','**Väritön','FI'),('1000','Okänd','FI'),('1001','Black','FI'),('1002','White','FI'),('1003','Blue','FI'),('1004','Baby blue','FI'),('1005','Beige','FI'),('1006','Gold','FI'),('1007','Light blue','FI'),('1008','Purple','FI'),('1010','Silcer','FI'),('1011','Red','FI'),('1012','Green','FI'),('1013','Yellow','FI'),('1014','Brown','FI'),('1015','Offwhite','FI'),('1016','Navy','FI'),('1017','Pink','FI'),('1018','Denim','FI'),('1019','Grey','FI'),('1020','Light pink','FI'),('1021','Turquoise','FI'),('1022','Multi colour','FI'),('1023','Dark denim','FI'),('1024','Light denim','FI'),('1025','Orange','FI'),('1026','Lime','FI'),('1027','Khaki','FI'),('1028','Mixed colours','FI')
GO

INSERT INTO [productlek].[tColor] ([HyErpId],[Value],[CommonName])
SELECT HyErpId, Value, REPLACE(REPLACE(REPLACE(Value,' ',''),'/',''),'*','') FROM [integration].[tTmpHyColor] WHERE [LanguageIso] = 'SV'
GO

INSERT INTO [productlek].[tColorTranslation] ([ColorId],[LanguageId],[Value])
SELECT [c].[ColorId], [l].[LanguageId], [thc].[Value] 
FROM [integration].[tTmpHyColor] thc
INNER JOIN [productlek].[tColor] c ON [c].[HyErpId] = [thc].[HyErpId]
INNER JOIN [core].[tLanguage] l ON [l].[ISO] = [thc].[LanguageIso]
GO


DROP TABLE [integration].[tTmpHyColor]