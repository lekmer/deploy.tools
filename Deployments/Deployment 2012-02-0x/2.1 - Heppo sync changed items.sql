SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
USE	[Heppo_live]
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

--/////////////////////////////////////
BEGIN TRANSACTION

PRINT N'Altering [lekmer].[vFilterProduct]'
GO


ALTER VIEW [lekmer].[vFilterProduct]
AS
	SELECT
		p.[ProductId] 'FilterProduct.ProductId',
		p.[CategoryId] 'FilterProduct.CategoryId',
		COALESCE(pt.[Title], p.[Title]) 'FilterProduct.Title',
		lp.[BrandId] 'FilterProduct.BrandId',
		lp.[AgeFromMonth] 'FilterProduct.AgeFromMonth',
		lp.[AgeToMonth] 'FilterProduct.AgeToMonth',
		lp.[IsNewFrom] 'FilterProduct.IsNewFrom',
		ch.[ChannelId] 'FilterProduct.ChannelId',
		ch.[CurrencyId] 'FilterProduct.CurrencyId',
		pmc.[PriceListRegistryId] 'FilterProduct.PriceListRegistryId',
		lpp.[ProductPopularity.Popularity] AS 'FilterProduct.Popularity',
		CASE 
			WHEN EXISTS ( SELECT 1 FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId] )
			THEN (SELECT ISNULL(SUM(ps.[NumberInStock]), 0) FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId] AND ps.[NumberInStock] >= 0)
			ELSE p.[NumberInStock]
		END AS 'FilterProduct.NumberInStock'
	FROM
		[product].[tProduct] p
		INNER JOIN [lekmer].[tLekmerProduct] lp ON p.[ProductId] = lp.[ProductId]

		INNER JOIN [product].[tProductRegistryProduct] AS prp ON p.[ProductId] = prp.[ProductId]
		INNER JOIN [product].[tProductModuleChannel] AS pmc ON prp.[ProductRegistryId] = pmc.[ProductRegistryId]
		INNER JOIN [core].[tChannel] AS ch ON pmc.[ChannelId] = ch.[ChannelId]

		LEFT JOIN [product].[tProductTranslation] AS pt ON p.[ProductId] = pt.[ProductId] AND ch.[LanguageId] = pt.[LanguageId]
		LEFT JOIN [lekmer].[vProductPopularity] lpp on pmc.[ChannelId] = lpp.[ProductPopularity.ChannelId] AND p.[ProductId] = lpp.[ProductPopularity.ProductId]

	WHERE
		p.[IsDeleted] = 0
		AND p.[ProductStatusId] = 0
GO
PRINT N'Creating index [IX_tProductSize_ProductId] on [lekmer].[tProductSize]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductSize_ProductId] ON [lekmer].[tProductSize] ([ProductId])
GO
PRINT N'Creating index [IX_tProductSize_ProductId_NumberInStock] on [lekmer].[tProductSize]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductSize_ProductId_NumberInStock] ON [lekmer].[tProductSize] ([ProductId], [NumberInStock])
GO
PRINT N'Creating index [IX_tProduct_IsDeleted_ProductStatusId] on [product].[tProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tProduct_IsDeleted_ProductStatusId] ON [product].[tProduct] ([IsDeleted], [ProductStatusId]) INCLUDE ([CategoryId], [NumberInStock], [ProductId], [Title])
GO
PRINT N'Creating extended properties'
GO
EXEC sp_addextendedproperty N'Description', N'Index is used by [lekmer].[vFilterProduct]', 'SCHEMA', N'product', 'TABLE', N'tProduct', 'INDEX', N'IX_tProduct_IsDeleted_ProductStatusId'
GO


--ROLLBACK
--RAISERROR('Remove ROLLBACK first!', 18, 0)
COMMIT
--/////////////////////////////////////