INSERT [campaignlek].[tCampaignPriceType] ([CampaignPriceTypeId], [Title], [CommonName]) VALUES (1, N'Outlet price', N'OutletPrice')
INSERT [campaignlek].[tCampaignPriceType] ([CampaignPriceTypeId], [Title], [CommonName]) VALUES (2, N'Campaign price', N'CampaignPrice')
INSERT [campaignlek].[tCampaignPriceType] ([CampaignPriceTypeId], [Title], [CommonName]) VALUES (3, N'Lowered price', N'LoweredPrice')
