SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [productlek].[pProductIdsGetByHyIdListSecure]'
GO
CREATE PROCEDURE [productlek].[pProductIdsGetByHyIdListSecure]
	@ProductHYIds	VARCHAR(MAX),
	@Delimiter		CHAR(1) = ','
AS
BEGIN
	SET NOCOUNT ON;	
	
	SELECT
		[p].[Product.Id],
		[p].[Product.ErpId]
	FROM
		[product].[vCustomProductRecord] p
		INNER JOIN [generic].[fnConvertIDListToTableString](@ProductHYIds, @Delimiter) AS pl ON [pl].[ID] = [p].[Product.ErpId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
