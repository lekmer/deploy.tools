/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 9/18/2014 11:11:45 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tEntity]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tFunctionType]

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Delete rows from [template].[tEntityFunction]
DELETE FROM [template].[tEntityFunction] WHERE [FunctionId]=1000061
DELETE FROM [template].[tEntityFunction] WHERE [FunctionId]=1000062
-- Operation applied to 2 rows out of 2

-- Delete rows from [template].[tModelFragmentFunction]
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=295
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=296
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=297
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=298
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=299
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=300
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=301
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=302
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=303
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=304
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=305
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=306
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=307
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=308
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=309
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=310
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=311
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=312
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=313
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=314
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=315
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=316
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=317
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=318
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=319
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=320
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=321
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=322
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=323
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=354
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=355
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=356
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=357
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=358
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=359
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000459
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000460
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000461
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000462
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001593
-- Operation applied to 40 rows out of 40

-- Add row to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001770, 1001197, 1, N'GiftCard.DiscountAmount_Formatted', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF

-- Add rows to [template].[tEntityFunction]
SET IDENTITY_INSERT [template].[tEntityFunction] ON
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000299, 8, 1, N'OrderItem.Price_Formatted', N'OrderItem.Price_Formatted')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000300, 8, 1, N'OrderItem.PriceSummary_Formatted', N'OrderItem.PriceSummary_Formatted')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000301, 8, 1, N'OrderItem.Vat_Formatted', N'OrderItem.Vat_Formatted')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000302, 7, 1, N'Order.TotalCostIncludingVat_Formatted', N'Order.TotalCostIncludingVat_Formatted')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000303, 7, 1, N'Order.TotalCostExcludingVat_Formatted', N'Order.TotalCostExcludingVat_Formatted')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000304, 7, 1, N'Order.TotalVat_Formatted', N'Order.TotalVat_Formatted')
SET IDENTITY_INSERT [template].[tEntityFunction] OFF
-- Operation applied to 6 rows out of 6

-- Add rows to [template].[tModelFragmentEntity]
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (139, 6)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (140, 9)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (140, 1000001)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (140, 1000003)
-- Operation applied to 4 rows out of 4

-- Add constraints to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
COMMIT TRANSACTION
GO
