SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [export].[tCdonExportRestrictionCategory]'
GO
EXEC sp_rename N'[export].[tCdonExportRestrictionCategory].[RestrictionReason]', N'Reason', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[tCdonExportRestrictionBrand]'
GO
EXEC sp_rename N'[export].[tCdonExportRestrictionBrand].[RestrictionReason]', N'Reason', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tCdonExportIncludeCategory]'
GO
CREATE TABLE [export].[tCdonExportIncludeCategory]
(
[ProductRegistryId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[Reason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCdonExportIncludeCategory] on [export].[tCdonExportIncludeCategory]'
GO
ALTER TABLE [export].[tCdonExportIncludeCategory] ADD CONSTRAINT [PK_tCdonExportIncludeCategory] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tCdonExportIncludeBrand]'
GO
CREATE TABLE [export].[tCdonExportIncludeBrand]
(
[ProductRegistryId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[Reason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCdonExportIncludeBrand] on [export].[tCdonExportIncludeBrand]'
GO
ALTER TABLE [export].[tCdonExportIncludeBrand] ADD CONSTRAINT [PK_tCdonExportIncludeBrand] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[tCdonExportRestrictionProduct]'
GO
EXEC sp_rename N'[export].[tCdonExportRestrictionProduct].[RestrictionReason]', N'Reason', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tCdonExportIncludeProduct]'
GO
CREATE TABLE [export].[tCdonExportIncludeProduct]
(
[ProductRegistryId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Reason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCdonExportIncludeProduct] on [export].[tCdonExportIncludeProduct]'
GO
ALTER TABLE [export].[tCdonExportIncludeProduct] ADD CONSTRAINT [PK_tCdonExportIncludeProduct] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeCategoryDeleteAllByCategory]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeCategoryDeleteAllByCategory]
	@CategoryIds	VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DELETE
		ic
	FROM
		[export].[tCdonExportIncludeCategory] ic
		INNER JOIN [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) AS pl ON pl.[ID] = [ic].[CategoryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionBrandInsert]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionBrandInsert]
	@ProductChannelIds	VARCHAR(MAX),
	@BrandId			INT,
	@Reason				NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME,
	@Delimiter			CHAR(1)
AS
BEGIN
	INSERT [export].[tCdonExportRestrictionBrand] (
		[ProductRegistryId],
		[BrandId],
		[Reason],
		[UserId],
		[CreatedDate]
	)
	SELECT 
		[pmc].[ProductRegistryId],
		@BrandId,
		@Reason,
		@UserId,
		@CreatedDate
	FROM [generic].[fnConvertIDListToTable](@ProductChannelIds, @Delimiter) c
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ID]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeCategoryGetAll]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeCategoryGetAll]
AS
BEGIN
	SELECT 
		DISTINCT [src].[CategoryId] 'ItemId',
		[ic].[ProductRegistryId] 'ProductRegistryId',
		[ic].[Reason] 'Reason',
		[ic].[UserId] 'UserId',
		[ic].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportIncludeCategory] ic
		 CROSS APPLY [product].[fnGetSubCategories] ([ic].[CategoryId]) src
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ic].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeBrandDeleteAllByBrand]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeBrandDeleteAllByBrand]
	@BrandIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	DELETE
		ib
	FROM
		[export].[tCdonExportIncludeBrand] ib
		INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) AS pl ON pl.[ID] = [ib].[BrandId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeBrandInsert]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeBrandInsert]
	@ProductChannelIds	VARCHAR(MAX),
	@BrandId			INT,
	@Reason				NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME,
	@Delimiter			CHAR(1)
AS
BEGIN
	INSERT [export].[tCdonExportIncludeBrand] (
		[ProductRegistryId],
		[BrandId],
		[Reason],
		[UserId],
		[CreatedDate]
	)
	SELECT 
		[pmc].[ProductRegistryId],
		@BrandId,
		@Reason,
		@UserId,
		@CreatedDate
	FROM [generic].[fnConvertIDListToTable](@ProductChannelIds, @Delimiter) c
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ID]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeProductInsert]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeProductInsert]
	@ProductChannelIds	VARCHAR(MAX),
	@ProductId			INT,
	@Reason				NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME,
	@Delimiter			CHAR(1)
AS
BEGIN
	INSERT [export].[tCdonExportIncludeProduct] (
		[ProductRegistryId],
		[ProductId],
		[Reason],
		[UserId],
		[CreatedDate]
	)
	SELECT 
		[pmc].[ProductRegistryId],
		@ProductId,
		@Reason,
		@UserId,
		@CreatedDate
	FROM [generic].[fnConvertIDListToTable](@ProductChannelIds, @Delimiter) c
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ID]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeCategoryGetAllSecure]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeCategoryGetAllSecure]
AS
BEGIN
	SELECT
		[ic].[CategoryId] 'ItemId',
		[ic].[ProductRegistryId] 'ProductRegistryId',
		[ic].[Reason] 'Reason',
		[ic].[UserId] 'UserId',
		[ic].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportIncludeCategory] ic
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ic].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionProductDeleteAllByProduct]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionProductDeleteAllByProduct]
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	DELETE
		rp
	FROM
		[export].[tCdonExportRestrictionProduct] rp
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) AS pl ON pl.[ID] = [rp].[ProductId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeProductDeleteAllByProduct]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeProductDeleteAllByProduct]
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	DELETE
		ip
	FROM
		[export].[tCdonExportIncludeProduct] ip
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) AS pl ON pl.[ID] = [ip].[ProductId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionDeleteAll]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionDeleteAll]
AS
BEGIN
	DELETE FROM [export].[tCdonExportIncludeProduct]
	DELETE FROM [export].[tCdonExportRestrictionProduct]
	
	DELETE FROM [export].[tCdonExportIncludeCategory]
	DELETE FROM [export].[tCdonExportRestrictionCategory]
	
	DELETE FROM [export].[tCdonExportIncludeBrand]
	DELETE FROM [export].[tCdonExportRestrictionBrand]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionProductInsert]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionProductInsert]
	@ProductChannelIds	VARCHAR(MAX),
	@ProductId			INT,
	@Reason				NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME,
	@Delimiter			CHAR(1)
AS
BEGIN
	INSERT [export].[tCdonExportRestrictionProduct] (
		[ProductRegistryId],
		[ProductId],
		[Reason],
		[UserId],
		[CreatedDate]
	)
	SELECT 
		[pmc].[ProductRegistryId],
		@ProductId,
		@Reason,
		@UserId,
		@CreatedDate
	FROM [generic].[fnConvertIDListToTable](@ProductChannelIds, @Delimiter) c
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ID]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeCategoryInsert]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeCategoryInsert]
	@ProductChannelIds	VARCHAR(MAX),
	@CategoryId			INT,
	@Reason				NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME,
	@Delimiter			CHAR(1)
AS
BEGIN
	INSERT [export].[tCdonExportIncludeCategory] (
		[ProductRegistryId],
		[CategoryId],
		[Reason],
		[UserId],
		[CreatedDate]
	)
	SELECT 
		[pmc].[ProductRegistryId],
		@CategoryId,
		@Reason,
		@UserId,
		@CreatedDate
	FROM [generic].[fnConvertIDListToTable](@ProductChannelIds, @Delimiter) c
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ID]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionCategoryInsert]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionCategoryInsert]
	@ProductChannelIds	VARCHAR(MAX),
	@CategoryId			INT,
	@Reason				NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME,
	@Delimiter			CHAR(1)
AS
BEGIN
	INSERT [export].[tCdonExportRestrictionCategory] (
		[ProductRegistryId],
		[CategoryId],
		[Reason],
		[UserId],
		[CreatedDate]
	)
	SELECT 
		[pmc].[ProductRegistryId],
		@CategoryId,
		@Reason,
		@UserId,
		@CreatedDate
	FROM [generic].[fnConvertIDListToTable](@ProductChannelIds, @Delimiter) c
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ID]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[pCdonExportRestrictionBrandGetAll]'
GO
ALTER PROCEDURE [export].[pCdonExportRestrictionBrandGetAll]
AS
BEGIN
	SELECT 
		[rb].CreatedDate 'CreatedDate',
		[rb].BrandId 'ItemId',
		[rb].ProductRegistryId 'ProductRegistryId',
		[rb].Reason 'Reason',
		[rb].UserId 'UserId',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportRestrictionBrand] rb
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rb].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[pCdonExportRestrictionCategoryGetAll]'
GO
ALTER PROCEDURE [export].[pCdonExportRestrictionCategoryGetAll]
AS
BEGIN
	SELECT 
		DISTINCT [src].[CategoryId] 'ItemId',
		[rc].[ProductRegistryId] 'ProductRegistryId',
		[rc].[Reason] 'Reason',
		[rc].[UserId] 'UserId',
		[rc].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportRestrictionCategory] rc
		 CROSS APPLY [product].[fnGetSubCategories] ([rc].[CategoryId]) src
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeProductGetAll]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeProductGetAll]
AS
BEGIN
	SELECT
		[ip].CreatedDate 'CreatedDate',
		[ip].ProductId 'ItemId',
		[ip].ProductRegistryId 'ProductRegistryId',
		[ip].Reason 'Reason',
		[ip].UserId 'UserId',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportIncludeProduct] ip
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ip].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionCategoryDeleteAllByCategory]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionCategoryDeleteAllByCategory]
	@CategoryIds	VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DELETE
		rc
	FROM
		[export].[tCdonExportRestrictionCategory] rc
		INNER JOIN [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) AS pl ON pl.[ID] = [rc].[CategoryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[pCdonExportRestrictionProductGetAll]'
GO
ALTER PROCEDURE [export].[pCdonExportRestrictionProductGetAll]
AS
BEGIN
	SELECT 
		[rp].CreatedDate 'CreatedDate',
		[rp].ProductId 'ItemId',
		[rp].ProductRegistryId 'ProductRegistryId',
		[rp].Reason 'Reason',
		[rp].UserId 'UserId',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportRestrictionProduct] rp
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rp].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportIncludeBrandGetAll]'
GO
CREATE PROCEDURE [export].[pCdonExportIncludeBrandGetAll]
AS
BEGIN
	SELECT
		[ib].CreatedDate 'CreatedDate',
		[ib].BrandId 'ItemId',
		[ib].ProductRegistryId 'ProductRegistryId',
		[ib].Reason 'Reason',
		[ib].UserId 'UserId',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportIncludeBrand] ib
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ib].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionBrandDeleteAllByBrand]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionBrandDeleteAllByBrand]
	@BrandIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	DELETE
		rb
	FROM
		[export].[tCdonExportRestrictionBrand] rb
		INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) AS pl ON pl.[ID] = [rb].[BrandId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionCategoryGetAllSecure]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionCategoryGetAllSecure]
AS
BEGIN
	SELECT 
		[rc].[CategoryId] 'ItemId',
		[rc].[ProductRegistryId] 'ProductRegistryId',
		[rc].[Reason] 'Reason',
		[rc].[UserId] 'UserId',
		[rc].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportRestrictionCategory] rc
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [export].[tCdonExportIncludeCategory]'
GO
ALTER TABLE [export].[tCdonExportIncludeCategory] ADD CONSTRAINT [FK_tCdonExportIncludeCategory_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
ALTER TABLE [export].[tCdonExportIncludeCategory] ADD CONSTRAINT [FK_tCdonExportIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [export].[tCdonExportIncludeBrand]'
GO
ALTER TABLE [export].[tCdonExportIncludeBrand] ADD CONSTRAINT [FK_tCdonExportIncludeBrand_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
ALTER TABLE [export].[tCdonExportIncludeBrand] ADD CONSTRAINT [FK_tCdonExportIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [export].[tCdonExportIncludeProduct]'
GO
ALTER TABLE [export].[tCdonExportIncludeProduct] ADD CONSTRAINT [FK_tCdonExportIncludeProduct_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
ALTER TABLE [export].[tCdonExportIncludeProduct] ADD CONSTRAINT [FK_tCdonExportIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
