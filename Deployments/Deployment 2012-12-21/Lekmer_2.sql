/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 12/21/2012 9:21:25 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [security].[tRolePrivilege]
ALTER TABLE [security].[tRolePrivilege] DROP CONSTRAINT [FK_tRolePrivilege_tPrivilege]
ALTER TABLE [security].[tRolePrivilege] DROP CONSTRAINT [FK_tRolePrivilege_tRole]

-- Add row to [security].[tPrivilege]
SET IDENTITY_INSERT [security].[tPrivilege] ON
INSERT INTO [security].[tPrivilege] ([PrivilegeId], [CommonName], [Title]) VALUES (1000004, N'Assortment.Cdon', N'Assortment.Cdon')
SET IDENTITY_INSERT [security].[tPrivilege] OFF

-- Add rows to [security].[tRolePrivilege]
INSERT INTO [security].[tRolePrivilege] ([RoleId], [PrivilegeId]) VALUES (1, 1000004)
INSERT INTO [security].[tRolePrivilege] ([RoleId], [PrivilegeId]) VALUES (217, 1000004)
-- Operation applied to 2 rows out of 2

-- Add constraints to [security].[tRolePrivilege]
ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [FK_tRolePrivilege_tPrivilege] FOREIGN KEY ([PrivilegeId]) REFERENCES [security].[tPrivilege] ([PrivilegeId])
ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [FK_tRolePrivilege_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId])
COMMIT TRANSACTION
GO
