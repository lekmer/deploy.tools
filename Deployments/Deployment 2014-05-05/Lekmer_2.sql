/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 4/29/2014 10:41:56 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]

-- Drop constraint FK_tModelSetting_tModel from [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModel]

-- Drop constraints from [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tEntity]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tFunctionType]

-- Delete rows from [template].[tModelFragmentEntity]
DELETE FROM [template].[tModelFragmentEntity] WHERE [ModelFragmentId]=1001396 AND [EntityId]=1
DELETE FROM [template].[tModelFragmentEntity] WHERE [ModelFragmentId]=1001396 AND [EntityId]=5
-- Operation applied to 2 rows out of 2

-- Update rows in [template].[tEntityFunction]
UPDATE [template].[tEntityFunction] SET [Description]=N'Absolute url' WHERE [FunctionId]=13
UPDATE [template].[tEntityFunction] SET [Description]=N'The absolute url to the product''s page. Will point to a custom product page specified with the sitestructure commonname.  Example: [Product.Url("produkt-ajax")]' WHERE [FunctionId]=1000073
UPDATE [template].[tEntityFunction] SET [Description]=N'Absolute url' WHERE [FunctionId]=1000113
UPDATE [template].[tEntityFunction] SET [CommonName]=N'Product.Url.RootRelative' WHERE [FunctionId]=1000114
-- Operation applied to 4 rows out of 4

-- Update row in [sitestructure].[tBlockType]
UPDATE [sitestructure].[tBlockType] SET [Title]=N'Review my wishlist' WHERE [BlockTypeId]=1000021

-- Add row to [sitestructure].[tBlockType]
INSERT INTO [sitestructure].[tBlockType] ([BlockTypeId], [Title], [CommonName], [Ordinal], [AvailableForAllPageTypes]) VALUES (1000048, N'Shared wishlist', N'SharedWishList', 0, 1)

-- Add rows to [template].[tEntityFunction]
SET IDENTITY_INSERT [template].[tEntityFunction] ON
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000295, 1, 1, N'Product.Url.RootRelative', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000296, 5, 1, N'Product.Url("SITESTRUCTURE.COMMONNAME")', N'The absolute url to the product''s page. Will point to a custom product page specified with the sitestructure commonname.  Example: [Product.Url("produkt-ajax")]')
SET IDENTITY_INSERT [template].[tEntityFunction] OFF
-- Operation applied to 2 rows out of 2

---- Add row to [template].[tModel]
--SET IDENTITY_INSERT [template].[tModel] ON
--INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName], [DefaultTemplateId]) VALUES (1000070, 21, N'Block shared wish list', N'BlockSharedWishList', NULL)
--SET IDENTITY_INSERT [template].[tModel] OFF

---- Add rows to [template].[tModelFragmentRegion]
--SET IDENTITY_INSERT [template].[tModelFragmentRegion] ON
--INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000301, 1000070, N'Head', 10)
--INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000302, 1000070, N'Content', 20)
--INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000303, 1000070, N'Product list', 30)
--INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000304, 1000070, N'Product', 40)
--INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000305, 1000070, N'Footer', 50)
--SET IDENTITY_INSERT [template].[tModelFragmentRegion] OFF
---- Operation applied to 5 rows out of 5

---- Add rows to [template].[tModelFragment]
--SET IDENTITY_INSERT [template].[tModelFragment] ON
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001393, 1000095, N'Package item', N'PackageItem', 1000032, 30, 10)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001394, 1000095, N'Product size option', N'ProductSizeOption', 1000032, 20, 5)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001395, 1000095, N'Package item size option', N'PackageItemSizeOption', 1000032, 50, 10)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001402, 1000301, N'Head', N'Head', 1000070, 0, 10)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001403, 1000302, N'Content', N'Content', 1000070, 0, 20)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001404, 1000303, N'Product list', N'ProductList', 1000070, 0, 5)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001405, 1000303, N'Product row', N'ProductRow', 1000070, 10, 5)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001406, 1000303, N'Empty space', N'EmptySpace', 1000070, 20, 5)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001407, 1000304, N'Product', N'Product', 1000070, 10, 10)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001408, 1000304, N'Product size option', N'ProductSizeOption', 1000070, 20, 5)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001409, 1000304, N'Package item', N'PackageItem', 1000070, 30, 10)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001410, 1000304, N'Package item size option', N'PackageItemSizeOption', 1000070, 50, 10)
--INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001411, 1000305, N'Footer', N'Footer', 1000070, 0, 10)
--SET IDENTITY_INSERT [template].[tModelFragment] OFF
-- Operation applied to 13 rows out of 13

-- Add rows to [template].[tModelFragmentEntity]
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001402, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001402, 1000010)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001403, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001403, 1000010)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001407, 1)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001407, 5)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001407, 1000003)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001408, 1000003)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001409, 1)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001409, 5)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001409, 1000003)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001410, 1000003)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001411, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001411, 1000010)
-- Operation applied to 14 rows out of 14

-- Add rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001316, 1001065, 2, N'WishList.HasTitle', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001317, 1001065, 1, N'WishList.Title', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001318, 1001069, 1, N'WishListItem.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001319, 1001069, 1, N'WishListItem.Ordinal', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001320, 1001075, 1, N'WishList.PriceIncludingVatSummary', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001321, 1001075, 1, N'WishList.PriceExcludingVatSummary', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001322, 1001075, 1, N'WishList.TotalProductCount', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001323, 1001075, 1, N'WishListKey', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001324, 1001075, 2, N'WishList.HasTitle', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001325, 1001075, 1, N'WishList.Title', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001326, 1001075, 1, N'WishList.AddAllToCartUrl', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001327, 1001075, 2, N'WishListIsEmpty', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001471, 1001069, 2, N'WishListItem.IsPackageItemsHashCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001472, 1001069, 1, N'WishListItem.PackageItemsHashCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001473, 47, 1, N'WishList.Form.PostUrl', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001474, 47, 1, N'WishList.Form.PostMode.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001475, 47, 1, N'WishList.Form.PostMode.Value', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001476, 47, 1, N'WishList.Form.ProductId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001477, 47, 1, N'WishList.Form.SizeId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001478, 47, 1, N'WishList.Form.PackageProductIds.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001479, 47, 1, N'WishList.Form.PackageProductSizeIds.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001480, 47, 1, N'WishList.Form.Referrer.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001481, 1001069, 1, N'Iterate:PackageItem', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001483, 1001393, 2, N'PackageItem.HasSize', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001488, 1001069, 1, N'ProductUpdateSizeForm.PostMode.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001489, 1001069, 1, N'ProductUpdateSizeForm.PostMode.Value', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001490, 1001069, 1, N'ProductUpdateSizeForm.PostUrl', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001491, 1001069, 1, N'ProductUpdateSizeForm.WishListItemId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001492, 1001069, 1, N'ProductUpdateSizeForm.ProductId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001493, 1001069, 1, N'ProductUpdateSizeForm.ProductSizeId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001494, 1001069, 1, N'PackageUpdateSizeForm.PostMode.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001495, 1001069, 1, N'PackageUpdateSizeForm.PostMode.Value', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001496, 1001069, 1, N'PackageUpdateSizeForm.PostUrl', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001497, 1001069, 1, N'PackageUpdateSizeForm.WishListItemId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001498, 1001069, 1, N'PackageUpdateSizeForm.ProductIds.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001499, 1001069, 1, N'PackageUpdateSizeForm.ProductSizeIds.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001500, 1001069, 1, N'Iterate:ProductSizeOption', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001502, 1001394, 2, N'selected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001503, 1001395, 2, N'selected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001523, 1001393, 1, N'Iterate:PackageItemSizeOption', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001543, 1001403, 1, N'ProductList', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001544, 1001403, 1, N'Paging', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001545, 1001403, 1, N'WishList.PriceIncludingVatSummary', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001546, 1001403, 1, N'WishList.PriceExcludingVatSummary', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001547, 1001403, 1, N'WishList.TotalProductCount', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001548, 1001403, 1, N'WishListKey', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001549, 1001403, 2, N'WishListIsEmpty', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001550, 1001403, 2, N'WishList.HasTitle', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001551, 1001403, 1, N'WishList.Title', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001552, 1001404, 1, N'Iterate:ProductRow', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001553, 1001405, 1, N'Iterate:Product', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001554, 1001405, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001555, 1001405, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001556, 1001406, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001557, 1001406, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001558, 1001407, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001559, 1001407, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001560, 1001407, 1, N'Product.Description', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001561, 1001407, 2, N'Product.HasSize', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001562, 1001407, 2, N'EditMode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001563, 1001407, 1, N'WishListItem.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001564, 1001407, 1, N'WishListItem.Ordinal', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001565, 1001407, 2, N'WishListItem.IsPackageItemsHashCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001566, 1001407, 1, N'WishListItem.PackageItemsHashCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001567, 1001407, 1, N'Iterate:PackageItem', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001568, 1001407, 1, N'ProductUpdateSizeForm.PostMode.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001569, 1001407, 1, N'ProductUpdateSizeForm.PostMode.Value', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001570, 1001407, 1, N'ProductUpdateSizeForm.PostUrl', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001571, 1001407, 1, N'ProductUpdateSizeForm.WishListItemId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001572, 1001407, 1, N'ProductUpdateSizeForm.ProductId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001573, 1001407, 1, N'ProductUpdateSizeForm.ProductSizeId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001574, 1001407, 1, N'PackageUpdateSizeForm.PostMode.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001575, 1001407, 1, N'PackageUpdateSizeForm.PostMode.Value', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001576, 1001407, 1, N'PackageUpdateSizeForm.PostUrl', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001577, 1001407, 1, N'PackageUpdateSizeForm.WishListItemId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001578, 1001407, 1, N'PackageUpdateSizeForm.ProductIds.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001579, 1001407, 1, N'PackageUpdateSizeForm.ProductSizeIds.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001580, 1001407, 1, N'Iterate:ProductSizeOption', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001581, 1001408, 2, N'selected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001582, 1001409, 2, N'PackageItem.HasSize', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001583, 1001409, 1, N'Iterate:PackageItemSizeOption', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001584, 1001410, 2, N'selected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001585, 1001069, 1, N'Mode.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001586, 1001069, 1, N'DeleteProduct.ModeValue', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001587, 1001069, 1, N'MoveProduct.ModeValue', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
-- Operation applied to 85 rows out of 85

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])

-- Add constraint FK_tModelSetting_tModel to [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] WITH NOCHECK ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
COMMIT TRANSACTION
GO
