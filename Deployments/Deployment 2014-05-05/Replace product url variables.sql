SELECT * FROM [template].[tTemplateFragment] WHERE CHARINDEX('[Product.FullUrl]', [Content]) > 0
SELECT * FROM [template].[tInclude] WHERE CHARINDEX('[Product.FullUrl]', [Content]) > 0

BEGIN TRANSACTION

UPDATE [template].[tTemplateFragment]
SET [Content] = REPLACE([Content], '[Product.FullUrl]', '[Product.Url]')
WHERE CHARINDEX('[Product.FullUrl]', [Content]) > 0

UPDATE [template].[tInclude]
SET [Content] = REPLACE([Content], '[Product.FullUrl]', '[Product.Url]')
WHERE CHARINDEX('[Product.FullUrl]', [Content]) > 0

ROLLBACK