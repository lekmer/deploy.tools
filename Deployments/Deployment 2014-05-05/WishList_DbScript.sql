DECLARE @WishListId	INT,
		@WishList	VARCHAR(500),
		@Product	VARCHAR(50),
		@ProductId	INT,
		@Iterator	INT

DECLARE WishListCursor CURSOR FOR 
SELECT WishListId, WishList
FROM lekmer.tWishList
WHERE WishList <> ''

OPEN WishListCursor   
FETCH NEXT FROM WishListCursor INTO @WishListId, @WishList

WHILE @@FETCH_STATUS = 0   
BEGIN
	SET @Iterator = 0
	
	WHILE LEN(@WishList) > 0
	BEGIN
		IF PATINDEX('%,%',@WishList) > 0
		BEGIN
			SET @Product = SUBSTRING(@WishList, 0, PATINDEX('%,%', @WishList))
			SET @ProductId = CAST(@Product as INT)

			IF EXISTS (SELECT ProductId FROM product.tProduct WHERE ProductId = @ProductId)
			BEGIN
				INSERT INTO lekmer.tWishListItem
				(
					WishListId,
					ProductId,
					SizeId, 
					Ordinal
				)
				VALUES
				(
					@WishListId,
					@ProductId,
					NULL,
					@Iterator
				)
			END

			SET @Iterator = @Iterator + 1
			SET @WishList = SUBSTRING(@WishList, LEN(@Product + ',') + 1, LEN(@WishList))
		END
		ELSE
		BEGIN
			SET @Product = @WishList
			SET @ProductId = CAST(@Product as INT)

			IF EXISTS (SELECT ProductId FROM product.tProduct WHERE ProductId = @ProductId)
			BEGIN
				INSERT INTO lekmer.tWishListItem
				(
					WishListId,
					ProductId,
					SizeId, 
					Ordinal
				)
				VALUES
				(
					@WishListId,
					CAST(@Product as INT),
					NULL,
					@Iterator
				)
			END

			SET @WishList = NULL
		END
	END

	FETCH NEXT FROM WishListCursor INTO @WishListId, @WishList
END   

CLOSE WishListCursor   
DEALLOCATE WishListCursor



--after import is finished successfully execute script below:

--ALTER TABLE lekmer.tWishList
--DROP COLUMN WishList
