SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [lekmer].[tBlockSharedWishList]'
GO
CREATE TABLE [lekmer].[tBlockSharedWishList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockSharedWishList] on [lekmer].[tBlockSharedWishList]'
GO
ALTER TABLE [lekmer].[tBlockSharedWishList] ADD CONSTRAINT [PK_tBlockSharedWishList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tWishListPackageItem]'
GO
CREATE TABLE [lekmer].[tWishListPackageItem]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[WishListItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tWishListPackageItem] on [lekmer].[tWishListPackageItem]'
GO
ALTER TABLE [lekmer].[tWishListPackageItem] ADD CONSTRAINT [PK_tWishListPackageItem] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tWishListItem]'
GO
CREATE TABLE [lekmer].[tWishListItem]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[WishListId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL,
[Ordinal] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tWishListItem] on [lekmer].[tWishListItem]'
GO
ALTER TABLE [lekmer].[tWishListItem] ADD CONSTRAINT [PK_tWishListItem] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tWishList]'
GO
ALTER TABLE [lekmer].[tWishList] ADD
[WishListTitle] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[CreatedDate] [datetime] NULL,
[UpdatedDate] [datetime] NULL,
[IpAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[NumberOfRemovedItems] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [lekmer].[tWishList] ALTER COLUMN [WishList] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockSharedWishListGetByIdSecure]'
GO
CREATE PROCEDURE [lekmer].[pBlockSharedWishListGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		BRMWL.*,
		b.*
	FROM
		[lekmer].[tBlockSharedWishList] AS BRMWL
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON BRMWL.[BlockId] = b.[Block.BlockId]
	WHERE
		BRMWL.[BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pWishListPackageItemDelete]'
GO
CREATE PROCEDURE [lekmer].[pWishListPackageItemDelete]
	@WishListItemId	INT
AS
BEGIN
	DELETE FROM
		[lekmer].[tWishListPackageItem]
	WHERE
		[WishListItemId] = @WishListItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pWishListGetbyKey]'
GO
ALTER procedure  [lekmer].[pWishListGetbyKey]
	@Key uniqueidentifier
as
begin

	select *
	from
		[lekmer].[tWishList] 
	where
		WishListKey = @Key		
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pWishListPackageItemUpdate]'
GO
CREATE PROCEDURE [lekmer].[pWishListPackageItemUpdate]
	@Id		INT,
	@SizeId	INT
AS
BEGIN
	UPDATE [lekmer].[tWishListPackageItem]
	SET [SizeId] = @SizeId
	WHERE [Id] = @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pWishListItemSave]'
GO

CREATE procedure [lekmer].[pWishListItemSave]
	@Id				INT,
	@WishListId		INT,
	@ProductId		INT,
	@SizeId			INT,
	@Ordinal		INT
AS
BEGIN
	UPDATE [lekmer].[tWishListItem] 
	SET
		[Ordinal] = @Ordinal,
		[SizeId] = @SizeId
	WHERE
		[Id] = @Id
		
	IF @@ROWCOUNT = 0
	BEGIN	
		INSERT INTO [lekmer].[tWishListItem] (
			WishListId,
			ProductId,
			SizeId,
			Ordinal
		)
		VALUES (
			@WishListId,
			@ProductId,
			@SizeId,
			@Ordinal
		)
		SET @Id = SCOPE_IDENTITY()
	END

	RETURN @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockSharedWishListSave]'
GO
CREATE PROCEDURE [lekmer].[pBlockSharedWishListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE [lekmer].[tBlockSharedWishList]
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount
	WHERE
		[BlockId] = @BlockId

	IF @@ROWCOUNT <> 0
		RETURN

	INSERT [lekmer].[tBlockSharedWishList]
	(
		[BlockId],
		[ColumnCount],
		[RowCount]
	)
	VALUES
	(
		@BlockId,
		@ColumnCount,
		@RowCount
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pWishListSave]'
GO
ALTER procedure [lekmer].[pWishListSave]
	@Id						int,
	@Key					uniqueidentifier,
	@Title					varchar(500),
	@CreatedDate			datetime,
	@UpdatedDate			datetime,
	@IpAddress				varchar(50),
	@NumberOfRemovedItems	int
as
begin
	update [lekmer].[tWishList] 
	set
		WishListTitle			= @Title,
		CreatedDate				= @CreatedDate,
		UpdatedDate				= @UpdatedDate,
		IpAddress				= @IpAddress,
		NumberOfRemovedItems	= @NumberOfRemovedItems
	where
		WishListId				= @Id
		
	if @@rowcount = 0
	begin	
		insert into [lekmer].[tWishList] 
		(
			WishListKey,
			WishListTitle,
			CreatedDate,
			UpdatedDate,
			IpAddress,
			NumberOfRemovedItems
		)
		values
		(
			@Key,
			@Title,
			@CreatedDate,
			@UpdatedDate,
			@IpAddress,
			@NumberOfRemovedItems
		)
		set @Id = scope_identity()		
	end

	return @Id
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pWishListDelete]'
GO
CREATE PROCEDURE [lekmer].[pWishListDelete]
	@Id	INT
AS
BEGIN
	DELETE FROM [lekmer].[tWishList] WHERE [WishListId] = @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pWishListItemDelete]'
GO
CREATE PROCEDURE [lekmer].[pWishListItemDelete]
	@Id	INT
AS
BEGIN
	EXEC [lekmer].[pWishListPackageItemDelete] @id	
	DELETE FROM [lekmer].[tWishListItem] WHERE [Id] = @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockSharedWishListDelete]'
GO
CREATE PROCEDURE [lekmer].[pBlockSharedWishListDelete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[lekmer].[tBlockSharedWishList]
	WHERE
		[BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockSharedWishListGetById]'
GO
CREATE PROCEDURE [lekmer].[pBlockSharedWishListGetById]
	@BlockId INT,
	@LanguageId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		BRMWL.*,
		b.*
	FROM
		[lekmer].[tBlockSharedWishList] AS BRMWL
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON BRMWL.[BlockId] = b.[Block.BlockId]
	WHERE
		BRMWL.[BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pWishListItemGetAllByWishList]'
GO

CREATE procedure [lekmer].[pWishListItemGetAllByWishList]
	@WishListId int
as
begin

	select *
	from
		[lekmer].[tWishListItem] 
	where
		WishListId = @WishListId		
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pWishListPackageItemGetAllByWishListItem]'
GO
CREATE PROCEDURE [lekmer].[pWishListPackageItemGetAllByWishListItem]
	@WishListItemId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[tWishListPackageItem]
	WHERE
		[WishListItemId] = @WishListItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pWishListPackageItemSave]'
GO
CREATE PROCEDURE [lekmer].[pWishListPackageItemSave]
	@WishListItemId	INT,
	@ProductId		INT,
	@SizeId			INT = NULL
AS
BEGIN
	INSERT INTO [lekmer].[tWishListPackageItem] (
		[WishListItemId],
		[ProductId],
		[SizeId]
	)
	VALUES (
		@WishListItemId,
		@ProductId,
		@SizeId
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockSharedWishList]'
GO
ALTER TABLE [lekmer].[tBlockSharedWishList] ADD CONSTRAINT [FK_tBlockSharedWishList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tWishListPackageItem]'
GO
ALTER TABLE [lekmer].[tWishListPackageItem] ADD CONSTRAINT [FK_tWishListPackageItem_tWishListItem] FOREIGN KEY ([WishListItemId]) REFERENCES [lekmer].[tWishListItem] ([Id])
ALTER TABLE [lekmer].[tWishListPackageItem] ADD CONSTRAINT [FK_tWishListPackageItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
ALTER TABLE [lekmer].[tWishListPackageItem] ADD CONSTRAINT [FK_tWishListPackageItem_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tWishListItem]'
GO
ALTER TABLE [lekmer].[tWishListItem] ADD CONSTRAINT [FK_tWishListItem_tWishList] FOREIGN KEY ([WishListId]) REFERENCES [lekmer].[tWishList] ([WishListId])
ALTER TABLE [lekmer].[tWishListItem] ADD CONSTRAINT [FK_tWishListItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
ALTER TABLE [lekmer].[tWishListItem] ADD CONSTRAINT [FK_tWishListItem_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
