/*
Run this script on:

(local).Lekmer_212_0    -  This database will be modified

to synchronize it with:

(local).Lekmer_212_1

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 11/13/2012 6:30:50 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Add row to [customer].[tAddressType]
SET IDENTITY_INSERT [customer].[tAddressType] ON
INSERT INTO [customer].[tAddressType] ([AddressTypeId], [Title], [CommonName]) VALUES (1000001, N'Alternate address', N'AlternateAddress')
SET IDENTITY_INSERT [customer].[tAddressType] OFF
COMMIT TRANSACTION
GO
