SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [campaign].[tCampaign]'
GO
ALTER TABLE [campaign].[tCampaign] DROP CONSTRAINT [FK_tCampaign_tCampaignRegistry]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tSizeTableMedia]'
GO
ALTER TABLE [lekmer].[tSizeTableMedia] DROP CONSTRAINT [FK_tSizeTableMedia_tSizeTable]
ALTER TABLE [lekmer].[tSizeTableMedia] DROP CONSTRAINT [FK_tSizeTableMedia_tMedia]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tSizeTableMedia]'
GO
ALTER TABLE [lekmer].[tSizeTableMedia] DROP CONSTRAINT [PK_tSizeTableMedia]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tCampaign_CampaignRegistryId] from [campaign].[tCampaign]'
GO
DROP INDEX [IX_tCampaign_CampaignRegistryId] ON [campaign].[tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tLekmerChannel] from [lekmer].[tLekmerChannel]'
GO
DROP INDEX [IX_tLekmerChannel] ON [lekmer].[tLekmerChannel]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [integration].[usp_FokRestrictionsProducts]'
GO
DROP PROCEDURE [integration].[usp_FokRestrictionsProducts]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pCartCampaignGetAll]'
GO
DROP PROCEDURE [campaign].[pCartCampaignGetAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[tCampaign]'
GO
ALTER TABLE [campaign].[tCampaign] DROP
COLUMN [CampaignRegistryId],
COLUMN [WebTitle],
COLUMN [IconMediaId],
COLUMN [ImageMediaId],
COLUMN [Description],
COLUMN [LinkContentNodeId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignLandingPage]'
GO
CREATE TABLE [campaignlek].[tCampaignLandingPage]
(
[CampaignLandingPageId] [int] NOT NULL IDENTITY(1, 1),
[CampaignId] [int] NOT NULL,
[WebTitle] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignLandingPage] on [campaignlek].[tCampaignLandingPage]'
GO
ALTER TABLE [campaignlek].[tCampaignLandingPage] ADD CONSTRAINT [PK_tCampaignLandingPage] PRIMARY KEY CLUSTERED  ([CampaignLandingPageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UQ_tCampaignLandingPage_CampaignId] on [campaignlek].[tCampaignLandingPage]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCampaignLandingPage_CampaignId] ON [campaignlek].[tCampaignLandingPage] ([CampaignId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignLandingPageTranslation]'
GO
CREATE TABLE [campaignlek].[tCampaignLandingPageTranslation]
(
[CampaignLandingPageId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[WebTitle] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignLandingPageTranslation] on [campaignlek].[tCampaignLandingPageTranslation]'
GO
ALTER TABLE [campaignlek].[tCampaignLandingPageTranslation] ADD CONSTRAINT [PK_tCampaignLandingPageTranslation] PRIMARY KEY CLUSTERED  ([CampaignLandingPageId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableMediaProductRegistry]'
GO
CREATE TABLE [lekmer].[tSizeTableMediaProductRegistry]
(
[SizeTableMediaProductRegistryId] [int] NOT NULL IDENTITY(1, 1),
[SizeTableMediaId] [int] NOT NULL,
[ProductRegistryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableMediaProductRegistry] on [lekmer].[tSizeTableMediaProductRegistry]'
GO
ALTER TABLE [lekmer].[tSizeTableMediaProductRegistry] ADD CONSTRAINT [PK_tSizeTableMediaProductRegistry] PRIMARY KEY CLUSTERED  ([SizeTableMediaProductRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tSizeTableMedia]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tSizeTableMedia]
(
[SizeTableMediaId] [int] NOT NULL IDENTITY(1, 1),
[SizeTableId] [int] NOT NULL,
[MediaId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tSizeTableMedia]([SizeTableId], [MediaId]) SELECT [SizeTableId], [MediaId] FROM [lekmer].[tSizeTableMedia]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tSizeTableMedia]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tSizeTableMedia]', N'tSizeTableMedia'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableMedia] on [lekmer].[tSizeTableMedia]'
GO
ALTER TABLE [lekmer].[tSizeTableMedia] ADD CONSTRAINT [PK_tSizeTableMedia] PRIMARY KEY CLUSTERED  ([SizeTableMediaId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerChannel]'
GO
ALTER TABLE [lekmer].[tLekmerChannel] ALTER COLUMN [ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tLekmerChannel_ErpId] on [lekmer].[tLekmerChannel]'
GO
CREATE NONCLUSTERED INDEX [IX_tLekmerChannel_ErpId] ON [lekmer].[tLekmerChannel] ([ErpId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignRegistryCampaignLandingPage]'
GO
CREATE TABLE [campaignlek].[tCampaignRegistryCampaignLandingPage]
(
[CampaignLandingPageId] [int] NOT NULL,
[CampaignRegistryId] [int] NOT NULL,
[IconMediaId] [int] NULL,
[ImageMediaId] [int] NULL,
[LinkContentNodeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignRegistryCampaignLandingPage] on [campaignlek].[tCampaignRegistryCampaignLandingPage]'
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaignLandingPage] ADD CONSTRAINT [PK_tCampaignRegistryCampaignLandingPage] PRIMARY KEY CLUSTERED  ([CampaignLandingPageId], [CampaignRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pMediaItemGetAllBySizeTable]'
GO
ALTER PROCEDURE [lekmer].[pMediaItemGetAllBySizeTable]
	@ChannelId INT,
	@SizeTableId INT
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		m.*
	FROM
		[media].[vCustomMedia] m
		INNER JOIN [lekmer].[tSizeTableMedia] stm ON [stm].[MediaId] = [m].[Media.Id]
		INNER JOIN [lekmer].[tSizeTableMediaProductRegistry] stmr ON stmr.[SizeTableMediaId] = stm.[SizeTableMediaId]
		INNER JOIN [product].[tProductModuleChannel] pmc ON pmc.[ProductRegistryId] = stmr.[ProductRegistryId]
	WHERE
		stm.[SizeTableId] = @SizeTableId
		AND
		pmc.[ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pReportGiftCardVoucherSent]'
GO
ALTER PROCEDURE [integration].[pReportGiftCardVoucherSent]
	@OrderId	int
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		m.OrderId,
		m.VoucherCode,	
		m.DiscountValue,
		o.Email,
		c.Title as [Status],
		g.CreatedDate as [SendDate],
		ca.Title as CampaignTitle,
		cs.Title as CampaignStatus,
		ca.StartDate as CampaignStart,
		ca.EndDate as CampaignEnd,
		cr.CommonName as Channel
	FROM 
		[campaignlek].[tGiftCardViaMailInfo] m
		inner join [order].[tOrder] o on o.OrderId = m.OrderId
		inner join [messaging].[tRecipient] r on r.[Address] = o.Email
		inner join [messaging].[tMessage] g on g.MessageId = r.MessageId
		inner join [campaignlek].[tGiftCardViaMailInfoStatus] c on c.StatusId = m.StatusId
		inner join [campaign].[tCampaign] ca on m.CampaignId = ca.CampaignId
		inner join [campaign].[tCampaignStatus] cs on ca.CampaignStatusId = cs.CampaignStatusId
		INNER JOIN [campaign].[tCampaignModuleChannel] cmc ON [cmc].[ChannelId] = [o].[ChannelId]
		inner join [campaign].[tCampaignRegistry] cr on cr.CampaignRegistryId = [cmc].[CampaignRegistryId]
	WHERE 
		m.OrderId = @OrderId
	AND m.VoucherCode IS NOT NULL
	ORDER BY
		g.CreatedDate	

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pImageGetAllBySizeTable]'
GO
ALTER PROCEDURE [lekmer].[pImageGetAllBySizeTable]
	@ChannelId INT,
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		[i].*
	FROM
		[media].[vCustomImage] i
		INNER JOIN [core].[tChannel] c ON [c].[LanguageId] = [i].[Image.LanguageId]
		INNER JOIN [lekmer].[tSizeTableMedia] stm ON [stm].[MediaId] = [i].[Media.Id]
		INNER JOIN [lekmer].[tSizeTableMediaProductRegistry] stmr ON stmr.[SizeTableMediaId] = stm.[SizeTableMediaId]
		INNER JOIN [product].[tProductModuleChannel] pmc ON pmc.[ProductRegistryId] = stmr.[ProductRegistryId]
	WHERE
		[c].[ChannelId] = @ChannelId
		AND
		[stm].[SizeTableId] = @SizeTableId
		AND
		pmc.[ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vSizeTableMedia]'
GO


CREATE VIEW [lekmer].[vSizeTableMedia]
AS
	SELECT
		[stm].[SizeTableMediaId] AS 'SizeTableMedia.SizeTableMediaId',
		[stm].[SizeTableId] AS 'SizeTableMedia.SizeTableId',
		[stm].[MediaId] AS 'SizeTableMedia.MediaId'
	FROM 
		[lekmer].[tSizeTableMedia] stm

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableMediaGetAllBySizeTable]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaGetAllBySizeTable]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[stm].*
	FROM
		[lekmer].[vSizeTableMedia] stm
	WHERE
		stm.[SizeTableMedia.SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCampaignLandingPageTranslation]'
GO

CREATE VIEW [campaignlek].[vCampaignLandingPageTranslation]
AS
SELECT 
	[CampaignLandingPageId] AS 'CampaignLandingPageTranslation.CampaignLandingPageId',
	[LanguageId]			AS 'CampaignLandingPageTranslation.LanguageId',
	[WebTitle]				AS 'CampaignLandingPageTranslation.WebTitle',
	[Description]			AS 'CampaignLandingPageTranslation.Description'
FROM 
	[campaignlek].[tCampaignLandingPageTranslation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pMediaItemGetAllBySizeTableSecure]'
GO
CREATE PROCEDURE [lekmer].[pMediaItemGetAllBySizeTableSecure]
	@SizeTableId INT
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		m.*
	FROM
		[lekmer].[tSizeTableMedia] stm
		INNER JOIN [media].[vCustomMedia] m ON [m].[Media.Id] = [stm].[MediaId]
	WHERE
		stm.[SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTableMedia]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTableMedia]
	@SizeTableMediaId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[lekmer].[tSizeTableMediaProductRegistry]
	WHERE
		[SizeTableMediaId] = @SizeTableMediaId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableMediaDelete]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableMediaDelete]
	@SizeTableMediaId INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTableMedia] @SizeTableMediaId

	DELETE FROM
		[lekmer].[tSizeTableMedia]
	WHERE
		[SizeTableMediaId] = @SizeTableMediaId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pStockRangeGetAll]'
GO
CREATE PROCEDURE [productlek].[pStockRangeGetAll]
AS
BEGIN
	SELECT
		[sr].[StockRangeId] 'StockRange.StockRangeId',
		[sr].[CommonName] 'StockRange.CommonName',
		[sr].[StartValue] 'StockRange.StartValue',
		[sr].[EndValue] 'StockRange.EndValue'
	FROM
		[productlek].[tStockRange] sr
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignLandingPageTranslationDelete]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageTranslationDelete]
	@CampaignLandingPageId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [campaignlek].[tCampaignLandingPageTranslation]
	WHERE [CampaignLandingPageId] = @CampaignLandingPageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCampaignRegistryCampaignLandingPage]'
GO


CREATE VIEW [campaignlek].[vCampaignRegistryCampaignLandingPage]
AS
SELECT 
	[CampaignLandingPageId] AS 'CampaignRegistryCampaignLandingPage.CampaignLandingPageId',
	[CampaignRegistryId]	AS 'CampaignRegistryCampaignLandingPage.CampaignRegistryId',
	[IconMediaId]			AS 'CampaignRegistryCampaignLandingPage.IconMediaId',
	[ImageMediaId]			AS 'CampaignRegistryCampaignLandingPage.ImageMediaId',
	[LinkContentNodeId]		AS 'CampaignRegistryCampaignLandingPage.ContentNodeId'
FROM 
	[campaignlek].[tCampaignRegistryCampaignLandingPage]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCampaignLandingPageSecure]'
GO


CREATE VIEW [campaignlek].[vCampaignLandingPageSecure]
AS
SELECT 
	[CampaignLandingPageId] AS 'CampaignLandingPage.CampaignLandingPageId',
	[CampaignId]			AS 'CampaignLandingPage.CampaignId',
	[WebTitle]				AS 'CampaignLandingPage.WebTitle',
	[Description]			AS 'CampaignLandingPage.Description'
FROM 
	[campaignlek].[tCampaignLandingPage]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignLandingPageGetByCampaignIdSecure]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageGetByCampaignIdSecure]
	@CampaignId INT
AS 
BEGIN

	DECLARE @CampaignLandingpageId INT
	SET @CampaignLandingpageId = (SELECT [CampaignLandingPage.CampaignLandingPageId]
								  FROM [campaignlek].[vCampaignLandingPageSecure] clp
								  WHERE [clp].[CampaignLandingPage.CampaignId] = @CampaignId)
	
	SELECT
		[clp].*
	FROM
		[campaignlek].[vCampaignLandingPageSecure] clp
	WHERE
		[clp].[CampaignLandingPage.CampaignLandingPageId] = @CampaignLandingpageId
		
	SELECT
		[crclp].*
	FROM
		[campaignlek].[vCampaignRegistryCampaignLandingPage] crclp
	WHERE
		[crclp].[CampaignRegistryCampaignLandingPage.CampaignLandingPageId] = @CampaignLandingpageId
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignRegistryCampaignDelete]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignDelete]
	@CampaignId		INT
AS
BEGIN
	DELETE FROM [campaignlek].[tCampaignRegistryCampaign] WHERE [CampaignId] = @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageRestrictionsUpdate]'
GO
ALTER PROCEDURE [productlek].[pPackageRestrictionsUpdate]
	@ProductId			INT,
	@BrandId			INT,
	@CategoryId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN
	-- Get Product Ids that need add to package restirctions.
	DECLARE @ProductRegIds [lekmer].[ProductRegistryProducts]

	IF @ProductId IS NOT NULL
	BEGIN
		INSERT INTO @ProductRegIds (ProductId, ProductRegistryId)
		SELECT p.[MasterProductId], pr.ID
		FROM (SELECT DISTINCT [pak].[MasterProductId]
				FROM [productlek].[tPackage] pak
				INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
				WHERE [pp].[ProductId] = @ProductId) p,
			  [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	IF @BrandId IS NOT NULL
	BEGIN
		INSERT INTO @ProductRegIds (ProductId, ProductRegistryId)
		SELECT p.[MasterProductId], pr.ID
		FROM (SELECT DISTINCT [pak].[MasterProductId]
				FROM [productlek].[tPackage] pak
				INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [pp].[ProductId]
				WHERE [lp].[BrandId] = @BrandId) p,
			  [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	IF @CategoryId IS NOT NULL
	BEGIN
		INSERT INTO @ProductRegIds (ProductId, ProductRegistryId)
		SELECT p.[MasterProductId], pr.ID
		FROM (SELECT DISTINCT [pak].[MasterProductId]
				FROM [productlek].[tPackage] pak
				INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
				INNER JOIN [product].[tProduct] lp ON [lp].[ProductId] = [pp].[ProductId]
				WHERE [lp].[CategoryId] IN (SELECT CategoryId FROM [product].[fnGetSubCategories] (@CategoryId))) p,
			  [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	-- Remove duplicates
	DELETE
		prIds
	FROM
		@ProductRegIds prIds
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.[ProductId] = [prIds].[ProductId] AND prrp.[ProductRegistryId] = [prIds].[ProductRegistryId]

	-- Add package restrictions
	INSERT INTO [lekmer].[tProductRegistryRestrictionProduct] (
		[ProductRegistryId] ,
		[ProductId] ,
		[RestrictionReason] ,
		[UserId] ,
		[CreatedDate]
	)
	SELECT
		[ProductRegistryId],
		[ProductId],
		'Package has restricted include product/product category/product brand',
		NULL,
		GETDATE()
	FROM
		@ProductRegIds
	
	
	DELETE prp FROM product.tProductRegistryProduct prp
	INNER JOIN @ProductRegIds prIds ON prIds.ProductId = [prp].[ProductId] AND [prIds].[ProductRegistryId] = [prp].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignRegistryCampaignLandingPageSave]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignLandingPageSave]
	@CampaignLandingPageId	INT,
	@CampaignRegistryIds	VARCHAR(MAX),
	@IconIds				VARCHAR(MAX),
	@ImageIds				VARCHAR(MAX),
	@ContentNodeIds		VARCHAR(MAX),
	@Delimiter				CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Delete campaign landing page data specific for registry
	DELETE FROM [campaignlek].[tCampaignRegistryCampaignLandingPage] WHERE [CampaignLandingPageId] = @CampaignLandingPageId
	
	
	DECLARE @tCampaignRegistryIds TABLE (CampaignRegistryId INT, Ordinal INT)
	INSERT INTO @tCampaignRegistryIds ([CampaignRegistryId], [Ordinal])
	SELECT [p].[ID], [p].[Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@CampaignRegistryIds, @Delimiter) p
	
	DECLARE @tIconIds TABLE (IconId INT, Ordinal INT)
	INSERT INTO @tIconIds ([IconId], [Ordinal])
	SELECT [p].[ID], [p].[Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@IconIds, @Delimiter) p
	
	DECLARE @tImageIds TABLE (ImageId INT, Ordinal INT)
	INSERT INTO @tImageIds ([ImageId], [Ordinal])
	SELECT [p].[ID], [p].[Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ImageIds, @Delimiter) p
	
	DECLARE @tContentNodeIds TABLE (ContentNodeId INT, Ordinal INT)
	INSERT INTO @tContentNodeIds ([ContentNodeId], [Ordinal])
	SELECT [p].[ID], [p].[Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ContentNodeIds, @Delimiter) p
	
	
	INSERT INTO [campaignlek].[tCampaignRegistryCampaignLandingPage] (
		[CampaignLandingPageId],
		[CampaignRegistryId],
		[IconMediaId],
		[ImageMediaId],
		[LinkContentNodeId]
	)
	SELECT
		@CampaignLandingPageId,
		[cr].[CampaignRegistryId],
		CASE [ic].[IconId] WHEN -1 THEN NULL ELSE [ic].[IconId] END,
		CASE [im].[ImageId] WHEN -1 THEN NULL ELSE [im].[ImageId] END,
		[cn].[ContentNodeId]
	FROM @tCampaignRegistryIds cr
	INNER JOIN @tIconIds ic ON [ic].[Ordinal] = [cr].[Ordinal]
	INNER JOIN @tImageIds im ON [im].[Ordinal] = [cr].[Ordinal]
	INNER JOIN @tContentNodeIds cn ON [cn].[Ordinal] = [cr].[Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableMediaSave]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableMediaSave]
	@SizeTableMediaId INT,
	@SizeTableId INT,
	@MediaId INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[lekmer].[tSizeTableMedia]
	SET 
		[SizeTableId] = @SizeTableId,
		[MediaId] = @MediaId
	WHERE
		[SizeTableMediaId] = @SizeTableMediaId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tSizeTableMedia](
			[SizeTableId],
			[MediaId]
		)
		VALUES (
			@SizeTableId,
			@MediaId
		)

		SET @SizeTableMediaId = SCOPE_IDENTITY()
	END

	RETURN @SizeTableMediaId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[vCampaign]'
GO




ALTER VIEW [campaign].[vCampaign]
AS
	SELECT
		[c].[CampaignId]			AS 'Campaign.Id',
		[c].[FolderId]				AS 'Campaign.FolderId',
		[c].[Title]					AS 'Campaign.Title',
		[c].[CampaignStatusId]		AS 'Campaign.CampaignStatusId',
		[c].[StartDate]				AS 'Campaign.StartDate',
		[c].[EndDate]				AS 'Campaign.EndDate',
		[c].[Priority]				AS 'Campaign.Priority',
		[c].[Exclusive]				AS 'Campaign.Exclusive',
		[c].[LevelId]				AS 'Campaign.LevelId',
		[cl].[Priority]				AS 'Campaign.LevelPriority',
		[c].[UseLandingPage]		AS 'Campaign.UseLandingPage',
		s.*
	FROM
		[campaign].[tCampaign] c
		INNER JOIN [campaign].[vCustomCampaignStatus] s ON [c].[CampaignStatusId] = [s].[CampaignStatus.Id]
		INNER JOIN [campaignlek].[tCampaignLevel] cl ON [cl].[LevelId] = [c].[LevelId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCartCampaignGetIdAll]'
GO

EXECUTE sp_refreshview N'campaign.vCustomCampaign'
Go
EXECUTE sp_refreshview N'campaign.vCartCampaign'
GO
EXECUTE sp_refreshview N'campaign.vProductCampaign'
GO
exec dev.pRefreshAllViews
GO

ALTER PROCEDURE [campaign].[pCartCampaignGetIdAll]
	@ChannelId INT
AS
BEGIN
	SELECT
		[c].[Campaign.Id]
	FROM
		[campaign].[vCustomCartCampaign] c
		INNER JOIN [campaignlek].[tCampaignRegistryCampaign] crc ON [crc].[CampaignId] = [c].[Campaign.Id]
		INNER JOIN [campaign].[tCampaignModuleChannel] m ON [m].[CampaignRegistryId] = [crc].[CampaignRegistryId]
	WHERE
		[m].[ChannelId] = @ChannelId
		AND [c].[Campaign.CampaignStatusId] = 0
	ORDER BY
		[c].[Campaign.LevelPriority],
		[c].[Campaign.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableMediaProductRegistryDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistryDelete]
	@SizeTableMediaProductRegistryId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[lekmer].[tSizeTableMediaProductRegistry]
	WHERE
		[SizeTableMediaProductRegistryId] = @SizeTableMediaProductRegistryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignSave]'
GO
ALTER PROCEDURE [campaign].[pCampaignSave]
	@CampaignId			INT,
	@Title				VARCHAR(500),
	@CampaignStatusId	INT,
	@FolderId			INT,
	@StartDate			DATETIME,
	@EndDate			DATETIME,
	@Exclusive			BIT,
	@Priority			INT,
	@LevelId			INT	,
	@UseLandingPage		BIT = 0
AS
BEGIN
	UPDATE
		[campaign].[tCampaign]
	SET
		[Title]					= @Title,
		[CampaignStatusId]		= @CampaignStatusId,
		[FolderId]				= @FolderId,
		[StartDate]				= @StartDate,
		[EndDate]				= @EndDate,
		[Exclusive]				= @Exclusive,
		[Priority]				= @Priority,
		[LevelId]				= @LevelId,
		[UseLandingPage]		= @UseLandingPage
	WHERE
		[CampaignId] = @CampaignId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		SET @Priority = (SELECT MAX(PRIORITY) FROM campaign.tCampaign) + 1
		IF @Priority IS NULL
			BEGIN
				SET @Priority = 1
			END
			
		INSERT campaign.tCampaign (
			[Title],
			[CampaignStatusId],
			[FolderId],
			[StartDate],
			[EndDate],
			[Exclusive],
			[Priority],
			[LevelId],
			[UseLandingPage]
		)
		VALUES (
			@Title,
			@CampaignStatusId,
			@FolderId,
			@StartDate,
			@EndDate,
			@Exclusive,
			@Priority,
			@LevelId,
			@UseLandingPage	
		)
		
		SET @CampaignId = CAST(SCOPE_IDENTITY() AS INT)
	END
	RETURN @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pCampaignInfoToCdon]'
GO
ALTER procedure [integration].[pCampaignInfoToCdon]
as
begin
	set nocount on
	
	set transaction isolation level read uncommitted
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		

	if object_id('tempdb..#Campaign') is not null
		 drop table #Campaign

	create table #Campaign
	(
		CampaignId int,
		PriceListId int primary key (CampaignId, PriceListId) with (ignore_dup_key = on)
	)

	insert into #Campaign ( CampaignId, PriceListId )
	select c.CampaignId, [crc].[CampaignRegistryId]
	from campaign.tCampaign c
	INNER JOIN [campaignlek].[tCampaignRegistryCampaign] crc ON [crc].[CampaignId] = [c].[CampaignId]
	where c.CampaignStatusId = 0
		and (c.StartDate is null or c.StartDate <= getdate())
		and (c.EndDate is null or getdate() <= c.EndDate)



	if object_id('tempdb..#CampaignAction') is not null
		 drop table #CampaignAction

	create table #CampaignAction (
		CampaignId int,
		SortIndex int,   
		ProductActionId int,
		Ordinal int
	)

	declare @Index int
	set @Index = 1

	insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
	select DISTINCT pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
	from #Campaign c
		inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
		left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
			and pa2.Ordinal < pa.Ordinal
	where pa2.ProductActionId is null

	while (@@ROWCOUNT > 0)
	begin
		set @Index = @Index + 1
	      
		insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
		SELECT DISTINCT pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
		from #Campaign c      
			inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
				and tca.SortIndex = @Index - 1
			inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
				and pa.Ordinal > tca.Ordinal
			left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
				and pa2.Ordinal > tca.Ordinal
				and pa2.Ordinal < pa.Ordinal
		where pa2.ProductActionId is null
		option (force order)
	end

	if object_id('tempdb..#CampaignProduct') is not null
		 drop table #CampaignProduct

	create table #CampaignProduct (
		CampaignId int,
		ProductId int,
		Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
	)

	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, ip.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [ca].[ProductActionId]
		inner join [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]
		
	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, p.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
					INNER JOIN product.tProduct p ON p.CategoryId = ic.CategoryId

	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, pdai.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
		inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId

	update cp set Price = pli.PriceIncludingVat
	from #Campaign c
		inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
		inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
			and pli.ProductId = cp.ProductId

	declare @LastIndex int
	set @Index = 1
	select @LastIndex = max(SortIndex)
	from #CampaignAction

	while (@Index <= @LastIndex)
	begin   
		update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1
			inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
			CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
						inner join product.tProduct p on p.CategoryId = ic.CategoryId
						inner join #CampaignProduct cp on cp.ProductId = p.ProductId and cp.CampaignId = ca.CampaignId
		where ca.SortIndex = @Index

		update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1
			inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
			inner join [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]
			inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
				and cp.CampaignId = ca.CampaignId
		where ca.SortIndex = @Index
	   
		update cp set Price = pdai.DiscountPrice
		-- select cp.Price, pdai.DiscountPrice
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1000001
			inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
			inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
				and cp.CampaignId = ca.CampaignId
			inner join #Campaign c on c.CampaignId = cp.CampaignId
			inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId
		where ca.SortIndex = @Index
			and (cp.Price > pdai.DiscountPrice or cp.Price is null)

		set @Index = @Index + 1
	end

	delete cp
	from #CampaignProduct cp 
	where Price is null

	update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
	from #CampaignProduct cp
		inner join #Campaign c on c.CampaignId = cp.CampaignId
	   

	if object_id('tempdb..#BestCampaignPrice') is not null
		 drop table #BestCampaignPrice

	create table #BestCampaignPrice (
		ProductId int,
		PriceListId int primary key (ProductId, PriceListId) with (ignore_dup_key = on),
		CampaignId int,
		Price decimal(16, 2)
	)
	insert into #BestCampaignPrice (ProductId, PriceListId, CampaignId, Price)
	select cp.ProductId, c.PriceListId, cp.CampaignId, cp.Price
	from #CampaignProduct cp
		inner join #Campaign c on c.CampaignId = cp.CampaignId
	order by cp.ProductId, c.PriceListId, cp.Price	

	drop table #Campaign
	drop table #CampaignAction
	drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title

	
	select case when (p.ProductStatusId = 0) then 'Online' else 'Offline' end as Status,
		p.Title as Product,
--		p.ErpId,
		lp.HyErpId AS ErpId,
		ca.ErpId as CategoryErpId,
		ca.Title as Category,
		p.NumberInStock,
		pl.CommonName as PriceList, 
	   pli.PriceIncludingVat as PriceListPrice, 
	   isnull(pld.Price, pli.PriceIncludingVat) as Price,
	   pld.Price as DiscountPrice, 
	   pli.PriceIncludingVat - pld.Price as Discount,
	   convert(int, round(convert(money, pli.PriceIncludingVat - pld.Price) * 100 / pli.PriceIncludingVat, 0)) as DiscountPercentage,
	   c.Title as Campaign,
	   c.StartDate as CampaignStart,
	   c.EndDate as CampaignEnd
	--into temp.CdonPriceNewCalc3
	from @PriceList tpl
		inner join product.tPriceList pl on pl.PriceListId = tpl.PriceListId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		left outer join #BestCampaignPrice pld on pld.PriceListId = pl.PriceListId
		   and pld.ProductId = pli.ProductId
		left outer join campaign.tCampaign c on c.CampaignId = pld.CampaignId
		inner join product.tProduct p on p.ProductId = pli.ProductId
      inner join lekmer.tLekmerProduct lp on lp.ProductId = pli.ProductId
      inner join product.tCategory ca on ca.CategoryId = p.CategoryId            

	
	drop table #BestCampaignPrice
		
  -- select pi.ProductId 
		--  ,lp.HyErpId AS ErpId
		--  ,pi.MediaId
		--  ,pi.ProductImageGroupId
		--  ,mf.Extension
	 --from @AllProductImages pi
	 --     inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	 --     inner join media.tMedia m on pi.MediaId = m.MediaId
		--   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCampaignLandingPage]'
GO



CREATE VIEW [campaignlek].[vCampaignLandingPage]
AS
SELECT 
	[clp].[CampaignLandingPageId]							AS 'CampaignLandingPage.CampaignLandingPageId',
	[clp].[CampaignId]										AS 'CampaignLandingPage.CampaignId',
	COALESCE ([clpt].[WebTitle], [clp].[WebTitle])			AS 'CampaignLandingPage.WebTitle',
	COALESCE ([clpt].[Description], [clp].[Description])	AS 'CampaignLandingPage.Description',
	[l].[LanguageId]										AS 'CampaignLandingPage.LanguageId'
FROM 
	[campaignlek].[tCampaignLandingPage] clp
	CROSS JOIN [core].[tLanguage] AS l
	LEFT JOIN  [campaignlek].[tCampaignLandingPageTranslation] clpt ON [clpt].[CampaignLandingPageId] = [clp].[CampaignLandingPageId]
																		AND [clpt].[LanguageId] = [l].[LanguageId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignLandingPageDelete]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageDelete]
	@CampaignId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @CampaignLandingPageId INT
	SET @CampaignLandingPageId = (SELECT [CampaignLandingPageId] FROM [campaignlek].[tCampaignLandingPage] WHERE [CampaignId] = @CampaignId)
	
	DELETE FROM [campaignlek].[tCampaignRegistryCampaignLandingPage] 
	WHERE [CampaignLandingPageId] = @CampaignLandingPageId
	
	EXEC [campaignlek].[pCampaignLandingPageTranslationDelete] @CampaignLandingPageId
			
	DELETE FROM [campaignlek].[tCampaignLandingPage]
	WHERE CampaignId = @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageUpdate]'
GO
ALTER PROCEDURE [productlek].[pPackageUpdate]
AS
BEGIN
	/*
	1. Update package stock numbers
		package.stock = MIN (products.stock)
		product.stock = SUM (product-sizes.stock) || product.stock
		
	2. Put packages offline when they are out of stock
		When
			package is online
			package is passive
			stock is 0
		Then
			set package offline
	*/

	SET NOCOUNT ON

	DECLARE @PackageId INT
	DECLARE @MasterProductId INT
	DECLARE @NumberInStock INT

	DECLARE cur_package CURSOR FAST_FORWARD
	FOR
		SELECT
			[PackageId],
			[MasterProductId]
		FROM
			[productlek].[tPackage] p

	OPEN cur_package

	FETCH NEXT FROM cur_package
	INTO @PackageId, @MasterProductId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @NumberInStock = (
			SELECT
				MIN(COALESCE(a1.[NumberInStock], p.NumberInStock))
			FROM
				[productlek].[tPackageProduct] pp
				INNER JOIN [product].[tProduct] p ON p.[ProductId] = pp.[ProductId]
				CROSS APPLY (
					SELECT
						SUM(ps.[NumberInStock]) AS 'NumberInStock'
					FROM
						[lekmer].[tProductSize] ps
					WHERE
						[ps].[ProductId] = [p].[ProductId]
				) a1
			WHERE
				[pp].[PackageId] = @PackageId
		)

		IF @NumberInStock IS NOT NULL
		BEGIN
			UPDATE
				[product].[tProduct]
			SET
				[NumberInStock] = @NumberInStock
			WHERE
				[ProductId] = @MasterProductId
				AND
				[NumberInStock] != @NumberInStock
		END

		IF @NumberInStock = 0
		BEGIN
			UPDATE
				p
			SET
				[p].[ProductStatusId] = 1 -- offline
			FROM 
				[product].[tProduct] p
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			WHERE
				[p].[ProductId] = @MasterProductId
				AND
				[p].[ProductStatusId] = 0 -- online
				AND 
				[lp].[StockStatusId] = 1 -- passive
		END

		FETCH NEXT FROM cur_package
		INTO @PackageId, @MasterProductId
	END

	CLOSE cur_package
	DEALLOCATE cur_package
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pProductCampaignGetIdAll]'
GO
ALTER PROCEDURE [campaign].[pProductCampaignGetIdAll]
	@ChannelId INT
AS
BEGIN
	SELECT
		[c].[Campaign.Id]
	FROM
		[campaign].[vCustomProductCampaign] c
		INNER JOIN [campaignlek].[tCampaignRegistryCampaign] crc ON [crc].[CampaignId] = [c].[Campaign.Id]
		INNER JOIN [campaign].[tCampaignModuleChannel] m ON [m].[CampaignRegistryId] = [crc].[CampaignRegistryId]
	WHERE
		[m].[ChannelId] = @ChannelId
		AND [c].[Campaign.CampaignStatusId] = 0
	ORDER BY
		[c].[Campaign.LevelPriority],
		[c].[Campaign.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignRegistryCampaignGetByCampaignId]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignGetByCampaignId]
	@CampaignId INT
AS
BEGIN
	SELECT
		[crc].[CampaignRegistryId] 'CampaignRegistryCampaign.CampaignRegistryId',
		[crc].[CampaignId] 'CampaignRegistryCampaign.CampaignId',
		[c].[ChannelId] 'CampaignRegistryCampaign.ChannelId',
		[co].[ISO] 'CampaignRegistryCampaign.CountryIso'
	FROM
		[campaignlek].[tCampaignRegistryCampaign] crc
		INNER JOIN [campaign].[tCampaignModuleChannel] cmc ON [cmc].[CampaignRegistryId] = [crc].[CampaignRegistryId]
		INNER JOIN [core].[tChannel] c ON [c].[ChannelId] = [cmc].[ChannelId]
		INNER JOIN [core].[tCountry] co ON [co].[CountryId] = [c].[CountryId]
	WHERE
		[crc].[CampaignId] = @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vSizeTableMediaProductRegistry]'
GO

CREATE VIEW [lekmer].[vSizeTableMediaProductRegistry]
AS
	SELECT
		[stmpr].[SizeTableMediaProductRegistryId] AS 'SizeTableMediaProductRegistry.SizeTableMediaProductRegistryId',
		[stmpr].[SizeTableMediaId] AS 'SizeTableMediaProductRegistry.SizeTableMediaId',
		[stmpr].[ProductRegistryId] AS 'SizeTableMediaProductRegistry.ProductRegistryId'
	FROM 
		[lekmer].[tSizeTableMediaProductRegistry] stmpr
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignLandingPageSave]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageSave]
	@CampaignLandingPageId	INT,
	@CampaignId				INT,
	@WebTitle				NVARCHAR(500),
	@Description			NVARCHAR(MAX),
	@CampaignRegistryIds	VARCHAR(MAX),
	@IconIds				VARCHAR(MAX),
	@ImageIds				VARCHAR(MAX),
	@ContentNodeIds		VARCHAR(MAX),
	@Delimiter				CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[campaignlek].[tCampaignLandingPage]
	SET 
		[WebTitle] = @WebTitle,
		[Description] = @Description
	WHERE
		[CampaignLandingPageId] = @CampaignLandingPageId
		AND [CampaignId] = @CampaignId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignLandingPage] (
			[CampaignId],
			[WebTitle],
			[Description]
		)
		VALUES (
			@CampaignId,
			@WebTitle,
			@Description
		)

		SET @CampaignLandingPageId = SCOPE_IDENTITY()
	END

	EXEC [campaignlek].[pCampaignRegistryCampaignLandingPageSave] @CampaignLandingPageId, @CampaignRegistryIds, @IconIds, @ImageIds, @ContentNodeIds, @Delimiter

	RETURN @CampaignLandingPageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[usp_OutletTagProduct]'
GO
CREATE PROCEDURE [integration].[usp_OutletTagProduct]
AS
BEGIN
	-- is product
	-- product is passive
	-- product has any discount
	-- stock > 0
	
	--> add tag 'outlet'

	SET NOCOUNT ON
	
	DECLARE @PassiveId INT
	SET @PassiveId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [CommonName] = 'Passive')
	
	DECLARE @OutletTagId INT
	SET @OutletTagId = (SELECT TagId FROM lekmer.tTag WHERE [CommonName] = 'outlet')

	DECLARE @ProductId INT
	DECLARE @NumberInStock INT
	
	DECLARE cur_product CURSOR FAST_FORWARD FOR
	SELECT 
		[lp].[ProductId],
		[p].[NumberInStock]
	FROM
		[lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [lp].[ProductId]
	WHERE
		[lp].[ProductTypeId] = 1 -- Is Product
		AND [lp].[StockStatusId] = @PassiveId -- Is Passive
		AND EXISTS ( -- has any discount
			SELECT 1 
			FROM [export].[tProductPrice] pp 
			WHERE [pp].[ProductId] = [lp].[ProductId]
				  AND [DiscountPriceIncludingVat] IS NOT NULL
				  AND [DiscountPriceExcludingVat] IS NOT NULL
		)

	OPEN cur_product

	FETCH NEXT FROM cur_product 
	INTO @ProductId,
		 @NumberInStock

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- without sizes
		IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId)
			BEGIN
				IF (@NumberInStock > 0) 
					AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @OutletTagId)
				BEGIN
					INSERT INTO [lekmer].[tProductTag] ([ProductId], [TagId])
					VALUES (@ProductId, @OutletTagId)
				END
			END
		ELSE
			BEGIN
				-- with sizes				
				IF EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId AND [NumberInStock] > 0)
				   AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @OutletTagId)
				BEGIN 
					INSERT INTO [lekmer].[tProductTag] ([ProductId], [TagId])
					VALUES (@ProductId, @OutletTagId)
				END
			END
		
		FETCH NEXT FROM cur_product 
		INTO @ProductId,
			 @NumberInStock
	END

	CLOSE cur_product
	DEALLOCATE cur_product
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pAvailGetProductDiscountPriceOnSite]'
GO
ALTER PROCEDURE [lekmer].[pAvailGetProductDiscountPriceOnSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--set statistics io on
	--set statistics time on

	SET NOCOUNT ON;

	DECLARE
		@Channel_SE INT,
		@Channel_NO INT,
		@Channel_DA INT,
		@Channel_FI INT,
		@PriceListId_FI INT,
		@ProductActionTypeId_Discount INT,
		@ProductActionTypeId_Percentage INT

	SELECT @Channel_SE = ChannelId FROM core.tChannel WHERE CommonName = 'Sweden'
	SELECT @Channel_NO = ChannelId FROM core.tChannel WHERE CommonName = 'Norway'
	SELECT @Channel_DA = ChannelId FROM core.tChannel WHERE CommonName = 'Denmark'
	SELECT @Channel_FI = ChannelId FROM core.tChannel WHERE CommonName = 'Finland'

	SELECT @ProductActionTypeId_Discount = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'ProductDiscount'

	SELECT @ProductActionTypeId_Percentage = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'PercentagePriceDiscount'

	SELECT @PriceListId_FI = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'Finland'

	---- #Campaign

	IF OBJECT_ID('tempdb..#Campaign') IS NOT NULL
		DROP TABLE #Campaign

	CREATE TABLE #Campaign
	(
	   CampaignId INT,
	   PriceListId INT PRIMARY KEY (CampaignId, PriceListId) WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO #Campaign (
		CampaignId,
		PriceListId
	)
	SELECT
		c.CampaignId,
		pl.PriceListId
	FROM
		campaign.tCampaign c
		INNER JOIN [campaignlek].[tCampaignRegistryCampaign] crc ON [crc].[CampaignId] = [c].[CampaignId]
		LEFT OUTER JOIN campaign.tCampaignModuleChannel cmc ON cmc.CampaignRegistryId = crc.CampaignRegistryId
		LEFT OUTER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = cmc.ChannelId
		LEFT OUTER JOIN product.tPriceList pl ON pl.PriceListRegistryId = pmc.PriceListRegistryId
	WHERE
		c.CampaignStatusId = 0
		AND (c.StartDate IS NULL OR c.StartDate <= GETDATE())
		AND (c.EndDate IS NULL OR GETDATE() <= c.EndDate)
		AND cmc.ChannelId in (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI)

	---- #CampaignAction

	IF OBJECT_ID('tempdb..#CampaignAction') IS NOT NULL
		DROP TABLE #CampaignAction

	CREATE TABLE #CampaignAction (
	   CampaignId INT,
	   SortIndex INT,   
	   ProductActionId INT,
	   Ordinal INT
	)

	DECLARE @Index INT
	SET @Index = 1

	INSERT INTO #CampaignAction (
		CampaignId,
		SortIndex,
		ProductActionId,
		Ordinal
	)
	SELECT DISTINCT
		pa.CampaignId,
		@index,
		pa.ProductActionId,
		pa.Ordinal
	FROM
		#Campaign c
		INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId
		LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal < pa.Ordinal --WTF?
	WHERE
		pa2.ProductActionId IS NULL

	WHILE (@@ROWCOUNT > 0) 
	BEGIN
		SET @Index = @Index + 1

		INSERT INTO #CampaignAction (
			CampaignId,
			SortIndex,
			ProductActionId,
			Ordinal
		)
		SELECT DISTINCT
			pa.CampaignId,
			@index,
			pa.ProductActionId,
			pa.Ordinal
		FROM
			#Campaign c
			INNER JOIN #CampaignAction tca ON tca.CampaignId = c.CampaignId AND tca.SortIndex = @Index - 1
			INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId AND pa.Ordinal > tca.Ordinal
			LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal > tca.Ordinal AND pa2.Ordinal < pa.Ordinal
		WHERE
			pa2.ProductActionId IS NULL
		OPTION (FORCE ORDER)
	END


	---- #CampaignProduct

	IF OBJECT_ID('tempdb..#CampaignProduct') IS NOT NULL
		DROP TABLE #CampaignProduct

	CREATE TABLE #CampaignProduct (
		CampaignId INT,
		PriceListId INT,
		ProductId INT,
		Price DECIMAL(16, 2) PRIMARY KEY (CampaignId, ProductId) WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		ap.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionAffectedProducts] ([ca].[ProductActionId]) ap

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		pdai.ProductId,
		NULL
	from
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = c.PriceListId AND pl.CurrencyId = pdai.CurrencyId

	-- Set product prices

	UPDATE cp
	SET Price = pli.PriceIncludingVat
	FROM
		#Campaign c
		INNER JOIN #CampaignProduct cp ON cp.CampaignId = c.CampaignId
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = c.PriceListId AND pli.ProductId = cp.ProductId

	DECLARE @LastIndex INT
	SET @Index = 1
	SELECT @LastIndex = MAX(SortIndex) FROM #CampaignAction

	WHILE (@Index <= @LastIndex)
	BEGIN
		UPDATE cp
		SET Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Percentage
			INNER JOIN campaign.tPercentagePriceDiscountAction ppda ON ppda.ProductActionId = pa.ProductActionId
			CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionAffectedProducts] ([pa].[ProductActionId]) ap
			INNER JOIN #CampaignProduct cp ON cp.ProductId = ap.ProductId AND cp.CampaignId = ca.CampaignId
		WHERE
			ca.SortIndex = @Index

		UPDATE cp
		SET Price = pdai.DiscountPrice
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Discount
			INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
			INNER JOIN #CampaignProduct cp ON cp.ProductId = pdai.ProductId AND cp.CampaignId = ca.CampaignId
			INNER JOIN product.tPriceList pl ON pl.PriceListId = cp.PriceListId AND pl.CurrencyId = pdai.CurrencyId
		WHERE
			ca.SortIndex = @Index
			AND (cp.Price > pdai.DiscountPrice OR cp.Price IS NULL)

		SET @Index = @Index + 1
	END

	DELETE cp FROM #CampaignProduct cp WHERE Price IS NULL

	UPDATE cp
	SET price = CASE
					WHEN (c.PriceListId = @PriceListId_FI) THEN Price
					ELSE ROUND(Price, 0)
				END
	FROM
		#CampaignProduct cp
		INNER JOIN #Campaign c ON c.CampaignId = cp.CampaignId

	---- #BestCampaignPrice

	IF OBJECT_ID('tempdb..#BestCampaignPrice') IS NOT NULL
		DROP TABLE #BestCampaignPrice

	CREATE TABLE #BestCampaignPrice (
		ProductId INT,
		PriceListId INT,
		CampaignId INT,
		Price DECIMAL(16, 2)
	)

	INSERT INTO #BestCampaignPrice (
		ProductId,
		PriceListId,
		CampaignId,
		Price) -- Fel
	SELECT
		cp.ProductId,
		cp.PriceListId,
		cp.CampaignId,
		cp.Price
	FROM
		#CampaignProduct cp
		LEFT OUTER JOIN #CampaignProduct cp2 
			ON cp2.PriceListId = cp.PriceListId
				AND cp2.ProductId = cp.ProductId
				AND (cp2.Price < cp.Price OR cp2.Price = cp.Price AND cp2.CampaignId > cp.CampaignId)      
	WHERE cp2.ProductId IS NULL

	DROP TABLE #Campaign
	DROP TABLE #CampaignAction
	DROP TABLE #CampaignProduct

	---- @PriceList

	DECLARE @PriceList TABLE (
		PriceListId INT PRIMARY KEY,
		ChannelId INT
	)

	INSERT INTO @PriceList 
	SELECT
		pl.PriceListId,
		c.ChannelId
	FROM
		core.tChannel c
		INNER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = c.ChannelId
		INNER JOIN product.tProductRegistry pr ON pr.ProductRegistryId = pmc.ProductRegistryId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = pr.ProductRegistryId
	WHERE
		c.ChannelId IN (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI)

	---- Result

	SELECT 
		pl.ChannelId,
		--ll.HYErpId,
		pli.ProductId,
		--pli.PriceListId,
		--pli.PriceIncludingVat AS ListPrice,
		--bcp.Price AS DiscountPrice,
		CASE 
			WHEN (bcp.Price IS NULL OR bcp.Price > pli.PriceIncludingVat) THEN pli.PriceIncludingVat
			ELSE bcp.Price 
		END AS SitePrice
		--CAST(((1-(bcp.Price/pli.PriceIncludingVat)) * 100) AS DECIMAL (18,2)) AS 'Total discount %',
	FROM
		@PriceList pl
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = pl.PriceListId
		INNER JOIN product.tProduct t ON t.ProductId = pli.ProductId
		LEFT OUTER JOIN #BestCampaignPrice bcp ON bcp.PriceListId = pl.PriceListId AND bcp.ProductId = t.ProductId
		LEFT OUTER JOIN campaign.tCampaign c ON c.CampaignId = bcp.CampaignId
		INNER JOIN lekmer.tLekmerProduct ll ON ll.ProductId = pli.ProductId
	WHERE
		c.CampaignId IS NOT NULL
	ORDER BY
		pl.ChannelId

   DROP TABLE #BestCampaignPrice

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductLekmer]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateProductLekmer]
AS
BEGIN
	SET NOCOUNT ON 

	-- Delte all ***Missing*** (tmp)
	DELETE FROM integration.tempProduct WHERE ArticleCodeTitle= '***Missing***'

	-- tCategory
	EXEC [integration].[usp_UpdateTCategoryLekmer] -- creates the items if necessary
	EXEC [integration].[usp_UpdateCategoryTitleLekmer] -- update titles if necessary
	
	-- tBrand
	EXEC [integration].[usp_UpdateBrandLekmer]
	EXEC [integration].[usp_UpdateBrandLekmerCreationDate] -- adds the date the brand was created
	
	-- tProduct
	EXEC [integration].[usp_UpdateProductLekmer]
	EXEC [integration].[usp_ImportUpdateLekmerProduct]
	
	-- tPriceListItem
	EXEC [integration].[usp_UpdateProductPRICELekmer]
	
	-- tProductSize
	EXEC [integration].[usp_UpdateSizeLekmer]

	--------------------------------------

	DECLARE 
		@HYArticleNumberFull NVARCHAR(50),
		@HYArticleNumberColorSize NVARCHAR(50),
		@HYArticleNumberColor NVARCHAR(50),
		@HYSizeId NVARCHAR(50),
		@EanCode NVARCHAR(50),
		@NoInStock NVARCHAR(250),
		@ArticleTitle NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@BrandId INT,
		@HYSizeValue NVARCHAR(50),
		@Weight NVARCHAR(50),
		@PossibleToDisplay NVARCHAR(100),
		@StockStatusErpId NVARCHAR(50),
		@StockStatusId INT,
		
		@CategoryId INT,
		@Price DECIMAL,
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@LekmerErpId NVARCHAR(25),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL,
		
		@NewFromDate DATETIME,
		@NewToDate DATETIME,

		-- Variables for Channel
		@HYChannel NVARCHAR(40),
		@ChannelId INT,
		@ProductRegistryId INT,
		@PriceListId INT,

		@TradeDoublerProductGroup NVARCHAR(50),

		@IsProductRegistry BIT,
		@NeedToInsertProductRelationData BIT
	
	SET @Vat = 25.0
	
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 4, @NewFromDate)

	------------------------------------

	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			tp.HYarticleId,
			tp.ArticleTitle,
			tp.SizeId,
			(tp.Price/100),
			tp.NoInStock,
			tp.ArticleGroupId,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.BrandId,
			tp.EanCode,
			tp.LekmerArtNo,
			[tp].[Weight],
			[tp].[PossibleToDisplay],
			[tp].[Ref1] -- StockStatusErpId
		FROM
			[integration].tempProduct tp
		--WHERE NOT EXISTS (SELECT 1
		--				  FROM lekmer.tLekmerProduct lp, product.tPriceListItem prp
		--				  WHERE lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
		--					AND lp.ProductId = prp.ProductId
		--					AND SUBSTRING(tp.HYarticleId, 3,1) = prp.PriceListId)

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@HYArticleNumberFull,
			@ArticleTitle,
			@HYSizeValue,
			@Price,
			@NoInStock,
			@VarugruppId,
			@VaruklassId,
			@VarukodId,
			@HYBrandId,
			@EanCode,
			@LekmerErpId,
			@Weight,
			@PossibleToDisplay,
			@StockStatusErpId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			SET @IsProductRegistry = 0
			SET @NeedToInsertProductRelationData = 0
			
			-- Split @HYArticleNumberFull
			-- 001-0000001-1017-108
			SET @HYChannel = SUBSTRING(@HYArticleNumberFull, 1, 3)                  -- [001]-0000001-1017-108
			SET @HYArticleNumberColorSize = SUBSTRING(@HYArticleNumberFull, 5, 17)  -- 001-[0000001-1017-108]
			SET @HYArticleNumberColor = SUBSTRING(@HYArticleNumberFull, 5, 12)      -- 001-[0000001-1017]-108
			SET @HYSizeId = SUBSTRING(@HYArticleNumberFull, 18, 3)                  -- 001-0000001-1017-[108]

			-- find channel id
			SET @ChannelId = (SELECT c.[ChannelId] FROM [lekmer].[tLekmerChannel] c WHERE c.[ErpId] = @HYChannel)
			IF @ChannelId IS NULL
			BEGIN
				-- undefined channel, skip the row
				GOTO NEXT_ROW
			END

			-- find product registry id
			SET @ProductRegistryId = (SELECT [pmc].[ProductRegistryId] FROM [product].[tProductModuleChannel] pmc WHERE [pmc].[ChannelId] = @ChannelId)
			
			-- find price list id
			SET @PriceListId = (
				SELECT TOP 1
					pl.[PriceListId]
				FROM
					[product].[tProductModuleChannel] pmc
					INNER JOIN [product].[tPriceList] pl ON pl.[PriceListRegistryId] = pmc.[PriceListRegistryId]
				WHERE
					pmc.[ChannelId] = @ChannelId
					AND pl.[PriceListStatusId] = 0 -- online
			)

			-- find stock status id
			SET @StockStatusId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [ErpId] = @StockStatusErpId)
			SET @StockStatusId = ISNULL(@StockStatusId, 0) -- Active by default

			-- find product id
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYArticleNumberColor)
			IF @ProductId IS NOT NULL
			BEGIN
				IF EXISTS ( SELECT 1 FROM [product].[tProductRegistryProduct]
							WHERE
								[ProductId] = @ProductId
								AND
								[ProductRegistryId] = @ProductRegistryId )
				BEGIN
					-- ???
					SET @IsProductRegistry = 1
					GOTO PRODUCT_END
				END
			END

			PRODUCT_START:
			
			---- Skip processing the same @HYErpId + @Channel few times
			--IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			--BEGIN
			--	GOTO PRODUCT_END
			--END ELSE BEGIN
			--	SET	@PrevHYErpId = @HYErpId
			--	SET	@PrevChannel = @Channel
			--END
			
			BEGIN TRANSACTION
			
			SET @CategoryId = (SELECT CategoryId 
							   FROM product.tCategory 
							   WHERE ErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, ''))
			
			SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)

			IF @ProductId IS NULL
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNumberFull + ' EanCode ' + @EanCode
				
				-- tProduct
				INSERT INTO [product].tProduct (
					ErpId,
					EanCode,
					IsDeleted,
					NumberInStock,
					CategoryId,
					Title,
					[Description],
					ProductStatusId
				)
				VALUES (
					@HYArticleNumberColor,
					@EanCode,
					0, -- IsDeleted
					@NoInStock,
					@CategoryId,
					@ArticleTitle, 
					'', -- Description
					1 -- Offline
				)
											
				SET @ProductId = SCOPE_IDENTITY()
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNumberFull + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
	
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId,
					HYErpId,
					BrandId,
					IsBookable,
					AgeFromMonth,
					AgeToMonth,
					IsNewFrom,
					IsNewTo,
					IsBatteryIncluded,
					ExpectedBackInStock,
					LekmerErpId,
					ShowVariantRelations,
					[Weight],
					[ProductTypeId],
					[IsAbove60L],
					[StockStatusId]
				)
				VALUES (
					@ProductId, 
					@HYArticleNumberColor,
					@BrandId,
					0, -- IsBookable, 
					0, -- AgeFromMonth
					0, -- AgeToMonth
					@NewFromDate,
					@NewToDate,
					0, -- IsBatteryIncluded
					NULL, --ExpectedBackInStock
					@LekmerErpId,
					1, -- ShowVariantRelations
					CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), @Weight) / 1000),
					1,
					CASE WHEN @PossibleToDisplay IS NULL OR @PossibleToDisplay <> 'G' THEN 0 ELSE 1 END,
					@StockStatusId
				)

			END	 -- Product already exist in tLekmerProduct
			ELSE IF NOT EXISTS ( SELECT 1 FROM product.tPriceListItem prp
								 WHERE
									prp.ProductId = @ProductId
									AND
									prp.PriceListId = @PriceListId )
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'EXISTING: @HYArticleNumberColorSize ' + @HYArticleNumberColorSize + ' @HYChannel ' + @HYChannel + ' ProductId' + CAST(@ProductId as varchar(10))
			END
			
			IF @NeedToInsertProductRelationData = 1
			BEGIN
				------------------------------------------------------------------------------------------
				-- tProductRegistryProduct
				
				IF NOT EXISTS ( SELECT 1 FROM product.tProductRegistryProduct
							    WHERE
									ProductId = @ProductId
									AND
									ProductRegistryId = @ProductRegistryId )
				BEGIN
					INSERT INTO product.tProductRegistryProduct (
						ProductId,
						ProductRegistryId
					)
					VALUES (
						@ProductId,
						@ProductRegistryId
					)
				END				
				
				------------------------------------------------------------------------------------------
				-- tPriceListItem

				IF NOT EXISTS ( SELECT 1 FROM product.tPriceListItem
							    WHERE
									ProductId = @ProductId
									AND
									PriceListId = @PriceListId )
				BEGIN
					INSERT INTO product.tPriceListItem (
						PriceListId,
						ProductId,
						PriceIncludingVat,
						PriceExcludingVat,
						VatPercentage
					)
					VALUES (
						@PriceListId,
						@ProductId, 
						@Price, 
						@Price / (1.0+@Vat/100.0),
						@Vat
					)
				END

				------------------------------------------------------------------------------------------ 
				-- tTradeDoublerProductGroupMapping

				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '')
													   AND ChannelId = @ChannelId)

				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId
										 AND tTDPGM.ChannelId = @ChannelId)
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							@ChannelId
						)
					END
				END 
			END

			COMMIT

			PRODUCT_END:
			
			------------------------------------------------------------------------------------------ 
			-- ProductSize
				
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [integration].[usp_ImportUpdateProductSizesLekmer]
				@HYChannel,
				@ProductId,
				@HYArticleNumberColorSize,
				@HYSizeId,
				@HYSizeValue,
				@NoInStock,
				@Weight,
				@StockStatusId
			
		END TRY
		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
				
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)

		END CATCH

		NEXT_ROW:

		FETCH NEXT FROM cur_product 
			INTO 
				@HYArticleNumberFull,
				@ArticleTitle,
				@HYSizeValue,
				@Price,
				@NoInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@HYBrandId,
				@EanCode,
				@LekmerErpId,
				@Weight,
				@PossibleToDisplay,
				@StockStatusErpId

	END
	
	CLOSE cur_product
	DEALLOCATE cur_product
	
	EXEC [integration].[usp_UpdateProductBrandLekmer]
	EXEC [integration].[usp_ChannelRestrictionsProducts]
	EXEC [integration].[usp_REATagProduct]
	
	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]
	
	-- Update stock status /active-passive/online-offline /*before package update !!!*/
	EXEC [integration].[usp_UpdateProductStatusLekmer]
	
	-- Add 'Outlet' tag
	EXEC [integration].[usp_OutletTagProduct]
	
	-- Update package stock
	EXEC [productlek].[pPackageUpdate]
	
	-- Track products chnages to eSales export
	EXEC [integration].[pTrackProductChangesLekmer]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignLandingPageTranslationSave]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageTranslationSave]
	@CampaignLandingPageId	INT,
	@LanguageId				INT,
	@WebTitle				NVARCHAR(500),
	@Description			NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[campaignlek].[tCampaignLandingPageTranslation]
	SET
		[WebTitle] = @WebTitle,
		[Description] = @Description
	WHERE
		[CampaignLandingPageId] = @CampaignLandingPageId
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [campaignlek].[tCampaignLandingPageTranslation] (
			[CampaignLandingPageId],
			[LanguageId],
			[WebTitle],
			[Description]				
		)
		VALUES (
			@CampaignLandingPageId,
			@LanguageId,
			@WebTitle,
			@Description
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pBackofficeProductImportById]'
GO
ALTER PROCEDURE [integration].[pBackofficeProductImportById]
	@HYErpId		NVARCHAR(20),
	@Title			NVARCHAR(256),
	@Description	NVARCHAR(MAX),
	@ChannelNameISO	NVARCHAR(10),
	@TagIds			VARCHAR(MAX),
	@UserName		NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Get language id by Channel
	DECLARE @LanguageId INT
	DECLARE @ChannelId INT
	
	SELECT 
		@LanguageId = l.LanguageId,
		@ChannelId = h.ChannelId
	FROM [core].[tLanguage] l
		 INNER JOIN [core].[tChannel] h ON l.LanguageId = h.LanguageId
		 INNER JOIN [core].[tCountry] c ON h.CountryId = c.CountryId
	WHERE
		c.ISO = @ChannelNameISO
	
	-- Get product id by HYErpId
	DECLARE @ProductId INT
	SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] WHERE HYErpId = @HYErpId)

	-- insert into tProductTransaltion empty rows
	INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
	SELECT
		p.ProductId,
		@LanguageId
	FROM
		[product].[tProduct] p
	WHERE
		NOT EXISTS (SELECT 1 FROM [product].[tProductTranslation] n
					WHERE n.ProductId = p.ProductId 
					AND n.LanguageId = @LanguageId)
					
	IF @Title = ''
		SET @Title = NULL
				
	BEGIN TRY
		BEGIN TRANSACTION				
			
		IF @LanguageId != 1
			BEGIN
				UPDATE [product].[tProductTranslation]
				SET Title = @Title
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (Title != @Title OR Title IS NULL)
	
	
				/*BEGIN*/
				-- Remove Restriction from [lekmer].[tProductRegistryRestrictionProduct] if it was added due to empty title.
				-- Get product id by HYErpId
				DECLARE @ProductRegistryId INT
				SET @ProductRegistryId = (SELECT ProductRegistryId FROM [product].[tProductModuleChannel] WHERE ChannelId = @ChannelId)
	
				-- Insert product to tProductRegistryproduct
				IF @Title IS NOT NULL
				BEGIN
					DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
					WHERE
						ProductRegistryId = @ProductRegistryId
						AND ProductId = @ProductId
						AND RestrictionReason = 'No Translation'
						AND UserId IS NULL
						
					DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]
					INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
					VALUES (@ProductId, @ProductRegistryId)
					
					EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
				END
				/*END*/
				
					
				UPDATE [product].[tProductTranslation]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END
		ELSE
			BEGIN
				IF @Title IS NOT NULL
				BEGIN
					UPDATE [product].[tProduct]
					SET Title = @Title
					WHERE
						ProductId = @ProductId
				END
					
				UPDATE [product].[tProduct]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END
			
		-- Update/Insert product tags.
		DECLARE @TagId INT
		DECLARE cur_tag CURSOR FAST_FORWARD FOR
			SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')
			
		OPEN cur_tag
		FETCH NEXT FROM cur_tag INTO @TagId
			
		WHILE @@FETCH_STATUS = 0
			BEGIN
				-- Update/Insert product tags.
				IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
						   INNER JOIN product.tProduct p ON pt.ProductId = p.ProductId	
						   INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
						   WHERE 
								lp.HYErpId = @HYErpId
								AND pt.TagId = @TagId)
					BEGIN
						INSERT INTO lekmer.tProductTag (ProductId, TagId)
						VALUES ((SELECT TOP(1) p.ProductId FROM product.tProduct p 
								INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
								WHERE lp.HYErpId = @HYErpId), @TagId)
					END
				
				FETCH NEXT FROM cur_tag INTO @TagId
			END
		CLOSE cur_tag
		DEALLOCATE cur_tag
		
		-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
		INSERT INTO integration.tBackofficeProductInfoImport
		(
			[HYErpId]
			,[Title]
			,[Description]
			,[ChannelNameISO]
			,[TagIdCollection]
			,[UserName]
			,[InsertedDate]
		)
		VALUES 
		(
			@HYErpId
			,@Title
			,@Description
			,@ChannelNameISO
			,@TagIds
			,@UserName
			,CAST(GETDATE() AS SMALLDATETIME)
		)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableMediaProductRegistryGetAllBySizeTable]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistryGetAllBySizeTable]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[stmpr].*
	FROM
		[lekmer].[vSizeTableMediaProductRegistry] stmpr
		INNER JOIN [lekmer].[tSizeTableMedia] stm ON [stm].[SizeTableMediaId] = [stmpr].[SizeTableMediaProductRegistry.SizeTableMediaId]
	WHERE
		[stm].[SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignLandingPageGetByCampaignId]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageGetByCampaignId]
	@CampaignId INT,
	@ChannelId	INT
AS 
BEGIN

	DECLARE @CampaignLandingpageId INT
	SET @CampaignLandingpageId = (SELECT [CampaignLandingPage.CampaignLandingPageId]
								  FROM [campaignlek].[vCampaignLandingPageSecure] clp
								  WHERE [clp].[CampaignLandingPage.CampaignId] = @CampaignId)
	
	SELECT
		[clp].*
	FROM
		[campaignlek].[vCampaignLandingPage] clp
		INNER JOIN [core].[tChannel] c ON [c].[LanguageId] = [clp].[CampaignLandingPage.LanguageId]
	WHERE
		[clp].[CampaignLandingPage.CampaignLandingPageId] = @CampaignLandingpageId
		AND [c].[ChannelId] = @ChannelId
		
	SELECT
		[crclp].*
	FROM
		[campaignlek].[vCampaignRegistryCampaignLandingPage] crclp
		INNER JOIN [campaign].[tCampaignModuleChannel] cmc ON [cmc].[CampaignRegistryId] = [crclp].[CampaignRegistryCampaignLandingPage.CampaignRegistryId]
	WHERE
		[crclp].[CampaignRegistryCampaignLandingPage.CampaignLandingPageId] = @CampaignLandingpageId
		AND [cmc].[ChannelId] = @ChannelId
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pProductExportToCdon_BACKUP20120810]'
GO
ALTER PROCEDURE [integration].[pProductExportToCdon_BACKUP20120810]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set transaction isolation level read uncommitted
	
	declare @Tree table (
		CategoryId int primary key with (ignore_dup_key = on),
		Depth int,
		HasChildren bit
	)
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		
   select ch.ChannelId
         ,ch.Title as Name
         ,replace(ch.Title, 'Lekmer.', '') as Tld
         ,cu.ISO as CurrencyCode
         ,cu.CurrencyId
     from @Channel c
          inner join core.tChannel ch
                  on ch.ChannelId = c.ChannelId
          inner join core.tCurrency cu
                  on cu.CurrencyId = ch.CurrencyId

	
	insert into @Tree values (1000445, 0, 1)	-- Barn och Baby	
	insert into @Tree values (1000494, 0, 1)	-- Inredning
	insert into @Tree values (1000533, 0, 1)	-- Leksaker
	insert into @Tree values (1000856, 0, 1)	-- Underhållning
		
	declare @depth int
	set @depth = 0
	while (@@ROWCOUNT > 0)
	begin
		set @depth = @depth + 1
		
		insert into @Tree
		select c.CategoryId, @depth, 1
		from @Tree t 
			inner join product.tCategory c on c.ParentCategoryId = t.CategoryId
	end
		
	update t set HasChildren = 0
	from @Tree t
		left outer join product.tCategory c on c.ParentCategoryId = t.CategoryId
	where c.CategoryId is null
	
	-- Category
	select c.CategoryId, 	   
	   c.ParentCategoryId,	   
	   c.Title, c.ErpId, t.Depth
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		
	-- Category Translation
	select ct.CategoryId, ct.LanguageId, case when (ct.LanguageId = 1) then c.Title else ct.Title end as Title
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		inner join product.tCategoryTranslation ct on ct.CategoryId = t.CategoryId
	where ct.Title is not null
	
	-- Product to examine
	declare @CategoryLeaves table
	(
		CategoryId int primary key
	)
	insert into @CategoryLeaves
	select CategoryId
	from @Tree
	where Depth = 2 and HasChildren = 0
	
	declare @Product table
	(
		ProductId int primary key
	)
	insert into @Product
	select p.ProductId
	from product.tProduct p
		inner join @CategoryLeaves cl on cl.CategoryId = p.CategoryId
	where p.IsDeleted = 0


   -- Import products using the same third level category	
   if object_id('tempdb..#OnlineCategory') is not null
       drop table #OnlineCategory

   create table #OnlineCategory
   (
      CategoryId int primary key
   )
   
   insert into #OnlineCategory
   select distinct CategoryId
   from product.tProduct p
   where p.ProductStatusId = 0

   if object_id('tempdb..#CategoryLevel') is not null
       drop table #CategoryLevel

   create table #CategoryLevel
   (
      CategoryId int primary key,
      ErpId varchar(13),
      Level1Id int,
      Level2Id int,
      Level3Id int
   )
   	
   insert into #CategoryLevel ( CategoryId, ErpId, Level1Id, Level2Id, Level3Id )
   select c.CategoryId, 
      c.ErpId, 
      convert(int, substring(erpId, 3, 2)) as Level1Id, 
      convert(int, substring(erpId, 6, 3)) as Level2Id, 
      convert(int, substring(erpId, 10, 4)) as Level3Id
   from #OnlineCategory oc
		inner join product.tCategory c on c.CategoryId = oc.CategoryId
   where len(c.ErpId) = 13
 

   if object_id('tempdb..#CategoryMapping') is not null
       drop table #CategoryMapping

   create table #CategoryMapping
   (
      SourceCategoryId int primary key with (ignore_dup_key = on),
      DestinationCategoryId int
   )	

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and cl2.Level2Id = cl.Level2Id
         and not cl2.Level1Id in (30)
   where cl.Level1Id = 30     

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and not cl2.Level1Id in (30)
      inner join product.tCategory c on c.CategoryId = cl.CategoryId
		and not c.Title in ('Övrigt', 'Tillbehör och accessoarer', 'Blandade titlar', 'Reservdelar och tillbehör',
			'Plocklådor')
   where cl.Level1Id = 30


--   if object_id('tempdb..#CategoryMappingPath') is not null
--       drop table #CategoryMappingPath
   
--select cm.DestinationCategoryId, ppc.Title + '/' + pc.Title + '/' + c.Title as Path1, 
--   cm.SourceCategoryId, ppc2.Title + '/' +  pc2.Title + '/' +  c2.Title as Path2
----   ,p.Title
--into #CategoryMappingPath
--from #CategoryMapping cm
--   inner join product.tCategory c on c.CategoryId = cm.DestinationCategoryId
--   inner join product.tCategory pc on pc.CategoryId = c.ParentCategoryId
--   inner join product.tCategory ppc on ppc.CategoryId = pc.ParentCategoryId      
--   inner join product.tCategory c2 on c2.CategoryId = cm.SourceCategoryId
--   inner join product.tCategory pc2 on pc2.CategoryId = c2.ParentCategoryId
--   inner join product.tCategory ppc2 on ppc2.CategoryId = pc2.ParentCategoryId      
   

--select cmp.*, p.ProductId, p.Title
--from #CategoryMappingPath cmp 
--   inner join product.tProduct p on p.CategoryId = cmp.DestinationCategoryId
--where p.ProductStatusId = 0
--order by cmp.Path1, cmp.Path2, p.Title
  
   drop table #CategoryLevel
	
	--insert into @Product
	--select p.ProductId
	--from #CategoryMapping cm
	--   inner join product.tProduct p on p.CategoryId = cm.SourceCategoryId
	--where p.IsDeleted = 0

	
	delete from @Product where ProductId in (
		select ProductId
		from product.tProductImage
		group by ProductId, MediaId
		having count(*) > 1
	)

	delete tp
	from @Product tp
		inner join lekmer.tProductSize ps on ps.ProductId = tp.ProductId

	declare @SpecialCategoryMapping table
	(
		SourceCategoryId int primary key,
		DestinationCategoryId int
	)
	 insert into @SpecialCategoryMapping ( SourceCategoryId, DestinationCategoryId )
	 select c.CategoryId, 1001016
	 from product.tCategory c
	 where ParentCategoryId = 1001015
		and not CategoryId in (1000938
,1001016
,1001017
,1001018
,1001047
,1001062
,1001065
,1001071
,1001072
,1001209
,1001284
,1001355
,1001368
,1002017)

	
	-- Product
   select p.ProductId
         ,lp.HyErpId as ErpId
         ,p.EanCode
         ,p.IsDeleted
         ,case 
				when (scm.DestinationCategoryId is not null) then scm.DestinationCategoryId 
				when (cm.DestinationCategoryId is not null) then cm.DestinationCategoryId 			
			else 
				p.CategoryId 
			end as CategoryId
         ,p.Title
         ,p.ProductStatusId
         ,lp.AgeFromMonth
         ,lp.AgeToMonth
		   ,lp.LekmerErpId as SupplierArticleNumber
		   ,p.[Description]
		   ,lp.BrandId
		   ,p.NumberInStock
     from @Product t
          inner join product.tProduct p on p.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on t.ProductId = lp.ProductId
          left outer join @SpecialCategoryMapping scm on scm.SourceCategoryId = p.CategoryId
          left outer join #CategoryMapping cm on cm.SourceCategoryId = p.CategoryId
			
	  drop table #CategoryMapping
---- Discounts
--declare @PriceListDiscounts table
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2)
--)
--insert into @PriceListDiscounts ( PriceListId, ProductId, DiscountPrice )
--select 
--   case 
--      when (c.CampaignRegistryId = 1) then 1
--      when (c.CampaignRegistryId = 2) then 2
--      when (c.CampaignRegistryId = 3) then 3
--      when (c.CampaignRegistryId = 4) then 4
--      when (c.CampaignRegistryId = 5) then 5
--   else
--      c.CampaignRegistryId
--   end as PriceListId,
--   pdai.ProductId, pdai.DiscountPrice, pda.ProductActionId
--from campaign.tCampaign c
--   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId   
--   inner join lekmer.tProductDiscountAction pda on pda.ProductActionId = pa.ProductActionId
--   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pda.ProductActionId   
--where c.CampaignStatusId = 0
--   and (c.StartDate is null or c.StartDate <= getdate())
--   and (c.EndDate is null or getdate() <= c.EndDate)
--   and pdai.ProductId = 7364


--create table #PriceListDiscount
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2) primary key (PriceListId, ProductId)
--)

--insert into #PriceListDiscount
--select PriceListId, ProductId, min(DiscountPrice)
--from @PriceListDiscounts
--group by PriceListId, ProductId

if object_id('tempdb..#Campaign') is not null
    drop table #Campaign

create table #Campaign
(
   CampaignId int,
   PriceListId int primary key (CampaignId, PriceListId) with (ignore_dup_key = on)
)

insert into #Campaign ( CampaignId, PriceListId )
select c.CampaignId, [crc].[CampaignRegistryId]
from campaign.tCampaign c
INNER JOIN [campaignlek].[tCampaignRegistryCampaign] crc ON [crc].[CampaignId] = [c].[CampaignId]
where c.CampaignStatusId = 0
   and (c.StartDate is null or c.StartDate <= getdate())
   and (c.EndDate is null or getdate() <= c.EndDate)



if object_id('tempdb..#CampaignAction') is not null
    drop table #CampaignAction

create table #CampaignAction (
   CampaignId int,
   SortIndex int,   
   ProductActionId int,
   Ordinal int
)

declare @Index int
set @Index = 1

insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
from #Campaign c
   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
   left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
      and pa2.Ordinal < pa.Ordinal
where pa2.ProductActionId is null

while (@@ROWCOUNT > 0)
begin
   set @Index = @Index + 1
      
   insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
   select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
   from #Campaign c      
      inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
         and tca.SortIndex = @Index - 1
      inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
         and pa.Ordinal > tca.Ordinal
      left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
         and pa2.Ordinal > tca.Ordinal
         and pa2.Ordinal < pa.Ordinal
   where pa2.ProductActionId is null
   option (force order)
end

if object_id('tempdb..#CampaignProduct') is not null
    drop table #CampaignProduct

create table #CampaignProduct (
   CampaignId int,
   ProductId int,
   Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
)

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, ip.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId
   
	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, p.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		inner join campaign.tPercentagePriceDiscountActionIncludeCategory ic on ic.ProductActionId = ca.ProductActionId
		inner join product.tProduct p on p.CategoryId = ic.CategoryId   

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, pdai.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
   inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId

update cp set Price = pli.PriceIncludingVat
from #Campaign c
   inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
   inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
      and pli.ProductId = cp.ProductId

declare @LastIndex int
set @Index = 1
select @LastIndex = max(SortIndex)
from #CampaignAction

while (@Index <= @LastIndex)
begin
		update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1
			inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId         
			inner join campaign.tPercentagePriceDiscountActionIncludeCategory ic on ic.ProductActionId = ca.ProductActionId
			inner join product.tProduct p on p.CategoryId = ic.CategoryId
			inner join #CampaignProduct cp on cp.ProductId = p.ProductId
				and cp.CampaignId = ca.CampaignId
		where ca.SortIndex = @Index

   update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1
      inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId         
      inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
   
   update cp set Price = pdai.DiscountPrice
   -- select cp.Price, pdai.DiscountPrice
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1000001
      inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
         and cp.CampaignId = ca.CampaignId
      inner join #Campaign c on c.CampaignId = cp.CampaignId
      inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId
   where ca.SortIndex = @Index
      and (cp.Price > pdai.DiscountPrice or cp.Price is null)

   set @Index = @Index + 1
end

delete cp
from #CampaignProduct cp 
where Price is null

update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
   

if object_id('tempdb..#BestCampaignPrice') is not null
    drop table #BestCampaignPrice

create table #BestCampaignPrice (
   ProductId int,
   PriceListId int,
   Price decimal(16, 2)
)
insert into #BestCampaignPrice (ProductId, PriceListId, Price)
select cp.ProductId, c.PriceListId, min(cp.Price) as Price
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
group by cp.ProductId, c.PriceListId

drop table #Campaign
drop table #CampaignAction
drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title

	-- Product Channel
	select pr.ProductId, pr.ProductRegistryId as ChannelId
	from @Channel c
		inner join product.tProductRegistryProduct pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tProduct p on p.ProductId = pr.ProductId
		inner join @Product t on t.ProductId = p.ProductId
		inner join @PriceList pl on pl.ChannelId = c.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		   and pli.ProductId = p.ProductId
	
	select pl.ChannelId, pli.ProductId, 
	   pli.PriceIncludingVat as Price, pli.PriceIncludingVat - pli.PriceExcludingVat as Vat, 
	   pld.Price as DiscountPrice, case when (pld.Price is null) then null else pld.Price * pli.VatPercentage / 100 end as DiscountPriceVat
	--into temp.CdonPriceNewCalc3
	from @PriceList pl
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		inner join @Product t on t.ProductId = pli.ProductId
		left outer join #BestCampaignPrice pld on pld.PriceListId = pl.PriceListId
		   and pld.ProductId = t.ProductId
	
   drop table #BestCampaignPrice
		
	--drop table #PriceListDiscount
	
	-- Product Media
	declare @AllProductImages table (
	   ProductId int,
	   MediaId int,
	   ProductImageGroupId int
	   primary key (ProductId, MediaId, ProductImageGroupId) with (ignore_dup_key = on)
	)

   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
         ,p.MediaId
         ,1 as ProductImageGroupId
   from @Product t
         inner join product.tProduct p on p.ProductId = t.ProductId
   where p.MediaId is not null
	
   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
      ,p.MediaId
      ,p.ProductImageGroupId
   from @Product t            
       inner join product.tProductImage p on p.ProductId = t.ProductId
   
   select pi.ProductId 
		  ,lp.HyErpId AS ErpId
		  ,pi.MediaId
		  ,pi.ProductImageGroupId
		  ,mf.Extension
	 from @AllProductImages pi
	      inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	      inner join media.tMedia m on pi.MediaId = m.MediaId
		   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
   -- Icons				   
   select lpi.IconId
         ,p.ProductId
     from @Product p
          inner join lekmer.tLekmerProductIcon lpi
                  on lpi.ProductId = p.ProductId
      
   -- Product translations (title & description)            
   select pt.ProductId
         ,lp.HyErpId AS ErpId
         ,pt.LanguageId
         ,pt.Title
         ,pt.[Description]
     from @product t 
         inner join product.tProductTranslation pt on pt.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on lp.ProductId = pt.ProductId
    where Title is not null or Description is not null
    
    -- Brands  
   select b.BrandId
         ,b.Title
         ,b.ExternalUrl
         ,b.MediaId
         ,mf.Extension
     from lekmer.tBrand b 
          inner join media.tMedia m on m.MediaId = b.MediaId
          inner join media.tMediaFormat mf on mf.MediaFormatId = m.MediaFormatId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignLandingPageTranslationWebTitleGetAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageTranslationWebTitleGetAll]
	@CampaignLandingPageId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] AS 'Id',
		[clpt].[CampaignLandingPageTranslation.LanguageId] AS 'LanguageId',
		[clpt].[CampaignLandingPageTranslation.WebTitle] AS 'Value'
	FROM
	    [campaignlek].[vCampaignLandingPageTranslation] clpt
	WHERE 
		[clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] = @CampaignLandingPageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignRegistryCampaignSave]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignSave]
	@CampaignId				INT,
	@CampaignRegistryIds	VARCHAR(MAX),
	@Delimiter				CHAR(1)
AS
BEGIN
	EXEC [campaignlek].[pCampaignRegistryCampaignDelete] @CampaignId	

	INSERT INTO [campaignlek].[tCampaignRegistryCampaign] ([CampaignRegistryId], [CampaignId])
	SELECT [crc].[ID], @CampaignId FROM [generic].[fnConvertIDListToTable](@CampaignRegistryIds, @Delimiter) crc
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignLandingPageTranslationDescriptionGetAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageTranslationDescriptionGetAll]
	@CampaignLandingPageId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] AS 'Id',
		[clpt].[CampaignLandingPageTranslation.LanguageId] AS 'LanguageId',
		[clpt].[CampaignLandingPageTranslation.Description] AS 'Value'
	FROM
	    [campaignlek].[vCampaignLandingPageTranslation] clpt
	WHERE 
		[clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] = @CampaignLandingPageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageSave]'
GO
ALTER PROCEDURE [productlek].[pPackageSave]
	@PackageId			INT,
	@MasterProductId	INT,
	@ErpId				VARCHAR(50) = NULL,
	@StatusId			INT,
	@StockStatusId		INT,
	@Title				NVARCHAR(256),
	@WebShopTitle		NVARCHAR(256),
	@NumberInStock		INT,
	@CategoryId			INT,
	@Description		NVARCHAR(MAX),
	@GeneralInfo		NVARCHAR(MAX),
	@PriceXml			XML = NULL
	/*
	'<prices>
		<price priceListId="1" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="2" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="3" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="4" priceIncludingVat="10.99" vat="25.00"/>
	</prices>'
	*/
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	BEGIN
		BEGIN TRANSACTION
			IF @ErpId IS NULL
			BEGIN
				SET @ErpId = (SELECT TOP (1) [pr].[ErpId] 
						  FROM [productlek].[tPackage] p
								INNER JOIN [product].[tProduct] pr ON [pr].[ProductId] = [p].[MasterProductId]
						  ORDER BY [PackageId] DESC)

				IF @ErpId IS NULL
				BEGIN
					SET @ErpId = 'P000001-0000'
				END
				ELSE
				BEGIN
					DECLARE @ErpTemp INT
					SET @ErpTemp = (SELECT CAST(SUBSTRING(@ErpId, 2, 6) AS INT) + 1)

					SET @ErpId = (SELECT 'P' + RIGHT('000000' + CAST(@ErpTemp AS VARCHAR(6)), 6) + '-0000')
				END
			END
			
			-- tProduct
			INSERT INTO [product].[tProduct] (
				[ErpId],
				[IsDeleted],
				[NumberInStock],
				[CategoryId],
				[Title],
				[WebShopTitle],
				[Description],
				[ProductStatusId]
			)
			VALUES (
				@ErpId,
				0, -- IsDeleted
				@NumberInStock,
				@CategoryId,
				@Title,
				@WebShopTitle,
				@Description,
				@StatusId
			)

			SET @MasterProductId = SCOPE_IDENTITY()

			-- tLekmerProduct
			INSERT INTO [lekmer].[tLekmerProduct] (
				[ProductId],
				[IsBookable],
				[AgeFromMonth],
				[AgeToMonth],
				[IsBatteryIncluded],
				[HYErpId],
				[ShowVariantRelations],
				[ProductTypeId],
				[StockStatusId]
			)
			VALUES (
				@MasterProductId,
				0, -- IsBookable
				0, -- AgeFromMonth
				0, -- AgeToMonth
				0, -- IsBatteryIncluded
				@ErpId,
				0, -- ShowVariantRelations
				2, -- ProductTypeId = Package
				@StockStatusId
			)
			
			-- tProductUrl
			INSERT INTO [lekmer].[tProductUrl] (
				[ProductId],
				[LanguageId],
				[UrlTitle])
			SELECT
				@MasterProductId,
				[LanguageId],
				CASE WHEN @WebShopTitle IS NOT NULL THEN @WebShopTitle ELSE @Title END
			FROM [core].[tLanguage]

			-- tProductRegistryProduct
			INSERT INTO [product].[tProductRegistryProduct] (
				[ProductId],
				[ProductRegistryId]
			)
			SELECT
				@MasterProductId,
				[pr].[ProductRegistryId]
			FROM  [product].[tProductRegistry] pr
			WHERE NOT EXISTS (SELECT 1 FROM [product].[tProductRegistryProduct] prp
							  WHERE [prp].[ProductId] = @MasterProductId
									AND [prp].[ProductRegistryId] = [pr].[ProductRegistryId])

			-- tPackage
			INSERT INTO [productlek].[tPackage] ([MasterProductId], [GeneralInfo]) SELECT @MasterProductId, @GeneralInfo
			
			SET @PackageId = CAST(SCOPE_IDENTITY() AS INT)
		COMMIT
	END
	ELSE
	BEGIN
		-- tProduct
		UPDATE [product].[tProduct]
		SET [NumberInStock] = @NumberInStock,
			[CategoryId] = @CategoryId,
			[ProductStatusId] = @StatusId,
			[Title] = @Title,
			[WebShopTitle] = @WebShopTitle,
			[Description] = @Description
		WHERE [ProductId] = @MasterProductId
		
		UPDATE [lekmer].[tLekmerProduct]
		SET  [StockStatusId] = @StockStatusId
		WHERE [ProductId] = @MasterProductId
		
		UPDATE [productlek].[tPackage]
		SET [GeneralInfo] = @GeneralInfo
		WHERE [PackageId] = @PackageId
	END
	
	-- tPriceListItem
	DELETE FROM [product].[tPriceListItem] WHERE [ProductId] = @MasterProductId
	
	IF @PriceXml IS NOT NULL
	BEGIN
		INSERT INTO [product].[tPriceListItem] (
				[PriceListId],
				[ProductId],
				[PriceIncludingVat],
				[PriceExcludingVat],
				[VatPercentage]
			)
		SELECT
			c.value('@priceListId[1]', 'int'),
			@MasterProductId,
			c.value('@priceIncludingVat[1]', 'decimal(16,2)'),
			c.value('@priceIncludingVat[1]', 'decimal(16,2)') / (1.0+c.value('@vat[1]', 'decimal(16,2)')/100.0),
			c.value('@vat[1]', 'decimal(16,2)')
		FROM
			@PriceXml.nodes('/prices/price') T(c)
	END
	
	SELECT @PackageId, @MasterProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignDelete]'
GO

ALTER PROCEDURE [campaign].[pCampaignDelete]
@CampaignId		int
AS
BEGIN
	EXEC [campaignlek].[pCampaignRegistryCampaignDelete] @CampaignId	

	DELETE FROM campaign.tCampaign
	WHERE CampaignId = @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableMediaDeleteAllBySizeTable]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaDeleteAllBySizeTable]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[lekmer].[tSizeTableMedia]
	WHERE
		[SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTable]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTable]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE stmpr FROM
		[lekmer].[tSizeTableMediaProductRegistry] stmpr
		INNER JOIN [lekmer].[tSizeTableMedia] stm ON [stm].[SizeTableMediaId] = [stmpr].[SizeTableMediaId]
	WHERE
		[stm].[SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableDelete]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pSizeTableIncludeBrandDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeCategoryDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeProductDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableTranslationDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableRowDelete] @SizeTableId
	
	EXEC [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTable] @SizeTableId
	EXEC [lekmer].[pSizeTableMediaDeleteAllBySizeTable] @SizeTableId
		
	DELETE FROM [lekmer].[tSizeTable]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableMediaProductRegistrySave]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistrySave]
	@SizeTableMediaProductRegistryId INT,
	@SizeTableMediaId INT,
	@ProductRegistryId INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[lekmer].[tSizeTableMediaProductRegistry]
	SET 
		[SizeTableMediaId] = @SizeTableMediaId,
		[ProductRegistryId] = @ProductRegistryId
	WHERE
		[SizeTableMediaProductRegistryId] = @SizeTableMediaProductRegistryId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tSizeTableMediaProductRegistry](
			[SizeTableMediaId],
			[ProductRegistryId]
		)
		VALUES (
			@SizeTableMediaId,
			@ProductRegistryId
		)

		SET @SizeTableMediaProductRegistryId = SCOPE_IDENTITY()
	END

	RETURN @SizeTableMediaProductRegistryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignRegistryCampaignLandingPageDelete]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignLandingPageDelete]
	@CampaignLandingPageId	INT,
	@CampaignRegistryIds	VARCHAR(MAX),
	@Delimiter				CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE
		crclp
	FROM
		[campaignlek].[tCampaignRegistryCampaignLandingPage] crclp
		INNER JOIN [generic].[fnConvertIDListToTable](@CampaignRegistryIds, @Delimiter) AS r ON r.[ID] = [crclp].[CampaignRegistryId]
	WHERE
		[crclp].[CampaignLandingPageId] = @CampaignLandingPageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pProductPriceOnSite]'
GO
ALTER PROCEDURE [integration].[pProductPriceOnSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
if object_id('tempdb..#Campaign') is not null
    drop table #Campaign

create table #Campaign
(
   CampaignId int,
   PriceListId int primary key (CampaignId, PriceListId) with (ignore_dup_key = on)
)

insert into #Campaign ( CampaignId, PriceListId )
select c.CampaignId, [crc].[CampaignRegistryId]
from campaign.tCampaign c
INNER JOIN [campaignlek].[tCampaignRegistryCampaign] crc ON [crc].[CampaignId] = [c].[CampaignId]
where c.CampaignStatusId = 0
   and (c.StartDate is null or c.StartDate <= getdate())
   and (c.EndDate is null or getdate() <= c.EndDate)



if object_id('tempdb..#CampaignAction') is not null
    drop table #CampaignAction

create table #CampaignAction (
   CampaignId int,
   SortIndex int,   
   ProductActionId int,
   Ordinal int
)

declare @Index int
set @Index = 1

insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
select DISTINCT pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
from #Campaign c
   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
   left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
      and pa2.Ordinal < pa.Ordinal
where pa2.ProductActionId is null

while (@@ROWCOUNT > 0)
begin
   set @Index = @Index + 1
      
   insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
   select DISTINCT pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
   from #Campaign c      
      inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
         and tca.SortIndex = @Index - 1
      inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
         and pa.Ordinal > tca.Ordinal
      left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
         and pa2.Ordinal > tca.Ordinal
         and pa2.Ordinal < pa.Ordinal
   where pa2.ProductActionId is null
   option (force order)
end

if object_id('tempdb..#CampaignProduct') is not null
    drop table #CampaignProduct

create table #CampaignProduct (
   CampaignId int,
   PriceListId int,
   ProductId int,
   Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
)

insert into #CampaignProduct ( CampaignId, PriceListId, ProductId, Price )
select c.CampaignId, c.PriceListId, ip.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [ca].[ProductActionId]
   CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionAffectedProducts] ([ca].[ProductActionId]) ip
   
insert into #CampaignProduct ( CampaignId, PriceListId, ProductId, Price )
select c.CampaignId, c.PriceListId, pdai.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
   inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId

		 
update cp set Price = pli.PriceIncludingVat
from #Campaign c
   inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
   inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
      and pli.ProductId = cp.ProductId

declare @LastIndex int
set @Index = 1
select @LastIndex = max(SortIndex)
from #CampaignAction

while (@Index <= @LastIndex)
begin
   update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1
      inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
      CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionAffectedProducts] ([ca].[ProductActionId]) ip
      inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
   
   update cp set Price = pdai.DiscountPrice
   -- select cp.Price, pdai.DiscountPrice
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1000001
      inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId      
      inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
         and cp.CampaignId = ca.CampaignId
       inner join product.tPriceList pl on pl.PriceListId = cp.PriceListId and pl.CurrencyId = pdai.CurrencyId
   where ca.SortIndex = @Index
      and (cp.Price > pdai.DiscountPrice or cp.Price is null)

   set @Index = @Index + 1
end

delete cp
from #CampaignProduct cp 
where Price is null

update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
   

if object_id('tempdb..#BestCampaignPrice') is not null
    drop table #BestCampaignPrice

create table #BestCampaignPrice (
   ProductId int,
   PriceListId int,
   CampaignId int,
   Price decimal(16, 2)
)

insert into #BestCampaignPrice (ProductId, PriceListId, CampaignId, Price)
select cp.ProductId, cp.PriceListId, cp.CampaignId, cp.Price
from #CampaignProduct cp
   left outer join #CampaignProduct cp2 on cp2.PriceListId = cp.PriceListId
      and cp2.ProductId = cp.ProductId
      and (cp2.Price < cp.Price or cp2.Price = cp.Price and cp2.CampaignId > cp.CampaignId)      
where cp2.ProductId is null

drop table #Campaign
drop table #CampaignAction
drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int		
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from core.tChannel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title
	
	select hh.CommonName, ll.HYErpId, 
		--pli.ProductId, 
	   t.Title as Product,
	   pli.PriceIncludingVat as ListPrice, 
	   --c.CampaignId, 
	   c.Title as Campaign, bcp.Price as DiscountPrice, 
	   case when (bcp.Price is null or bcp.Price > pli.PriceIncludingVat) then pli.PriceIncludingVat else bcp.Price end as SitePrice,
	   cast(((1-(bcp.Price/pli.PriceIncludingVat)) * 100) as decimal (18,2)) as 'Total discount %'
	--into temp.CdonPriceNewCalc3
	from @PriceList pl
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		inner join product.tProduct t on t.ProductId = pli.ProductId
		left outer join #BestCampaignPrice bcp on bcp.PriceListId = pl.PriceListId
		   and bcp.ProductId = t.ProductId
		left outer join campaign.tCampaign c on c.CampaignId = bcp.CampaignId
		inner join core.tChannel hh on hh.ChannelId = pl.ChannelId
		inner join lekmer.tLekmerProduct ll on ll.ProductId = pli.ProductId
	where
		c.CampaignId is not null --
	order by
		pl.ChannelId --
   drop table #BestCampaignPrice
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [lekmer].[tSizeTableMediaProductRegistry]'
GO
ALTER TABLE [lekmer].[tSizeTableMediaProductRegistry] ADD CONSTRAINT [IX_tSizeTableMediaProductRegistry_SizeTableMediaId_ProductRegistryId] UNIQUE NONCLUSTERED  ([SizeTableMediaId], [ProductRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignLandingPageTranslation]'
GO
ALTER TABLE [campaignlek].[tCampaignLandingPageTranslation] ADD CONSTRAINT [FK_tCampaignLandingPageTranslation_tCampaignLandingPage] FOREIGN KEY ([CampaignLandingPageId]) REFERENCES [campaignlek].[tCampaignLandingPage] ([CampaignLandingPageId]) ON DELETE CASCADE
ALTER TABLE [campaignlek].[tCampaignLandingPageTranslation] ADD CONSTRAINT [FK_tCampaignLandingPageTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableMediaProductRegistry]'
GO
ALTER TABLE [lekmer].[tSizeTableMediaProductRegistry] ADD CONSTRAINT [FK_tSizeTableMediaProductRegistry_tSizeTableMedia] FOREIGN KEY ([SizeTableMediaId]) REFERENCES [lekmer].[tSizeTableMedia] ([SizeTableMediaId])
ALTER TABLE [lekmer].[tSizeTableMediaProductRegistry] ADD CONSTRAINT [FK_tSizeTableMediaProductRegistry_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignRegistryCampaignLandingPage]'
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaignLandingPage] ADD CONSTRAINT [FK_tCampaignRegistryCampaignLandingPage_tCampaignLandingPage] FOREIGN KEY ([CampaignLandingPageId]) REFERENCES [campaignlek].[tCampaignLandingPage] ([CampaignLandingPageId])
ALTER TABLE [campaignlek].[tCampaignRegistryCampaignLandingPage] ADD CONSTRAINT [FK_tCampaignRegistryCampaignLandingPage_tCampaignRegistry] FOREIGN KEY ([CampaignRegistryId]) REFERENCES [campaign].[tCampaignRegistry] ([CampaignRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignLandingPage]'
GO
ALTER TABLE [campaignlek].[tCampaignLandingPage] ADD CONSTRAINT [FK_tCampaignLandingPage_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableMedia]'
GO
ALTER TABLE [lekmer].[tSizeTableMedia] ADD CONSTRAINT [FK_tSizeTableMedia_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId]) ON DELETE CASCADE
ALTER TABLE [lekmer].[tSizeTableMedia] ADD CONSTRAINT [FK_tSizeTableMedia_tMedia] FOREIGN KEY ([MediaId]) REFERENCES [media].[tMedia] ([MediaId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
