SET IDENTITY_INSERT [productlek].[tStockRange] ON
INSERT [productlek].[tStockRange] ([StockRangeId], [CommonName], [StartValue], [EndValue]) VALUES (1, N'red', NULL, 10)
INSERT [productlek].[tStockRange] ([StockRangeId], [CommonName], [StartValue], [EndValue]) VALUES (2, N'yellow', 11, 50)
INSERT [productlek].[tStockRange] ([StockRangeId], [CommonName], [StartValue], [EndValue]) VALUES (3, N'green', 51, NULL)
SET IDENTITY_INSERT [productlek].[tStockRange] OFF
