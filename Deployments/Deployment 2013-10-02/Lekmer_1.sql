SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [campaignlek].[tCampaignRegistryCampaign]'
GO
CREATE TABLE [campaignlek].[tCampaignRegistryCampaign]
(
[CampaignRegistryId] [int] NOT NULL,
[CampaignId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignRegistryCampaign] on [campaignlek].[tCampaignRegistryCampaign]'
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaign] ADD CONSTRAINT [PK_tCampaignRegistryCampaign] PRIMARY KEY CLUSTERED  ([CampaignRegistryId], [CampaignId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tStockRange]'
GO
CREATE TABLE [productlek].[tStockRange]
(
[StockRangeId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StartValue] [int] NULL,
[EndValue] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tStockRange] on [productlek].[tStockRange]'
GO
ALTER TABLE [productlek].[tStockRange] ADD CONSTRAINT [PK_tStockRange] PRIMARY KEY CLUSTERED  ([StockRangeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignRegistryCampaign]'
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaign] ADD CONSTRAINT [FK_tCampaignRegistryCampaign_tCampaignRegistry] FOREIGN KEY ([CampaignRegistryId]) REFERENCES [campaign].[tCampaignRegistry] ([CampaignRegistryId])
ALTER TABLE [campaignlek].[tCampaignRegistryCampaign] ADD CONSTRAINT [FK_tCampaignRegistryCampaign_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
