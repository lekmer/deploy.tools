DECLARE @DateFrom DATE, @ChannelId INT
SET @DateFrom = '2014-10-01'
SET @ChannelId = 1 -- 2, 3, 4, 1000005

WHILE @DateFrom <= DATEADD(DAY, -1, GETDATE())
BEGIN
   INSERT INTO [statistics].[tTopListProduct] (
		[ProductId],
		[ChannelId],
		[Date],
		[PurchaseSum]
	)
	SELECT
		[p].[ProductId],
		@ChannelId,
		@DateFrom,
		SUM([oi].[Quantity] * [oi].[ActualPriceIncludingVat])
	FROM
		[product].[tProduct] p
		INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[ProductId] = [p].[ProductId]
		INNER JOIN [order].[tOrderItem] oi  ON [oi].[OrderItemId] = [oip].[OrderItemId]
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [oi].[OrderId]
	WHERE
		[p].[IsDeleted] = 0 AND
		[o].[ChannelId] = @ChannelId AND
		[o].[CreatedDate] >= @DateFrom AND
		[o].[CreatedDate] < DATEADD(DAY, 1, @DateFrom) AND
		[o].[OrderStatusId] = 4
	GROUP BY
		[p].[ProductId]
	
	SET @DateFrom = DATEADD(DAY, 1, @DateFrom)
END