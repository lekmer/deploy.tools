SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [statistics].[tTopListProduct]'
GO
CREATE TABLE [statistics].[tTopListProduct]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[Date] [datetime] NOT NULL,
[PurchaseSum] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tTopListProduct] on [statistics].[tTopListProduct]'
GO
ALTER TABLE [statistics].[tTopListProduct] ADD CONSTRAINT [PK_tTopListProduct] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [statistics].[pPopulateTopListProduct]'
GO
CREATE PROCEDURE [statistics].[pPopulateTopListProduct]
	@Date					DATE,
	@KeepOrderStatisticDays INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [statistics].[tTopListProduct]
	WHERE [Date] < DATEADD(DAY, -@KeepOrderStatisticDays, @Date)

	DECLARE @ChannelId INT
	DECLARE channel_cursor CURSOR FOR SELECT [ChannelId] FROM [core].[tChannel]
	OPEN channel_cursor
	FETCH NEXT FROM channel_cursor INTO @ChannelId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO [statistics].[tTopListProduct] (
			[ProductId],
			[ChannelId],
			[Date],
			[PurchaseSum]
		)
		SELECT
			[p].[ProductId],
			@ChannelId,
			@Date,
			SUM([oi].[Quantity] * [oi].[ActualPriceIncludingVat])
		FROM
			[product].[tProduct] p
			INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[ProductId] = [p].[ProductId]
			INNER JOIN [order].[tOrderItem] oi  ON [oi].[OrderItemId] = [oip].[OrderItemId]
			INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [oi].[OrderId]
		WHERE
			[p].[IsDeleted] = 0 AND
			[o].[ChannelId] = @ChannelId AND
			[o].[CreatedDate] >= @Date AND
			[o].[CreatedDate] < DATEADD(DAY, 1, @Date) AND
			[o].[OrderStatusId] = 4
		GROUP BY
			[p].[ProductId]		

		FETCH NEXT FROM channel_cursor INTO @ChannelId
	END
	CLOSE channel_cursor
	DEALLOCATE channel_cursor
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
