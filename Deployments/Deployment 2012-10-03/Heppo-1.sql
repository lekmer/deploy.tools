/*
Run this script on:

        (local).Heppo_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 028\DB\HeppoDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 03.10.2012 0:16:06

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [customerlek].[tAddress]'
GO
ALTER TABLE [customerlek].[tAddress] ADD
[Reference] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customerlek].[tCustomerInformation]'
GO
ALTER TABLE [customerlek].[tCustomerInformation] ADD
[IsCompany] [bit] NULL,
[AlternateAddressId] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockEsalesRecommend]'
GO
ALTER TABLE [lekmer].[tBlockEsalesRecommend] ADD
[FallbackPanelPath] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemGroupFixedDiscountAction]'
GO
CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountAction]
(
[CartActionId] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemGroupFixedDiscountAction] on [lekmer].[tCartItemGroupFixedDiscountAction]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountAction] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountAction] PRIMARY KEY CLUSTERED  ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemGroupFixedDiscountActionCurrency]'
GO
CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValue] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemGroupFixedDiscountActionCurrency] on [lekmer].[tCartItemGroupFixedDiscountActionCurrency]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionCurrency] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]'
GO
CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemGroupFixedDiscountActionExcludeBrand] on [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionExcludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]'
GO
CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemGroupFixedDiscountActionExcludeCategory] on [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionExcludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct]'
GO
CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemGroupFixedDiscountActionExcludeProduct] on [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionExcludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand]'
GO
CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemGroupFixedDiscountActionIncludeBrand] on [lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionIncludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory]'
GO
CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemGroupFixedDiscountActionIncludeCategory] on [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionIncludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct]'
GO
CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemGroupFixedDiscountActionIncludeProduct] on [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionIncludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD
[AlternateAddressId] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tRecommendedPrice]'
GO
CREATE TABLE [lekmer].[tRecommendedPrice]
(
[ProductId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[Price] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRecommendedPrice] on [lekmer].[tRecommendedPrice]'
GO
ALTER TABLE [lekmer].[tRecommendedPrice] ADD CONSTRAINT [PK_tRecommendedPrice] PRIMARY KEY CLUSTERED  ([ProductId], [ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[tOrderAddress]'
GO
ALTER TABLE [orderlek].[tOrderAddress] ADD
[Reference] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customerlek].[pAddressSave]'
GO

ALTER PROCEDURE [customerlek].[pAddressSave]
	@AddressId		INT,
	@CustomerId		INT,
	@Addressee		NVARCHAR(100),
	@StreetAddress	NVARCHAR(200),
	@StreetAddress2 NVARCHAR(200),
	@PostalCode		NVARCHAR(10),
	@City			NVARCHAR(100),
	@CountryId		INT,
	@PhoneNumber	NVARCHAR(20),
	@AddressTypeId	INT,
	@HouseNumber	NVARCHAR(50),
	@HouseExtension	NVARCHAR(100),
	@Reference		NVARCHAR(50)
AS	
BEGIN
	SET NOCOUNT ON
	
	--Check if customer exist
	IF NOT EXISTS ( 
					SELECT 
						1 
					FROM 
						[customer].[tCustomer] 
					WHERE 
						[tCustomer].[CustomerId] = @CustomerId 
					)
		RETURN -1
	
	UPDATE  [customer].[tAddress]
	SET 
		Addressee = @Addressee,
		StreetAddress = @StreetAddress,
		StreetAddress2 = @StreetAddress2,
		PostalCode = @PostalCode,
		City = @City,
		CountryId = @CountryId,
		PhoneNumber = @PhoneNumber,
		AddressTypeId = @AddressTypeId
	WHERE
		AddressId = @AddressId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customer].[tAddress]
		(
			CustomerId,
			Addressee,
			StreetAddress,
			StreetAddress2,
			PostalCode,
			City,
			CountryId,
			PhoneNumber,
			AddressTypeId
		)
		VALUES
		(
			@CustomerId,
			@Addressee,
			@StreetAddress,
			@StreetAddress2,
			@PostalCode,
			@City,
			@CountryId,
			@PhoneNumber,
			@AddressTypeId
		)
		SET	@AddressId = SCOPE_IDENTITY()
	END
	
	UPDATE  [customerlek].[tAddress]
	SET 
		HouseNumber = @HouseNumber,
		HouseExtension = @HouseExtension,
		Reference = @Reference
	WHERE
		AddressId = @AddressId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customerlek].[tAddress]
		(
			AddressId,
			CustomerId,
			HouseNumber,
			HouseExtension,
			Reference
		)
		VALUES
		(
			@AddressId,
			@CustomerId,
			@HouseNumber,
			@HouseExtension,
			@Reference
		)
	END

	RETURN @AddressId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customerlek].[pCustomerInformationSave]'
GO

ALTER PROCEDURE [customerlek].[pCustomerInformationSave]
	@CustomerId INT,
	@FirstName NVARCHAR(50),
	@LastName NVARCHAR(50),
	@CivicNumber NVARCHAR(50),
	@PhoneNumber NVARCHAR(50),
	@CellPhoneNumber NVARCHAR(50),
	@Email VARCHAR(320),
	@CreatedDate DATETIME,
	@DefaultBillingAddressId INT = NULL,
	@DefaultDeliveryAddressId INT = NULL,
	@GenderTypeId INT,
	@IsCompany BIT,
	@AlternateAddressId INT = NULL
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[customer].[tCustomerInformation]
	SET    
		FirstName = @FirstName, 
		LastName = @LastName,
		CivicNumber = @CivicNumber,
		PhoneNumber = @PhoneNumber,
		CellPhoneNumber = @CellPhoneNumber,
		Email = @Email,
		DefaultBillingAddressId = @DefaultBillingAddressId,
		DefaultDeliveryAddressId = @DefaultDeliveryAddressId
	WHERE 
		CustomerId = @CustomerId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO	[customer].[tCustomerInformation]
		(
			CustomerId,
			FirstName, 
			LastName,
			CivicNumber,
			PhoneNumber,
			CellPhoneNumber,
			Email,
			CreatedDate,
			DefaultBillingAddressId,
			DefaultDeliveryAddressId
		)
		VALUES
		(
			@CustomerId,
			@FirstName, 
			@LastName,
			@CivicNumber,
			@PhoneNumber,
			@CellPhoneNumber,
			@Email,
			@CreatedDate,
			@DefaultBillingAddressId,
			@DefaultDeliveryAddressId
		)
	END
	
	-- Save Lekmer part
	
	UPDATE
		[customerlek].[tCustomerInformation]
	SET    
		GenderTypeId = @GenderTypeId,
		IsCompany = @IsCompany,
		AlternateAddressId = @AlternateAddressId
	WHERE 
		CustomerId = @CustomerId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO	[customerlek].[tCustomerInformation]
		(
			CustomerId,
			GenderTypeId,
			IsCompany,
			AlternateAddressId
		)
		VALUES
		(
			@CustomerId,
			@GenderTypeId,
			@IsCompany,
			@AlternateAddressId
		)
	END

	RETURN @CustomerId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockEsalesRecommendSave]'
GO

ALTER PROCEDURE [lekmer].[pBlockEsalesRecommendSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@RecommendationType INT,
	@PanelPath NVARCHAR(MAX),
	@FallbackPanelPath NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tBlockEsalesRecommend]
	SET
		[ColumnCount]	= @ColumnCount,
		[RowCount]		= @RowCount,
		[EsalesRecommendationTypeId] = @RecommendationType,
		[PanelPath]		= @PanelPath,
		[FallbackPanelPath] = @FallbackPanelPath
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockEsalesRecommend]
	(
		[BlockId],
		[ColumnCount],
		[RowCount],
		[EsalesRecommendationTypeId],
		[PanelPath],
		[FallbackPanelPath]		
	)
	VALUES
	(
		@BlockId,
		@ColumnCount,
		@RowCount,
		@RecommendationType,
		@PanelPath,
		@FallbackPanelPath
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionCurrencyDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionCurrencyDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionCurrency]
	WHERE
		CartActionId = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionCurrencyInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionCurrencyInsert]
	@CartActionId	INT,
	@CurrencyId		INT,
	@MonetaryValue	DECIMAL(16,2)
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionCurrency] (
		CartActionId,
		CurrencyId,
		MonetaryValue
	)
	VALUES (
		@CartActionId,
		@CurrencyId,
		@MonetaryValue
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionDelete]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionDelete]
	@CartActionId INT
AS 
BEGIN
	DELETE FROM [lekmer].[tCartItemGroupFixedDiscountAction]
	WHERE CartActionId = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandDeleteAll]'
GO



CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]
	WHERE
		CartActionId = @CartActionId
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandInsert]
	@CartActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand] (
		CartActionId,
		BrandId
	)
	VALUES (
		@CartActionId,
		@BrandId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		CategoryId,
		IncludeSubcategories
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAllRecursive]'
GO


CREATE procedure [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAllRecursive]
	@CartActionId		int
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT CategoryId, IncludeSubcategories 
	FROM [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]
	WHERE CartActionId = @CartActionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT count(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryInsert]
	@CartActionId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory] (
		CartActionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES (
		@CartActionId,
		@CategoryId,
		@IncludeSubcategories
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandInsert]
	@CartActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand] (
		CartActionId,
		BrandId
	)
	VALUES (
		@CartActionId,
		@BrandId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		CategoryId,
		IncludeSubcategories
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAllRecursive]'
GO


CREATE procedure [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAllRecursive]
	@CartActionId		int
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT CategoryId, IncludeSubcategories 
	FROM [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory]
	WHERE CartActionId = @CartActionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT count(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryInsert]
	@CartActionId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory] (
		CartActionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES (
		@CartActionId,
		@CategoryId,
		@IncludeSubcategories
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionSave]'
GO

CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionSave]
	@CartActionId			INT,
	@IncludeAllProducts		BIT
AS
BEGIN
	UPDATE
		[lekmer].[tCartItemGroupFixedDiscountAction]
	SET
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [lekmer].[tCartItemGroupFixedDiscountAction] (
			CartActionId,
			IncludeAllProducts
		)
		VALUES (
			@CartActionId,
			@IncludeAllProducts
		)
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pOrderSave]'
GO
ALTER PROCEDURE [lekmer].[pOrderSave]
	@OrderId int,
	@PaymentCost decimal(16,2),
	@VoucherDiscount decimal(16,2),
	@CustomerIdentificationKey nchar(50),
	@AlternateAddressId int = null
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		PaymentCost = @PaymentCost,
		VoucherDiscount = @VoucherDiscount,
		CustomerIdentificationKey = @CustomerIdentificationKey,
		AlternateAddressId = @AlternateAddressId
	WHERE 
		OrderId = @OrderId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tLekmerOrder]
		(
			OrderId,
			PaymentCost,
			VoucherDiscount,
			CustomerIdentificationKey,
			AlternateAddressId
		)
		VALUES
		(
			@OrderId,
			@PaymentCost,
			@VoucherDiscount,
			@CustomerIdentificationKey,
			@AlternateAddressId
		)
	END
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pRecommendedPriceDeleteAllByProduct]'
GO
CREATE PROCEDURE [lekmer].[pRecommendedPriceDeleteAllByProduct]
	@ProductId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tRecommendedPrice]
	WHERE 
		[ProductId] = @ProductId
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vRecommendedPrice]'
GO

CREATE VIEW [lekmer].[vRecommendedPrice]
AS
SELECT     lekmer.tRecommendedPrice.*
FROM         lekmer.tRecommendedPrice

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pRecommendedPriceGetAllByProduct]'
GO
CREATE PROCEDURE [lekmer].[pRecommendedPriceGetAllByProduct]
	@ProductId	INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vRecommendedPrice]
	WHERE
		[ProductId] = @ProductId
END 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pRecommendedPriceSave]'
GO
CREATE PROCEDURE [lekmer].[pRecommendedPriceSave]
	@ProductId	INT,
	@ChannelId	INT,
	@Price		DECIMAL(16,2)
AS
BEGIN
	INSERT INTO [lekmer].[tRecommendedPrice] (
		ProductId,
		ChannelId,
		Price
	)
	VALUES(
		@ProductId,
		@ChannelId,
		@Price
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[pOrderAddressSave]'
GO

ALTER PROCEDURE [orderlek].[pOrderAddressSave]
	@OrderAddressId INT,
	@HouseNumber	NVARCHAR(50),
	@HouseExtension	NVARCHAR(100),
	@Reference		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[orderlek].[tOrderAddress]
	SET 
		HouseNumber = @HouseNumber,
		HouseExtension = @HouseExtension,
		Reference = @Reference
	WHERE	
		OrderAddressId = @OrderAddressId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [orderlek].[tOrderAddress]
		(
			OrderAddressId,
			HouseNumber,
			HouseExtension,
			Reference
		)
		VALUES
		(
			@OrderAddressId,
			@HouseNumber,
			@HouseExtension,
			@Reference
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customerlek].[vAddress]'
GO


ALTER VIEW [customerlek].[vAddress]
AS
	SELECT
		AddressId AS 'Address.AddressId',
		CustomerId AS 'Address.CustomerId',
		HouseNumber AS 'Address.HouseNumber',
		HouseExtension AS 'Address.HouseExtension',
		Reference AS 'Address.Reference'
	FROM
		[customerlek].[tAddress]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customerlek].[vCustomerInformation]'
GO



ALTER VIEW [customerlek].[vCustomerInformation]
AS
	SELECT
		CustomerId AS 'CustomerInformation.CustomerId',
		GenderTypeId AS 'CustomerInformation.GenderTypeId',
		IsCompany AS 'CustomerInformation.IsCompany',
		AlternateAddressId AS 'CustomerInformation.AlternateAddressId'
	FROM
		[customerlek].[tCustomerInformation]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[vOrderAddress]'
GO


ALTER VIEW [orderlek].[vOrderAddress]
AS
	SELECT
		[OrderAddressId] AS 'OrderAddress.OrderAddressId',
		[HouseNumber] AS 'OrderAddress.HouseNumber',
		[HouseExtension] AS 'OrderAddress.HouseExtension',
		[Reference] AS 'OrderAddress.Reference'
	FROM
		[orderlek].[tOrderAddress]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomAddress]'
GO


ALTER VIEW [customer].[vCustomAddress]
AS
	SELECT
		a.*,
		la.[Address.HouseNumber],
		la.[Address.HouseExtension],
		la.[Address.Reference]
	FROM
		[customer].[vAddress] a
		LEFT OUTER JOIN [customerlek].[vAddress] la 
			ON la.[Address.AddressId] = a.[Address.AddressId]
			AND la.[Address.CustomerId] = a.[Address.CustomerId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCartItemGroupFixedDiscountActionCurrency]'
GO


CREATE VIEW [lekmer].[vCartItemGroupFixedDiscountActionCurrency]
AS
	SELECT
		ci.[CartActionId]	AS 'CartItemGroupFixedDiscountActionCurrency.CartActionId',
		ci.[CurrencyId]		AS 'CartItemGroupFixedDiscountActionCurrency.CurrencyId',
		ci.[MonetaryValue]	AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[lekmer].[tCartItemGroupFixedDiscountActionCurrency] ci
		INNER JOIN [core].[vCustomCurrency] c ON ci.[CurrencyId] = c.[Currency.Id]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionCurrencyGetByAction]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemGroupFixedDiscountActionCurrency]
	WHERE 
		[CartItemGroupFixedDiscountActionCurrency.CartActionId] = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductGetIdAllSecure]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductGetIdAllSecure]
	@CartActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] c
		INNER JOIN [product].[tProduct] p ON c.[ProductId] = p.[ProductId]
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[IsDeleted] = 0
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductGetIdAllSecure]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductGetIdAllSecure]
	@CartActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] c
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = c.[ProductId]
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[IsDeleted] = 0
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrderAddress]'
GO


ALTER VIEW [order].[vCustomOrderAddress]
AS
	SELECT
		oa.*,
		loa.[OrderAddress.HouseNumber],
		loa.[OrderAddress.HouseExtension],
		loa.[OrderAddress.Reference]
	FROM
		[order].[vOrderAddress] oa
		LEFT OUTER JOIN [orderlek].[vOrderAddress] loa
			ON loa.[OrderAddress.OrderAddressId] = oa.[OrderAddress.OrderAddressId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrder]'
GO




ALTER VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		lo.[PaymentCost] AS [Lekmer.PaymentCost],
		lo.[VoucherDiscount] AS [Lekmer.VoucherDiscount],
		lo.[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey],
		lo.[FeedbackToken] AS [Lekmer.FeedbackToken],
		lo.[AlternateAddressId] AS [Lekmer.AlternateAddressId]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomCustomer]'
GO




ALTER VIEW [customer].[vCustomCustomer]
AS
	SELECT
		C.*,
		CI.[CustomerInformation.GenderTypeId],
		CI.[CustomerInformation.IsCompany],
		CI.[CustomerInformation.AlternateAddressId],
		BA.[Address.HouseNumber] AS 'BillingAddress.HouseNumber',
		BA.[Address.HouseExtension] AS 'BillingAddress.HouseExtension',
		BA.[Address.Reference] AS 'BillingAddress.Reference',
		DA.[Address.HouseNumber] AS 'DeliveryAddress.HouseNumber',
		DA.[Address.HouseExtension] AS 'DeliveryAddress.HouseExtension',
		DA.[Address.Reference] AS 'DeliveryAddress.Reference',
		AA.[Address.AddressId] AS 'AlternateAddress.AddressId',
		AA.[Address.CustomerId] AS  'AlternateAddress.CustomerId',
		AA.[Address.AddressTypeId] AS 'AlternateAddress.AddressTypeId',
		AA.[Address.Addressee] AS 'AlternateAddress.Addressee',
		AA.[Address.StreetAddress] AS 'AlternateAddress.StreetAddress',
		AA.[Address.StreetAddress2] AS 'AlternateAddress.StreetAddress2',
		AA.[Address.PostalCode] AS 'AlternateAddress.PostalCode',
		AA.[Address.City]  AS 'AlternateAddress.City',
		AA.[Address.CountryId] AS 'AlternateAddress.CountryId',
		AA.[Address.PhoneNumber] AS 'AlternateAddress.PhoneNumber',
		AA.[Address.HouseNumber] AS 'AlternateAddress.HouseNumber',
		AA.[Address.HouseExtension] AS 'AlternateAddress.HouseExtension',
		AA.[Address.Reference] AS 'AlternateAddress.Reference'
	FROM
		[customer].[vCustomer] C
		LEFT JOIN [customerlek].vCustomerInformation CI ON CI.[CustomerInformation.CustomerId] = C.[CustomerInformation.InformationId]
		LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = C.[CustomerInformation.DefaultBillingAddressId]
		LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = C.[CustomerInformation.DefaultDeliveryAddressId]
		LEFT JOIN [customer].vCustomAddress AA ON AA.[Address.AddressId] = CI.[CustomerInformation.AlternateAddressId]
		


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomCustomerSecure]'
GO



ALTER VIEW [customer].[vCustomCustomerSecure]
AS
	SELECT
		C.*,
		CI.[CustomerInformation.GenderTypeId],
		CI.[CustomerInformation.IsCompany],
		CI.[CustomerInformation.AlternateAddressId],
		BA.[Address.HouseNumber] AS 'BillingAddress.HouseNumber',
		BA.[Address.HouseExtension] AS 'BillingAddress.HouseExtension',
		BA.[Address.Reference] AS 'BillingAddress.Reference',
		DA.[Address.HouseNumber] AS 'DeliveryAddress.HouseNumber',
		DA.[Address.HouseExtension] AS  'DeliveryAddress.HouseExtension',
		DA.[Address.Reference] AS 'DeliveryAddress.Reference',
		AA.[Address.AddressId] AS 'AlternateAddress.AddressId',
		AA.[Address.CustomerId] AS  'AlternateAddress.CustomerId',
		AA.[Address.AddressTypeId] AS 'AlternateAddress.AddressTypeId',
		AA.[Address.Addressee] AS 'AlternateAddress.Addressee',
		AA.[Address.StreetAddress] AS 'AlternateAddress.StreetAddress',
		AA.[Address.StreetAddress2] AS 'AlternateAddress.StreetAddress2',
		AA.[Address.PostalCode] AS 'AlternateAddress.PostalCode',
		AA.[Address.City]  AS 'AlternateAddress.City',
		AA.[Address.CountryId] AS 'AlternateAddress.CountryId',
		AA.[Address.PhoneNumber] AS 'AlternateAddress.PhoneNumber',
		AA.[Address.HouseNumber] AS 'AlternateAddress.HouseNumber',
		AA.[Address.HouseExtension] AS 'AlternateAddress.HouseExtension',
		AA.[Address.Reference] AS 'AlternateAddress.Reference'
	FROM
		[customer].[vCustomerSecure] C
		LEFT JOIN [customerlek].vCustomerInformation CI ON CI.[CustomerInformation.CustomerId] = C.[CustomerInformation.InformationId]
		LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = C.[CustomerInformation.DefaultBillingAddressId]
		LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = C.[CustomerInformation.DefaultDeliveryAddressId]
		LEFT JOIN [customer].vCustomAddress AA ON AA.[Address.AddressId] = CI.[CustomerInformation.AlternateAddressId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockEsalesRecommend]'
GO

ALTER VIEW [lekmer].[vBlockEsalesRecommend]
AS
	SELECT
		b.*,
		ber.[ColumnCount] AS 'BlockEsalesRecommend.ColumnCount',
		ber.[RowCount] AS 'BlockEsalesRecommend.RowCount',
		ber.[EsalesRecommendationTypeId] AS 'BlockEsalesRecommend.EsalesRecommendationTypeId',
		ber.[PanelPath] AS 'BlockEsalesRecommend.PanelPath',
		ber.[FallbackPanelPath] AS 'BlockEsalesRecommend.FallbackPanelPath'
	FROM
		[lekmer].[tBlockEsalesRecommend] ber
		INNER JOIN [sitestructure].[vCustomBlock] b ON b.[Block.BlockId] = ber.[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockEsalesRecommendSecure]'
GO

ALTER VIEW [lekmer].[vBlockEsalesRecommendSecure]
AS
	SELECT
		b.*,
		ber.[ColumnCount] AS 'BlockEsalesRecommend.ColumnCount',
		ber.[RowCount] AS 'BlockEsalesRecommend.RowCount',
		ber.[EsalesRecommendationTypeId] AS 'BlockEsalesRecommend.EsalesRecommendationTypeId',
		ber.[PanelPath] AS 'BlockEsalesRecommend.PanelPath',
		ber.[FallbackPanelPath] AS 'BlockEsalesRecommend.FallbackPanelPath'
	FROM
		[lekmer].[tBlockEsalesRecommend] ber
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON b.[Block.BlockId] = ber.[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCartItemGroupFixedDiscountAction]'
GO



CREATE VIEW [lekmer].[vCartItemGroupFixedDiscountAction]
AS
	SELECT
		cid.[CartActionId]				AS 'CartItemGroupFixedDiscountAction.CartActionId',
		cid.[IncludeAllProducts]		AS 'CartItemGroupFixedDiscountAction.IncludeAllProducts',
		ca.*
	FROM
		[lekmer].[tCartItemGroupFixedDiscountAction] cid
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = cid.[CartActionId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionGetById]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemGroupFixedDiscountAction]
	WHERE
		[CartItemGroupFixedDiscountAction.CartActionId] = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProduct]'
GO







ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@CartActionId		INT
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] c
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = c.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[Product.ChannelId] = @ChannelId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@CartActionId		INT
AS
BEGIN
	SELECT
		p.[Product.Id]
	FROM
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] c
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = c.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[Product.ChannelId] = @ChannelId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductWithoutStatusFilter]'
GO


ALTER VIEW [lekmer].[vCustomProductWithoutStatusFilter]
AS 
	SELECT
		p.*,
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.[ParentContentNodeId] AS 'Product.ParentContentNodeId',
		i.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductWithoutStatusFilter] p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] AS ssmc ON ssmc.[ChannelId] = c.[Channel.Id]
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON p.[Product.Id] = pssr.[ProductId] AND ssmc.[SiteStructureRegistryId] = pssr.[SiteStructureRegistryId]
		LEFT JOIN [media].[vCustomImage] AS i ON i.[Image.MediaId] = p.[Product.MediaId] AND i.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductView]'
GO



ALTER VIEW [product].[vCustomProductView]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductViewWithoutStatusFilter]'
GO


ALTER VIEW [lekmer].[vCustomProductViewWithoutStatusFilter]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductViewWithoutStatusFilter] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO





ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId',
		NULL AS 'Lekmer.RecommendedPrice'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customerlek].[tCustomerInformation]'
GO
ALTER TABLE [customerlek].[tCustomerInformation] ADD
CONSTRAINT [FK_tCustomerInformation_tAddress] FOREIGN KEY ([CustomerId], [AlternateAddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemGroupFixedDiscountAction]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountAction] ADD
CONSTRAINT [FK_tCartItemGroupFixedDiscountAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemGroupFixedDiscountActionCurrency]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionCurrency] ADD
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionCurrency_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand] ADD
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeBrand_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory] ADD
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeCategory_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] ADD
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeProduct_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand] ADD
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionIncludeBrand_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory] ADD
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionIncludeCategory_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] ADD
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionIncludeProduct_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemGroupFixedDiscountActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD
CONSTRAINT [FK_tLekmerOrder_tOrderAddress] FOREIGN KEY ([AlternateAddressId]) REFERENCES [order].[tOrderAddress] ([OrderAddressId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tRecommendedPrice]'
GO
ALTER TABLE [lekmer].[tRecommendedPrice] ADD
CONSTRAINT [FK_tRecommendedPrice_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE,
CONSTRAINT [FK_tRecommendedPrice_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
