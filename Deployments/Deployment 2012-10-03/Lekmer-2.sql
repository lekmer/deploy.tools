/*
Run this script on:

(local).Lekmer_live – this database will be modified

to synchronize its data with:

D:\Projects\lmheppo\Scensum\trunk\Release\Release 028\DB\LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 9.0.0 from Red Gate Software Ltd at 03.10.2012 0:21:33

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraints from [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tEntity]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tFunctionType]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]

-- Drop constraint FK_tModelSetting_tModel from [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModel]

-- Add row to [campaign].[tCartActionType]
SET IDENTITY_INSERT [campaign].[tCartActionType] ON
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000010, N'Cart item group fixed discount', N'CartItemGroupFixedDiscount')
SET IDENTITY_INSERT [campaign].[tCartActionType] OFF

-- Add rows to [sitestructure].[tBlockType]
INSERT INTO [sitestructure].[tBlockType] ([BlockTypeId], [Title], [CommonName], [Ordinal], [AvailableForAllPageTypes]) VALUES (1000034, N'Skrill Response', N'SkrillResponse', 0, 1)
INSERT INTO [sitestructure].[tBlockType] ([BlockTypeId], [Title], [CommonName], [Ordinal], [AvailableForAllPageTypes]) VALUES (1000035, N'Skrill Response Successful', N'SkrillResponseSuccessful', 0, 1)
INSERT INTO [sitestructure].[tBlockType] ([BlockTypeId], [Title], [CommonName], [Ordinal], [AvailableForAllPageTypes]) VALUES (1000036, N'Skrill Response Unsuccessful', N'SkrillResponseUnsuccessful', 0, 1)
-- Operation applied to 3 rows out of 3

-- Add row to [template].[tEntity]
SET IDENTITY_INSERT [template].[tEntity] ON
INSERT INTO [template].[tEntity] ([EntityId], [CommonName], [Title]) VALUES (1000013, N'Flag', N'Flag')
SET IDENTITY_INSERT [template].[tEntity] OFF

-- Add rows to [template].[tModel]
SET IDENTITY_INSERT [template].[tModel] ON
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName], [DefaultTemplateId]) VALUES (1000051, 23, N'Skrill Response', N'SkrillResponse', NULL)
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName], [DefaultTemplateId]) VALUES (1000052, 23, N'Skrill response successful', N'BlockSkrillResponseSuccessful', NULL)
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName], [DefaultTemplateId]) VALUES (1000053, 23, N'Skrill response unsuccessful', N'BlockSkrillResponseUnsuccessful', NULL)
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName], [DefaultTemplateId]) VALUES (1000055, 184, N'Campaign flags', N'CampaignFlags', NULL)
SET IDENTITY_INSERT [template].[tModel] OFF
-- Operation applied to 4 rows out of 4

-- Add rows to [template].[tEntityFunction]
SET IDENTITY_INSERT [template].[tEntityFunction] ON
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000157, 1000013, 1, N'Flag.Title', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000158, 1000013, 1, N'Flag.Class', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000159, 5, 1, N'Product.CampaignFlags', N'Product campaign flags')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000160, 1, 1, N'Product.CampaignFlags("TEMPLATE_ID")', N'Product campaign flags, using custom "Campaign flags" template. Example: [Product.CampaignFlags("100001")]')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000161, 5, 1, N'Product.CampaignFlags("TEMPLATE_ID")', N'Product campaign flags, using custom "Campaign flags" template. Example: [Product.CampaignFlags("100001")]')
SET IDENTITY_INSERT [template].[tEntityFunction] OFF
-- Operation applied to 5 rows out of 5

-- Add rows to [template].[tModelFragmentRegion]
SET IDENTITY_INSERT [template].[tModelFragmentRegion] ON
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000181, 1000051, N'Head', 10)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000182, 1000051, N'Content', 20)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000183, 1000052, N'Head', 10)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000184, 1000052, N'Content', 20)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000185, 1000053, N'Head', 10)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000186, 1000053, N'Content', 20)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000191, 1000055, N'Flag', 10)
SET IDENTITY_INSERT [template].[tModelFragmentRegion] OFF
-- Operation applied to 7 rows out of 7

-- Add rows to [template].[tModelFragment]
SET IDENTITY_INSERT [template].[tModelFragment] ON
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001198, 1000181, N'Head', N'Head', 1000051, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001199, 1000182, N'Content', N'Content', 1000051, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001202, 1000183, N'Head', N'Head', 1000052, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001203, 1000184, N'Content', N'Content', 1000052, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001204, 1000185, N'Head', N'Head', 1000053, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001205, 1000186, N'Content', N'Content', 1000053, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001213, 1000191, N'Flag list', N'FlagList', 1000055, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001216, 1000191, N'Flag', N'Flag', 1000055, 30, 10)
SET IDENTITY_INSERT [template].[tModelFragment] OFF
-- Operation applied to 8 rows out of 8

-- Add row to [template].[tModelFragmentEntity]
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001216, 1000013)

-- Add rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000684, 1001199, 1, N'ResponseRedirectUrl', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000735, 1001213, 1, N'Iterate:Flag', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000736, 1001216, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000737, 1001216, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000738, 1000061, 1, N'Cart.VatSummary', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000739, 1000062, 1, N'CartItem.VatSummary', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000740, 1001179, 1, N'Cart.VatSummaryWithDecimalDigits', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000741, 1001179, 1, N'Cart.VatSummaryWithoutDecimalDigits', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000742, 1001180, 1, N'CartItem.VatSummaryWithDecimalDigits', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000743, 1001180, 1, N'CartItem.VatSummaryWithoutDecimalDigits', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000744, 35, 1, N'Cart.VatSummary', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000745, 37, 1, N'CartItem.VatSummary', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
-- Operation applied to 12 rows out of 12

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])

-- Add constraint FK_tModelSetting_tModel to [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] WITH CHECK ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
COMMIT TRANSACTION
GO
