SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [lekmer].[pLekmerCartUpdateCreatedDate]'
GO
DROP PROCEDURE [lekmer].[pLekmerCartUpdateCreatedDate]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD
[IPAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tSavedCart]'
GO
CREATE TABLE [orderlek].[tSavedCart]
(
[SavedCartId] [int] NOT NULL IDENTITY(1, 1),
[CartId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[IsReminderSent] [bit] NOT NULL CONSTRAINT [DF_tSavedCart_IsReminderSent] DEFAULT ((0)),
[IsNeedReminder] [bit] NOT NULL CONSTRAINT [DF_tSavedCart_IsNeedReminder] DEFAULT ((1))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSavedCart] on [orderlek].[tSavedCart]'
GO
ALTER TABLE [orderlek].[tSavedCart] ADD CONSTRAINT [PK_tSavedCart] PRIMARY KEY CLUSTERED  ([SavedCartId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[tCart]'
GO
ALTER TABLE [order].[tCart] ADD
[UpdatedDate] [datetime] NULL,
[IPAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[RemovedItemsCount] [int] NOT NULL CONSTRAINT [DF_tCart_RemovedItemsCount] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tMaksuturvaPendingOrder]'
GO
CREATE TABLE [orderlek].[tMaksuturvaPendingOrder]
(
[MaksuturvaPendingOrderId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[OrderId] [int] NOT NULL,
[StatusId] [int] NOT NULL,
[FirstAttempt] [datetime] NULL,
[LastAttempt] [datetime] NULL,
[NextAttempt] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tMaksuturvaPendingOrder] on [orderlek].[tMaksuturvaPendingOrder]'
GO
ALTER TABLE [orderlek].[tMaksuturvaPendingOrder] ADD CONSTRAINT [PK_tMaksuturvaPendingOrder] PRIMARY KEY CLUSTERED  ([MaksuturvaPendingOrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vMaksuturvaPendingOrder]'
GO
CREATE VIEW [orderlek].[vMaksuturvaPendingOrder]
AS
SELECT
	[po].[MaksuturvaPendingOrderId] AS 'MaksuturvaPendingOrder.MaksuturvaPendingOrderId',
	[po].[ChannelId] AS 'MaksuturvaPendingOrder.ChannelId',
	[po].[OrderId] AS 'MaksuturvaPendingOrder.OrderId',
	[po].[StatusId] AS 'MaksuturvaPendingOrder.StatusId',
	[po].[FirstAttempt] AS 'MaksuturvaPendingOrder.FirstAttempt',
	[po].[LastAttempt] AS 'MaksuturvaPendingOrder.LastAttempt',
	[po].[NextAttempt] AS 'MaksuturvaPendingOrder.NextAttempt'
FROM
	[orderlek].[tMaksuturvaPendingOrder] po
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pMaksuturvaPendingOrderGetPendingOrdersForStatusCheck]'
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderGetPendingOrdersForStatusCheck]
	@CheckToDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[po].*
	FROM
		[orderlek].[vMaksuturvaPendingOrder] po
	WHERE
		[po].[MaksuturvaPendingOrder.NextAttempt] <= @CheckToDate
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pMaksuturvaPendingOrderGet]'
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderGet]
	@OrderId INT
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[po].*
	FROM
		[orderlek].[vMaksuturvaPendingOrder] po
	WHERE
		[po].[MaksuturvaPendingOrder.OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pMaksuturvaPendingOrderUpdate]'
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderUpdate]
	@ChannelId					INT,
	@OrderId					INT,
	@StatusId					INT,
	@FirstAttempt				DATETIME = NULL,
	@LastAttempt				DATETIME = NULL,
	@NextAttempt				DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tMaksuturvaPendingOrder]
	SET	
		[ChannelId] = @ChannelId,
		[StatusId] = @StatusId,
		[FirstAttempt] = @FirstAttempt,
		[LastAttempt] = @LastAttempt,
		[NextAttempt] = @NextAttempt
	WHERE
		[OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pLekmerOrderGetByEmailAndDate]'
GO
CREATE PROCEDURE [orderlek].[pLekmerOrderGetByEmailAndDate]
	@Email VARCHAR(320),
	@Date DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.CreatedDate] >= @Date
		AND [Order.Email] = @Email
		AND [OrderStatus.CommonName] = 'OrderInHY'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomCartItem]'
GO

ALTER VIEW [order].[vCustomCartItem]
AS
	SELECT
		c.*,
		l.SizeId AS [LekmerCartItem.SizeId],
		l.ErpId AS [LekmerCartItem.ErpId],
		l.IsAffectedByCampaign AS [LekmerCartItem.IsAffectedByCampaign],
		[l].[IPAddress] AS  [LekmerCartItem.IPAddress]
	FROM
		[order].[vCartItem] c
		INNER JOIN [lekmer].[tLekmerCartItem] l ON c.[CartItem.CartItemId] = l.[CartItemId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pMaksuturvaPendingOrderInsert]'
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderInsert]
	@ChannelId		INT,
	@OrderId		INT,
	@StatusId		INT,
	@FirstAttempt	DATETIME = NULL,
	@LastAttempt	DATETIME = NULL,
	@NextAttempt	DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tMaksuturvaPendingOrder] (
		[ChannelId],
		[OrderId],
		[StatusId],
		[FirstAttempt],
		[LastAttempt],
		[NextAttempt]
	)
	VALUES (
		@ChannelId,
		@OrderId,
		@StatusId,
		@FirstAttempt,
		@LastAttempt,
		@NextAttempt
	)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCart]'
GO

ALTER VIEW [order].[vCart]
AS
SELECT	
	[CartId] AS 'Cart.CartId',
	[CartGuid] AS 'Cart.CartGuid',
	[CreatedDate] AS 'Cart.CreatedDate',
	[UpdatedDate] AS 'Cart.UpdatedDate',
	[IPAddress] AS 'Cart.IPAddress'
FROM
	[order].tCart

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pSavedCartUpdate]'
GO
CREATE PROCEDURE [orderlek].[pSavedCartUpdate]
	@CartId			INT,
	@IsReminderSent	BIT,
	@IsNeedReminder	BIT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tSavedCart]
	SET
		[IsReminderSent] = @IsReminderSent,
		[IsNeedReminder] = @IsNeedReminder
	WHERE
		[CartId] = @CartId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pMaksuturvaPendingOrderDelete]'
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderDelete]
	@OrderId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DELETE [orderlek].[tMaksuturvaPendingOrder]
	WHERE [OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pLekmerCartItemSave]'
GO
ALTER PROCEDURE [lekmer].[pLekmerCartItemSave]
	@CartItemId 			INT,
	@ProductId				INT,
	@SizeId					INT,
	@ErpId					VARCHAR(50),
	@IsAffectedByCampaign	BIT,
	@IPAddress				VARCHAR(50)
AS
BEGIN
      SET NOCOUNT ON

      UPDATE
            [lekmer].[tLekmerCartItem]
      SET
            SizeId = @SizeId,
            ErpId = @ErpId,
            IsAffectedByCampaign = IsAffectedByCampaign
      WHERE            
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO [lekmer].[tLekmerCartItem]
            (
				  CartItemId,
                  ProductId,
                  SizeId,
                  ErpId,
                  IsAffectedByCampaign,
                  IPAddress
            )
            VALUES
            (
				  @CartItemId,
                  @ProductId,
                  @SizeId,
                  @ErpId,
                  @IsAffectedByCampaign,
                  @IPAddress
            )
      END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[pCartDeleteExpiredItems]'
GO
ALTER PROCEDURE [orderlek].[pCartDeleteExpiredItems]
	@LifeCycleDays INT
AS
BEGIN
	SET NOCOUNT ON;

	-- Protection of Fools
	IF @LifeCycleDays < 30
		SET @LifeCycleDays = 30
	
	DECLARE	@ExpirationDate DATETIME = DATEADD(DAY, -@LifeCycleDays, DATEDIFF(DAY, 0, GETDATE()))
	DECLARE	@BatchSize INT = 1000

	IF EXISTS ( SELECT * FROM tempdb..sysobjects WHERE id = OBJECT_ID('tempdb..#DeleteItems') ) 
		DROP TABLE #DeleteItems

	CREATE TABLE #DeleteItems
	(
	  CartId INT,
	  PRIMARY KEY ( CartId )
	)

	WHILE 1 = 1
	BEGIN

		INSERT	INTO [#DeleteItems]
				SELECT TOP ( @BatchSize ) c.[CartId]
				FROM [order].[tCart] c
				WHERE
					([c].[UpdatedDate] IS NULL AND [c].[CreatedDate] < @ExpirationDate)
					OR
					([c].[UpdatedDate] IS NOT NULL AND [c].[UpdatedDate] < @ExpirationDate)
					
		IF @@ROWCOUNT = 0
			BREAK

		DELETE cipe --tCartItemPackageElement
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			LEFT OUTER JOIN [orderlek].[tCartItemPackageElement] cipe ON [cipe].[CartItemId] = [ci].[CartItemId]

		DELETE lci --tLekmerCartItem
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			LEFT OUTER JOIN [lekmer].[tLekmerCartItem] lci ON [lci].[CartItemId] = [ci].[CartItemId]
			
		DELETE ci --tCartItem
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			
		DELETE cio --CartItemOption
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [campaignlek].[tCartItemOption] cio ON [cio].[CartId] = [di].[CartId]
			
		DELETE c --tCart
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			
		TRUNCATE TABLE [#DeleteItems]
	END

	DROP TABLE [#DeleteItems]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pCartSave]'
GO
ALTER PROCEDURE [order].[pCartSave]
	@CartGuid		UNIQUEIDENTIFIER,
	@CreatedDate	DATETIME,
	@IPAddress		VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [order].tCart
	(
		[CartGuid],
		[CreatedDate],
		[IPAddress]
	)
	VALUES
	(
		@CartGuid,
		@CreatedDate,
		@IPAddress
	)
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pSavedCartGetForReminder]'
GO
CREATE PROCEDURE [orderlek].[pSavedCartGetForReminder] 
	@SavedCartReminderInDays	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @CurrentDate AS DATETIME 
	SET @CurrentDate = GETDATE()
	
	DECLARE @Date AS DATETIME
	SET @Date = DATEADD(DAY, @SavedCartReminderInDays*(-1), @CurrentDate)
	
	SELECT	
		[sc].*,
		[cc].[Cart.CartGuid] AS 'CartGuid'
	FROM
		[orderlek].[tSavedCart] sc
		INNER JOIN [order].[vCustomCart] cc ON [cc].[Cart.CartId] = [sc].[CartId]
	WHERE
		[sc].[IsReminderSent] = 0
		AND [sc].[IsNeedReminder] = 1
		AND	NOT EXISTS (SELECT 1 FROM [lekmer].[tNewsletterUnsubscriber] nu WHERE [nu].[Email] = [sc].[Email])
		AND [cc].[Cart.CreatedDate] < @Date
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCartGetByIdAndCartGuid]'
GO
CREATE PROCEDURE [orderlek].[pCartGetByIdAndCartGuid] 
	@CartId		INT,
	@CartGuid	UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON
	SELECT	
		*
	FROM
		[order].[vCustomCart]
	WHERE
		[Cart.CartId] = @CartId
		AND [Cart.CartGuid] = @CartGuid
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pSavedCartSave]'
GO
CREATE PROCEDURE [orderlek].[pSavedCartSave]
	@CartId		INT,
	@ChannelId	INT,
	@Email		VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [orderlek].[tSavedCart] (
		[CartId],
		[ChannelId],
		[Email]
	)
	VALUES (
		@CartId,
		@ChannelId,
		@Email
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pLekmerCartUpdate]'
GO
CREATE PROCEDURE [lekmer].[pLekmerCartUpdate]
	@CartGuid			UNIQUEIDENTIFIER,
	@UpdatedDate		DATETIME,
	@RemovedItemsCount	INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[order].tCart
	SET
		[UpdatedDate] = @UpdatedDate,
		[RemovedItemsCount] = [RemovedItemsCount] + @RemovedItemsCount
	WHERE
		[CartGuid] = @CartGuid
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
