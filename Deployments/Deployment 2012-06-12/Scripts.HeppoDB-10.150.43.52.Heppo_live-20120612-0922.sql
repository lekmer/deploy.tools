/*
Run this script on:

        10.150.43.52.Heppo_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 015\DB\HeppoDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 12.06.2012 9:22:06

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [lekmer].[pDiscountGetAllByProduct]'
GO
DROP PROCEDURE [lekmer].[pDiscountGetAllByProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCartItemOption]'
GO
CREATE TABLE [campaignlek].[tCartItemOption]
(
[CartItemOptionId] [int] NOT NULL IDENTITY(1, 1),
[CartId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Quantity] [int] NOT NULL CONSTRAINT [DF_tCartItemOption_Quantity] DEFAULT ((1)),
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemOption] on [campaignlek].[tCartItemOption]'
GO
ALTER TABLE [campaignlek].[tCartItemOption] ADD CONSTRAINT [PK_tCartItemOption] PRIMARY KEY CLUSTERED  ([CartItemOptionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tProductAutoFreeCartAction]'
GO
CREATE TABLE [campaignlek].[tProductAutoFreeCartAction]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Quantity] [int] NOT NULL CONSTRAINT [DF_tProductAutoFreeCartAction_Quantity] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductAutoFreeCartAction] on [campaignlek].[tProductAutoFreeCartAction]'
GO
ALTER TABLE [campaignlek].[tProductAutoFreeCartAction] ADD CONSTRAINT [PK_tProductAutoFreeCartAction] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD
[IsAffectedByCampaign] [bit] NOT NULL CONSTRAINT [DF_tLekmerCartItem_IsAffectedByCampaign] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCartItemDiscountCartAction]'
GO
CREATE TABLE [campaignlek].[tCartItemDiscountCartAction]
(
[CartActionId] [int] NOT NULL,
[Quantity] [int] NOT NULL,
[ApplyForCheapest] [bit] NOT NULL,
[IsPercentageDiscount] [bit] NOT NULL,
[PercentageDiscountAmount] [decimal] (16, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemDiscountCartAction] on [campaignlek].[tCartItemDiscountCartAction]'
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartAction] ADD CONSTRAINT [PK_tCartItemDiscountCartAction] PRIMARY KEY CLUSTERED  ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCartItemDiscountCartActionCurrency]'
GO
CREATE TABLE [campaignlek].[tCartItemDiscountCartActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValue] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemDiscountCartActionCurrency] on [campaignlek].[tCartItemDiscountCartActionCurrency]'
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionCurrency] ADD CONSTRAINT [PK_tCartItemDiscountCartActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCustomerGroupCondition]'
GO
CREATE TABLE [campaignlek].[tCustomerGroupCondition]
(
[ConditionId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCustomerGroupCondition] on [campaignlek].[tCustomerGroupCondition]'
GO
ALTER TABLE [campaignlek].[tCustomerGroupCondition] ADD CONSTRAINT [PK_tCustomerGroupCondition] PRIMARY KEY CLUSTERED  ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCustomerGroupConditionExclude]'
GO
CREATE TABLE [campaignlek].[tCustomerGroupConditionExclude]
(
[ConditionId] [int] NOT NULL,
[CustomerGroupId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCustomerGroupConditionExclude] on [campaignlek].[tCustomerGroupConditionExclude]'
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionExclude] ADD CONSTRAINT [PK_tCustomerGroupConditionExclude] PRIMARY KEY CLUSTERED  ([ConditionId], [CustomerGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCustomerGroupConditionInclude]'
GO
CREATE TABLE [campaignlek].[tCustomerGroupConditionInclude]
(
[ConditionId] [int] NOT NULL,
[CustomerGroupId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCustomerGroupConditionInclude] on [campaignlek].[tCustomerGroupConditionInclude]'
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionInclude] ADD CONSTRAINT [PK_tCustomerGroupConditionInclude] PRIMARY KEY CLUSTERED  ([ConditionId], [CustomerGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountCartAction]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountCartAction]
(
[CartActionId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountCartAction] on [campaignlek].[tFixedDiscountCartAction]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountCartAction] ADD CONSTRAINT [PK_tFixedDiscountCartAction] PRIMARY KEY CLUSTERED  ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedDiscountCartActionCurrency]'
GO
CREATE TABLE [campaignlek].[tFixedDiscountCartActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValue] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedDiscountCartActionCurrency] on [campaignlek].[tFixedDiscountCartActionCurrency]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountCartActionCurrency] ADD CONSTRAINT [PK_tFixedDiscountCartActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemDiscountCartActionCurrencyDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionCurrencyDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCartItemDiscountCartActionCurrency]
	WHERE
		CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemDiscountCartActionCurrencyInsert]'
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionCurrencyInsert]
	@CartActionId	INT,
	@CurrencyId		INT,
	@MonetaryValue	DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tCartItemDiscountCartActionCurrency] (
		CartActionId,
		CurrencyId,
		MonetaryValue
	)
	VALUES (
		@CartActionId,
		@CurrencyId,
		@MonetaryValue
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemDiscountCartActionDelete]'
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pCartItemDiscountCartActionCurrencyDeleteAll] @CartActionId

	DELETE FROM [campaignlek].[tCartItemDiscountCartAction]
	WHERE CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemDiscountCartActionSave]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionSave]
	@CartActionId				INT,
	@Quantity					INT,
	@ApplyForCheapest			BIT,
	@IsPercentageDiscount		BIT,
	@PercentageDiscountAmount	DECIMAL(16,2)
AS
BEGIN
	UPDATE
		[campaignlek].[tCartItemDiscountCartAction]
	SET
		Quantity = @Quantity,
		ApplyForCheapest = @ApplyForCheapest,
		IsPercentageDiscount = @IsPercentageDiscount,
		PercentageDiscountAmount = @PercentageDiscountAmount
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tCartItemDiscountCartAction] (
			CartActionId,
			Quantity,
			ApplyForCheapest,
			IsPercentageDiscount,
			PercentageDiscountAmount
		)
		VALUES (
			@CartActionId,
			@Quantity,
			@ApplyForCheapest,
			@IsPercentageDiscount,
			@PercentageDiscountAmount
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemOptionDelete]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemOptionDelete]
	@CartItemOptionId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM 
		[campaignlek].[tCartItemOption]
	WHERE
		CartItemOptionId = @CartItemOptionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemOptionSave]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemOptionSave]
	@CartItemOptionId	INT,
    @CartId				INT,
    @ProductId			INT,
    @SizeId				INT,
	@ErpId				VARCHAR(50),
    @Quantity			INT,
    @CreatedDate		DATETIME
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[campaignlek].[tCartItemOption]
	SET
		SizeId = @SizeId,
		ErpId = @ErpId,
		Quantity = Quantity + @Quantity
	WHERE
		CartId = @CartId
		AND ProductId = @ProductId
            

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [campaignlek].[tCartItemOption]
		(
			  CartId,
			  ProductId,
			  SizeId,
			  ErpId,
			  Quantity,
			  CreatedDate            
		)
		VALUES
		(
			  @CartId,
			  @ProductId,
			  @SizeId,
			  @ErpId,
			  @Quantity,
			  @CreatedDate
		)
	END
	
	
	DELETE FROM [campaignlek].[tCartItemOption] WHERE Quantity <= 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCustomerGroupConditionExcludeDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionExcludeDeleteAll]
	@ConditionId INT
AS 
BEGIN 
	DELETE
		[campaignlek].[tCustomerGroupConditionExclude]
	WHERE
		ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCustomerGroupConditionIncludeDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionIncludeDeleteAll]
	@ConditionId INT
AS 
BEGIN 
	DELETE
		[campaignlek].[tCustomerGroupConditionInclude]
	WHERE
		ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCustomerGroupConditionExcludeGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionExcludeGetIdAll]
	@ConditionId	INT
AS
BEGIN
	SELECT 
		[CustomerGroupId]
	FROM 
		[campaignlek].[tCustomerGroupConditionExclude] 
	WHERE 
		[ConditionId] = @ConditionId 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCustomerGroupConditionExcludeInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionExcludeInsert]
	@ConditionId		INT,
	@CustomerGroupId	INT
AS
BEGIN
	INSERT [campaignlek].[tCustomerGroupConditionExclude] (
		ConditionId,
		CustomerGroupId
	)
	VALUES (
		@ConditionId,
		@CustomerGroupId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCustomerGroupConditionIncludeGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionIncludeGetIdAll]
	@ConditionId	INT
AS
BEGIN
	SELECT 
		[CustomerGroupId]
	FROM 
		[campaignlek].[tCustomerGroupConditionInclude] 
	WHERE 
		[ConditionId] = @ConditionId 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCustomerGroupConditionIncludeInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionIncludeInsert]
	@ConditionId		INT,
	@CustomerGroupId	INT
AS
BEGIN
	INSERT [campaignlek].[tCustomerGroupConditionInclude] (
		ConditionId,
		CustomerGroupId
	)
	VALUES (
		@ConditionId,
		@CustomerGroupId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCustomerGroupConditionSave]'
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionSave]
	@ConditionId INT
AS 
BEGIN
	IF EXISTS (SELECT 1 FROM [campaignlek].[tCustomerGroupCondition] WHERE ConditionId = @ConditionId)
		RETURN
	
	INSERT [campaignlek].[tCustomerGroupCondition]
	( 
		ConditionId
	)
	VALUES 
	(
		@ConditionId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountCartActionCurrencyDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionCurrencyDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountCartActionCurrency]
	WHERE
		CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountCartActionCurrencyInsert]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionCurrencyInsert]
	@CartActionId	INT,
	@CurrencyId		INT,
	@MonetaryValue	DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountCartActionCurrency] (
		CartActionId,
		CurrencyId,
		MonetaryValue
	)
	VALUES (
		@CartActionId,
		@CurrencyId,
		@MonetaryValue
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountCartActionDelete]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pFixedDiscountCartActionCurrencyDeleteAll] @CartActionId

	DELETE FROM [campaignlek].[tFixedDiscountCartAction]
	WHERE CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountCartActionSave]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionSave]
	@CartActionId INT
AS 
BEGIN
	IF EXISTS (SELECT 1 FROM [campaignlek].[tFixedDiscountCartAction] WHERE CartActionId = @CartActionId)
		RETURN
	
	INSERT [campaignlek].[tFixedDiscountCartAction]
	( 
		CartActionId
	)
	VALUES 
	(
		@CartActionId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pProductAutoFreeCartActionDelete]'
GO

CREATE PROCEDURE [campaignlek].[pProductAutoFreeCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	DELETE FROM [campaignlek].[tProductAutoFreeCartAction]
	WHERE CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pProductAutoFreeCartActionSave]'
GO

CREATE PROCEDURE [campaignlek].[pProductAutoFreeCartActionSave]
	@CartActionId	INT,
	@ProductId		INT,
	@Quantity		INT
AS
BEGIN
	UPDATE
		[campaignlek].[tProductAutoFreeCartAction]
	SET
		ProductId = @ProductId,
		Quantity = @Quantity
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tProductAutoFreeCartAction] (
			CartActionId,
			ProductId,
			Quantity
		)
		VALUES (
			@CartActionId,
			@ProductId,
			@Quantity
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockProductFilterTagDelete]'
GO
CREATE PROCEDURE [lekmer].[pBlockProductFilterTagDelete]
	@TagId INT
AS 
BEGIN
	SET NOCOUNT ON

    DELETE
        [lekmer].[tBlockProductFilterTag]
    WHERE
        [TagId] = @TagId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pLekmerCartItemSave]'
GO
ALTER PROCEDURE [lekmer].[pLekmerCartItemSave]
	@CartItemId 			INT,
	@ProductId				INT,
	@SizeId					INT,
	@ErpId					VARCHAR(50),
	@IsAffectedByCampaign	BIT
AS
BEGIN
      SET NOCOUNT ON

      UPDATE
            [lekmer].[tLekmerCartItem]
      SET
            SizeId = @SizeId,
            ErpId = @ErpId,
            IsAffectedByCampaign = IsAffectedByCampaign
      WHERE            
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO [lekmer].[tLekmerCartItem]
            (
				  CartItemId,
                  ProductId,
                  SizeId,
                  ErpId,
                  IsAffectedByCampaign
            )
            VALUES
            (
				  @CartItemId,
                  @ProductId,
                  @SizeId,
                  @ErpId,
                  @IsAffectedByCampaign
            )
      END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [security].[pRoleRemoveUsers]'
GO
ALTER PROCEDURE [security].[pRoleRemoveUsers]
	@RoleId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[security].[tUserRole]
	WHERE
		[RoleId] = @RoleId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCustomerGroupConditionDelete]'
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionDelete]
	@ConditionId INT
AS 
BEGIN
	EXEC [campaignlek].[pCustomerGroupConditionIncludeDeleteAll] @ConditionId
	EXEC [campaignlek].[pCustomerGroupConditionExcludeDeleteAll] @ConditionId

	DELETE FROM [campaignlek].[tCustomerGroupCondition]
	WHERE ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[pCustomerGroupGetAll]'
GO
CREATE PROCEDURE [customerlek].[pCustomerGroupGetAll]
AS
BEGIN
	SELECT
		CG.*
	FROM
		[customer].[vCustomCustomerGroup] AS CG
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCartItemDiscountCartActionCurrency]'
GO

CREATE VIEW [campaignlek].[vCartItemDiscountCartActionCurrency]
AS
	SELECT
		pdcac.[CartActionId]	AS 'CartItemDiscountCartActionCurrency.CartActionId',
		pdcac.[CurrencyId]		AS 'CartItemDiscountCartActionCurrency.CurrencyId',
		pdcac.[MonetaryValue]	AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tCartItemDiscountCartActionCurrency] pdcac
		INNER JOIN [core].[vCustomCurrency] c ON pdcac.[CurrencyId] = c.[Currency.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemDiscountCartActionCurrencyGetByAction]'
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartItemDiscountCartActionCurrency]
	WHERE 
		[CartItemDiscountCartActionCurrency.CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCartItemDiscountCartAction]'
GO

CREATE VIEW [campaignlek].[vCartItemDiscountCartAction]
AS
	SELECT
		pdca.[CartActionId]				AS 'CartItemDiscountCartAction.CartActionId',
		pdca.[Quantity]					AS 'CartItemDiscountCartAction.Quantity',
		pdca.[ApplyForCheapest]			AS 'CartItemDiscountCartAction.ApplyForCheapest',
		pdca.[IsPercentageDiscount]		AS 'CartItemDiscountCartAction.IsPercentageDiscount',
		pdca.[PercentageDiscountAmount] AS 'CartItemDiscountCartAction.PercentageDiscountAmount',
		ca.*
	FROM
		[campaignlek].[tCartItemDiscountCartAction] pdca
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = pdca.[CartActionId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemDiscountCartActionGetById]'
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartItemDiscountCartAction]
	WHERE
		[CartItemDiscountCartAction.CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCustomerGroupCondition]'
GO
CREATE VIEW [campaignlek].[vCustomerGroupCondition]
AS
	SELECT
		c.*
	FROM
		[campaignlek].[tCustomerGroupCondition] cgc
		INNER JOIN [campaign].[vCustomCondition] c ON c.[Condition.Id] = cgc.[ConditionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCustomerGroupConditionGetById]'
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionGetById]
	@ConditionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCustomerGroupCondition]
	WHERE
		[Condition.Id] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vFixedDiscountCartActionCurrency]'
GO
CREATE VIEW [campaignlek].[vFixedDiscountCartActionCurrency]
AS
	SELECT
		fdcac.[CartActionId]	AS 'FixedDiscountCartActionCurrency.CartActionId',
		fdcac.[CurrencyId]		AS 'FixedDiscountCartActionCurrency.CurrencyId',
		fdcac.[MonetaryValue]	AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tFixedDiscountCartActionCurrency] fdcac
		INNER JOIN [core].[vCustomCurrency] c ON fdcac.[CurrencyId] = c.[Currency.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountCartActionCurrencyGetByAction]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedDiscountCartActionCurrency]
	WHERE 
		[FixedDiscountCartActionCurrency.CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vFixedDiscountCartAction]'
GO
CREATE VIEW [campaignlek].[vFixedDiscountCartAction]
AS
	SELECT
		fdca.[CartActionId]	AS 'FixedDiscountCartAction.CartActionId',
		ca.*
	FROM
		[campaignlek].[tFixedDiscountCartAction] fdca
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = fdca.[CartActionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedDiscountCartActionGetById]'
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedDiscountCartAction]
	WHERE
		[FixedDiscountCartAction.CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vProductAutoFreeCartAction]'
GO


CREATE VIEW [campaignlek].[vProductAutoFreeCartAction]
AS
	SELECT
		pafca.[CartActionId] AS 'ProductAutoFreeCartAction.CartActionId',
		pafca.[ProductId] AS 'ProductAutoFreeCartAction.ProductId',
		pafca.[Quantity] AS 'ProductAutoFreeCartAction.Quantity',
		ca.*
	FROM
		[campaignlek].[tProductAutoFreeCartAction] pafca
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = pafca.[CartActionId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pProductAutoFreeCartActionGetById]'
GO

CREATE PROCEDURE [campaignlek].[pProductAutoFreeCartActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vProductAutoFreeCartAction]
	WHERE
		[ProductAutoFreeCartAction.CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCartItemOption]'
GO


CREATE VIEW [campaignlek].[vCartItemOption]
AS
SELECT 
		cio.CartItemOptionId AS 'CartItemOption.CartItemOptionId',
		cio.CartId AS 'CartItemOption.CartId',
		cio.SizeId AS 'CartItemOption.SizeId',
		cio.ErpId AS 'CartItemOption.ErpId',
		cio.Quantity AS 'CartItemOption.Quantity', 
		cio.CreatedDate AS 'CartItemOption.CreatedDate',
		p.*		
FROM
	[campaignlek].[tCartItemOption] cio
	INNER JOIN [product].[vCustomProduct] p on p.[Product.Id] = cio.[ProductId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pTopSellerGetAll]'
GO
ALTER procedure [statistics].[pTopSellerGetAll]
	@ChannelId int,
	@DateTimeFrom datetime,
	@DateTimeTo datetime,
	@TopCount int,
	@StatusIds		varchar(max)
as
begin
	declare @ProductIds table(ProductId int, Quantity int)
	
	insert
		@ProductIds
	(
		ProductId,
		Quantity
	)
	select top(@TopCount)
		p.ProductId,
		sum(oi.Quantity)
	from
		product.tProduct p
		inner join [order].tOrderItemProduct oip WITH(NOLOCK) on p.ProductId = oip.ProductId
		inner join [order].tOrderItem oi WITH(NOLOCK) on oi.OrderItemId = oip.OrderItemId
		inner join [order].tOrder o WITH(NOLOCK) on oi.OrderId = o.OrderId
	where
		o.ChannelId = @ChannelId
		and o.CreatedDate between isnull(@DateTimeFrom, o.CreatedDate) and isnull(@DateTimeTo, o.CreatedDate)
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
	group by
		p.ProductId
	order by
		sum(oi.Quantity) desc

	select
		p.*,
		pli.*,
		i.Quantity as 'TopSeller.Quantity'
	from
		@ProductIds as i 
		--inner join [product].[vCustomProduct] as p on p.[Product.Id] = i.ProductId
		--Need to include offline products
		inner join [lekmer].[vCustomProductWithoutStatusFilter] as p on p.[Product.Id] = i.ProductId
		inner join [product].[vCustomPriceListItem] as pli
			on pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				null
			)
	where
		p.[Product.ChannelId] = @ChannelId
	order by
		i.Quantity DESC
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemOptionGetAllByCart]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemOptionGetAllByCart] 
	@ChannelId	INT,
	@CustomerId	INT,
	@CartId		INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[campaignlek].[vCartItemOption] cio
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = cio.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				cio.[Product.CurrencyId],
				cio.[Product.Id],
				cio.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		cio.[CartItemOption.CartId] = @CartId 
		AND cio.[Product.ChannelId] = @ChannelId
	ORDER BY cio.[CartItemOption.CreatedDate] ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomCartItem]'
GO
ALTER VIEW [order].[vCustomCartItem]
AS
	SELECT
		c.*,
		l.SizeId AS [LekmerCartItem.SizeId],
		l.ErpId AS [LekmerCartItem.ErpId],
		l.IsAffectedByCampaign AS [LekmerCartItem.IsAffectedByCampaign]
	FROM
		[order].[vCartItem] c
		INNER JOIN [lekmer].[tLekmerCartItem] l ON c.[CartItem.CartItemId] = l.[CartItemId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartItemDiscountCartAction]'
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartAction] ADD
CONSTRAINT [FK_tCartItemDiscountCartAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartItemDiscountCartActionCurrency]'
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionCurrency] ADD
CONSTRAINT [FK_tCartItemDiscountCartActionCurrency_tCartItemDiscountCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tCartItemDiscountCartAction] ([CartActionId]),
CONSTRAINT [FK_tCartItemDiscountCartActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartItemOption]'
GO
ALTER TABLE [campaignlek].[tCartItemOption] ADD
CONSTRAINT [FK_tCartItemOption_tCart] FOREIGN KEY ([CartId]) REFERENCES [order].[tCart] ([CartId]),
CONSTRAINT [FK_tCartItemOption_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]),
CONSTRAINT [FK_tCartItemOption_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCustomerGroupCondition]'
GO
ALTER TABLE [campaignlek].[tCustomerGroupCondition] ADD
CONSTRAINT [FK_tCustomerGroupCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCustomerGroupConditionExclude]'
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionExclude] ADD
CONSTRAINT [FK_tCustomerGroupConditionExclude_tCustomerGroupCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaignlek].[tCustomerGroupCondition] ([ConditionId]),
CONSTRAINT [FK_tCustomerGroupConditionExclude_tCustomerGroup] FOREIGN KEY ([CustomerGroupId]) REFERENCES [customer].[tCustomerGroup] ([CustomerGroupId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCustomerGroupConditionInclude]'
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionInclude] ADD
CONSTRAINT [FK_tCustomerGroupConditionInclude_tCustomerGroupCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaignlek].[tCustomerGroupCondition] ([ConditionId]),
CONSTRAINT [FK_tCustomerGroupConditionInclude_tCustomerGroup] FOREIGN KEY ([CustomerGroupId]) REFERENCES [customer].[tCustomerGroup] ([CustomerGroupId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountCartAction]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountCartAction] ADD
CONSTRAINT [FK_tFixedDiscountCartAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedDiscountCartActionCurrency]'
GO
ALTER TABLE [campaignlek].[tFixedDiscountCartActionCurrency] ADD
CONSTRAINT [FK_tFixedDiscountCartActionCurrency_tFixedDiscountCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tFixedDiscountCartAction] ([CartActionId]),
CONSTRAINT [FK_tFixedDiscountCartActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tProductAutoFreeCartAction]'
GO
ALTER TABLE [campaignlek].[tProductAutoFreeCartAction] ADD
CONSTRAINT [FK_tProductAutoFreeCartAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId]),
CONSTRAINT [FK_tProductAutoFreeCartAction_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
