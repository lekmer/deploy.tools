SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tBlockProductFilter]'
GO
ALTER TABLE [lekmer].[tBlockProductFilter] DROP
CONSTRAINT [FK_tBlockProductFilter_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tOrderItemSize]'
GO
ALTER TABLE [lekmer].[tOrderItemSize] DROP
CONSTRAINT [FK_tOrderItemSize_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] DROP
CONSTRAINT [FK_tProductSize_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tSize]'
GO
ALTER TABLE [lekmer].[tSize] DROP CONSTRAINT [PK_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tSize]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tSize]
(
[SizeId] [int] NOT NULL IDENTITY(1, 1),
[EU] [decimal] (5, 2) NOT NULL,
[EUTitle] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[USMale] [decimal] (3, 1) NOT NULL,
[USFemale] [decimal] (3, 1) NOT NULL,
[UKMale] [decimal] (3, 1) NOT NULL,
[UKFemale] [decimal] (3, 1) NOT NULL,
[Millimeter] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tSize] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tSize]([SizeId], [EU], [USMale], [USFemale], [UKMale], [UKFemale], [Millimeter]) SELECT [SizeId], [EU], [USMale], [USFemale], [UKMale], [UKFemale], [Millimeter] FROM [lekmer].[tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tSize] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[lekmer].[tSize]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[lekmer].[tmp_rg_xx_tSize]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tSize]', N'tSize'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSize] on [lekmer].[tSize]'
GO
ALTER TABLE [lekmer].[tSize] ADD CONSTRAINT [PK_tSize] PRIMARY KEY CLUSTERED  ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vSize]'
GO

ALTER VIEW [lekmer].[vSize]
AS
SELECT
	[SizeId] AS 'Size.Id',
	[EU] AS 'Size.EU',
	[EUTitle] AS 'Size.EUTitle',
	[USMale] AS 'Size.USMale',
	[USFemale] AS 'Size.USFemale',
	[UKMale] AS 'Size.UKMale',
	[UKFemale] AS 'Size.UKFemale',
	[Millimeter] AS 'Size.Millimeter'
FROM
	[lekmer].[tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vProductSize]'
GO




ALTER VIEW [lekmer].[vProductSize]
AS
SELECT     
	ps.[ProductId] AS 'ProductSize.ProductId',
	ps.[SizeId] AS 'ProductSize.SizeId',
	ps.[ErpId] AS 'ProductSize.ErpId',
	ps.[NumberInStock] AS 'ProductSize.NumberInStock',
	ps.[MillimeterDeviation] AS 'ProductSize.MillimeterDeviation',
	ps.[OverrideEU] AS 'ProductSize.OverrideEU',
	s.*
FROM
	[lekmer].[tProductSize] ps
	INNER JOIN [lekmer].[vSize] s ON ps.[SizeId] = s.[Size.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockProductFilter]'
GO
ALTER TABLE [lekmer].[tBlockProductFilter] ADD
CONSTRAINT [FK_tBlockProductFilter_tSize] FOREIGN KEY ([DefaultSizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tOrderItemSize]'
GO
ALTER TABLE [lekmer].[tOrderItemSize] ADD
CONSTRAINT [FK_tOrderItemSize_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] ADD
CONSTRAINT [FK_tProductSize_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO