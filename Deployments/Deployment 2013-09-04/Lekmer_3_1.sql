﻿SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductUrl]'
GO
ALTER TABLE [lekmer].[tProductUrl] DROP CONSTRAINT [FK_tProductUrl_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tProduct]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tBrand]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tBatteryType]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tSizeDeviation]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tProductType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [productlek].[tProductColor]'
GO
ALTER TABLE [productlek].[tProductColor] DROP CONSTRAINT [FK_tProductColor_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductSimilar]'
GO
ALTER TABLE [lekmer].[tProductSimilar] DROP CONSTRAINT [FK_tProductSimilar_tLekmerProduct]
ALTER TABLE [lekmer].[tProductSimilar] DROP CONSTRAINT [FK_tProductSimilar_tLekmerProduct1]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] DROP CONSTRAINT [FK_tProductPopularity_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductUrlHistory]'
GO
ALTER TABLE [lekmer].[tProductUrlHistory] DROP CONSTRAINT [FK_tProductUrlHistory_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] DROP CONSTRAINT [FK_tProductSize_tLekmerProduct]
ALTER TABLE [lekmer].[tProductSize] DROP CONSTRAINT [FK_tProductSize_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProductTranslation]'
GO
ALTER TABLE [lekmer].[tLekmerProductTranslation] DROP CONSTRAINT [FK_tLekmerProductTranslation_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProductIcon]'
GO
ALTER TABLE [lekmer].[tLekmerProductIcon] DROP CONSTRAINT [FK_tLekmerProductIcon_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaignlek].[tCartItemOption]'
GO
ALTER TABLE [campaignlek].[tCartItemOption] DROP CONSTRAINT [FK_tCartItemOption_tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tMonitorProduct]'
GO
ALTER TABLE [lekmer].[tMonitorProduct] DROP CONSTRAINT [FK_tMonitorProduct_tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] DROP CONSTRAINT [FK_tLekmerCartItem_tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [orderlek].[tCartItemPackageElement]'
GO
ALTER TABLE [orderlek].[tCartItemPackageElement] DROP CONSTRAINT [FK_tCartItemPackageElement_tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [PK_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_CreatedDate]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_ShowVariantRelations]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_IsAbove60L]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] DROP CONSTRAINT [PK_tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tLekmerProduct_BrandId] from [lekmer].[tLekmerProduct]'
GO
DROP INDEX [IX_tLekmerProduct_BrandId] ON [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tLekmerProduct_HYErpId] from [lekmer].[tLekmerProduct]'
GO
DROP INDEX [IX_tLekmerProduct_HYErpId] ON [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tProductSize_NumberInStock(ProductId)] from [lekmer].[tProductSize]'
GO
DROP INDEX [IX_tProductSize_NumberInStock(ProductId)] ON [lekmer].[tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tProductSize_ProductId] from [lekmer].[tProductSize]'
GO
DROP INDEX [IX_tProductSize_ProductId] ON [lekmer].[tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tProductSize_ProductId_NumberInStock] from [lekmer].[tProductSize]'
GO
DROP INDEX [IX_tProductSize_ProductId_NumberInStock] ON [lekmer].[tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tProductSize_ErpId] from [lekmer].[tProductSize]'
GO
DROP INDEX [IX_tProductSize_ErpId] ON [lekmer].[tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tLekmerProduct]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tLekmerProduct]
(
[ProductId] [int] NOT NULL,
[BrandId] [int] NULL,
[IsBookable] [bit] NOT NULL,
[AgeFromMonth] [int] NOT NULL,
[AgeToMonth] [int] NOT NULL,
[IsNewFrom] [datetime] NULL,
[IsNewTo] [datetime] NULL,
[Measurement] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[BatteryTypeId] [int] NULL,
[NumberOfBatteries] [int] NULL,
[IsBatteryIncluded] [bit] NOT NULL,
[ExpectedBackInStock] [datetime] NULL,
[HYErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_tLekmerProduct_CreatedDate] DEFAULT (getdate()),
[SizeDeviationId] [int] NULL,
[LekmerErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ShowVariantRelations] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_ShowVariantRelations] DEFAULT ((0)),
[Weight] [decimal] (18, 3) NULL,
[ProductTypeId] [int] NOT NULL,
[IsAbove60L] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_IsAbove60L] DEFAULT ((0)),
[StockStatusId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tLekmerProduct]([ProductId], [BrandId], [IsBookable], [AgeFromMonth], [AgeToMonth], [IsNewFrom], [IsNewTo], [Measurement], [BatteryTypeId], [NumberOfBatteries], [IsBatteryIncluded], [ExpectedBackInStock], [HYErpId], [CreatedDate], [SizeDeviationId], [LekmerErpId], [ShowVariantRelations], [Weight], [ProductTypeId], [IsAbove60L], [StockStatusId]) SELECT [ProductId], [BrandId], [IsBookable], [AgeFromMonth], [AgeToMonth], [IsNewFrom], [IsNewTo], [Measurement], [BatteryTypeId], [NumberOfBatteries], [IsBatteryIncluded], [ExpectedBackInStock], [HYErpId], [CreatedDate], [SizeDeviationId], [LekmerErpId], [ShowVariantRelations], [Weight], [ProductTypeId], [IsAbove60L], 0 FROM [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tLekmerProduct]', N'tLekmerProduct'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tLekmerProduct] on [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [PK_tLekmerProduct] PRIMARY KEY CLUSTERED  ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tLekmerProduct_BrandId] on [lekmer].[tLekmerProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tLekmerProduct_BrandId] ON [lekmer].[tLekmerProduct] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tLekmerProduct_HYErpId] on [lekmer].[tLekmerProduct]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tLekmerProduct_HYErpId] ON [lekmer].[tLekmerProduct] ([HYErpId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tProductSize]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tProductSize]
(
[ProductId] [int] NOT NULL,
[SizeId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[NumberInStock] [int] NOT NULL,
[MillimeterDeviation] [int] NULL,
[OverrideEU] [decimal] (3, 1) NULL,
[OverrideMillimeter] [int] NULL,
[Weight] [decimal] (18, 3) NULL,
[StockStatusId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tProductSize]([ProductId], [SizeId], [ErpId], [NumberInStock], [MillimeterDeviation], [OverrideEU], [OverrideMillimeter], [Weight], [StockStatusId]) SELECT [ProductId], [SizeId], [ErpId], [NumberInStock], [MillimeterDeviation], [OverrideEU], [OverrideMillimeter], [Weight], 0 FROM [lekmer].[tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tProductSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tProductSize]', N'tProductSize'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductSize] on [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] ADD CONSTRAINT [PK_tProductSize] PRIMARY KEY CLUSTERED  ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tProductSize_NumberInStock(ProductId)] on [lekmer].[tProductSize]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductSize_NumberInStock(ProductId)] ON [lekmer].[tProductSize] ([NumberInStock]) INCLUDE ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tProductSize_ProductId] on [lekmer].[tProductSize]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductSize_ProductId] ON [lekmer].[tProductSize] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tProductSize_ProductId_NumberInStock] on [lekmer].[tProductSize]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductSize_ProductId_NumberInStock] ON [lekmer].[tProductSize] ([ProductId], [NumberInStock])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tProductSize_ErpId] on [lekmer].[tProductSize]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductSize_ErpId] ON [lekmer].[tProductSize] ([ErpId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO


ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	[product].[fProductHasSizes](ProductId) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations',
	[Weight] AS 'Lekmer.Weight',
	[ProductTypeId] AS 'Lekmer.ProductTypeId',
	[IsAbove60L] AS 'Lekmer.IsAbove60L',
	[StockStatusId] AS 'Lekmer.StockStatusId'
FROM
	lekmer.tLekmerProduct AS p

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProduct]'
GO

ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vProductSize]'
GO


ALTER VIEW [lekmer].[vProductSize]
AS
SELECT     
	ps.[ProductId] AS 'ProductSize.ProductId',
	ps.[SizeId] AS 'ProductSize.SizeId',
	ps.[ErpId] AS 'ProductSize.ErpId',
	ps.[NumberInStock] AS 'ProductSize.NumberInStock',
	ps.[MillimeterDeviation] AS 'ProductSize.MillimeterDeviation',
	ps.[OverrideEU] AS 'ProductSize.OverrideEU',
	ps.[OverrideMillimeter] AS 'ProductSize.OverrideMillimeter',
	[ps].[Weight] AS 'ProductSize.Weight',
	[ps].[StockStatusId] AS 'ProductSize.StockStatusId',
	s.*
FROM
	[lekmer].[tProductSize] ps
	INNER JOIN [lekmer].[vSize] s ON ps.[SizeId] = s.[Size.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO

ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.IsAbove60L],
		[Lekmer.StockStatusId],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId',
		NULL AS 'Lekmer.RecommendedPrice'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductRecord]'
GO

ALTER VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId]
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductViewWithoutStatusFilter]'
GO

ALTER VIEW [lekmer].[vCustomProductViewWithoutStatusFilter]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductViewWithoutStatusFilter] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[usp_UpdateProductStatusLekmer]'
GO
CREATE PROCEDURE [integration].[usp_UpdateProductStatusLekmer]
AS
BEGIN
	DECLARE @ProductId INT
	DECLARE @NumberInStock INT
	DECLARE @StockStatusId INT
	
	DECLARE @OfflineId INT
	SET @OfflineId = (SELECT [ProductStatusId] FROM [product].[tProductStatus] WHERE [CommonName] = 'Offline')
	
	DECLARE @PassiveId INT
	SET @PassiveId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [CommonName] = 'Passive')
	
	DECLARE @SalesTagId INT
	SET @SalesTagId = (SELECT TagId FROM lekmer.tTag WHERE Value = 'Sale')

	DECLARE cur_product CURSOR FAST_FORWARD FOR
	SELECT 
		[lp].[ProductId],
		[p].[NumberInStock], 
		[lp].[StockStatusId]
	FROM
		[lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [lp].[ProductId]
	WHERE
		[lp].[ProductTypeId] = 1

	OPEN cur_product

	FETCH NEXT FROM cur_product 
	INTO @ProductId,
		 @NumberInStock,
		 @StockStatusId

	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId)
			BEGIN
				-- no sizes
				-- set offline
				-- remove sale tag
				
				IF (@NumberInStock <= 0 AND @StockStatusId = @PassiveId)
				BEGIN 
					UPDATE [product].[tProduct]
					SET [ProductStatusId] = @OfflineId
					WHERE [ProductId] = @ProductId
					
					DELETE FROM lekmer.tProductTag
					WHERE [ProductId] = @ProductId
						  AND TagId = @SalesTagId
				END
			END
		ELSE
			BEGIN
				-- product with sizes
				
				IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId AND ([StockStatusId] != @PassiveId OR [NumberInStock] > 0))
				BEGIN 
					-- set offline
					-- remove sale tag
				
					UPDATE [product].[tProduct]
					SET [ProductStatusId] = @OfflineId
					WHERE [ProductId] = @ProductId
					
					DELETE FROM lekmer.tProductTag
					WHERE [ProductId] = @ProductId
						  AND TagId = @SalesTagId
				END
				
				IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId AND [StockStatusId] != @PassiveId)
				BEGIN
					-- set passive
					UPDATE [lekmer].[tLekmerProduct]
					SET [StockStatusId] = @PassiveId
					WHERE [ProductId] = @ProductId
				END
				
				-- update number in stock on product level
				UPDATE [product].[tProduct]
				SET [NumberInStock] = (
										SELECT SUM([ps].[NumberInStock])
										FROM [lekmer].[tProductSize] ps
										WHERE [ps].[ProductId] = @ProductId AND [ps].[NumberInStock] >= 0
									  )
				WHERE [ProductId] = @ProductId
			END
		
		FETCH NEXT FROM cur_product 
		INTO @ProductId,
			 @NumberInStock,
			 @StockStatusId
	END

	CLOSE cur_product
	DEALLOCATE cur_product
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_UpdateTCategoryLekmer]'
GO
ALTER PROCEDURE [integration].[usp_UpdateTCategoryLekmer]
AS
BEGIN
	DECLARE
		@Seperator NVARCHAR(1),	
		@ErpCategoryPrefix NVARCHAR(2),
		@ErpClassId NVARCHAR(50),
		@ErpClassGroupId NVARCHAR(50),
		@ErpClassGroupCodeId NVARCHAR(50),
		@ClassGroupCodeId NVARCHAR(50),
		@HYErpId NVARCHAR(50),
		@ArticleClassId NVARCHAR(10),
		@ArticleClassTitle NVARCHAR(40),
		@ArticleGroupId NVARCHAR(10),
		@ArticleGroupTitle NVARCHAR(40),
		@ArticleCodeId NVARCHAR(10),
		@ArticleCodeTitle NVARCHAR(40)
	
	SET @Seperator = '-'
	SET @ErpCategoryPrefix = 'C_'
		
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT DISTINCT
			SUBSTRING(tp.HYarticleId, 5, 12),
			tp.ArticleClassId ,
			tp.ArticleClassTitle,
			tp.ArticleGroupId,
			tp.ArticleGroupTitle,
			tp.ArticleCodeId,
			tp.ArticleCodeTitle
		FROM
			[integration].tempProduct tp
			LEFT JOIN lekmer.tLekmerProduct lp ON lp.HYErpId = SUBSTRING(tp.HYarticleId, 5, 12)
			LEFT JOIN product.tProduct p ON p.ProductId = lp.ProductId
			LEFT JOIN product.tCategory c ON c.CategoryId = p.CategoryId
		WHERE
			SUBSTRING(tp.HYarticleId, 1, 3) = 1
			AND
			(
				@ErpCategoryPrefix 
					+ ISNULL(tp.ArticleClassId, '') + @Seperator 
					+ ISNULL(tp.ArticleGroupId, '') + @Seperator 
					+ ISNULL(tp.ArticleCodeId, '') <> c.ErpId
				OR
					c.ErpId is null
			)
		
	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO 
			@HYErpId,
			@ArticleClassId,
			@ArticleClassTitle,
			@ArticleGroupId,
			@ArticleGroupTitle,
			@ArticleCodeId,
			@ArticleCodeTitle
		
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @ClassGroupCodeId = @ArticleClassId + @Seperator + @ArticleGroupId + @Seperator + @ArticleCodeId
			
			BEGIN TRY
				BEGIN TRANSACTION
					SET @ErpClassId = @ErpCategoryPrefix + @ArticleClassId
					SET @ErpClassGroupId = @ErpClassId + @Seperator + @ArticleGroupId
					SET @ErpClassGroupCodeId = @ErpClassGroupId + @Seperator + @ArticleCodeId
				
					-- varuklass
					IF not exists (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassId)
						INSERT INTO 
							product.tCategory (ParentCategoryId, Title, ErpId)
						SELECT 	
							null,
							@ArticleClassTitle,
							@ErpClassId

					-- Varugrupp
					IF NOT EXISTS (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassGroupId)
						INSERT INTO 
							[product].tCategory (ParentCategoryId, Title, ErpId)
						SELECT 	
							(SELECT CategoryId FROM product.tCategory WHERE ErpId = @ErpClassId),
							@ArticleGroupTitle,		
							@ErpClassGroupId
					
					-- Varukod
					IF (NOT EXISTS (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassGroupCodeId)
						AND @ArticleCodeTitle <> '***Missing***' )
							INSERT INTO 
								[product].tCategory (ParentCategoryId, Title, ErpId)
							SELECT 	
								(SELECT CategoryId FROM product.tCategory WHERE ErpId = @ErpClassGroupId),
								@ArticleCodeTitle,		
								@ErpClassGroupCodeId

					-- ändra referensen på categoryId om den bytat
					UPDATE 
						tp
					SET 
						tp.CategoryId = (SELECT categoryId FROM product.tCategory
										 WHERE ErpId = @ErpClassGroupCodeId)
					FROM 
						[lekmer].tLekmerProduct lp						
						INNER JOIN product.tProduct tp ON tp.ProductId = lp.ProductId				
					WHERE 
						lp.HYErpId = @HYErpId 
						AND tp.CategoryId <> (SELECT categoryId FROM product.tCategory 
											  WHERE ErpId = @ErpClassGroupCodeId)
					
				COMMIT
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0 ROLLBACK

				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				VALUES(@ClassGroupCodeId, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
				
				-- Reset identity column counter
				DBCC CHECKIDENT ('product.tCategory', reseed, 0)
				DBCC CHECKIDENT ('product.tCategory', reseed)

			END CATCH
		
			FETCH NEXT FROM cur_product 
				INTO 
					@HYErpId,
					@ArticleClassId,
					@ArticleClassTitle,
					@ArticleGroupId,
					@ArticleGroupTitle,
					@ArticleCodeId,
					@ArticleCodeTitle
		END
	CLOSE cur_product
	DEALLOCATE cur_product	              
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_UpdateSizeLekmer]'
GO
ALTER PROCEDURE [integration].[usp_UpdateSizeLekmer]
AS
BEGIN

	UPDATE
		[ps]
	SET
		[ps].[NumberInStock] = [tp].[NoInStock],
		[ps].[Weight] = CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
	FROM
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tProductSize] ps ON [ps].[ErpId] = SUBSTRING([tp].[HYarticleId], 5, 16)
	WHERE 
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND 
		(
			[ps].[NumberInStock] <> [tp].[NoInStock]
			OR [ps].[Weight] IS NULL
			OR [ps].[Weight] <> CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
		)
			
			
	UPDATE
		[ps]
	SET
		[ps].[StockStatusId] = [ss].[StockStatusId]
	FROM
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tProductSize] ps ON [ps].[ErpId] = SUBSTRING([tp].[HYarticleId], 5, 16)
		INNER JOIN [productlek].[tStockStatus] ss ON [ss].[ErpId] = [tp].[Ref1]
	WHERE 
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND 
		[ps].[StockStatusId] != [ss].[StockStatusId]
		
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductSizesLekmer]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateProductSizesLekmer]
	@HYChannel NVARCHAR(40),
	@ProductId INT,
	@HYArticleNoColorSize NVARCHAR(50),
	@HYSizeId NVARCHAR (50),
	@HYSizeValue NVARCHAR (50),
	@NoInStock NVARCHAR(250),
	@Weight NVARCHAR (50),
	@StockStatusId INT
AS
BEGIN

	DECLARE 
		@Data NVARCHAR(4000),
		@SizeId INT,
		@NeedToInsertProductSize BIT
	
	IF @HYChannel = '001' -- Only SE channel
	IF @HYSizeId NOT IN ('000', '100', '223')
	IF NOT EXISTS(SELECT 1 FROM lekmer.tProductSize WHERE ErpId = @HYArticleNoColorSize)
	
	BEGIN TRY
		BEGIN TRANSACTION

		SET @Data = 'tProductSIZE Shoes: @HYArticleNoColorSize ' + @HYArticleNoColorSize + ' @HYChannel ' + @HYChannel + ' @HYSizeValue ' + @HYSizeValue

		SET @NeedToInsertProductSize = 0
		
		SET @SizeId = (SELECT SizeId FROM lekmer.tSize s WHERE s.ErpId = @HYSizeId)
		
		IF @SizeId IS NOT NULL
		SET @NeedToInsertProductSize = 1
		
		IF @NeedToInsertProductSize = 1
		BEGIN
			IF NOT EXISTS (SELECT 1
						   FROM [lekmer].tProductSize ps
						   WHERE ps.ProductId = @ProductId 
						   AND ps.SizeId = @SizeId)
			BEGIN
				INSERT INTO [lekmer].tProductSize (
					ProductId,
					SizeId,
					ErpId,
					NumberInStock,
					[Weight],
					[StockStatusId]
				)
				VALUES (
					@ProductId,
					@SizeId,
					@HYArticleNoColorSize,
					@NoInStock,
					CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), @Weight) / 1000),
					@StockStatusId
				)
			END
		END
	
		COMMIT	
	END TRY
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		
		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			@Data,
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)

	END CATCH
		
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductView]'
GO

ALTER VIEW [product].[vCustomProductView]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateLekmerProduct]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateLekmerProduct]
AS
BEGIN
	DECLARE 
		@HYChannelNL NVARCHAR(40),
		@ChannelIdNL INT
	
	SET @HYChannelNL = '005'
	SET @ChannelIdNL = 1000005

	-- Update tLekmerProduct
	UPDATE 
		[lp]
	SET 
		[lp].[ExpectedBackInStock] =  NULL
	FROM 
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tLekmerProduct] lp				ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
		INNER JOIN [product].[tProduct] p					ON [p].[ProductId] = [lp].[ProductId]
		INNER JOIN [product].[tProductRegistryProduct] pli	ON [pli].[ProductId] = [lp].[ProductId]
																AND ([pli].[ProductRegistryId] = SUBSTRING([tp].[HYarticleId], 3, 1)
																	 OR ([pli].[ProductRegistryId] = @ChannelIdNL AND LEFT([tp].[HYarticleId], 3) = @HYChannelNL))

	WHERE 
		[lp].[ExpectedBackInStock] <> LEN([lp].[ExpectedBackInStock])
		AND [tp].[NoInStock] > 0
	
	-- Update tLekmerProduct
	UPDATE 
		[lp]
	SET 
		[lp].[LekmerErpId] = [tp].[LekmerArtNo],
		[lp].[Weight] = CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
	FROM 
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
	WHERE
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND
			(
				[lp].[LekmerErpId] IS NULL
				OR [lp].[LekmerErpId] <> [tp].[LekmerArtNo]
				OR [lp].[Weight] IS NULL
				OR [lp].[Weight] <> CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000)
			)
	
	UPDATE
		[lp]
	SET
		[lp].[IsAbove60L] = CASE WHEN [tp].[PossibleToDisplay] IS NULL OR [tp].[PossibleToDisplay] <> 'G' THEN 0 ELSE 1 END
	FROM
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
	WHERE
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND 
		[IsAbove60L] <> CASE WHEN [tp].[PossibleToDisplay] IS NULL OR [tp].[PossibleToDisplay] <> 'G' THEN 0 ELSE 1 END
			
	UPDATE 
		[lp]
	SET
		[lp].[StockStatusId] = [ss].[StockStatusId]
	FROM 
		[integration].[tempProduct] tp
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
		INNER JOIN [productlek].[tStockStatus] ss ON [ss].[ErpId] = [tp].[Ref1]
	WHERE
		SUBSTRING([tp].[HYarticleId], 3, 1) = 1
		AND 
		[lp].[StockStatusId] != [ss].[StockStatusId]

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductLekmer]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateProductLekmer]
AS
BEGIN
	SET NOCOUNT ON 

	-- Delte all ***Missing*** (tmp)
	DELETE FROM integration.tempProduct WHERE ArticleCodeTitle= '***Missing***'

	-- tCategory
	EXEC [integration].[usp_UpdateTCategoryLekmer] -- creates the items if necessary
	EXEC [integration].[usp_UpdateCategoryTitleLekmer] -- update titles if necessary
	
	-- tBrand
	EXEC [integration].[usp_UpdateBrandLekmer]
	EXEC [integration].[usp_UpdateBrandLekmerCreationDate] -- adds the date the brand was created
	
	-- tProduct
	EXEC [integration].[usp_UpdateProductLekmer]
	EXEC [integration].[usp_ImportUpdateLekmerProduct]
	
	-- tPriceListItem
	EXEC [integration].[usp_UpdateProductPRICELekmer]
	
	-- tProductSize
	EXEC [integration].[usp_UpdateSizeLekmer]

	--------------------------------------

	DECLARE 
		@HYArticleNoFull NVARCHAR(50),
		@HYArticleNoColorSize NVARCHAR(50),
		@HYArticleNoColor NVARCHAR(50),
		@HYSizeId NVARCHAR(50),
		@EanCode NVARCHAR(50),
		@NoInStock NVARCHAR(250),
		@ArticleTitle NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@BrandId INT,
		@HYSizeValue NVARCHAR(50),
		@Weight NVARCHAR(50),
		@PossibleToDisplay NVARCHAR(100),
		@StockStatusErpId NVARCHAR(50),
		@StockStatusId INT,
		
		@CategoryId INT,
		@Price DECIMAL,
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@LekmerErpId NVARCHAR(25),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL,
		
		@NewFromDate DATETIME,
		@NewToDate DATETIME,

		-- Variables for Channel
		@HYChannel NVARCHAR(40),
		
		@TradeDoublerProductGroup NVARCHAR(50),
		@ChannelId INT,
		@IsProductRegistry BIT,
		@NeedToInsertProductRelationData BIT,
		
		@HYChannelNL NVARCHAR(40),
		@ChannelIdNL INT
	
	SET @Vat = 25.0
	
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 4, @NewFromDate)
	
	SET @HYChannelNL = '005'
	SET @ChannelIdNL = 1000005	

	------------------------------------

	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			tp.HYarticleId,
			tp.ArticleTitle,
			tp.SizeId,
			(tp.Price/100),
			tp.NoInStock,
			tp.ArticleGroupId,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.BrandId,
			tp.EanCode,
			tp.LekmerArtNo,
			[tp].[Weight],
			[tp].[PossibleToDisplay],
			[tp].[Ref1] -- StockStatusErpId
		FROM
			[integration].tempProduct tp
		--WHERE NOT EXISTS (SELECT 1
		--				  FROM lekmer.tLekmerProduct lp, product.tPriceListItem prp
		--				  WHERE lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
		--					AND lp.ProductId = prp.ProductId
		--					AND SUBSTRING(tp.HYarticleId, 3,1) = prp.PriceListId)

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@HYArticleNoFull,
			@ArticleTitle,
			@HYSizeValue,
			@Price,
			@NoInStock,
			@VarugruppId,
			@VaruklassId,
			@VarukodId,
			@HYBrandId,
			@EanCode,
			@LekmerErpId,
			@Weight,
			@PossibleToDisplay,
			@StockStatusErpId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			SET @IsProductRegistry = 0
			SET @NeedToInsertProductRelationData = 0
			
			-- Split @HYArticleNoFull
			-- 001-0000001-1017-108
			SET @HYChannel = SUBSTRING(@HYArticleNoFull, 1, 3)              -- [001]-0000001-1017-108
			SET @HYArticleNoColorSize = SUBSTRING(@HYArticleNoFull, 5, 17)  -- 001-[0000001-1017-108]
			SET @HYSizeId = SUBSTRING(@HYArticleNoFull, 18, 3)              -- 001-0000001-1017-[108]
			SET @HYArticleNoColor = SUBSTRING(@HYArticleNoFull, 5, 12)      -- 001-[0000001-1017]-108
		
			SET @StockStatusId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [ErpId] = @StockStatusErpId)
			SET @StockStatusId = ISNULL(@StockStatusId, 0) -- Active by default
		
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYArticleNoColor)
			IF @ProductId IS NOT NULL
			BEGIN
				IF EXISTS (SELECT 1 FROM product.tProductRegistryProduct
						   WHERE ProductId = @ProductId
								 AND (ProductRegistryId = @HYChannel 
									  OR ([ProductRegistryId] = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!
				BEGIN
					SET @IsProductRegistry = 1
					GOTO PRODUCT_END
				END
			END
			
			PRODUCT_START:
			
			---- Skip processing the same @HYErpId + @Channel few times
			--IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			--BEGIN
			--	GOTO PRODUCT_END
			--END ELSE BEGIN
			--	SET	@PrevHYErpId = @HYErpId
			--	SET	@PrevChannel = @Channel
			--END
			
			BEGIN TRANSACTION
			
			SET @CategoryId = (SELECT CategoryId 
							   FROM product.tCategory 
							   WHERE ErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, ''))
			
			SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)

			IF @ProductId IS NULL
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNoFull + ' EanCode ' + @EanCode
				
				-- tProduct
				INSERT INTO [product].tProduct (
					ErpId,
					EanCode,
					IsDeleted,
					NumberInStock,
					CategoryId,
					Title,
					[Description],
					ProductStatusId
				)
				VALUES (
					@HYArticleNoColor,
					@EanCode,
					0, -- IsDeleted
					@NoInStock,
					@CategoryId,
					@ArticleTitle, 
					'', -- Description
					1 -- Offline
				)
											
				SET @ProductId = SCOPE_IDENTITY()
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNoFull + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
	
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId,
					HYErpId,
					BrandId,
					IsBookable,
					AgeFromMonth,
					AgeToMonth,
					IsNewFrom,
					IsNewTo,
					IsBatteryIncluded,
					ExpectedBackInStock,
					LekmerErpId,
					ShowVariantRelations,
					[Weight],
					[ProductTypeId],
					[IsAbove60L],
					[StockStatusId]
				)
				VALUES (
					@ProductId, 
					@HYArticleNoColor,
					@BrandId,
					0, -- IsBookable, 
					0, -- AgeFromMonth
					0, -- AgeToMonth
					@NewFromDate,
					@NewToDate,
					0, -- IsBatteryIncluded
					NULL, --ExpectedBackInStock
					@LekmerErpId,
					1, -- ShowVariantRelations
					CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), @Weight) / 1000),
					1,
					CASE WHEN @PossibleToDisplay IS NULL OR @PossibleToDisplay <> 'G' THEN 0 ELSE 1 END,
					@StockStatusId
				)

			END	 -- Product already exist in tLekmerProduct
			ELSE IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem prp 
								WHERE prp.ProductId = @ProductId
									  AND (prp.PriceListId = @HYChannel 
										   OR (prp.PriceListId = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!(PriceListId)
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'EXISTING: @HYArticleNoColorSize ' + @HYArticleNoColorSize + ' @HYChannel ' + @HYChannel + ' ProductId' + CAST(@ProductId as varchar(10))
			END
			
			IF @NeedToInsertProductRelationData = 1
			BEGIN
				------------------------------------------------------------------------------------------
				-- tProductRegistryProduct
				
				IF NOT EXISTS (SELECT 1 FROM product.tProductRegistryProduct
							   WHERE ProductId = @ProductId
									 AND (ProductRegistryId = @HYChannel 
										  OR ([ProductRegistryId] = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!
				BEGIN
					INSERT INTO product.tProductRegistryProduct (
						ProductId,
						ProductRegistryId
					)
					VALUES (
						@ProductId,
						CASE 
							WHEN @HYChannel = @HYChannelNL THEN @ChannelIdNL
							ELSE @HYChannel
						END
					)
				END				
				
				------------------------------------------------------------------------------------------
				-- tPriceListItem
				-- PriceListId and @HYChannel have same values

				IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem
							   WHERE ProductId = @ProductId
									 AND (PriceListId = @HYChannel 
									      OR (PriceListId = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!(PriceListId)
				BEGIN
					INSERT INTO product.tPriceListItem (
						PriceListId,
						ProductId,
						PriceIncludingVat,
						PriceExcludingVat,
						VatPercentage
					)
					VALUES (
						CASE 
							WHEN @HYChannel = @HYChannelNL THEN @ChannelIdNL
							ELSE @HYChannel
						END,
						@ProductId, 
						@Price, 
						@Price / (1.0+@Vat/100.0),
						@Vat
					)
				END

				------------------------------------------------------------------------------------------ 
				-- tTradeDoublerProductGroupMapping

				SET @ChannelId = CAST(@HYChannel AS INT)
				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '')
													   AND (ChannelId = @ChannelId 
															OR (ChannelId = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!

				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId
										 AND (tTDPGM.ChannelId = @ChannelId
											  OR (tTDPGM.ChannelId = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							CASE 
								WHEN @HYChannel = @HYChannelNL THEN @ChannelIdNL
								ELSE @ChannelId
							END
						)
					END
				END 
			END

			COMMIT

			PRODUCT_END:
			
			------------------------------------------------------------------------------------------ 
			-- ProductSize
				
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [integration].[usp_ImportUpdateProductSizesLekmer]
				@HYChannel,
				@ProductId,
				@HYArticleNoColorSize,
				@HYSizeId,
				@HYSizeValue,
				@NoInStock,
				@Weight,
				@StockStatusId
			
		END TRY
		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
				
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)

		END CATCH

		FETCH NEXT FROM cur_product 
			INTO 
				@HYArticleNoFull,
				@ArticleTitle,
				@HYSizeValue,
				@Price,
				@NoInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@HYBrandId,
				@EanCode,
				@LekmerErpId,
				@Weight,
				@PossibleToDisplay,
				@StockStatusErpId

	END
	
	CLOSE cur_product
	DEALLOCATE cur_product
	
	EXEC [integration].[usp_UpdateProductBrandLekmer]
	--exec [integration].[usp_FokRestrictionsProducts] -- Outdated, soon to be removed, [usp_ChannelRestrictionsProducts] in its place
	EXEC [integration].[usp_ChannelRestrictionsProducts]
	EXEC [integration].[usp_REATagProduct]
	
	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]
	
	-- Update stock status /active-passive/online-offline /*before package update !!!*/
	EXEC [integration].[usp_UpdateProductStatusLekmer]
	
	EXEC [productlek].[pPackageUpdate]
	
	-- Track products chnages to eSales export
	EXEC [integration].[pTrackProductChangesLekmer]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]'
GO
ALTER PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	SELECT
		[p].[ProductId] 'Product.Id', 
		[p].[ItemsInPackage] 'Product.ItemsInPackage',
		[p].[ErpId] 'Product.ErpId',
		[p].[EanCode] 'Product.EanCode',
		[p].[NumberInStock] 'Product.NumberInStock', 
		[p].[CategoryId] 'Product.CategoryId',
		COALESCE([pt].[WebShopTitle], [p].WebShopTitle) 'Product.WebShopTitle',
		COALESCE([pt].[Title], [p].Title) 'Product.Title',
		[p].[ProductStatusId] 'Product.ProductStatusId',
		COALESCE([pt].[Description], [p].[Description]) 'Product.Description',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription]) 'Product.ShortDescription',
		@ChannelId 'Product.ChannelId', 
		@CurrencyId 'Product.CurrencyId',
		@PriceListRegistryId 'Product.PriceListRegistryId',
		[i].*,
		[pssr].[ParentContentNodeId] 'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId] 'Product.TemplateContentNodeId',
		[lp].[Lekmer.BrandId],
		[lp].[Lekmer.IsBookable],
		[lp].[Lekmer.AgeFromMonth],
		[lp].[Lekmer.AgeToMonth],
		[lp].[Lekmer.IsNewFrom],
		[lp].[Lekmer.IsNewTo],
		COALESCE ([lpt].[Product.Measurement], [lp].[Lekmer.Measurement]) 'Lekmer.Measurement',
		[lp].[Lekmer.BatteryTypeId],
		[lp].[Lekmer.NumberOfBatteries],
		[lp].[Lekmer.IsBatteryIncluded],
		[lp].[Lekmer.ExpectedBackInStock],
		[lp].[Lekmer.CreatedDate],
		[lp].[Lekmer.SizeDeviationId],
		[lp].[Lekmer.HasSizes],
		[lp].[Lekmer.LekmerErpId],
		[lp].[Lekmer.ShowVariantRelations],
		[lp].[Lekmer.Weight],
		[lp].[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		[bt].*,
		[sd].*,
		[pu].[ProductUrl.UrlTitle] 'Lekmer.UrlTitle',
		[rp].[Price] 'Lekmer.RecommendedPrice',
		[pli].*
	FROM
		[product].[tProduct] AS p
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON [lpt].[Product.Id] = [lp].[Lekmer.ProductId] AND [lpt].[Language.Id] = @LanguageId
		LEFT JOIN [lekmer].[vBatteryType] bt ON [bt].[BatteryType.Id] = [lp].[Lekmer.BatteryTypeId]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON [sd].[SizeDeviation.Id] = [lp].[Lekmer.SizeDeviationId]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl	ON [pl].[Id] = [p].[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductWithoutStatusFilter]'
GO

ALTER VIEW [lekmer].[vCustomProductWithoutStatusFilter]
AS 
	SELECT
		p.*,
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.[ParentContentNodeId] AS 'Product.ParentContentNodeId',
		i.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductWithoutStatusFilter] p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] AS ssmc ON ssmc.[ChannelId] = c.[Channel.Id]
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON p.[Product.Id] = pssr.[ProductId] AND ssmc.[SiteStructureRegistryId] = pssr.[SiteStructureRegistryId]
		LEFT JOIN [media].[vCustomImage] AS i ON i.[Image.MediaId] = p.[Product.MediaId] AND i.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tProductColor]'
GO
ALTER TABLE [productlek].[tProductColor] ADD CONSTRAINT [FK_tProductColor_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tBatteryType] FOREIGN KEY ([BatteryTypeId]) REFERENCES [lekmer].[tBatteryType] ([BatteryTypeId]) ON DELETE SET NULL
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tSizeDeviation] FOREIGN KEY ([SizeDeviationId]) REFERENCES [lekmer].[tSizeDeviation] ([SizeDeviationId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [productlek].[tProductType] ([ProductTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProductTranslation]'
GO
ALTER TABLE [lekmer].[tLekmerProductTranslation] ADD CONSTRAINT [FK_tLekmerProductTranslation_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductSimilar]'
GO
ALTER TABLE [lekmer].[tProductSimilar] ADD CONSTRAINT [FK_tProductSimilar_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
ALTER TABLE [lekmer].[tProductSimilar] ADD CONSTRAINT [FK_tProductSimilar_tLekmerProduct1] FOREIGN KEY ([SimilarProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductUrl]'
GO
ALTER TABLE [lekmer].[tProductUrl] ADD CONSTRAINT [FK_tProductUrl_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProductIcon]'
GO
ALTER TABLE [lekmer].[tLekmerProductIcon] ADD CONSTRAINT [FK_tLekmerProductIcon_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductUrlHistory]'
GO
ALTER TABLE [lekmer].[tProductUrlHistory] ADD CONSTRAINT [FK_tProductUrlHistory_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] ADD CONSTRAINT [FK_tProductPopularity_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] ADD CONSTRAINT [FK_tProductSize_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
ALTER TABLE [lekmer].[tProductSize] ADD CONSTRAINT [FK_tProductSize_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tCartItemPackageElement]'
GO
ALTER TABLE [orderlek].[tCartItemPackageElement] ADD CONSTRAINT [FK_tCartItemPackageElement_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tMonitorProduct]'
GO
ALTER TABLE [lekmer].[tMonitorProduct] ADD CONSTRAINT [FK_tMonitorProduct_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartItemOption]'
GO
ALTER TABLE [campaignlek].[tCartItemOption] ADD CONSTRAINT [FK_tCartItemOption_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerCartItem]'
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [FK_tLekmerCartItem_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
