SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [order].[tOrderItemProduct]'
GO
ALTER TABLE [order].[tOrderItemProduct] ADD
[IsDropShip] [bit] NOT NULL CONSTRAINT [DF_tOrderItemProduct_IsDropShip] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vOrderItem]'
GO
ALTER VIEW [order].[vOrderItem]
AS
SELECT
	[oi].[OrderItemId]					AS 'OrderItem.OrderItemId',
	[oi].[OrderId]						AS 'OrderItem.OrderId',
	[oi].[Quantity]						AS 'OrderItem.Quantity',
	[oi].[OriginalPriceIncludingVat]	AS 'OrderItem.OriginalPriceIncludingVat',
	[oi].[ActualPriceIncludingVat]		AS 'OrderItem.ActualPriceIncludingVat',
	[oi].[VAT]							AS 'OrderItem.VAT',
	[oip].[ProductId]					AS 'OrderItem.ProductId',
	[oip].[ErpId]						AS 'OrderItem.ErpId',
	[oip].[EanCode]						AS 'OrderItem.EanCode',
	[oip].[Title]						AS 'OrderItem.Title',
	[oip].[ProductTypeId]				AS 'OrderItem.ProductTypeId',
	[oip].[IsDropShip]					AS 'OrderItem.IsDropShip',
	[ois].*
FROM 
	[order].[tOrderItem] oi
	INNER JOIN [order].[tOrderItemProduct] oip ON [oi].[OrderItemId] = [oip].[OrderItemId]
	INNER JOIN [order].[vCustomOrderItemStatus] ois ON [oi].[OrderItemStatusId] = [ois].[OrderItemStatus.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrderItem]'
GO
ALTER VIEW [order].[vCustomOrderItem]
AS
	SELECT
		[oi].*,
		[ois].[OrderItemId]	AS 'OrderItemSize.OrderItemId',
		[ois].[SizeId]		AS 'OrderItemSize.SizeId',
		[ois].[ErpId]		AS 'OrderItemSize.ErpId',
		[ps].*,
		[ci].*,
		[lp].[BrandId]
	FROM
		[order].[vOrderItem] oi
		LEFT JOIN [lekmer].[tOrderItemSize] ois ON [oi].[OrderItem.OrderItemId] = [ois].[OrderItemId]
		LEFT JOIN [lekmer].[vProductSize] ps ON [oi].[OrderItem.ProductId] = [ps].[ProductSize.ProductId] AND [ois].[SizeId] = [ps].[ProductSize.SizeId]
		LEFT JOIN [product].[tProduct] p ON [oi].[OrderItem.ProductId] = [p].[ProductId]
		LEFT JOIN [media].[vCustomImageSecure] ci ON [ci].[Image.MediaId] = [p].[MediaId]
		LEFT JOIN [lekmer].[tLekmerProduct] lp ON [oi].[OrderItem.ProductId] = [lp].[ProductId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCategory]'
GO


ALTER VIEW [product].[vCategory]
AS
SELECT     
	c.CategoryId 'Category.Id',
	c.ParentCategoryId 'Category.ParentCategoryId',
	COALESCE (ct.Title, c.Title) 'Category.Title', 
	c.ErpId 'Category.ErpId',
	t.LanguageId 'Category.LanguageId',
	t.[ChannelId] 'Category.ChannelId'
FROM
	[product].[tCategory] c
	CROSS JOIN [core].[tChannel] t
	LEFT JOIN [product].[tCategoryTranslation] ct ON ct.CategoryId = c.CategoryId AND ct.LanguageId = t.LanguageId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategory]'
GO

ALTER VIEW [product].[vCustomCategory]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		COALESCE ([lct].[PackageInfo], [l].[PackageInfo]) AS [LekmerCategory.PackageInfo],
		[l].[MonitorThreshold] AS [LekmerCategory.MonitorThreshold],
		[l].[MaxQuantityPerOrder] AS [LekmerCategory.MaxQuantityPerOrder],
		[l].[DeliveryTimeId] AS [LekmerCategory.DeliveryTimeId],
		[dt].*
	FROM
		[product].[vCategory] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]
		LEFT JOIN [productlek].[tLekmerCategoryTranslation] lct ON [lct].[CategoryId] = [c].[Category.Id] AND [lct].[LanguageId] = [c].[Category.LanguageId]
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [l].[DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [c].[Category.ChannelId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCategoryView]'
GO


ALTER VIEW [product].[vCategoryView]
AS
	SELECT     
		c.*,
		r.[CategoryRegistry.ProductParentContentNodeId],
		r.[CategoryRegistry.ProductTemplateContentNodeId]
	FROM
		[product].[vCustomCategory] AS c
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] mch ON mch.[ChannelId] = c.[Category.ChannelId]
		LEFT JOIN [product].[vCustomCategorySiteStructureRegistry] r
			ON mch.[SiteStructureRegistryId] = r.[CategoryRegistry.SiteStructureRegistryId]
			AND c.[Category.Id] = r.[CategoryRegistry.CategoryId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pCategoryGetViewById]'
GO
ALTER PROCEDURE [product].[pCategoryGetViewById]
	@CategoryId	INT,
	@ChannelId	INT
AS
BEGIN
	SELECT
		c.*
	FROM
		product.vCustomCategoryView AS c
	WHERE
		c.[Category.Id] = @CategoryId
		AND
		[c].[Category.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderItemSave]'
GO
ALTER PROCEDURE [order].[pOrderItemSave]
	@OrderItemId				INT,
	@OrderId					INT,
	@ProductId					INT,
	@Quantity					INT,
	@ActualPriceIncludingVat	DECIMAL(16,2),
	@OriginalPriceIncludingVat	DECIMAL(16,2),
	@VAT						DECIMAL(16,2),
	@ErpId						VARCHAR(50),
	@EanCode					VARCHAR(20),
	@Title						NVARCHAR(50),
	@OrderItemStatusId			INT = 1,
	@ProductTypeId				INT,
	@IsDropShip					BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderItem]
	SET
		OrderId						= @OrderId,
		Quantity					= @Quantity,
		ActualPriceIncludingVat		= @ActualPriceIncludingVat,
		OriginalPriceIncludingVat	= @OriginalPriceIncludingVat,
		VAT							= @VAT,
		OrderItemStatusId			= @OrderItemStatusId
	WHERE
		OrderItemId = @OrderItemId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderItem] (
			OrderId,
			Quantity,
			ActualPriceIncludingVat,
			OriginalPriceIncludingVat,
			VAT,
			OrderItemStatusId
		)
		VALUES (
			@OrderId,
			@Quantity,
			@ActualPriceIncludingVat,
			@OriginalPriceIncludingVat,
			@VAT,
			@OrderItemStatusId
		)

		SET @OrderItemId = SCOPE_IDENTITY();
		
		INSERT INTO [order].[tOrderItemProduct] (
			[OrderItemId],
			[ProductId],
			[ErpId],
			[EanCode],
			[Title],
			[ProductTypeId],
			[IsDropShip]
		)
		VALUES (
			@OrderItemId,
			@ProductId,
			@ErpId,
			@EanCode,
			@Title,
			@ProductTypeId,
			@IsDropShip
		)
	END
	
	RETURN @OrderItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pCategoryGetViewAll]'
GO
ALTER PROCEDURE [product].[pCategoryGetViewAll]
	@ChannelId	INT,
	@LanguageId	INT
AS
BEGIN
	SELECT
		c.*
	FROM
		product.vCustomCategoryView AS c
	WHERE
		[c].[Category.ChannelId] = @ChannelId
		AND
		[c].[Category.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pSupplierSearch]'
GO
CREATE PROCEDURE [productlek].[pSupplierSearch]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		[s].*
	FROM 
		[productlek].[vSupplier] s
	WHERE
		[s].[Supplier.Name] LIKE @SearchCriteria ESCAPE '\'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
