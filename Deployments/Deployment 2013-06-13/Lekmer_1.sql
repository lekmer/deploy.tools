SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [esales]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[tAd]'
GO
CREATE TABLE [esales].[tAd]
(
[AdId] [int] NOT NULL IDENTITY(1, 1),
[FolderId] [int] NOT NULL,
[Key] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DisplayFrom] [date] NULL,
[DisplayTo] [date] NULL,
[ProductCategory] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Gender] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Format] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[SearchTags] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAd] on [esales].[tAd]'
GO
ALTER TABLE [esales].[tAd] ADD CONSTRAINT [PK_tAd] PRIMARY KEY CLUSTERED  ([AdId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[tAdChannel]'
GO
CREATE TABLE [esales].[tAdChannel]
(
[AdChannelId] [int] NOT NULL IDENTITY(1, 1),
[AdId] [int] NOT NULL,
[RegistryId] [int] NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[ShowOnSite] [bit] NOT NULL,
[LandingPageUrl] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[LandingPageId] [int] NULL,
[ImageUrl] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAdChannel] on [esales].[tAdChannel]'
GO
ALTER TABLE [esales].[tAdChannel] ADD CONSTRAINT [PK_tAdChannel] PRIMARY KEY CLUSTERED  ([AdChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tAdChannel_AdId_RegistryId] on [esales].[tAdChannel]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tAdChannel_AdId_RegistryId] ON [esales].[tAdChannel] ([AdId], [RegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[tEsalesRegistry]'
GO
CREATE TABLE [esales].[tEsalesRegistry]
(
[EsalesRegistryId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tEsalesRegistry] on [esales].[tEsalesRegistry]'
GO
ALTER TABLE [esales].[tEsalesRegistry] ADD CONSTRAINT [PK_tEsalesRegistry] PRIMARY KEY CLUSTERED  ([EsalesRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTable]'
GO
CREATE TABLE [lekmer].[tSizeTable]
(
[SizeTableId] [int] NOT NULL IDENTITY(1, 1),
[SizeTableFolderId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Column1Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Column2Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTable] on [lekmer].[tSizeTable]'
GO
ALTER TABLE [lekmer].[tSizeTable] ADD CONSTRAINT [PK_tSizeTable] PRIMARY KEY CLUSTERED  ([SizeTableId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableRow]'
GO
CREATE TABLE [lekmer].[tSizeTableRow]
(
[SizeTableRowId] [int] NOT NULL IDENTITY(1, 1),
[SizeTableId] [int] NOT NULL,
[SizeId] [int] NOT NULL,
[Column1Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Column2Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableRow] on [lekmer].[tSizeTableRow]'
GO
ALTER TABLE [lekmer].[tSizeTableRow] ADD CONSTRAINT [PK_tSizeTableRow] PRIMARY KEY CLUSTERED  ([SizeTableRowId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[tAdFolder]'
GO
CREATE TABLE [esales].[tAdFolder]
(
[AdFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAdFolder] on [esales].[tAdFolder]'
GO
ALTER TABLE [esales].[tAdFolder] ADD CONSTRAINT [PK_tAdFolder] PRIMARY KEY CLUSTERED  ([AdFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableFolder]'
GO
CREATE TABLE [lekmer].[tSizeTableFolder]
(
[SizeTableFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentSizeTableFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableFolder] on [lekmer].[tSizeTableFolder]'
GO
ALTER TABLE [lekmer].[tSizeTableFolder] ADD CONSTRAINT [PK_tSizeTableFolder] PRIMARY KEY CLUSTERED  ([SizeTableFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableIncludeCategory]'
GO
CREATE TABLE [lekmer].[tSizeTableIncludeCategory]
(
[SizeTableId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableIncludeCategory] on [lekmer].[tSizeTableIncludeCategory]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategory] ADD CONSTRAINT [PK_tSizeTableIncludeCategory] PRIMARY KEY CLUSTERED  ([SizeTableId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableIncludeProduct]'
GO
CREATE TABLE [lekmer].[tSizeTableIncludeProduct]
(
[SizeTableId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableIncludeProduct] on [lekmer].[tSizeTableIncludeProduct]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] ADD CONSTRAINT [PK_tSizeTableIncludeProduct] PRIMARY KEY CLUSTERED  ([SizeTableId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableTranslation]'
GO
CREATE TABLE [lekmer].[tSizeTableTranslation]
(
[SizeTableId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Column1Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Column2Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableTranslation] on [lekmer].[tSizeTableTranslation]'
GO
ALTER TABLE [lekmer].[tSizeTableTranslation] ADD CONSTRAINT [PK_tSizeTableTranslation] PRIMARY KEY CLUSTERED  ([SizeTableId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableRowTranslation]'
GO
CREATE TABLE [lekmer].[tSizeTableRowTranslation]
(
[SizeTableRowId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Column1Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Column2Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableRowTranslation] on [lekmer].[tSizeTableRowTranslation]'
GO
ALTER TABLE [lekmer].[tSizeTableRowTranslation] ADD CONSTRAINT [PK_tSizeTableRowTranslation] PRIMARY KEY CLUSTERED  ([SizeTableRowId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[tEsalesModuleChannel]'
GO
CREATE TABLE [esales].[tEsalesModuleChannel]
(
[ChannelId] [int] NOT NULL,
[EsalesRegistryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tEsalesModuleChannel] on [esales].[tEsalesModuleChannel]'
GO
ALTER TABLE [esales].[tEsalesModuleChannel] ADD CONSTRAINT [PK_tEsalesModuleChannel] PRIMARY KEY CLUSTERED  ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tEsalesModuleChannel_EsalesRegistryId] on [esales].[tEsalesModuleChannel]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tEsalesModuleChannel_EsalesRegistryId] ON [esales].[tEsalesModuleChannel] ([EsalesRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableIncludeBrand]'
GO
CREATE TABLE [lekmer].[tSizeTableIncludeBrand]
(
[SizeTableId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableIncludeBrand] on [lekmer].[tSizeTableIncludeBrand]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeBrand] ADD CONSTRAINT [PK_tSizeTableIncludeBrand] PRIMARY KEY CLUSTERED  ([SizeTableId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableGetByProduct]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetByProduct]
	@ProductId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SizeTableId INT
	
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeProduct] WHERE [ProductId] = @ProductId)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	/*--------------------------------------------------------------------------------------------------------------------------*/

	DECLARE @CategoryFirstLevelId INT
	DECLARE @CategorySecondLevelId INT
	DECLARE @CategoryThirdLevelId INT

	SET @CategoryThirdLevelId = (SELECT [CategoryId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId)
	-- Size table include category 3 level
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	SET @CategorySecondLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	-- Size table include category 2 level
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategory] WHERE [CategoryId] = @CategorySecondLevelId)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	SET @CategoryFirstLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategorySecondLevelId)		
	-- Size table include category 1 level
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategory] WHERE [CategoryId] = @CategoryFirstLevelId)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	/*--------------------------------------------------------------------------------------------------------------------------*/

	DECLARE @BrandId INT
	SET @BrandId = (SELECT [BrandId] FROM [lekmer].[tLekmerProduct] WHERE [ProductId] = @ProductId)
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeBrand] WHERE [BrandId] = @BrandId)
	RETURN @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableFolderDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderDelete]
	@SizeTableFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [lekmer].[tSizeTableFolder]
	WHERE [SizeTableFolderId] = @SizeTableFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[vAdChannel]'
GO

CREATE VIEW [esales].[vAdChannel]
AS
	SELECT
		[ac].[AdChannelId] 'AdChannel.AdChannelId',
		[ac].[AdId] 'AdChannel.AdId',
		[ac].[RegistryId] 'AdChannel.RegistryId',
		[ac].[Title] 'AdChannel.Title',
		[ac].[ShowOnSite] 'AdChannel.ShowOnSite',
		[ac].[LandingPageUrl] 'AdChannel.LandingPageUrl',
		[ac].[LandingPageId] 'AdChannel.LandingPageId',
		[ac].[ImageUrl] 'AdChannel.ImageUrl',
		[ac].[ImageId] 'AdChannel.ImageId'
	FROM 
		[esales].[tAdChannel] ac

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdChannelGetAllByAd]'
GO
CREATE PROCEDURE [esales].[pAdChannelGetAllByAd]
	@AdId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[ac].*
	FROM
		[esales].[vAdChannel] ac
	WHERE
		[ac].[AdChannel.AdId] = @AdId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vSizeTableRowSecure]'
GO

CREATE VIEW [lekmer].[vSizeTableRowSecure]
AS
	SELECT
		[st].[SizeTableRowId]		'SizeTableRow.SizeTableRowId',
		[st].[SizeTableId]			'SizeTableRow.SizeTableId',
		[st].[SizeId]				'SizeTableRow.SizeId',
		[st].[Column1Value]			'SizeTableRow.Column1Value',
		[st].[Column2Value]			'SizeTableRow.Column2Value',
		[st].[Ordinal]				'SizeTableRow.Ordinal'
	FROM 
		[lekmer].[tSizeTableRow] st
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableRowGetAllBySizeTable]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowGetAllBySizeTable]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		st.*
	FROM
		[lekmer].[vSizeTableRowSecure] st
	WHERE
		st.[SizeTableRow.SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableIncludesGetAll]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludesGetAll]
	@SizeTableId	INT
AS
BEGIN
	SELECT [ProductId] FROM [lekmer].[tSizeTableIncludeProduct] WHERE [SizeTableId] = @SizeTableId
	SELECT [CategoryId] FROM [lekmer].[tSizeTableIncludeCategory] WHERE [SizeTableId] = @SizeTableId
	SELECT [BrandId] FROM [lekmer].[tSizeTableIncludeBrand] WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vSizeTableRowTranslation]'
GO
CREATE VIEW [lekmer].[vSizeTableRowTranslation]
AS
SELECT 
	[SizeTableRowId]	AS 'SizeTableRowTranslation.SizeTableRowId',
	[LanguageId]		AS 'SizeTableRowTranslation.LanguageId',
	[Column1Value]		AS 'SizeTableRowTranslation.Column1Value',
	[Column2Value]		AS 'SizeTableRowTranslation.Column2Value'
FROM 
	[lekmer].[tSizeTableRowTranslation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableRowTranslationGetAll]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowTranslationGetAll]
	@SizeTableRowId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [strt].[SizeTableRowTranslation.SizeTableRowId],
		[strt].[SizeTableRowTranslation.LanguageId],
		[strt].[SizeTableRowTranslation.Column1Value],
		[strt].[SizeTableRowTranslation.Column2Value]
	FROM
	    [lekmer].[vSizeTableRowTranslation] strt
	WHERE 
		strt.[SizeTableRowTranslation.SizeTableRowId] = @SizeTableRowId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdDelete]'
GO

CREATE PROCEDURE [esales].[pAdDelete]
	@AdId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [esales].[tAd]
	WHERE [AdId] = @AdId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vSizeTableRow]'
GO
CREATE VIEW [lekmer].[vSizeTableRow]
AS
	SELECT
		[st].[SizeTableRowId]	'SizeTableRow.SizeTableRowId',
		[st].[SizeTableId]		'SizeTableRow.SizeTableId',
		[st].[SizeId]			'SizeTableRow.SizeId',
		COALESCE ([stt].[Column1Value], [st].[Column1Value]) 'SizeTableRow.Column1Value',
		COALESCE ([stt].[Column2Value], [st].[Column2Value]) 'SizeTableRow.Column2Value',
		[st].[Ordinal]			'SizeTableRow.Ordinal',
		[l].[LanguageId]		'SizeTableRow.LanguageId'
	FROM 
		[lekmer].[tSizeTableRow] st
		CROSS JOIN [core].[tLanguage] AS l
		LEFT JOIN [lekmer].[tSizeTableRowTranslation] AS stt ON [stt].[SizeTableRowId] = [st].[SizeTableRowId] AND [stt].[LanguageId] = l.[LanguageId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableSave]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableSave]
	@SizeTableId		INT,
	@SizeTableFolderId	INT,
	@CommonName			VARCHAR(50),
	@Title				NVARCHAR(50),
	@Description		NVARCHAR(MAX),
	@Column1Title		NVARCHAR(50),
	@Column2Title		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tSizeTable]
		WHERE [Title] = @Title
			AND [SizeTableId] <> @SizeTableId
			AND	[SizeTableFolderId] = @SizeTableFolderId
	)
	RETURN -1
	
	-- Same common names are not allowed (return -2)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tSizeTable]
		WHERE [CommonName] = @CommonName
			AND [SizeTableId] <> @SizeTableId
	)
	RETURN -2
	
	UPDATE 
		[lekmer].[tSizeTable]
	SET 
		[SizeTableFolderId] = @SizeTableFolderId,
		[CommonName] = @CommonName,
		[Title] = @Title,
		[Description] = @Description,
		[Column1Title] = @Column1Title,
		[Column2Title] = @Column2Title
	WHERE
		[SizeTableId] = @SizeTableId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tSizeTable] (
			[SizeTableFolderId],
			[CommonName],
			[Title],
			[Description],
			[Column1Title],
			[Column2Title]
		)
		VALUES (
			@SizeTableFolderId,
			@CommonName,
			@Title,
			@Description,
			@Column1Title,
			@Column2Title
		)

		SET @SizeTableId = SCOPE_IDENTITY()
	END

	RETURN @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableRowTranslationDeleteBySizeTable]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowTranslationDeleteBySizeTable]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [strt]
	FROM [lekmer].[tSizeTableRowTranslation] strt
	INNER JOIN [lekmer].[tSizeTableRow] st ON [st].[SizeTableRowId] = [strt].[SizeTableRowId]
	WHERE [st].[SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableRowDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pSizeTableRowTranslationDeleteBySizeTable] @SizeTableId

	DELETE FROM [lekmer].[tSizeTableRow]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdIsUniqueKey]'
GO

CREATE PROCEDURE [esales].[pAdIsUniqueKey]
	@AdId INT,
	@Key NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	-- Same keys are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [esales].[tAd]
		WHERE
			[Key] = @Key
			AND
			[AdId] <> @AdId
	)
	RETURN -1

	RETURN 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdFolderDelete]'
GO

CREATE PROCEDURE [esales].[pAdFolderDelete]
	@AdFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [esales].[tAdFolder]
	WHERE [AdFolderId] = @AdFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdChannelSave]'
GO


CREATE PROCEDURE [esales].[pAdChannelSave]
	@AdChannelId INT,
	@AdId INT,
	@RegistryId INT,
	@Title NVARCHAR(250),
	@ShowOnSite BIT,
	@LandingPageUrl NVARCHAR(MAX),
	@LandingPageId INT,
	@ImageUrl NVARCHAR(MAX),
	@ImageId INT
AS
BEGIN
	SET NOCOUNT ON

	IF @AdChannelId IS NOT NULL
		UPDATE
			[esales].[tAdChannel]
		SET
			[AdId] = @AdId,
			[RegistryId] = @RegistryId,
			[Title] = @Title,
			[ShowOnSite] = @ShowOnSite,
			[LandingPageUrl] = @LandingPageUrl,
			[LandingPageId] = @LandingPageId,
			[ImageUrl] = @ImageUrl,
			[ImageId] = @ImageId
		WHERE
			[AdChannelId] = @AdChannelId

	IF  @AdChannelId IS NULL OR @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [esales].[tAdChannel] (
			[AdId],
			[RegistryId],
			[Title],
			[ShowOnSite],
			[LandingPageUrl],
			[LandingPageId],
			[ImageUrl],
			[ImageId]
		)
		VALUES (
			@AdId,
			@RegistryId,
			@Title,
			@ShowOnSite,
			@LandingPageUrl,
			@LandingPageId,
			@ImageUrl,
			@ImageId
		)

		SET @AdChannelId = SCOPE_IDENTITY()
	END

	RETURN @AdChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vSizeTableFolder]'
GO
CREATE VIEW [lekmer].[vSizeTableFolder]
AS
	SELECT
		[stf].[SizeTableFolderId]		'SizeTableFolder.SizeTableFolderId',
		[stf].[ParentSizeTableFolderId]	'SizeTableFolder.ParentSizeTableFolderId',
		[stf].[Title]					'SizeTableFolder.Title'
	FROM 
		[lekmer].[tSizeTableFolder] stf
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableFolderGetById]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderGetById]
	@SizeTableFolderId	INT
AS
BEGIN
	SELECT
		[stf].*
	FROM
		[lekmer].[vSizeTableFolder] stf
	WHERE
		[stf].[SizeTableFolder.SizeTableFolderId] = @SizeTableFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableFolderSave]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderSave]
	@SizeTableFolderId INT,
	@ParentSizeTableFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tSizeTableFolder]
		WHERE [Title] = @Title
			AND [SizeTableFolderId] <> @SizeTableFolderId
			AND	((@ParentSizeTableFolderId IS NULL AND [ParentSizeTableFolderId] IS NULL)
				OR [ParentSizeTableFolderId] = @ParentSizeTableFolderId)
	)
	RETURN -1
	
	UPDATE 
		[lekmer].[tSizeTableFolder]
	SET 
		[ParentSizeTableFolderId] = @ParentSizeTableFolderId,
		[Title] = @Title
	WHERE
		[SizeTableFolderId] = @SizeTableFolderId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tSizeTableFolder] (
			[ParentSizeTableFolderId],
			[Title]
		)
		VALUES (
			@ParentSizeTableFolderId,
			@Title
		)

		SET @SizeTableFolderId = SCOPE_IDENTITY()
	END

	RETURN @SizeTableFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableFolderGetTree]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderGetTree]
	@SelectedId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tTree TABLE (Id INT, ParentId INT, Title NVARCHAR(50))

	INSERT INTO @tTree
	SELECT [stf].*
	FROM [lekmer].[vSizeTableFolder] AS stf
	WHERE [stf].[SizeTableFolder.ParentSizeTableFolderId] IS NULL

	IF (@SelectedId IS NOT NULL)
	BEGIN
		INSERT INTO @tTree
		SELECT [stf].*
		FROM [lekmer].[vSizeTableFolder] AS stf
		WHERE [stf].[SizeTableFolder.ParentSizeTableFolderId] = @SelectedId

		DECLARE @ParentId INT
		WHILE (@SelectedId IS NOT NULL)
		BEGIN
			SET @ParentId = (SELECT [stf].[SizeTableFolder.ParentSizeTableFolderId]
							 FROM [lekmer].[vSizeTableFolder] AS stf
							 WHERE [stf].[SizeTableFolder.SizeTableFolderId] = @SelectedId)
							 
			INSERT INTO @tTree
			SELECT [stf].*
			FROM [lekmer].[vSizeTableFolder] AS stf
			WHERE [stf].[SizeTableFolder.ParentSizeTableFolderId] = @ParentId
			
			SET @SelectedId = @ParentId
		END
	END
		
	SELECT
		Id,
		ParentId,
		Title,
		CAST((CASE WHEN EXISTS (SELECT 1 FROM [lekmer].[vSizeTableFolder] AS stf
								WHERE [stf].[SizeTableFolder.ParentSizeTableFolderId] = Id)
		THEN 1 ELSE 0 END) AS BIT) AS 'HasChildren'
	FROM
		@tTree
	ORDER BY
		ParentId ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vSizeTableSecure]'
GO

CREATE VIEW [lekmer].[vSizeTableSecure]
AS
	SELECT
		[st].[SizeTableId]			'SizeTable.SizeTableId',
		[st].[SizeTableFolderId]	'SizeTable.SizeTableFolderId',
		[st].[CommonName]			'SizeTable.CommonName',
		[st].[Title]				'SizeTable.Title',
		[st].[Description]			'SizeTable.Description',
		[st].[Column1Title]			'SizeTable.Column1Title',
		[st].[Column2Title]			'SizeTable.Column2Title'
	FROM 
		[lekmer].[tSizeTable] st
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableGetAllByFolder]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetAllByFolder]
	@SizeTableFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[st].*
	FROM
		[lekmer].[vSizeTableSecure] st
	WHERE
		[st].[SizeTable.SizeTableFolderId] = @SizeTableFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableGetByIdSecure]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetByIdSecure]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[st].*
	FROM
		[lekmer].[vSizeTableSecure] st
	WHERE
		[st].[SizeTable.SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableSearch]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableSearch]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		[st].*
	FROM 
		[lekmer].[vSizeTableSecure] st 
	WHERE
		[st].[SizeTable.Title] LIKE @SearchCriteria ESCAPE '\'
		OR [st].[SizeTable.CommonName] LIKE @SearchCriteria ESCAPE '\'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[vAd]'
GO

CREATE VIEW [esales].[vAd]
AS
	SELECT
		[a].[AdId] 'Ad.AdId',
		[a].[FolderId] 'Ad.FolderId',
		[a].[Key] 'Ad.Key',
		[a].[Title] 'Ad.Title',
		[a].[DisplayFrom] 'Ad.DisplayFrom',
		[a].[DisplayTo] 'Ad.DisplayTo',
		[a].[ProductCategory] 'Ad.ProductCategory',
		[a].[Gender] 'Ad.Gender',
		[a].[Format] 'Ad.Format',
		[a].[SearchTags] 'Ad.SearchTags',
		[a].[CreatedDate] 'Ad.CreatedDate',
		[a].[UpdatedDate] 'Ad.UpdatedDate'
	FROM 
		[esales].[tAd] a

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdGetById]'
GO
CREATE PROCEDURE [esales].[pAdGetById]
	@AdId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[a].*
	FROM
		[esales].[vAd] a
	WHERE
		[a].[Ad.AdId] = @AdId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vSizeTable]'
GO

CREATE VIEW [lekmer].[vSizeTable]
AS
	SELECT
		[st].[SizeTableId]			'SizeTable.SizeTableId',
		[st].[SizeTableFolderId]	'SizeTable.SizeTableFolderId',
		[st].[CommonName]			'SizeTable.CommonName',
		COALESCE ([stt].[Title], [st].[Title]) 'SizeTable.Title',
		COALESCE ([stt].[Description], [st].[Description]) 'SizeTable.Description',
		COALESCE ([stt].[Column1Title], [st].[Column1Title]) 'SizeTable.Column1Title',
		COALESCE ([stt].[Column2Title], [st].[Column2Title]) 'SizeTable.Column2Title',
		[l].[LanguageId]			'SizeTable.LanguageId'
	FROM 
		[lekmer].[tSizeTable] st
		CROSS JOIN [core].[tLanguage] AS l
		LEFT JOIN [lekmer].[tSizeTableTranslation] AS stt ON [stt].[SizeTableId] = [st].[SizeTableId] AND [stt].[LanguageId] = l.[LanguageId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableGetById]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetById]
	@LanguageId		INT,
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vSizeTable]
	WHERE
		[SizeTable.SizeTableId] = @SizeTableId
		AND [SizeTable.LanguageId] = @LanguageId
		
	SELECT
		*
	FROM
		[lekmer].[vSizeTableRow]
	WHERE
		[SizeTableRow.SizeTableId] = @SizeTableId
		AND [SizeTableRow.LanguageId] = @LanguageId
	ORDER BY [SizeTableRow.Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdSave]'
GO

CREATE PROCEDURE [esales].[pAdSave]
	@AdId INT,
	@FolderId INT,
	@Key NVARCHAR(50),
	@Title NVARCHAR(250),
	@DisplayFrom DATETIME,
	@DisplayTo DATETIME,
	@ProductCategory NVARCHAR(250),
	@Gender NVARCHAR(250),
	@Format NVARCHAR(250),
	@SearchTags NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON

	-- Same keys are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [esales].[tAd]
		WHERE
			[Key] = @Key
			AND
			[AdId] <> @AdId
	)
	RETURN -1

	IF @AdId IS NOT NULL
		UPDATE
			[esales].[tAd]
		SET
			[FolderId] = @FolderId,
			[Key] = @Key,
			[Title] = @Title,
			[DisplayFrom] = @DisplayFrom,
			[DisplayTo] = @DisplayTo,
			[ProductCategory] = @ProductCategory,
			[Gender] = @Gender,
			[Format] = @Format,
			[SearchTags] = @SearchTags,
			[UpdatedDate] = GETDATE()
		WHERE
			[AdId] = @AdId

	IF  @AdId IS NULL OR @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [esales].[tAd] (
			[FolderId],
			[Key],
			[Title],
			[DisplayFrom],
			[DisplayTo],
			[ProductCategory],
			[Gender],
			[Format],
			[SearchTags],
			[CreatedDate],
			[UpdatedDate]
		)
		VALUES (
			@FolderId,
			@Key,
			@Title,
			@DisplayFrom,
			@DisplayTo,
			@ProductCategory,
			@Gender,
			@Format,
			@SearchTags,
			GETDATE(),
			GETDATE()
		)

		SET @AdId = SCOPE_IDENTITY()
	END

	RETURN @AdId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[vEsalesRegistry]'
GO


CREATE VIEW [esales].[vEsalesRegistry]
AS
	SELECT
		[er].[EsalesRegistryId]	'EsalesRegistry.EsalesRegistryId',
		[er].[CommonName]		'EsalesRegistry.CommonName',
		[er].[Title]			'EsalesRegistry.Title',
		[emc].[ChannelId] AS	'EsalesRegistry.ChannelId',
		[ssmc].[SiteStructureRegistryId] AS 'EsalesRegistry.SiteStructureRegistryId'
	FROM
		[esales].[tEsalesRegistry] er
		LEFT OUTER JOIN [esales].[tEsalesModuleChannel] emc ON [emc].[EsalesRegistryId] = [er].[EsalesRegistryId]
		LEFT OUTER JOIN [sitestructure].[tSiteStructureModuleChannel] ssmc ON [ssmc].[ChannelId] = [emc].[ChannelId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableTranslationSave]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableTranslationSave]
	@SizeTableId	INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(MAX),
	@Column1Title	NVARCHAR(50),
	@Column2Title	NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tSizeTableTranslation]
	SET
		[Title] = @Title,
		[Description] = @Description,
		[Column1Title] = @Column1Title,
		[Column2Title] = @Column2Title
	WHERE
		[SizeTableId] = @SizeTableId 
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [lekmer].[tSizeTableTranslation] (
			[SizeTableId],
			[LanguageId],
			[Title],
			[Description],
			[Column1Title],
			[Column2Title]
		)
		VALUES (
			@SizeTableId,
			@LanguageId,
			@Title,
			@Description,
			@Column1Title,
			@Column2Title
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableIncludeBrandDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludeBrandDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tSizeTableIncludeBrand]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pEsalesRegistryGetAll]'
GO
CREATE PROCEDURE [esales].[pEsalesRegistryGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		er.*
	FROM
		[esales].[vEsalesRegistry] er
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableRowSave]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowSave]
	@SizeTableId		INT,
	@SizeId				INT,
	@Column1Value		NVARCHAR(50),
	@Column2Value		NVARCHAR(50),
	@Ordinal			INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [lekmer].[tSizeTableRow] (
		[SizeTableId],
		[SizeId],
		[Column1Value],
		[Column2Value],
		[Ordinal]
	)
	VALUES (
		@SizeTableId,
		@SizeId,
		@Column1Value,
		@Column2Value,
		@Ordinal
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[vAdFolder]'
GO


CREATE VIEW [esales].[vAdFolder]
AS
	SELECT
		[af].[AdFolderId] 'AdFolder.AdFolderId',
		[af].[ParentFolderId] 'AdFolder.ParentFolderId',
		[af].[Title] 'AdFolder.Title'
	FROM 
		[esales].[tAdFolder] af

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdFolderGetAll]'
GO
CREATE PROCEDURE [esales].[pAdFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rf.*
	FROM
		[esales].[vAdFolder] rf
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableIncludesClear]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludesClear]
	@ProductIds		VARCHAR(MAX),
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DELETE ip FROM [lekmer].[tSizeTableIncludeProduct] ip
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter)) p ON [p].[ID] = [ip].[ProductId]

	DELETE ic FROM [lekmer].[tSizeTableIncludeCategory] ic
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter)) c ON [c].[ID] = [ic].[CategoryId]
	
	DELETE ib FROM [lekmer].[tSizeTableIncludeBrand] ib
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter)) b ON [b].[ID] = [ib].[BrandId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableIncludeCategoryDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludeCategoryDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tSizeTableIncludeCategory]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableIncludeProductDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludeProductDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tSizeTableIncludeProduct]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableIncludesSave]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludesSave]
	@SizeTableId	INT,
	@ProductIds		VARCHAR(MAX),
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	EXEC [lekmer].[pSizeTableIncludeProductDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeCategoryDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeBrandDelete] @SizeTableId

	EXEC [lekmer].[pSizeTableIncludesClear] @ProductIds, @CategoryIds, @BrandIds, @Delimiter
	
	INSERT [lekmer].[tSizeTableIncludeProduct] ([SizeTableId], [ProductId])
	SELECT @SizeTableId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p
	
	INSERT [lekmer].[tSizeTableIncludeCategory] ([SizeTableId], [CategoryId])
	SELECT @SizeTableId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) p
	
	INSERT [lekmer].[tSizeTableIncludeBrand] ([SizeTableId], [BrandId])
	SELECT @SizeTableId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) p
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableFolderGetAllByParent]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SELECT
		[stf].*
	FROM
		[lekmer].[vSizeTableFolder] stf
	WHERE
		([stf].[SizeTableFolder.ParentSizeTableFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		([stf].[SizeTableFolder.ParentSizeTableFolderId] IS NULL AND @ParentId IS NULL)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableGetAll]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[st].*
	FROM
		[lekmer].[vSizeTableSecure] st
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableFolderGetAll]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		stf.*
	FROM
		[lekmer].[vSizeTableFolder] stf
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdFolderSave]'
GO

CREATE PROCEDURE [esales].[pAdFolderSave]
	@AdFolderId INT,
	@ParentFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [esales].[tAdFolder]
		WHERE [Title] = @Title
			AND [AdFolderId] <> @AdFolderId
			AND	(
				(@ParentFolderId IS NULL AND [ParentFolderId] IS NULL)
				OR
				[ParentFolderId] = @ParentFolderId
				)
	)
	RETURN -1

	UPDATE
		[esales].[tAdFolder]
	SET
		[ParentFolderId] = @ParentFolderId,
		[Title] = @Title
	WHERE
		[AdFolderId] = @AdFolderId

	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [esales].[tAdFolder] (
			[ParentFolderId],
			[Title]
		)
		VALUES (
			@ParentFolderId,
			@Title
		)

		SET @AdFolderId = SCOPE_IDENTITY()
	END

	RETURN @AdFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdFolderGetAllByParent]'
GO
CREATE PROCEDURE [esales].[pAdFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rf.*
	FROM
		[esales].[vAdFolder] rf
	WHERE
		(rf.[AdFolder.ParentFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		(rf.[AdFolder.ParentFolderId] IS NULL AND @ParentId IS NULL)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdChannelDeleteAllByAd]'
GO

CREATE PROCEDURE [esales].[pAdChannelDeleteAllByAd]
	@AdId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [esales].[tAdChannel]
	WHERE [AdId] = @AdId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableGetByProductSecure]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetByProductSecure]
	@ProductId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @SizeTableId INT
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeProduct] WHERE [ProductId] = @ProductId)
	RETURN @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vSizeTableTranslation]'
GO

CREATE VIEW [lekmer].[vSizeTableTranslation]
AS
SELECT 
	[SizeTableId] AS 'SizeTableTranslation.SizeTableId',
	[LanguageId] AS 'SizeTableTranslation.LanguageId',
	[Title] AS 'SizeTableTranslation.Title',
	[Description] AS 'SizeTableTranslation.Description',
	[Column1Title] AS 'SizeTableTranslation.Column1Title',
	[Column2Title] AS 'SizeTableTranslation.Column2Title'
FROM 
	[lekmer].[tSizeTableTranslation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableTranslationGetAll]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableTranslationGetAll]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
	    [stt].*
	FROM
	    [lekmer].[vSizeTableTranslation] stt
	WHERE 
		stt.[SizeTableTranslation.SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableGetByBrand]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetByBrand]
	@BrandId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @SizeTableId INT
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeBrand] WHERE [BrandId] = @BrandId)
	RETURN @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableGetByCategory]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetByCategory]
	@CategoryId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @CategoryFirstLevelId INT
	DECLARE @CategorySecondLevelId INT
	DECLARE @CategoryThirdLevelId INT
	
	SET @CategoryThirdLevelId = @CategoryId
	DECLARE @SizeTableId INT
	-- Size table include category 3 level
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	SET @CategorySecondLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	-- Size table include category 2 level
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategory] WHERE [CategoryId] = @CategorySecondLevelId)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId
	
	SET @CategoryFirstLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategorySecondLevelId)		
	-- Size table include category 1 level
	SET @SizeTableId = (SELECT [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategory] WHERE [CategoryId] = @CategoryFirstLevelId)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableTranslationDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableTranslationDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [lekmer].[tSizeTableTranslation]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pSizeTableIncludeBrandDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeCategoryDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeProductDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableTranslationDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableRowDelete] @SizeTableId
		
	DELETE FROM [lekmer].[tSizeTable]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdFolderGetTree]'
GO
CREATE PROCEDURE [esales].[pAdFolderGetTree]
	@SelectedId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tTree TABLE (Id INT, ParentId INT, Title NVARCHAR(50))

	INSERT INTO @tTree
	SELECT rf.*
	FROM [esales].[vAdFolder] AS rf
	WHERE rf.[AdFolder.ParentFolderId] IS NULL

	IF (@SelectedId IS NOT NULL)
	BEGIN
		INSERT INTO @tTree
		SELECT rf.*
		FROM [esales].[vAdFolder] AS rf
		WHERE rf.[AdFolder.ParentFolderId] = @SelectedId

		DECLARE @ParentId INT
		WHILE (@SelectedId IS NOT NULL)
		BEGIN
			SET @ParentId = (SELECT rf.[AdFolder.ParentFolderId]
							 FROM [esales].[vAdFolder] AS rf
							 WHERE rf.[AdFolder.AdFolderId] = @SelectedId)

			INSERT INTO @tTree
			SELECT rf.*
			FROM [esales].[vAdFolder] AS rf
			WHERE rf.[AdFolder.ParentFolderId] = @ParentId

			SET @SelectedId = @ParentId
		END
	END

	SELECT
		Id,
		ParentId,
		Title,
		CAST((CASE WHEN EXISTS (SELECT 1 FROM [esales].[vAdFolder] AS rf
								WHERE rf.[AdFolder.ParentFolderId] = Id)
		THEN 1 ELSE 0 END) AS BIT) AS 'HasChildren'
	FROM
		@tTree
	ORDER BY
		ParentId ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [esales].[pAdFolderGetById]'
GO
CREATE PROCEDURE [esales].[pAdFolderGetById]
	@AdFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rf.*
	FROM
		[esales].[vAdFolder] rf
	WHERE
		rf.[AdFolder.AdFolderId] = @AdFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableRowTranslationSave]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowTranslationSave]
	@SizeTableRowId	INT,
	@LanguageId		INT,
	@Column1Value	NVARCHAR(50),
	@Column2Value	NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [lekmer].[tSizeTableRowTranslation] (
		[SizeTableRowId],
		[LanguageId],
		[Column1Value],
		[Column2Value]
	)
	VALUES (
		@SizeTableRowId,
		@LanguageId,
		@Column1Value,
		@Column2Value
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [esales].[tAdChannel]'
GO
ALTER TABLE [esales].[tAdChannel] ADD CONSTRAINT [FK_tAdChannel_tAd] FOREIGN KEY ([AdId]) REFERENCES [esales].[tAd] ([AdId])
ALTER TABLE [esales].[tAdChannel] ADD CONSTRAINT [FK_tAdChannel_tEsalesRegistry] FOREIGN KEY ([RegistryId]) REFERENCES [esales].[tEsalesRegistry] ([EsalesRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableRowTranslation]'
GO
ALTER TABLE [lekmer].[tSizeTableRowTranslation] ADD CONSTRAINT [FK_tSizeTableRowTranslation_tSizeTableRow] FOREIGN KEY ([SizeTableRowId]) REFERENCES [lekmer].[tSizeTableRow] ([SizeTableRowId])
ALTER TABLE [lekmer].[tSizeTableRowTranslation] ADD CONSTRAINT [FK_tSizeTableRowTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableRow]'
GO
ALTER TABLE [lekmer].[tSizeTableRow] ADD CONSTRAINT [FK_tSizeTableRow_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
ALTER TABLE [lekmer].[tSizeTableRow] ADD CONSTRAINT [FK_tSizeTableRow_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [esales].[tAd]'
GO
ALTER TABLE [esales].[tAd] ADD CONSTRAINT [FK_tAd_tAdFolder] FOREIGN KEY ([FolderId]) REFERENCES [esales].[tAdFolder] ([AdFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableIncludeCategory]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategory] ADD CONSTRAINT [FK_tSizeTableIncludeCategory_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
ALTER TABLE [lekmer].[tSizeTableIncludeCategory] ADD CONSTRAINT [FK_tSizeTableIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableIncludeProduct]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] ADD CONSTRAINT [FK_tSizeTableIncludeProduct_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
ALTER TABLE [lekmer].[tSizeTableIncludeProduct] ADD CONSTRAINT [FK_tSizeTableIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableTranslation]'
GO
ALTER TABLE [lekmer].[tSizeTableTranslation] ADD CONSTRAINT [FK_tSizeTableTranslation_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
ALTER TABLE [lekmer].[tSizeTableTranslation] ADD CONSTRAINT [FK_tSizeTableTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableIncludeBrand]'
GO
ALTER TABLE [lekmer].[tSizeTableIncludeBrand] ADD CONSTRAINT [FK_tSizeTableIncludeBrand_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
ALTER TABLE [lekmer].[tSizeTableIncludeBrand] ADD CONSTRAINT [FK_tSizeTableIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTable]'
GO
ALTER TABLE [lekmer].[tSizeTable] ADD CONSTRAINT [FK_tSizeTable_tSizeTableFolder] FOREIGN KEY ([SizeTableFolderId]) REFERENCES [lekmer].[tSizeTableFolder] ([SizeTableFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableFolder]'
GO
ALTER TABLE [lekmer].[tSizeTableFolder] ADD CONSTRAINT [FK_tSizeTableFolder_tSizeTableFolder] FOREIGN KEY ([ParentSizeTableFolderId]) REFERENCES [lekmer].[tSizeTableFolder] ([SizeTableFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [esales].[tAdFolder]'
GO
ALTER TABLE [esales].[tAdFolder] ADD CONSTRAINT [FK_tAdFolder_tAdFolder] FOREIGN KEY ([ParentFolderId]) REFERENCES [esales].[tAdFolder] ([AdFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [esales].[tEsalesModuleChannel]'
GO
ALTER TABLE [esales].[tEsalesModuleChannel] ADD CONSTRAINT [FK_tEsalesModuleChannel_tEsalesRegistry] FOREIGN KEY ([EsalesRegistryId]) REFERENCES [esales].[tEsalesRegistry] ([EsalesRegistryId])
ALTER TABLE [esales].[tEsalesModuleChannel] ADD CONSTRAINT [FK_tEsalesModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
