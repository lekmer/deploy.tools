SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [orderlek].[tCollectorPaymentType]'
GO
CREATE TABLE [orderlek].[tCollectorPaymentType]
(
[CollectorPaymentTypeId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[Code] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[StartFee] [decimal] (16, 2) NOT NULL,
[InvoiceFee] [decimal] (16, 2) NOT NULL,
[MonthlyInterestRate] [decimal] (16, 2) NOT NULL,
[NoOfMonths] [int] NOT NULL,
[MinPurchaseAmount] [decimal] (16, 2) NOT NULL,
[LowestPayment] [decimal] (16, 2) NOT NULL,
[PartPaymentType] [int] NOT NULL,
[InvoiceType] [int] NOT NULL,
[Ordinal] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCollectorPaymentType] on [orderlek].[tCollectorPaymentType]'
GO
ALTER TABLE [orderlek].[tCollectorPaymentType] ADD CONSTRAINT [PK_tCollectorPaymentType] PRIMARY KEY CLUSTERED  ([CollectorPaymentTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tCollectorPaymentType_ChannelId_Code] on [orderlek].[tCollectorPaymentType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tCollectorPaymentType_ChannelId_Code] ON [orderlek].[tCollectorPaymentType] ([ChannelId], [Code])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[tOrderPayment]'
GO
ALTER TABLE [order].[tOrderPayment] ADD
[CollectorStoreId] [int] NULL,
[CollectorPaymentCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vOrderPayment]'
GO


ALTER VIEW [order].[vOrderPayment]
AS
	SELECT
		[OrderPaymentId] AS 'OrderPayment.OrderPaymentId',
		[OrderId] AS 'OrderPayment.OrderId',
		[PaymentTypeId] AS 'OrderPayment.PaymentTypeId',
		[Price] AS 'OrderPayment.Price',
		[Vat] AS 'OrderPayment.Vat',
		[ReferenceId] AS 'OrderPayment.ReferenceId',
		[Captured] AS 'OrderPayment.Captured',
		[KlarnaEID] AS 'OrderPayment.KlarnaEID',
		[KlarnaPClass] AS 'OrderPayment.KlarnaPClass',
		[MaksuturvaCode] AS 'OrderPayment.MaksuturvaCode',
		[QliroClientRef] AS 'OrderPayment.QliroClientRef',
		[QliroPaymentCode] AS 'OrderPayment.QliroPaymentCode',
		[CollectorStoreId] AS 'OrderPayment.CollectorStoreId',
		[CollectorPaymentCode] AS 'OrderPayment.CollectorPaymentCode'
	FROM
		[order].[tOrderPayment]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tCollectorTransaction]'
GO
CREATE TABLE [orderlek].[tCollectorTransaction]
(
[TransactionId] [int] NOT NULL IDENTITY(1, 1),
[StoreId] [int] NOT NULL,
[TransactionTypeId] [int] NOT NULL,
[StatusCode] [int] NULL,
[Created] [datetime] NOT NULL,
[CivicNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Duration] [bigint] NULL,
[OrderId] [int] NULL,
[ProductCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[AvailableReservationAmount] [decimal] (16, 2) NULL,
[InvoiceNo] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[InvoiceStatus] [int] NULL,
[InvoiceUrl] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[FaultCode] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[FaultMessage] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ErrorMessage] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ResponseContent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCollectorTransaction] on [orderlek].[tCollectorTransaction]'
GO
ALTER TABLE [orderlek].[tCollectorTransaction] ADD CONSTRAINT [PK_tCollectorTransaction] PRIMARY KEY CLUSTERED  ([TransactionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorTransactionSaveAddInvoiceResponse]'
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionSaveAddInvoiceResponse]
	@TransactionId INT,
	@StatusCode INT,
	@AvailableReservationAmount DECIMAL(16,2),
	@InvoiceNo VARCHAR(50),
	@InvoiceStatus INT,
	@InvoiceUrl VARCHAR(200),
	@FaultCode NVARCHAR(100),
	@FaultMessage NVARCHAR(MAX),
	@ErrorMessage NVARCHAR(MAX),
	@Duration BIGINT,
	@ResponseContent NVARCHAR(MAX)
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tCollectorTransaction]
	SET
		[StatusCode] = @StatusCode,
		[AvailableReservationAmount] = @AvailableReservationAmount,
		[InvoiceNo] = @InvoiceNo,
		[InvoiceStatus] = @InvoiceStatus,
		[InvoiceUrl] = @InvoiceUrl,
		[FaultCode] = @FaultCode,
		[FaultMessage] = @FaultMessage,
		[ErrorMessage] = @ErrorMessage,
		[Duration] = @Duration,
		[ResponseContent] = @ResponseContent
	WHERE
		[TransactionId] = @TransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorTransactionCreateInvoiceNotification]'
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionCreateInvoiceNotification]
	@StoreId INT,
	@TransactionTypeId INT,
	@ResponseContent NVARCHAR(MAX),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tCollectorTransaction]
			( [StoreId],
			  [TransactionTypeId],
			  [ResponseContent],
			  [Created]
			)
	VALUES
			( @StoreId,
			  @TransactionTypeId,			  
			  @ResponseContent,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorTransactionCreateGetAddress]'
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionCreateGetAddress]
	@StoreId INT,
	@TransactionTypeId INT,
	@CivicNumber VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tCollectorTransaction]
			( [StoreId],
			  [TransactionTypeId],
			  [CivicNumber],
			  [Created]
			)
	VALUES
			( @StoreId,
			  @TransactionTypeId,
			  @CivicNumber,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vCollectorPaymentType]'
GO

CREATE VIEW [orderlek].[vCollectorPaymentType]
AS
SELECT
	[cpt].[CollectorPaymentTypeId] AS 'CollectorPaymentType.Id',
	[cpt].[ChannelId] AS 'CollectorPaymentType.ChannelId',
	[cpt].[Code] AS 'CollectorPaymentType.Code',
	[cpt].[Description] AS 'CollectorPaymentType.Description',
	[cpt].[StartFee] AS 'CollectorPaymentType.StartFee',
	[cpt].[InvoiceFee] AS 'CollectorPaymentType.InvoiceFee',
	[cpt].[MonthlyInterestRate] AS 'CollectorPaymentType.MonthlyInterestRate',
	[cpt].[NoOfMonths] AS 'CollectorPaymentType.NoOfMonths',
	[cpt].[MinPurchaseAmount] AS 'CollectorPaymentType.MinPurchaseAmount',
	[cpt].[LowestPayment] AS 'CollectorPaymentType.LowestPayment',
	[cpt].[PartPaymentType] AS 'CollectorPaymentType.PartPaymentType',
	[cpt].[InvoiceType] AS 'CollectorPaymentType.InvoiceType',
	[cpt].[Ordinal] AS 'CollectorPaymentType.Ordinal'
FROM
	[orderlek].[tCollectorPaymentType] cpt

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorPaymentTypeGetAllByChannel]'
GO
CREATE PROCEDURE [orderlek].[pCollectorPaymentTypeGetAllByChannel]
	@ChannelId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vCollectorPaymentType]
	WHERE
		[CollectorPaymentType.ChannelId] = @ChannelId
	ORDER BY
		[CollectorPaymentType.Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pOrder]'
GO

ALTER PROCEDURE [integration].[pOrder]
	@OrderId int
AS
begin
	set nocount on
	set transaction isolation level read uncommitted
	begin try
		
		--set statistics io on
		--set statistics time on
		
		-- Order
		select 		
			o.OrderId,
            o.FreightCost,
            lo.OptionalFreightCost,
            lo.DiapersFreightCost,
            o.ChannelId as Channel,   
            op.OrderPaymentId, 
            pt.ErpId as PaymentMethod, 
            op.ReferenceId,
            op.Price,            
            --u.VoucherDiscount as VoucherAmount
            a1.ItemsActualPriceIncludingVat - op.Price as VoucherAmount,
			lci.IsCompany, -- NEW
			op.Captured as PaymentCaptured,
			op.[KlarnaEID],
            op.[KlarnaPClass],
            op.[QliroClientRef],
            op.[QliroPaymentCode],
            op.[CollectorStoreId],
            op.[CollectorPaymentCode],
            o.[IP] AS IpAddress,
            o.[CreatedDate]
		from
			[order].[tPaymentType] pt
			inner join [order].[tOrderPayment] op on op.PaymentTypeId = pt.PaymentTypeId
			inner join [order].[tOrder] o on op.OrderId = o.OrderId
			inner join [customerlek].[tCustomerInformation] lci on lci.CustomerId = o.CustomerId
			left join [lekmer].[tLekmerOrder] lo on lo.OrderId = o.OrderId
			cross apply (
				select sum(oi.ActualPriceIncludingVat * oi.Quantity) as 'ItemsActualPriceIncludingVat'
				from [order].[tOrderItem] oi
				where oi.OrderId = o.OrderId
			) as a1
			where
				o.OrderStatusId = 2
				and
				o.OrderId = @OrderId
		
		-- OrderRow // Old
		--select
		--	o.OrderItemId as OrderRowId, 
		--	p.Title, 
		--	o.Quantity, 
		--	p.ProductId,
		--	o.ActualPriceIncludingVat as Price, 
		--	o.OriginalPriceIncludingVat as OrdinaryPrice, 
		--	coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		--from
		--	[order].tOrderItem o
		--	left join [order].tOrderItemProduct p 
		--		on o.OrderItemId = p.OrderItemId
		--	left join lekmer.tOrderItemSize ois
		--		on ois.OrderItemId = o.OrderItemId
		--where
		--		o.OrderId = @OrderId 
				
		-- OrderRow
		select
			o.OrderItemId as OrderRowId, 
			p.Title, 
			o.Quantity, 
			p.ProductId,
			o.ActualPriceIncludingVat as Price, 
			o.OriginalPriceIncludingVat as OrdinaryPrice, 
			coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
			inner join lekmer.tLekmerProduct l
				on p.ProductId = l.ProductId
		where
			o.OrderId = @OrderId 
			and l.ProductTypeId != 2 -- package article
		UNION ALL
		select
			poi.OrderItemId as OrderRowId,
			poip.Title,
			poi.Quantity,
			poip.ProductId,
			poi.PackagePriceIncludingVat as Price,
			poi.OriginalPriceIncludingVat as OrdinaryPrice,
			coalesce(poip.[SizeErpId], poip.ErpId) as HyErpId
		from
			orderlek.tPackageOrderItem poi
			left join orderlek.tPackageOrderItemProduct poip
				on poi.PackageOrderItemId = poip.PackageOrderItemId
		where
			poi.OrderId = @OrderId
			
		-- Customer
		select 		
            c.CustomerId, 
			c.FirstName, 
			c.LastName,
            c.CivicNumber, 
            c.PhoneNumber,
            c.CellPhoneNumber as MobilePhoneNumber, 
            o.Email,
			case g.IsCompany when 1 then '05' else isnull(gt.ErpId, '00') end as Gender --NEW
		from	
			[order].torder o
			inner join [customer].tCustomerInformation c
				on c.CustomerId = o.CustomerId
			left join customerlek.tCustomerInformation g
				on c.CustomerId = g.CustomerId
			left join customerlek.tGenderType gt
				on g.GenderTypeId = gt.GenderTypeId
		where
			o.OrderId = @OrderId
		
		-- BillingAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.BillingAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- DeliveryAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.DeliveryAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- AlternativeAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].tOrder o
			inner join [lekmer].tLekmerOrder lo on lo.OrderId = o.OrderId
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = lo.AlternateAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId


		-- DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join [order].tDeliveryMethod d
				on o.DeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId
			
		-- Optional DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join lekmer.tLekmerOrder lo
				on lo.OrderId = o.OrderId
			inner join [order].tDeliveryMethod d
				on lo.OptionalDeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId
			
		-- Diapers DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join lekmer.tLekmerOrder lo
				on lo.OrderId = o.OrderId
			inner join [order].tDeliveryMethod d
				on lo.DiapersDeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId

	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorPaymentTypeDelete]'
GO

CREATE PROCEDURE [orderlek].[pCollectorPaymentTypeDelete]
	@CollectorPaymentTypeId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [orderlek].[tCollectorPaymentType]
	WHERE [CollectorPaymentTypeId] = @CollectorPaymentTypeId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorPaymentTypeSave]'
GO
CREATE PROCEDURE [orderlek].[pCollectorPaymentTypeSave]
	@CollectorPaymentTypeId INT,
	@ChannelId INT,
	@Code VARCHAR(50),
	@Description NVARCHAR(100),
	@StartFee DECIMAL(16,2),
	@InvoiceFee DECIMAL(16,2),
	@MonthlyInterestRate DECIMAL(16,2),
	@NoOfMonths INT,
	@MinPurchaseAmount DECIMAL(16,2),
	@LowestPayment DECIMAL(16,2),
	@PartPaymentType INT,
	@InvoiceType INT,
	@Ordinal INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Id INT
	
	SET @Id = @CollectorPaymentTypeId
	
	UPDATE
		[orderlek].[tCollectorPaymentType]
	SET
		[Description] = @Description,
		[StartFee] = @StartFee,
		[InvoiceFee] = @InvoiceFee,
		[MonthlyInterestRate] = @MonthlyInterestRate,
		[NoOfMonths] = @NoOfMonths,
		[MinPurchaseAmount]= @MinPurchaseAmount,
		[LowestPayment] = @LowestPayment,
		[PartPaymentType] = @PartPaymentType,
		[InvoiceType] = @InvoiceType,
		[Ordinal] = @Ordinal
	WHERE
		[ChannelId] = @ChannelId
		AND
		[Code] = @Code
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [orderlek].[tCollectorPaymentType]
				( [ChannelId],
				  [Code],
				  [Description],
				  [StartFee],
				  [InvoiceFee],
				  [MonthlyInterestRate],
				  [NoOfMonths],
				  [MinPurchaseAmount],
				  [LowestPayment],
				  [PartPaymentType],
				  [InvoiceType],
				  [Ordinal]
				)
		VALUES
				( @ChannelId,
				  @Code,
				  @Description,
				  @StartFee,
				  @InvoiceFee,
				  @MonthlyInterestRate,
				  @NoOfMonths,
				  @MinPurchaseAmount,
				  @LowestPayment,
				  @PartPaymentType,
				  @InvoiceType,
				  @Ordinal
				)

		SET @Id = SCOPE_IDENTITY()
	END

	RETURN @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorTransactionSaveResult]'
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionSaveResult]
	@TransactionId INT,
	@StatusCode INT,
	@FaultCode NVARCHAR(100),
	@FaultMessage NVARCHAR(MAX),
	@ErrorMessage NVARCHAR(MAX),
	@Duration BIGINT,
	@ResponseContent NVARCHAR(MAX)
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tCollectorTransaction]
	SET
		[StatusCode] = @StatusCode,
		[FaultCode] = @FaultCode,
		[FaultMessage] = @FaultMessage,
		[ErrorMessage] = @ErrorMessage,
		[Duration] = @Duration,
		[ResponseContent] = @ResponseContent
	WHERE
		[TransactionId] = @TransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorTransactionSaveInvoiceNotificationResult]'
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionSaveInvoiceNotificationResult]
	@TransactionId INT,
	@InvoiceNo VARCHAR(50),
	@OrderId INT,
	@InvoiceStatus INT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tCollectorTransaction]
	SET
		[InvoiceNo] = @InvoiceNo,
		[InvoiceStatus] = @InvoiceStatus,
		[OrderId] = @OrderId
	WHERE
		[TransactionId] = @TransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorTransactionCreateAddInvoice]'
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionCreateAddInvoice]
	@StoreId INT,
	@TransactionTypeId INT,
	@CivicNumber VARCHAR(50),
	@OrderId INT,
	@ProductCode VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tCollectorTransaction]
			( [StoreId],
			  [TransactionTypeId],
			  [CivicNumber],
			  [OrderId],
			  [ProductCode],
			  [Created]
			)
	VALUES
			( @StoreId,
			  @TransactionTypeId,			  
			  @CivicNumber,
			  @OrderId,
			  @ProductCode,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderPaymentSave]'
GO
ALTER PROCEDURE [order].[pOrderPaymentSave]
	@OrderPaymentId INT,
	@OrderId INT,
	@PaymentTypeId INT,
	@Price DECIMAL(16,2),
	@Vat DECIMAL(16,2),
	@ReferenceId VARCHAR(50),
	@Captured BIT = NULL,
	@KlarnaEID INT = NULL,
	@KlarnaPClass INT = NULL,
	@MaksuturvaCode VARCHAR(50) = NULL,
	@QliroClientRef VARCHAR(50) = NULL,
	@QliroPaymentCode VARCHAR(50) = NULL,
	@CollectorStoreId INT = NULL,
	@CollectorPaymentCode VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderPayment]
	SET
		[OrderId] = @OrderId,
		[PaymentTypeId] = @PaymentTypeId,
		[Price] = @Price,
		[Vat] = @VAT,
		[ReferenceId] = @ReferenceId,
		[Captured] = @Captured,
		[KlarnaEID] = @KlarnaEID,
		[KlarnaPClass] = @KlarnaPClass,
		[MaksuturvaCode] = @MaksuturvaCode,
		[QliroClientRef] = @QliroClientRef,
		[QliroPaymentCode] = @QliroPaymentCode,
		[CollectorStoreId] = @CollectorStoreId,
		[CollectorPaymentCode] = @CollectorPaymentCode
	WHERE
		[OrderPaymentId] = @OrderPaymentId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderPayment]
		(
			[OrderId],
			[PaymentTypeId],
			[Price],
			[Vat],
			[ReferenceId],
			[Captured],
			[KlarnaEID],
			[KlarnaPClass],
			[MaksuturvaCode],
			[QliroClientRef],
			[QliroPaymentCode],
			[CollectorStoreId],
			[CollectorPaymentCode]
		)
		VALUES
		(
			@OrderId,
			@PaymentTypeId,
			@Price,
			@Vat,
			@ReferenceId,
			@Captured,
			@KlarnaEID,
			@KlarnaPClass,
			@MaksuturvaCode,
			@QliroClientRef,
			@QliroPaymentCode,
			@CollectorStoreId,
			@CollectorPaymentCode
		)

		SET @OrderPaymentId = CAST(SCOPE_IDENTITY() AS INT)
	END

	RETURN @OrderPaymentId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tCollectorPaymentType]'
GO
ALTER TABLE [orderlek].[tCollectorPaymentType] ADD CONSTRAINT [FK_tCollectorPaymentType_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
