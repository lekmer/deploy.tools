BEGIN TRANSACTION

DECLARE @CollectorAliasFolderId INT
SELECT @CollectorAliasFolderId = f.[AliasFolderId] FROM [template].[tAliasFolder] f WHERE f.[Title] = 'Collector'

DECLARE @ErrorCodesAliasFolderId INT
SELECT @ErrorCodesAliasFolderId = f.[AliasFolderId] FROM [template].[tAliasFolder] f WHERE f.[Title] = 'Error codes'

DECLARE @AliasId INT

INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@CollectorAliasFolderId, N'Order.Collector.UnspecifiedException', 1, N'A problem has occurred in communication with Collector. Please try again later or choose another way to pay.', N'Appears when something went wrong with Collector and payment can''t be done.', NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@CollectorAliasFolderId, N'Order.Collector.GetAddress.UnspecifiedException', 1, N'A problem has occurred in communication with Collector.', N'Appears when something went wrong with Collector and address can''t be retrieved.', NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@CollectorAliasFolderId, N'Order.Collector.DeliveryMethod.InvoiceDescription.Freight', 1, N'Freight cost', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@CollectorAliasFolderId, N'Order.Collector.DeliveryMethod.InvoiceDescription.OptionalFreight', 1, N'Optional freight', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@CollectorAliasFolderId, N'Order.Collector.DeliveryMethod.InvoiceDescription.DiapersFreight', 1, N'Diapers freight', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@ErrorCodesAliasFolderId, N'Order.Collector.FaultCode.DENIED_TO_PURCHASE', 1, N'DENIED_TO_PURCHASE', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, N'Tyvärr blev du inte godkänd att handla på faktura, var god välj ett annat betalningsalternativ.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, N'Dessverre ble du ikke godkjent for å handle på faktura, vær vennlig å velge et annet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, N'Desværre blev du ikke godkendt for at handle på faktura, var venlig og vælg et andet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, N'Valitettavasti emme hyväksy sinua laskuasiakkaaksi. Ole hyvä ja valitse toinen maksutapa.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@ErrorCodesAliasFolderId, N'Order.Collector.FaultCode.CREDIT_CHECK_DENIED', 1, N'CREDIT_CHECK_DENIED', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, N'Tyvärr blev du inte godkänd att handla på faktura, var god välj ett annat betalningsalternativ.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, N'Dessverre ble du ikke godkjent for å handle på faktura, vær vennlig å velge et annet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, N'Desværre blev du ikke godkendt for at handle på faktura, var venlig og vælg et andet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, N'Valitettavasti emme hyväksy sinua laskuasiakkaaksi. Ole hyvä ja valitse toinen maksutapa.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@ErrorCodesAliasFolderId, N'Order.Collector.FaultCode.ADDRESS_NOT_FOUND', 1, N'', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, N'Tyvärr blev du inte godkänd att handla på faktura, var god välj ett annat betalningsalternativ.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@ErrorCodesAliasFolderId, N'Order.Collector.FaultCode.RESERVATION_NOT_APPROVED', 1, N'RESERVATION_NOT_APPROVED', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, N'Tyvärr blev du inte godkänd att handla på faktura, var god välj ett annat betalningsalternativ.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, N'Dessverre ble du ikke godkjent for å handle på faktura, vær vennlig å velge et annet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, N'Desværre blev du ikke godkendt for at handle på faktura, var venlig og vælg et andet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, N'Valitettavasti emme hyväksy sinua laskuasiakkaaksi. Ole hyvä ja valitse toinen maksutapa.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@ErrorCodesAliasFolderId, N'Order.Collector.FaultCode.PURCHASE_AMOUNT_GREATER_THAN _MAX_CREDIT_AMOUNT', 1, N'PURCHASE_AMOUNT_GREATER_THAN _MAX_CREDIT_AMOUNT', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, N'Köpesumman är för stor för fakturaköp, var god minska värdet för varukorgen eller välj ett annat betalningsalternativ.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, N'Verdien på din handlevogn er for stor for fakturabetaling, vennligst ta bort artikler eller velg et annet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, N'Værdien på din indkøbskurv er for stor for at betale mod faktura. Venligst fjern artikler eller vælg et andet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, N'Ostosten yhteissumma on liian suuri voidaksesi maksaa laskulla. Poista tuotteita tai valitse toinen maksutapa.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@ErrorCodesAliasFolderId, N'Order.Collector.FaultCode.INVALID_REGISTRATION_NUMBER', 1, N'INVALID_REGISTRATION_NUMBER', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, N'Det personnummer du har slagit in är felaktigt, var god kontrollera detta och försök igen.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, N'Personnummeret som du har tastet inn er ikke korrekt, vennligst kontroller tallene og forsøk igjen')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, N'Det indtastede personnummer er forkert. Kontrollér venligst, og prøv igen.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, N'Henkilötunnuksesi on virheellinen. Tarkista henkilötunnus ja yritä uudelleen.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@ErrorCodesAliasFolderId, N'Order.Collector.FaultCode.AGREEMENT_RULES_VALIDATION_FAILED', 1, N'AGREEMENT_RULES_VALIDATION_FAILED', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, N'De personuppgifter du har angivit stämmer inte överens med dina folkbokföringsuppgifter, var god kontrollera detta och försök igen.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, N'Personopplysningene som du har lagt inn stemmer ikke overens med opplysningene i folkeregisteret, vennligst kontroller dette og forsøk igjen')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, N'De angivne kontaktoplysninger stemmer ikke overens med dine oplysninger i folkeregisteret. Kontrollér venligst, og prøv igen.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, N'Antamasi henkilötiedot eivät vastaa väestörekisterin henkilötietoja. Tarkista ja yritä uudelleen.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)


INSERT [template].[tAlias] ([AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate]) VALUES (@ErrorCodesAliasFolderId, N'Order.Collector.FaultCode.UNHANDLED_EXCEPTION', 1, N'UNHANDLED_EXCEPTION', NULL, NULL)
SET @AliasId = SCOPE_IDENTITY()

INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1, N'Tekniskt fel, var god försök senare eller välj ett annat betalningsalternativ.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 2, NULL)
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000001, N'Teknisk feil. Vennligst prøv igjen senere eller velg et annet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000002, N'Teknisk fejl, venligst forsøg senere eller vælg et andet betalingsalternativ')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000003, N'Tekninen vika. Ole hyvä ja yritä myöhemmin uudelleen tai valitse toinen maksutapa.')
INSERT [template].[tAliasTranslation] ([AliasId], [LanguageId], [Value]) VALUES (@AliasId, 1000005, NULL)

ROLLBACK TRANSACTION