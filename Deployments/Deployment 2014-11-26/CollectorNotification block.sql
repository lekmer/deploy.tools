--INSERT INTO [sitestructure].[tBlockType]
--		( [BlockTypeId],
--		  [Title],
--		  [CommonName],
--		  [Ordinal],
--		  [AvailableForAllPageTypes]
--		)
--VALUES
--		( 1000051, -- BlockTypeId - int
--		  N'Collector notification', -- Title - nvarchar(50)
--		  'CollectorNotification', -- CommonName - varchar(50)
--		  0, -- Ordinal - int
--		  0  -- AvailableForAllPageTypes - bit
--		)


INSERT INTO [sitestructure].[tContentPageTypeBlockType]
		( [ContentPageTypeId],
		  [BlockTypeId]
		)
VALUES
		( 1, -- ContentPageTypeId - int
		  1000051  -- BlockTypeId - int
		)
