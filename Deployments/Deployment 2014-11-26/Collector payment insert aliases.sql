﻿BEGIN TRAN

INSERT	INTO [template].[tAlias]
		( [AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate] )
VALUES
		( 1000018, -- AliasFolderId - int
		  'Order.Checkout.Payment.PaymentType_CollectorInvoice', -- CommonName - varchar(100)
		  3, -- AliasTypeId - int
		  N'CollectorInvoice', -- Value - nvarchar(max)
		  N'CollectorInvoice', -- Description - nvarchar(max)
		  GETDATE() -- LastUsedDate - datetime
		  ),
		( 1000018, -- AliasFolderId - int
		  'Order.Checkout.Payment.PaymentType_CollectorPartPayment', -- CommonName - varchar(100)
		  3, -- AliasTypeId - int
		  N'CollectorPartPayment', -- Value - nvarchar(max)
		  N'CollectorPartPayment', -- Description - nvarchar(max)
		  GETDATE() -- LastUsedDate - datetime
		  ),
		( 1000018, -- AliasFolderId - int
		  'Order.Checkout.Payment.PaymentType_CollectorSpecialPartPayment', -- CommonName - varchar(100)
		  3, -- AliasTypeId - int
		  N'CollectorSpecialPartPayment', -- Value - nvarchar(max)
		  N'CollectorSpecialPartPayment', -- Description - nvarchar(max)
		  GETDATE() -- LastUsedDate - datetime
		  )



INSERT	INTO [template].[tAlias]
		( [AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description], [LastUsedDate] )
VALUES
		( 1000018, -- AliasFolderId - int
		  'Order.Checkout.Payment.Collector.PartPaymentAccount', -- CommonName - varchar(100)
		  1, -- AliasTypeId - int
		  N'Collector konto x kr i månaden', -- Value - nvarchar(max)
		  NULL, -- Description - nvarchar(max)
		  GETDATE() -- LastUsedDate - datetime
		  ),
		( 1000018, -- AliasFolderId - int
		  'Order.Checkout.Payment.Collector.PartPayment', -- CommonName - varchar(100)
		  1, -- AliasTypeId - int
		  N'Betala x kr i y månader', -- Value - nvarchar(max)
		  NULL, -- Description - nvarchar(max)
		  GETDATE() -- LastUsedDate - datetime
		  )

ROLLBACK
--COMMIT
