BEGIN TRAN

--[order].[tPaymentType]

--SET IDENTITY_INSERT [order].[tPaymentType] ON
--INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId], [Ordinal]) VALUES (1000016, N'CollectorInvoice', N'Collector Invoice', N'44', 140)
--INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId], [Ordinal]) VALUES (1000017, N'CollectorPartPayment', N'Collector Part Payment', N'45', 150)
--INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId], [Ordinal]) VALUES (1000018, N'CollectorSpecialPartPayment', N'Collector Special Part Payment', N'46', 160)
--SET IDENTITY_INSERT [order].[tPaymentType] OFF

SELECT * FROM [order].[tPaymentType]


--[order].[tPaymentTypeDeliveryMethod]

INSERT INTO [order].[tPaymentTypeDeliveryMethod] ( [PaymentTypeId], [DeliveryMethodId] )
VALUES
	( 1000016, 1 ), -- PaymentTypeId, DeliveryMethodId
	( 1000016, 1000003 ),
	( 1000016, 1000004 ),
	( 1000016, 1000005 ),
	
	( 1000017, 1 ), -- PaymentTypeId, DeliveryMethodId
	( 1000017, 1000003 ),
	( 1000017, 1000004 ),
	( 1000017, 1000005 ),
	
	( 1000018, 1 ), -- PaymentTypeId, DeliveryMethodId
	( 1000018, 1000003 ),
	( 1000018, 1000004 ),
	( 1000018, 1000005 )


--[order].[tChannelPaymentType]

INSERT INTO [order].[tChannelPaymentType]
VALUES
	(2, 1000016),
	(2, 1000017),
	(2, 1000018),
	
	(3, 1000016),
	(3, 1000017),
	(3, 1000018),
	
	(4, 1000016),
	(4, 1000017),
	(4, 1000018)


--[lekmer].[tPaymentTypePrice]

INSERT INTO [lekmer].[tPaymentTypePrice] ( [CountryId], [CurrencyId], [PaymentTypeId], [PaymentCost] )
VALUES
	( 1000001, 1000001, 1000016, 0.00 ), -- CountryId -- CurrencyId -- PaymentTypeId -- PaymentCost
	( 1000001, 1000001, 1000017, 0.00 ),
	( 1000001, 1000001, 1000018, 0.00 ),
	
	( 1000002, 1000002, 1000016, 0.00 ), -- CountryId -- CurrencyId -- PaymentTypeId -- PaymentCost
	( 1000002, 1000002, 1000017, 0.00 ),
	( 1000002, 1000002, 1000018, 0.00 ),
	
	( 1000003, 1000003, 1000016, 0.00 ), -- CountryId -- CurrencyId -- PaymentTypeId -- PaymentCost
	( 1000003, 1000003, 1000017, 0.00 ),
	( 1000003, 1000003, 1000018, 0.00 ) 

ROLLBACK
--COMMIT