SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [integration].[tBackofficeProductInfoImport]'
GO
ALTER TABLE [integration].[tBackofficeProductInfoImport] DROP CONSTRAINT [PK_tBackofficeProductInfoImport]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [integration].[tBackofficeProductInfoImport]'
GO
CREATE TABLE [integration].[tmp_rg_xx_tBackofficeProductInfoImport]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[HYErpId] [nvarchar] (20) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ChannelId] [int] NULL,
[TagIdCollection] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[UserName] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[InsertedDate] [smalldatetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [integration].[tmp_rg_xx_tBackofficeProductInfoImport] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [integration].[tmp_rg_xx_tBackofficeProductInfoImport]([Id], [HYErpId], [Title], [Description], [TagIdCollection], [UserName], [InsertedDate]) SELECT [Id], [HYErpId], [Title], [Description], [TagIdCollection], [UserName], [InsertedDate] FROM [integration].[tBackofficeProductInfoImport]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [integration].[tmp_rg_xx_tBackofficeProductInfoImport] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO



UPDATE t
SET [t].[ChannelId] = ( SELECT h.ChannelId
					FROM [core].[tChannel] h
						 INNER JOIN [core].[tCountry] c ON h.CountryId = c.CountryId
					WHERE c.ISO = [tOld].[ChannelNameISO]
)
FROM [integration].[tmp_rg_xx_tBackofficeProductInfoImport] t
INNER JOIN [integration].[tBackofficeProductInfoImport] tOld ON [tOld].[Id] = [t].[Id]



GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[integration].[tBackofficeProductInfoImport]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[integration].[tmp_rg_xx_tBackofficeProductInfoImport]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [integration].[tBackofficeProductInfoImport]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[integration].[tmp_rg_xx_tBackofficeProductInfoImport]', N'tBackofficeProductInfoImport'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBackofficeProductInfoImport] on [integration].[tBackofficeProductInfoImport]'
GO
ALTER TABLE [integration].[tBackofficeProductInfoImport] ADD CONSTRAINT [PK_tBackofficeProductInfoImport] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pBackofficeProductInfoImportLogDelete]'
GO

CREATE PROCEDURE [integration].[pBackofficeProductInfoImportLogDelete]
	@Days INT
AS 
BEGIN
	SET NOCOUNT ON
	
	-- Current date.
	DECLARE	@CurrentDate AS DATETIME
	SET @CurrentDate = GETDATE()
	
	-- Deletion date.
	DECLARE	@DeletionDate AS DATETIME
	--SET @DeletionDate = DATEADD(day, -@Days, @CurrentDate)
	
	/* PT55880074 - Strange product titles
	   Lets keep logs for one year
	*/
	SET @DeletionDate = DATEADD(year, -1, @CurrentDate)
	
	-- Delete old rows.
	DELETE FROM
		[integration].[tBackofficeProductInfoImport]
	WHERE
		[InsertedDate] < @DeletionDate	 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pBackofficeProductInfoImportDeleteByDate]'
GO

ALTER PROCEDURE [integration].[pBackofficeProductInfoImportDeleteByDate]
	@Days INT
AS 
BEGIN
	/*NOT IN USE*/
	RAISERROR(N'NOT IN USE, not supported', 16, 1);
	RETURN
	
	SET NOCOUNT ON

	-- Current date.
	DECLARE	@CurrentDate AS DATETIME
	SET @CurrentDate = GETDATE()

	-- Deletion date.
	DECLARE	@DeletionDate AS DATETIME
	--SET @DeletionDate = DATEADD(day, -@Days, @CurrentDate)

	/* PT55880074 - Strange product titles
	   Lets keep logs for one year
	*/
	SET @DeletionDate = DATEADD(year, -1, @CurrentDate)

	-- Delete old rows.
	DELETE FROM
		integration.tBackofficeProductInfoImport
	WHERE
		[InsertedDate] < @DeletionDate	 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pBackofficeProductInfoImport]'
GO
CREATE PROCEDURE [integration].[pBackofficeProductInfoImport]
	@HYErpId		NVARCHAR(20),
	@Title			NVARCHAR(256),
	@Description	NVARCHAR(MAX),
	@TagIds			VARCHAR(MAX),
	@ChannelId		INT,
	@LanguageId		INT,
	@UserName		NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @TitleFixed NVARCHAR(256)
	
	-- Get product id by HYErpId
	DECLARE @ProductId INT
	SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] WHERE HYErpId = @HYErpId)

	-- insert into tProductTransaltion empty rows
	INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
	SELECT
		p.ProductId,
		@LanguageId
	FROM
		[product].[tProduct] p
	WHERE
		NOT EXISTS (SELECT 1 FROM [product].[tProductTranslation] n
					WHERE n.ProductId = p.ProductId 
						  AND n.LanguageId = @LanguageId)
					
	IF @Title = ''
		SET @Title = NULL
	SET @TitleFixed = [generic].[fStripIllegalSymbols](@Title)
				
	BEGIN TRY
		BEGIN TRANSACTION				

		IF @LanguageId != 1
			BEGIN
				UPDATE [product].[tProductTranslation]
				SET Title = @TitleFixed
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (Title != @TitleFixed OR Title IS NULL)

				-- Remove Restriction from [lekmer].[tProductRegistryRestrictionProduct] if it was added due to empty title.
				DECLARE @ProductRegistryId INT
				SET @ProductRegistryId = (SELECT ProductRegistryId FROM [product].[tProductModuleChannel] WHERE ChannelId = @ChannelId)

				-- Insert product to tProductRegistryProduct
				IF @TitleFixed IS NOT NULL
				BEGIN
					DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
					WHERE
						ProductRegistryId = @ProductRegistryId
						AND ProductId = @ProductId
						AND RestrictionReason = 'No Translation'
						AND UserId IS NULL

					DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]
					INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
					VALUES (@ProductId, @ProductRegistryId)

					EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
				END

				UPDATE [product].[tProductTranslation]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND LanguageId = @LanguageId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END
		ELSE
			BEGIN
				IF @TitleFixed IS NOT NULL
				BEGIN
					UPDATE [product].[tProduct]
					SET Title = @TitleFixed
					WHERE ProductId = @ProductId
				END
					
				UPDATE [product].[tProduct]
				SET [Description] = @Description
				WHERE
					ProductId = @ProductId
					AND (@Description IS NOT NULL AND @Description != '')
					AND ([Description] != @Description OR [Description] IS NULL)
			END

		-- Update/Insert product tags.
		DECLARE @TagId INT
		DECLARE cur_tag CURSOR FAST_FORWARD FOR
			SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')
			
		OPEN cur_tag
		FETCH NEXT FROM cur_tag INTO @TagId
			
		WHILE @@FETCH_STATUS = 0
			BEGIN
				-- Update/Insert product tags.
				IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
						  	   WHERE [pt].[ProductId] = @ProductId
									 AND [pt].[TagId] = @TagId)
					BEGIN
						INSERT INTO lekmer.tProductTag (ProductId, TagId)
						VALUES (@ProductId, @TagId)
					END
					
				EXEC [lekmer].[pProductTagUsageSave] @ProductId, @TagId, -1						
				
				FETCH NEXT FROM cur_tag INTO @TagId
			END
		CLOSE cur_tag
		DEALLOCATE cur_tag
		
		-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
		INSERT INTO [integration].[tBackofficeProductInfoImport] (
			[HYErpId]
			,[Title]
			,[Description]
			,[ChannelId]
			,[TagIdCollection]
			,[UserName]
			,[InsertedDate])
		VALUES (
			@HYErpId
			,@Title
			,@Description
			,@ChannelId
			,@TagIds
			,@UserName
			,CAST(GETDATE() AS SMALLDATETIME))

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog] (
			[Data],
			[Message],
			[Date],
			[OcuredInProcedure])
		VALUES (
			'',
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE())
	END CATCH		 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pBackofficeProductImportById]'
GO
ALTER PROCEDURE [integration].[pBackofficeProductImportById]
	@HYErpId		NVARCHAR(20),
	@Title			NVARCHAR(256),
	@Description	NVARCHAR(MAX),
	@ChannelNameISO	NVARCHAR(10),
	@TagIds			VARCHAR(MAX),
	@UserName		NVARCHAR(100)
AS
BEGIN
	/*NOT IN USE*/
	RAISERROR(N'NOT IN USE, not supported', 16, 1);
	RETURN

	--SET NOCOUNT ON

	---- Get language id by Channel
	--DECLARE @LanguageId INT
	--DECLARE @ChannelId INT
	--DECLARE @TitleFixed NVARCHAR(256)

	--SELECT 
	--	@LanguageId = l.LanguageId,
	--	@ChannelId = h.ChannelId
	--FROM [core].[tLanguage] l
	--	 INNER JOIN [core].[tChannel] h ON l.LanguageId = h.LanguageId
	--	 INNER JOIN [core].[tCountry] c ON h.CountryId = c.CountryId
	--WHERE
	--	c.ISO = @ChannelNameISO

	---- Get product id by HYErpId
	--DECLARE @ProductId INT
	--SET @ProductId = (SELECT ProductId FROM [lekmer].[tLekmerProduct] WHERE HYErpId = @HYErpId)

	---- insert into tProductTransaltion empty rows
	--INSERT INTO [product].[tProductTranslation] (ProductId, LanguageId)
	--SELECT
	--	p.ProductId,
	--	@LanguageId
	--FROM
	--	[product].[tProduct] p
	--WHERE
	--	NOT EXISTS (SELECT 1 FROM [product].[tProductTranslation] n
	--				WHERE n.ProductId = p.ProductId 
	--				AND n.LanguageId = @LanguageId)

	--IF @Title = ''
	--	SET @Title = NULL

	--SET @TitleFixed = [generic].[fStripIllegalSymbols](@Title)

	--BEGIN TRY
	--	BEGIN TRANSACTION				

	--	IF @LanguageId != 1
	--		BEGIN
	--			UPDATE [product].[tProductTranslation]
	--			SET Title = @TitleFixed
	--			WHERE
	--				ProductId = @ProductId
	--				AND LanguageId = @LanguageId
	--				AND (Title != @TitleFixed OR Title IS NULL)


	--			/*BEGIN*/
	--			-- Remove Restriction from [lekmer].[tProductRegistryRestrictionProduct] if it was added due to empty title.
	--			-- Get product id by HYErpId
	--			DECLARE @ProductRegistryId INT
	--			SET @ProductRegistryId = (SELECT ProductRegistryId FROM [product].[tProductModuleChannel] WHERE ChannelId = @ChannelId)

	--			-- Insert product to tProductRegistryproduct
	--			IF @TitleFixed IS NOT NULL
	--			BEGIN
	--				DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
	--				WHERE
	--					ProductRegistryId = @ProductRegistryId
	--					AND ProductId = @ProductId
	--					AND RestrictionReason = 'No Translation'
	--					AND UserId IS NULL

	--				DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]
	--				INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
	--				VALUES (@ProductId, @ProductRegistryId)

	--				EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
	--			END
	--			/*END*/


	--			UPDATE [product].[tProductTranslation]
	--			SET [Description] = @Description
	--			WHERE
	--				ProductId = @ProductId
	--				AND LanguageId = @LanguageId
	--				AND (@Description IS NOT NULL AND @Description != '')
	--				AND ([Description] != @Description OR [Description] IS NULL)
	--		END
	--	ELSE
	--		BEGIN
	--			IF @TitleFixed IS NOT NULL
	--			BEGIN
	--				UPDATE [product].[tProduct]
	--				SET Title = @TitleFixed
	--				WHERE
	--					ProductId = @ProductId
	--			END

	--			UPDATE [product].[tProduct]
	--			SET [Description] = @Description
	--			WHERE
	--				ProductId = @ProductId
	--				AND (@Description IS NOT NULL AND @Description != '')
	--				AND ([Description] != @Description OR [Description] IS NULL)
	--		END

	--	-- Update/Insert product tags.
	--	DECLARE @TagId INT
	--	DECLARE cur_tag CURSOR FAST_FORWARD FOR
	--		SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')

	--	OPEN cur_tag
	--	FETCH NEXT FROM cur_tag INTO @TagId

	--	WHILE @@FETCH_STATUS = 0
	--		BEGIN
	--			-- Update/Insert product tags.
	--			IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
	--					  	   WHERE [pt].[ProductId] = @ProductId
	--								 AND [pt].[TagId] = @TagId)
	--				BEGIN
	--					INSERT INTO lekmer.tProductTag (ProductId, TagId)
	--					VALUES (@ProductId, @TagId)
	--				END

	--			EXEC [lekmer].[pProductTagUsageSave] @ProductId, @TagId, -1						

	--			FETCH NEXT FROM cur_tag INTO @TagId
	--		END
	--	CLOSE cur_tag
	--	DEALLOCATE cur_tag

	--	-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
	--	INSERT INTO integration.tBackofficeProductInfoImport
	--	(
	--		[HYErpId]
	--		,[Title]
	--		,[Description]
	--		,[ChannelNameISO]
	--		,[TagIdCollection]
	--		,[UserName]
	--		,[InsertedDate]
	--	)
	--	VALUES 
	--	(
	--		@HYErpId
	--		,@Title
	--		,@Description
	--		,@ChannelNameISO
	--		,@TagIds
	--		,@UserName
	--		,CAST(GETDATE() AS SMALLDATETIME)
	--	)

	--	COMMIT TRANSACTION
	--END TRY
	--BEGIN CATCH
	--	-- If transaction is active, roll it back.
	--	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

	--	INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
	--				values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	--END CATCH
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
