/*
Run this script on:

(local).Lekmer_212_0    -  This database will be modified

to synchronize it with:

(local).Lekmer_212_1

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 2/18/2014 11:00:34 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [security].[tRolePrivilege]
ALTER TABLE [security].[tRolePrivilege] DROP CONSTRAINT [FK_tRolePrivilege_tPrivilege]
ALTER TABLE [security].[tRolePrivilege] DROP CONSTRAINT [FK_tRolePrivilege_tRole]

-- Update rows in [security].[tPrivilege]
UPDATE [security].[tPrivilege] SET [CommonName]=N'Tools.Cdon', [Title]=N'Tools.Cdon' WHERE [PrivilegeId]=1000004
UPDATE [security].[tPrivilege] SET [CommonName]=N'Tools.Cdon.ReadOnly', [Title]=N'Tools.Cdon.ReadOnly' WHERE [PrivilegeId]=1000030
-- Operation applied to 2 rows out of 2

-- Add rows to [security].[tPrivilege]
SET IDENTITY_INSERT [security].[tPrivilege] ON
INSERT INTO [security].[tPrivilege] ([PrivilegeId], [CommonName], [Title]) VALUES (1000033, N'Tools.Titles', N'Tools.Titles')
INSERT INTO [security].[tPrivilege] ([PrivilegeId], [CommonName], [Title]) VALUES (1000034, N'Tools.Titles.ReadOnly', N'Tools.Titles.ReadOnly')
SET IDENTITY_INSERT [security].[tPrivilege] OFF
-- Operation applied to 2 rows out of 2

-- Add rows to [security].[tRolePrivilege]
INSERT INTO [security].[tRolePrivilege] ([RoleId], [PrivilegeId]) VALUES (1, 1000033)
INSERT INTO [security].[tRolePrivilege] ([RoleId], [PrivilegeId]) VALUES (217, 1000033)
-- Operation applied to 2 rows out of 2

-- Add constraints to [security].[tRolePrivilege]
ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [FK_tRolePrivilege_tPrivilege] FOREIGN KEY ([PrivilegeId]) REFERENCES [security].[tPrivilege] ([PrivilegeId])
ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [FK_tRolePrivilege_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId])
COMMIT TRANSACTION
GO
