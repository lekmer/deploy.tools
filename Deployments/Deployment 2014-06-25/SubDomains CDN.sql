BEGIN TRAN

SET IDENTITY_INSERT [corelek].[tSubDomain] ON
INSERT INTO [corelek].[tSubDomain] ( [SubDomainId], [CommonName], [DomainUrl], [UrlTypeId], [ContentTypeId] )
VALUES
( 10, 'cdn.static.lekmer.com',   'a-lekmer.r.worldssl.net/media', 1, 1 ),
( 21, 'cdn.media-1.lekmer.com',  'a-lekmer.r.worldssl.net',       1, 2 ),
( 22, 'cdn.media-2.lekmer.com',  'b-lekmer.r.worldssl.net',       1, 2 ),
( 40, 'cdn.media-ex.lekmer.com', 'a-lekmer.r.worldssl.net',       1, 4 )
SET IDENTITY_INSERT [corelek].[tSubDomain] OFF

INSERT INTO [corelek].[tSubDomainChannel] ( [ChannelId], [SubDomainId] )
VALUES
( 1, 10 ),
( 2, 10 ),
( 3, 10 ),
( 4, 10 ),
( 1000005, 10 ),
( 1, 21 ),
( 2, 21 ),
( 3, 21 ),
( 4, 21 ),
( 1000005, 21 ),
( 1, 22 ),
( 2, 22 ),
( 3, 22 ),
( 4, 22 ),
( 1000005, 22 ),
( 1, 40 ),
( 2, 40 ),
( 3, 40 ),
( 4, 40 ),
( 1000005, 40 )

ROLLBACK