SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[tLekmerChannel]'
GO
ALTER TABLE [lekmer].[tLekmerChannel] ADD
[VatPercentage] [decimal] (16, 2) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vLekmerChannel]'
GO

ALTER VIEW [lekmer].[vLekmerChannel]
AS
SELECT
	[ChannelId]		AS [Channel.Id],
	[TimeFormat]	AS [Channel.TimeFormat],
	[WeekDayFormat]	AS [Channel.WeekDayFormat],
	[DayFormat]		AS [Channel.DayFormat],
	[DateTimeFormat]AS [Channel.DateTimeFormat],
	[TimeZoneDiff]	AS [Channel.TimeZoneDiff],
	[ErpId]			AS [Channel.ErpId],
	[VatPercentage]	AS [Channel.VatPercentage]
FROM
	[lekmer].[tLekmerChannel]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [core].[vCustomChannel]'
GO

ALTER VIEW [core].[vCustomChannel]
AS
	SELECT
		vC.*,
		vLC.[Channel.TimeFormat],
		vLC.[Channel.WeekDayFormat],
		vLC.[Channel.DayFormat],
		vLC.[Channel.DateTimeFormat],
		vLC.[Channel.TimeZoneDiff],
		vLC.[Channel.ErpId],
		vLC.[Channel.VatPercentage]
	FROM
		[core].[vChannel] AS vC
		LEFT OUTER JOIN [lekmer].[vLekmerChannel] vLC ON vLC.[Channel.Id] = vC.[Channel.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_UpdateProductPRICELekmer]'
GO

ALTER PROCEDURE [integration].[usp_UpdateProductPRICELekmer]
AS
BEGIN

	IF OBJECT_ID('tempdb..#tProductPrice') IS NOT NULL
		DROP TABLE #tProductPrice
		
	CREATE TABLE #tProductPrice
	(
		ProductId INT NOT NULL,
		PriceListId INT NOT NULL,
		PriceIncludingVat DECIMAL(16,2) NOT NULL,
		PriceExcludingVat DECIMAL(16,2) NOT NULL,
		VatPercentage DECIMAL(16,2) NOT NULL
		CONSTRAINT PK_#tProductPrice PRIMARY KEY(ProductId, PricelistId) WITH (IGNORE_DUP_KEY=ON)
	)
	
	BEGIN TRY

		PRINT 'Update prices prepare.';

		WITH #tempProductPrices AS
		(
			SELECT 
				lc.[ChannelId],
				LEFT([HYarticleId], 3) AS ChannelHYID, -- '001'
				SUBSTRING([HYarticleId], 5, 12) AS ProductHYID, -- '8065259-0000'
				CAST([Price]/100.0 AS DECIMAL(16,2)) AS PriceIncludingVat,
				CAST((([Price]/100.0) / (1.0+(ISNULL(lc.[VatPercentage], 25.0)/100.0))) AS DECIMAL(16,2)) AS PriceExcludingVat,
				lc.[VatPercentage]
			FROM
				[integration].[tempProductPrice] tpp
				LEFT OUTER JOIN [lekmer].[tLekmerChannel] lc ON lc.[ErpId] = LEFT([HYarticleId], 3) -- '001'
		),
		#stageProductPrices AS
		(
			SELECT
				[ChannelId],
				[ProductHYID],
				(SELECT MAX(Price) FROM (VALUES (pp.[PriceIncludingVat]), (0.02)) AS AllPrices(Price)) AS 'PriceIncludingVat',
				(SELECT MAX(Price) FROM (VALUES (pp.[PriceExcludingVat]), (0.01)) AS AllPrices(Price)) AS 'PriceExcludingVat',
				[VatPercentage]
			FROM
				[#tempProductPrices] pp
		)
		INSERT [#tProductPrice]
		(
			[ProductId],
			[PriceListId],
			[PriceIncludingVat],
			[PriceExcludingVat],
			[VatPercentage]
		)		
		SELECT
			DISTINCT
			pli.[ProductId],
			pli.[PriceListId],
			t.[PriceIncludingVat],
			t.[PriceExcludingVat],
			t.[VatPercentage]
		FROM 
			[#stageProductPrices] t
			INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[HYErpId] = t.[ProductHYID]
			INNER JOIN [product].[tPriceListItem] pli ON pli.[ProductId] = lp.[ProductId]
			INNER JOIN [product].[tPriceList] pl ON pl.[PriceListId] = pli.[PriceListId]
			INNER JOIN [product].[tProductModuleChannel] pmc ON pmc.[PriceListRegistryId] = pl.[PriceListRegistryId] AND pmc.[ChannelId] = t.[ChannelId]
		WHERE
			pli.[PriceIncludingVat] <> t.[PriceIncludingVat]
			OR
			pli.[PriceExcludingVat] <> t.[PriceExcludingVat]
		;

		PRINT 'Update prices.'

		BEGIN TRANSACTION
		
		UPDATE
			ppli
		SET
			ppli.[PriceIncludingVat] = t.[PriceIncludingVat],
			ppli.[PriceExcludingVat] = t.[PriceExcludingVat],
			ppli.[VatPercentage] = t.[VatPercentage]
		FROM
			product.[tPriceListItem] ppli
			INNER JOIN #tProductPrice t ON ppli.[ProductId] = t.[ProductId] AND ppli.[PriceListId] = t.[PriceListId]

		COMMIT
		
		PRINT 'Update prices done.'		
		
		IF OBJECT_ID('tempdb..#tProductPrice') IS NOT NULL
			DROP TABLE #tProductPrice
	
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductLekmer]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateProductLekmer]
AS
BEGIN
	SET NOCOUNT ON 

	-- Delte all ***Missing*** (tmp)
	DELETE FROM integration.tempProduct WHERE ArticleCodeTitle= '***Missing***'

	-- tCategory
	EXEC [integration].[usp_UpdateTCategoryLekmer] -- creates the items if necessary
	EXEC [integration].[usp_UpdateCategoryTitleLekmer] -- update titles if necessary
	
	-- tBrand
	EXEC [integration].[usp_UpdateBrandLekmer]
	EXEC [integration].[usp_UpdateBrandLekmerCreationDate] -- adds the date the brand was created
	
	-- tProduct
	EXEC [integration].[usp_UpdateProductLekmer]
	EXEC [integration].[usp_ImportUpdateLekmerProduct]
	
	-- tPriceListItem
	EXEC [integration].[usp_UpdateProductPRICELekmer]
	
	-- tProductSize
	EXEC [integration].[usp_UpdateSizeLekmer]

	--------------------------------------

	DECLARE 
		@HYArticleNumberFull NVARCHAR(50),
		@HYArticleNumberColorSize NVARCHAR(50),
		@HYArticleNumberColor NVARCHAR(50),
		@HYSizeId NVARCHAR(50),
		@EanCode NVARCHAR(50),
		@NoInStock NVARCHAR(250),
		@ArticleTitle NVARCHAR(250),
		@ArticleTitleFixed NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@BrandId INT,
		@HYSizeValue NVARCHAR(50),
		@Weight NVARCHAR(50),
		@PossibleToDisplay NVARCHAR(100),
		@StockStatusErpId NVARCHAR(50),
		@StockStatusId INT,
		@SupplierId NVARCHAR(500),
		@PurchasePrice VARCHAR(100),
		@PurchaseCurrency VARCHAR(10),
		@SupplierArticleNumber NVARCHAR(50),
		@AveragePrice VARCHAR(100),
		
		@CategoryId INT,
		@Price DECIMAL(16,2),
		@PriceExclVat DECIMAL(16,2),
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@LekmerErpId NVARCHAR(25),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL(16,2),
		
		@NewFromDate DATETIME,
		@NewToDate DATETIME,

		-- Variables for Channel
		@HYChannel NVARCHAR(40),
		@ChannelId INT,
		@ProductRegistryId INT,
		@PriceListId INT,

		@TradeDoublerProductGroup NVARCHAR(50),

		@ProductIsNew BIT,
		@ProductPriceNotExist BIT,
		@ProductRegistryNotExist BIT,
		
		@PurchaseCurrencyId INT
	
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 4, @NewFromDate)

	------------------------------------

	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			tp.HYarticleId,
			tp.ArticleTitle,
			tp.SizeId,
			(CAST(tp.Price AS DECIMAL(16,2))/100.0),
			tp.NoInStock,
			tp.ArticleGroupId,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.BrandId,
			tp.EanCode,
			tp.LekmerArtNo,
			[tp].[Weight],
			[tp].[PossibleToDisplay],
			[tp].[Ref1], -- StockStatusErpId
			[tp].[SupplierId],
			[tp].[PurchasePrice],
			[tp].[PurchaseCurrency],
			[tp].[SupplierArticleNumber],
			[tp].[AveragePrice]
		FROM
			[integration].tempProduct tp
		--WHERE NOT EXISTS (SELECT 1
		--				  FROM lekmer.tLekmerProduct lp, product.tPriceListItem prp
		--				  WHERE lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
		--					AND lp.ProductId = prp.ProductId
		--					AND SUBSTRING(tp.HYarticleId, 3,1) = prp.PriceListId)

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@HYArticleNumberFull,
			@ArticleTitle,
			@HYSizeValue,
			@Price,
			@NoInStock,
			@VarugruppId,
			@VaruklassId,
			@VarukodId,
			@HYBrandId,
			@EanCode,
			@LekmerErpId,
			@Weight,
			@PossibleToDisplay,
			@StockStatusErpId,
			@SupplierId,
			@PurchasePrice,
			@PurchaseCurrency,
			@SupplierArticleNumber,
			@AveragePrice

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			-- Split @HYArticleNumberFull
			-- 001-0000001-1017-108
			SET @HYChannel = SUBSTRING(@HYArticleNumberFull, 1, 3)                  -- [001]-0000001-1017-108
			SET @HYArticleNumberColorSize = SUBSTRING(@HYArticleNumberFull, 5, 17)  -- 001-[0000001-1017-108]
			SET @HYArticleNumberColor = SUBSTRING(@HYArticleNumberFull, 5, 12)      -- 001-[0000001-1017]-108
			SET @HYSizeId = SUBSTRING(@HYArticleNumberFull, 18, 3)                  -- 001-0000001-1017-[108]

			-- find channel id
			SET @ChannelId = (SELECT c.[ChannelId] FROM [lekmer].[tLekmerChannel] c WHERE c.[ErpId] = @HYChannel)
			IF @ChannelId IS NULL
			BEGIN
				-- undefined channel, skip the row
				GOTO NEXT_ROW
			END
			
			-- find product registry id
			SET @ProductRegistryId = (SELECT [pmc].[ProductRegistryId] FROM [product].[tProductModuleChannel] pmc WHERE [pmc].[ChannelId] = @ChannelId)
			
			-- find price list id
			SET @PriceListId = (
				SELECT TOP 1
					pl.[PriceListId]
				FROM
					[product].[tProductModuleChannel] pmc
					INNER JOIN [product].[tPriceList] pl ON pl.[PriceListRegistryId] = pmc.[PriceListRegistryId]
				WHERE
					pmc.[ChannelId] = @ChannelId
					AND pl.[PriceListStatusId] = 0 -- online
			)

			-- find stock status id
			SET @StockStatusId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [ErpId] = @StockStatusErpId)
			SET @StockStatusId = ISNULL(@StockStatusId, 0) -- Active by default

			-- find product id
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYArticleNumberColor)
			
			---- Skip processing the same @HYErpId + @Channel few times
			--IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			--BEGIN
			--	GOTO PRODUCT_END
			--END ELSE BEGIN
			--	SET	@PrevHYErpId = @HYErpId
			--	SET	@PrevChannel = @Channel
			--END
			
			-- By default product, price and registry already exist
			SET @ProductIsNew = 0
			SET @ProductPriceNotExist = 0
			SET @ProductRegistryNotExist = 0
			
			IF @ProductId IS NULL
			BEGIN
				-- New product, need to prepare some variables
				SET @ProductIsNew = 1
				SET @ProductPriceNotExist = 1
				SET @ProductRegistryNotExist = 1
				
				SET @CategoryId = (SELECT CategoryId 
							   FROM product.tCategory 
							   WHERE ErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, ''))
			
				SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)
				SET @PurchaseCurrencyId = (SELECT TOP(1) [CurrencyId] FROM [core].[tCurrency] WHERE [ISO] = @PurchaseCurrency)
			END
			ELSE
			BEGIN
				-- Product already exist
				
				-- Lets check if product has price
				IF NOT EXISTS ( SELECT 1 FROM product.tPriceListItem prp
								WHERE
									prp.ProductId = @ProductId
									AND
									prp.PriceListId = @PriceListId )
				BEGIN
					SET @ProductPriceNotExist = 1
				END
				
				-- Lets check if product exists in registry
				IF NOT EXISTS ( SELECT 1 FROM product.tProductRegistryProduct
								WHERE
									ProductId = @ProductId
									AND
									ProductRegistryId = @ProductRegistryId )
				BEGIN
					SET @ProductRegistryNotExist = 1
				END
			END
			
			
			-- Now lets do changes
			
			BEGIN TRANSACTION
			
			-- New product
			IF @ProductId IS NULL
			BEGIN
				SET @Data = 'NEW: HYID ' + @HYArticleNumberFull
				
				SET @ArticleTitleFixed = [generic].[fStripIllegalSymbols](@ArticleTitle)
				
				-- tProduct
				INSERT INTO [product].tProduct (
					ErpId,
					EanCode,
					IsDeleted,
					NumberInStock,
					CategoryId,
					Title,
					[Description],
					ProductStatusId
				)
				VALUES (
					@HYArticleNumberColor,
					@EanCode,
					0, -- IsDeleted
					@NoInStock,
					@CategoryId,
					@ArticleTitleFixed,
					'', -- Description
					1 -- Offline
				)
											
				SET @ProductId = SCOPE_IDENTITY()
				
				SET @Data = 'NEW: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
	
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId,
					HYErpId,
					BrandId,
					IsBookable,
					AgeFromMonth,
					AgeToMonth,
					IsNewFrom,
					IsNewTo,
					IsBatteryIncluded,
					ExpectedBackInStock,
					LekmerErpId,
					ShowVariantRelations,
					[Weight],
					[ProductTypeId],
					[IsAbove60L],
					[StockStatusId],
					[SupplierId],
					[PurchasePrice],
					[PurchaseCurrencyId],
					[SupplierArticleNumber],
					[AveragePrice]
				)
				VALUES (
					@ProductId, 
					@HYArticleNumberColor,
					@BrandId,
					0, -- IsBookable, 
					0, -- AgeFromMonth
					0, -- AgeToMonth
					@NewFromDate,
					@NewToDate,
					0, -- IsBatteryIncluded
					NULL, --ExpectedBackInStock
					@LekmerErpId,
					1, -- ShowVariantRelations
					CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), @Weight) / 1000),
					1,
					CASE WHEN @PossibleToDisplay IS NULL OR @PossibleToDisplay <> 'G' THEN 0 ELSE 1 END,
					@StockStatusId,
					@SupplierId,
					CONVERT(DECIMAl(16,2), CONVERT(DECIMAl(16,2), @PurchasePrice) / 100.0),
					@PurchaseCurrencyId,
					@SupplierArticleNumber,
					CONVERT(DECIMAl(16,2), CONVERT(DECIMAl(16,2), @AveragePrice) / 100.0)
				)
			END
			
			------------------------------------------------------------------------------------------
			-- tProductRegistryProduct
			-- Insert when product is new and registry not exist
			--	or when price is not exists and registry not exist
			
			-- If price already exists, that could mean product also exists in registry or not if it is restricted
			-- So we need to add product to registry only when price is not yet set
			
			IF @ProductPriceNotExist = 1 AND @ProductRegistryNotExist = 1
			BEGIN
				SET @Data = 'AddRegistry: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
			
				INSERT INTO product.tProductRegistryProduct (
					ProductId,
					ProductRegistryId
				)
				VALUES (
					@ProductId,
					@ProductRegistryId
				)
			END
				
			------------------------------------------------------------------------------------------
			-- tPriceListItem
			-- Insert when not exist
				
			IF @ProductPriceNotExist = 1
			BEGIN
				SET @Data = 'AddPrice: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
				
				-- get channel VAT
				SELECT @Vat = ISNULL(c.[VatPercentage], 25.0) FROM [lekmer].[tLekmerChannel] c WHERE c.[ChannelId] = @ChannelId
				
				SET @PriceExclVat = @Price / (1.0+@Vat/100.0);
				IF @Price < 0.02 SET @Price = 0.02;
				IF @PriceExclVat < 0.01 SET @PriceExclVat = 0.01;
			
				INSERT INTO product.tPriceListItem (
					PriceListId,
					ProductId,
					PriceIncludingVat,
					PriceExcludingVat,
					VatPercentage
				)
				VALUES (
					@PriceListId,
					@ProductId, 
					@Price, 
					@PriceExclVat,
					@Vat
				)
			END

			------------------------------------------------------------------------------------------ 
			-- tTradeDoublerProductGroupMapping
			-- Insert when product is new
				
			IF @ProductIsNew = 1
			BEGIN
				SET @Data = 'AddTradeDoubler: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
			
				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '')
													   AND ChannelId = @ChannelId)

				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId
										 AND tTDPGM.ChannelId = @ChannelId)
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							@ChannelId
						)
					END
				END 
			END

			COMMIT

			------------------------------------------------------------------------------------------ 
			-- ProductSize
			-- Run update for each article
			
			SET @Data = 'RunSizeUpdate: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
				
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [integration].[usp_ImportUpdateProductSizesLekmer]
				@HYChannel,
				@ProductId,
				@HYArticleNumberColorSize,
				@HYSizeId,
				@HYSizeValue,
				@NoInStock,
				@Weight,
				@StockStatusId
			
		END TRY
		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
				
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)

		END CATCH

		NEXT_ROW:

		FETCH NEXT FROM cur_product 
			INTO 
				@HYArticleNumberFull,
				@ArticleTitle,
				@HYSizeValue,
				@Price,
				@NoInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@HYBrandId,
				@EanCode,
				@LekmerErpId,
				@Weight,
				@PossibleToDisplay,
				@StockStatusErpId,
				@SupplierId,
				@PurchasePrice,
				@PurchaseCurrency,
				@SupplierArticleNumber,
				@AveragePrice

	END
	
	CLOSE cur_product
	DEALLOCATE cur_product
	
	EXEC [integration].[usp_UpdateProductBrandLekmer]
	EXEC [integration].[usp_ChannelRestrictionsProducts]
	EXEC [integration].[usp_REATagProduct]
	
	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]
	
	-- Update stock status /active-passive/online-offline /*before package update !!!*/
	EXEC [integration].[usp_UpdateProductStatusLekmer]
	
	-- Add 'Outlet' tag
	-- Disabled 2013-10-31
	--EXEC [integration].[usp_OutletTagProduct]
	
	-- Update package stock
	EXEC [productlek].[pPackageUpdate]
	
	-- Track products chnages to eSales export
	EXEC [integration].[pTrackProductChangesLekmer]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
