-- [PriceListId] 4 Finland

SELECT
	[PriceListId],
	[ProductId],
	[PriceIncludingVat],
	[PriceExcludingVat],
	CAST(
		[PriceIncludingVat] / ( 1.0+(24.0/100.0) )
		AS DECIMAL(16,2)
	) AS PriceExcludingVat,
	[VatPercentage],
	[tmpIdentity]
FROM
	[product].[tPriceListItem] pl
WHERE
	pl.[PriceListId] = 4 -- Finland

BEGIN TRAN

UPDATE pl
SET
	pl.[PriceExcludingVat] = CAST(
		[PriceIncludingVat] / ( 1.0+(24.0/100.0) )
		AS DECIMAL(16,2)
	),
	pl.[VatPercentage] = 24
FROM
	[product].[tPriceListItem] pl
WHERE
	pl.[PriceListId] = 4 -- Finland
	
ROLLBACK