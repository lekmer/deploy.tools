SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [lekmer].[tLekmerBlock]'
GO
CREATE TABLE [lekmer].[tLekmerBlock]
(
[BlockId] [int] NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[StartDailyIntervalMinutes] [int] NULL,
[EndDailyIntervalMinutes] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tLekmerBlock] on [lekmer].[tLekmerBlock]'
GO
ALTER TABLE [lekmer].[tLekmerBlock] ADD CONSTRAINT [PK_tLekmerBlock] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[vCustomBlock]'
GO


ALTER view [sitestructure].[vCustomBlock]
as
 select
  b.*,
  l.StartDate AS 'Block.StartDate',
  l.EndDate AS 'Block.EndDate',
  l.StartDailyIntervalMinutes AS 'Block.StartDailyIntervalMinutes',
  l.EndDailyIntervalMinutes AS 'Block.EndDailyIntervalMinutes'
 from
  [sitestructure].[vBlock] b left join [lekmer].[tLekmerBlock] l 
   on b.[Block.BlockId] = l.[BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[vCustomBlockSecure]'
GO


ALTER view [sitestructure].[vCustomBlockSecure]
as
 select
  b.*,
  l.StartDate AS 'Block.StartDate',
  l.EndDate AS 'Block.EndDate',
  l.StartDailyIntervalMinutes AS 'Block.StartDailyIntervalMinutes',
  l.EndDailyIntervalMinutes AS 'Block.EndDailyIntervalMinutes'
 from
  [sitestructure].[vBlockSecure] b left join [lekmer].[tLekmerBlock] l 
   on b.[Block.BlockId] = l.[BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pBlockTopListProductGetAllByBlock]'
GO

ALTER PROCEDURE [addon].[pBlockTopListProductGetAllByBlock]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		p.*,
		pli.*
	FROM
		addon.vCustomBlockTopListProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				NULL
			)
		CROSS APPLY (
			SELECT SUM(ps.[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[Product.Id]
		) a1
	WHERE
		p.[BlockTopListProduct.BlockId] = @BlockId
		AND p.[Product.ChannelId] = @ChannelId
		AND ISNULL(a1.[NumberInStock], p.[Product.NumberInStock]) > 0
	ORDER BY
		[BlockTopListProduct.Position]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pBlockTopListProductGetAllByBlockSecure]'
GO

ALTER PROCEDURE [addon].[pBlockTopListProductGetAllByBlockSecure]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @CurrencyId INT, @ProductRegistryId INT

	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	SELECT @ProductRegistryId = ProductRegistryId FROM product.tProductModuleChannel WHERE ChannelId = @ChannelId

	SELECT
		*
	FROM
		addon.vCustomBlockTopListProductSecure AS CBTLPS
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = CBTLPS.[Product.Id] AND PRP.ProductRegistryId = @ProductRegistryId
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = CBTLPS.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, CBTLPS.[Product.Id], PMC.PriceListRegistryId, NULL)
	WHERE
		[BlockTopListProduct.BlockId] = @BlockId
	ORDER BY
		[BlockTopListProduct.Position]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pLekmerBlockSave]'
GO
CREATE PROCEDURE [lekmer].[pLekmerBlockSave]
 @BlockId     int,
 @StartDate     datetime = NULL,
 @EndDate     datetime = NULL,
 @StartDailyIntervalMinutes int = NULL,
 @EndDailyIntervalMinutes int = NULL
as
begin
 update
  [lekmer].[tLekmerBlock]
 set
  [StartDate]      = @StartDate,
  [EndDate]      = @EndDate,
  [StartDailyIntervalMinutes]  = @StartDailyIntervalMinutes,
  [EndDailyIntervalMinutes]  = @EndDailyIntervalMinutes
 where
  [BlockId] = @BlockId
  
 if @@ROWCOUNT = 0
  begin
   insert
    [lekmer].[tLekmerBlock]
   (
    [BlockId],
    [StartDate],
    [EndDate],
    [StartDailyIntervalMinutes],
    [EndDailyIntervalMinutes]
   )
   values
   (
    @BlockId,
    @StartDate,
    @EndDate,
    @StartDailyIntervalMinutes,
    @EndDailyIntervalMinutes
   )
  end
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pTopListProductGetIdAllByStatisticsAndCategoryIdListExcludeForced]'
GO

ALTER PROCEDURE [addon].[pTopListProductGetIdAllByStatisticsAndCategoryIdListExcludeForced]
	@ChannelId				INT,
	@BlockId				INT,
	@IncludeAllCategories	BIT,
	@CategoryIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@CountOrdersFrom		DATETIME,
	@From					INT,
	@To						INT
AS
BEGIN

	DECLARE @sql AS NVARCHAR(MAX)
	DECLARE @sqlCount AS NVARCHAR(MAX)
	DECLARE @sqlFragment AS NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT
			ROW_NUMBER() OVER (ORDER BY SUM(oi.[OrderItem.Quantity]) DESC) AS Number,
			p.[Product.Id] AS ProductId
		FROM
			product.vCustomProduct p
			INNER JOIN [order].vCustomOrderItem oi WITH(NOLOCK) ON oi.[OrderItem.ProductId] = p.[Product.Id]
			INNER JOIN [order].vCustomOrder o WITH(NOLOCK)
				ON oi.[OrderItem.OrderId] = o.[Order.OrderId]
				AND o.[Order.ChannelId] = @ChannelId
				AND o.[Order.CreatedDate] >= @CountOrdersFrom
			CROSS APPLY (
				SELECT SUM(ps.[NumberInStock]) AS ''NumberInStock'' FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[Product.Id]
			) a1
		WHERE
			p.[Product.ChannelId] = @ChannelId
			AND ISNULL(a1.[NumberInStock], p.[Product.NumberInStock]) > 0'
			
	IF (@IncludeAllCategories = 0 AND @CategoryIds IS NOT NULL)
	SET @sqlFragment = @sqlFragment + '
			AND ( p.[Product.CategoryId] IN ( SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter) ) )'

	SET @sqlFragment = @sqlFragment + '		
			AND NOT EXISTS ( SELECT 1 FROM [addon].[tBlockTopListProduct] fp WHERE fp.[BlockId] = @BlockId AND fp.[ProductId] = p.[Product.Id] )
		GROUP BY
			p.[Product.Id]
	'

	SET @sqlCount = '
		SELECT COUNT(*)
		FROM
			(' + @sqlFragment + ') AS CountResults'

	SET @sql = '
		SELECT ProductId
		FROM
			(' + @sqlFragment + ') AS SearchResult
		WHERE
			Number BETWEEN @From AND @To'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom
		     
	EXEC sp_executesql @sql, 
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME,
			@From					INT,
			@To						INT',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom,
			@From,
			@To
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductGetIdAllByBlock]'
GO
ALTER PROCEDURE [product].[pProductGetIdAllByBlock]
	@ChannelId INT,
	@CustomerId INT,
	@BlockId INT,
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	
	SET @sqlFilter = '
		(
			SELECT ROW_NUMBER() OVER (ORDER BY a1.[ProductInStock] DESC, bp.[BlockProductListProduct.Ordinal]) AS Number,
			[p].[Product.Id]
			FROM [product].[vCustomProduct] p
				INNER JOIN [product].[vCustomBlockProductListProduct] AS bp ON bp.[BlockProductListProduct.ProductId] = p.[Product.Id]
				INNER JOIN product.vCustomPriceListItem AS pli
					ON pli.[Price.ProductId] = P.[Product.Id]
					AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)
				CROSS APPLY (
					SELECT
						CASE WHEN ISNULL(SUM(ps.[NumberInStock]), p.[Product.NumberInStock]) > 0 THEN 1 ELSE 0 END AS ''ProductInStock''
					FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[Product.Id]
				) a1
			WHERE
				bp.[BlockProductListProduct.BlockId] = @BlockId
				AND p.[Product.ChannelId] = @ChannelId
		)'

	SET @sql = '
		SELECT * FROM
		' + @sqlFilter + '
		AS SearchResult'
	SET @sqlCount = '
		SELECT COUNT(1) FROM
		' + @sqlFilter + '
		AS SearchResultsCount'
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE Number > (@Page - 1) * @PageSize
			AND Number <= @Page * @PageSize'
	END	

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	INT,
			@CustomerId	INT,
			@BlockId	INT,
			@Page		INT,
			@PageSize	INT',
			@ChannelId,
			@CustomerId,
			@BlockId,
			@Page,
			@PageSize
			
	EXEC sp_executesql @sql,
		N'	@ChannelId	INT,
			@CustomerId	INT,
			@BlockId	INT,
			@Page		INT,
			@PageSize	INT',
			@ChannelId,
			@CustomerId,
			@BlockId,
			@Page,
			@PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerBlock]'
GO
ALTER TABLE [lekmer].[tLekmerBlock] ADD CONSTRAINT [FK_tLekmerBlock_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
