SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [import].[pImportProduct]'
GO
ALTER PROCEDURE [import].[pImportProduct]

AS
BEGIN
	SET NOCOUNT ON	
	
	-- Delte all ***Missing*** (tmp)
	DELETE FROM import.tProduct WHERE ArticleCodeTitle= '***Missing***'
	
	EXEC [import].[pImportCategory]
	EXEC [import].[pImportBrand]
	EXEC [integration].[usp_UpdateBrandHeppoCreationDate] 
	EXEC [import].[pUpdateProduct]
	EXEC [import].[pUpdateProductNumberInStock]
	
	DECLARE 
		@NeedToInsertProductRelationData BIT,
	
		@HYErpId NVARCHAR(50),
		@HYErpIdSize NVARCHAR(50),
		@Channel INT,
		@NumberInStock INT,
		@ArticleTitle NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@SizeId NVARCHAR(25), -- for taging socks
		@Price DECIMAL,
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@VarugruppTitel NVARCHAR(50),
		@VarukodTitel NVARCHAR(50),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL = 25.0,
		
		@PrevHYErpId NVARCHAR(50),
		@PrevChannel INT,
		
		@HYArticleClassId NVARCHAR(50),
		@CategoryErpId NVARCHAR(50),
		@CategoryId INT,
		@BrandId INT,
		@NewFromDate DATETIME,
		@NewToDate DATETIME,
		@TagGroupId_CategoryLevel3 INT = 1000008,
		@TagId_1 INT,
		@TagId_2 INT,
		@TradeDoublerProductGroup NVARCHAR(50),
		@ChannelId INT,
		@IsProductRegistry BIT,
		@PriceListId INT,
		@ProductRegistryId INT
		
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 1, @NewFromDate)
	
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT 
			tp.HYErpId,
			tp.HYErpIdSize,
			tp.ChannelId,
			tp.ArticleTitle,
			tp.SizeId,
			tp.Price/100,
			tp.NumberInStock,
			tp.ArticleGroupId,
			tp.ArticleGroupTitle,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.ArticleCodeTitle,
			tp.BrandId
		FROM
			[import].tProduct tp


	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO 
			@HYErpId,
			@HYErpIdSize,
			@Channel,			 
			@ArticleTitle,
			@SizeId,
			@Price,
			@NumberInStock,
			@VarugruppId,
			@VarugruppTitel,
			@VaruklassId,
			@VarukodId,
			@VarukodTitel,
			@HYBrandId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			SET @IsProductRegistry = 0
			
			SET @ChannelId =  (select ChannelId from lekmer.tLekmerChannel where ErpId = @Channel)			
										 
			SET @ProductRegistryId = (Select ProductRegistryId from product.tProductModuleChannel where ChannelId = @ChannelId)
			
			SET @PriceListId = (Select PriceListId 
										from product.tProductModuleChannel m 
										inner join product.tPriceList t
										on m.PriceListRegistryId = t.PriceListRegistryId
										where ChannelId = @ChannelId)


		
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYErpId)
			IF @ProductId IS NOT NULL
			BEGIN
				IF EXISTS (SELECT 1 FROM product.tProductRegistryProduct prp
						   WHERE prp.ProductId = @ProductId
						   AND prp.ProductRegistryId = @ProductRegistryId)
				BEGIN
					SET @IsProductRegistry = 1
					GOTO PRODUCT_END
				END
			END
			
			PRODUCT_START:
			
			-- Skip processing the same @HYErpId + @Channel few times
			IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			BEGIN
				GOTO PRODUCT_END
			END ELSE BEGIN
				SET	@PrevHYErpId = @HYErpId
				SET	@PrevChannel = @Channel
			END
			
			BEGIN TRANSACTION
			
			SET @NeedToInsertProductRelationData = 0
			
			SET @HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '')
			SET @CategoryErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, '')

			SET @CategoryId = (SELECT CategoryId FROM product.tCategory WHERE ErpId = @CategoryErpId)
			
			SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)
							
			IF @ProductId IS NULL 
			BEGIN
				SET @NeedToInsertProductRelationData = 1
			
				-- Data that was sent into insert
				SET @Data = 'NEW: HYErpId ' + @HYErpId + ' @Fok ' + CAST(@Channel AS VARCHAR(10)) + 'HY-Cat ' + @CategoryErpId 
				
				-- tProduct
				INSERT INTO product.tProduct (
					ErpId,  
					IsDeleted, 
					NumberInStock, 
					CategoryId, 
					Title, 
					[Description], 
					ProductStatusId
				)
				VALUES (
					@HYErpId,
					0, --IsDeleted
					@NumberInStock, 
					@CategoryId,
					@ArticleTitle, 
					'', --Description, 
					1 --ProductStatusId = 'Ready to translate'
				)
											
				SET @ProductId = SCOPE_IDENTITY()
											
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId, 
					HYErpId, 
					BrandId, 
					IsBookable, 
					AgeFromMonth, 
					AgeToMonth, 
					IsNewFrom, 
					IsNewTo,
					IsBatteryIncluded, 
					ExpectedBackInStock,
					ShowVariantRelations
				)
				VALUES (
					@ProductId, 
					@HYErpId,
					@BrandId,	
					0, --IsBookable, 
					0, --AgeFromMonth, 
					1, --AgeToMonth, 
					@NewFromDate,
					@NewToDate, 
					0, --IsBatteryIncluded, 
					NULL, --ExpectedBackInStock
					1
				)
				
				------------------------------------------------------------------------------------------
				-- tProductNews
				-- Flag so that the product is and has been the news
				
				IF (@ProductId NOT IN (SELECT productId FROM integration.[tProductNews]))
				BEGIN
					INSERT INTO integration.tProductNews (
						ProductId,
						ErpId
					)
					VALUES (
						@ProductId,
						@HYErpId
					)
				END
					
				------------------------------------------------------------------------------------------
				-- tProductTag
				-- tag with categoryLevel3 (sneaker boots, etc.)

				SET @TagId_1 = NULL

				SELECT @TagId_1 = TagId
				FROM lekmer.tTag
				WHERE Value = @VarukodTitel
					  AND TagGroupId = @TagGroupId_CategoryLevel3

				IF @TagId_1 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1
								   FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND Tagid = @TagId_1)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_1
					END
				END

				------------------------------------------------------------------------------------------
				-- tProductTag
				-- Tag man/kvinna/pojke/flocka/unisex

				SET @TagId_1 = NULL
				SET @TagId_2 = NULL

				IF @VarugruppTitel IN ('Kvinna', 'Man', 'Flicka', 'Pojke')
				BEGIN
					SELECT @TagId_1 = TagId
					FROM lekmer.tTag
					WHERE Value = @VarugruppTitel
						  AND TagGroupId = 1000002
				END
				ELSE IF @VarugruppTitel = ('Unisex barn')
				BEGIN
					SET @TagId_1 = 1000013 -- Pojkskor
					SET @TagId_2 = 1000012 -- Flickskor
				END
				ELSE IF @VarugruppTitel = ('Unisex kvinna/man')
				BEGIN
					SET @TagId_1 = 1000009 -- Man
					SET @TagId_2 = 1000010 -- Kvinna
				END

				IF @TagId_1 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND TagId = @TagId_1)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_1
					END
				END

				IF @TagId_2 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND TagId = @TagId_2)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_2
					END
				END
				------------------------------------------------------------------------------------------
			END -- Product already exist in tLekmerProduct
			ELSE IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem prp WHERE prp.ProductId = @ProductId AND prp.PriceListId = @PriceListId)
			BEGIN
				SET @NeedToInsertProductRelationData = 1

				SET @Data = 'EXISTING: @HYErpId ' + @HYErpId + ' @Fok ' + CAST(@Channel AS VARCHAR(10)) + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
			END

			IF @NeedToInsertProductRelationData = 1
			BEGIN
				------------------------------------------------------------------------------------------
				-- tProductRegistryProduct				
				
				IF @IsProductRegistry = 0
				BEGIN
					INSERT INTO product.tProductRegistryProduct (
						ProductId, 
						ProductRegistryId
					)
					VALUES (
						@ProductId,
						@ProductRegistryId
					)
				END

				------------------------------------------------------------------------------------------
				-- tPriceListItem				
				
				IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem
							   WHERE ProductId = @ProductId
							   AND PriceListId = @PriceListId)
				BEGIN
					INSERT INTO product.tPriceListItem (
						PriceListId,
						ProductId,
						PriceIncludingVat,
						PriceExcludingVat,
						VatPercentage
					)
					VALUES (
						@PriceListId,
						@ProductId,
						@Price,
						@Price / (1.0+@Vat/100.0),
						@Vat
					)
				END

				------------------------------------------------------------------------------------------ 
				-- tTradeDoublerProductGroupMapping

				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = @HYArticleClassId
													   AND ChannelId = @Channel)

				

				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM  lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId
										 AND tTDPGM.ChannelId = @ChannelId)
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							@ChannelId
						)
					END
				END 
			END

			COMMIT
			
			PRODUCT_END:
			
			--ProductSize
			
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [import].[pImportProductSize]
				@HYErpId,
				@HYErpIdSize,
				@Channel, -- Channel id value from HY
				@SizeId,
				@NumberInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@ProductId
			
			-- tProductTag
			-- tag products under category 'Smycken' OR 'Skotillbehör\Skovård' OR 'Skotillbehör\Skotillbehör' with CategoryLevel3
			IF (@VaruklassId = '50' 
				OR (@VaruklassId = '20' AND @VarugruppId = '201')
				OR (@VaruklassId = '20' AND @VarugruppId = '202'))
			BEGIN
				SET @TagId_1 = NULL
				
				SELECT @TagId_1 = TagId
				FROM lekmer.tTag
				WHERE Value = @VarukodTitel
					  AND TagGroupId = @TagGroupId_CategoryLevel3
					  
				IF @TagId_1 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1
								   FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND Tagid = @TagId_1)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_1
					END
				END
			END
			
			-- tag products under category 'Skotillbehör\Skotillbehör\Sula' with CategoryLevel3
			IF (@VaruklassId = '20' AND @VarugruppId = '202' AND @VarukodId = '2209')
			BEGIN
				SET @TagId_1 = 1000521 --Ilaggssulor
				IF NOT EXISTS (SELECT 1
							   FROM lekmer.tProductTag
							   WHERE ProductId = @ProductId
									 AND Tagid = @TagId_1)
				BEGIN
					EXEC lekmer.pProductTagSave @ProductId, @TagId_1
				END
			END
			
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK

			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
			
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)
			
		END CATCH

		FETCH_NEXT: --LABEL
		FETCH NEXT FROM cur_product INTO
			 @HYErpId,
			 @HYErpIdSize,
			 @Channel,			 
			 @ArticleTitle,
			 @SizeId,
			 @Price,
			 @NumberInStock,
			 @VarugruppId,
			 @VarugruppTitel,
			 @VaruklassId,
			 @VarukodId,
			 @VarukodTitel,
			 @HYBrandId
	END

	CLOSE cur_product
	DEALLOCATE cur_product


	EXEC [import].[pUpdateProductBrand]
	EXEC [import].[pAddProductColorTags]
	-- temp lösning	
	--EXEC [integration].[usp_REATagProduct] done as schedule task

	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]

	-- Track products chnages to eSales export
	EXEC [integration].[pTrackProductChangesHeppo]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
