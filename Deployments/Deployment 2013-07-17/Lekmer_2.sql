/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 7/17/2013 10:01:13 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraint FK_tModelFragmentEntity_tModelFragment from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Delete rows from [template].[tModelFragmentFunction]
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000804
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000805
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000818
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000819
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000820
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000821
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000822
-- Operation applied to 7 rows out of 7

-- Delete rows from [template].[tModelFragment]
DELETE FROM [template].[tModelFragment] WHERE [ModelFragmentId]=1001227
DELETE FROM [template].[tModelFragment] WHERE [ModelFragmentId]=1001228
-- Operation applied to 2 rows out of 2

-- Delete row from [template].[tModelFragmentRegion]
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=1000199

-- Update rows in [template].[tModelFragmentFunction]
UPDATE [template].[tModelFragmentFunction] SET [CommonName]=N'Form.UnregFromInterspire.Name' WHERE [FunctionId]=1000807
UPDATE [template].[tModelFragmentFunction] SET [CommonName]=N'Form.UnregFromInterspire.Value' WHERE [FunctionId]=1000812
UPDATE [template].[tModelFragmentFunction] SET [CommonName]=N'Form.UnregFromInterspire.Selected' WHERE [FunctionId]=1000815
-- Operation applied to 3 rows out of 3

-- Add row to [lekmer].[tNewsletterSubscriberType]
INSERT INTO [lekmer].[tNewsletterSubscriberType] ([NewsletterSubscriberTypeId], [Title], [CommonName]) VALUES (3, N'BackOffice', N'BackOffice')

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraint FK_tModelFragmentEntity_tModelFragment to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] WITH CHECK ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
COMMIT TRANSACTION
GO
