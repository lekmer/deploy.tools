SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tNewsletterUnsubscriberOption]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] DROP CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterUnsubscriber]
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] DROP CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tNewsletterUnsubscriberOption_Unique] from [lekmer].[tNewsletterUnsubscriberOption]'
GO
DROP INDEX [IX_tNewsletterUnsubscriberOption_Unique] ON [lekmer].[tNewsletterUnsubscriberOption]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tNewsletterUnsubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriber] DROP
COLUMN [UnregFromAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tNewsletterUnsubscriberOption]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] DROP
COLUMN [InterspireContactList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterSubscriberDeleteByEmail]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberDeleteByEmail]
	@ChannelId	INT,
	@Email		VARCHAR(320)
AS
BEGIN
	DELETE
		[lekmer].[tNewsletterSubscriber]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
	RETURN @@ROWCOUNT
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vNewsletterUnsubscriberOption]'
GO



ALTER VIEW [lekmer].[vNewsletterUnsubscriberOption]
AS
SELECT
	[UnsubscriberOprionId] AS 'NewsletterUnsubscriberOption.UnsubscriberOprionId',
	[UnsubscriberId] AS 'NewsletterUnsubscriberOption.UnsubscriberId',
	[NewsletterTypeId] AS 'NewsletterUnsubscriberOption.NewsletterTypeId',
	[CreatedDate] AS 'NewsletterUnsubscriberOption.CreatedDate'
FROM
	[lekmer].[tNewsletterUnsubscriberOption]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vNewsletterUnsubscriber]'
GO


ALTER VIEW [lekmer].[vNewsletterUnsubscriber]
AS
SELECT
	[UnsubscriberId] AS 'NewsletterUnsubscriber.UnsubscriberId',
	[ChannelId] AS 'NewsletterUnsubscriber.ChannelId',
	[Email] AS 'NewsletterUnsubscriber.Email',
	[CreatedDate] AS 'NewsletterUnsubscriber.CreatedDate',
	[UpdatedDate] AS 'NewsletterUnsubscriber.UpdatedDate'
FROM
	[lekmer].[tNewsletterUnsubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriber_GetById]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_GetById]
	@Id	INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[lekmer].[vNewsletterUnsubscriber]
	WHERE
		[NewsletterUnsubscriber.UnsubscriberId] = @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pNewsletterUnsubscriber_Delete]'
GO
ALTER PROCEDURE [lekmer].[pNewsletterUnsubscriber_Delete]
	@Id	INT
AS
BEGIN
	SET NOCOUNT ON
	
	-- Remove Options
	DELETE
		[lekmer].[tNewsletterUnsubscriberOption]
	FROM
		[lekmer].[tNewsletterUnsubscriberOption]
	WHERE
		[UnsubscriberId] = @Id
	
	DELETE
		[lekmer].[tNewsletterUnsubscriber]
	WHERE
		[UnsubscriberId] = @Id
		
	RETURN @@ROWCOUNT
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterSubscriberGetById]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberGetById]
	@Id	INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[lekmer].[vNewsletterSubscriber]
	WHERE
		[NewsletterSubscriber.SubscriberId] = @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pNewsletterUnsubscriber_Save]'
GO
ALTER PROCEDURE [lekmer].[pNewsletterUnsubscriber_Save]
	@ChannelId		INT,
	@Email			VARCHAR(320),
	@CreatedDate	DATETIME,
	@UpdateDate		DATETIME
AS
BEGIN
	DECLARE @Id INT
	UPDATE
		[lekmer].[tNewsletterUnsubscriber]
	SET
		[UpdatedDate] = @UpdateDate,
		@Id = [UnsubscriberId]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tNewsletterUnsubscriber]
		(
			[ChannelId],
			[Email],
			[CreatedDate],
			[UpdatedDate]
		)
		VALUES
		(
			@ChannelId,
			@Email,
			@CreatedDate,
			@UpdateDate
		)
		SET @Id = SCOPE_IDENTITY()
	END
	RETURN @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pNewsletterSubscriberDelete]'
GO
ALTER PROCEDURE [lekmer].[pNewsletterSubscriberDelete]
	@Id	INT
AS
BEGIN
	DELETE
		[lekmer].[tNewsletterSubscriber]
	WHERE
		[SubscriberId] = @Id
	RETURN @@ROWCOUNT
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pNewsletterUnsubscriberOption_Save]'
GO
ALTER PROCEDURE [lekmer].[pNewsletterUnsubscriberOption_Save]
	@UnsubscriberId INT,
	@NewsletterTypeId INT,
	@CreatedDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [lekmer].[tNewsletterUnsubscriberOption]
	(
		[UnsubscriberId],
		[NewsletterTypeId],
		[CreatedDate]
	)
	VALUES
	(
		@UnsubscriberId,
		@NewsletterTypeId,
		@CreatedDate
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriber_DeleteByEmail]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_DeleteByEmail]
	@ChannelId	INT,
	@Email		VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Remove Options
	DELETE
		[lekmer].[tNewsletterUnsubscriberOption]
	FROM
		[lekmer].[tNewsletterUnsubscriber] nu
		INNER JOIN [lekmer].[tNewsletterUnsubscriberOption] nuo ON [nuo].[UnsubscriberId] = [nu].[UnsubscriberId]
	WHERE
		nu.[Email] = @Email AND
		nu.[ChannelId] = @ChannelId
	
	DELETE
		[lekmer].[tNewsletterUnsubscriber]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
		
	RETURN @@ROWCOUNT
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterSubscriberSearchByEmail]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberSearchByEmail]
	@ChannelId		INT,
	@Email			NVARCHAR(MAX),
	@Page			INT = NULL,
	@PageSize		INT,
	@SortBy			VARCHAR(20) = NULL,
	@SortAcsending	BIT = 1
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

    SET @sqlFragment = 'SELECT ROW_NUMBER() OVER (ORDER BY ' 
		+ COALESCE(@SortBy, '[ns].[NewsletterSubscriber.Email]')
		+ CASE WHEN (@SortAcsending = 0) THEN ' DESC' ELSE ' ASC' END + ') AS _Number,
		[ns].*
		FROM [lekmer].[vNewsletterSubscriber] ns
		WHERE [ns].[NewsletterSubscriber.Email] LIKE ''%'' + @Email + ''%''
		AND [ns].[NewsletterSubscriber.ChannelId] = @ChannelId'
		
	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE _Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND _Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	INT,
			@Email		NVARCHAR(MAX)',
			@ChannelId,
			@Email

	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT,
			@Email		NVARCHAR(MAX)',
			@ChannelId,
			@Email
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriber_SearchByEmail]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_SearchByEmail]
	@ChannelId		INT,
	@Email			NVARCHAR(MAX),
	@Page			INT = NULL,
	@PageSize		INT,
	@SortBy			VARCHAR(20) = NULL,
	@SortAcsending	BIT = 1
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

    SET @sqlFragment = 'SELECT ROW_NUMBER() OVER (ORDER BY ' 
		+ COALESCE(@SortBy, '[ns].[NewsletterUnsubscriber.Email]')
		+ CASE WHEN (@SortAcsending = 0) THEN ' DESC' ELSE ' ASC' END + ') AS _Number,
		[ns].*
		FROM [lekmer].[vNewsletterUnsubscriber] ns
		WHERE [ns].[NewsletterUnsubscriber.Email] LIKE ''%'' + @Email + ''%''
		AND [ns].[NewsletterUnsubscriber.ChannelId] = @ChannelId'
		
	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE _Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND _Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	INT,
			@Email		NVARCHAR(MAX)',
			@ChannelId,
			@Email

	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT,
			@Email		NVARCHAR(MAX)',
			@ChannelId,
			@Email
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tNewsletterUnsubscriberOption]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] ADD CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterUnsubscriber] FOREIGN KEY ([UnsubscriberId]) REFERENCES [lekmer].[tNewsletterUnsubscriber] ([UnsubscriberId])
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] ADD CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterType] FOREIGN KEY ([NewsletterTypeId]) REFERENCES [lekmer].[tNewsletterType] ([NewsletterTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
