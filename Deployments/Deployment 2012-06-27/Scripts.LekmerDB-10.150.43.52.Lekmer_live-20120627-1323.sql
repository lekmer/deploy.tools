/*
Run this script on:

        10.150.43.52.Lekmer_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 019\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 27.06.2012 13:23:51

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [lekmer].[tLekmerCategory]'
GO
CREATE TABLE [lekmer].[tLekmerCategory]
(
[CategoryId] [int] NOT NULL,
[AllowMultipleSizesPurchase] [bit] NULL CONSTRAINT [DF_tLekmerCategory_AllowMultipleSizesPurchase] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tLekmerCategory] on [lekmer].[tLekmerCategory]'
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD CONSTRAINT [PK_tLekmerCategory] PRIMARY KEY CLUSTERED  ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCategorySave]'
GO

CREATE PROCEDURE [lekmer].[pCategorySave]
	@CategoryId					int,
	@Title						nvarchar(50),
	@AllowMultipleSizesPurchase	bit
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tCategory]
	SET
		[Title] = @Title
	WHERE
		[CategoryId] = @CategoryId
	
	SET NOCOUNT OFF
		
	UPDATE
		[lekmer].[tLekmerCategory]
	SET
		[AllowMultipleSizesPurchase] = @AllowMultipleSizesPurchase
	WHERE
		[CategoryId] = @CategoryId
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			[lekmer].[tLekmerCategory]
		( 
			[CategoryId],
			[AllowMultipleSizesPurchase]
		)
		VALUES 
		(
			@CategoryId,
			@AllowMultipleSizesPurchase
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategory]'
GO



ALTER view [product].[vCustomCategory]
as
	select
		c.*,
		l.AllowMultipleSizesPurchase as [LekmerCategory.AllowMultipleSizesPurchase]
	from
		[product].[vCategory] c
		left join [lekmer].[tLekmerCategory] l on c.[Category.Id] = l.CategoryId


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategorySecure]'
GO


ALTER view [product].[vCustomCategorySecure]
as
	select
		c.*,
		l.AllowMultipleSizesPurchase as [LekmerCategory.AllowMultipleSizesPurchase]
	from
		[product].[vCategorySecure] c
		left join [lekmer].[tLekmerCategory] l on c.[Category.Id] = l.CategoryId
		
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerCategory]'
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD
CONSTRAINT [FK_tLekmerCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
