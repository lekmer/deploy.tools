SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] DROP CONSTRAINT [FK_tGiftCardViaMailInfo_tCampaign]
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] DROP CONSTRAINT [FK_tGiftCardViaMailInfo_tGiftCardViaMailInfo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [integration].[pReportOutOfStockRelationLists]'
GO
--DROP PROCEDURE [integration].[pReportOutOfStockRelationLists]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCartDoesNotContainCondition]'
GO
CREATE TABLE [campaignlek].[tCartDoesNotContainCondition]
(
[ConditionId] [int] NOT NULL,
[ConfigId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartDoesNotContainCondition] on [campaignlek].[tCartDoesNotContainCondition]'
GO
ALTER TABLE [campaignlek].[tCartDoesNotContainCondition] ADD CONSTRAINT [PK_tCartDoesNotContainCondition] PRIMARY KEY CLUSTERED  ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD
[VoucherInfoId] [int] NULL,
[VoucherCode] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionConfiguratorDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionConfiguratorDeleteAll]
	@ConfigId INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [campaignlek].[pCampaignActionIncludeBrandDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeBrandDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeCategoryDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeCategoryDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeProductDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeProductDeleteAll] @ConfigId
	
	DELETE FROM [campaignlek].[tCampaignActionConfigurator]
	WHERE CampaignActionConfiguratorId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pPercentagePriceDiscountActionDelete]'
GO

ALTER PROCEDURE [campaign].[pPercentagePriceDiscountActionDelete]
	@ProductActionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaign].[tPercentagePriceDiscountAction] WHERE ProductActionId = @ProductActionId)

	DELETE FROM [campaign].[tPercentagePriceDiscountAction]
	WHERE ProductActionId = @ProductActionId
	
	EXEC [campaignlek].[pCampaignActionConfiguratorDeleteAll] @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pReportRelationListsWithOutOfStockProducts]'
GO
ALTER PROCEDURE [integration].[pReportRelationListsWithOutOfStockProducts]
AS
BEGIN
	DECLARE @tRelationListProduct TABLE (
			RelationListId INT,
			RelationListTitle NVARCHAR(256),
			RelationListCommonName VARCHAR(50),
			ProductId INT,
			ProductErpId VARCHAR(50),
			ProductTitle NVARCHAR(256))

	INSERT INTO @tRelationListProduct (
		[RelationListId],
		[RelationListTitle],
		[RelationListCommonName],
		[ProductId],
		[ProductErpId],
		[ProductTitle])
	SELECT
		[rl].[RelationListId] 'RelationListId',
		[rl].[Title] 'RelationListTitle',
		[rlt].[CommonName] 'RelationListCommonName',
		[rlp].[ProductId] 'ProductId',
		[p].[ErpId],
		[p].[Title]
	FROM [product].[tRelationList] rl
		 INNER JOIN [product].[tRelationListType] rlt ON [rlt].[RelationListTypeId] = [rl].[RelationListTypeId]
		 INNER JOIN [product].[tRelationListProduct] rlp ON [rlp].[RelationListId] = [rl].[RelationListId]
		 INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [rlp].[ProductId]
		 CROSS APPLY (
				SELECT SUM(ps.[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId]
			) s
	WHERE
		(([s].[NumberInStock] IS NULL AND [p].[NumberInStock] < 1)
		OR
		([s].[NumberInStock] IS NOT NULL AND [s].[NumberInStock] < 1))
		AND 
		[rl].[RelationListTypeId] <> 1000001
	ORDER BY
		[rl].[RelationListId]


	SELECT * FROM @tRelationListProduct
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailInfoUpdate]'
GO
ALTER PROCEDURE [campaignlek].[pGiftCardViaMailInfoUpdate]
	@GiftCardViaMailInfoId	INT,
	@StatusId	INT,
	@VoucherInfoId	INT,
	@VoucherCode	NVARCHAR(100)
AS
BEGIN
	UPDATE	[campaignlek].[tGiftCardViaMailInfo]
	SET		[StatusId] = @StatusId
	WHERE	GiftCardViaMailInfoId = @GiftCardViaMailInfoId
		
	IF @VoucherInfoId IS NOT NULL
		UPDATE	[campaignlek].[tGiftCardViaMailInfo]
		SET		[VoucherInfoId] = @VoucherInfoId
		WHERE	GiftCardViaMailInfoId = @GiftCardViaMailInfoId
	
	IF @VoucherCode IS NOT NULL
		UPDATE	[campaignlek].[tGiftCardViaMailInfo]
		SET		[VoucherCode] = @VoucherCode
		WHERE	GiftCardViaMailInfoId = @GiftCardViaMailInfoId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCampaignActionConfigurator]'
GO

CREATE VIEW [campaignlek].[vCampaignActionConfigurator]
AS
	SELECT
		[cac].[CampaignActionConfiguratorId] AS 'CampaignActionConfigurator.CampaignActionConfiguratorId',
		[cac].[IncludeAllProducts] AS 'CampaignActionConfigurator.IncludeAllProducts'
	FROM
		[campaignlek].[tCampaignActionConfigurator] cac
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pReportProductsForRelationListsWithOutOfStockProducts]'
GO
ALTER PROCEDURE [integration].[pReportProductsForRelationListsWithOutOfStockProducts]
AS
BEGIN
	DECLARE @tRelationListProduct TABLE (
			RelationListId INT,
			RelationListTitle NVARCHAR(256),
			RelationListCommonName VARCHAR(50),
			ProductId INT,
			ProductErpId VARCHAR(50),
			ProductTitle NVARCHAR(256))

	INSERT INTO @tRelationListProduct (
		[RelationListId],
		[RelationListTitle],
		[RelationListCommonName],
		[ProductId],
		[ProductErpId],
		[ProductTitle])
	SELECT
		[rl].[RelationListId] 'RelationListId',
		[rl].[Title] 'RelationListTitle',
		[rlt].[CommonName] 'RelationListCommonName',
		[rlp].[ProductId] 'ProductId',
		[p].[ErpId],
		[p].[Title]
	FROM [product].[tRelationList] rl
		 INNER JOIN [product].[tRelationListType] rlt ON [rlt].[RelationListTypeId] = [rl].[RelationListTypeId]
		 INNER JOIN [product].[tRelationListProduct] rlp ON [rlp].[RelationListId] = [rl].[RelationListId]
		 INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [rlp].[ProductId]
		 CROSS APPLY (
				SELECT SUM(ps.[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId]
			) s
	WHERE
		(([s].[NumberInStock] IS NULL AND [p].[NumberInStock] < 1)
		OR
		([s].[NumberInStock] IS NOT NULL AND [s].[NumberInStock] < 1))
		AND 
		[rl].[RelationListTypeId] <> 1000001
	ORDER BY
		[rl].[RelationListId]

	SELECT
		[rl].[RelationListId] 'RelationListId',
		[rl].[Title] 'RelationListTitle',
		[rlt].[CommonName] 'RelationListCommonName',
		[rlp].[ProductId] 'ProductId',
		[p].[ErpId] 'ProductErpId',
		[p].[Title] 'ProductTitle'
	FROM [product].[tRelationList] rl
		 INNER JOIN [product].[tRelationListType] rlt ON [rlt].[RelationListTypeId] = [rl].[RelationListTypeId]
		 INNER JOIN [product].[tProductRelationList] rlp ON [rlp].[RelationListId] = [rl].[RelationListId]
		 INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [rlp].[ProductId]
	WHERE
		[rl].[RelationListId] IN (SELECT [RelationListId] FROM @tRelationListProduct)
	ORDER BY
		[rl].[RelationListId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartDoesNotContainConditionDelete]'
GO
CREATE PROCEDURE [campaignlek].[pCartDoesNotContainConditionDelete]
	@ConditionId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaignlek].[tCartDoesNotContainCondition] WHERE [ConditionId] = @ConditionId)
	
	DELETE FROM
		[campaignlek].[tCartDoesNotContainCondition]
	WHERE
		[ConditionId] = @ConditionId
		
	EXEC [campaignlek].[pCampaignActionConfiguratorDeleteAll] @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vGiftCardViaMailInfo]'
GO

CREATE VIEW [campaignlek].[vGiftCardViaMailInfo]
AS
	SELECT 
		gcvmi.[GiftCardViaMailInfoId] AS 'GiftCardViaMailInfo.GiftCardViaMailInfoId',
		gcvmi.[OrderId] AS 'GiftCardViaMailInfo.OrderId',
		gcvmi.[CampaignId] AS 'GiftCardViaMailInfo.CampaignId',
		gcvmi.[DiscountValue] AS 'GiftCardViaMailInfo.DiscountValue',
		gcvmi.[DateToSend] AS 'GiftCardViaMailInfo.DateToSend',
		gcvmi.[StatusId] AS 'GiftCardViaMailInfo.StatusId',
		gcvmi.[VoucherInfoId] AS 'GiftCardViaMailInfo.VoucherInfoId',
		gcvmi.[VoucherCode] AS 'GiftCardViaMailInfo.VoucherCode',
		o.[ChannelId] AS 'GiftCardViaMailInfo.ChannelId'
	FROM
		[campaignlek].[tGiftCardViaMailInfo] gcvmi
		INNER JOIN [order].[tOrder] o ON o.[OrderId] = gcvmi.[OrderId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailInfoGetAllByOrder]'
GO
ALTER PROCEDURE [campaignlek].[pGiftCardViaMailInfoGetAllByOrder]
	@OrderId INT
AS 
BEGIN
	SELECT
		gcvmi.*
	FROM
		[campaignlek].[vGiftCardViaMailInfo] gcvmi
	WHERE
		gcvmi.[GiftCardViaMailInfo.OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tKlarnaTransaction]'
GO
CREATE TABLE [orderlek].[tKlarnaTransaction]
(
[KlarnaTransactionId] [int] NOT NULL IDENTITY(1, 1),
[KlarnaShopId] [int] NOT NULL,
[KlarnaMode] [int] NOT NULL,
[KlarnaTransactionTypeId] [int] NOT NULL,
[Created] [datetime] NOT NULL,
[CivicNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[OrderId] [int] NULL,
[Amount] [decimal] (16, 2) NULL,
[CurrencyId] [int] NULL,
[PClassId] [int] NULL,
[YearlySalary] [int] NULL,
[Duration] [bigint] NULL,
[ReservationNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ResponseCodeId] [int] NULL,
[KlarnaFaultCode] [int] NULL,
[KlarnaFaultReason] [varchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tKlarnaTransaction] on [orderlek].[tKlarnaTransaction]'
GO
ALTER TABLE [orderlek].[tKlarnaTransaction] ADD CONSTRAINT [PK_tKlarnaTransaction] PRIMARY KEY CLUSTERED  ([KlarnaTransactionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pKlarnaTransactionCreateReservation]'
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionCreateReservation]
	@KlarnaShopId INT,
	@KlarnaMode INT,
	@KlarnaTransactionTypeId INT,
	@CreatedDate DATETIME,
	@CivicNumber VARCHAR(50),
	@OrderId INT,
	@Amount DECIMAL(16,2),
	@CurrencyId INT,
	@PClassId INT,
	@YearlySalary INT
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tKlarnaTransaction]
			( [KlarnaShopId],
			  [KlarnaMode],
			  [KlarnaTransactionTypeId],
			  [Created],
			  [CivicNumber],
			  [OrderId],
			  [Amount],
			  [CurrencyId],
			  [PClassId],
			  [YearlySalary]
			)
	VALUES
			( @KlarnaShopId,
			  @KlarnaMode,
			  @KlarnaTransactionTypeId,
			  @CreatedDate,
			  @CivicNumber,
			  @OrderId,
			  @Amount,
			  @CurrencyId,
			  @PClassId,
			  @YearlySalary
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailInfoInsert]'
GO

ALTER PROCEDURE [campaignlek].[pGiftCardViaMailInfoInsert]
	@OrderId		INT,
	@CampaignId		INT,
	@DiscountValue	DECIMAL(16,2),
	@DateToSend		DATETIME,
	@StatusId		INT
AS
BEGIN
	INSERT [campaignlek].[tGiftCardViaMailInfo] (
		OrderId,
		CampaignId,
		DiscountValue,
		DateToSend,
		StatusId
	)
	VALUES (
		@OrderId,
		@CampaignId,
		@DiscountValue,
		@DateToSend,
		@StatusId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailInfoGetAllToSend]'
GO
ALTER PROCEDURE [campaignlek].[pGiftCardViaMailInfoGetAllToSend]
AS
BEGIN
	SELECT 
		gcvmi.*
	FROM
		[campaignlek].[vGiftCardViaMailInfo] gcvmi
		INNER JOIN [order].[tOrder] o ON o.[OrderId] = gcvmi.[GiftCardViaMailInfo.OrderId]
	WHERE
		o.[OrderStatusId] = 4 -- OrderInHY
		  AND gcvmi.[GiftCardViaMailInfo.StatusId] = 1 -- ReadyToSend
		  AND gcvmi.[GiftCardViaMailInfo.DateToSend] < GETDATE()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pKlarnaTransactionCreateGetAddress]'
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionCreateGetAddress]
	@KlarnaShopId INT,
	@KlarnaMode INT,
	@KlarnaTransactionTypeId INT,
	@CreatedDate DATETIME,
	@CivicNumber VARCHAR(50)
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tKlarnaTransaction]
			( [KlarnaShopId],
			  [KlarnaMode],
			  [KlarnaTransactionTypeId],
			  [Created],
			  [CivicNumber]
			)
	VALUES
			( @KlarnaShopId,
			  @KlarnaMode,
			  @KlarnaTransactionTypeId,
			  @CreatedDate,
			  @CivicNumber
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pLekmerOrderMonitor]'
GO
CREATE PROCEDURE [lekmer].[pLekmerOrderMonitor]
	@Date				DATETIME,
	@MinAfterPurchase	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder] 
	WHERE
		[Order.CreatedDate] >= DATEADD(MINUTE, -@MinAfterPurchase, @Date)
		AND [Order.CreatedDate] < @Date
		AND [OrderStatus.CommonName] = 'OrderInHY'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vKlarnaTransaction]'
GO

CREATE VIEW [orderlek].[vKlarnaTransaction]
AS
SELECT
	[kt].[KlarnaTransactionId],
	[kt].[KlarnaShopId],
	[kt].[KlarnaMode],
	
	CASE [kt].[KlarnaMode]
		WHEN 0 THEN 'NO_FLAG'
		WHEN 2 THEN 'KRED_TEST_MODE'
		ELSE CONVERT(VARCHAR(50), [kt].[KlarnaMode])
	END AS 'KlarnaMode.Name',
	
	[kt].[KlarnaTransactionTypeId],
	
	CASE [kt].[KlarnaTransactionTypeId]
		WHEN 1 THEN 'GetAddress'
		WHEN 2 THEN 'ReserveAmount'
		WHEN 2 THEN 'ReserveAmountCompany'
		ELSE CONVERT(VARCHAR(50), [kt].[KlarnaTransactionTypeId])
	END AS 'KlarnaTransactionType.Name',
	
	[kt].[Created],
	[kt].[CivicNumber],
	[kt].[OrderId],
	[kt].[Amount],
	[kt].[CurrencyId],
	
	CASE [kt].[CurrencyId]
		WHEN 0 THEN 'SEK'
		WHEN 1 THEN 'NOK'
		WHEN 2 THEN 'EUR'
		WHEN 3 THEN 'DKK'
		ELSE CONVERT(VARCHAR(50), [kt].[CurrencyId])
	END AS 'Currency.Code',
	
	[kt].[PClassId],
	[kt].[YearlySalary],
	[kt].[Duration],
	[kt].[ReservationNumber],
	[kt].[ResponseCodeId],
	
	CASE [kt].[ResponseCodeId]
		WHEN 1 THEN 'OK'
		WHEN 2 THEN 'NoRisk'
		WHEN 3 THEN 'TimeoutResponse'
		WHEN 4 THEN 'SpecifiedError'
		WHEN 5 THEN 'UnspecifiedError'
		ELSE CONVERT(VARCHAR(50), [kt].[ResponseCodeId])
	END AS 'ResponseCode.Name',
	
	[kt].[KlarnaFaultCode],
	[kt].[KlarnaFaultReason]
FROM
	[orderlek].[tKlarnaTransaction] kt
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailProductActionDelete]'
GO

ALTER PROCEDURE [campaignlek].[pGiftCardViaMailProductActionDelete]
	@ProductActionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaignlek].[tGiftCardViaMailProductAction] WHERE ProductActionId = @ProductActionId)

	EXEC [campaignlek].[pGiftCardViaMailProductActionCurrencyDeleteAll] @ProductActionId

	DELETE FROM [campaignlek].[tGiftCardViaMailProductAction]
	WHERE ProductActionId = @ProductActionId
	
	EXEC [campaignlek].[pCampaignActionConfiguratorDeleteAll] @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionConfiguratorSave]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionConfiguratorSave]
	@ConfiguratorId		INT,
	@IncludeAllProducts	BIT
AS
BEGIN
	UPDATE
		[campaignlek].[tCampaignActionConfigurator]
	SET
		[IncludeAllProducts] = @IncludeAllProducts
	WHERE
		[CampaignActionConfiguratorId] = @ConfiguratorId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT	INTO [campaignlek].[tCampaignActionConfigurator]
				( [IncludeAllProducts] )
		VALUES
				( @IncludeAllProducts )

		SET @ConfiguratorId = SCOPE_IDENTITY()
	END

	RETURN @ConfiguratorId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionConfiguratorGetById]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionConfiguratorGetById]
	@ConfiguratorId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCampaignActionConfigurator]
	WHERE
		[CampaignActionConfigurator.CampaignActionConfiguratorId] = @ConfiguratorId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemsGroupValueConditionDelete]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemsGroupValueConditionDelete]
	@ConditionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaignlek].[tCartItemsGroupValueCondition] WHERE [ConditionId] = @ConditionId)

	EXEC [campaignlek].[pCartItemsGroupValueConditionCurrencyDeleteAll] @ConditionId

	DELETE FROM [campaignlek].[tCartItemsGroupValueCondition]
	WHERE [ConditionId] = @ConditionId

	EXEC [campaignlek].[pCampaignActionConfiguratorDeleteAll] @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartDoesNotContainConditionSave]'
GO
CREATE PROCEDURE [campaignlek].[pCartDoesNotContainConditionSave]
	@ConditionId		INT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tCartDoesNotContainCondition]
	SET
		ConfigId = @ConfigId
	WHERE
		[ConditionId] = @ConditionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tCartDoesNotContainCondition] (
			ConditionId,
			ConfigId
		)
		VALUES (
			@ConditionId,
			@ConfigId
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pKlarnaTransactionSaveReservationResponse]'
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionSaveReservationResponse]
	@KlarnaTransactionId INT,
	@ReservationNumber VARCHAR(50),
	@ResponseCodeId INT,
	@KlarnaFaultCode INT,
	@KlarnaFaultReason NVARCHAR(MAX),
	@Duration BIGINT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tKlarnaTransaction]
	SET
		[ReservationNumber] = @ReservationNumber,
		[ResponseCodeId] = @ResponseCodeId,
		[KlarnaFaultCode] = @KlarnaFaultCode,
		[KlarnaFaultReason] = @KlarnaFaultReason,
		[Duration] = @Duration
	WHERE
		[KlarnaTransactionId] = @KlarnaTransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCartDoesNotContainCondition]'
GO


CREATE VIEW [campaignlek].[vCartDoesNotContainCondition]
AS
	SELECT
		[cdncc].[ConditionId] AS 'CartDoesNotContainCondition.ConditionId',
		[cdncc].[ConfigId] AS 'CartDoesNotContainCondition.ConfigId',
		[cc].*
	FROM
		[campaignlek].[tCartDoesNotContainCondition] cdncc
		INNER JOIN [campaign].[vCustomCondition] cc ON [cc].[Condition.Id] = [cdncc].[ConditionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartDoesNotContainConditionGetById]'
GO

CREATE PROCEDURE [campaignlek].[pCartDoesNotContainConditionGetById]
	@ConditionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartDoesNotContainCondition]
	WHERE
		[Condition.Id] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pKlarnaTransactionSaveResult]'
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionSaveResult]
	@KlarnaTransactionId INT,
	@ResponseCodeId INT,
	@KlarnaFaultCode INT,
	@KlarnaFaultReason NVARCHAR(MAX),
	@Duration BIGINT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tKlarnaTransaction]
	SET
		[ResponseCodeId] = @ResponseCodeId,
		[KlarnaFaultCode] = @KlarnaFaultCode,
		[KlarnaFaultReason] = @KlarnaFaultReason,
		[Duration] = @Duration
	WHERE
		[KlarnaTransactionId] = @KlarnaTransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartDoesNotContainCondition]'
GO
ALTER TABLE [campaignlek].[tCartDoesNotContainCondition] ADD CONSTRAINT [FK_tCartDoesNotContainCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
ALTER TABLE [campaignlek].[tCartDoesNotContainCondition] ADD CONSTRAINT [FK_tCartDoesNotContainCondition_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD CONSTRAINT [FK_tGiftCardViaMailInfo_tGiftCardViaMailInfoStatus] FOREIGN KEY ([StatusId]) REFERENCES [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
