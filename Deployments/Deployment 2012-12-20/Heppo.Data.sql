/*
Run this script on a database with the same schema as:

HeppoDB – the database with this schema will be modified

to synchronize its data with:

HeppoDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 12/20/2012 10:19:17 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Update rows in [campaign].[tConditionType]
UPDATE [campaign].[tConditionType] SET [Title]=N'Customer group' WHERE [ConditionTypeId]=1000003
UPDATE [campaign].[tConditionType] SET [Title]=N'Cart items group value' WHERE [ConditionTypeId]=1000004
-- Operation applied to 2 rows out of 2

-- Add rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000786, 84, 2, N'Klarna.GetAddress.IsTimeoutResponse', N'[True] when GetAddress function failed due timeout response.')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
-- Operation applied to 1 rows out of 14

-- Add rows to [campaignlek].[tGiftCardViaMailInfoStatus]
SET IDENTITY_INSERT [campaignlek].[tGiftCardViaMailInfoStatus] ON
INSERT INTO [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId], [Title], [CommonName]) VALUES (3, N'ExceedsQuantity', N'ExceedsQuantity')
INSERT INTO [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId], [Title], [CommonName]) VALUES (4, N'EmptyVoucherCode', N'EmptyVoucherCode')
SET IDENTITY_INSERT [campaignlek].[tGiftCardViaMailInfoStatus] OFF
-- Operation applied to 2 rows out of 2

-- Add row to [campaign].[tConditionType]
SET IDENTITY_INSERT [campaign].[tConditionType] ON
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (1000005, N'Cart does not contain', N'CartDoesNotContain')
SET IDENTITY_INSERT [campaign].[tConditionType] OFF

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
COMMIT TRANSACTION
GO
