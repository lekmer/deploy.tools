SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] DROP CONSTRAINT [FK_tGiftCardViaMailInfo_tOrder]
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] DROP CONSTRAINT [FK_tGiftCardViaMailInfo_tGiftCardViaMailInfoStatus]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] DROP CONSTRAINT [PK_tGiftCardViaMailInfo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[tGiftCardViaMailCartAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartAction] ADD
[TemplateId] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[tGiftCardViaMailProductAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] ADD
[TemplateId] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [campaignlek].[tGiftCardViaMailInfo]'
GO
CREATE TABLE [campaignlek].[tmp_rg_xx_tGiftCardViaMailInfo]
(
[GiftCardViaMailInfoId] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[CampaignId] [int] NOT NULL,
[ActionId] [int] NOT NULL,
[DiscountValue] [decimal] (16, 2) NOT NULL,
[DateToSend] [datetime] NOT NULL,
[StatusId] [int] NOT NULL,
[VoucherInfoId] [int] NULL,
[VoucherCode] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [campaignlek].[tmp_rg_xx_tGiftCardViaMailInfo] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [campaignlek].[tmp_rg_xx_tGiftCardViaMailInfo]([GiftCardViaMailInfoId], [OrderId], [CampaignId], [DiscountValue], [DateToSend], [StatusId], [VoucherInfoId], [VoucherCode]) SELECT [GiftCardViaMailInfoId], [OrderId], [CampaignId], [DiscountValue], [DateToSend], [StatusId], [VoucherInfoId], [VoucherCode] FROM [campaignlek].[tGiftCardViaMailInfo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [campaignlek].[tmp_rg_xx_tGiftCardViaMailInfo] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[campaignlek].[tGiftCardViaMailInfo]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[campaignlek].[tmp_rg_xx_tGiftCardViaMailInfo]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [campaignlek].[tGiftCardViaMailInfo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[campaignlek].[tmp_rg_xx_tGiftCardViaMailInfo]', N'tGiftCardViaMailInfo'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tGiftCardViaMailInfo] on [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD CONSTRAINT [PK_tGiftCardViaMailInfo] PRIMARY KEY CLUSTERED  ([GiftCardViaMailInfoId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailInfoInsert]'
GO

ALTER PROCEDURE [campaignlek].[pGiftCardViaMailInfoInsert]
	@OrderId		INT,
	@CampaignId		INT,
	@ActionId		INT,
	@DiscountValue	DECIMAL(16,2),
	@DateToSend		DATETIME,
	@StatusId		INT
AS
BEGIN
	INSERT [campaignlek].[tGiftCardViaMailInfo] (
		OrderId,
		CampaignId,
		ActionId,
		DiscountValue,
		DateToSend,
		StatusId
	)
	VALUES (
		@OrderId,
		@CampaignId,
		@ActionId,
		@DiscountValue,
		@DateToSend,
		@StatusId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailCartActionSave]'
GO

ALTER PROCEDURE [campaignlek].[pGiftCardViaMailCartActionSave]
	@CartActionId		INT,
	@SendingInterval	INT,
	@TemplateId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailCartAction]
	SET
		[SendingInterval] = @SendingInterval,
		[TemplateId] = @TemplateId
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tGiftCardViaMailCartAction] (
			[CartActionId],
			[SendingInterval],
			[TemplateId]
		)
		VALUES (
			@CartActionId,
			@SendingInterval,
			@TemplateId
		)
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[vGiftCardViaMailInfo]'
GO

ALTER VIEW [campaignlek].[vGiftCardViaMailInfo]
AS
	SELECT 
		gcvmi.[GiftCardViaMailInfoId] AS 'GiftCardViaMailInfo.GiftCardViaMailInfoId',
		gcvmi.[OrderId] AS 'GiftCardViaMailInfo.OrderId',
		gcvmi.[CampaignId] AS 'GiftCardViaMailInfo.CampaignId',
		gcvmi.[ActionId] AS 'GiftCardViaMailInfo.ActionId',
		gcvmi.[DiscountValue] AS 'GiftCardViaMailInfo.DiscountValue',
		gcvmi.[DateToSend] AS 'GiftCardViaMailInfo.DateToSend',
		gcvmi.[StatusId] AS 'GiftCardViaMailInfo.StatusId',
		gcvmi.[VoucherInfoId] AS 'GiftCardViaMailInfo.VoucherInfoId',
		gcvmi.[VoucherCode] AS 'GiftCardViaMailInfo.VoucherCode',
		o.[ChannelId] AS 'GiftCardViaMailInfo.ChannelId'
	FROM
		[campaignlek].[tGiftCardViaMailInfo] gcvmi
		INNER JOIN [order].[tOrder] o ON o.[OrderId] = gcvmi.[OrderId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[vGiftCardViaMailCartAction]'
GO



ALTER VIEW [campaignlek].[vGiftCardViaMailCartAction]
AS
	SELECT
		gcca.[CartActionId] AS 'GiftCardViaMailCartAction.CartActionId',
		gcca.[SendingInterval] AS 'GiftCardViaMailCartAction.SendingInterval',
		gcca.[TemplateId] AS 'GiftCardViaMailCartAction.TemplateId',
		pa.*
	FROM
		[campaignlek].[tGiftCardViaMailCartAction] gcca
		INNER JOIN [campaign].[vCustomCartAction] pa ON pa.[CartAction.Id] = gcca.[CartActionId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pFilterProductGetAll]'
GO
ALTER PROCEDURE [lekmer].[pFilterProductGetAll]
	@CustomerId INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		/*Select only necessary columns (see data mapper)*/
		[FilterProduct.ProductId],
		[FilterProduct.CategoryId],
		--[FilterProduct.Title] ,
		[FilterProduct.BrandId] ,
		[FilterProduct.AgeFromMonth] ,
		[FilterProduct.AgeToMonth] ,
		[FilterProduct.IsNewFrom] ,
		--[FilterProduct.ChannelId] ,
		--[FilterProduct.CurrencyId] ,
		--[FilterProduct.PriceListRegistryId] ,
		[FilterProduct.Popularity] ,
		--[Price.PriceListId] ,
		--[Price.ProductId] ,
		[pp].[DiscountPriceIncludingVat] AS 'Price.DiscountPriceIncludingVat',
		[Price.PriceIncludingVat],
		--[Price.PriceExcludingVat] ,
		--[Price.VatPercentage]
		[FilterProduct.NumberInStock]
	FROM
		lekmer.vFilterProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[FilterProduct.ProductId]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[FilterProduct.CurrencyId],
				p.[FilterProduct.ProductId],
				p.[FilterProduct.PriceListRegistryId],
				@CustomerId
			)
		LEFT JOIN [export].[tProductPrice] pp
			ON p.[FilterProduct.ProductId] = pp.[ProductId]
			AND p.[FilterProduct.ChannelId] = pp.[ChannelId]
	WHERE
		[FilterProduct.ChannelId] = @ChannelId
	ORDER BY
		[FilterProduct.Title]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailProductActionSave]'
GO

ALTER PROCEDURE [campaignlek].[pGiftCardViaMailProductActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT,
	@SendingInterval	INT,
	@ConfigId			INT,
	@TemplateId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailProductAction]
	SET
		SendingInterval = @SendingInterval,
		ConfigId = @ConfigId,
		TemplateId = @TemplateId
	WHERE
		ProductActionId = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaignlek].[tGiftCardViaMailProductAction] (
			ProductActionId,
			SendingInterval,
			ConfigId,
			TemplateId
		)
		VALUES (
			@ProductActionId,
			@SendingInterval,
			@ConfigId,
			@TemplateId
		)
	END
	
	UPDATE 
		[campaignlek].[tCampaignActionConfigurator]
	SET 
		IncludeAllProducts = @IncludeAllProducts
	WHERE 
		[CampaignActionConfiguratorId] = @ConfigId
	
	RETURN @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[vGiftCardViaMailProductAction]'
GO

ALTER VIEW [campaignlek].[vGiftCardViaMailProductAction]
AS
	SELECT
		gcpa.[ProductActionId] AS 'GiftCardViaMailProductAction.ProductActionId',
		cac.[IncludeAllProducts] AS 'GiftCardViaMailProductAction.IncludeAllProducts',
		gcpa.[SendingInterval] AS 'GiftCardViaMailProductAction.SendingInterval',
		gcpa.[ConfigId] AS 'GiftCardViaMailProductAction.ConfigId',
		gcpa.[TemplateId] AS 'GiftCardViaMailProductAction.TemplateId',
		pa.*
	FROM
		[campaignlek].[tGiftCardViaMailProductAction] gcpa
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = gcpa.[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = gcpa.[ConfigId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailCartAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartAction] ADD CONSTRAINT [FK_tGiftCardViaMailCartAction_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD CONSTRAINT [FK_tGiftCardViaMailInfo_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD CONSTRAINT [FK_tGiftCardViaMailInfo_tGiftCardViaMailInfoStatus] FOREIGN KEY ([StatusId]) REFERENCES [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailProductAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] ADD CONSTRAINT [FK_tGiftCardViaMailProductAction_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
