INSERT	INTO [orderlek].[tOrderVoucherInfo] (
	[OrderId],
	[AmountUsed],
	[VoucherStatus]
)
SELECT
	[OrderId],
	[VoucherDiscount],
	3 -- Consumed
FROM
	[lekmer].[tLekmerOrder] lo
WHERE
	NOT EXISTS (SELECT 1 FROM [orderlek].[tOrderVoucherInfo] ovi WHERE ovi.[OrderId] = lo.[OrderId])