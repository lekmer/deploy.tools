IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
BEGIN TRANSACTION
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [orderlek].[tOrderVoucherInfo]'
GO
CREATE TABLE [orderlek].[tOrderVoucherInfo]
(
[OrderId] [int] NOT NULL,
[VoucherCode] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[DiscountType] [int] NULL,
[DiscountValue] [decimal] (16, 2) NULL,
[AmountUsed] [decimal] (16, 2) NULL,
[AmountLeft] [decimal] (16, 2) NULL,
[VoucherStatus] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tOrderVoucherInfo] on [orderlek].[tOrderVoucherInfo]'
GO
ALTER TABLE [orderlek].[tOrderVoucherInfo] ADD CONSTRAINT [PK_tOrderVoucherInfo] PRIMARY KEY CLUSTERED  ([OrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vOrderVoucherInfo]'
GO


CREATE VIEW [orderlek].[vOrderVoucherInfo]
AS
SELECT 
	[ovi].[OrderId] AS 'OrderVoucherInfo.OrderId',
	[ovi].[VoucherCode] AS 'OrderVoucherInfo.VoucherCode',
	[ovi].[DiscountType] AS 'OrderVoucherInfo.DiscountType',
	[ovi].[DiscountValue] AS 'OrderVoucherInfo.DiscountValue',
	[ovi].[AmountUsed] AS 'OrderVoucherInfo.AmountUsed',
	[ovi].[AmountLeft] AS 'OrderVoucherInfo.AmountLeft',
	[ovi].[VoucherStatus] AS 'OrderVoucherInfo.VoucherStatus'
FROM 
	[orderlek].[tOrderVoucherInfo] ovi
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderVoucherInfoSave]'
GO

CREATE PROCEDURE [orderlek].[pOrderVoucherInfoSave]
	@OrderId INT,
	@VoucherCode NVARCHAR(100),
	@DiscountType INT,
	@DiscountValue DECIMAL(16,2),
	@AmountUsed DECIMAL(16,2),
	@AmountLeft DECIMAL(16,2),
	@VoucherStatus INT
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[orderlek].[tOrderVoucherInfo]
	SET 
			[VoucherCode] = @VoucherCode,
			[DiscountType] = @DiscountType,
			[DiscountValue] = @DiscountValue,
			[AmountUsed] = @AmountUsed,
			[AmountLeft] = @AmountLeft,
			[VoucherStatus] = @VoucherStatus
	WHERE	
			[OrderId] = @OrderId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [orderlek].[tOrderVoucherInfo]
		(
			[OrderId],
			[VoucherCode],
			[DiscountType],
			[DiscountValue],
			[AmountUsed],
			[AmountLeft],
			[VoucherStatus]
		)
		VALUES
		(
			@OrderId,
			@VoucherCode,
			@DiscountType,
			@DiscountValue,
			@AmountUsed,
			@AmountLeft,
			@VoucherStatus
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderVoucherInfoDelete]'
GO

CREATE PROCEDURE [orderlek].[pOrderVoucherInfoDelete]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE
		[orderlek].[tOrderVoucherInfo]
	WHERE
		[OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderVoucherInfoGetByOrderId]'
GO
CREATE PROCEDURE [orderlek].[pOrderVoucherInfoGetByOrderId]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vOrderVoucherInfo] 
	WHERE
		[OrderVoucherInfo.OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vOrderVoucherInfoDetails]'
GO

CREATE VIEW [orderlek].[vOrderVoucherInfoDetails]
AS
SELECT 
	[ovi].[OrderId] AS 'OrderVoucherInfo.OrderId',
	[ovi].[VoucherCode] AS 'OrderVoucherInfo.VoucherCode',
	[ovi].[DiscountType] AS 'OrderVoucherInfo.DiscountType',
	
	CASE [ovi].[DiscountType]
		WHEN 0 THEN 'Unknown'
		WHEN 1 THEN 'Percentage'
		WHEN 2 THEN 'Price'
		WHEN 3 THEN 'GiftCard'
		ELSE 'Unknown'
	END AS 'OrderVoucherInfo.DiscountType.Name',
	
	[ovi].[DiscountValue] AS 'OrderVoucherInfo.DiscountValue',
	[ovi].[AmountUsed] AS 'OrderVoucherInfo.AmountUsed',
	[ovi].[AmountLeft] AS 'OrderVoucherInfo.AmountLeft',
	[ovi].[VoucherStatus] AS 'OrderVoucherInfo.VoucherStatus',
	
	CASE [ovi].[VoucherStatus]
		WHEN 0 THEN 'Unknown'
		WHEN 1 THEN 'Reserved'
		WHEN 2 THEN 'Released'
		WHEN 3 THEN 'Consumed'
		ELSE 'Unknown'
	END AS 'OrderVoucherInfo.VoucherStatus.Name'
FROM
	[orderlek].[tOrderVoucherInfo] ovi

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderVoucherInfoUpdateStatus]'
GO

CREATE PROCEDURE [orderlek].[pOrderVoucherInfoUpdateStatus]
	@OrderId INT,
	@VoucherStatus INT
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[orderlek].[tOrderVoucherInfo]
	SET 
			[VoucherStatus] = @VoucherStatus
	WHERE	
			[OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tOrderVoucherInfo]'
GO
ALTER TABLE [orderlek].[tOrderVoucherInfo] ADD CONSTRAINT [FK_tOrderVoucherInfo_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) IF EXISTS(SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
