SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [orderlek].[tCollectorTransaction]'
GO
ALTER TABLE [orderlek].[tCollectorTransaction] DROP CONSTRAINT [PK_tCollectorTransaction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [orderlek].[tQliroTransaction]'
GO
ALTER TABLE [orderlek].[tQliroTransaction] DROP CONSTRAINT [PK_tQliroTransaction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [orderlek].[tQliroTransaction]'
GO
CREATE TABLE [orderlek].[tmp_rg_xx_tQliroTransaction]
(
[TransactionId] [int] NOT NULL IDENTITY(1, 1),
[ClientRef] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Mode] [int] NULL,
[TransactionTypeId] [int] NOT NULL,
[TimeoutProcessed] [int] NULL,
[StatusCode] [int] NULL,
[Created] [datetime] NOT NULL,
[CivicNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Duration] [bigint] NULL,
[OrderId] [int] NULL,
[Amount] [decimal] (16, 2) NULL,
[CurrencyCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PaymentType] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ReservationNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[InvoiceStatus] [int] NULL,
[ReturnCodeId] [int] NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[CustomerMessage] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ResponseContent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [orderlek].[tmp_rg_xx_tQliroTransaction] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [orderlek].[tmp_rg_xx_tQliroTransaction]([TransactionId], [ClientRef], [Mode], [TransactionTypeId], [StatusCode], [Created], [CivicNumber], [Duration], [OrderId], [Amount], [CurrencyCode], [PaymentType], [ReservationNumber], [InvoiceStatus], [ReturnCodeId], [Message], [CustomerMessage], [ResponseContent]) SELECT [TransactionId], [ClientRef], [Mode], [TransactionTypeId], [StatusCode], [Created], [CivicNumber], [Duration], [OrderId], [Amount], [CurrencyCode], [PaymentType], [ReservationNumber], [InvoiceStatus], [ReturnCodeId], [Message], [CustomerMessage], [ResponseContent] FROM [orderlek].[tQliroTransaction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [orderlek].[tmp_rg_xx_tQliroTransaction] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[orderlek].[tQliroTransaction]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[orderlek].[tmp_rg_xx_tQliroTransaction]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [orderlek].[tQliroTransaction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[orderlek].[tmp_rg_xx_tQliroTransaction]', N'tQliroTransaction'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tQliroTransaction] on [orderlek].[tQliroTransaction]'
GO
ALTER TABLE [orderlek].[tQliroTransaction] ADD CONSTRAINT [PK_tQliroTransaction] PRIMARY KEY CLUSTERED  ([TransactionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [orderlek].[tCollectorTransaction]'
GO
CREATE TABLE [orderlek].[tmp_rg_xx_tCollectorTransaction]
(
[TransactionId] [int] NOT NULL IDENTITY(1, 1),
[StoreId] [int] NOT NULL,
[TransactionTypeId] [int] NOT NULL,
[TimeoutProcessed] [int] NULL,
[StatusCode] [int] NULL,
[Created] [datetime] NOT NULL,
[CivicNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Duration] [bigint] NULL,
[OrderId] [int] NULL,
[ProductCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[AvailableReservationAmount] [decimal] (16, 2) NULL,
[InvoiceNo] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[InvoiceStatus] [int] NULL,
[InvoiceUrl] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[FaultCode] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[FaultMessage] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ErrorMessage] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ResponseContent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [orderlek].[tmp_rg_xx_tCollectorTransaction] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [orderlek].[tmp_rg_xx_tCollectorTransaction]([TransactionId], [StoreId], [TransactionTypeId], [StatusCode], [Created], [CivicNumber], [Duration], [OrderId], [ProductCode], [AvailableReservationAmount], [InvoiceNo], [InvoiceStatus], [InvoiceUrl], [FaultCode], [FaultMessage], [ErrorMessage], [ResponseContent]) SELECT [TransactionId], [StoreId], [TransactionTypeId], [StatusCode], [Created], [CivicNumber], [Duration], [OrderId], [ProductCode], [AvailableReservationAmount], [InvoiceNo], [InvoiceStatus], [InvoiceUrl], [FaultCode], [FaultMessage], [ErrorMessage], [ResponseContent] FROM [orderlek].[tCollectorTransaction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [orderlek].[tmp_rg_xx_tCollectorTransaction] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[orderlek].[tCollectorTransaction]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[orderlek].[tmp_rg_xx_tCollectorTransaction]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [orderlek].[tCollectorTransaction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[orderlek].[tmp_rg_xx_tCollectorTransaction]', N'tCollectorTransaction'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCollectorTransaction] on [orderlek].[tCollectorTransaction]'
GO
ALTER TABLE [orderlek].[tCollectorTransaction] ADD CONSTRAINT [PK_tCollectorTransaction] PRIMARY KEY CLUSTERED  ([TransactionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroTimeoutOrderUpdate]'
GO
CREATE PROCEDURE [orderlek].[pQliroTimeoutOrderUpdate]
	@TransactionId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tQliroTransaction]
	SET	
		[TimeoutProcessed] = 1
	WHERE
		[TransactionId] = @TransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorTimeoutOrderUpdate]'
GO

CREATE PROCEDURE [orderlek].[pCollectorTimeoutOrderUpdate]
	@TransactionId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tCollectorTransaction]
	SET	
		[TimeoutProcessed] = 1
	WHERE
		[TransactionId] = @TransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCollectorTimeoutOrderGetAll]'
GO
CREATE PROCEDURE [orderlek].[pCollectorTimeoutOrderGetAll]
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[ct].[TransactionId] 'TimeoutOrder.TransactionId',
		[ct].[StatusCode] 'TimeoutOrder.StatusCode',
		[ct].[InvoiceStatus] 'TimeoutOrder.InvoiceStatus',
		[ct].[OrderId] 'TimeoutOrder.OrderId',
		[ct].[InvoiceNo] 'TimeoutOrder.InvoiceNo',
		[o].[ChannelId] 'TimeoutOrder.ChannelId'
	FROM
		[orderlek].[tCollectorTransaction] ct
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [ct].[OrderId]
	WHERE
		[ct].[TransactionTypeId] = 4 -- AddInvoiceTimeout
		AND
		(
			[ct].[TimeoutProcessed] IS NULL
			OR
			[ct].[TimeoutProcessed] <> 1
		)
		AND [o].[OrderStatusId] = 11 --PaymentTimeout
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[vQliroTransaction]'
GO
ALTER VIEW [orderlek].[vQliroTransaction]
AS
SELECT
	[qt].[TransactionId],
	[qt].[ClientRef],
	[qt].[Mode] AS 'ProductionStatusType',
	
	CASE [qt].[Mode]
		WHEN 0 THEN 'Production'
		WHEN 1 THEN 'Test'
		ELSE CONVERT(VARCHAR(50), [qt].[Mode])
	END AS 'ProductionStatusType.Name',
	
	[qt].[TransactionTypeId],
	
	CASE [qt].[TransactionTypeId]
		WHEN 0 THEN 'None'
		WHEN 1 THEN 'GetAddress'
		WHEN 2 THEN 'ReserveAmount'
		WHEN 3 THEN 'ReserveAmountCompany'
		WHEN 4 THEN 'CancelReservation'
		WHEN 5 THEN 'CheckOrderStatus'
		WHEN 6 THEN 'GetPaymentTypes'
		WHEN 7 THEN 'ReserveAmountTimeout'
		ELSE CONVERT(VARCHAR(50), [qt].[TransactionTypeId])
	END AS 'TransactionType.Name',
	
	[qt].[TimeoutProcessed],
	[qt].[StatusCode],
	
	CASE [qt].[StatusCode]
		WHEN 0 THEN 'Unknown'
		WHEN 1 THEN 'Ok'
		WHEN 2 THEN 'TimeoutResponse'
		WHEN 3 THEN 'UnspecifiedError'
		ELSE CONVERT(VARCHAR(50), [qt].[StatusCode])
	END AS 'StatusCode.Name',
	
	[qt].[Created],
	[qt].[CivicNumber],
	[qt].[Duration],
	[qt].[OrderId],
	[qt].[Amount],
	[qt].[CurrencyCode],
	[qt].[PaymentType],
	[qt].[ReservationNumber],
	[qt].[InvoiceStatus],
	
	CASE [qt].[InvoiceStatus]
		WHEN 0 THEN 'Invalid'
		WHEN 1 THEN 'Ok'
		WHEN 2 THEN 'Denied'
		WHEN 3 THEN 'Holding'
		WHEN 4 THEN 'Timeout'
		WHEN 5 THEN 'NoRisk'
		ELSE CONVERT(VARCHAR(50), [qt].[InvoiceStatus])
	END AS 'InvoiceStatus.Name',
	
	[qt].[ReturnCodeId],
	[qt].[Message],
	[qt].[CustomerMessage],
	[qt].[ResponseContent]
FROM
	[orderlek].[tQliroTransaction] qt



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroTimeoutOrderGetAll]'
GO
CREATE PROCEDURE [orderlek].[pQliroTimeoutOrderGetAll]
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[qt].[TransactionId] 'TimeoutOrder.TransactionId',
		[qt].[StatusCode] 'TimeoutOrder.StatusCode',
		[qt].[InvoiceStatus] 'TimeoutOrder.InvoiceStatus',
		[qt].[OrderId] 'TimeoutOrder.OrderId',
		[qt].[ReservationNumber] 'TimeoutOrder.ReservationNumber',
		[o].[ChannelId] 'TimeoutOrder.ChannelId'
	FROM
		[orderlek].[tQliroTransaction] qt
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [qt].[OrderId]
	WHERE
		[qt].[TransactionTypeId] = 7 -- ReserveAmountTimeout
		AND
		(
			[qt].[TimeoutProcessed] IS NULL
			OR
			[qt].[TimeoutProcessed] <> 1
		)
		AND [o].[OrderStatusId] = 11 --PaymentTimeout
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
