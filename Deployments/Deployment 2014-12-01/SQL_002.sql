/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 12/1/2014 3:01:21 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Add row to [order].[tOrderItemStatus]
INSERT INTO [order].[tOrderItemStatus] ([OrderItemStatusId], [Title], [CommonName]) VALUES (8, N'Payment timeout', N'PaymentTimeout')

-- Add row to [order].[tOrderStatus]
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (11, N'Payment timeout', N'PaymentTimeout')
COMMIT TRANSACTION
GO
