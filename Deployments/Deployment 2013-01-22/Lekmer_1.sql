SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [lekmer].[pCheckMediaFileUsage]'
GO
CREATE PROCEDURE [lekmer].[pCheckMediaFileUsage]
	@MediaId	INT
AS	
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [media].[tMedia] WHERE [MediaId] = @MediaId)
	BEGIN
		RETURN 1 -- NOT EXISTS
	END
	
	IF EXISTS (
		SELECT 1 FROM [media].[tMedia] m
		LEFT JOIN [product].[tProduct] p ON [p].[MediaId] = [m].[MediaId]
		LEFT JOIN [product].[tProductImage] ip ON [ip].[MediaId] = [m].[MediaId]
		LEFT JOIN [lekmer].[tBrand] b ON [b].[MediaId] = [m].[MediaId]
		LEFT JOIN [lekmer].[tIcon] i ON [i].[MediaId] = [m].[MediaId]
		WHERE 
			[m].[MediaId] = @MediaId
			AND [p].[MediaId] IS NULL 
			AND [ip].[MediaId] IS NULL
			AND [b].[MediaId] IS NULL 
			AND [i].[MediaId] IS NULL
	)
	BEGIN
		RETURN 2 -- NOT_USED
	END
	
	IF EXISTS (
		SELECT 1 FROM [media].[tMedia] m
		LEFT JOIN [product].[tProduct] p ON [p].[MediaId] = [m].[MediaId]
		LEFT JOIN [product].[tProductImage] ip ON [ip].[MediaId] = [m].[MediaId]
		LEFT JOIN [lekmer].[tBrand] b ON [b].[MediaId] = [m].[MediaId]
		LEFT JOIN [lekmer].[tIcon] i ON [i].[MediaId] = [m].[MediaId]
		WHERE 
			[m].[MediaId] = @MediaId
			AND (
					[p].[MediaId] IS NOT NULL 
					OR [ip].[MediaId] IS NOT NULL 
					OR [b].[MediaId] IS NOT NULL 
					OR [i].[MediaId] IS NOT NULL
				)
	)
	BEGIN
		RETURN 3 -- USED
	END
	
	RETURN 4 -- UNKNOWN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
