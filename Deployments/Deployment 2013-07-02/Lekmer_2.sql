IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
BEGIN TRANSACTION
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [orderlek].[vKlarnaTransaction]'
GO

ALTER VIEW [orderlek].[vKlarnaTransaction]
AS
SELECT
	[kt].[KlarnaTransactionId],
	[kt].[KlarnaShopId],
	[kt].[KlarnaMode],
	
	CASE [kt].[KlarnaMode]
		WHEN 0 THEN 'NO_FLAG'
		WHEN 2 THEN 'KRED_TEST_MODE'
		ELSE CONVERT(VARCHAR(50), [kt].[KlarnaMode])
	END AS 'KlarnaMode.Name',
	
	[kt].[KlarnaTransactionTypeId],
	
	CASE [kt].[KlarnaTransactionTypeId]
		WHEN 1 THEN 'GetAddress'
		WHEN 2 THEN 'ReserveAmount'
		WHEN 3 THEN 'ReserveAmountCompany'
		WHEN 4 THEN 'CancelReservation'
		WHEN 5 THEN 'CheckOrderStatus'
		ELSE CONVERT(VARCHAR(50), [kt].[KlarnaTransactionTypeId])
	END AS 'KlarnaTransactionType.Name',
	
	[kt].[Created],
	[kt].[CivicNumber],
	[kt].[OrderId],
	[kt].[Amount],
	[kt].[CurrencyId],
	
	CASE [kt].[CurrencyId]
		WHEN 0 THEN 'SEK'
		WHEN 1 THEN 'NOK'
		WHEN 2 THEN 'EUR'
		WHEN 3 THEN 'DKK'
		ELSE CONVERT(VARCHAR(50), [kt].[CurrencyId])
	END AS 'Currency.Code',
	
	[kt].[PClassId],
	[kt].[YearlySalary],
	[kt].[Duration],
	[kt].[ReservationNumber],
	[kt].[ResponseCodeId],
	
	CASE [kt].[ResponseCodeId]
		WHEN 1 THEN 'OK'
		WHEN 2 THEN 'NoRisk'
		WHEN 3 THEN 'TimeoutResponse'
		WHEN 4 THEN 'SpecifiedError'
		WHEN 5 THEN 'UnspecifiedError'
		WHEN 6 THEN 'Denied'
		WHEN 7 THEN 'Pending'
		ELSE CONVERT(VARCHAR(50), [kt].[ResponseCodeId])
	END AS 'ResponseCode.Name',
	
	[kt].[KlarnaFaultCode],
	[kt].[KlarnaFaultReason],
	[kt].[ResponseContent]
FROM
	[orderlek].[tKlarnaTransaction] kt

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[pKlarnaTransactionSaveResult]'
GO

ALTER PROCEDURE [orderlek].[pKlarnaTransactionSaveResult]
	@KlarnaTransactionId INT,
	@ResponseCodeId INT,
	@KlarnaFaultCode INT,
	@KlarnaFaultReason NVARCHAR(MAX),
	@Duration BIGINT,
	@ResponseContent NVARCHAR(MAX)
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tKlarnaTransaction]
	SET
		[ResponseCodeId] = @ResponseCodeId,
		[KlarnaFaultCode] = @KlarnaFaultCode,
		[KlarnaFaultReason] = @KlarnaFaultReason,
		[Duration] = @Duration,
		[ResponseContent] = @ResponseContent
	WHERE
		[KlarnaTransactionId] = @KlarnaTransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
/*
Start of RedGate SQL Source Control versioning database-level extended properties.
*/
/*
End of RedGate SQL Source Control versioning database-level extended properties.
*/
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) IF EXISTS(SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
