/*
Run this script on:

        10.150.43.52.Heppo_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 020\DB\HeppoDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 17.07.2012 9:25:13

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [productlek]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tBlockProductFilterTag]'
GO
ALTER TABLE [lekmer].[tBlockProductFilterTag] DROP
CONSTRAINT [FK_tBlockProductFilterTag_tTag]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductTag]'
GO
ALTER TABLE [lekmer].[tProductTag] DROP
CONSTRAINT [FK_tProductTag_tTag]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tTagTranslation]'
GO
ALTER TABLE [lekmer].[tTagTranslation] DROP
CONSTRAINT [FK_tTagTranslation_tTag]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tTag]'
GO
ALTER TABLE [lekmer].[tTag] DROP
CONSTRAINT [FK_tTag_tTagGroup]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tTag]'
GO
ALTER TABLE [lekmer].[tTag] DROP CONSTRAINT [PK_tTag]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tTag_TagGroupId] from [lekmer].[tTag]'
GO
DROP INDEX [IX_tTag_TagGroupId] ON [lekmer].[tTag]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tTag_Value] from [lekmer].[tTag]'
GO
DROP INDEX [IX_tTag_Value] ON [lekmer].[tTag]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tTag]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tTag]
(
[TagId] [int] NOT NULL IDENTITY(1, 1),
[TagGroupId] [int] NOT NULL,
[Value] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[CommonName] [varchar] (255) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tTag] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tTag]([TagId], [TagGroupId], [Value], [CommonName]) SELECT [TagId], [TagGroupId], [Value], [TagId] FROM [lekmer].[tTag]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tTag] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[lekmer].[tTag]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[lekmer].[tmp_rg_xx_tTag]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tTag]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tTag]', N'tTag'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tTag] on [lekmer].[tTag]'
GO
ALTER TABLE [lekmer].[tTag] ADD CONSTRAINT [PK_tTag] PRIMARY KEY CLUSTERED  ([TagId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tTag_TagGroupId] on [lekmer].[tTag]'
GO
CREATE NONCLUSTERED INDEX [IX_tTag_TagGroupId] ON [lekmer].[tTag] ([TagGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tTag_Value] on [lekmer].[tTag]'
GO
CREATE NONCLUSTERED INDEX [IX_tTag_Value] ON [lekmer].[tTag] ([Value])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCampaignFlag]'
GO
CREATE TABLE [lekmer].[tCampaignFlag]
(
[CampaignId] [int] NOT NULL,
[FlagId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignFlag] on [lekmer].[tCampaignFlag]'
GO
ALTER TABLE [lekmer].[tCampaignFlag] ADD CONSTRAINT [PK_tCampaignFlag] PRIMARY KEY CLUSTERED  ([CampaignId], [FlagId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tFlag]'
GO
CREATE TABLE [lekmer].[tFlag]
(
[FlagId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Class] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFlag] on [lekmer].[tFlag]'
GO
ALTER TABLE [lekmer].[tFlag] ADD CONSTRAINT [PK_tFlag] PRIMARY KEY CLUSTERED  ([FlagId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCampaignFolderFlag]'
GO
CREATE TABLE [lekmer].[tCampaignFolderFlag]
(
[FolderId] [int] NOT NULL,
[FlagId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignFolderFlag] on [lekmer].[tCampaignFolderFlag]'
GO
ALTER TABLE [lekmer].[tCampaignFolderFlag] ADD CONSTRAINT [PK_tCampaignFolderFlag] PRIMARY KEY CLUSTERED  ([FolderId], [FlagId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tFlagTranslation]'
GO
CREATE TABLE [lekmer].[tFlagTranslation]
(
[FlagId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFlagTranslation] on [lekmer].[tFlagTranslation]'
GO
ALTER TABLE [lekmer].[tFlagTranslation] ADD CONSTRAINT [PK_tFlagTranslation] PRIMARY KEY CLUSTERED  ([FlagId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tBlockProductSearchEsalesResult]'
GO
CREATE TABLE [productlek].[tBlockProductSearchEsalesResult]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NULL,
[RowCount] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockProductSearchEsalesResult] on [productlek].[tBlockProductSearchEsalesResult]'
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] ADD CONSTRAINT [PK_tBlockProductSearchEsalesResult] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tProductEventStatus]'
GO
CREATE TABLE [productlek].[tProductEventStatus]
(
[ProductEventStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductEventStatus] on [productlek].[tProductEventStatus]'
GO
ALTER TABLE [productlek].[tProductEventStatus] ADD CONSTRAINT [PK_tProductEventStatus] PRIMARY KEY CLUSTERED  ([ProductEventStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tProductChangeEvent]'
GO
CREATE TABLE [productlek].[tProductChangeEvent]
(
[ProductChangeEventId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[EventStatusId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ActionAppliedDate] [datetime] NULL,
[Reference] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductChangeEvent] on [productlek].[tProductChangeEvent]'
GO
ALTER TABLE [productlek].[tProductChangeEvent] ADD CONSTRAINT [PK_tProductChangeEvent] PRIMARY KEY CLUSTERED  ([ProductChangeEventId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandGetAllByBlock]'
GO
ALTER PROCEDURE [lekmer].[pBrandGetAllByBlock]
	@ChannelId		INT,
	@BlockId		INT,
	@Page			INT = NULL,
	@PageSize		INT
AS
BEGIN
	SET NOCOUNT ON
		
	DECLARE @sql		 NVARCHAR(MAX)
	DECLARE @sqlCount	 NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)
	
	DECLARE @blockIncludeAllBrands BIT
	SELECT 
		@blockIncludeAllBrands = IncludeAllBrands 
	FROM 
		lekmer.tBlockBrandList
	WHERE 
		BlockId = @BlockId
	
	IF (@blockIncludeAllBrands = 1)
		BEGIN
			SET @sqlFragment = '
			(
				SELECT 
					ROW_NUMBER() OVER (ORDER BY cast(lower([b].[Brand.Title]) AS binary) ASC) AS Number,
					[b].*
				FROM 
					[lekmer].[vBrand] b
				WHERE 
					[b].[Brand.BrandId] NOT IN (
													SELECT [prrb].[BrandId] 
													FROM [lekmer].[tProductRegistryRestrictionBrand] prrb
													INNER JOIN [product].[tProductModuleChannel] pmc 
														ON pmc.ProductRegistryId = prrb.ProductRegistryId
														   AND pmc.ChannelId = ' +  CAST(@ChannelId AS VARCHAR(10)) + '
												)
					AND [b].[ChannelId] = ' +  CAST(@ChannelId AS VARCHAR(10)) + '
			)'
		END
	ELSE
		BEGIN
			SET @sqlFragment = '
			(
				SELECT 
					ROW_NUMBER() OVER (ORDER BY [bb].[Ordinal] ASC) AS Number,
					[b].*
				FROM 
					[lekmer].[tBlockBrandListBrand] bb
					INNER JOIN [lekmer].[vBrand] b ON [bb].[BrandId] = [b].[Brand.BrandId]
				WHERE
					[b].[Brand.BrandId] NOT IN (
													SELECT [prrb].[BrandId] 
													FROM [lekmer].[tProductRegistryRestrictionBrand] prrb
													INNER JOIN [product].[tProductModuleChannel] pmc 
														ON pmc.ProductRegistryId = prrb.ProductRegistryId
														   AND pmc.ChannelId = ' +  CAST(@ChannelId AS VARCHAR(10)) + '
												)
					AND [bb].[BlockId] = '+  CAST(@BlockId AS VARCHAR(10)) +'
					AND [b].[ChannelId] = ' +  CAST(@ChannelId AS VARCHAR(10)) + '
			)'
		END

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	INT, 
			@BlockId	INT',
			@ChannelId,
			@BlockId

	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT, 
			@BlockId	INT',
			@ChannelId,
			@BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBrandSiteStructureProductRegistryWrapperGetAllByBrand]'
GO
CREATE PROCEDURE [lekmer].[pBrandSiteStructureProductRegistryWrapperGetAllByBrand]
	@BrandId	INT
AS
BEGIN
	SELECT 
		bssr.BrandId 'BrandSiteStructureProductRegistryWrapper.BrandId',
		bssr.SiteStructureRegistryId 'BrandSiteStructureProductRegistryWrapper.SiteStructureRegistryId',
		bssr.ContentNodeId 'BrandSiteStructureProductRegistryWrapper.ContentNodeId',
		pr.ProductRegistryId 'BrandSiteStructureProductRegistryWrapper.ProductRegistryId',
		pr.Title 'BrandSiteStructureProductRegistryWrapper.Title',
		cn.ContentNodeStatusId 'BrandSiteStructureProductRegistryWrapper.ContentNodeStatusId'		
	FROM 
		[lekmer].[tBrandSiteStructureRegistry] bssr
		INNER JOIN [sitestructure].[tSiteStructureModuleChannel] ssmc ON ssmc.[SiteStructureRegistryId] = bssr.[SiteStructureRegistryId]
		INNER JOIN [product].[tProductModuleChannel] pmc ON pmc.ChannelId = ssmc.[ChannelId]
		INNER JOIN [product].[tProductRegistry] pr ON pr.[ProductRegistryId] = pmc.[ProductRegistryId]
		INNER JOIN [sitestructure].[tContentNode] cn ON cn.[ContentNodeId] = bssr.[ContentNodeId]
	WHERE 
		bssr.[BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCampaignFlagSave]'
GO



CREATE PROCEDURE [lekmer].[pCampaignFlagSave]
	@CampaignId	INT,
	@FlagId		INT
AS
BEGIN
	
	IF @FlagId IS NULL
	BEGIN
		DELETE [lekmer].[tCampaignFlag]
		WHERE [CampaignId] = @CampaignId
		RETURN
	END
	
	UPDATE [lekmer].[tCampaignFlag]
	SET [FlagId] = @FlagId
	WHERE [CampaignId] = @CampaignId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tCampaignFlag]
		( 
			[CampaignId],
			[FlagId]
		)
		VALUES
		(
			@CampaignId,
			@FlagId
		)					
	END 
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCampaignFolderFlagSave]'
GO



CREATE PROCEDURE [lekmer].[pCampaignFolderFlagSave]
	@FolderId	INT,
	@FlagId		INT
AS
BEGIN
	
	IF @FlagId IS NULL
	BEGIN
		DELETE [lekmer].[tCampaignFolderFlag]
		WHERE [FolderId] = @FolderId
		RETURN
	END
	
	UPDATE [lekmer].[tCampaignFolderFlag]
	SET [FlagId] = @FlagId
	WHERE [FolderId] = @FolderId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tCampaignFolderFlag]
		( 
			[FolderId],
			[FlagId]
		)
		VALUES
		(
			@FolderId,
			@FlagId
		)					
	END 
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentNodeChangeStatus]'
GO
CREATE PROCEDURE [lekmer].[pContentNodeChangeStatus]
	@IsSetOnline	BIT,
	@ContentPageIds	VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DECLARE @StatusId INT
	
	IF @IsSetOnline = 1
		SET @StatusId = (SELECT ContentNodeStatusId FROM [sitestructure].[tContentNodeStatus] WHERE CommonName = 'Online')
	ELSE
		SET @StatusId = (SELECT ContentNodeStatusId FROM [sitestructure].[tContentNodeStatus] WHERE CommonName = 'Offline')
		
	UPDATE cn
	SET cn.[ContentNodeStatusId] = @StatusId
	FROM [sitestructure].[tContentNode] cn
	WHERE cn.ContentNodeId IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ContentPageIds, @Delimiter))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagDelete]'
GO



CREATE PROCEDURE [lekmer].[pFlagDelete]
	@FlagId		INT
AS
BEGIN
	DELETE [lekmer].[tFlag]
	WHERE [FlagId] = @FlagId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vFlagSecure]'
GO



CREATE VIEW [lekmer].[vFlagSecure]
AS
SELECT     
      FlagId AS 'Flag.Id',
      Title AS 'Flag.Title',
      Class AS 'Flag.Class', 
      Ordinal AS 'Flag.Ordinal'
FROM
      lekmer.tFlag

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagGetAllSecure]'
GO



CREATE PROCEDURE [lekmer].[pFlagGetAllSecure]
AS
BEGIN
	SET NOCOUNT ON
	SELECT *
	FROM lekmer.vFlagSecure
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vFlag]'
GO



CREATE VIEW [lekmer].[vFlag]
AS
SELECT     
      f.FlagId AS 'Flag.Id',
      COALESCE (ft.Title, f.Title) AS 'Flag.Title',
      f.Class AS 'Flag.Class', 
      f.Ordinal AS 'Flag.Ordinal',
      l.LanguageId AS 'Flag.LanguageId'
FROM
      lekmer.tFlag AS f
      CROSS JOIN core.tLanguage AS l
      LEFT JOIN lekmer.tFlagTranslation AS ft ON ft.FlagId = f.FlagId AND ft.LanguageId = l.LanguageId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagGetByCampaign]'
GO



CREATE PROCEDURE [lekmer].[pFlagGetByCampaign]
@LanguageId int,
@CampaignId int
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @FlagId int
	SET @FlagId = (SELECT FlagId FROM lekmer.tCampaignFlag WHERE CampaignId = @CampaignId)
	IF(@FlagId IS NULL)
	BEGIN
		SET @FlagId = (SELECT cff.FlagId FROM lekmer.tCampaignFolderFlag cff INNER JOIN campaign.tCampaign c ON cff.FolderId = c.FolderId WHERE c.CampaignId = @CampaignId)
	END
	SELECT *
	FROM lekmer.vFlag
	WHERE [Flag.Id] = @FlagId and [Flag.LanguageId] = @LanguageId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagGetByCampaignFolderSecure]'
GO



CREATE PROCEDURE [lekmer].[pFlagGetByCampaignFolderSecure]
@FolderId int
AS
BEGIN
	SET NOCOUNT ON
	SELECT f.*
	FROM lekmer.vFlagSecure f inner join lekmer.tCampaignFolderFlag cff on f.[Flag.Id] = cff.FlagId
	WHERE cff.FolderId = @FolderId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagGetByCampaignSecure]'
GO



CREATE PROCEDURE [lekmer].[pFlagGetByCampaignSecure]
@CampaignId int
AS
BEGIN
	SET NOCOUNT ON
	SELECT f.*
	FROM lekmer.vFlagSecure f inner join lekmer.tCampaignFlag cf on f.[Flag.Id] = cf.FlagId
	WHERE cf.CampaignId = @CampaignId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagGetByIdSecure]'
GO



CREATE PROCEDURE [lekmer].[pFlagGetByIdSecure]
@FlagId int
AS
BEGIN
	SET NOCOUNT ON
	SELECT *
	FROM lekmer.vFlagSecure 
	WHERE [Flag.Id] = @FlagId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagSave]'
GO



CREATE PROCEDURE [lekmer].[pFlagSave]
	@FlagId		INT,
	@Title		NVARCHAR(50),
	@Class		VARCHAR(50),
	@Ordinal	INT
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM lekmer.tFlag WHERE Title = @Title AND FlagId <> @FlagId)
		RETURN -1

	UPDATE
		[lekmer].[tFlag]
	SET
		[Title] = @Title,	
		[Class] = @Class,
		[Ordinal] = @Ordinal
	WHERE
		[FlagId] = @FlagId
		
	IF @@ROWCOUNT = 0
	BEGIN
		SET @Ordinal = (SELECT MAX(Ordinal) FROM [lekmer].[tFlag]) + 1
		IF @Ordinal is null
			BEGIN
				SET @Ordinal = 1
			END
		INSERT INTO [lekmer].[tFlag]
		( 
			[Title],
			[Class],
			[Ordinal]
		)
		VALUES
		(
			@Title,
			@Class ,
			@Ordinal
		)
		SET @FlagId = scope_identity()						
	END 
	RETURN @FlagId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagSetOrdinal]'
GO



CREATE PROCEDURE [lekmer].[pFlagSetOrdinal]
	@FlagIdList		varchar(max),
	@Separator			char(1)
AS
BEGIN
	UPDATE
		[lekmer].[tFlag]
	SET
		Ordinal = t.Ordinal
	FROM
		[lekmer].[tFlag] f
		INNER JOIN generic.fnConvertIDListToTableWithOrdinal(@FlagIdList, @Separator) t ON f.FlagId = t.Id
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagTranslationGetAllByFlag]'
GO



CREATE PROCEDURE [lekmer].[pFlagTranslationGetAllByFlag]
	@FlagId int
AS
BEGIN
	SET NOCOUNT ON
	SELECT
	    FlagId AS 'Id',
		LanguageId AS 'LanguageId',
		Title AS 'Value'
	FROM
	    lekmer.tFlagTranslation 
	WHERE 
		FlagId = @FlagId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFlagTranslationSave]'
GO



CREATE PROCEDURE [lekmer].[pFlagTranslationSave]
@FlagId		int,
@LanguageId	int,
@Title		nvarchar(50)
AS
BEGIN
	UPDATE
		lekmer.tFlagTranslation
	SET
		[Title] = @Title
	WHERE
		[FlagId] = @FlagId AND
		[LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO lekmer.tFlagTranslation
		(
			[FlagId],
			[LanguageId],
			[Title]				
		)
		VALUES
		(
			@FlagId,
			@LanguageId,
			@Title
		)
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeDecreaseNumberInStock]'
GO
ALTER PROCEDURE [lekmer].[pProductSizeDecreaseNumberInStock]
	@ProductId INT,
	@SizeId INT,
	@Quantity INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		lekmer.tProductSize
	SET
		NumberInStock = (CASE WHEN NumberInStock - @Quantity < 0 THEN 0 ELSE NumberInStock - @Quantity END)
	WHERE
		ProductId = @ProductId
		AND SizeId = @SizeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeGetAll]'
GO
ALTER PROCEDURE [lekmer].[pProductSizeGetAll]
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		ProductId,
		SizeId
	FROM
		[lekmer].[tProductSize]
	WHERE
		NumberInStock > 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeSave]'
GO

ALTER PROCEDURE [lekmer].[pProductSizeSave]
	@ProductId				INT,
	@SizeId					INT,
	@Millimeter				INT,
	@MillimeterDeviation	INT,
	@OverrideEU				DECIMAL(3, 1)
AS 
BEGIN 
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tProductSize]
	SET
		MillimeterDeviation = @MillimeterDeviation,
		OverrideEU			= @OverrideEU
	WHERE 
		ProductId = @ProductId AND
		SizeId = @SizeId
		
	UPDATE
		[lekmer].[tSize]
	SET
		Millimeter = @Millimeter
	WHERE 
		SizeId = @SizeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeDeviationGetAll]'
GO
ALTER PROCEDURE [lekmer].[pSizeDeviationGetAll]
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vSizeDeviation]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeGetAll]'
GO
ALTER PROCEDURE [lekmer].[pSizeGetAll]
AS 
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		*
	FROM 
		[lekmer].[vSize]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vTag]'
GO
ALTER VIEW [lekmer].[vTag]
AS
SELECT     
      t.TagId AS 'Tag.TagId', 
      t.TagGroupId AS 'Tag.TagGroupId', 
      COALESCE (tt.Value, t.Value) AS 'Tag.Value',
      t.CommonName AS 'Tag.CommonName',
      l.LanguageId AS 'Tag.LanguageId'
FROM
      [lekmer].[tTag] AS t
      CROSS JOIN core.tLanguage AS l
      LEFT JOIN [lekmer].tTagTranslation AS tt ON tt.TagId = t.TagId AND tt.LanguageId = l.LanguageId
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vTagSecure]'
GO
ALTER VIEW [lekmer].[vTagSecure]
AS
SELECT     
      t.TagId AS 'Tag.TagId', 
      t.TagGroupId AS 'Tag.TagGroupId', 
      t.Value AS 'Tag.Value',
      t.CommonName AS 'Tag.CommonName'
FROM
      [lekmer].[tTag] AS t
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pTagSave]'
GO
ALTER PROCEDURE [lekmer].[pTagSave]
	@TagId		INT,
	@TagGroupId INT,
	@Value		NVARCHAR(255),
	@CommonName VARCHAR(255)
AS
BEGIN
	IF EXISTS ( SELECT 1 
				FROM lekmer.tTag 
				WHERE 
					(Value = @Value AND TagGroupId = @TagGroupId AND TagId <> @TagId)
					OR (CommonName = @CommonName AND TagGroupId = @TagGroupId AND TagId <> @TagId)
			   )
		RETURN -1
	 
	UPDATE
		[lekmer].[tTag]
	SET
		[Value] = @Value,
		[CommonName] = @CommonName
	WHERE
		[TagId] = @TagId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tTag]
		( 
			[TagGroupId],
			[Value],
			[CommonName]
		)
		VALUES
		(
			@TagGroupId,
			@Value,
			@CommonName
		)
		
		SET @TagId = SCOPE_IDENTITY()						
	END 
	RETURN @TagId
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchEsalesResultDelete]'
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultDelete]
	@BlockId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE
		[productlek].[tBlockProductSearchEsalesResult]
	WHERE
		[BlockId] = @BlockId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vBlockProductSearchEsalesResult]'
GO

CREATE VIEW [productlek].[vBlockProductSearchEsalesResult]
AS
	SELECT
		[BlockId] 'BlockProductSearchEsalesResult.BlockId' ,
		[ColumnCount] 'BlockProductSearchEsalesResult.ColumnCount' ,
		[RowCount] 'BlockProductSearchEsalesResult.RowCount'
	FROM
		[productlek].[tBlockProductSearchEsalesResult]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchEsalesResultSave]'
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultSave]
	@BlockId INT ,
	@ColumnCount INT ,
	@RowCount INT
AS 
BEGIN

	SET NOCOUNT ON

	UPDATE
		[productlek].[tBlockProductSearchEsalesResult]
	SET	
		[ColumnCount] = @ColumnCount ,
		[RowCount] = @RowCount
	WHERE
		[BlockId] = @BlockId
	
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[productlek].[tBlockProductSearchEsalesResult]
			( [BlockId] ,
			  [ColumnCount] ,
			  [RowCount]
			)
		VALUES
			( @BlockId ,
			  @ColumnCount ,
			  @RowCount
			)
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeEventDeleteExpiredItems]'
GO

CREATE PROCEDURE [productlek].[pProductChangeEventDeleteExpiredItems]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ExpirationDate DATETIME
	SET @ExpirationDate = DATEADD(DAY, -2, GETDATE())

	DELETE 
		pce
	FROM
		[productlek].[tProductChangeEvent] pce
	WHERE 
		pce.[EventStatusId] = 2 --ActionApplied
		AND
		pce.[ActionAppliedDate] < @ExpirationDate
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vProductChangeEvent]'
GO

CREATE VIEW [productlek].[vProductChangeEvent]
AS
SELECT     
	[pce].[ProductChangeEventId] AS 'ProductChangeEvent.ProductChangeEventId' ,
	[pce].[ProductId] AS 'ProductChangeEvent.ProductId' ,
	[pce].[EventStatusId] AS 'ProductChangeEvent.EventStatusId' ,
	[pce].[CreatedDate] AS 'ProductChangeEvent.CreatedDate' ,
	[pce].[ActionAppliedDate] AS 'ProductChangeEvent.ActionAppliedDate' ,
	[pce].[Reference] AS 'ProductChangeEvent.Reference'
FROM
	[productlek].[tProductChangeEvent] pce
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeEventGetAllInQueue]'
GO
CREATE PROCEDURE [productlek].[pProductChangeEventGetAllInQueue]
	@NumberOfItems INT,
	@EventStatusId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT TOP (@NumberOfItems)
		[pce].*
	FROM
		[productlek].[vProductChangeEvent] pce
	WHERE
		[pce].[ProductChangeEvent.EventStatusId] = @EventStatusId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeEventInsert]'
GO

CREATE PROCEDURE [productlek].[pProductChangeEventInsert]
	@ProductId INT,
	@EventStatusId INT,
	@CreatedDate DATETIME,
	@ActionAppliedDate DATETIME = NULL,
	@Reference NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON

	INSERT [productlek].[tProductChangeEvent]
	(
		[ProductId],
		[EventStatusId],
		[CreatedDate],
		[ActionAppliedDate],
		[Reference]
	)
	VALUES
	(
		@ProductId,
		@EventStatusId,
		@CreatedDate,
		@ActionAppliedDate,
		@Reference
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeEventSetStatusByIdList]'
GO

CREATE PROCEDURE [productlek].[pProductChangeEventSetStatusByIdList]
	@ProductChangeEventIds VARCHAR(MAX) ,
	@Delimiter CHAR(1) ,
	@EventStatusId INT ,
	@ActionAppliedDate DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@ProductChangeEventIds, @Delimiter)

	IF ( @ActionAppliedDate IS NULL ) 
	BEGIN
		UPDATE
			pce
		SET	
			[EventStatusId] = @EventStatusId
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[EventStatusId] != @EventStatusId
	END
	ELSE
	BEGIN
		UPDATE
			pce
		SET	
			[EventStatusId] = @EventStatusId,
			[ActionAppliedDate] = @ActionAppliedDate
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[EventStatusId] != @EventStatusId
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pTrackProductChangesHeppo]'
GO

CREATE PROCEDURE [integration].[pTrackProductChangesHeppo]
AS 
BEGIN
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tProductChanges') IS NOT NULL 
		DROP TABLE #tProductChanges
	
	CREATE TABLE #tProductChanges (
		[ProductId] INT NOT NULL
		CONSTRAINT PK_#tProductChanges PRIMARY KEY ( [ProductId] )
	)

	INSERT	#tProductChanges ( [ProductId] )
			SELECT DISTINCT
				[lp].[ProductId]
			FROM
				[import].[tProduct] p
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = [p].[HYErpId]
		UNION
			SELECT DISTINCT
				[lp].[ProductId]
			FROM
				[import].[tProductPrice] pp
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([pp].[HYErpIdSize], 0, 12)
	
	IF (SELECT COUNT(*) FROM [#tProductChanges]) < 10000
	BEGIN
		INSERT [productlek].[tProductChangeEvent] ( [ProductId], [EventStatusId], [CreatedDate], [ActionAppliedDate], [Reference] )
		SELECT [ProductId], 0, GETDATE(), NULL, 'HY import' FROM [#tProductChanges]
	END
	
	DROP TABLE #tProductChanges
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetIdAllWithoutAnyFilter]'
GO


CREATE PROCEDURE [lekmer].[pProductGetIdAllWithoutAnyFilter]
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	DECLARE @sqlCountFilter NVARCHAR(MAX)

	SELECT COUNT(*)
		FROM
			product.tProduct AS p
		WHERE
			P.IsDeleted = 0

	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY p.[ProductId]) AS Number,
			p.[ProductId]
		FROM
			product.tProduct AS p
		WHERE
			P.IsDeleted = 0
	) As Result
	WHERE Number > (@Page - 1) * @PageSize
		AND Number <= @Page * @PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductSizeGetAllByProduct]
	@ProductId	INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vProductSize]
	WHERE
		[ProductSize.ProductId] = @ProductId
	ORDER BY
		[Size.EU]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductSizeGetAllByProductIdList]'
GO
CREATE PROCEDURE [lekmer].[pProductSizeGetAllByProductIdList]
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		ps.*
	FROM 
		[lekmer].[vProductSize] ps
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl 
			ON pl.Id = ps.[ProductSize.ProductId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeGetById]'
GO
ALTER PROCEDURE [lekmer].[pProductSizeGetById]
	@ProductId		INT,
	@SizeId			INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		lekmer.vProductSize
	WHERE
		[ProductSize.ProductId] = @ProductId AND
		[ProductSize.SizeId] = @SizeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pTagGetAllIdsByCategory]'
GO
CREATE PROCEDURE [lekmer].[pTagGetAllIdsByCategory]
	@CategoryId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		DISTINCT [pt].[TagId]
	FROM
		[product].[tProduct] p
		INNER JOIN [lekmer].[tProductTag] pt ON [p].[ProductId] = [pt].[ProductId]
	WHERE
		[p].[CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [import].[pImportProduct]'
GO

ALTER PROCEDURE [import].[pImportProduct]

AS
BEGIN
	SET NOCOUNT ON	
	
	EXEC [import].[pImportCategory]
	EXEC [import].[pImportBrand]
	EXEC [integration].[usp_UpdateBrandHeppoCreationDate] 
	EXEC [import].[pUpdateProduct]
	EXEC [import].[pUpdateProductNumberInStock]
	
	DECLARE 
		@NeedToInsertProductRelationData BIT,
	
		@HYErpId NVARCHAR(50),
		@HYErpIdSize NVARCHAR(50),
		@Channel INT,
		@NumberInStock INT,
		@ArticleTitle NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@SizeId NVARCHAR(25), -- for taging socks
		@Price DECIMAL,
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@VarugruppTitel NVARCHAR(50),
		@VarukodTitel NVARCHAR(50),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL = 25.0,
		
		@PrevHYErpId NVARCHAR(50),
		@PrevChannel INT,
		
		@HYArticleClassId NVARCHAR(50),
		@CategoryErpId NVARCHAR(50),
		@CategoryId INT,
		@BrandId INT,
		@NewFromDate DATETIME,
		@NewToDate DATETIME,
		@TagGroupId_CategoryLevel3 INT = 1000008,
		@TagId_1 INT,
		@TagId_2 INT,
		@TradeDoublerProductGroup NVARCHAR(50),
		@ChannelId INT,
		@IsProductRegistry BIT,
		@PriceListId INT,
		@ProductRegistryId INT
		
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 1, @NewFromDate)
	
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT 
			tp.HYErpId,
			tp.HYErpIdSize,
			tp.ChannelId,
			tp.ArticleTitle,
			tp.SizeId,
			tp.Price/100,
			tp.NumberInStock,
			tp.ArticleGroupId,
			tp.ArticleGroupTitle,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.ArticleCodeTitle,
			tp.BrandId
		FROM
			[import].tProduct tp


	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO 
			@HYErpId,
			@HYErpIdSize,
			@Channel,			 
			@ArticleTitle,
			@SizeId,
			@Price,
			@NumberInStock,
			@VarugruppId,
			@VarugruppTitel,
			@VaruklassId,
			@VarukodId,
			@VarukodTitel,
			@HYBrandId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			SET @IsProductRegistry = 0
			
			SET @ChannelId =  (select ChannelId from lekmer.tLekmerChannel where ErpId = @Channel)			
										 
			SET @ProductRegistryId = (Select ProductRegistryId from product.tProductModuleChannel where ChannelId = @ChannelId)
			
			SET @PriceListId = (Select PriceListId 
										from product.tProductModuleChannel m 
										inner join product.tPriceList t
										on m.PriceListRegistryId = t.PriceListRegistryId
										where ChannelId = @ChannelId)


		
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYErpId)
			IF @ProductId IS NOT NULL
			BEGIN
				IF EXISTS (SELECT 1 FROM product.tProductRegistryProduct prp
						   WHERE prp.ProductId = @ProductId
						   AND prp.ProductRegistryId = @ProductRegistryId)
				BEGIN
					SET @IsProductRegistry = 1
					GOTO PRODUCT_END
				END
			END
			
			PRODUCT_START:
			
			-- Skip processing the same @HYErpId + @Channel few times
			IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			BEGIN
				GOTO PRODUCT_END
			END ELSE BEGIN
				SET	@PrevHYErpId = @HYErpId
				SET	@PrevChannel = @Channel
			END
			
			BEGIN TRANSACTION
			
			SET @NeedToInsertProductRelationData = 0
			
			SET @HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '')
			SET @CategoryErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, '')

			SET @CategoryId = (SELECT CategoryId FROM product.tCategory WHERE ErpId = @CategoryErpId)
			
			SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)
							
			IF @ProductId IS NULL 
			BEGIN
				SET @NeedToInsertProductRelationData = 1
			
				-- Data that was sent into insert
				SET @Data = 'NEW: HYErpId ' + @HYErpId + ' @Fok ' + CAST(@Channel AS VARCHAR(10)) + 'HY-Cat ' + @CategoryErpId 
				
				-- tProduct
				INSERT INTO product.tProduct (
					ErpId,  
					IsDeleted, 
					NumberInStock, 
					CategoryId, 
					Title, 
					[Description], 
					ProductStatusId
				)
				VALUES (
					@HYErpId,
					0, --IsDeleted
					@NumberInStock, 
					@CategoryId,
					@ArticleTitle, 
					'', --Description, 
					1 --ProductStatusId = 'Ready to translate'
				)
											
				SET @ProductId = SCOPE_IDENTITY()
											
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId, 
					HYErpId, 
					BrandId, 
					IsBookable, 
					AgeFromMonth, 
					AgeToMonth, 
					IsNewFrom, 
					IsNewTo,
					IsBatteryIncluded, 
					ExpectedBackInStock,
					ShowVariantRelations
				)
				VALUES (
					@ProductId, 
					@HYErpId,
					@BrandId,	
					0, --IsBookable, 
					0, --AgeFromMonth, 
					1, --AgeToMonth, 
					@NewFromDate,
					@NewToDate, 
					0, --IsBatteryIncluded, 
					NULL, --ExpectedBackInStock
					1
				)
				
				------------------------------------------------------------------------------------------
				-- tProductNews
				-- Flag so that the product is and has been the news
				
				IF (@ProductId NOT IN (SELECT productId FROM integration.[tProductNews]))
				BEGIN
					INSERT INTO integration.tProductNews (
						ProductId,
						ErpId
					)
					VALUES (
						@ProductId,
						@HYErpId
					)
				END
					
				------------------------------------------------------------------------------------------
				-- tProductTag
				-- tag with categoryLevel3 (sneaker boots, etc.)

				SET @TagId_1 = NULL

				SELECT @TagId_1 = TagId
				FROM lekmer.tTag
				WHERE Value = @VarukodTitel
					  AND TagGroupId = @TagGroupId_CategoryLevel3

				IF @TagId_1 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1
								   FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND Tagid = @TagId_1)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_1
					END
				END

				------------------------------------------------------------------------------------------
				-- tProductTag
				-- Tag man/kvinna/pojke/flocka/unisex

				SET @TagId_1 = NULL
				SET @TagId_2 = NULL

				IF @VarugruppTitel IN ('Kvinna', 'Man', 'Flicka', 'Pojke')
				BEGIN
					SELECT @TagId_1 = TagId
					FROM lekmer.tTag
					WHERE Value = @VarugruppTitel
						  AND TagGroupId = 1000002
				END
				ELSE IF @VarugruppTitel = ('Unisex barn')
				BEGIN
					SET @TagId_1 = 1000013 -- Pojkskor
					SET @TagId_2 = 1000012 -- Flickskor
				END
				ELSE IF @VarugruppTitel = ('Unisex kvinna/man')
				BEGIN
					SET @TagId_1 = 1000009 -- Man
					SET @TagId_2 = 1000010 -- Kvinna
				END

				IF @TagId_1 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND TagId = @TagId_1)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_1
					END
				END

				IF @TagId_2 IS NOT NULL
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag
								   WHERE ProductId = @ProductId
										 AND TagId = @TagId_2)
					BEGIN
						EXEC lekmer.pProductTagSave @ProductId, @TagId_2
					END
				END
				------------------------------------------------------------------------------------------
			END -- Product already exist in tLekmerProduct
			ELSE IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem prp WHERE prp.ProductId = @ProductId AND prp.PriceListId = @PriceListId)
			BEGIN
				SET @NeedToInsertProductRelationData = 1

				SET @Data = 'EXISTING: @HYErpId ' + @HYErpId + ' @Fok ' + CAST(@Channel AS VARCHAR(10)) + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
			END

			IF @NeedToInsertProductRelationData = 1
			BEGIN
				------------------------------------------------------------------------------------------
				-- tProductRegistryProduct				
				
				IF @IsProductRegistry = 0
				BEGIN
					INSERT INTO product.tProductRegistryProduct (
						ProductId, 
						ProductRegistryId
					)
					VALUES (
						@ProductId,
						@ProductRegistryId
					)
				END

				------------------------------------------------------------------------------------------
				-- tPriceListItem				
				
				IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem
							   WHERE ProductId = @ProductId
							   AND PriceListId = @PriceListId)
				BEGIN
					INSERT INTO product.tPriceListItem (
						PriceListId,
						ProductId,
						PriceIncludingVat,
						PriceExcludingVat,
						VatPercentage
					)
					VALUES (
						@PriceListId,
						@ProductId,
						@Price,
						@Price / (1.0+@Vat/100.0),
						@Vat
					)
				END

				------------------------------------------------------------------------------------------ 
				-- tTradeDoublerProductGroupMapping

				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = @HYArticleClassId
													   AND ChannelId = @Channel)

				

				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM  lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId
										 AND tTDPGM.ChannelId = @ChannelId)
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							@ChannelId
						)
					END
				END 
			END

			COMMIT
			
			PRODUCT_END:
			
			--ProductSize
			
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [import].[pImportProductSize]
				@HYErpId,
				@HYErpIdSize,
				@Channel, -- Channel id value from HY
				@SizeId,
				@NumberInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@ProductId
			
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK

			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
			
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)
			
		END CATCH

		FETCH_NEXT: --LABEL
		FETCH NEXT FROM cur_product INTO
			 @HYErpId,
			 @HYErpIdSize,
			 @Channel,			 
			 @ArticleTitle,
			 @SizeId,
			 @Price,
			 @NumberInStock,
			 @VarugruppId,
			 @VarugruppTitel,
			 @VaruklassId,
			 @VarukodId,
			 @VarukodTitel,
			 @HYBrandId
	END

	CLOSE cur_product
	DEALLOCATE cur_product


	EXEC [import].[pUpdateProductBrand]
	EXEC [import].[pAddProductColorTags]
	-- temp lösning	
	--EXEC [integration].[usp_REATagProduct] done as schedule task

	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]

	-- Track products chnages to eSales export
	EXEC [integration].[pTrackProductChangesHeppo]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pLekmerOrderGetLatest]'
GO

ALTER PROCEDURE [lekmer].[pLekmerOrderGetLatest]
	@Date DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.CreatedDate] >= @Date
		AND [Order.CreatedDate] < DATEADD(day, 1, @Date)
		AND [Lekmer.FeedbackToken] IS NULL
		AND [OrderStatus.CommonName] = 'OrderInHY'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchEsalesResultGetById]'
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultGetById]
	@LanguageId INT ,
	@BlockId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		ba.* ,
		b.*
	FROM
		[productlek].[vBlockProductSearchEsalesResult] AS ba
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON ba.[BlockProductSearchEsalesResult.BlockId] = b.[Block.BlockId]
	WHERE
		ba.[BlockProductSearchEsalesResult.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchEsalesResultGetByIdSecure]'
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		ba.* ,
		b.*
	FROM
		[productlek].[vBlockProductSearchEsalesResult] AS ba
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON ba.[BlockProductSearchEsalesResult.BlockId] = b.[Block.BlockId]
	WHERE
		ba.[BlockProductSearchEsalesResult.BlockId] = @BlockId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vProductViewWithoutStatusFilter]'
GO

CREATE VIEW [lekmer].[vProductViewWithoutStatusFilter]
AS
SELECT
	P.ProductId 'Product.Id', 
	P.ItemsInPackage 'Product.ItemsInPackage',
	P.ErpId 'Product.ErpId',
	P.EanCode 'Product.EanCode',
	P.NumberInStock 'Product.NumberInStock', 
	P.CategoryId 'Product.CategoryId',
	COALESCE(PT.WebShopTitle, P.WebShopTitle) 'Product.WebShopTitle',
	COALESCE(PT.Title, P.Title) 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
	COALESCE(PT.Description, P.Description) 'Product.Description',
	COALESCE(PT.ShortDescription, P.ShortDescription) 'Product.ShortDescription',
	Ch.ChannelId 'Product.ChannelId', 
    Ch.CurrencyId 'Product.CurrencyId',
    PMC.PriceListRegistryId 'Product.PriceListRegistryId',
	I.*,
	PSSR.ParentContentNodeId 'Product.ParentContentNodeId',
	PSSR.TemplateContentNodeId 'Product.TemplateContentNodeId'
FROM
	product.tProduct AS P

	/* filetrring */
	INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
	INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
	INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId

	/* extra info */
	LEFT JOIN sitestructure.tSiteStructureModuleChannel AS SSMC ON SSMC.ChannelId = Ch.ChannelId
	LEFT JOIN product.tProductSiteStructureRegistry AS PSSR
		ON P.ProductId = PSSR.ProductId
		AND SSMC.SiteStructureRegistryId = PSSR.SiteStructureRegistryId
	LEFT JOIN media.vCustomImage AS I
		ON I.[Image.MediaId] = P.MediaId
		AND I.[Image.LanguageId] = Ch.LanguageId
	LEFT JOIN product.tProductTranslation AS PT
		ON P.ProductId = PT.ProductId
		AND Ch.LanguageId = PT.LanguageId
WHERE
	P.IsDeleted = 0
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCustomProductViewWithoutStatusFilter]'
GO

CREATE VIEW [lekmer].[vCustomProductViewWithoutStatusFilter]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle'
	FROM
		[lekmer].[vProductViewWithoutStatusFilter] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetViewAllByIdListWithoutStatusFilter]'
GO
CREATE PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutStatusFilter]
	@ChannelId		INT,
	@CustomerId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[p].*,
		[pli].*
	FROM
		[lekmer].[vCustomProductViewWithoutStatusFilter] p
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl
			ON pl.Id = p.[Product.Id]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCampaignFlag]'
GO
ALTER TABLE [lekmer].[tCampaignFlag] ADD
CONSTRAINT [FK_tCampaignFlag_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCampaignFlag_tFlag] FOREIGN KEY ([FlagId]) REFERENCES [lekmer].[tFlag] ([FlagId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCampaignFolderFlag]'
GO
ALTER TABLE [lekmer].[tCampaignFolderFlag] ADD
CONSTRAINT [FK_tCampaignFolderFlag_tCampaignFolder] FOREIGN KEY ([FolderId]) REFERENCES [campaign].[tCampaignFolder] ([FolderId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCampaignFolderFlag_tFlag] FOREIGN KEY ([FlagId]) REFERENCES [lekmer].[tFlag] ([FlagId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tFlagTranslation]'
GO
ALTER TABLE [lekmer].[tFlagTranslation] ADD
CONSTRAINT [FK_tFlagTranslation_tFlag] FOREIGN KEY ([FlagId]) REFERENCES [lekmer].[tFlag] ([FlagId]) ON DELETE CASCADE,
CONSTRAINT [FK_tFlagTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockProductFilterTag]'
GO
ALTER TABLE [lekmer].[tBlockProductFilterTag] ADD
CONSTRAINT [FK_tBlockProductFilterTag_tTag] FOREIGN KEY ([TagId]) REFERENCES [lekmer].[tTag] ([TagId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductTag]'
GO
ALTER TABLE [lekmer].[tProductTag] ADD
CONSTRAINT [FK_tProductTag_tTag] FOREIGN KEY ([TagId]) REFERENCES [lekmer].[tTag] ([TagId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tTagTranslation]'
GO
ALTER TABLE [lekmer].[tTagTranslation] ADD
CONSTRAINT [FK_tTagTranslation_tTag] FOREIGN KEY ([TagId]) REFERENCES [lekmer].[tTag] ([TagId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tTag]'
GO
ALTER TABLE [lekmer].[tTag] ADD
CONSTRAINT [FK_tTag_tTagGroup] FOREIGN KEY ([TagGroupId]) REFERENCES [lekmer].[tTagGroup] ([TagGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tBlockProductSearchEsalesResult]'
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] ADD
CONSTRAINT [FK_tBlockProductSearchEsalesResult_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tProductChangeEvent]'
GO
ALTER TABLE [productlek].[tProductChangeEvent] ADD
CONSTRAINT [FK_tProductChangeEvent_tProductEventStatus] FOREIGN KEY ([EventStatusId]) REFERENCES [productlek].[tProductEventStatus] ([ProductEventStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
