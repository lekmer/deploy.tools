declare @ProductFolderId int,
		@NewProductFolderId int,
		@TestProductFolderId int,
		@BrandFolderId int,
		@IconFolderId int,
		@BannerFolderId int,
		@ProductsImagesId int,
		@IconImagesId int,
		@BannerImagesId int,
		@BrandImagesId int;

set @ProductFolderId =( select top 1 MediaFolderId FROM [media].[tMediaFolder] where title = 'Products')
set @NewProductFolderId =( select top 1 MediaFolderId FROM [media].[tMediaFolder] where title = 'New Products')
set @TestProductFolderId =( select top 1 MediaFolderId FROM [media].[tMediaFolder] where title = 'Test Products')
set @IconFolderId =( select top 1 MediaFolderId FROM [media].[tMediaFolder] where title = 'Icons')
set @BrandFolderId =( select top 1 MediaFolderId FROM [media].[tMediaFolder] where title = 'Brands')
set @BannerFolderId =( select top 1 MediaFolderId FROM [media].[tMediaFolder] where title = 'Banners')

if(@ProductFolderId is not null)
begin
	exec [media].[pMediaFolderSave] null, @ProductFolderId, 'Images'
	exec [media].[pMediaFolderSave] null, @ProductFolderId, 'Pdf'
	exec [media].[pMediaFolderSave] null, @ProductFolderId, 'Movies'
	
	set @ProductsImagesId = (select top 1 MediaFolderId from [media].[tMediaFolder] where (MediaFolderParentId = @ProductFolderId)and(Title = 'Images') )
	
	update [media].[tMedia] set MediaFolderId =  @ProductsImagesId where MediaFolderId = @ProductFolderId
	
	update [media].[tMedia] set MediaFolderId =  @ProductsImagesId where MediaFolderId in
	(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId = @ProductFolderId)
	
	delete [media].[tMediaFolder] where MediaFolderParentId = @ProductFolderId and ((Title != 'Images')and(Title  != 'Pdf')and(Title != 'Movies'))
	
	if (@TestProductFolderId is not null)
	begin
		update [media].[tMedia] set MediaFolderId =  @ProductsImagesId where MediaFolderId = @TestProductFolderId
		
		update [media].[tMedia] set MediaFolderId =  @ProductsImagesId where MediaFolderId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId = @TestProductFolderId)
		
		update [media].[tMedia] set MediaFolderId =  @ProductsImagesId where MediaFolderId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId = @TestProductFolderId))
		
		delete [media].[tMediaFolder]  where MediaFolderId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId = @TestProductFolderId))
		
		delete [media].[tMediaFolder] where MediaFolderId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId = @TestProductFolderId)
		
		delete [media].[tMediaFolder] where MediaFolderId = @TestProductFolderId
	end
	if (@NewProductFolderId is not null)
	begin
		update [media].[tMedia] set MediaFolderId =  @ProductsImagesId where MediaFolderId = @NewProductFolderId
		
		update [media].[tMedia] set MediaFolderId =  @ProductsImagesId where MediaFolderId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId = @NewProductFolderId)
		
		update [media].[tMedia] set MediaFolderId =  @ProductsImagesId where MediaFolderId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId = @NewProductFolderId))
		
		delete [media].[tMediaFolder]  where MediaFolderId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId = @NewProductFolderId))
		
		delete [media].[tMediaFolder] where MediaFolderId in
		(select MediaFolderId from [media].[tMediaFolder] where MediaFolderParentId = @NewProductFolderId)
		
		delete [media].[tMediaFolder] where MediaFolderId = @NewProductFolderId
	end
end

if(@BannerFolderId is not null)
begin
	exec [media].[pMediaFolderSave] null, @BannerFolderId, 'Images'
	exec [media].[pMediaFolderSave] null, @BannerFolderId, 'Movies'
	
	set @BannerImagesId = (select top 1 MediaFolderId from [media].[tMediaFolder] where (MediaFolderParentId = @BannerFolderId)and(Title = 'Images') )
	
	update [media].[tMedia] set MediaFolderId =  @BannerImagesId where MediaFolderId = @BannerFolderId
		
end

if(@IconFolderId is not null)
begin
	exec [media].[pMediaFolderSave] null, @IconFolderId, 'Images'
	
	set @IconImagesId = (select top 1 MediaFolderId from [media].[tMediaFolder] where (MediaFolderParentId = @IconFolderId)and(Title = 'Images') )
	
	update [media].[tMedia] set MediaFolderId =  @IconImagesId where MediaFolderId = @IconFolderId
		
end

if(@BrandFolderId is not null)
begin
	exec [media].[pMediaFolderSave] null, @BrandFolderId, 'Images'
	
	set @BrandImagesId = (select top 1 MediaFolderId from [media].[tMediaFolder] where (MediaFolderParentId = @BrandFolderId)and(Title = 'Images') )
	
	update [media].[tMedia] set MediaFolderId =  @BrandImagesId where MediaFolderId = @IconFolderId
		
end


