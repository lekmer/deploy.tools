SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [campaignlek].[pFixedPriceActionIncludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaignlek].[pFixedPriceActionIncludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pFixedDiscountActionExcludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaignlek].[pFixedDiscountActionExcludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pFixedPriceActionExcludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaignlek].[pFixedPriceActionExcludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pCartItemDiscountCartActionExcludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pCampaignActionIncludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaignlek].[pCampaignActionIncludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pCartItemFixedDiscountActionExcludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pFixedDiscountActionIncludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaignlek].[pFixedDiscountActionIncludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pCartItemDiscountCartActionIncludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pProductDiscountActionItemGetAllSecure]'
GO
DROP PROCEDURE [lekmer].[pProductDiscountActionItemGetAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pCampaignActionExcludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaignlek].[pCampaignActionExcludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD
[DiapersDeliveryMethodId] [int] NULL,
[DiapersFreightCost] [decimal] (16, 2) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartDoesNotContainConditionDelete]'
GO
ALTER PROCEDURE [campaignlek].[pCartDoesNotContainConditionDelete]
	@ConditionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaignlek].[tCartDoesNotContainCondition] WHERE [ConditionId] = @ConditionId)
	
	DELETE FROM [campaignlek].[tCartDoesNotContainCondition]
	WHERE [ConditionId] = @ConditionId
		
	EXEC [campaignlek].[pCampaignActionConfiguratorDeleteAll] @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[aip].[ProductId]
	FROM
		[lekmer].[tCartItemFixedDiscountActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE
		[aip].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrder]'
GO


ALTER VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		lo.[PaymentCost] AS [Lekmer.PaymentCost],
		lo.[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey],
		lo.[FeedbackToken] AS [Lekmer.FeedbackToken],
		lo.[AlternateAddressId] AS [Lekmer.AlternateAddressId],
		[lo].[NeedSendInsuranceInfo] AS [Lekmer.NeedSendInsuranceInfo],
		[lo].[CivicNumber] AS [Lekmer.CivicNumber],
		[lo].[OptionalDeliveryMethodId] AS [Lekmer.OptionalDeliveryMethodId],
		[lo].[OptionalFreightCost] AS [Lekmer.OptionalFreightCost],
		[lo].[DiapersDeliveryMethodId] AS [Lekmer.DiapersDeliveryMethodId],
		[lo].[DiapersFreightCost] AS [Lekmer.DiapersFreightCost]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [generic].[fStringToStringTableWithOrdinal]'
GO
CREATE FUNCTION [generic].[fStringToStringTableWithOrdinal](
	@String NVARCHAR(MAX),
	@Delimiter CHAR(1) = ','
)
RETURNS @tblSepString TABLE(SepString NVARCHAR(50), Ordinal INT)
AS
BEGIN
	IF @String IS NULL OR @String = ''
		RETURN

	IF SUBSTRING(@String, LEN(@String), 1) <> @Delimiter
		SELECT @String = @String + @Delimiter

	DECLARE @ind1 INT, @ind2 INT, @counter INT
	SELECT @ind1 = 1
	
	SET @counter = 1;

	WHILE 1 = 1
	BEGIN
		SELECT @ind2 = CHARINDEX(@Delimiter, @String, @ind1)

		IF @ind2 = 0
			BREAK

		INSERT
			@tblSepString
		VALUES
		(
			SUBSTRING(@String, @ind1, @ind2 - @ind1),
			@counter
		)
		SET @counter = @counter + 1
		SET @ind1 = @ind2 + 1
	END

	RETURN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pReviewUpdate]'
GO
CREATE PROCEDURE [review].[pReviewUpdate]
	@FeedbackIds	VARCHAR(MAX),
	@AuthorNames	NVARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	-- Feedback ids.
	DECLARE @tFeedback TABLE (FeedbackId INT, Ordinal INT)
	INSERT INTO @tFeedback ([FeedbackId], [Ordinal])
	SELECT [Id], [Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@FeedbackIds, @Delimiter)

	-- Author names.
	DECLARE @tAuthorNames TABLE (AuthorName NVARCHAR(50), Ordinal INT)
	INSERT INTO @tAuthorNames ([AuthorName], [Ordinal])
	SELECT [SepString], [Ordinal] FROM [generic].[fStringToStringTableWithOrdinal](@AuthorNames, @Delimiter)

	UPDATE r
	SET [r].[AuthorName] = [a].[AuthorName]
	FROM [review].[tReview] r
	INNER JOIN @tFeedback f ON [f].[FeedbackId] = [r].[RatingReviewFeedbackId]
	INNER JOIN @tAuthorNames a ON [a].[Ordinal] = [f].[Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[aep].[ProductId]
	FROM
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE
		[aep].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAllRecursive]
	@CartActionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories]
	FROM [lekmer].[tCartItemFixedDiscountActionExcludeCategory]
	WHERE [CartActionId] = @CartActionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartItemPriceActionIncludeProductGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartItemPriceActionIncludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[aip].[ProductId]
	FROM 
		[addon].[tCartItemPriceActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE 
		[aip].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedDiscountActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedDiscountActionIncludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[CategoryId]
	FROM 
		[campaignlek].[tFixedDiscountActionIncludeCategory]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedPriceActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedPriceActionIncludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[CategoryId]
	FROM 
		[campaignlek].[tFixedPriceActionIncludeCategory]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[vCartItemsGroupValueCondition]'
GO
ALTER VIEW [campaignlek].[vCartItemsGroupValueCondition]
AS
	SELECT
		[cigvc].[ConditionId] AS 'CartItemsGroupValueCondition.ConditionId',
		[cigvc].[ConfigId] AS 'CartItemsGroupValueCondition.ConfigId',
		[cc].*
	FROM
		[campaignlek].[tCartItemsGroupValueCondition] cigvc
		INNER JOIN [campaign].[vCustomCondition] cc ON [cc].[Condition.Id] = [cigvc].[ConditionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedPriceActionIncludeBrandGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedPriceActionIncludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tFixedPriceActionIncludeBrand]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailProductActionSave]'
GO
ALTER PROCEDURE [campaignlek].[pGiftCardViaMailProductActionSave]
	@ProductActionId	INT,
	@SendingInterval	INT,
	@TemplateId			INT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailProductAction]
	SET
		[SendingInterval] = @SendingInterval,
		[ConfigId] = @ConfigId,
		[TemplateId] = @TemplateId
	WHERE
		[ProductActionId] = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tGiftCardViaMailProductAction] (
			[ProductActionId],
			[SendingInterval],
			[ConfigId],
			[TemplateId]
		)
		VALUES (
			@ProductActionId,
			@SendingInterval,
			@ConfigId,
			@TemplateId
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemFixedDiscountActionExcludeBrandGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[lekmer].[tCartItemFixedDiscountActionExcludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAllRecursive]
	@CartActionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories]
	FROM [lekmer].[tCartItemFixedDiscountActionIncludeCategory]
	WHERE [CartActionId] = @CartActionId

	DECLARE @CategoryId INT, @Sub BIT
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pOrderSave]'
GO
ALTER PROCEDURE [lekmer].[pOrderSave]
	@OrderId					INT,
	@PaymentCost				DECIMAL(16,2),
	@CustomerIdentificationKey	NCHAR(50),
	@AlternateAddressId			INT = NULL,
	@CivicNumber				NVARCHAR(50) = NULL,
	@OptionalDeliveryMethodId	INT,
	@OptionalFreightCost		DECIMAL(16, 2),
	@DiapersDeliveryMethodId	INT,
	@DiapersFreightCost			DECIMAL(16, 2)
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		[PaymentCost] = @PaymentCost,
		[CustomerIdentificationKey] = @CustomerIdentificationKey,
		[AlternateAddressId] = @AlternateAddressId,
		[CivicNumber] = @CivicNumber,
		[OptionalDeliveryMethodId] = @OptionalDeliveryMethodId,
		[OptionalFreightCost] = @OptionalFreightCost,
		[DiapersDeliveryMethodId] = @DiapersDeliveryMethodId,
		[DiapersFreightCost] = @DiapersFreightCost
	WHERE 
		[OrderId] = @OrderId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tLekmerOrder]
		(
			[OrderId],
			[PaymentCost],
			[CustomerIdentificationKey],
			[AlternateAddressId],
			[CivicNumber],
			[OptionalDeliveryMethodId],
			[OptionalFreightCost],
			[DiapersDeliveryMethodId],
			[DiapersFreightCost]
		)
		VALUES
		(
			@OrderId,
			@PaymentCost,
			@CustomerIdentificationKey,
			@AlternateAddressId,
			@CivicNumber,
			@OptionalDeliveryMethodId,
			@OptionalFreightCost,
			@DiapersDeliveryMethodId,
			@DiapersFreightCost
		)
	END
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAllRecursive]
	@CartActionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories]
	FROM [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory]
	WHERE [CartActionId] = @CartActionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartContainsConditionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartContainsConditionExcludeCategoryGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT 
		[cec].[CategoryId],
		[cec].[IncludeSubcategories]
	FROM 
		[addon].[tCartContainsConditionExcludeCategory] cec
	WHERE 
		[cec].[ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedPriceActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedPriceActionExcludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[CategoryId]
	FROM 
		[campaignlek].[tFixedPriceActionExcludeCategory]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemDiscountCartActionExcludeBrandGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tCartItemDiscountCartActionExcludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartItemPriceActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartItemPriceActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[c].[CategoryId]
	FROM 
		[addon].[tCartItemPriceActionIncludeCategory] c
	WHERE 
		[c].[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedDiscountActionIncludeProductGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedDiscountActionIncludeProductGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT
		[aip].[ProductId]
	FROM
		[campaignlek].[tFixedDiscountActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE
		[aip].[ProductActionId] = @ProductActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionExcludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryGetIdAllRecursive]
	@ConfigId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryIteration (CategoryId, IncludeSubCategories)
	SELECT [CategoryId], [IncludeSubCategories]
	FROM [campaignlek].[tCampaignActionExcludeCategory]
	WHERE [ConfigId] = @ConfigId

	DECLARE @tCategoryResult TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryResult (CategoryId, IncludeSubCategories)
	SELECT CategoryId, IncludeSubCategories FROM @tCategoryIteration
	
	DECLARE @CategoryId INT, @IncludeSubCategories BIT

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 CategoryId FROM @tCategoryIteration)
		SET @IncludeSubCategories = (SELECT TOP 1 IncludeSubCategories FROM @tCategoryIteration)
		
		IF (@IncludeSubCategories = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult ([CategoryId], [IncludeSubCategories])
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE [CategoryId] = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pOrder]'
GO

ALTER PROCEDURE [integration].[pOrder]
	@OrderId int
AS
begin
	set nocount on
	set transaction isolation level read uncommitted
	begin try
		
		--set statistics io on
		--set statistics time on
		
		-- Order
		select 		
			o.OrderId,
            o.FreightCost,
            lo.OptionalFreightCost,
            lo.DiapersFreightCost,
            o.ChannelId as Channel,   
            op.OrderPaymentId, 
            pt.ErpId as PaymentMethod, 
            op.ReferenceId, 
            op.Price,
            --u.VoucherDiscount as VoucherAmount
            a1.ItemsActualPriceIncludingVat - op.Price as VoucherAmount,
			lci.IsCompany, -- NEW
			op.Captured as PaymentCaptured
		from
			[order].[tPaymentType] pt
			inner join [order].[tOrderPayment] op on op.PaymentTypeId = pt.PaymentTypeId
			inner join [order].[tOrder] o on op.OrderId = o.OrderId
			inner join [customerlek].[tCustomerInformation] lci on lci.CustomerId = o.CustomerId
			left join [lekmer].[tLekmerOrder] lo on lo.OrderId = o.OrderId
			cross apply (
				select sum(oi.ActualPriceIncludingVat * oi.Quantity) as 'ItemsActualPriceIncludingVat'
				from [order].[tOrderItem] oi
				where oi.OrderId = o.OrderId
			) as a1
			where
				o.OrderStatusId = 2
				and
				o.OrderId = @OrderId
		
		-- OrderRow // Old
		--select
		--	o.OrderItemId as OrderRowId, 
		--	p.Title, 
		--	o.Quantity, 
		--	p.ProductId,
		--	o.ActualPriceIncludingVat as Price, 
		--	o.OriginalPriceIncludingVat as OrdinaryPrice, 
		--	coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		--from
		--	[order].tOrderItem o
		--	left join [order].tOrderItemProduct p 
		--		on o.OrderItemId = p.OrderItemId
		--	left join lekmer.tOrderItemSize ois
		--		on ois.OrderItemId = o.OrderItemId
		--where
		--		o.OrderId = @OrderId 
				
		-- OrderRow
		select
			o.OrderItemId as OrderRowId, 
			p.Title, 
			o.Quantity, 
			p.ProductId,
			o.ActualPriceIncludingVat as Price, 
			o.OriginalPriceIncludingVat as OrdinaryPrice, 
			coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
			inner join lekmer.tLekmerProduct l
				on p.ProductId = l.ProductId
		where
			o.OrderId = @OrderId 
			and l.ProductTypeId != 2 -- package article
		UNION ALL
		select
			poi.OrderItemId as OrderRowId,
			poip.Title,
			poi.Quantity,
			poip.ProductId,
			poi.PackagePriceIncludingVat as Price,
			poi.OriginalPriceIncludingVat as OrdinaryPrice,
			coalesce(poip.[SizeErpId], poip.ErpId) as HyErpId
		from
			orderlek.tPackageOrderItem poi
			left join orderlek.tPackageOrderItemProduct poip
				on poi.PackageOrderItemId = poip.PackageOrderItemId
		where
			poi.OrderId = @OrderId
			
		-- Customer
		select 		
            c.CustomerId, 
			c.FirstName, 
			c.LastName,
            c.CivicNumber, 
            c.PhoneNumber,
            c.CellPhoneNumber as MobilePhoneNumber, 
            o.Email,
			case g.IsCompany when 1 then '05' else isnull(gt.ErpId, '00') end as Gender --NEW
		from	
			[order].torder o
			inner join [customer].tCustomerInformation c
				on c.CustomerId = o.CustomerId
			left join customerlek.tCustomerInformation g
				on c.CustomerId = g.CustomerId
			left join customerlek.tGenderType gt
				on g.GenderTypeId = gt.GenderTypeId
		where
			o.OrderId = @OrderId
		
		-- BillingAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.BillingAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- DeliveryAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.DeliveryAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- AlternativeAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].tOrder o
			inner join [lekmer].tLekmerOrder lo on lo.OrderId = o.OrderId
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = lo.AlternateAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId


		-- DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join [order].tDeliveryMethod d
				on o.DeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId
			
		-- Optional DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join lekmer.tLekmerOrder lo
				on lo.OrderId = o.OrderId
			inner join [order].tDeliveryMethod d
				on lo.OptionalDeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId
			
		-- Diapers DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join lekmer.tLekmerOrder lo
				on lo.OrderId = o.OrderId
			inner join [order].tDeliveryMethod d
				on lo.DiapersDeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId

	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionExcludeBrandGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionExcludeBrandGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tCampaignActionExcludeBrand]
	WHERE 
		[ConfigId] = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionIncludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartContainsConditionIncludeBrandGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartContainsConditionIncludeBrandGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT
		[cib].[BrandId]
	FROM
		[addon].[tCartContainsConditionIncludeBrand] cib
	WHERE
		[cib].[ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedDiscountActionExcludeProductGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedDiscountActionExcludeProductGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tFixedDiscountActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [aep].[ProductId] = [p].[ProductId]
	WHERE
		[aep].[ProductActionId] = @ProductActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedDiscountActionIncludeBrandGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedDiscountActionIncludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tFixedDiscountActionIncludeBrand]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedDiscountActionExcludeBrandGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedDiscountActionExcludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tFixedDiscountActionExcludeBrand]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemFixedDiscountActionIncludeBrandGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[lekmer].[tCartItemFixedDiscountActionIncludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[lekmer].[tCartItemFixedDiscountActionIncludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemDiscountCartActionExcludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeCategoryGetIdAllRecursive]
	@CartActionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories]
	FROM [campaignlek].[tCartItemDiscountCartActionExcludeCategory]
	WHERE [CartActionId] = @CartActionId

	DECLARE @CategoryId INT, @Sub BIT
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionIncludeProductGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionIncludeProductGetIdAll]
	@ConfigId	INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tCampaignActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [aip].[ProductId] = [p].[ProductId]
	WHERE
		[aip].[ConfigId] = @ConfigId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[vGiftCardViaMailProductAction]'
GO

ALTER VIEW [campaignlek].[vGiftCardViaMailProductAction]
AS
	SELECT
		gcpa.[ProductActionId] AS 'GiftCardViaMailProductAction.ProductActionId',
		gcpa.[SendingInterval] AS 'GiftCardViaMailProductAction.SendingInterval',
		gcpa.[TemplateId] AS 'GiftCardViaMailProductAction.TemplateId',
		gcpa.[ConfigId] AS 'GiftCardViaMailProductAction.ConfigId',
		pa.*
	FROM
		[campaignlek].[tGiftCardViaMailProductAction] gcpa
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = gcpa.[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = gcpa.[ConfigId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartDoesNotContainConditionGetById]'
GO
ALTER PROCEDURE [campaignlek].[pCartDoesNotContainConditionGetById]
	@ConditionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartDoesNotContainCondition]
	WHERE
		[CartDoesNotContainCondition.ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedPriceActionExcludeProductGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedPriceActionExcludeProductGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT
		[aep].[ProductId]
	FROM
		[campaignlek].[tFixedPriceActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE
		[aep].[ProductActionId] = @ProductActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartContainsConditionExcludeBrandGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartContainsConditionExcludeBrandGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT 
		[ceb].[BrandId]
	FROM 
		[addon].[tCartContainsConditionExcludeBrand] ceb 
	WHERE 
		[ceb].[ConditionId] = @ConditionId 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionIncludeBrandGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionIncludeBrandGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tCampaignActionIncludeBrand]
	WHERE 
		[ConfigId] = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedPriceActionExcludeBrandGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedPriceActionExcludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tFixedPriceActionExcludeBrand]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pPercentagePriceDiscountActionSave]'
GO
ALTER PROCEDURE [campaign].[pPercentagePriceDiscountActionSave]
	@ProductActionId	INT,
	@DiscountAmount		DECIMAL(16, 2),
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaign].[tPercentagePriceDiscountAction]
	SET
		DiscountAmount = @DiscountAmount,
		ConfigId = @ConfigId
	WHERE
		ProductActionId = @ProductActionId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaign].[tPercentagePriceDiscountAction] (
			ProductActionId,
			DiscountAmount,
			ConfigId
		)
		VALUES (
			@ProductActionId,
			@DiscountAmount,
			@ConfigId
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemFixedDiscountActionExcludeProductGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[aep].[ProductId]
	FROM
		[lekmer].[tCartItemFixedDiscountActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE
		[aep].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemDiscountCartActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[campaignlek].[tCartItemDiscountCartActionIncludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemDiscountCartActionIncludeProductGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tCartItemDiscountCartActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE
		[aip].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemDiscountCartActionIncludeBrandGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tCartItemDiscountCartActionIncludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAllRecursive]
	@CartActionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories]
	FROM [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]
	WHERE [CartActionId] = @CartActionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemDiscountCartActionIncludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeCategoryGetIdAllRecursive]
	@CartActionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories]
	FROM [campaignlek].[tCartItemDiscountCartActionIncludeCategory]
	WHERE [CartActionId] = @CartActionId

	DECLARE @CategoryId INT, @Sub BIT
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemDiscountCartActionExcludeProductGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tCartItemDiscountCartActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE
		[aep].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[aip].[ProductId]
	FROM
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE
		[aip].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[vReviewRecord]'
GO

ALTER VIEW [review].[vReviewRecord]
AS
	SELECT
		r.*,
		rrf.*,
		(CASE WHEN p.[WebShopTitle] IS NULL THEN p.[Title] ELSE p.[WebShopTitle] END) 'ReviewRecord.Product.Title',
		[p].[ErpId] 'ReviewRecord.Product.ErpId',
		c.[UserName] 'ReviewRecord.Customer.UserName'
	FROM 
		[review].[vReview] r
		INNER JOIN [review].[vRatingReviewFeedback] rrf ON rrf.[RatingReviewFeedback.RatingReviewFeedbackId] = r.[Review.RatingReviewFeedbackId]
		INNER JOIN [review].[tRatingReviewUser] rru ON rru.[RatingReviewUserId] = rrf.[RatingReviewFeedback.RatingReviewUserId]
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = rrf.[RatingReviewFeedback.ProductId]
		LEFT JOIN [customer].[tCustomerUser] c ON c.[CustomerId] = rru.[CustomerId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedDiscountActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedDiscountActionExcludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[CategoryId]
	FROM 
		[campaignlek].[tFixedDiscountActionExcludeCategory]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartContainsConditionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartContainsConditionIncludeCategoryGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT 
		[cic].[CategoryId],
		[cic].[IncludeSubcategories]
	FROM 
		[addon].[tCartContainsConditionIncludeCategory] cic
	WHERE 
		[cic].[ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemsGroupValueConditionSave]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemsGroupValueConditionSave]
	@ConditionId		INT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tCartItemsGroupValueCondition]
	SET
		ConfigId = @ConfigId
	WHERE
		[ConditionId] = @ConditionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tCartItemsGroupValueCondition] (
			ConditionId,
			ConfigId
		)
		VALUES (
			@ConditionId,
			@ConfigId
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[vPercentagePriceDiscountAction]'
GO
ALTER VIEW [campaign].[vPercentagePriceDiscountAction]
AS
	SELECT
		[ppda].[ProductActionId] AS 'PercentagePriceDiscountAction.ProductActionId',
		[ppda].[DiscountAmount] AS 'PercentagePriceDiscountAction.DiscountAmount',
		[ppda].[ConfigId] AS 'PercentagePriceDiscountAction.ConfigId',
		[pa].*
	FROM
		[campaign].[tPercentagePriceDiscountAction] ppda
		INNER JOIN [campaign].[vCustomProductAction] pa on [pa].[ProductAction.Id] = [ppda].[ProductActionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCartItemDiscountCartActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[campaignlek].[tCartItemDiscountCartActionExcludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartItemPriceActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartItemPriceActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[c].[CategoryId]
	FROM 
		[addon].[tCartItemPriceActionExcludeCategory] c
	WHERE 
		[c].[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartItemPriceActionExcludeProductGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartItemPriceActionExcludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[aep].[ProductId]
	FROM 
		[addon].[tCartItemPriceActionExcludeProduct] aep
		INNER JOIN  [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE 
		[aep].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionExcludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[lekmer].[tCartItemFixedDiscountActionExcludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemPercentageDiscountActionIncludeBrandGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionIncludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		[aic].[CategoryId],
		[aic].[IncludeSubCategories]
	FROM 
		[campaignlek].[tCampaignActionIncludeCategory] aic
	WHERE 
		[aic].[ConfigId] = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemPercentageDiscountActionExcludeBrandGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionExcludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAllRecursive]
	@CartActionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories] 
	FROM [lekmer].[tCartItemPercentageDiscountActionIncludeCategory]
	WHERE [CartActionId] = @CartActionId

	DECLARE @CategoryId INT, @Sub BIT
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[aip].[ProductId]
	FROM
		[lekmer].[tCartItemPercentageDiscountActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE
		[aip].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pFixedPriceActionIncludeProductGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pFixedPriceActionIncludeProductGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT
		[aip].[ProductId]
	FROM
		[campaignlek].[tFixedPriceActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE
		[aip].[ProductActionId] = @ProductActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		[aec].[CategoryId],
		[aec].[IncludeSubCategories]
	FROM 
		[campaignlek].[tCampaignActionExcludeCategory] aec
	WHERE 
		[aec].[ConfigId] = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartContainsConditionExcludeProductGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartContainsConditionExcludeProductGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT 
		P.ProductId
	FROM 
		[addon].[tCartContainsConditionExcludeProduct] cep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [cep].[ProductId]
	WHERE 
		[cep].[ConditionId] = @ConditionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartContainsConditionExcludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [addon].[pCartContainsConditionExcludeCategoryGetIdAllRecursive]
	@ConditionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories]
	FROM [addon].[tCartContainsConditionExcludeCategory]
	WHERE [ConditionId] = @ConditionId

	DECLARE @CategoryId INT, @Sub BIT
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT count(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionIncludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryGetIdAllRecursive]
	@ConfigId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryIteration (CategoryId, IncludeSubCategories)
	SELECT [CategoryId], [IncludeSubCategories]
	FROM [campaignlek].[tCampaignActionIncludeCategory]
	WHERE [ConfigId] = @ConfigId

	DECLARE @tCategoryResult TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryResult (CategoryId, IncludeSubCategories)
	SELECT CategoryId, IncludeSubCategories FROM @tCategoryIteration
	
	DECLARE @CategoryId INT, @IncludeSubCategories BIT

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 CategoryId FROM @tCategoryIteration)
		SET @IncludeSubCategories = (SELECT TOP 1 IncludeSubCategories FROM @tCategoryIteration)
		
		IF (@IncludeSubCategories = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult ([CategoryId], [IncludeSubCategories])
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE [CategoryId] = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[aep].[ProductId]
	FROM
		[lekmer].[tCartItemPercentageDiscountActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE
		[aep].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartContainsConditionIncludeProductGetIdAll]'
GO
ALTER PROCEDURE [addon].[pCartContainsConditionIncludeProductGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT 
		[cip].[ProductId]
	FROM 
		[addon].[tCartContainsConditionIncludeProduct] cip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [cip].[ProductId]
	WHERE 
		[cip].[ConditionId] = @ConditionId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAllRecursive]
	@CartActionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories]
	FROM [lekmer].[tCartItemPercentageDiscountActionExcludeCategory]
	WHERE [CartActionId] = @CartActionId

	DECLARE @CategoryId INT, @Sub BIT
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionExcludeProductGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionExcludeProductGetIdAll]
	@ConfigId	INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tCampaignActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [aep].[ProductId] = [p].[ProductId]
	WHERE
		[aep].[ConfigId] = @ConfigId
		AND [p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pCartContainsConditionIncludeCategoryGetIdAllRecursive]'
GO
ALTER PROCEDURE [addon].[pCartContainsConditionIncludeCategoryGetIdAllRecursive]
	@ConditionId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT [CategoryId], [IncludeSubcategories]
	FROM [addon].[tCartContainsConditionIncludeCategory]
	WHERE [ConditionId] = @ConditionId

	DECLARE @CategoryId INT, @Sub BIT
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c 
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDiscountActionItemGetAll]'
GO
ALTER PROCEDURE [lekmer].[pProductDiscountActionItemGetAll]
	@ActionId	INT,
	@SortBy		VARCHAR(20) = NULL
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'SELECT
					[pdai].*
				FROM 
					[lekmer].[vProductDiscountActionItem] pdai
					INNER JOIN [product].[tProduct] p ON [pdai].[ProductDiscountActionItem.ProductId] = [p].[ProductId]
				WHERE
					[p].[IsDeleted] = 0
					AND [pdai].[ProductDiscountActionItem.ActionId] = ' + CAST(@ActionId AS VARCHAR(10))

	IF @SortBy IS NOT NULL 
    BEGIN
        SET @sql = @sql + ' ORDER BY ' + @SortBy
    END

	EXEC SP_EXECUTESQL @sql
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
