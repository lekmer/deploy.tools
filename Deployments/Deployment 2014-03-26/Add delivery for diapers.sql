/*
   [order].[tDeliveryMethod]
[orderlek].[tDeliveryMethod]
   [order].[tDeliveryMethodPrice]
   [order].[tPaymentTypeDeliveryMethod]
   [order].[tChannelDeliveryMethod]
*/

BEGIN TRANSACTION

SET IDENTITY_INSERT [order].[tDeliveryMethod] ON

INSERT INTO [order].[tDeliveryMethod](
	[DeliveryMethodId],
	[CommonName],
	[Title],
	[ErpId],
	[TrackingUrl]
)
VALUES
(
	1000005, -- DeliveryMethodId - int
	'DoorstepDiapers', -- CommonName - varchar(50)
	N'Doorstep diapers', -- Title - nvarchar(50)
	'07', -- ErpId - varchar(50)
	NULL  -- TrackingUrl - varchar(1000)
)

SET IDENTITY_INSERT [order].[tDeliveryMethod] OFF


INSERT INTO [orderlek].[tDeliveryMethod](
	[DeliveryMethodId],
	[Priority],
	[IsCompany],
	[DeliveryMethodTypeId]
)
VALUES
(
	1000005, -- DeliveryMethodId - int
	60, -- Priority - int
	0, -- IsCompany - bit
	2  -- DeliveryMethodTypeId - int
)


INSERT INTO [order].[tDeliveryMethodPrice](
	[CountryId],
	[CurrencyId],
	[DeliveryMethodId],
	[FreightCost]
)
VALUES
( -- SE
	1, -- CountryId - int
	1, -- CurrencyId - int
	1000005, -- DeliveryMethodId - int
	29.00  -- FreightCost - decimal
),
( -- DK
	1000002, -- CountryId - int
	1000002, -- CurrencyId - int
	1000005, -- DeliveryMethodId - int
	29.00  -- FreightCost - decimal
),
( -- FI
	1000003, -- CountryId - int
	1000003, -- CurrencyId - int
	1000005, -- DeliveryMethodId - int
	2.90  -- FreightCost - decimal
)


INSERT INTO [order].[tPaymentTypeDeliveryMethod](
	[PaymentTypeId],
	[DeliveryMethodId]
)
VALUES
(
	1, -- PaymentTypeId - int
	1000005  -- DeliveryMethodId - int
),
(
	1000002, -- PaymentTypeId - int
	1000005  -- DeliveryMethodId - int
),
(
	1000003, -- PaymentTypeId - int
	1000005  -- DeliveryMethodId - int
),
(
	1000007, -- PaymentTypeId - int
	1000005  -- DeliveryMethodId - int
)


INSERT INTO [order].[tChannelDeliveryMethod](
	[ChannelId],
	[DeliveryMethodId]
)
VALUES
( -- SE
	1, -- ChannelId - int
	1000005  -- DeliveryMethodId - int
),
( -- DK
	3, -- ChannelId - int
	1000005  -- DeliveryMethodId - int
),
( -- FI
	4, -- ChannelId - int
	1000005  -- DeliveryMethodId - int
)

ROLLBACK TRANSACTION