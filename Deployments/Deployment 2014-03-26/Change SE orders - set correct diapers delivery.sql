/*
SE - there was only diapers delivery option, no optional home delivery
*/

BEGIN TRANSACTION

UPDATE lo
SET
	lo.[DiapersDeliveryMethodId] = lo.[OptionalDeliveryMethodId],
	lo.[DiapersFreightCost] = lo.[OptionalFreightCost],
	lo.[OptionalDeliveryMethodId] = NULL,
	lo.[OptionalFreightCost] = NULL
FROM
	[order].[tOrder] o
	INNER JOIN [lekmer].[tLekmerOrder] lo ON lo.[OrderId] = o.[OrderId]
WHERE
	o.[ChannelId] = 1
	AND
	lo.[OptionalDeliveryMethodId] IS NOT NULL
	
ROLLBACK