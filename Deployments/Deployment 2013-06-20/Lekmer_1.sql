IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
BEGIN TRANSACTION
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [orderlek].[pKlarnaTransactionCreateCheckOrderStatus]'
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionCreateCheckOrderStatus]
	@KlarnaShopId INT,
	@KlarnaMode INT,
	@KlarnaTransactionTypeId INT,
	@CreatedDate DATETIME,
	@OrderId INT,
	@ReservationNumber VARCHAR(50)
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tKlarnaTransaction]
			( [KlarnaShopId],
			  [KlarnaMode],
			  [KlarnaTransactionTypeId],
			  [Created],
			  [OrderId],
			  [ReservationNumber]
			)
	VALUES
			( @KlarnaShopId,
			  @KlarnaMode,
			  @KlarnaTransactionTypeId,
			  @CreatedDate,
			  @OrderId,
			  @ReservationNumber
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tKlarnaPendingOrder]'
GO
CREATE TABLE [orderlek].[tKlarnaPendingOrder]
(
[KlarnaPendingOrderId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[OrderId] [int] NOT NULL,
[FirstAttempt] [datetime] NOT NULL,
[LastAttempt] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tKlarnaPendingOrder] on [orderlek].[tKlarnaPendingOrder]'
GO
ALTER TABLE [orderlek].[tKlarnaPendingOrder] ADD CONSTRAINT [PK_tKlarnaPendingOrder] PRIMARY KEY CLUSTERED  ([KlarnaPendingOrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vKlarnaPendingOrder]'
GO

CREATE VIEW [orderlek].[vKlarnaPendingOrder]
AS
SELECT
	[po].[KlarnaPendingOrderId] AS 'KlarnaPendingOrder.KlarnaPendingOrderId',
	[po].[ChannelId] AS 'KlarnaPendingOrder.ChannelId',
	[po].[OrderId] AS 'KlarnaPendingOrder.OrderId',
	[po].[FirstAttempt] AS 'KlarnaPendingOrder.FirstAttempt',
	[po].[LastAttempt] AS 'KlarnaPendingOrder.LastAttempt'
FROM
	[orderlek].[tKlarnaPendingOrder] po

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pKlarnaPendingOrderInsert]'
GO

CREATE PROCEDURE [orderlek].[pKlarnaPendingOrderInsert]
	@ChannelId INT,
	@OrderId INT,
	@FirstAttempt DATETIME,
	@LastAttempt DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tKlarnaPendingOrder]
			( [ChannelId],
			  [OrderId],
			  [FirstAttempt],
			  [LastAttempt]
			)
	VALUES
			( @ChannelId,
			  @OrderId,
			  @FirstAttempt,
			  @LastAttempt
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pKlarnaPendingOrderDelete]'
GO

CREATE PROCEDURE [orderlek].[pKlarnaPendingOrderDelete]
	@KlarnaPendingOrderId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DELETE [orderlek].[tKlarnaPendingOrder]
	WHERE [KlarnaPendingOrderId] = @KlarnaPendingOrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pKlarnaPendingOrderUpdate]'
GO

CREATE PROCEDURE [orderlek].[pKlarnaPendingOrderUpdate]
	@KlarnaPendingOrderId INT,
	@ChannelId INT,
	@OrderId INT,
	@FirstAttempt DATETIME,
	@LastAttempt DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tKlarnaPendingOrder]
	SET	
		[ChannelId] = @ChannelId,
		[OrderId] = @OrderId,
		[FirstAttempt] = @FirstAttempt,
		[LastAttempt] = @LastAttempt
	WHERE
		[KlarnaPendingOrderId] = @KlarnaPendingOrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pKlarnaPendingOrderGetPendingOrdersForStatusCheck]'
GO
CREATE PROCEDURE [orderlek].[pKlarnaPendingOrderGetPendingOrdersForStatusCheck]
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[orderlek].[vKlarnaPendingOrder]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) IF EXISTS(SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
