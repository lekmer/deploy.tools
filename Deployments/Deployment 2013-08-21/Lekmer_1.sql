SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tBlockProductFilter]'
GO
ALTER TABLE [lekmer].[tBlockProductFilter] DROP CONSTRAINT [FK_tBlockProductFilter_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tOrderItemSize]'
GO
ALTER TABLE [lekmer].[tOrderItemSize] DROP CONSTRAINT [FK_tOrderItemSize_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [orderlek].[tPackageOrderItemProduct]'
GO
ALTER TABLE [orderlek].[tPackageOrderItemProduct] DROP CONSTRAINT [FK_tPackageOrderItemProduct_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] DROP CONSTRAINT [FK_tProductSize_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tSizeTableRow]'
GO
ALTER TABLE [lekmer].[tSizeTableRow] DROP CONSTRAINT [FK_tSizeTableRow_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tSize]'
GO
ALTER TABLE [lekmer].[tSize] DROP CONSTRAINT [PK_tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tSize_ErpId] from [lekmer].[tSize]'
GO
DROP INDEX [IX_tSize_ErpId] ON [lekmer].[tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerCategory]'
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD
[PackageInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBrand]'
GO
ALTER TABLE [lekmer].[tBrand] ADD
[PackageInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tSizeTableMedia]'
GO
CREATE TABLE [lekmer].[tSizeTableMedia]
(
[SizeTableId] [int] NOT NULL,
[MediaId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSizeTableMedia] on [lekmer].[tSizeTableMedia]'
GO
ALTER TABLE [lekmer].[tSizeTableMedia] ADD CONSTRAINT [PK_tSizeTableMedia] PRIMARY KEY CLUSTERED  ([SizeTableId], [MediaId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tSizeTable]'
GO
ALTER TABLE [lekmer].[tSizeTable] ALTER COLUMN [Column2Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tSizeTableRow]'
GO
ALTER TABLE [lekmer].[tSizeTableRow] ALTER COLUMN [Column2Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tSize]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tSize]
(
[SizeId] [int] NOT NULL IDENTITY(1, 1),
[ErpId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ErpTitle] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[EU] [decimal] (5, 2) NOT NULL,
[EUTitle] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[USMale] [decimal] (3, 1) NOT NULL,
[USFemale] [decimal] (3, 1) NOT NULL,
[UKMale] [decimal] (3, 1) NOT NULL,
[UKFemale] [decimal] (3, 1) NOT NULL,
[Millimeter] [int] NOT NULL,
[Ordinal] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tSize] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tSize]([SizeId], [ErpId], [ErpTitle], [EU], [EUTitle], [USMale], [USFemale], [UKMale], [UKFemale], [Millimeter], [Ordinal]) SELECT [SizeId], [ErpId], [ErpTitle], [EU], [EUTitle], [USMale], [USFemale], [UKMale], [UKFemale], [Millimeter], [SizeId] FROM [lekmer].[tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tSize] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[lekmer].[tSize]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[lekmer].[tmp_rg_xx_tSize]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tSize]', N'tSize'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSize] on [lekmer].[tSize]'
GO
ALTER TABLE [lekmer].[tSize] ADD CONSTRAINT [PK_tSize] PRIMARY KEY CLUSTERED  ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tSize_ErpId] on [lekmer].[tSize]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tSize_ErpId] ON [lekmer].[tSize] ([ErpId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBrandTranslation]'
GO
ALTER TABLE [lekmer].[tBrandTranslation] ADD
[PackageInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tContentMessageIncludeCategory]'
GO
CREATE TABLE [lekmer].[tContentMessageIncludeCategory]
(
[ContentMessageId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tContentMessageIncludeCategory] on [lekmer].[tContentMessageIncludeCategory]'
GO
ALTER TABLE [lekmer].[tContentMessageIncludeCategory] ADD CONSTRAINT [PK_tContentMessageIncludeCategory] PRIMARY KEY CLUSTERED  ([ContentMessageId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tContentMessage]'
GO
CREATE TABLE [lekmer].[tContentMessage]
(
[ContentMessageId] [int] NOT NULL IDENTITY(1, 1),
[ContentMessageFolderId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tContentMessage] on [lekmer].[tContentMessage]'
GO
ALTER TABLE [lekmer].[tContentMessage] ADD CONSTRAINT [PK_tContentMessage] PRIMARY KEY CLUSTERED  ([ContentMessageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tPackageTranslation]'
GO
CREATE TABLE [productlek].[tPackageTranslation]
(
[PackageId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[GeneralInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPackageTranslation] on [productlek].[tPackageTranslation]'
GO
ALTER TABLE [productlek].[tPackageTranslation] ADD CONSTRAINT [PK_tPackageTranslation] PRIMARY KEY CLUSTERED  ([PackageId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[tPackage]'
GO
ALTER TABLE [productlek].[tPackage] ADD
[GeneralInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tLekmerCategoryTranslation]'
GO
CREATE TABLE [productlek].[tLekmerCategoryTranslation]
(
[CategoryId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[PackageInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tLekmerCategoryTranslation] on [productlek].[tLekmerCategoryTranslation]'
GO
ALTER TABLE [productlek].[tLekmerCategoryTranslation] ADD CONSTRAINT [PK_tLekmerCategoryTranslation] PRIMARY KEY CLUSTERED  ([CategoryId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tContentMessageFolder]'
GO
CREATE TABLE [lekmer].[tContentMessageFolder]
(
[ContentMessageFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentContentMessageFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tContentMessageFolder] on [lekmer].[tContentMessageFolder]'
GO
ALTER TABLE [lekmer].[tContentMessageFolder] ADD CONSTRAINT [PK_tContentMessageFolder] PRIMARY KEY CLUSTERED  ([ContentMessageFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tContentMessageIncludeBrand]'
GO
CREATE TABLE [lekmer].[tContentMessageIncludeBrand]
(
[ContentMessageId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tContentMessageIncludeBrand] on [lekmer].[tContentMessageIncludeBrand]'
GO
ALTER TABLE [lekmer].[tContentMessageIncludeBrand] ADD CONSTRAINT [PK_tContentMessageIncludeBrand] PRIMARY KEY CLUSTERED  ([ContentMessageId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tContentMessageIncludeProduct]'
GO
CREATE TABLE [lekmer].[tContentMessageIncludeProduct]
(
[ContentMessageId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tContentMessageIncludeProduct] on [lekmer].[tContentMessageIncludeProduct]'
GO
ALTER TABLE [lekmer].[tContentMessageIncludeProduct] ADD CONSTRAINT [PK_tContentMessageIncludeProduct] PRIMARY KEY CLUSTERED  ([ContentMessageId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tContentMessageTranslation]'
GO
CREATE TABLE [lekmer].[tContentMessageTranslation]
(
[ContentMessageId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tContentMessageTranslation] on [lekmer].[tContentMessageTranslation]'
GO
ALTER TABLE [lekmer].[tContentMessageTranslation] ADD CONSTRAINT [PK_tContentMessageTranslation] PRIMARY KEY CLUSTERED  ([ContentMessageId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vContentMessageSecure]'
GO


CREATE VIEW [lekmer].[vContentMessageSecure]
AS
	SELECT
		[cm].[ContentMessageId]			'ContentMessage.ContentMessageId',
		[cm].[ContentMessageFolderId]	'ContentMessage.ContentMessageFolderId',
		[cm].[CommonName]				'ContentMessage.CommonName',
		[cm].[Message]					'ContentMessage.Message'
	FROM 
		[lekmer].[tContentMessage] cm

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageGetAllByFolder]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetAllByFolder]
	@ContentMessageFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[cm].*
	FROM
		[lekmer].[vContentMessageSecure] cm
	WHERE
		[cm].[ContentMessage.ContentMessageFolderId] = @ContentMessageFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vContentMessageFolder]'
GO

CREATE VIEW [lekmer].[vContentMessageFolder]
AS
	SELECT
		[cmf].[ContentMessageFolderId]			'ContentMessageFolder.ContentMessageFolderId',
		[cmf].[ParentContentMessageFolderId]	'ContentMessageFolder.ParentContentMessageFolderId',
		[cmf].[Title]							'ContentMessageFolder.Title'
	FROM 
		[lekmer].[tContentMessageFolder] cmf
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageFolderGetTree]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderGetTree]
	@SelectedId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tTree TABLE (Id INT, ParentId INT, Title NVARCHAR(50))

	INSERT INTO @tTree
	SELECT [cmf].*
	FROM [lekmer].[vContentMessageFolder] AS cmf
	WHERE [cmf].[ContentMessageFolder.ParentContentMessageFolderId] IS NULL

	IF (@SelectedId IS NOT NULL)
	BEGIN
		INSERT INTO @tTree
		SELECT [cmf].*
		FROM [lekmer].[vContentMessageFolder] AS cmf
		WHERE [cmf].[ContentMessageFolder.ParentContentMessageFolderId] = @SelectedId

		DECLARE @ParentId INT
		WHILE (@SelectedId IS NOT NULL)
		BEGIN
			SET @ParentId = (SELECT [cmf].[ContentMessageFolder.ParentContentMessageFolderId]
							 FROM [lekmer].[vContentMessageFolder] AS cmf
							 WHERE [cmf].[ContentMessageFolder.ContentMessageFolderId] = @SelectedId)
							 
			INSERT INTO @tTree
			SELECT [cmf].*
			FROM [lekmer].[vContentMessageFolder] AS cmf
			WHERE [cmf].[ContentMessageFolder.ParentContentMessageFolderId] = @ParentId
			
			SET @SelectedId = @ParentId
		END
	END
		
	SELECT
		Id,
		ParentId,
		Title,
		CAST((CASE WHEN EXISTS (SELECT 1 FROM [lekmer].[vContentMessageFolder] AS cmf
								WHERE [cmf].[ContentMessageFolder.ParentContentMessageFolderId] = Id)
		THEN 1 ELSE 0 END) AS BIT) AS 'HasChildren'
	FROM
		@tTree
	ORDER BY
		ParentId ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrandTranslation]'
GO
ALTER VIEW [lekmer].[vBrandTranslation]
AS
SELECT 
	[BrandId]		'BrandTranslation.BrandId',
	[LanguageId]	'BrandTranslation.LanguageId',
	[Title]			'BrandTranslation.Title',
	[Description]	'BrandTranslation.Description',
	[PackageInfo]	'BrandTranslation.PackageInfo'
FROM 
	[lekmer].[tBrandTranslation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrand]'
GO
ALTER VIEW [lekmer].[vBrand]
AS
	SELECT     
		[B].[BrandId] 'Brand.BrandId',
		COALESCE ([bt].[BrandTranslation.Title], [B].[Title]) 'Brand.Title',
		COALESCE ([bt].[BrandTranslation.Description], [B].[Description]) 'Brand.Description',		
		COALESCE ([bt].[BrandTranslation.PackageInfo], [B].[PackageInfo]) 'Brand.PackageInfo',		
		[B].[ExternalUrl] 'Brand.ExternalUrl',
		[B].[MediaId] 'Brand.MediaId',
		[B].[ErpId] 'Brand.ErpId',
		[Ch].[ChannelId],
		[I].*,
		[Bssr].[ContentNodeId] 'Brand.ContentNodeId'
	FROM
		lekmer.tBrand AS B
		CROSS JOIN core.tChannel AS Ch
		
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = B.MediaId AND I.[Image.LanguageId] = Ch.LanguageId
		LEFT JOIN lekmer.vBrandTranslation AS bt ON bt.[BrandTranslation.BrandId] = B.BrandId AND bt.[BrandTranslation.LanguageId] = Ch.LanguageId
		LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = Ch.ChannelId
		LEFT JOIN lekmer.tBrandSiteStructureRegistry Bssr ON Bssr.BrandId = B.BrandId AND Bssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageSearch]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageSearch]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		[cm].*
	FROM 
		[lekmer].[vContentMessageSecure] cm
	WHERE
		[cm].[ContentMessage.Message] LIKE @SearchCriteria ESCAPE '\'
		OR [cm].[ContentMessage.CommonName] LIKE @SearchCriteria ESCAPE '\'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrandSecure]'
GO
ALTER VIEW [lekmer].[vBrandSecure]
AS
SELECT     
	[B].[BrandId] 'Brand.BrandId',
    [B].[Title] 'Brand.Title',
    [B].[Description] 'Brand.Description',
    [B].[PackageInfo] 'Brand.PackageInfo',
    [B].[ExternalUrl] 'Brand.ExternalUrl',
    [B].[MediaId] 'Brand.MediaId',
    [B].[ErpId] 'Brand.ErpId',
    [I].*,
    NULL 'Brand.ContentNodeId'
FROM
	lekmer.tBrand B
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = B.MediaId
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategory]'
GO

ALTER VIEW [product].[vCustomCategory]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		COALESCE ([lct].[PackageInfo], [l].[PackageInfo]) AS [LekmerCategory.PackageInfo]
	FROM
		[product].[vCategory] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]
		LEFT JOIN [productlek].[tLekmerCategoryTranslation] lct ON [lct].[CategoryId] = [c].[Category.Id] AND [lct].[LanguageId] = [c].[Category.LanguageId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategorySecure]'
GO

ALTER VIEW [product].[vCustomCategorySecure]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		[l].[PackageInfo] AS [LekmerCategory.PackageInfo]
	FROM
		[product].[vCategorySecure] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vSize]'
GO

ALTER VIEW [lekmer].[vSize]
AS
SELECT
	[SizeId] AS 'Size.Id',
	[ErpId] AS 'Size.ErpId',
	[ErpTitle] AS 'Size.ErpTitle',
	[EU] AS 'Size.EU',
	[EUTitle] AS 'Size.EUTitle',
	[USMale] AS 'Size.USMale',
	[USFemale] AS 'Size.USFemale',
	[UKMale] AS 'Size.UKMale',
	[UKFemale] AS 'Size.UKFemale',
	[Millimeter] AS 'Size.Millimeter',
	[Ordinal] AS  'Size.Ordinal'
FROM
	[lekmer].[tSize]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeGetAll]'
GO
ALTER PROCEDURE [lekmer].[pSizeGetAll]
AS 
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		*
	FROM 
		[lekmer].[vSize]
	ORDER BY
		[Size.Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageTranslationDelete]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageTranslationDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [lekmer].[tContentMessageTranslation]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageIncludeProductDelete]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludeProductDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tContentMessageIncludeProduct]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageIncludeCategoryDelete]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludeCategoryDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tContentMessageIncludeCategory]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageIncludeBrandDelete]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludeBrandDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tContentMessageIncludeBrand]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageDelete]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pContentMessageIncludeBrandDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeCategoryDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeProductDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageTranslationDelete] @ContentMessageId
		
	DELETE FROM [lekmer].[tContentMessage]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageSave]'
GO
ALTER PROCEDURE [productlek].[pPackageSave]
	@PackageId			INT,
	@MasterProductId	INT,
	@ErpId				VARCHAR(50) = NULL,
	@StatusId			INT,
	@Title				NVARCHAR(256),
	@WebShopTitle		NVARCHAR(256),
	@NumberInStock		INT,
	@CategoryId			INT,
	@Description		NVARCHAR(MAX),
	@GeneralInfo		NVARCHAR(MAX),
	@PriceXml			XML = NULL
	/*
	'<prices>
		<price priceListId="1" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="2" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="3" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="4" priceIncludingVat="10.99" vat="25.00"/>
	</prices>'
	*/
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	BEGIN
		BEGIN TRANSACTION
			IF @ErpId IS NULL
			BEGIN
				SET @ErpId = (SELECT TOP (1) [pr].[ErpId] 
						  FROM [productlek].[tPackage] p
								INNER JOIN [product].[tProduct] pr ON [pr].[ProductId] = [p].[MasterProductId]
						  ORDER BY [PackageId] DESC)

				IF @ErpId IS NULL
				BEGIN
					SET @ErpId = 'P000001-0000'
				END
				ELSE
				BEGIN
					DECLARE @ErpTemp INT
					SET @ErpTemp = (SELECT CAST(SUBSTRING(@ErpId, 2, 6) AS INT) + 1)

					SET @ErpId = (SELECT 'P' + RIGHT('000000' + CAST(@ErpTemp AS VARCHAR(6)), 6) + '-0000')
				END
			END
			
			-- tProduct
			INSERT INTO [product].[tProduct] (
				[ErpId],
				[IsDeleted],
				[NumberInStock],
				[CategoryId],
				[Title],
				[WebShopTitle],
				[Description],
				[ProductStatusId]
			)
			VALUES (
				@ErpId,
				0, -- IsDeleted
				@NumberInStock,
				@CategoryId,
				@Title,
				@WebShopTitle,
				@Description,
				@StatusId
			)

			SET @MasterProductId = SCOPE_IDENTITY()

			-- tLekmerProduct
			INSERT INTO [lekmer].[tLekmerProduct] (
				[ProductId],
				[IsBookable],
				[AgeFromMonth],
				[AgeToMonth],
				[IsBatteryIncluded],
				[HYErpId],
				[ShowVariantRelations],
				[ProductTypeId]
			)
			VALUES (
				@MasterProductId,
				0, -- IsBookable
				0, -- AgeFromMonth
				0, -- AgeToMonth
				0, -- IsBatteryIncluded
				@ErpId,
				0, -- ShowVariantRelations
				2 -- ProductTypeId = Package
			)
			
			-- tProductUrl
			INSERT INTO [lekmer].[tProductUrl] (
				[ProductId],
				[LanguageId],
				[UrlTitle])
			SELECT
				@MasterProductId,
				[LanguageId],
				CASE WHEN @WebShopTitle IS NOT NULL THEN @WebShopTitle ELSE @Title END
			FROM [core].[tLanguage]

			-- tProductRegistryProduct
			INSERT INTO [product].[tProductRegistryProduct] (
				[ProductId],
				[ProductRegistryId]
			)
			SELECT
				@MasterProductId,
				[pr].[ProductRegistryId]
			FROM  [product].[tProductRegistry] pr
			WHERE NOT EXISTS (SELECT 1 FROM [product].[tProductRegistryProduct] prp
							  WHERE [prp].[ProductId] = @MasterProductId
									AND [prp].[ProductRegistryId] = [pr].[ProductRegistryId])

			-- tPackage
			INSERT INTO [productlek].[tPackage] ([MasterProductId], [GeneralInfo]) SELECT @MasterProductId, @GeneralInfo
			
			SET @PackageId = CAST(SCOPE_IDENTITY() AS INT)
		COMMIT
	END
	ELSE
	BEGIN
		-- tProduct
		UPDATE [product].[tProduct]
		SET [NumberInStock] = @NumberInStock,
			[CategoryId] = @CategoryId,
			[ProductStatusId] = @StatusId,
			[Title] = @Title,
			[WebShopTitle] = @WebShopTitle,
			[Description] = @Description
		WHERE [ProductId] = @MasterProductId
		
		UPDATE [productlek].[tPackage]
		SET [GeneralInfo] = @GeneralInfo
		WHERE [PackageId] = @PackageId
	END
	
	-- tPriceListItem
	DELETE FROM [product].[tPriceListItem] WHERE [ProductId] = @MasterProductId
	
	IF @PriceXml IS NOT NULL
	BEGIN
		INSERT INTO [product].[tPriceListItem] (
				[PriceListId],
				[ProductId],
				[PriceIncludingVat],
				[PriceExcludingVat],
				[VatPercentage]
			)
		SELECT
			c.value('@priceListId[1]', 'int'),
			@MasterProductId,
			c.value('@priceIncludingVat[1]', 'decimal(16,2)'),
			c.value('@priceIncludingVat[1]', 'decimal(16,2)') / (1.0+c.value('@vat[1]', 'decimal(16,2)')/100.0),
			c.value('@vat[1]', 'decimal(16,2)')
		FROM
			@PriceXml.nodes('/prices/price') T(c)
	END
	
	SELECT @PackageId, @MasterProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[vPackageSecure]'
GO



ALTER VIEW [productlek].[vPackageSecure]
AS
SELECT
	[p].[PackageId] 'Package.PackageId',
	[p].[MasterProductId] 'Package.MasterProductId',
	[p].[GeneralInfo] 'Package.GeneralInfo',
	[cpr].*
FROM
	[productlek].[tPackage] p
	INNER JOIN [product].[vCustomProductRecord] cpr ON [cpr].[Product.Id] = [p].[MasterProductId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableMediaDelete]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [lekmer].[tSizeTableMedia]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pSizeTableDelete]'
GO
ALTER PROCEDURE [lekmer].[pSizeTableDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pSizeTableIncludeBrandDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeCategoryDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeProductDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableTranslationDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableRowDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableMediaDelete] @SizeTableId
		
	DELETE FROM [lekmer].[tSizeTable]
	WHERE [SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vContentMessage]'
GO


CREATE VIEW [lekmer].[vContentMessage]
AS
	SELECT
		[cm].[ContentMessageId]			'ContentMessage.ContentMessageId',
		[cm].[ContentMessageFolderId]	'ContentMessage.ContentMessageFolderId',
		[cm].[CommonName]			'ContentMessage.CommonName',
		COALESCE ([cmt].[Message], [cm].[Message]) 'ContentMessage.Message',
		[l].[LanguageId]			'ContentMessage.LanguageId'
	FROM 
		[lekmer].[tContentMessage] cm
		CROSS JOIN [core].[tLanguage] AS l
		LEFT JOIN [lekmer].[tContentMessageTranslation] AS cmt ON [cmt].[ContentMessageId] = [cm].[ContentMessageId] AND [cmt].[LanguageId] = l.[LanguageId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageGetByBrand]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetByBrand]
	@BrandId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ContentMessageId INT
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeBrand] WHERE [BrandId] = @BrandId)
	RETURN @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageIncludesClear]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludesClear]
	@ProductIds		VARCHAR(MAX),
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DELETE ip FROM [lekmer].[tContentMessageIncludeProduct] ip
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter)) p ON [p].[ID] = [ip].[ProductId]

	DELETE ic FROM [lekmer].[tContentMessageIncludeCategory] ic
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter)) c ON [c].[ID] = [ic].[CategoryId]
	
	DELETE ib FROM [lekmer].[tContentMessageIncludeBrand] ib
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter)) b ON [b].[ID] = [ib].[BrandId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageIncludesSave]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludesSave]
	@ContentMessageId	INT,
	@ProductIds		VARCHAR(MAX),
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	EXEC [lekmer].[pContentMessageIncludeProductDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeCategoryDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeBrandDelete] @ContentMessageId

	EXEC [lekmer].[pContentMessageIncludesClear] @ProductIds, @CategoryIds, @BrandIds, @Delimiter
	
	INSERT [lekmer].[tContentMessageIncludeProduct] ([ContentMessageId], [ProductId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p
	
	INSERT [lekmer].[tContentMessageIncludeCategory] ([ContentMessageId], [CategoryId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) p
	
	INSERT [lekmer].[tContentMessageIncludeBrand] ([ContentMessageId], [BrandId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) p
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vPackageTranslation]'
GO
CREATE VIEW [productlek].[vPackageTranslation]
AS
SELECT 
	[PackageId]		'PackageTranslation.PackageId',
	[LanguageId]	'PackageTranslation.LanguageId',
	[GeneralInfo]	'PackageTranslation.GeneralInfo'
FROM 
	[productlek].[tPackageTranslation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vPackage]'
GO
CREATE VIEW [productlek].[vPackage]
AS
	SELECT     
		[p].[PackageId] 'Package.PackageId',
		[p].[MasterProductId] 'Package.MasterProductId',
		COALESCE ([pt].[PackageTranslation.GeneralInfo], [p].[GeneralInfo]) 'Package.GeneralInfo',
		[ch].[ChannelId],
		[cpr].*
	FROM
		[productlek].[tPackage] p
		INNER JOIN [product].[vCustomProductRecord] cpr ON [cpr].[Product.Id] = [p].[MasterProductId]
		CROSS JOIN [core].[tChannel] ch
		LEFT JOIN [productlek].[vPackageTranslation] pt ON [pt].[PackageTranslation.PackageId] = [p].[PackageId]
			AND [pt].[PackageTranslation.LanguageId] = [ch].[LanguageId]
		
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageGetByMasterProduct]'
GO
CREATE PROCEDURE [productlek].[pPackageGetByMasterProduct]
	@MasterProductId	INT,
	@ChannelId			INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	     *
	FROM
	    [productlek].[vPackage]
	WHERE 
		[Package.MasterProductId] = @MasterProductId
		AND [ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageGetAll]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[cm].*
	FROM
		[lekmer].[vContentMessageSecure] cm
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageFolderGetAllByParent]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SELECT
		[cmf].*
	FROM
		[lekmer].[vContentMessageFolder] cmf
	WHERE
		([cmf].[ContentMessageFolder.ParentContentMessageFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		([cmf].[ContentMessageFolder.ParentContentMessageFolderId] IS NULL AND @ParentId IS NULL)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageIncludesGetAll]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludesGetAll]
	@ContentMessageId	INT
AS
BEGIN
	SELECT [ProductId] FROM [lekmer].[tContentMessageIncludeProduct] WHERE [ContentMessageId] = @ContentMessageId
	SELECT [CategoryId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [ContentMessageId] = @ContentMessageId
	SELECT [BrandId] FROM [lekmer].[tContentMessageIncludeBrand] WHERE [ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageGetByProduct]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetByProduct]
	@ProductId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ContentMessageId INT
	
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeProduct] WHERE [ProductId] = @ProductId)
	IF @ContentMessageId IS NOT NULL AND @ContentMessageId > 0
		RETURN @ContentMessageId

	/*--------------------------------------------------------------------------------------------------------------------------*/

	DECLARE @CategoryFirstLevelId INT
	DECLARE @CategorySecondLevelId INT
	DECLARE @CategoryThirdLevelId INT

	SET @CategoryThirdLevelId = (SELECT [CategoryId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId)
	-- Size table include category 3 level
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	IF @ContentMessageId IS NOT NULL AND @ContentMessageId > 0
		RETURN @ContentMessageId

	SET @CategorySecondLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	-- Size table include category 2 level
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @CategorySecondLevelId)
	IF @ContentMessageId IS NOT NULL AND @ContentMessageId > 0
		RETURN @ContentMessageId

	SET @CategoryFirstLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategorySecondLevelId)		
	-- Size table include category 1 level
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @CategoryFirstLevelId)
	IF @ContentMessageId IS NOT NULL AND @ContentMessageId > 0
		RETURN @ContentMessageId

	/*--------------------------------------------------------------------------------------------------------------------------*/

	DECLARE @BrandId INT
	SET @BrandId = (SELECT [BrandId] FROM [lekmer].[tLekmerProduct] WHERE [ProductId] = @ProductId)
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeBrand] WHERE [BrandId] = @BrandId)
	RETURN @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageGetByIdSecure]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetByIdSecure]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[cm].*
	FROM
		[lekmer].[vContentMessageSecure] cm
	WHERE
		[cm].[ContentMessage.ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vContentMessageTranslation]'
GO


CREATE VIEW [lekmer].[vContentMessageTranslation]
AS
SELECT 
	[ContentMessageId] AS 'ContentMessageTranslation.ContentMessageId',
	[LanguageId] AS 'ContentMessageTranslation.LanguageId',
	[Message] AS 'ContentMessageTranslation.Message'
FROM 
	[lekmer].[tContentMessageTranslation]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageTranslationGetAll]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageTranslationGetAll]
	@ContentMessageId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
	    [cmt].*
	FROM
	    [lekmer].[vContentMessageTranslation] cmt
	WHERE 
		cmt.[ContentMessageTranslation.ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageTranslationSave]'
GO
CREATE PROCEDURE [productlek].[pPackageTranslationSave]
	@PackageId		INT,
	@LanguageId		INT,
	@GeneralInfo	NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[productlek].[tPackageTranslation]
	SET
		[GeneralInfo] = @GeneralInfo
	WHERE
		[PackageId] = @PackageId AND
		[LanguageId] = @LanguageId

	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [productlek].[tPackageTranslation] (
			[PackageId],
			[LanguageId],
			[GeneralInfo]
		)
		VALUES (
			@PackageId,
			@LanguageId,
			@GeneralInfo
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeTableMediaSave]'
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaSave]
	@SizeTableId	INT,
	@MediaIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	EXEC [lekmer].[pSizeTableMediaDelete] @SizeTableId
	
	INSERT [lekmer].[tSizeTableMedia] ([SizeTableId], [MediaId])
	SELECT @SizeTableId, [m].[ID] FROM [generic].[fnConvertIDListToTable](@MediaIds, @Delimiter) m
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageGetByCategory]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetByCategory]
	@CategoryId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @CategoryFirstLevelId INT
	DECLARE @CategorySecondLevelId INT
	DECLARE @CategoryThirdLevelId INT
	
	SET @CategoryThirdLevelId = @CategoryId
	DECLARE @ContentMessageId INT
	-- Size table include category 3 level
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	IF @ContentMessageId IS NOT NULL AND @ContentMessageId > 0
		RETURN @ContentMessageId

	SET @CategorySecondLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	-- Size table include category 2 level
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @CategorySecondLevelId)
	IF @ContentMessageId IS NOT NULL AND @ContentMessageId > 0
		RETURN @ContentMessageId
	
	SET @CategoryFirstLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategorySecondLevelId)		
	-- Size table include category 1 level
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @CategoryFirstLevelId)
	IF @ContentMessageId IS NOT NULL AND @ContentMessageId > 0
		RETURN @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageGeneralInfoTranslationGetAllByPackage]'
GO
CREATE PROCEDURE [productlek].[pPackageGeneralInfoTranslationGetAllByPackage]
	@PackageId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [PackageTranslation.PackageId]   AS 'Id',
		[PackageTranslation.LanguageId]  AS 'LanguageId',
		[PackageTranslation.GeneralInfo] AS 'Value'
	FROM
	    [productlek].[vPackageTranslation] 
	WHERE
		[PackageTranslation.PackageId] = @PackageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pSizeSetOrdinals]'
GO

CREATE PROCEDURE [lekmer].[pSizeSetOrdinals]
	@SizeIdList VARCHAR(MAX),
	@Separator CHAR(1)
AS
BEGIN
	UPDATE
		s
	SET
		s.[Ordinal] = t.[Ordinal]
	FROM
		[lekmer].[tSize] s
		INNER JOIN generic.fnConvertIDListToTableWithOrdinal(@SizeIdList, @Separator) t ON s.[SizeId] = t.[Id]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pMediaItemGetAllBySizeTable]'
GO
CREATE PROCEDURE [lekmer].[pMediaItemGetAllBySizeTable]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT M.*
	FROM [lekmer].[tSizeTableMedia] STM
	INNER JOIN [media].[vCustomMedia] M ON [M].[Media.Id] = [STM].[MediaId]
	WHERE STM.[SizeTableId] = @SizeTableId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageFolderGetAll]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		cmf.*
	FROM
		[lekmer].[vContentMessageFolder] cmf
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageFolderDelete]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderDelete]
	@ContentMessageFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [lekmer].[tContentMessageFolder]
	WHERE [ContentMessageFolderId] = @ContentMessageFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandSave]'
GO
ALTER PROCEDURE [lekmer].[pBrandSave]
	@BrandId		INT,
	@Title			VARCHAR(500),
	@MediaId		INT = NULL,
	@Description 	VARCHAR(MAX) = NULL,
	@ExternalUrl 	VARCHAR(500) = NULL,
	@PackageInfo 	NVARCHAR(MAX) = NULL
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM lekmer.tBrand WHERE Title = @Title AND BrandId <> @BrandId)
		RETURN -1

	UPDATE
		[lekmer].[tBrand]
	SET
		[Title] = @Title,	
		[MediaId] = @MediaId,
		[Description] = @Description,
		[ExternalUrl] = @ExternalUrl,
		[PackageInfo] = @PackageInfo
	WHERE
		[BrandId] = @BrandId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tBrand]
		( 
			[Title],
			[MediaId],
			[Description],
			[ExternalUrl],
			[PackageInfo]
		)
		VALUES
		(
			@Title,
			@MediaId ,
			@Description,
			@ExternalUrl,
			@PackageInfo
		)
		SET @BrandId = scope_identity()						
	END 
	
	RETURN @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageFolderGetById]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderGetById]
	@ContentMessageFolderId	INT
AS
BEGIN
	SELECT
		[cmf].*
	FROM
		[lekmer].[vContentMessageFolder] cmf
	WHERE
		[cmf].[ContentMessageFolder.ContentMessageFolderId] = @ContentMessageFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageDelete]'
GO
ALTER PROCEDURE [productlek].[pPackageDelete]
	@PackageId	INT
AS
BEGIN
	DECLARE @MasterProductId INT 
	SET @MasterProductId = (SELECT [MasterProductId] FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	
	UPDATE [product].[tProduct]
	SET [IsDeleted] = 1
	WHERE [ProductId] = @MasterProductId
	
	DELETE FROM [productlek].[tPackageTranslation]
	WHERE [PackageId] = @PackageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pCategoryPackageInfoTranslationGetAllByCategory]'
GO
CREATE PROCEDURE [productlek].[pCategoryPackageInfoTranslationGetAllByCategory]
	@CategoryId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [CategoryId] AS 'Id',
		[LanguageId] AS 'LanguageId',
		[PackageInfo] AS 'Value'
	FROM
	    [productlek].[tLekmerCategoryTranslation] 
	WHERE
		[CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandTranslationSave]'
GO
ALTER PROCEDURE [lekmer].[pBrandTranslationSave]
	@BrandId		INT,
	@LanguageId		INT,
	@Title			NVARCHAR(500),
	@Description	NVARCHAR(MAX),
	@PackageInfo	NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		lekmer.tBrandTranslation
	SET
		[Title] = @Title,
		[Description] = @Description,
		[PackageInfo] = @PackageInfo
	WHERE
		[BrandId] = @BrandId AND
		[LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO lekmer.tBrandTranslation
		(
			[BrandId],
			[LanguageId],
			[Title],
			[Description],
			[PackageInfo]	
		)
		VALUES
		(
			@BrandId,
			@LanguageId,
			@Title,
			@Description,
			@PackageInfo
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCategoryTranslationSave]'
GO
ALTER PROCEDURE [lekmer].[pCategoryTranslationSave]
	@CategoryId		INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@PackageInfo	NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tCategoryTranslation]
	SET
		[Title] = @Title
	WHERE
		[CategoryId] = @CategoryId AND
		[LanguageId] = @LanguageId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [product].[tCategoryTranslation] (
			[CategoryId],
			[LanguageId],
			[Title]
		)
		VALUES (
			@CategoryId,
			@LanguageId,
			@Title
		)
	END
	

	UPDATE
		[productlek].[tLekmerCategoryTranslation]
	SET
		[PackageInfo] = @PackageInfo
	WHERE
		[CategoryId] = @CategoryId AND
		[LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [productlek].[tLekmerCategoryTranslation] (
			[CategoryId],
			[LanguageId],
			[PackageInfo]				
		)
		VALUES (
			@CategoryId,
			@LanguageId,
			@PackageInfo
		)
	END	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageSearch]'
GO
ALTER PROCEDURE [productlek].[pPackageSearch]
	@CategoryId		INT,
	@Title			NVARCHAR(256),
	@BrandId		INT,
	@StatusId		INT,
	@PriceFrom		DECIMAL(16,2),
	@PriceTo		DECIMAL(16,2),
	@ErpId			VARCHAR(MAX),
	@ChannelId		INT,
	@Page			INT = NULL,
	@PageSize		INT,
	@SortBy			VARCHAR(20) = NULL,
	@SortDescending BIT = NULL
AS
BEGIN
	DECLARE @sqlGeneral NVARCHAR(MAX), @sql NVARCHAR(MAX), @sqlCount NVARCHAR(MAX), @sqlFragment NVARCHAR(MAX)
	DECLARE @CurrencyId INT, @ProductRegistryId INT

	SELECT @CurrencyId = [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId
	SELECT @ProductRegistryId = [ProductRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId
	
	SET @sqlFragment = '
				SELECT ROW_NUMBER() OVER (ORDER BY [p].[' 
		+ COALESCE(@SortBy, 'ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
					[pak].[PackageId] ''Package.PackageId'',
					[pak].[MasterProductId] ''Package.MasterProductId'',
					[pak].[GeneralInfo] ''Package.GeneralInfo'',
					[p].[ProductId],
					[pli].*
				FROM
					[productlek].[tPackage] pak
					INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [pak].[MasterProductId]
					LEFT JOIN [product].[tProductRegistryProduct] prp ON [prp].[ProductId] = [p].[ProductId] AND [prp].[ProductRegistryId] = @ProductRegistryId
					LEFT JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [prp].[ProductRegistryId] AND [pmc].[ChannelId] = @ChannelId
					LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[ProductId]
																	  AND [pli].[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
																										@CurrencyId,
																										[p].[ProductId],
																										[pmc].[PriceListRegistryId],
																										NULL)
					INNER JOIN [lekmer].[tLekmerProduct] AS lp ON [lp].[ProductId] = [p].[ProductId]
				WHERE
					[p].[IsDeleted] = 0'
		
		+ CASE WHEN (@CategoryId IS NOT NULL) THEN + '
					AND [p].[CategoryId] IN (SELECT * FROM product.fnGetSubCategories(@CategoryId))' ELSE '' END 
				
		+ CASE WHEN (@Title IS NOT NULL) THEN + '
					AND CONTAINS([p].*, @Title)' ELSE '' END
		
		+ CASE WHEN (@PriceFrom IS NOT NULL) THEN + '
					AND [pli].[Price.PriceIncludingVAT] >= @PriceFrom' ELSE '' END
		
		+ CASE WHEN (@PriceTo IS NOT NULL) THEN + '
					AND [pli].[Price.PriceIncludingVAT] <= @PriceTo' ELSE '' END
		
		+ CASE WHEN (@ErpId IS NOT NULL) THEN + '
					AND [p].[ErpId] IN (SELECT * FROM generic.fnConvertIDListToTableString(@ErpId,'',''))' ELSE '' END
		
		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
					AND [p].[ProductStatusId] = @StatusId' ELSE '' END
		
		+ CASE WHEN (@BrandId IS NOT NULL) THEN + '
					AND [lp].[BrandId] = @BrandId' ELSE '' END

	SET @sqlGeneral = '
		IF (@Title IS NOT NULL)
		BEGIN
			SET @Title = generic.fPrepareFulltextSearchParameter(@Title)
		END'
		
	SET @sql = @sqlGeneral + '
		SELECT
			p.*,
			vp.*
		FROM
			(' + @sqlFragment + '
			) AS p
			INNER JOIN [product].[vCustomProductRecord] vp on [vp].[Product.Id] = [p].[ProductId]'

	IF @Page != 0 AND @Page IS NOT NULL
	BEGIN
		SET @sql = @sql + '
		WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
	END
	
	SET @sqlCount = @sqlGeneral + '
		SELECT COUNT(1) FROM
		(' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	
			@CategoryId int,
			@Title nvarchar(256),
			@BrandId int,
			@StatusId int,
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@ChannelId	int,			
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@CategoryId,
			@Title,
			@BrandId,
			@StatusId,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@ChannelId,			
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId

	EXEC sp_executesql @sql, 
		N'	
			@CategoryId int,
			@Title nvarchar(256),
			@BrandId int,
			@StatusId int,
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@ChannelId	int,			
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@CategoryId,
			@Title,
			@BrandId,
			@StatusId,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@ChannelId,			
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pDailySearchSave]'
GO
ALTER PROCEDURE [statistics].[pDailySearchSave]
	@ChannelId		INT,
	@Query			NVARCHAR(400),
	@Date			SMALLDATETIME,
	@HitCount		INT,
	@SearchCount	INT
AS
BEGIN
	UPDATE
		[statistics].[tDailySearch]
	SET
		[HitCount] = @HitCount,
		[SearchCount] = [SearchCount] + @SearchCount
	WHERE
		[ChannelId] = @ChannelId
		AND [Query] = @Query
		AND [Date] = @Date

	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT [statistics].[tDailySearch] (
			[ChannelId],
			[Query],
			[Date],
			[HitCount],
			[SearchCount]
		)
		VALUES (
			@ChannelId,
			@Query,
			@Date,
			@HitCount,
			@SearchCount
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageSave]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageSave]
	@ContentMessageId		INT,
	@ContentMessageFolderId	INT,
	@CommonName				VARCHAR(50),
	@Message				NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same common names are not allowed (return -2)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tContentMessage]
		WHERE [CommonName] = @CommonName
			AND [ContentMessageId] <> @ContentMessageId
	)
	RETURN -2
	
	UPDATE 
		[lekmer].[tContentMessage]
	SET 
		[ContentMessageFolderId] = @ContentMessageFolderId,
		[CommonName] = @CommonName,
		[Message] = @Message
	WHERE
		[ContentMessageId] = @ContentMessageId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tContentMessage] (
			[ContentMessageFolderId],
			[CommonName],
			[Message]
		)
		VALUES (
			@ContentMessageFolderId,
			@CommonName,
			@Message
		)

		SET @ContentMessageId = SCOPE_IDENTITY()
	END

	RETURN @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageGetById]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetById]
	@LanguageId		INT,
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vContentMessage]
	WHERE
		[ContentMessage.ContentMessageId] = @ContentMessageId
		AND [ContentMessage.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCategorySave]'
GO
ALTER PROCEDURE [lekmer].[pCategorySave]
	@CategoryId					INT,
	@Title						NVARCHAR(50),
	@AllowMultipleSizesPurchase	BIT,
	@PackageInfo				NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tCategory]
	SET
		[Title] = @Title
	WHERE
		[CategoryId] = @CategoryId
	
	SET NOCOUNT OFF
		
	UPDATE
		[lekmer].[tLekmerCategory]
	SET
		[AllowMultipleSizesPurchase] = @AllowMultipleSizesPurchase,
		[PackageInfo] = @PackageInfo
	WHERE
		[CategoryId] = @CategoryId
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			[lekmer].[tLekmerCategory]
		( 
			[CategoryId],
			[AllowMultipleSizesPurchase],
			[PackageInfo]
		)
		VALUES 
		(
			@CategoryId,
			@AllowMultipleSizesPurchase,
			@PackageInfo
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageGetByProductSecure]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageGetByProductSecure]
	@ProductId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ContentMessageId INT
	SET @ContentMessageId = (SELECT [ContentMessageId] FROM [lekmer].[tContentMessageIncludeProduct] WHERE [ProductId] = @ProductId)
	RETURN @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageFolderSave]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderSave]
	@ContentMessageFolderId INT,
	@ParentContentMessageFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tContentMessageFolder]
		WHERE [Title] = @Title
			AND [ContentMessageFolderId] <> @ContentMessageFolderId
			AND	((@ParentContentMessageFolderId IS NULL AND [ParentContentMessageFolderId] IS NULL)
				OR [ParentContentMessageFolderId] = @ParentContentMessageFolderId)
	)
	RETURN -1
	
	UPDATE 
		[lekmer].[tContentMessageFolder]
	SET 
		[ParentContentMessageFolderId] = @ParentContentMessageFolderId,
		[Title] = @Title
	WHERE
		[ContentMessageFolderId] = @ContentMessageFolderId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tContentMessageFolder] (
			[ParentContentMessageFolderId],
			[Title]
		)
		VALUES (
			@ParentContentMessageFolderId,
			@Title
		)

		SET @ContentMessageFolderId = SCOPE_IDENTITY()
	END

	RETURN @ContentMessageFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBrandPackageInfoTranslationGetAllByBrand]'
GO
CREATE PROCEDURE [productlek].[pBrandPackageInfoTranslationGetAllByBrand]
	@BrandId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [BrandTranslation.BrandId] AS 'Id',
		[BrandTranslation.LanguageId] AS 'LanguageId',
		[BrandTranslation.PackageInfo] AS 'Value'
	FROM
	    [lekmer].[vBrandTranslation]
	WHERE
		[BrandTranslation.BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageTranslationSave]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageTranslationSave]
	@ContentMessageId	INT,
	@LanguageId		INT,
	@Message	NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tContentMessageTranslation]
	SET
		[Message] = @Message
	WHERE
		[ContentMessageId] = @ContentMessageId 
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [lekmer].[tContentMessageTranslation] (
			[ContentMessageId],
			[LanguageId],
			[Message]
		)
		VALUES (
			@ContentMessageId,
			@LanguageId,
			@Message
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableMedia]'
GO
ALTER TABLE [lekmer].[tSizeTableMedia] ADD CONSTRAINT [FK_tSizeTableMedia_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId]) ON DELETE CASCADE
ALTER TABLE [lekmer].[tSizeTableMedia] ADD CONSTRAINT [FK_tSizeTableMedia_tMedia] FOREIGN KEY ([MediaId]) REFERENCES [media].[tMedia] ([MediaId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tSizeTableRow]'
GO
ALTER TABLE [lekmer].[tSizeTableRow] ADD CONSTRAINT [FK_tSizeTableRow_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockProductFilter]'
GO
ALTER TABLE [lekmer].[tBlockProductFilter] ADD CONSTRAINT [FK_tBlockProductFilter_tSize] FOREIGN KEY ([DefaultSizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tOrderItemSize]'
GO
ALTER TABLE [lekmer].[tOrderItemSize] ADD CONSTRAINT [FK_tOrderItemSize_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] ADD CONSTRAINT [FK_tProductSize_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tPackageOrderItemProduct]'
GO
ALTER TABLE [orderlek].[tPackageOrderItemProduct] ADD CONSTRAINT [FK_tPackageOrderItemProduct_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tContentMessageIncludeCategory]'
GO
ALTER TABLE [lekmer].[tContentMessageIncludeCategory] ADD CONSTRAINT [FK_tContentMessageIncludeCategory_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
ALTER TABLE [lekmer].[tContentMessageIncludeCategory] ADD CONSTRAINT [FK_tContentMessageIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tPackageTranslation]'
GO
ALTER TABLE [productlek].[tPackageTranslation] ADD CONSTRAINT [FK_tPackageTranslation_tPackage] FOREIGN KEY ([PackageId]) REFERENCES [productlek].[tPackage] ([PackageId]) ON DELETE CASCADE
ALTER TABLE [productlek].[tPackageTranslation] ADD CONSTRAINT [FK_tPackageTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tContentMessageFolder]'
GO
ALTER TABLE [lekmer].[tContentMessageFolder] ADD CONSTRAINT [FK_tContentMessageFolder_tContentMessageFolder] FOREIGN KEY ([ParentContentMessageFolderId]) REFERENCES [lekmer].[tContentMessageFolder] ([ContentMessageFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tContentMessage]'
GO
ALTER TABLE [lekmer].[tContentMessage] ADD CONSTRAINT [FK_tContentMessage_tContentMessageFolder] FOREIGN KEY ([ContentMessageFolderId]) REFERENCES [lekmer].[tContentMessageFolder] ([ContentMessageFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tLekmerCategoryTranslation]'
GO
ALTER TABLE [productlek].[tLekmerCategoryTranslation] ADD CONSTRAINT [FK_tLekmerCategoryTranslation_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
ALTER TABLE [productlek].[tLekmerCategoryTranslation] ADD CONSTRAINT [FK_tLekmerCategoryTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tContentMessageIncludeBrand]'
GO
ALTER TABLE [lekmer].[tContentMessageIncludeBrand] ADD CONSTRAINT [FK_tContentMessageIncludeBrand_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
ALTER TABLE [lekmer].[tContentMessageIncludeBrand] ADD CONSTRAINT [FK_tContentMessageIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tContentMessageIncludeProduct]'
GO
ALTER TABLE [lekmer].[tContentMessageIncludeProduct] ADD CONSTRAINT [FK_tContentMessageIncludeProduct_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
ALTER TABLE [lekmer].[tContentMessageIncludeProduct] ADD CONSTRAINT [FK_tContentMessageIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tContentMessageTranslation]'
GO
ALTER TABLE [lekmer].[tContentMessageTranslation] ADD CONSTRAINT [FK_tContentMessageTranslation_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
ALTER TABLE [lekmer].[tContentMessageTranslation] ADD CONSTRAINT [FK_tContentMessageTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
