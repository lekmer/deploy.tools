/*
Run this script on:

(local).Lekmer_live – this database will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 8/21/2013 10:33:06 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Update rows in [template].[tModelFragmentFunction]
UPDATE [template].[tModelFragmentFunction] SET [FunctionTypeId]=1 WHERE [FunctionId]=149
UPDATE [template].[tModelFragmentFunction] SET [FunctionTypeId]=1 WHERE [FunctionId]=334
-- Operation applied to 2 rows out of 2

-- Add rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001169, 47, 2, N'HasPackagGeneralInfo', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001170, 47, 1, N'PackagGeneralInfo', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001171, 47, 2, N'HasBrandPackageInfo', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001172, 47, 1, N'BrandPackageInfo', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001173, 47, 2, N'HasCategoryPackageInfo', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001174, 47, 1, N'CategoryPackageInfo', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001175, 47, 2, N'HasContentMessage', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001176, 47, 1, N'ContentMessage', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
-- Operation applied to 8 rows out of 8

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
COMMIT TRANSACTION
GO
