SELECT *
FROM sys.indexes
WHERE ALLOW_PAGE_LOCKS = 0

SELECT 'ALTER INDEX ' + I.Name + ' ON ' + '[' + S.[name] + '].[' + T.Name + ']' + ' SET (ALLOW_PAGE_LOCKS = ON)' As Command
FROM sys.indexes I
LEFT OUTER JOIN sys.tables T ON I.object_id = t.object_id
LEFT OUTER JOIN sys.schemas S ON t.schema_id = S.[schema_id]
WHERE I.allow_page_locks = 0 AND T.Name IS NOT NULL

ALTER INDEX UQ_tVariation_VariationType_Value ON [product].[tVariation] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tVariationType_VariationGroupId_Title ON [product].[tVariationType] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tMediaFolder_MediaFolderParentId_Title ON [media].[tMediaFolder] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tModelFragment_ModelId_Title ON [template].[tModelFragment] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tPriceListFolder_ParentPriceListFolder_Title ON [product].[tPriceListFolder] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tAlias_CommonName ON [template].[tAlias] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tModelSetting_ModelId_CommonName ON [template].[tModelSetting] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tModelSetting_ModelId_Title ON [template].[tModelSetting] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tComponent_CommonName ON [template].[tComponent] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tAliasFolder_ParentAliasFolderId_Title ON [template].[tAliasFolder] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tVariationGroup_Title ON [product].[tVariationGroup] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_Block_ContentNodeId_ContentAreaId_Title ON [sitestructure].[tBlock] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX UQ_tIncludeFolder_ParentIncludeFolderId_Title ON [template].[tIncludeFolder] SET (ALLOW_PAGE_LOCKS = ON)
ALTER INDEX IX_tProduct_ErpId ON [product].[tProduct] SET (ALLOW_PAGE_LOCKS = ON)