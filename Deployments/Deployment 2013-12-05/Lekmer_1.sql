SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD
[MaxQuantityPerOrder] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerCategory]'
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD
[MaxQuantityPerOrder] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBrand]'
GO
ALTER TABLE [lekmer].[tBrand] ADD
[MaxQuantityPerOrder] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO




ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	[product].[fProductHasSizes](ProductId) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations',
	[Weight] AS 'Lekmer.Weight',
	[ProductTypeId] AS 'Lekmer.ProductTypeId',
	[IsAbove60L] AS 'Lekmer.IsAbove60L',
	[StockStatusId] AS 'Lekmer.StockStatusId',
	[MonitorThreshold] AS 'Lekmer.MonitorThreshold',
	[MaxQuantityPerOrder] AS 'Lekmer.MaxQuantityPerOrder'
FROM
	lekmer.tLekmerProduct AS p



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProduct]'
GO



ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		[lp].[Lekmer.MonitorThreshold],
		[lp].[Lekmer.MaxQuantityPerOrder],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrand]'
GO


ALTER VIEW [lekmer].[vBrand]
AS
	SELECT     
		[B].[BrandId] 'Brand.BrandId',
		COALESCE ([bt].[BrandTranslation.Title], [B].[Title]) 'Brand.Title',
		COALESCE ([bt].[BrandTranslation.Description], [B].[Description]) 'Brand.Description',		
		COALESCE ([bt].[BrandTranslation.PackageInfo], [B].[PackageInfo]) 'Brand.PackageInfo',		
		[B].[ExternalUrl] 'Brand.ExternalUrl',
		[B].[MediaId] 'Brand.MediaId',
		[B].[ErpId] 'Brand.ErpId',
		[B].[MonitorThreshold] 'Brand.MonitorThreshold',
		[B].[MaxQuantityPerOrder] 'Brand.MaxQuantityPerOrder',
		[Ch].[ChannelId],
		[I].*,
		[Bssr].[ContentNodeId] 'Brand.ContentNodeId'
	FROM
		lekmer.tBrand AS B
		CROSS JOIN core.tChannel AS Ch
		
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = B.MediaId AND I.[Image.LanguageId] = Ch.LanguageId
		LEFT JOIN lekmer.vBrandTranslation AS bt ON bt.[BrandTranslation.BrandId] = B.BrandId AND bt.[BrandTranslation.LanguageId] = Ch.LanguageId
		LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = Ch.ChannelId
		LEFT JOIN lekmer.tBrandSiteStructureRegistry Bssr ON Bssr.BrandId = B.BrandId AND Bssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategory]'
GO



ALTER VIEW [product].[vCustomCategory]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		COALESCE ([lct].[PackageInfo], [l].[PackageInfo]) AS [LekmerCategory.PackageInfo],
		[l].[MonitorThreshold] AS [LekmerCategory.MonitorThreshold],
		[l].[MaxQuantityPerOrder] AS [LekmerCategory.MaxQuantityPerOrder]
	FROM
		[product].[vCategory] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]
		LEFT JOIN [productlek].[tLekmerCategoryTranslation] lct ON [lct].[CategoryId] = [c].[Category.Id] AND [lct].[LanguageId] = [c].[Category.LanguageId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO



ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.IsAbove60L],
		[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId',
		NULL AS 'Lekmer.RecommendedPrice'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrandSecure]'
GO


ALTER VIEW [lekmer].[vBrandSecure]
AS
SELECT     
	[B].[BrandId] 'Brand.BrandId',
    [B].[Title] 'Brand.Title',
    [B].[Description] 'Brand.Description',
    [B].[PackageInfo] 'Brand.PackageInfo',
    [B].[ExternalUrl] 'Brand.ExternalUrl',
    [B].[MediaId] 'Brand.MediaId',
    [B].[ErpId] 'Brand.ErpId',
	[B].[MonitorThreshold] 'Brand.MonitorThreshold',
	[B].[MaxQuantityPerOrder] 'Brand.MaxQuantityPerOrder',
    [I].*,
    NULL 'Brand.ContentNodeId'
FROM
	lekmer.tBrand B
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = B.MediaId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductRecord]'
GO



ALTER VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder]
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategorySecure]'
GO



ALTER VIEW [product].[vCustomCategorySecure]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		[l].[PackageInfo] AS [LekmerCategory.PackageInfo],
		[l].[MonitorThreshold] AS [LekmerCategory.MonitorThreshold],
		[l].[MaxQuantityPerOrder] AS [LekmerCategory.MaxQuantityPerOrder]
	FROM
		[product].[vCategorySecure] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductView]'
GO



ALTER VIEW [product].[vCustomProductView]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductViewWithoutStatusFilter]'
GO



ALTER VIEW [lekmer].[vCustomProductViewWithoutStatusFilter]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductViewWithoutStatusFilter] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSave]'
GO

ALTER PROCEDURE [lekmer].[pProductSave]
	@ProductId				INT,
	@ItemsInPackage			INT,
	@ErpId					VARCHAR(50),
	@EANCode				VARCHAR(20),
	@Title					NVARCHAR(256),
	@WebShopTitle			NVARCHAR(256),
	@ProductStatusId		INT,
	@Description			NVARCHAR(MAX),
	@ShortDescription		NVARCHAR(MAX),
	@CategoryId				INT,
	@MediaId				INT,
	@BrandId				INT,
	@AgeFromMonth			INT,
	@AgeToMonth				INT,
	@IsBookable				BIT,
	@IsNewFrom				DATETIME,
	@IsNewTo				DATETIME,
	@Measurement			NVARCHAR(MAX),
	@BatteryTypeId			INT,
	@NumberOfBatteries		INT,
	@IsBatteryIncluded		BIT,
	@ExpectedBackInStock	DATETIME,
	@CreatedDate			DATETIME,
	@SizeDeviationId		INT,
	@ShowVariantRelations	BIT,
	@MonitorThreshold		INT,
	@MaxQuantityPerOrder	INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tProduct]
	SET
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	WHERE
		[ProductId] = @ProductId
		
	UPDATE
		[lekmer].[tLekmerProduct]
	SET
		[BrandId]				= @BrandId,
		[AgeFromMonth]			= @AgeFromMonth,
		[AgeToMonth]			= @AgeToMonth,
		[IsBookable]			= @IsBookable,
		[IsNewFrom]				= @IsNewFrom,
		[IsNewTo]				= @IsNewTo,
		[Measurement]			= @Measurement,
		[BatteryTypeId]			= @BatteryTypeId,
		[NumberOfBatteries]		= @NumberOfBatteries,
		[IsBatteryIncluded]		= @IsBatteryIncluded,
		[ExpectedBackInStock]	= @ExpectedBackInStock,
		[CreatedDate]			= @CreatedDate,
		[SizeDeviationId]		= @SizeDeviationId,
		[ShowVariantRelations]	= @ShowVariantRelations,
		[MonitorThreshold]		= @MonitorThreshold,
		[MaxQuantityPerOrder]	= @MaxQuantityPerOrder
	WHERE
		[ProductId] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandSave]'
GO
ALTER PROCEDURE [lekmer].[pBrandSave]
	@BrandId				INT,
	@Title					VARCHAR(500),
	@MediaId				INT = NULL,
	@Description 			VARCHAR(MAX) = NULL,
	@ExternalUrl 			VARCHAR(500) = NULL,
	@PackageInfo 			NVARCHAR(MAX) = NULL,
	@MonitorThreshold		INT = NULL,
	@MaxQuantityPerOrder	INT = NULL
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM lekmer.tBrand WHERE Title = @Title AND BrandId <> @BrandId)
		RETURN -1

	UPDATE
		[lekmer].[tBrand]
	SET
		[Title] = @Title,	
		[MediaId] = @MediaId,
		[Description] = @Description,
		[ExternalUrl] = @ExternalUrl,
		[PackageInfo] = @PackageInfo,
		[MonitorThreshold] = @MonitorThreshold,
		[MaxQuantityPerOrder] = @MaxQuantityPerOrder
	WHERE
		[BrandId] = @BrandId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tBrand]
		( 
			[Title],
			[MediaId],
			[Description],
			[ExternalUrl],
			[PackageInfo],
			[MonitorThreshold],
			[MaxQuantityPerOrder]
		)
		VALUES
		(
			@Title,
			@MediaId ,
			@Description,
			@ExternalUrl,
			@PackageInfo,
			@MonitorThreshold,
			@MaxQuantityPerOrder
		)
		SET @BrandId = scope_identity()						
	END 
	
	RETURN @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductWithoutStatusFilter]'
GO


ALTER VIEW [lekmer].[vCustomProductWithoutStatusFilter]
AS 
	SELECT
		p.*,
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.[ParentContentNodeId] AS 'Product.ParentContentNodeId',
		i.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductWithoutStatusFilter] p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] AS ssmc ON ssmc.[ChannelId] = c.[Channel.Id]
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON p.[Product.Id] = pssr.[ProductId] AND ssmc.[SiteStructureRegistryId] = pssr.[SiteStructureRegistryId]
		LEFT JOIN [media].[vCustomImage] AS i ON i.[Image.MediaId] = p.[Product.MediaId] AND i.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCategorySave]'
GO
ALTER PROCEDURE [lekmer].[pCategorySave]
	@CategoryId					INT,
	@Title						NVARCHAR(50),
	@AllowMultipleSizesPurchase	BIT,
	@PackageInfo				NVARCHAR(MAX),
	@MonitorThreshold			INT,
	@MaxQuantityPerOrder		INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tCategory]
	SET
		[Title] = @Title
	WHERE
		[CategoryId] = @CategoryId
	
	SET NOCOUNT OFF
		
	UPDATE
		[lekmer].[tLekmerCategory]
	SET
		[AllowMultipleSizesPurchase] = @AllowMultipleSizesPurchase,
		[PackageInfo] = @PackageInfo,
		[MonitorThreshold] = @MonitorThreshold,
		[MaxQuantityPerOrder] = @MaxQuantityPerOrder
	WHERE
		[CategoryId] = @CategoryId
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			[lekmer].[tLekmerCategory]
		( 
			[CategoryId],
			[AllowMultipleSizesPurchase],
			[PackageInfo],
			[MonitorThreshold],
			[MaxQuantityPerOrder]
		)
		VALUES 
		(
			@CategoryId,
			@AllowMultipleSizesPurchase,
			@PackageInfo,
			@MonitorThreshold,
			@MaxQuantityPerOrder
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]'
GO
ALTER PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	SELECT
		[p].[ProductId] 'Product.Id', 
		[p].[ItemsInPackage] 'Product.ItemsInPackage',
		[p].[ErpId] 'Product.ErpId',
		[p].[EanCode] 'Product.EanCode',
		[p].[NumberInStock] 'Product.NumberInStock', 
		[p].[CategoryId] 'Product.CategoryId',
		COALESCE([pt].[WebShopTitle], [p].WebShopTitle) 'Product.WebShopTitle',
		COALESCE([pt].[Title], [p].Title) 'Product.Title',
		[p].[ProductStatusId] 'Product.ProductStatusId',
		COALESCE([pt].[Description], [p].[Description]) 'Product.Description',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription]) 'Product.ShortDescription',
		@ChannelId 'Product.ChannelId', 
		@CurrencyId 'Product.CurrencyId',
		@PriceListRegistryId 'Product.PriceListRegistryId',
		[i].*,
		[pssr].[ParentContentNodeId] 'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId] 'Product.TemplateContentNodeId',
		[lp].[Lekmer.BrandId],
		[lp].[Lekmer.IsBookable],
		[lp].[Lekmer.AgeFromMonth],
		[lp].[Lekmer.AgeToMonth],
		[lp].[Lekmer.IsNewFrom],
		[lp].[Lekmer.IsNewTo],
		COALESCE ([lpt].[Product.Measurement], [lp].[Lekmer.Measurement]) 'Lekmer.Measurement',
		[lp].[Lekmer.BatteryTypeId],
		[lp].[Lekmer.NumberOfBatteries],
		[lp].[Lekmer.IsBatteryIncluded],
		[lp].[Lekmer.ExpectedBackInStock],
		[lp].[Lekmer.CreatedDate],
		[lp].[Lekmer.SizeDeviationId],
		[lp].[Lekmer.HasSizes],
		[lp].[Lekmer.LekmerErpId],
		[lp].[Lekmer.ShowVariantRelations],
		[lp].[Lekmer.Weight],
		[lp].[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		[lp].[Lekmer.MonitorThreshold],
		[lp].[Lekmer.MaxQuantityPerOrder],
		[bt].*,
		[sd].*,
		[pu].[ProductUrl.UrlTitle] 'Lekmer.UrlTitle',
		[rp].[Price] 'Lekmer.RecommendedPrice',
		[pli].*
	FROM
		[product].[tProduct] AS p
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON [lpt].[Product.Id] = [lp].[Lekmer.ProductId] AND [lpt].[Language.Id] = @LanguageId
		LEFT JOIN [lekmer].[vBatteryType] bt ON [bt].[BatteryType.Id] = [lp].[Lekmer.BatteryTypeId]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON [sd].[SizeDeviation.Id] = [lp].[Lekmer.SizeDeviationId]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl	ON [pl].[Id] = [p].[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
