SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [productlek].[pPackageSave]'
GO
ALTER PROCEDURE [productlek].[pPackageSave]
	@PackageId			INT,
	@MasterProductId	INT,
	@ErpId				VARCHAR(50) = NULL,
	@StatusId			INT,
	@StockStatusId		INT,
	@Title				NVARCHAR(256),
	@WebShopTitle		NVARCHAR(256),
	@NumberInStock		INT,
	@CategoryId			INT,
	@Description		NVARCHAR(MAX),
	@GeneralInfo		NVARCHAR(MAX),
	@PriceXml			XML = NULL
	/*
	'<prices>
		<price priceListId="1" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="2" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="3" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="4" priceIncludingVat="10.99" vat="25.00"/>
	</prices>'
	*/
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	BEGIN
		BEGIN TRANSACTION
			IF @ErpId IS NULL
			BEGIN
				SET @ErpId = (SELECT TOP (1) [pr].[ErpId] 
						  FROM [productlek].[tPackage] p
								INNER JOIN [product].[tProduct] pr ON [pr].[ProductId] = [p].[MasterProductId]
						  ORDER BY [PackageId] DESC)

				IF @ErpId IS NULL
				BEGIN
					SET @ErpId = 'P000001-0000'
				END
				ELSE
				BEGIN
					DECLARE @ErpTemp INT
					SET @ErpTemp = (SELECT CAST(SUBSTRING(@ErpId, 2, 6) AS INT) + 1)

					SET @ErpId = (SELECT 'P' + RIGHT('000000' + CAST(@ErpTemp AS VARCHAR(6)), 6) + '-0000')
				END
			END
			
			-- tProduct
			INSERT INTO [product].[tProduct] (
				[ErpId],
				[IsDeleted],
				[NumberInStock],
				[CategoryId],
				[Title],
				[WebShopTitle],
				[Description],
				[ProductStatusId]
			)
			VALUES (
				@ErpId,
				0, -- IsDeleted
				@NumberInStock,
				@CategoryId,
				@Title,
				@WebShopTitle,
				@Description,
				@StatusId
			)

			SET @MasterProductId = SCOPE_IDENTITY()

			-- tLekmerProduct
			INSERT INTO [lekmer].[tLekmerProduct] (
				[ProductId],
				[IsBookable],
				[AgeFromMonth],
				[AgeToMonth],
				[IsBatteryIncluded],
				[HYErpId],
				[ShowVariantRelations],
				[ProductTypeId],
				[StockStatusId]
			)
			VALUES (
				@MasterProductId,
				0, -- IsBookable
				0, -- AgeFromMonth
				0, -- AgeToMonth
				0, -- IsBatteryIncluded
				@ErpId,
				0, -- ShowVariantRelations
				2, -- ProductTypeId = Package
				@StockStatusId
			)
			
			-- tProductRegistryProduct
			INSERT INTO [product].[tProductRegistryProduct] (
				[ProductId],
				[ProductRegistryId]
			)
			SELECT
				@MasterProductId,
				[pr].[ProductRegistryId]
			FROM  [product].[tProductRegistry] pr
			WHERE NOT EXISTS (SELECT 1 FROM [product].[tProductRegistryProduct] prp
							  WHERE [prp].[ProductId] = @MasterProductId
									AND [prp].[ProductRegistryId] = [pr].[ProductRegistryId])

			-- tPackage
			INSERT INTO [productlek].[tPackage] ([MasterProductId], [GeneralInfo]) SELECT @MasterProductId, @GeneralInfo
			
			SET @PackageId = CAST(SCOPE_IDENTITY() AS INT)
		COMMIT
	END
	ELSE
	BEGIN
		-- tProduct
		UPDATE [product].[tProduct]
		SET [NumberInStock] = @NumberInStock,
			[CategoryId] = @CategoryId,
			[ProductStatusId] = @StatusId,
			[Title] = @Title,
			[WebShopTitle] = @WebShopTitle,
			[Description] = @Description
		WHERE [ProductId] = @MasterProductId
		
		UPDATE [lekmer].[tLekmerProduct]
		SET  [StockStatusId] = @StockStatusId
		WHERE [ProductId] = @MasterProductId
		
		UPDATE [productlek].[tPackage]
		SET [GeneralInfo] = @GeneralInfo
		WHERE [PackageId] = @PackageId
	END
	
	-- tPriceListItem
	DELETE FROM [product].[tPriceListItem] WHERE [ProductId] = @MasterProductId
	
	IF @PriceXml IS NOT NULL
	BEGIN
		INSERT INTO [product].[tPriceListItem] (
				[PriceListId],
				[ProductId],
				[PriceIncludingVat],
				[PriceExcludingVat],
				[VatPercentage]
			)
		SELECT
			c.value('@priceListId[1]', 'int'),
			@MasterProductId,
			c.value('@priceIncludingVat[1]', 'decimal(16,2)'),
			c.value('@priceIncludingVat[1]', 'decimal(16,2)') / (1.0+c.value('@vat[1]', 'decimal(16,2)')/100.0),
			c.value('@vat[1]', 'decimal(16,2)')
		FROM
			@PriceXml.nodes('/prices/price') T(c)
	END
	
	SELECT @PackageId, @MasterProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
