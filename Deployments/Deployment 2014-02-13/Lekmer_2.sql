/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 2/13/2014 11:52:10 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraint FK_tModelFragmentEntity_tModelFragment from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraint FK_tModelFragmentFunction_tModelFragment from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]

-- Drop constraint FK_tModelSetting_tModel from [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModel]

-- Update rows in [template].[tModelFragment]
UPDATE [template].[tModelFragment] SET [Ordinal]=50 WHERE [ModelFragmentId]=110
UPDATE [template].[tModelFragment] SET [Ordinal]=60 WHERE [ModelFragmentId]=111
UPDATE [template].[tModelFragment] SET [Ordinal]=70 WHERE [ModelFragmentId]=112
UPDATE [template].[tModelFragment] SET [Ordinal]=80 WHERE [ModelFragmentId]=113
UPDATE [template].[tModelFragment] SET [Size]=10 WHERE [ModelFragmentId]=1001064
UPDATE [template].[tModelFragment] SET [Size]=20 WHERE [ModelFragmentId]=1001065
UPDATE [template].[tModelFragment] SET [Size]=5 WHERE [ModelFragmentId]=1001066
UPDATE [template].[tModelFragment] SET [Size]=5 WHERE [ModelFragmentId]=1001067
UPDATE [template].[tModelFragment] SET [Size]=5 WHERE [ModelFragmentId]=1001068
UPDATE [template].[tModelFragment] SET [Ordinal]=10, [Size]=10 WHERE [ModelFragmentId]=1001069
-- Operation applied to 10 rows out of 10

-- Update row in [template].[tModelFragmentRegion]
UPDATE [template].[tModelFragmentRegion] SET [Ordinal]=60 WHERE [ModelFragmentRegionId]=1000216

-- Update rows in [template].[tModel]
UPDATE [template].[tModel] SET [ModelFolderId]=1000005 WHERE [ModelId]=1000006
UPDATE [template].[tModel] SET [ModelFolderId]=1000005 WHERE [ModelId]=1000008
UPDATE [template].[tModel] SET [ModelFolderId]=1000005 WHERE [ModelId]=1000051
UPDATE [template].[tModel] SET [ModelFolderId]=1000005 WHERE [ModelId]=1000052
UPDATE [template].[tModel] SET [ModelFolderId]=1000005 WHERE [ModelId]=1000053
UPDATE [template].[tModel] SET [ModelFolderId]=1000005 WHERE [ModelId]=1000060
UPDATE [template].[tModel] SET [ModelFolderId]=1000005 WHERE [ModelId]=1000063
UPDATE [template].[tModel] SET [ModelFolderId]=1000005 WHERE [ModelId]=1000064
-- Operation applied to 8 rows out of 8

-- Add row to [template].[tModelFolder]
SET IDENTITY_INSERT [template].[tModelFolder] ON
INSERT INTO [template].[tModelFolder] ([ModelFolderId], [Title]) VALUES (1000005, N'Payment')
SET IDENTITY_INSERT [template].[tModelFolder] OFF

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraint FK_tModelFragmentEntity_tModelFragment to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] WITH CHECK ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraint FK_tModelFragmentFunction_tModelFragment to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] WITH CHECK ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])

-- Add constraint FK_tModelSetting_tModel to [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] WITH CHECK ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
COMMIT TRANSACTION
GO
