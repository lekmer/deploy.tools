/*
Run this script on:

        (local).Heppo_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 004\DB\HeppoDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 27.03.2012 9:39:58

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping extended properties'
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'order', 'VIEW', N'vCustomOrder', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'order', 'VIEW', N'vCustomOrder', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [order].[tChannelPaymentType]'
GO
ALTER TABLE [order].[tChannelPaymentType] DROP
CONSTRAINT [FK_tChannelPaymentType_tPaymentType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[integrationLog]'
GO
ALTER TABLE [integration].[integrationLog] DROP CONSTRAINT [PK__integrat__3214EC0737088A03]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[OrderLog]'
GO
ALTER TABLE [integration].[OrderLog] DROP CONSTRAINT [PK__OrderLog__3214EC073337F91F]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tAccessoriesSize]'
GO
ALTER TABLE [integration].[tAccessoriesSize] DROP CONSTRAINT [PK__tAccesso__B0A650D35D633D15]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tBrandCreation]'
GO
ALTER TABLE [integration].[tBrandCreation] DROP CONSTRAINT [PK__tBrandCr__DAD4F05E102EE5CE]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tDealOfTheDay]'
GO
ALTER TABLE [integration].[tDealOfTheDay] DROP CONSTRAINT [PK__tDealOfT__47CE2B703503A526]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tempProduct]'
GO
ALTER TABLE [integration].[tempProduct] DROP CONSTRAINT [PK__tempProd__ED0627FB3F9DD004]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tempProductPrice]'
GO
ALTER TABLE [integration].[tempProductPrice] DROP CONSTRAINT [PK__tempProd__ED0627FB3AD91AE7]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tempProductStockPosition]'
GO
ALTER TABLE [integration].[tempProductStockPosition] DROP CONSTRAINT [PK__tempProd__3FA33FFA2DA90702]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tOrderLog]'
GO
ALTER TABLE [integration].[tOrderLog] DROP CONSTRAINT [PK__tOrderLo__3214EC075358458B]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tOrderQue]'
GO
ALTER TABLE [integration].[tOrderQue] DROP CONSTRAINT [PK__tOrderQu__C3905BCF5AF96753]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tProductOutOfStock]'
GO
ALTER TABLE [integration].[tProductOutOfStock] DROP CONSTRAINT [PK__tProduct__0C37165A4EC11524]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tProductStockPosition]'
GO
ALTER TABLE [integration].[tProductStockPosition] DROP CONSTRAINT [PK__tProduct__3FA33FFA485CFD3E]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tOrderItem_OrderId] from [order].[tOrderItem]'
GO
DROP INDEX [IX_tOrderItem_OrderId] ON [order].[tOrderItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockProductSimilarList]'
GO
ALTER TABLE [lekmer].[tBlockProductSimilarList] ADD
[PriceRangeType] [int] NOT NULL CONSTRAINT [DF_tBlockProductSimilarList_PriceType] DEFAULT ((1))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockProductSimilarListSave]'
GO
ALTER PROCEDURE [lekmer].[pBlockProductSimilarListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@TotalProductCount INT,
	@PriceRangeType INT
AS
BEGIN
	UPDATE [lekmer].[tBlockProductSimilarList]
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount,
		[TotalProductCount] = @TotalProductCount,
		[PriceRangeType] = @PriceRangeType
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockProductSimilarList] (
		[BlockId],
		[ColumnCount],
		[RowCount],
		[TotalProductCount],
		[PriceRangeType]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@TotalProductCount,
		@PriceRangeType
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAllByBlockAndProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductGetIdAllByBlockAndProduct]
		@ChannelId		INT,
		@CustomerId		INT,
		@BlockId		INT,
		@ProductId		INT,
		@ShowVariants	BIT,
		@Page			INT = NULL,
		@PageSize		INT,
		@SortBy			VARCHAR(50) = NULL,
		@SortDescending BIT = NULL
AS
BEGIN
	DECLARE @RelationListVariantTypeId INT
	SET @RelationListVariantTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'Variant')
		
	DECLARE @maxCount CHAR(4)
	SET @maxCount = '2000'

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @CustomerIdString VARCHAR(10)
	
	IF (@CustomerId IS NULL)
		SET @CustomerIdString = 'null'
	ELSE
		SET @CustomerIdString = CAST(@CustomerId AS VARCHAR(10))
		
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sql = '
	declare @tResult table (ProductId int, SortBy nvarchar(max))	
	insert @tResult	(ProductId, SortBy)
	select distinct TOP ' + @maxCount + ' p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	from
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN [product].[tBlockProductRelationListItem] bprli ON rl.RelationListTypeId = bprli.RelationListTypeId
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				' + @CustomerIdString + '
			)
	WHERE
		prl.ProductId = ' + cast(@ProductId as varchar) + '
		AND rlp.ProductId <> ' + cast(@ProductId as varchar) + '
		' + CASE WHEN (@ShowVariants = 0) 
				THEN ' AND rl.RelationListTypeId <> ' + cast(@RelationListVariantTypeId as varchar) + ' '
				ELSE ' '
			END + '
		AND p.[Product.ChannelId] = ' + cast(@ChannelId as varchar) + '
		AND bprli.BlockId = ' + cast(@BlockId as varchar) + '

	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + CAST(@Page * @PageSize AS varchar(10)) + ' *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
		AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + @maxCount + ' ProductId FROM @tResult AS SearchResult'
	END
	
	EXEC (@sql)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockProductSimilarList]'
GO


ALTER VIEW [lekmer].[vBlockProductSimilarList]
AS
	SELECT
		[BlockId] as 'BlockProductSimilarList.BlockId',
		[ColumnCount] as 'BlockProductSimilarList.ColumnCount',
		[RowCount] as 'BlockProductSimilarList.RowCount',
		[TotalProductCount] as 'BlockProductSimilarList.TotalProductCount',
		[PriceRangeType] as 'BlockProductSimilarList.PriceRangeType'
	FROM
		[lekmer].[tBlockProductSimilarList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductUrlHistorySave]'
GO

ALTER PROCEDURE [lekmer].[pProductUrlHistorySave]
	@ProductId					INT,
	@LanguageId					INT,
	@UrlTitleOld				NVARCHAR(256),
	@HistoryLifeIntervalTypeId	INT
AS 
BEGIN 
	INSERT INTO lekmer.tProductUrlHistory (
		ProductId,
		LanguageId,
		UrlTitleOld,
		HistoryLifeIntervalTypeId
	)
	VALUES (
		@ProductId,
		@LanguageId,
		@UrlTitleOld,
		@HistoryLifeIntervalTypeId
	)
	
	RETURN @@IDENTITY
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrder]'
GO

ALTER VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		lo.[PaymentCost] AS [Lekmer.PaymentCost],
		lo.[VoucherDiscount] AS [Lekmer.VoucherDiscount],
		lo.[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pVoucherDiscountActionGetById]'
GO
ALTER PROCEDURE [lekmer].[pVoucherDiscountActionGetById]
	@ActionId int
AS 
BEGIN 
	SELECT
		CCA.*,
		VA.*
	FROM
		campaign.vCustomCartAction as CCA
		left join lekmer.tVoucherAction VA on VA.CartActionId=CCA.[CartAction.Id]
	WHERE
		[CartAction.Id] = @ActionId
END 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrand]'
GO

ALTER VIEW [lekmer].[vBrand]
AS
	SELECT     
		B.BrandId AS 'Brand.BrandId',
		COALESCE (bt.[BrandTranslation.Title], B.Title) AS 'Brand.Title',
		COALESCE (bt.[BrandTranslation.Description], B.[Description]) AS 'Brand.Description',
		B.ExternalUrl AS 'Brand.ExternalUrl',
		B.MediaId AS 'Brand.MediaId',
		B.ErpId AS 'Brand.ErpId',
		Ch.ChannelId,
		I.*,
		Bssr.ContentNodeId AS 'Brand.ContentNodeId'
	FROM
		lekmer.tBrand AS B
		CROSS JOIN core.tChannel AS Ch
		
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = B.MediaId AND I.[Image.LanguageId] = Ch.LanguageId
		LEFT JOIN lekmer.vBrandTranslation AS bt ON bt.[BrandTranslation.BrandId] = B.BrandId AND bt.[BrandTranslation.LanguageId] = Ch.LanguageId
		LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = Ch.ChannelId
		LEFT JOIN lekmer.tBrandSiteStructureRegistry Bssr ON Bssr.BrandId = B.BrandId AND Bssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_integrationLog] on [integration].[integrationLog]'
GO
ALTER TABLE [integration].[integrationLog] ADD CONSTRAINT [PK_integrationLog] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_OrderLog] on [integration].[OrderLog]'
GO
ALTER TABLE [integration].[OrderLog] ADD CONSTRAINT [PK_OrderLog] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAccessoriesSize] on [integration].[tAccessoriesSize]'
GO
ALTER TABLE [integration].[tAccessoriesSize] ADD CONSTRAINT [PK_tAccessoriesSize] PRIMARY KEY CLUSTERED  ([AccessoriesSizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBrandCreation] on [integration].[tBrandCreation]'
GO
ALTER TABLE [integration].[tBrandCreation] ADD CONSTRAINT [PK_tBrandCreation] PRIMARY KEY CLUSTERED  ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tDealOfTheDay] on [integration].[tDealOfTheDay]'
GO
ALTER TABLE [integration].[tDealOfTheDay] ADD CONSTRAINT [PK_tDealOfTheDay] PRIMARY KEY CLUSTERED  ([BlockId], [ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tempProduct] on [integration].[tempProduct]'
GO
ALTER TABLE [integration].[tempProduct] ADD CONSTRAINT [PK_tempProduct] PRIMARY KEY CLUSTERED  ([HYarticleId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tempProductPrice] on [integration].[tempProductPrice]'
GO
ALTER TABLE [integration].[tempProductPrice] ADD CONSTRAINT [PK_tempProductPrice] PRIMARY KEY CLUSTERED  ([HYarticleId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tempProductStockPosition] on [integration].[tempProductStockPosition]'
GO
ALTER TABLE [integration].[tempProductStockPosition] ADD CONSTRAINT [PK_tempProductStockPosition] PRIMARY KEY CLUSTERED  ([HYarticleId], [Lagerplats])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tOrderLog] on [integration].[tOrderLog]'
GO
ALTER TABLE [integration].[tOrderLog] ADD CONSTRAINT [PK_tOrderLog] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tOrderQue] on [integration].[tOrderQue]'
GO
ALTER TABLE [integration].[tOrderQue] ADD CONSTRAINT [PK_tOrderQue] PRIMARY KEY CLUSTERED  ([OrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductOutOfStock] on [integration].[tProductOutOfStock]'
GO
ALTER TABLE [integration].[tProductOutOfStock] ADD CONSTRAINT [PK_tProductOutOfStock] PRIMARY KEY CLUSTERED  ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductStockPosition] on [integration].[tProductStockPosition]'
GO
ALTER TABLE [integration].[tProductStockPosition] ADD CONSTRAINT [PK_tProductStockPosition] PRIMARY KEY CLUSTERED  ([HYarticleId], [Lagerplats])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tImageRotatorGroup_BlockId_Ordinal] on [lekmer].[tImageRotatorGroup]'
GO
CREATE NONCLUSTERED INDEX [IX_tImageRotatorGroup_BlockId_Ordinal] ON [lekmer].[tImageRotatorGroup] ([BlockId], [Ordinal])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrder_CreatedDate] on [order].[tOrder]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrder_CreatedDate] ON [order].[tOrder] ([CreatedDate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrderItem_OrderId_(Quantity)] on [order].[tOrderItem]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrderItem_OrderId_(Quantity)] ON [order].[tOrderItem] ([OrderId]) INCLUDE ([Quantity])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrderItemProduct_ProductId] on [order].[tOrderItemProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrderItemProduct_ProductId] ON [order].[tOrderItemProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [order].[tChannelPaymentType]'
GO
ALTER TABLE [order].[tChannelPaymentType] ADD
CONSTRAINT [FK_tChannelPaymentType_tPaymentType] FOREIGN KEY ([PaymentTypeId]) REFERENCES [order].[tPaymentType] ([PaymentTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
