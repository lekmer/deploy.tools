/*
Run this script on:

        (local).Heppo_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 004\DB\HeppoDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 27.03.2012 9:30:26

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [lekmer].[tBlockBrandTopList]'
GO
CREATE TABLE [lekmer].[tBlockBrandTopList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[TotalBrandCount] [int] NOT NULL,
[IncludeAllCategories] [bit] NOT NULL,
[OrderStatisticsDayCount] [int] NOT NULL,
[LinkContentNodeId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockBrandTopList] on [lekmer].[tBlockBrandTopList]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopList] ADD CONSTRAINT [PK_tBlockBrandTopList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tBlockBrandTopListBrand]'
GO
CREATE TABLE [lekmer].[tBlockBrandTopListBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[Position] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockBrandTopListBrand] on [lekmer].[tBlockBrandTopListBrand]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD CONSTRAINT [PK_tBlockBrandTopListBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tBlockBrandTopListCategory]'
GO
CREATE TABLE [lekmer].[tBlockBrandTopListCategory]
(
[BlockId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockBrandTopListCategory] on [lekmer].[tBlockBrandTopListCategory]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListCategory] ADD CONSTRAINT [PK_tBlockBrandTopListCategory] PRIMARY KEY CLUSTERED  ([BlockId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListBrandDeleteAll]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandTopListBrand]
	WHERE  [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListBrandSave]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandSave]
	@BlockId INT,
	@BrandId INT,
	@Position INT
AS
BEGIN
	UPDATE [lekmer].[tBlockBrandTopListBrand]
	SET
		[Position] = @Position
	WHERE
		[BlockId] = @BlockId
		AND [BrandId] = @BrandId
		
	IF @@ROWCOUNT <> 0 RETURN
		
	INSERT [lekmer].[tBlockBrandTopListBrand] (
		[BlockId],
		[BrandId],
		[Position]
	)
	VALUES (
		@BlockId,
		@BrandId,
		@Position
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListCategoryDeleteAll]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategoryDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandTopListCategory]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListCategorySave]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategorySave]
	@BlockId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	UPDATE [lekmer].[tBlockBrandTopListCategory]
	SET
		[IncludeSubcategories] = @IncludeSubcategories
	WHERE
		[BlockId] = @BlockId
		AND [CategoryId] = @CategoryId
		
	IF @@ROWCOUNT <> 0 RETURN
		
	INSERT [lekmer].[tBlockBrandTopListCategory] (
		[BlockId],
		[CategoryId],
		[IncludeSubcategories]
	)
	VALUES (
		@BlockId,
		@CategoryId,
		@IncludeSubcategories
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListDelete]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListDelete]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandTopListCategory]
	WHERE  [BlockId] = @BlockId

	DELETE [lekmer].[tBlockBrandTopListBrand]
	WHERE  [BlockId] = @BlockId

	DELETE [lekmer].[tBlockBrandTopList]
	WHERE  [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopList]'
GO




CREATE VIEW [lekmer].[vBlockBrandTopList]
AS
	SELECT
		b.[BlockId] AS 'BlockBrandTopList.BlockId',
		b.[ColumnCount] AS 'BlockBrandTopList.ColumnCount',
		b.[RowCount] AS 'BlockBrandTopList.RowCount',
		b.[TotalBrandCount] AS 'BlockBrandTopList.TotalBrandCount',
		b.[IncludeAllCategories] AS 'BlockBrandTopList.IncludeAllCategories',
		b.[OrderStatisticsDayCount] AS 'BlockBrandTopList.OrderStatisticsDayCount',
		b.[LinkContentNodeId] AS 'BlockBrandTopList.LinkContentNodeId'
	FROM
		[lekmer].[tBlockBrandTopList] b
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListSave]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@TotalBrandCount INT,
	@IncludeAllCategories BIT,
	@OrderStatisticsDayCount INT,
	@LinkContentNodeId INT
AS
BEGIN
	UPDATE lekmer.tBlockBrandTopList
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount,
		[TotalBrandCount] = @TotalBrandCount,
		[IncludeAllCategories] = @IncludeAllCategories,
		[OrderStatisticsDayCount] = @OrderStatisticsDayCount,
		[LinkContentNodeId] = @LinkContentNodeId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0 RETURN
		
	INSERT lekmer.tBlockBrandTopList (
		[BlockId],
		[ColumnCount],
		[RowCount],
		[TotalBrandCount],
		[IncludeAllCategories],
		[OrderStatisticsDayCount],
		[LinkContentNodeId]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@TotalBrandCount,
		@IncludeAllCategories,
		@OrderStatisticsDayCount,
		@LinkContentNodeId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopListCategory]'
GO

CREATE VIEW [lekmer].[vBlockBrandTopListCategory]
AS
	SELECT
		b.[BlockId] AS 'BlockBrandTopListCategory.BlockId',
		b.[CategoryId] AS 'BlockBrandTopListCategory.CategoryId',
		b.[IncludeSubcategories] AS 'BlockBrandTopListCategory.IncludeSubcategories',
		c.*
	FROM
		[lekmer].[tBlockBrandTopListCategory] b
		INNER JOIN [product].[vCustomCategory] c ON b.[CategoryId] = c.[Category.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopListCategorySecure]'
GO

CREATE VIEW [lekmer].[vBlockBrandTopListCategorySecure]
AS
	SELECT
		b.[BlockId]	AS 'BlockBrandTopListCategory.BlockId',
		b.[CategoryId] AS 'BlockBrandTopListCategory.CategoryId',
		b.[IncludeSubcategories] AS 'BlockBrandTopListCategory.IncludeSubcategories',
		c.*
	FROM
		[lekmer].[tBlockBrandTopListCategory] b
		INNER JOIN [product].[vCustomCategorySecure] c on b.[CategoryId] = c.[Category.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListCategoryGetAllByBlockSecure]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategoryGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vBlockBrandTopListCategorySecure]
	WHERE
		[BlockBrandTopListCategory.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListGetById]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListGetById]
	@LanguageId INT,
	@BlockId INT
AS
BEGIN
	SELECT 
		btl.*,
		b.*
	FROM 
		[lekmer].[vBlockBrandTopList] AS btl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON btl.[BlockBrandTopList.BlockId] = b.[Block.BlockId]
	WHERE
		btl.[BlockBrandTopList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListGetByIdSecure]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		btl.*,
		b.*
	FROM 
		[lekmer].[vBlockBrandTopList] AS btl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON btl.[BlockBrandTopList.BlockId] = b.[Block.BlockId]
	WHERE
		btl.[BlockBrandTopList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopListBrand]'
GO



CREATE VIEW [lekmer].[vBlockBrandTopListBrand]
AS
	SELECT
		btlb.[BlockId] AS 'BlockBrandTopListBrand.BlockId',
		btlb.[BrandId] AS 'BlockBrandTopListBrand.BrandId',
		btlb.[Position] AS 'BlockBrandTopListBrand.Position',
		b.*
	FROM
		[lekmer].[tBlockBrandTopListBrand] btlb
		INNER JOIN [lekmer].[vBrand] b on b.[Brand.BrandId] = btlb.[BrandId]	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListBrandGetAllByBlock]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandGetAllByBlock]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[lekmer].[vBlockBrandTopListBrand] b
	WHERE
		b.[BlockBrandTopListBrand.BlockId] = @BlockId
	ORDER BY
		[BlockBrandTopListBrand.Position]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListCategoryGetAllByBlock]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategoryGetAllByBlock]
	@LanguageId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[lekmer].[vBlockBrandTopListCategorySecure] b
		INNER JOIN [product].[vCustomCategory] AS c ON b.[BlockBrandTopListCategory.CategoryId] = c.[Category.Id]
	WHERE
		[BlockBrandTopListCategory.BlockId] = @BlockId
		AND c.[Category.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBrandGetAllByIdList]'
GO

CREATE PROCEDURE [lekmer].[pBrandGetAllByIdList]
	@ChannelId	INT,
	@BrandIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		b.*
	FROM
		[lekmer].[vBrand] b
		INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) AS bl ON bl.[ID] = b.[Brand.BrandId]
	WHERE 
		b.[ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetIdAllByProductAndRelationType]'
GO
CREATE PROCEDURE [lekmer].[pProductGetIdAllByProductAndRelationType]
		@ChannelId		INT,
		@CustomerId		INT,
		@ProductId		INT,
		@RelationType	VARCHAR(50)
AS
BEGIN
	DECLARE @RelationListTypeId INT
	SET @RelationListTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = @RelationType)

	SELECT DISTINCT p.[Product.Id]
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		prl.ProductId = @ProductId
		AND rlp.ProductId <> @ProductId
		AND rl.RelationListTypeId = @RelationListTypeId
		AND p.[Product.ChannelId] = @ChannelId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopListBrandSecure]'
GO


CREATE VIEW [lekmer].[vBlockBrandTopListBrandSecure]
AS
	SELECT
		btlb.[BlockId] AS 'BlockBrandTopListBrand.BlockId',
		btlb.[BrandId] AS 'BlockBrandTopListBrand.BrandId',
		btlb.[Position] AS 'BlockBrandTopListBrand.Position',
		b.*
	FROM
		[lekmer].[tBlockBrandTopListBrand] btlb
		INNER JOIN [lekmer].[vBrandSecure] b on b.[Brand.BrandId] = btlb.[BrandId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListBrandGetAllByBlockSecure]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[lekmer].[vBlockBrandTopListBrandSecure] AS b
	WHERE
		b.[BlockBrandTopListBrand.BlockId] = @BlockId
	ORDER BY
		[BlockBrandTopListBrand.Position]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pTopListBrandGetIdAllByBlock]'
GO

CREATE PROCEDURE [lekmer].[pTopListBrandGetIdAllByBlock]
	@ChannelId				INT,
	@BlockId				INT,
	@IncludeAllCategories	BIT,
	@CategoryIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@CountOrdersFrom		DATETIME,
	@From					INT,
	@To						INT
AS
BEGIN
	DECLARE @sql AS NVARCHAR(MAX)
	DECLARE @sqlCount AS NVARCHAR(MAX)
	DECLARE @sqlFragment AS NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT
			ROW_NUMBER() OVER (ORDER BY SUM(oi.[OrderItem.Quantity]) DESC) AS Number,
			b.[Brand.BrandId] AS BrandId
		FROM
			[lekmer].[vBrand] b
			INNER JOIN [product].[vCustomProduct] p ON p.[Lekmer.BrandId] = b.[Brand.BrandId]
			INNER JOIN [order].[vCustomOrderItem] oi WITH(NOLOCK) ON oi.[OrderItem.ProductId] = p.[Product.Id]
			INNER JOIN [order].[vCustomOrder] o WITH(NOLOCK) ON oi.[OrderItem.OrderId] = o.[Order.OrderId]
		WHERE
			p.[Product.ChannelId] = @ChannelId
			AND o.[Order.CreatedDate] > @CountOrdersFrom
	'
	
	IF @IncludeAllCategories = 0
	BEGIN
		SET @sqlFragment += '
			AND (
				p.[Product.CategoryId] in (
					SELECT
						[Id]
					FROM
						[generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter)
				)
			)
	'
	END
	
	SET @sqlFragment += '
			AND NOT EXISTS (
				SELECT *
				FROM
					[lekmer].[vBlockBrandTopListBrand] fb
				where
					fb.[BlockBrandTopListBrand.BlockId] = @BlockId
					AND fb.[BlockBrandTopListBrand.BrandId] = b.[Brand.BrandId]
			)
		GROUP BY
			b.[Brand.BrandId]
	'

	SET @sqlCount = '
		SELECT COUNT(*)
		FROM
			(' + @sqlFragment + ') AS CountResults'

	SET @sql = '
		SELECT [BrandId]
		FROM
			(' + @sqlFragment + ') AS SearchResult
		WHERE
			[Number] BETWEEN @From AND @To'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom
		     
	EXEC sp_executesql @sql, 
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME,
			@From					INT,
			@To						INT',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom,
			@From,
			@To
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [lekmer].[tBlockBrandTopListBrand]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD CONSTRAINT [IX_tBlockBrandTopListBrand] UNIQUE NONCLUSTERED  ([BlockId], [Position])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockBrandTopList]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopList] ADD
CONSTRAINT [FK_tBlockBrandTopList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]),
CONSTRAINT [FK_tBlockBrandTopList_tContentNode] FOREIGN KEY ([LinkContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockBrandTopListBrand]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD
CONSTRAINT [FK_tBlockBrandTopListBrand_tBlockBrandTopList] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockBrandTopList] ([BlockId]),
CONSTRAINT [FK_tBlockBrandTopListBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockBrandTopListCategory]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListCategory] ADD
CONSTRAINT [FK_tBlockBrandTopListCategory_tBlockBrandTopList] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockBrandTopList] ([BlockId]),
CONSTRAINT [FK_tBlockBrandTopListCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
