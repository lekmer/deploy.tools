/*
Run this script on:

        (local).Lekmer_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 004\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 27.03.2012 9:43:39

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [lekmer].[tBlockBrandTopList]'
GO
CREATE TABLE [lekmer].[tBlockBrandTopList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[TotalBrandCount] [int] NOT NULL,
[IncludeAllCategories] [bit] NOT NULL,
[OrderStatisticsDayCount] [int] NOT NULL,
[LinkContentNodeId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockBrandTopList] on [lekmer].[tBlockBrandTopList]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopList] ADD CONSTRAINT [PK_tBlockBrandTopList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tBlockBrandTopListBrand]'
GO
CREATE TABLE [lekmer].[tBlockBrandTopListBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[Position] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockBrandTopListBrand] on [lekmer].[tBlockBrandTopListBrand]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD CONSTRAINT [PK_tBlockBrandTopListBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tBlockBrandTopListCategory]'
GO
CREATE TABLE [lekmer].[tBlockBrandTopListCategory]
(
[BlockId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockBrandTopListCategory] on [lekmer].[tBlockBrandTopListCategory]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListCategory] ADD CONSTRAINT [PK_tBlockBrandTopListCategory] PRIMARY KEY CLUSTERED  ([BlockId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[tImageImportLog]'
GO
CREATE TABLE [integration].[tImageImportLog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Data] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [varchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__tImageIm__3214EC0758F81366] on [integration].[tImageImportLog]'
GO
ALTER TABLE [integration].[tImageImportLog] ADD CONSTRAINT [PK__tImageIm__3214EC0758F81366] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListBrandDeleteAll]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandTopListBrand]
	WHERE  [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListBrandSave]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandSave]
	@BlockId INT,
	@BrandId INT,
	@Position INT
AS
BEGIN
	UPDATE [lekmer].[tBlockBrandTopListBrand]
	SET
		[Position] = @Position
	WHERE
		[BlockId] = @BlockId
		AND [BrandId] = @BrandId
		
	IF @@ROWCOUNT <> 0 RETURN
		
	INSERT [lekmer].[tBlockBrandTopListBrand] (
		[BlockId],
		[BrandId],
		[Position]
	)
	VALUES (
		@BlockId,
		@BrandId,
		@Position
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListCategoryDeleteAll]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategoryDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandTopListCategory]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListCategorySave]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategorySave]
	@BlockId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	UPDATE [lekmer].[tBlockBrandTopListCategory]
	SET
		[IncludeSubcategories] = @IncludeSubcategories
	WHERE
		[BlockId] = @BlockId
		AND [CategoryId] = @CategoryId
		
	IF @@ROWCOUNT <> 0 RETURN
		
	INSERT [lekmer].[tBlockBrandTopListCategory] (
		[BlockId],
		[CategoryId],
		[IncludeSubcategories]
	)
	VALUES (
		@BlockId,
		@CategoryId,
		@IncludeSubcategories
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListDelete]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListDelete]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandTopListCategory]
	WHERE  [BlockId] = @BlockId

	DELETE [lekmer].[tBlockBrandTopListBrand]
	WHERE  [BlockId] = @BlockId

	DELETE [lekmer].[tBlockBrandTopList]
	WHERE  [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopList]'
GO




CREATE VIEW [lekmer].[vBlockBrandTopList]
AS
	SELECT
		b.[BlockId] AS 'BlockBrandTopList.BlockId',
		b.[ColumnCount] AS 'BlockBrandTopList.ColumnCount',
		b.[RowCount] AS 'BlockBrandTopList.RowCount',
		b.[TotalBrandCount] AS 'BlockBrandTopList.TotalBrandCount',
		b.[IncludeAllCategories] AS 'BlockBrandTopList.IncludeAllCategories',
		b.[OrderStatisticsDayCount] AS 'BlockBrandTopList.OrderStatisticsDayCount',
		b.[LinkContentNodeId] AS 'BlockBrandTopList.LinkContentNodeId'
	FROM
		[lekmer].[tBlockBrandTopList] b
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListSave]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@TotalBrandCount INT,
	@IncludeAllCategories BIT,
	@OrderStatisticsDayCount INT,
	@LinkContentNodeId INT
AS
BEGIN
	UPDATE lekmer.tBlockBrandTopList
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount,
		[TotalBrandCount] = @TotalBrandCount,
		[IncludeAllCategories] = @IncludeAllCategories,
		[OrderStatisticsDayCount] = @OrderStatisticsDayCount,
		[LinkContentNodeId] = @LinkContentNodeId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0 RETURN
		
	INSERT lekmer.tBlockBrandTopList (
		[BlockId],
		[ColumnCount],
		[RowCount],
		[TotalBrandCount],
		[IncludeAllCategories],
		[OrderStatisticsDayCount],
		[LinkContentNodeId]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@TotalBrandCount,
		@IncludeAllCategories,
		@OrderStatisticsDayCount,
		@LinkContentNodeId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pAvailGetProductDiscountPriceOnSite]'
GO
CREATE PROCEDURE [lekmer].[pAvailGetProductDiscountPriceOnSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--set statistics io on
	--set statistics time on
	
	SET NOCOUNT ON;

	DECLARE
		@Channel_SE INT,
		@Channel_NO INT,
		@Channel_DA INT,
		@Channel_FI INT,
		@PriceListId_FI INT,
		@ProductActionTypeId_Discount INT,
		@ProductActionTypeId_Percentage INT
		
	SELECT @Channel_SE = ChannelId FROM core.tChannel WHERE CommonName = 'Sweden'
	SELECT @Channel_NO = ChannelId FROM core.tChannel WHERE CommonName = 'Norway'
	SELECT @Channel_DA = ChannelId FROM core.tChannel WHERE CommonName = 'Denmark'
	SELECT @Channel_FI = ChannelId FROM core.tChannel WHERE CommonName = 'Finland'
	
	SELECT @ProductActionTypeId_Discount = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'ProductDiscount'
	
	SELECT @ProductActionTypeId_Percentage = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'PercentagePriceDiscount'
	
	SELECT @PriceListId_FI = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'Finland'
	
	---- #Campaign
	
	IF OBJECT_ID('tempdb..#Campaign') IS NOT NULL
		DROP TABLE #Campaign

	CREATE TABLE #Campaign
	(
	   CampaignId INT PRIMARY KEY,
	   PriceListId INT
	)

	INSERT INTO #Campaign (
		CampaignId,
		PriceListId
	)
	SELECT
		c.CampaignId,
		pl.PriceListId
	FROM
		campaign.tCampaign c
		LEFT OUTER JOIN campaign.tCampaignModuleChannel cmc ON cmc.CampaignRegistryId = c.CampaignRegistryId
		LEFT OUTER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = cmc.ChannelId
		LEFT OUTER JOIN product.tPriceList pl ON pl.PriceListRegistryId = pmc.PriceListRegistryId
	WHERE
		c.CampaignStatusId = 0
		AND (c.StartDate IS NULL OR c.StartDate <= GETDATE())
		AND (c.EndDate IS NULL OR GETDATE() <= c.EndDate)
		AND cmc.ChannelId in (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI)

	---- #CampaignAction
	
	IF OBJECT_ID('tempdb..#CampaignAction') IS NOT NULL
		DROP TABLE #CampaignAction

	CREATE TABLE #CampaignAction (
	   CampaignId INT,
	   SortIndex INT,   
	   ProductActionId INT,
	   Ordinal INT
	)

	DECLARE @Index INT
	SET @Index = 1

	INSERT INTO #CampaignAction (
		CampaignId,
		SortIndex,
		ProductActionId,
		Ordinal
	)
	SELECT
		pa.CampaignId,
		@index,
		pa.ProductActionId,
		pa.Ordinal
	FROM
		#Campaign c
		INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId
		LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal < pa.Ordinal --WTF?
	WHERE
		pa2.ProductActionId IS NULL

	WHILE (@@ROWCOUNT > 0) 
	BEGIN
		SET @Index = @Index + 1
  
		INSERT INTO #CampaignAction (
			CampaignId,
			SortIndex,
			ProductActionId,
			Ordinal
		)
		SELECT
			pa.CampaignId,
			@index,
			pa.ProductActionId,
			pa.Ordinal
		FROM
			#Campaign c
			INNER JOIN #CampaignAction tca ON tca.CampaignId = c.CampaignId AND tca.SortIndex = @Index - 1
			INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId AND pa.Ordinal > tca.Ordinal
			LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal > tca.Ordinal AND pa2.Ordinal < pa.Ordinal
		WHERE
			pa2.ProductActionId IS NULL
		OPTION (FORCE ORDER)
	END


	---- #CampaignProduct

	IF OBJECT_ID('tempdb..#CampaignProduct') IS NOT NULL
		DROP TABLE #CampaignProduct

	CREATE TABLE #CampaignProduct (
		CampaignId INT,
		PriceListId INT,
		ProductId INT,
		Price DECIMAL(16, 2) PRIMARY KEY (CampaignId, ProductId) WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		ip.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN campaign.tPercentagePriceDiscountActionIncludeProduct ip ON ip.ProductActionId = ca.ProductActionId

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		pdai.ProductId,
		NULL
	from
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		p.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN campaign.tPercentagePriceDiscountActionIncludeCategory ppda ON ppda.ProductActionId = ca.ProductActionId
		INNER JOIN product.tProduct p ON p.CategoryId = ppda.CategoryId

	-- Set product prices

	UPDATE cp
	SET Price = pli.PriceIncludingVat
	FROM
		#Campaign c
		INNER JOIN #CampaignProduct cp ON cp.CampaignId = c.CampaignId
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = c.PriceListId AND pli.ProductId = cp.ProductId

	DECLARE @LastIndex INT
	SET @Index = 1
	SELECT @LastIndex = MAX(SortIndex) FROM #CampaignAction

	WHILE (@Index <= @LastIndex)
	BEGIN
		UPDATE cp
		SET Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Percentage
			INNER JOIN campaign.tPercentagePriceDiscountAction ppda ON ppda.ProductActionId = pa.ProductActionId
			INNER JOIN campaign.tPercentagePriceDiscountActionIncludeProduct ip ON ip.ProductActionId = ca.ProductActionId
			INNER JOIN #CampaignProduct cp ON cp.ProductId = ip.ProductId AND cp.CampaignId = ca.CampaignId
		WHERE
			ca.SortIndex = @Index

		UPDATE cp
		SET Price = pdai.DiscountPrice
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Discount
			INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
			INNER JOIN #CampaignProduct cp ON cp.ProductId = pdai.ProductId AND cp.CampaignId = ca.CampaignId
		WHERE
			ca.SortIndex = @Index
			AND (cp.Price > pdai.DiscountPrice OR cp.Price IS NULL)

		SET @Index = @Index + 1
	END

	DELETE cp FROM #CampaignProduct cp WHERE Price IS NULL

	UPDATE cp
	SET price = CASE
					WHEN (c.PriceListId = @PriceListId_FI) THEN Price
					ELSE ROUND(Price, 0)
				END
	FROM
		#CampaignProduct cp
		INNER JOIN #Campaign c ON c.CampaignId = cp.CampaignId

	---- #BestCampaignPrice

	IF OBJECT_ID('tempdb..#BestCampaignPrice') IS NOT NULL
		DROP TABLE #BestCampaignPrice

	CREATE TABLE #BestCampaignPrice (
		ProductId INT,
		PriceListId INT,
		CampaignId INT,
		Price DECIMAL(16, 2)
	)

	INSERT INTO #BestCampaignPrice (
		ProductId,
		PriceListId,
		CampaignId,
		Price) -- Fel
	SELECT
		cp.ProductId,
		cp.PriceListId,
		cp.CampaignId,
		cp.Price
	FROM
		#CampaignProduct cp
		LEFT OUTER JOIN #CampaignProduct cp2 
			ON cp2.PriceListId = cp.PriceListId
				AND cp2.ProductId = cp.ProductId
				AND (cp2.Price < cp.Price OR cp2.Price = cp.Price AND cp2.CampaignId > cp.CampaignId)      
	WHERE cp2.ProductId IS NULL

	DROP TABLE #Campaign
	DROP TABLE #CampaignAction
	DROP TABLE #CampaignProduct

	---- @PriceList
	
	DECLARE @PriceList TABLE (
		PriceListId INT PRIMARY KEY,
		ChannelId INT
	)

	INSERT INTO @PriceList 
	SELECT
		pl.PriceListId,
		c.ChannelId
	FROM
		core.tChannel c
		INNER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = c.ChannelId
		INNER JOIN product.tProductRegistry pr ON pr.ProductRegistryId = pmc.ProductRegistryId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = pr.ProductRegistryId
	WHERE
		c.ChannelId IN (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI)

	---- Result

	SELECT 
		pl.ChannelId,
		--ll.HYErpId,
		pli.ProductId,
		--pli.PriceListId,
		--pli.PriceIncludingVat AS ListPrice,
		--bcp.Price AS DiscountPrice,
		CASE 
			WHEN (bcp.Price IS NULL OR bcp.Price > pli.PriceIncludingVat) THEN pli.PriceIncludingVat
			ELSE bcp.Price 
		END AS SitePrice
		--CAST(((1-(bcp.Price/pli.PriceIncludingVat)) * 100) AS DECIMAL (18,2)) AS 'Total discount %',
	FROM
		@PriceList pl
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = pl.PriceListId
		INNER JOIN product.tProduct t ON t.ProductId = pli.ProductId
		LEFT OUTER JOIN #BestCampaignPrice bcp ON bcp.PriceListId = pl.PriceListId AND bcp.ProductId = t.ProductId
		LEFT OUTER JOIN campaign.tCampaign c ON c.CampaignId = bcp.CampaignId
		INNER JOIN lekmer.tLekmerProduct ll ON ll.ProductId = pli.ProductId
	WHERE
		c.CampaignId IS NOT NULL
	ORDER BY
		pl.ChannelId
		
   DROP TABLE #BestCampaignPrice
   
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopListCategory]'
GO

CREATE VIEW [lekmer].[vBlockBrandTopListCategory]
AS
	SELECT
		b.[BlockId] AS 'BlockBrandTopListCategory.BlockId',
		b.[CategoryId] AS 'BlockBrandTopListCategory.CategoryId',
		b.[IncludeSubcategories] AS 'BlockBrandTopListCategory.IncludeSubcategories',
		c.*
	FROM
		[lekmer].[tBlockBrandTopListCategory] b
		INNER JOIN [product].[vCustomCategory] c ON b.[CategoryId] = c.[Category.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopListCategorySecure]'
GO

CREATE VIEW [lekmer].[vBlockBrandTopListCategorySecure]
AS
	SELECT
		b.[BlockId]	AS 'BlockBrandTopListCategory.BlockId',
		b.[CategoryId] AS 'BlockBrandTopListCategory.CategoryId',
		b.[IncludeSubcategories] AS 'BlockBrandTopListCategory.IncludeSubcategories',
		c.*
	FROM
		[lekmer].[tBlockBrandTopListCategory] b
		INNER JOIN [product].[vCustomCategorySecure] c on b.[CategoryId] = c.[Category.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListCategoryGetAllByBlockSecure]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategoryGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vBlockBrandTopListCategorySecure]
	WHERE
		[BlockBrandTopListCategory.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListGetById]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListGetById]
	@LanguageId INT,
	@BlockId INT
AS
BEGIN
	SELECT 
		btl.*,
		b.*
	FROM 
		[lekmer].[vBlockBrandTopList] AS btl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON btl.[BlockBrandTopList.BlockId] = b.[Block.BlockId]
	WHERE
		btl.[BlockBrandTopList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListGetByIdSecure]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		btl.*,
		b.*
	FROM 
		[lekmer].[vBlockBrandTopList] AS btl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON btl.[BlockBrandTopList.BlockId] = b.[Block.BlockId]
	WHERE
		btl.[BlockBrandTopList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopListBrand]'
GO



CREATE VIEW [lekmer].[vBlockBrandTopListBrand]
AS
	SELECT
		btlb.[BlockId] AS 'BlockBrandTopListBrand.BlockId',
		btlb.[BrandId] AS 'BlockBrandTopListBrand.BrandId',
		btlb.[Position] AS 'BlockBrandTopListBrand.Position',
		b.*
	FROM
		[lekmer].[tBlockBrandTopListBrand] btlb
		INNER JOIN [lekmer].[vBrand] b on b.[Brand.BrandId] = btlb.[BrandId]	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListBrandGetAllByBlock]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandGetAllByBlock]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[lekmer].[vBlockBrandTopListBrand] b
	WHERE
		b.[BlockBrandTopListBrand.BlockId] = @BlockId
	ORDER BY
		[BlockBrandTopListBrand.Position]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListCategoryGetAllByBlock]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategoryGetAllByBlock]
	@LanguageId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[lekmer].[vBlockBrandTopListCategorySecure] b
		INNER JOIN [product].[vCustomCategory] AS c ON b.[BlockBrandTopListCategory.CategoryId] = c.[Category.Id]
	WHERE
		[BlockBrandTopListCategory.BlockId] = @BlockId
		AND c.[Category.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBrandGetAllByIdList]'
GO

CREATE PROCEDURE [lekmer].[pBrandGetAllByIdList]
	@ChannelId	INT,
	@BrandIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		b.*
	FROM
		[lekmer].[vBrand] b
		INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) AS bl ON bl.[ID] = b.[Brand.BrandId]
	WHERE 
		b.[ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetIdAllByProductAndRelationType]'
GO
CREATE PROCEDURE [lekmer].[pProductGetIdAllByProductAndRelationType]
		@ChannelId		INT,
		@CustomerId		INT,
		@ProductId		INT,
		@RelationType	VARCHAR(50)
AS
BEGIN
	DECLARE @RelationListTypeId INT
	SET @RelationListTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = @RelationType)

	SELECT DISTINCT p.[Product.Id]
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		prl.ProductId = @ProductId
		AND rlp.ProductId <> @ProductId
		AND rl.RelationListTypeId = @RelationListTypeId
		AND p.[Product.ChannelId] = @ChannelId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockBrandTopListBrandSecure]'
GO


CREATE VIEW [lekmer].[vBlockBrandTopListBrandSecure]
AS
	SELECT
		btlb.[BlockId] AS 'BlockBrandTopListBrand.BlockId',
		btlb.[BrandId] AS 'BlockBrandTopListBrand.BrandId',
		btlb.[Position] AS 'BlockBrandTopListBrand.Position',
		b.*
	FROM
		[lekmer].[tBlockBrandTopListBrand] btlb
		INNER JOIN [lekmer].[vBrandSecure] b on b.[Brand.BrandId] = btlb.[BrandId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockBrandTopListBrandGetAllByBlockSecure]'
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[lekmer].[vBlockBrandTopListBrandSecure] AS b
	WHERE
		b.[BlockBrandTopListBrand.BlockId] = @BlockId
	ORDER BY
		[BlockBrandTopListBrand.Position]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pTopListBrandGetIdAllByBlock]'
GO

CREATE PROCEDURE [lekmer].[pTopListBrandGetIdAllByBlock]
	@ChannelId				INT,
	@BlockId				INT,
	@IncludeAllCategories	BIT,
	@CategoryIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@CountOrdersFrom		DATETIME,
	@From					INT,
	@To						INT
AS
BEGIN
	DECLARE @sql AS NVARCHAR(MAX)
	DECLARE @sqlCount AS NVARCHAR(MAX)
	DECLARE @sqlFragment AS NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT
			ROW_NUMBER() OVER (ORDER BY SUM(oi.[OrderItem.Quantity]) DESC) AS Number,
			b.[Brand.BrandId] AS BrandId
		FROM
			[lekmer].[vBrand] b
			INNER JOIN [product].[vCustomProduct] p ON p.[Lekmer.BrandId] = b.[Brand.BrandId]
			INNER JOIN [order].[vCustomOrderItem] oi WITH(NOLOCK) ON oi.[OrderItem.ProductId] = p.[Product.Id]
			INNER JOIN [order].[vCustomOrder] o WITH(NOLOCK) ON oi.[OrderItem.OrderId] = o.[Order.OrderId]
		WHERE
			p.[Product.ChannelId] = @ChannelId
			AND o.[Order.CreatedDate] > @CountOrdersFrom
	'
	
	IF @IncludeAllCategories = 0
	BEGIN
		SET @sqlFragment += '
			AND (
				p.[Product.CategoryId] in (
					SELECT
						[Id]
					FROM
						[generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter)
				)
			)
	'
	END
	
	SET @sqlFragment += '
			AND NOT EXISTS (
				SELECT *
				FROM
					[lekmer].[vBlockBrandTopListBrand] fb
				where
					fb.[BlockBrandTopListBrand.BlockId] = @BlockId
					AND fb.[BlockBrandTopListBrand.BrandId] = b.[Brand.BrandId]
			)
		GROUP BY
			b.[Brand.BrandId]
	'

	SET @sqlCount = '
		SELECT COUNT(*)
		FROM
			(' + @sqlFragment + ') AS CountResults'

	SET @sql = '
		SELECT [BrandId]
		FROM
			(' + @sqlFragment + ') AS SearchResult
		WHERE
			[Number] BETWEEN @From AND @To'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom
		     
	EXEC sp_executesql @sql, 
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME,
			@From					INT,
			@To						INT',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom,
			@From,
			@To
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [lekmer].[tBlockBrandTopListBrand]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD CONSTRAINT [IX_tBlockBrandTopListBrand] UNIQUE NONCLUSTERED  ([BlockId], [Position])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockBrandTopList]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopList] ADD
CONSTRAINT [FK_tBlockBrandTopList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]),
CONSTRAINT [FK_tBlockBrandTopList_tContentNode] FOREIGN KEY ([LinkContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockBrandTopListBrand]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListBrand] ADD
CONSTRAINT [FK_tBlockBrandTopListBrand_tBlockBrandTopList] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockBrandTopList] ([BlockId]),
CONSTRAINT [FK_tBlockBrandTopListBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockBrandTopListCategory]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopListCategory] ADD
CONSTRAINT [FK_tBlockBrandTopListCategory_tBlockBrandTopList] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockBrandTopList] ([BlockId]),
CONSTRAINT [FK_tBlockBrandTopListCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
