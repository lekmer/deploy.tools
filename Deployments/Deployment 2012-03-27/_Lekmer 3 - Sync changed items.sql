/*
Run this script on:

        (local).Lekmer_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 004\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 27.03.2012 9:48:11

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping extended properties'
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'order', 'VIEW', N'vCustomOrder', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'order', 'VIEW', N'vCustomOrder', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [customer].[tCustomerComment]'
GO
ALTER TABLE [customer].[tCustomerComment] DROP
CONSTRAINT [FK_tCustomerComment_tCustomer]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [customer].[tCustomerGroupCustomer]'
GO
ALTER TABLE [customer].[tCustomerGroupCustomer] DROP
CONSTRAINT [FK_tCustomerGroupCustomer_tCustomer]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [customer].[tCustomerUser]'
GO
ALTER TABLE [customer].[tCustomerUser] DROP
CONSTRAINT [FK_tCustomer_tCustomerUser]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[integrationLog]'
GO
ALTER TABLE [integration].[integrationLog] DROP CONSTRAINT [PK__integrat__3214EC0737088A03]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[OrderLog]'
GO
ALTER TABLE [integration].[OrderLog] DROP CONSTRAINT [PK__OrderLog__3214EC073337F91F]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[SEOCategorys]'
GO
ALTER TABLE [integration].[SEOCategorys] DROP CONSTRAINT [PK__SEOCateg__BE27E945533AB45D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tAccessoriesSize]'
GO
ALTER TABLE [integration].[tAccessoriesSize] DROP CONSTRAINT [PK__tAccesso__B0A650D318D9089E]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tAge]'
GO
ALTER TABLE [integration].[tAge] DROP CONSTRAINT [PK__tAge__67B311717D30EE29]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tBrandCreation]'
GO
ALTER TABLE [integration].[tBrandCreation] DROP CONSTRAINT [PK__tBrandCr__DAD4F05E4F3417FF]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tChannelRestrictionBrand]'
GO
ALTER TABLE [integration].[tChannelRestrictionBrand] DROP CONSTRAINT [PK__tChannel__618860890C7331B9]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tChannelRestrictionCategory]'
GO
ALTER TABLE [integration].[tChannelRestrictionCategory] DROP CONSTRAINT [PK__tChannel__A255AADC1043C29D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tChannelRestrictionProduct]'
GO
ALTER TABLE [integration].[tChannelRestrictionProduct] DROP CONSTRAINT [PK__tChannel__0F50561A14145381]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tDealOfTheDay]'
GO
ALTER TABLE [integration].[tDealOfTheDay] DROP CONSTRAINT [PK__tDealOfT__47CE2B702E29C09D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tempProduct]'
GO
ALTER TABLE [integration].[tempProduct] DROP CONSTRAINT [PK__tempProd__ED0627FB3F9DD004]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tempProductPrice]'
GO
ALTER TABLE [integration].[tempProductPrice] DROP CONSTRAINT [PK__tempProd__ED0627FB3AD91AE7]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tempProductStockPosition]'
GO
ALTER TABLE [integration].[tempProductStockPosition] DROP CONSTRAINT [PK__tempProd__3FA33FFA44B3F18E]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tLekmerArtNo]'
GO
ALTER TABLE [integration].[tLekmerArtNo] DROP CONSTRAINT [PK__tLekmerA__ED0627FB6A1E19B5]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tOrderLog]'
GO
ALTER TABLE [integration].[tOrderLog] DROP CONSTRAINT [PK__tOrderLo__3214EC0723F08164]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tOrderQue]'
GO
ALTER TABLE [integration].[tOrderQue] DROP CONSTRAINT [PK__tOrderQu__C3905BCF27C11248]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tProductOutOfStock]'
GO
ALTER TABLE [integration].[tProductOutOfStock] DROP CONSTRAINT [PK__tProduct__0C37165A635B0C3D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tProductRegistryRestrictions]'
GO
ALTER TABLE [integration].[tProductRegistryRestrictions] DROP CONSTRAINT [PK__tProduct__0F50561A46D4DD78]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tProductStockPosition]'
GO
ALTER TABLE [integration].[tProductStockPosition] DROP CONSTRAINT [PK__tProduct__3FA33FFA3898336C]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tProductsTranslatedInHY]'
GO
ALTER TABLE [integration].[tProductsTranslatedInHY] DROP CONSTRAINT [PK__tProduct__1F9F439771BF3B7D]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tProductTitlesUpdatedInHY]'
GO
ALTER TABLE [integration].[tProductTitlesUpdatedInHY] DROP CONSTRAINT [PK__tProduct__1F9F439754C42C7A]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [integration].[tTradeDoublerProductGroupMapping]'
GO
ALTER TABLE [integration].[tTradeDoublerProductGroupMapping] DROP CONSTRAINT [PK__tTradeDo__AE1067045D03DB91]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tOrderItem_OrderId] from [order].[tOrderItem]'
GO
DROP INDEX [IX_tOrderItem_OrderId] ON [order].[tOrderItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockProductSimilarList]'
GO
ALTER TABLE [lekmer].[tBlockProductSimilarList] ADD
[PriceRangeType] [int] NOT NULL CONSTRAINT [DF_tBlockProductSimilarList_PriceType] DEFAULT ((1))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockProductSimilarListSave]'
GO
ALTER PROCEDURE [lekmer].[pBlockProductSimilarListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@TotalProductCount INT,
	@PriceRangeType INT
AS
BEGIN
	UPDATE [lekmer].[tBlockProductSimilarList]
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount,
		[TotalProductCount] = @TotalProductCount,
		[PriceRangeType] = @PriceRangeType
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockProductSimilarList] (
		[BlockId],
		[ColumnCount],
		[RowCount],
		[TotalProductCount],
		[PriceRangeType]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@TotalProductCount,
		@PriceRangeType
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAllByBlockAndProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductGetIdAllByBlockAndProduct]
		@ChannelId		INT,
		@CustomerId		INT,
		@BlockId		INT,
		@ProductId		INT,
		@ShowVariants	BIT,
		@Page			INT = NULL,
		@PageSize		INT,
		@SortBy			VARCHAR(50) = NULL,
		@SortDescending BIT = NULL
AS
BEGIN
	DECLARE @RelationListVariantTypeId INT
	SET @RelationListVariantTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'Variant')
		
	DECLARE @maxCount CHAR(4)
	SET @maxCount = '2000'

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @CustomerIdString VARCHAR(10)
	
	IF (@CustomerId IS NULL)
		SET @CustomerIdString = 'null'
	ELSE
		SET @CustomerIdString = CAST(@CustomerId AS VARCHAR(10))
		
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sql = '
	declare @tResult table (ProductId int, SortBy nvarchar(max))	
	insert @tResult	(ProductId, SortBy)
	select distinct TOP ' + @maxCount + ' p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	from
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN [product].[tBlockProductRelationListItem] bprli ON rl.RelationListTypeId = bprli.RelationListTypeId
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				' + @CustomerIdString + '
			)
	WHERE
		prl.ProductId = ' + cast(@ProductId as varchar) + '
		AND rlp.ProductId <> ' + cast(@ProductId as varchar) + '
		' + CASE WHEN (@ShowVariants = 0) 
				THEN ' AND rl.RelationListTypeId <> ' + cast(@RelationListVariantTypeId as varchar) + ' '
				ELSE ' '
			END + '
		AND p.[Product.ChannelId] = ' + cast(@ChannelId as varchar) + '
		AND bprli.BlockId = ' + cast(@BlockId as varchar) + '

	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + CAST(@Page * @PageSize AS varchar(10)) + ' *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
		AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + @maxCount + ' ProductId FROM @tResult AS SearchResult'
	END
	
	EXEC (@sql)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockProductSimilarList]'
GO


ALTER VIEW [lekmer].[vBlockProductSimilarList]
AS
	SELECT
		[BlockId] as 'BlockProductSimilarList.BlockId',
		[ColumnCount] as 'BlockProductSimilarList.ColumnCount',
		[RowCount] as 'BlockProductSimilarList.RowCount',
		[TotalProductCount] as 'BlockProductSimilarList.TotalProductCount',
		[PriceRangeType] as 'BlockProductSimilarList.PriceRangeType'
	FROM
		[lekmer].[tBlockProductSimilarList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pReviewSummaryGetByProduct]'
GO

ALTER PROCEDURE [addon].[pReviewSummaryGetByProduct]
         @ChannelId         int,
         @ProductId int
AS
BEGIN
         SELECT
                   avg(cast(r.[Review.Rating] AS decimal)) AS 'AverageRating',
                   count(r.[Review.ProductId]) AS 'ReviewCount'
         FROM 
                   addon.vCustomReview r
         WHERE 
                   r.[Review.ProductId] = @ProductId
                   AND r.[Review.ChannelId] = @ChannelId
                   AND r.[Review.ReviewStatusId] = 2 /*Approved status id*/
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrder]'
GO

ALTER VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		lo.[PaymentCost] AS [Lekmer.PaymentCost],
		lo.[VoucherDiscount] AS [Lekmer.VoucherDiscount],
		lo.[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrand]'
GO

ALTER VIEW [lekmer].[vBrand]
AS
	SELECT     
		B.BrandId AS 'Brand.BrandId',
		COALESCE (bt.[BrandTranslation.Title], B.Title) AS 'Brand.Title',
		COALESCE (bt.[BrandTranslation.Description], B.[Description]) AS 'Brand.Description',
		B.ExternalUrl AS 'Brand.ExternalUrl',
		B.MediaId AS 'Brand.MediaId',
		B.ErpId AS 'Brand.ErpId',
		Ch.ChannelId,
		I.*,
		Bssr.ContentNodeId AS 'Brand.ContentNodeId'
	FROM
		lekmer.tBrand AS B
		CROSS JOIN core.tChannel AS Ch
		
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = B.MediaId AND I.[Image.LanguageId] = Ch.LanguageId
		LEFT JOIN lekmer.vBrandTranslation AS bt ON bt.[BrandTranslation.BrandId] = B.BrandId AND bt.[BrandTranslation.LanguageId] = Ch.LanguageId
		LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = Ch.ChannelId
		LEFT JOIN lekmer.tBrandSiteStructureRegistry Bssr ON Bssr.BrandId = B.BrandId AND Bssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pReviewGetAllByProduct]'
GO
ALTER PROCEDURE [addon].[pReviewGetAllByProduct]
         @ChannelId         int,
         @ProductId int,
         @Page                  int,
         @PageSize   int
AS
BEGIN
         SET NOCOUNT ON; 

    DECLARE @sql nvarchar(max)
         DECLARE @sqlCount nvarchar(max)
         DECLARE @sqlFragment nvarchar(max)
         
         SET @sqlFragment = '
                   SELECT ROW_NUMBER() OVER (ORDER BY r.[Review.CreatedDate] DESC) AS Number,
                            r.*
                  FROM 
                            [addon].[vCustomReview] AS r
                   WHERE 
                            r.[Review.ProductId] = @ProductId AND r.[Review.ChannelId] = @ChannelId AND r.[Review.ReviewStatusId] = 2' /*Approved status id*/

         SET @sql = 
                   'SELECT * FROM
                   (' + @sqlFragment + '
                   )
                   AS SearchResult'

         IF @Page != 0 AND @Page IS NOT NULL 
         BEGIN
                   SET @sql = @sql + '
         WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
         END
         
         SET @sqlCount = 'SELECT COUNT(1) FROM
                   (
                   ' + @sqlFragment + '
                   )
                   AS CountResults'
                   print @sqlCount
                   print @sql
         EXEC sp_executesql @sqlCount,
                   N'      @ChannelId         INT,
                            @ProductId INT',
                            @ChannelId,
                            @ProductId
                                 
         EXEC sp_executesql @sql, 
                   N'      @ChannelId         INT,
                            @ProductId INT',
                            @ChannelId,
                            @ProductId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_integrationLog] on [integration].[integrationLog]'
GO
ALTER TABLE [integration].[integrationLog] ADD CONSTRAINT [PK_integrationLog] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_OrderLog] on [integration].[OrderLog]'
GO
ALTER TABLE [integration].[OrderLog] ADD CONSTRAINT [PK_OrderLog] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SEOCategorys] on [integration].[SEOCategorys]'
GO
ALTER TABLE [integration].[SEOCategorys] ADD CONSTRAINT [PK_SEOCategorys] PRIMARY KEY CLUSTERED  ([ContentNodeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAccessoriesSize] on [integration].[tAccessoriesSize]'
GO
ALTER TABLE [integration].[tAccessoriesSize] ADD CONSTRAINT [PK_tAccessoriesSize] PRIMARY KEY CLUSTERED  ([AccessoriesSizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAge] on [integration].[tAge]'
GO
ALTER TABLE [integration].[tAge] ADD CONSTRAINT [PK_tAge] PRIMARY KEY CLUSTERED  ([HyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBrandCreation] on [integration].[tBrandCreation]'
GO
ALTER TABLE [integration].[tBrandCreation] ADD CONSTRAINT [PK_tBrandCreation] PRIMARY KEY CLUSTERED  ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tChannelRestrictionBrand] on [integration].[tChannelRestrictionBrand]'
GO
ALTER TABLE [integration].[tChannelRestrictionBrand] ADD CONSTRAINT [PK_tChannelRestrictionBrand] PRIMARY KEY CLUSTERED  ([BrandId], [ProductRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tChannelRestrictionCategory] on [integration].[tChannelRestrictionCategory]'
GO
ALTER TABLE [integration].[tChannelRestrictionCategory] ADD CONSTRAINT [PK_tChannelRestrictionCategory] PRIMARY KEY CLUSTERED  ([CategoryId], [ProductRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tChannelRestrictionProduct] on [integration].[tChannelRestrictionProduct]'
GO
ALTER TABLE [integration].[tChannelRestrictionProduct] ADD CONSTRAINT [PK_tChannelRestrictionProduct] PRIMARY KEY CLUSTERED  ([ProductId], [ProductRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tDealOfTheDay] on [integration].[tDealOfTheDay]'
GO
ALTER TABLE [integration].[tDealOfTheDay] ADD CONSTRAINT [PK_tDealOfTheDay] PRIMARY KEY CLUSTERED  ([BlockId], [ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tempProduct] on [integration].[tempProduct]'
GO
ALTER TABLE [integration].[tempProduct] ADD CONSTRAINT [PK_tempProduct] PRIMARY KEY CLUSTERED  ([HYarticleId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tempProductPrice] on [integration].[tempProductPrice]'
GO
ALTER TABLE [integration].[tempProductPrice] ADD CONSTRAINT [PK_tempProductPrice] PRIMARY KEY CLUSTERED  ([HYarticleId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tempProductStockPosition] on [integration].[tempProductStockPosition]'
GO
ALTER TABLE [integration].[tempProductStockPosition] ADD CONSTRAINT [PK_tempProductStockPosition] PRIMARY KEY CLUSTERED  ([HYarticleId], [Lagerplats])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tLekmerArtNo] on [integration].[tLekmerArtNo]'
GO
ALTER TABLE [integration].[tLekmerArtNo] ADD CONSTRAINT [PK_tLekmerArtNo] PRIMARY KEY CLUSTERED  ([HYarticleId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tOrderLog] on [integration].[tOrderLog]'
GO
ALTER TABLE [integration].[tOrderLog] ADD CONSTRAINT [PK_tOrderLog] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tOrderQue] on [integration].[tOrderQue]'
GO
ALTER TABLE [integration].[tOrderQue] ADD CONSTRAINT [PK_tOrderQue] PRIMARY KEY CLUSTERED  ([OrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductOutOfStock] on [integration].[tProductOutOfStock]'
GO
ALTER TABLE [integration].[tProductOutOfStock] ADD CONSTRAINT [PK_tProductOutOfStock] PRIMARY KEY CLUSTERED  ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductRegistryRestrictions] on [integration].[tProductRegistryRestrictions]'
GO
ALTER TABLE [integration].[tProductRegistryRestrictions] ADD CONSTRAINT [PK_tProductRegistryRestrictions] PRIMARY KEY CLUSTERED  ([ProductId], [ProductRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductStockPosition] on [integration].[tProductStockPosition]'
GO
ALTER TABLE [integration].[tProductStockPosition] ADD CONSTRAINT [PK_tProductStockPosition] PRIMARY KEY CLUSTERED  ([HYarticleId], [Lagerplats])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductsTranslatedInHY] on [integration].[tProductsTranslatedInHY]'
GO
ALTER TABLE [integration].[tProductsTranslatedInHY] ADD CONSTRAINT [PK_tProductsTranslatedInHY] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductTitlesUpdatedInHY] on [integration].[tProductTitlesUpdatedInHY]'
GO
ALTER TABLE [integration].[tProductTitlesUpdatedInHY] ADD CONSTRAINT [PK_tProductTitlesUpdatedInHY] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tTradeDoublerProductGroupMapping] on [integration].[tTradeDoublerProductGroupMapping]'
GO
ALTER TABLE [integration].[tTradeDoublerProductGroupMapping] ADD CONSTRAINT [PK_tTradeDoublerProductGroupMapping] PRIMARY KEY CLUSTERED  ([HYArticleClassId], [ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tTag_Value] on [lekmer].[tTag]'
GO
CREATE NONCLUSTERED INDEX [IX_tTag_Value] ON [lekmer].[tTag] ([Value])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrder_CreatedDate] on [order].[tOrder]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrder_CreatedDate] ON [order].[tOrder] ([CreatedDate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrderItem_OrderId_(Quantity)] on [order].[tOrderItem]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrderItem_OrderId_(Quantity)] ON [order].[tOrderItem] ([OrderId]) INCLUDE ([Quantity])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrderItemProduct_ProductId] on [order].[tOrderItemProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrderItemProduct_ProductId] ON [order].[tOrderItemProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customer].[tCustomerComment]'
GO
ALTER TABLE [customer].[tCustomerComment] ADD
CONSTRAINT [FK_tCustomerComment_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customer].[tCustomerGroupCustomer]'
GO
ALTER TABLE [customer].[tCustomerGroupCustomer] ADD
CONSTRAINT [FK_tCustomerGroupCustomer_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customer].[tCustomerUser]'
GO
ALTER TABLE [customer].[tCustomerUser] ADD
CONSTRAINT [FK_tCustomer_tCustomerUser] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
