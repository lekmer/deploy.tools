SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [order].[tOrderPayment]'
GO
ALTER TABLE [order].[tOrderPayment] ADD
[Captured] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vOrderPayment]'
GO
ALTER VIEW [order].[vOrderPayment]
AS
	SELECT
		[OrderPaymentId] AS 'OrderPayment.OrderPaymentId',
		[OrderId] AS 'OrderPayment.OrderId',
		[PaymentTypeId] AS 'OrderPayment.PaymentTypeId',
		[Price] AS 'OrderPayment.Price',
		[Vat] AS 'OrderPayment.Vat',
		[ReferenceId] AS 'OrderPayment.ReferenceId',
		[Captured] AS 'OrderPayment.Captured'
	FROM
		[order].[tOrderPayment]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pOrder]'
GO

ALTER PROCEDURE [integration].[pOrder]
	@OrderId int
AS
begin
	set nocount on
	set transaction isolation level read uncommitted
	begin try
		
		--set statistics io on
		--set statistics time on
		
		-- Order
		select 		
			o.OrderId,
            o.FreightCost,
            lo.OptionalFreightCost,
            o.ChannelId as Channel,   
            op.OrderPaymentId, 
            pt.ErpId as PaymentMethod, 
            op.ReferenceId, 
            op.Price,
            --u.VoucherDiscount as VoucherAmount
            a1.ItemsActualPriceIncludingVat - op.Price as VoucherAmount,
			lci.IsCompany, -- NEW
			op.Captured as PaymentCaptured
		from
			[order].[tPaymentType] pt
			inner join [order].[tOrderPayment] op on op.PaymentTypeId = pt.PaymentTypeId
			inner join [order].[tOrder] o on op.OrderId = o.OrderId
			inner join [customerlek].[tCustomerInformation] lci on lci.CustomerId = o.CustomerId
			left join [lekmer].[tLekmerOrder] lo on lo.OrderId = o.OrderId
			cross apply (
				select sum(oi.ActualPriceIncludingVat * oi.Quantity) as 'ItemsActualPriceIncludingVat'
				from [order].[tOrderItem] oi
				where oi.OrderId = o.OrderId
			) as a1
			where
				o.OrderStatusId = 2
				and
				o.OrderId = @OrderId
		
		-- OrderRow // Old
		--select
		--	o.OrderItemId as OrderRowId, 
		--	p.Title, 
		--	o.Quantity, 
		--	p.ProductId,
		--	o.ActualPriceIncludingVat as Price, 
		--	o.OriginalPriceIncludingVat as OrdinaryPrice, 
		--	coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		--from
		--	[order].tOrderItem o
		--	left join [order].tOrderItemProduct p 
		--		on o.OrderItemId = p.OrderItemId
		--	left join lekmer.tOrderItemSize ois
		--		on ois.OrderItemId = o.OrderItemId
		--where
		--		o.OrderId = @OrderId 
				
		-- OrderRow
		select
			o.OrderItemId as OrderRowId, 
			p.Title, 
			o.Quantity, 
			p.ProductId,
			o.ActualPriceIncludingVat as Price, 
			o.OriginalPriceIncludingVat as OrdinaryPrice, 
			coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
			inner join lekmer.tLekmerProduct l
				on p.ProductId = l.ProductId
		where
			o.OrderId = @OrderId 
			and l.ProductTypeId != 2 -- package article
		UNION ALL
		select
			poi.OrderItemId as OrderRowId,
			poip.Title,
			poi.Quantity,
			poip.ProductId,
			poi.PackagePriceIncludingVat as Price,
			poi.OriginalPriceIncludingVat as OrdinaryPrice,
			coalesce(poip.[SizeErpId], poip.ErpId) as HyErpId
		from
			orderlek.tPackageOrderItem poi
			left join orderlek.tPackageOrderItemProduct poip
				on poi.PackageOrderItemId = poip.PackageOrderItemId
		where
			poi.OrderId = @OrderId
			
		-- Customer
		select 		
            c.CustomerId, 
			c.FirstName, 
			c.LastName,
            c.CivicNumber, 
            c.PhoneNumber,
            c.CellPhoneNumber as MobilePhoneNumber, 
            o.Email,
			case g.IsCompany when 1 then '05' else isnull(gt.ErpId, '00') end as Gender --NEW
		from	
			[order].torder o
			inner join [customer].tCustomerInformation c
				on c.CustomerId = o.CustomerId
			left join customerlek.tCustomerInformation g
				on c.CustomerId = g.CustomerId
			left join customerlek.tGenderType gt
				on g.GenderTypeId = gt.GenderTypeId
		where
			o.OrderId = @OrderId
		
		-- BillingAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.BillingAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- DeliveryAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.DeliveryAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- AlternativeAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].tOrder o
			inner join [lekmer].tLekmerOrder lo on lo.OrderId = o.OrderId
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = lo.AlternateAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId


		-- DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join [order].tDeliveryMethod d
				on o.DeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId
			
		-- Optional DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join lekmer.tLekmerOrder lo
				on lo.OrderId = o.OrderId
			inner join [order].tDeliveryMethod d
				on lo.OptionalDeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId

	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderPaymentSave]'
GO
ALTER PROCEDURE [order].[pOrderPaymentSave]
	@OrderPaymentId INT,
	@OrderId INT,
	@PaymentTypeId INT,
	@Price DECIMAL(16,2),
	@Vat DECIMAL(16,2),
	@ReferenceId VARCHAR(50),
	@Captured BIT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderPayment]
	SET
		[OrderId] = @OrderId,
		[PaymentTypeId] = @PaymentTypeId,
		[Price] = @Price,
		[Vat] = @VAT,
		[ReferenceId] = @ReferenceId,
		[Captured] = @Captured
	WHERE
		[OrderPaymentId] = @OrderPaymentId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderPayment]
		(
			[OrderId],
			[PaymentTypeId],
			[Price],
			[Vat],
			[ReferenceId],
			[Captured]
		)
		VALUES
		(
			@OrderId,
			@PaymentTypeId,
			@Price,
			@Vat,
			@ReferenceId,
			@Captured
		)

		SET @OrderPaymentId = CAST(SCOPE_IDENTITY() AS INT)
	END

	RETURN @OrderPaymentId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
