SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [lekmer].[tBlockEsalesAds]'
GO
CREATE TABLE [lekmer].[tBlockEsalesAds]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[PanelPath] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Format] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Gender] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ProductCategory] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BlockEsalesAds] on [lekmer].[tBlockEsalesAds]'
GO
ALTER TABLE [lekmer].[tBlockEsalesAds] ADD CONSTRAINT [PK_BlockEsalesAds] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockEsalesAds]'
GO


CREATE VIEW [lekmer].[vBlockEsalesAds]
AS
	SELECT
		b.*,
		bea.[ColumnCount] AS 'BlockEsalesAds.ColumnCount',
		bea.[RowCount] AS 'BlockEsalesAds.RowCount',
		bea.[PanelPath] AS 'BlockEsalesAds.PanelPath',
		bea.[Format] AS 'BlockEsalesAds.Format',
		bea.[Gender] AS 'BlockEsalesAds.Gender',
		bea.[ProductCategory] AS 'BlockEsalesAds.ProductCategory'
	FROM
		[lekmer].[tBlockEsalesAds] bea
		INNER JOIN [sitestructure].[vCustomBlock] b ON b.[Block.BlockId] = bea.[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockEsalesAdsGetById]'
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesAdsGetById]
	@LanguageId INT,
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vBlockEsalesAds] AS bea
	WHERE
		bea.[Block.BlockId] = @BlockId
		AND
		bea.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockEsalesAdsDelete]'
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesAdsDelete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[lekmer].[tBlockEsalesAds]
	WHERE
		[BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockEsalesAdsSecure]'
GO


CREATE VIEW [lekmer].[vBlockEsalesAdsSecure]
AS
	SELECT
		b.*,
		bea.[ColumnCount] AS 'BlockEsalesAds.ColumnCount',
		bea.[RowCount] AS 'BlockEsalesAds.RowCount',
		bea.[PanelPath] AS 'BlockEsalesAds.PanelPath',
		bea.[Format] AS 'BlockEsalesAds.Format',
		bea.[Gender] AS 'BlockEsalesAds.Gender',
		bea.[ProductCategory] AS 'BlockEsalesAds.ProductCategory'
	FROM
		[lekmer].[tBlockEsalesAds] bea
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON b.[Block.BlockId] = bea.[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockEsalesAdsGetByIdSecure]'
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesAdsGetByIdSecure]
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vBlockEsalesAdsSecure] AS bea
	WHERE
		bea.[Block.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockEsalesAdsSave]'
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesAdsSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@PanelPath NVARCHAR(MAX),
	@Format NVARCHAR(50),
	@Gender NVARCHAR(50),
	@ProductCategory NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tBlockEsalesAds]
	SET
		[ColumnCount]	= @ColumnCount,
		[RowCount]		= @RowCount,
		[PanelPath]		= @PanelPath,
		[Format]		= @Format,
		[Gender]		= @Gender,
		[ProductCategory] = @ProductCategory
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockEsalesAds]
	(
		[BlockId],
		[ColumnCount],
		[RowCount],
		[PanelPath],
		[Format],
		[Gender],
		[ProductCategory]
	)
	VALUES
	(
		@BlockId,
		@ColumnCount,
		@RowCount,
		@PanelPath,
		@Format,
		@Gender,
		@ProductCategory
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pProductChangeEventDeleteExpiredItems]'
GO
ALTER PROCEDURE [productlek].[pProductChangeEventDeleteExpiredItems]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ExpirationDate DATETIME
	SET @ExpirationDate = DATEADD(DAY, -5, GETDATE())

	DELETE 
		pce
	FROM
		[productlek].[tProductChangeEvent] pce
	WHERE 
		pce.[CreatedDate] < @ExpirationDate
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockEsalesAds]'
GO
ALTER TABLE [lekmer].[tBlockEsalesAds] ADD CONSTRAINT [FK_BlockEsalesAds_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
