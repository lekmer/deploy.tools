/*
Run this script on a database with the same schema as:

HeppoDB – the database with this schema will be modified

to synchronize its data with:

HeppoDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 11/23/2012 10:29:21 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraints from [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tEntity]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tFunctionType]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]

-- Drop constraint FK_tModelSetting_tModel from [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModel]

-- Add row to [sitestructure].[tBlockType]
INSERT INTO [sitestructure].[tBlockType] ([BlockTypeId], [Title], [CommonName], [Ordinal], [AvailableForAllPageTypes]) VALUES (1000038, N'eSales Ads', N'EsalesAds', 0, 1)

-- Add row to [template].[tEntity]
SET IDENTITY_INSERT [template].[tEntity] ON
INSERT INTO [template].[tEntity] ([EntityId], [CommonName], [Title]) VALUES (1000014, N'Ad', N'Ad')
SET IDENTITY_INSERT [template].[tEntity] OFF

-- Add rows to [template].[tModel]
SET IDENTITY_INSERT [template].[tModel] ON
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName], [DefaultTemplateId]) VALUES (1000056, 182, N'Trustpilot order confirm', N'TrustpilotOrderConfirmEmail', NULL)
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName], [DefaultTemplateId]) VALUES (1000057, 1000004, N'Block eSales Ads', N'BlockEsalesAds', NULL)
SET IDENTITY_INSERT [template].[tModel] OFF
-- Operation applied to 2 rows out of 2

-- Add rows to [template].[tEntityFunction]
SET IDENTITY_INSERT [template].[tEntityFunction] ON
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000162, 1000014, 1, N'Ad.Key', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000163, 1000014, 1, N'Ad.Ticket', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000164, 1000014, 1, N'Ad.CampaignKey', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000165, 1000014, 1, N'Ad.Format', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000166, 1000014, 1, N'Ad.Site', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000167, 1000014, 1, N'Ad.Gender', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000168, 1000014, 1, N'Ad.ProductCategory', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000169, 1000014, 1, N'Ad.Title', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000170, 1000014, 1, N'Ad.LandingPageUrl', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000171, 1000014, 1, N'Ad.ImageUrl', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000172, 1000014, 1, N'Ad.Tags', N'')
SET IDENTITY_INSERT [template].[tEntityFunction] OFF
-- Operation applied to 11 rows out of 11

-- Add rows to [template].[tModelFragmentRegion]
SET IDENTITY_INSERT [template].[tModelFragmentRegion] ON
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000193, 1000056, N'Content', 10)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000194, 1000056, N'E-mail fields', 20)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000195, 1000057, N'Content', 10)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000196, 1000057, N'Ad list', 20)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000197, 1000057, N'Ad', 30)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000198, 1000057, N'Head', 40)
SET IDENTITY_INSERT [template].[tModelFragmentRegion] OFF
-- Operation applied to 6 rows out of 6

-- Add rows to [template].[tModelFragment]
SET IDENTITY_INSERT [template].[tModelFragment] ON
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001217, 1000193, N'Content', N'Content', 1000056, 0, 20)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001218, 1000194, N'From name', N'FromName', 1000056, 0, 3)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001219, 1000194, N'From e-mail', N'FromEmail', 1000056, 10, 3)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001220, 1000194, N'Subject', N'Subject', 1000056, 20, 3)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001221, 1000195, N'Content', N'Content', 1000057, 0, 20)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001222, 1000196, N'Ad list', N'AdList', 1000057, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001223, 1000196, N'Ad row', N'AdRow', 1000057, 10, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001224, 1000196, N'Empty space', N'EmptySpace', 1000057, 20, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001225, 1000197, N'Ad', N'Ad', 1000057, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001226, 1000198, N'Head', N'Head', 1000057, 0, 10)
SET IDENTITY_INSERT [template].[tModelFragment] OFF
-- Operation applied to 10 rows out of 10

-- Add rows to [template].[tModelFragmentEntity]
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001221, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001221, 33)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001221, 1000010)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001225, 1000014)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001226, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001226, 33)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001226, 1000010)
-- Operation applied to 7 rows out of 7

-- Add rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
--INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000751, 1000053, 1, N'Order.DeliveryMethod.Id', N'')
--INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000752, 1000053, 1, N'Order.DeliveryMethod.Title', N'')
--INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000753, 1000053, 1, N'Order.DeliveryMethod.Alias', N'Use alias "Order.Checkout.OrderConfirmation.DeliveryMethod_[CommonName]" to controll variable content.')
--INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000754, 1000055, 1, N'Order.DeliveryMethod.Id.Json', N'')
--INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000755, 1000055, 1, N'Order.DeliveryMethod.Title.Json', N'')
--INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000756, 1000055, 1, N'Order.DeliveryMethod.Alias.Json', N'Use alias "Order.Checkout.OrderConfirmation.DeliveryMethod_[CommonName]" to controll variable content.')
--INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000757, 139, 1, N'Order.DeliveryMethod.Id', N'')
--INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000758, 139, 1, N'Order.DeliveryMethod.Alias', N'Use alias "Email.OrderConfirm.DeliveryMethod_[CommonName]" to controll variable content.')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000759, 1001217, 1, N'Order.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000760, 1001217, 1, N'Order.Number', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000761, 1001217, 1, N'Customer.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000762, 1001217, 1, N'Customer.Email', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000763, 1001220, 1, N'Order.Number', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000764, 1001217, 1, N'Order.Email', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000765, 1001221, 1, N'AdList', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000766, 1001222, 1, N'Iterate:AdRow', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000767, 1001223, 1, N'Iterate:Ad', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000768, 1001223, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000769, 1001223, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000770, 1001224, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000771, 1001224, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000772, 1001225, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000773, 1001225, 2, N'IsLast', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
-- Operation applied to 23 rows out of 23

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])

-- Add constraint FK_tModelSetting_tModel to [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] WITH CHECK ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
COMMIT TRANSACTION
GO
