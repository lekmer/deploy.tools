SELECT
	*,
	CAST(o.[PackagePriceIncludingVat] * (1 - 1 / 1.25 ) AS DECIMAL(16,2))
FROM
	[orderlek].tPackageOrderItem o
WHERE
	o.[VAT] = CAST(o.[PackagePriceIncludingVat] * (1 - 1 / 1.25 ) AS DECIMAL(16,2))
	
	
UPDATE [orderlek].tPackageOrderItem
SET
	[VAT] = CAST([PackagePriceIncludingVat] * (1 - 1 / 1.25 ) AS DECIMAL(16,2))
WHERE
	[VAT] != CAST([PackagePriceIncludingVat] * (1 - 1 / 1.25 ) AS DECIMAL(16,2))