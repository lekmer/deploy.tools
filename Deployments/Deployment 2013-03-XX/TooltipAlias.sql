BEGIN TRANSACTION

DECLARE @BackofficeFolderId INT
DECLARE @TooltipFolderId INT
DECLARE @CampaignFolderId INT
DECLARE @ConditionFolderId INT
DECLARE @ProductActionFolderId INT
DECLARE @CartActionFolderId INT

/*Folders*/

SELECT @BackofficeFolderId = [AliasFolderId] FROM [template].[tAliasFolder] af WHERE af.[Title] = N'Backoffice'
IF @BackofficeFolderId IS NULL
BEGIN
	INSERT INTO [template].[tAliasFolder] ( [Title], [ParentAliasFolderId] )
	VALUES ( N'Backoffice', NULL )

	SET @BackofficeFolderId = SCOPE_IDENTITY()
END


SELECT @TooltipFolderId = [AliasFolderId] FROM [template].[tAliasFolder] af WHERE af.[Title] = N'Tooltip'
IF @TooltipFolderId IS NULL
BEGIN
	INSERT INTO [template].[tAliasFolder] ( [Title], [ParentAliasFolderId] )
	VALUES ( N'Tooltip', @BackofficeFolderId )

	SET @TooltipFolderId = SCOPE_IDENTITY()
END


SELECT @CampaignFolderId = [AliasFolderId] FROM [template].[tAliasFolder] af WHERE af.[Title] = N'Campaign'
IF @CampaignFolderId IS NULL
BEGIN
	INSERT INTO [template].[tAliasFolder] ( [Title], [ParentAliasFolderId] )
	VALUES ( N'Campaign', @TooltipFolderId )

	SET @CampaignFolderId = SCOPE_IDENTITY()
END


SELECT @ConditionFolderId = [AliasFolderId] FROM [template].[tAliasFolder] af WHERE af.[Title] = N'Condition'
IF @ConditionFolderId IS NULL
BEGIN
	INSERT INTO [template].[tAliasFolder] ( [Title], [ParentAliasFolderId] )
	VALUES ( N'Condition', @TooltipFolderId )

	SET @ConditionFolderId = SCOPE_IDENTITY()
END


SELECT @ProductActionFolderId = [AliasFolderId] FROM [template].[tAliasFolder] af WHERE af.[Title] = N'Product action'
IF @ProductActionFolderId IS NULL
BEGIN
	INSERT INTO [template].[tAliasFolder] ( [Title], [ParentAliasFolderId] )
	VALUES ( N'Product action', @TooltipFolderId )

	SET @ProductActionFolderId = SCOPE_IDENTITY()
END


SELECT @CartActionFolderId = [AliasFolderId] FROM [template].[tAliasFolder] af WHERE af.[Title] = N'Cart action'
IF @CartActionFolderId IS NULL
BEGIN
	INSERT INTO [template].[tAliasFolder] ( [Title], [ParentAliasFolderId] )
	VALUES ( N'Cart action', @TooltipFolderId )

	SET @CartActionFolderId = SCOPE_IDENTITY()
END


/*Aliases*/

INSERT INTO [template].[tAlias] ( [AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description] )
VALUES
		( @CampaignFolderId,
		  'Backoffice.Tooltip.CampaignFlag',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CampaignFolderId,
		  'Backoffice.Tooltip.CampaignLevel',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CampaignFolderId,
		  'Backoffice.Tooltip.CampaignExclusiveCheckBox',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		)


INSERT INTO [template].[tAlias] ( [AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description] )
VALUES
		( @ConditionFolderId,
		  'Backoffice.ToolTip.CartValue',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ConditionFolderId,
		  'Backoffice.ToolTip.CartContains',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ConditionFolderId,
		  'Backoffice.ToolTip.Voucher',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ConditionFolderId,
		  'Backoffice.ToolTip.CartValueRange',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ConditionFolderId,
		  'Backoffice.ToolTip.CustomerGroupUser',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ConditionFolderId,
		  'Backoffice.ToolTip.CartItemsGroupValue',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ConditionFolderId,
		  'Backoffice.ToolTip.CartDoesNotContain',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		)
		
		
INSERT INTO [template].[tAlias] ( [AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description] )
VALUES
		( @ProductActionFolderId,
		  'Backoffice.ToolTip.PercentagePriceDiscount',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ProductActionFolderId,
		  'Backoffice.ToolTip.ProductDiscount',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ProductActionFolderId,
		  'Backoffice.ToolTip.FixedDiscount',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ProductActionFolderId,
		  'Backoffice.ToolTip.GiftCardViaMailProduct',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @ProductActionFolderId,
		  'Backoffice.ToolTip.FreeFreightFlag',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		)


INSERT INTO [template].[tAlias] ( [AliasFolderId], [CommonName], [AliasTypeId], [Value], [Description] )
VALUES		
		( @CartActionFolderId,
		  'Backoffice.ToolTip.FreightValue',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.CartItemPrice',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.VoucherDiscountActionType',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.PercentageDiscount',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.CartItemDiscount',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.FixedCartDiscount',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.ProductAutoFree',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.GiftCardViaMailCart',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.CartItemPercentageDiscount',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.CartItemFixedDiscount',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		),
		( @CartActionFolderId,
		  'Backoffice.ToolTip.CartItemGroupFixedDiscount',
		  1, -- Single line
		  N'Value', -- Value - nvarchar(max)
		  N''  -- Description - nvarchar(max)
		)

ROLLBACK