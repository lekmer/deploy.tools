/****** Object:  Table [lekmer].[tNewsletterType]    Script Date: 02/07/2013 10:02:35 ******/
INSERT [lekmer].[tNewsletterType] ([NewsletterTypeId], [Title], [CommonName]) VALUES (1, N'Interspire contact list', N'InterspireContactList')
INSERT [lekmer].[tNewsletterType] ([NewsletterTypeId], [Title], [CommonName]) VALUES (2, N'Monitor product', N'MonitorProduct')
INSERT [lekmer].[tNewsletterType] ([NewsletterTypeId], [Title], [CommonName]) VALUES (3, N'Order review', N'OrderReview')
/****** Object:  Table [lekmer].[tNewsletterSubscriberType]    Script Date: 02/07/2013 10:02:35 ******/
INSERT [lekmer].[tNewsletterSubscriberType] ([NewsletterSubscriberTypeId], [Title], [CommonName]) VALUES (1, N'Manual', N'Manual')
INSERT [lekmer].[tNewsletterSubscriberType] ([NewsletterSubscriberTypeId], [Title], [CommonName]) VALUES (2, N'OrderBased', N'OrderBased')
