/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 2/7/2013 10:01:27 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraint FK_tModelFragmentEntity_tModelFragment from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraint FK_tModelFragmentFunction_tModelFragment from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Add row to [template].[tModelFragmentRegion]
SET IDENTITY_INSERT [template].[tModelFragmentRegion] ON
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000199, 1000020, N'Unregistration Options', 30)
SET IDENTITY_INSERT [template].[tModelFragmentRegion] OFF

-- Add rows to [template].[tModelFragment]
SET IDENTITY_INSERT [template].[tModelFragment] ON
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001227, 1000199, N'Unregistration Option List', N'UnregistrationOptionList', 1000020, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001228, 1000199, N'Unregistration Option', N'UnregistrationOption', 1000020, 10, 10)
SET IDENTITY_INSERT [template].[tModelFragment] OFF
-- Operation applied to 2 rows out of 2

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraint FK_tModelFragmentEntity_tModelFragment to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] WITH CHECK ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraint FK_tModelFragmentFunction_tModelFragment to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] WITH CHECK ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
COMMIT TRANSACTION
GO
