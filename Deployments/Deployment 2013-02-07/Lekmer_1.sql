SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tNewsletterSubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterSubscriber] DROP CONSTRAINT [FK_tNewsletterSubscriber_tChannel]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tNewsletterSubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterSubscriber] DROP CONSTRAINT [PK_tNewsletterSubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tNewsletterSubscriber_Email] from [lekmer].[tNewsletterSubscriber]'
GO
DROP INDEX [IX_tNewsletterSubscriber_Email] ON [lekmer].[tNewsletterSubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tNewsletterSubscriber_ChannelId] from [lekmer].[tNewsletterSubscriber]'
GO
DROP INDEX [IX_tNewsletterSubscriber_ChannelId] ON [lekmer].[tNewsletterSubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tNewsletterSubscriber_SentStatus] from [lekmer].[tNewsletterSubscriber]'
GO
DROP INDEX [IX_tNewsletterSubscriber_SentStatus] ON [lekmer].[tNewsletterSubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [integration].[usp_UpdateTNewsletterSubscriber]'
GO
DROP PROCEDURE [integration].[usp_UpdateTNewsletterSubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [integration].[usp_ChangeStatusTNewsletterSubscriber]'
GO
DROP PROCEDURE [integration].[usp_ChangeStatusTNewsletterSubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [integration].[usp_GetAllNewSubscribers]'
GO
DROP PROCEDURE [integration].[usp_GetAllNewSubscribers]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[tSubDomainUrlType]'
GO
CREATE TABLE [corelek].[tSubDomainUrlType]
(
[SubDomainUrlTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSubDomainUrlType] on [corelek].[tSubDomainUrlType]'
GO
ALTER TABLE [corelek].[tSubDomainUrlType] ADD CONSTRAINT [PK_tSubDomainUrlType] PRIMARY KEY CLUSTERED  ([SubDomainUrlTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[tSubDomain]'
GO
CREATE TABLE [corelek].[tSubDomain]
(
[SubDomainId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DomainUrl] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[UrlTypeId] [int] NOT NULL,
[ContentTypeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSubDomain] on [corelek].[tSubDomain]'
GO
ALTER TABLE [corelek].[tSubDomain] ADD CONSTRAINT [PK_tSubDomain] PRIMARY KEY CLUSTERED  ([SubDomainId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[tSubDomainContentType]'
GO
CREATE TABLE [corelek].[tSubDomainContentType]
(
[SubDomainContentTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSubDomainContentType] on [corelek].[tSubDomainContentType]'
GO
ALTER TABLE [corelek].[tSubDomainContentType] ADD CONSTRAINT [PK_tSubDomainContentType] PRIMARY KEY CLUSTERED  ([SubDomainContentTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[tSubDomainChannel]'
GO
CREATE TABLE [corelek].[tSubDomainChannel]
(
[ChannelId] [int] NOT NULL,
[SubDomainId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSubDomainChannel] on [corelek].[tSubDomainChannel]'
GO
ALTER TABLE [corelek].[tSubDomainChannel] ADD CONSTRAINT [PK_tSubDomainChannel] PRIMARY KEY CLUSTERED  ([ChannelId], [SubDomainId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tNewsletterSubscriber]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tNewsletterSubscriber]
(
[SubscriberId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[SubscriberTypeId] [int] NULL,
[SentStatus] [bit] NULL,
[CreatedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tNewsletterSubscriber] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tNewsletterSubscriber]([SubscriberId], [ChannelId], [Email], [SentStatus], [CreatedDate], [UpdatedDate]) SELECT [SubscriberId], [ChannelId], [Email], [SentStatus], [CreatedDate], [CreatedDate] FROM [lekmer].[tNewsletterSubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tNewsletterSubscriber] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[lekmer].[tNewsletterSubscriber]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[lekmer].[tmp_rg_xx_tNewsletterSubscriber]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tNewsletterSubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tNewsletterSubscriber]', N'tNewsletterSubscriber'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tNewsletterSubscriber] on [lekmer].[tNewsletterSubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterSubscriber] ADD CONSTRAINT [PK_tNewsletterSubscriber] PRIMARY KEY CLUSTERED  ([SubscriberId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tNewsletterSubscriber_ChannelId] on [lekmer].[tNewsletterSubscriber]'
GO
CREATE NONCLUSTERED INDEX [IX_tNewsletterSubscriber_ChannelId] ON [lekmer].[tNewsletterSubscriber] ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tNewsletterSubscriber_Email] on [lekmer].[tNewsletterSubscriber]'
GO
CREATE NONCLUSTERED INDEX [IX_tNewsletterSubscriber_Email] ON [lekmer].[tNewsletterSubscriber] ([Email])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tNewsletterSubscriber_SentStatus] on [lekmer].[tNewsletterSubscriber]'
GO
CREATE NONCLUSTERED INDEX [IX_tNewsletterSubscriber_SentStatus] ON [lekmer].[tNewsletterSubscriber] ([SentStatus])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tNewsletterSubscriberType]'
GO
CREATE TABLE [lekmer].[tNewsletterSubscriberType]
(
[NewsletterSubscriberTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tNewsletterSubscriberType] on [lekmer].[tNewsletterSubscriberType]'
GO
ALTER TABLE [lekmer].[tNewsletterSubscriberType] ADD CONSTRAINT [PK_tNewsletterSubscriberType] PRIMARY KEY CLUSTERED  ([NewsletterSubscriberTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tNewsletterType]'
GO
CREATE TABLE [lekmer].[tNewsletterType]
(
[NewsletterTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tNewsletterType] on [lekmer].[tNewsletterType]'
GO
ALTER TABLE [lekmer].[tNewsletterType] ADD CONSTRAINT [PK_tNewsletterType] PRIMARY KEY CLUSTERED  ([NewsletterTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tNewsletterUnsubscriberOption]'
GO
CREATE TABLE [lekmer].[tNewsletterUnsubscriberOption]
(
[UnsubscriberOprionId] [int] NOT NULL IDENTITY(1, 1),
[UnsubscriberId] [int] NOT NULL,
[NewsletterTypeId] [int] NOT NULL,
[InterspireContactList] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tNewsletterUnsubscriberOption] on [lekmer].[tNewsletterUnsubscriberOption]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] ADD CONSTRAINT [PK_tNewsletterUnsubscriberOption] PRIMARY KEY CLUSTERED  ([UnsubscriberOprionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tNewsletterUnsubscriberOption_Unique] on [lekmer].[tNewsletterUnsubscriberOption]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tNewsletterUnsubscriberOption_Unique] ON [lekmer].[tNewsletterUnsubscriberOption] ([UnsubscriberId], [NewsletterTypeId], [InterspireContactList])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tNewsletterUnsubscriber]'
GO
CREATE TABLE [lekmer].[tNewsletterUnsubscriber]
(
[UnsubscriberId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[UnregFromAll] [bit] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tNewsletterUnsubscriber] on [lekmer].[tNewsletterUnsubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriber] ADD CONSTRAINT [PK_tNewsletterUnsubscriber] PRIMARY KEY CLUSTERED  ([UnsubscriberId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryRestrictionCategoryGetAllByCategory]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryRestrictionCategoryGetAllByCategory]
	@CategoryId		INT
AS
BEGIN
	SELECT
		[rc].[ProductRegistryId] 'ProductRegistryId',
		[rc].[CategoryId] 'ItemId',
		[rc].[RestrictionReason] 'RestrictionReason',
		[rc].[UserId] 'UserId',
		[rc].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM 
		[lekmer].[tProductRegistryRestrictionCategory] rc
		INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
	WHERE
		[rc].[CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryRestrictionProductGetAllWithChannel]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryRestrictionProductGetAllWithChannel]
AS
BEGIN
	SELECT
		[rp].[ProductRegistryId] 'ProductRegistryId',
		[rp].[ProductId] 'ItemId',
		[rp].[RestrictionReason] 'RestrictionReason',
		[rp].[UserId] 'UserId',
		[rp].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [lekmer].[tProductRegistryRestrictionProduct] rp
	INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rp].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryRestrictionCategoryGetAllWithChannel]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryRestrictionCategoryGetAllWithChannel]
AS
BEGIN
	SELECT 
		DISTINCT [src].[CategoryId] 'ItemId',
		[rc].[ProductRegistryId] 'ProductRegistryId',
		[rc].[RestrictionReason] 'RestrictionReason',
		[rc].[UserId] 'UserId',
		[rc].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [lekmer].[tProductRegistryRestrictionCategory] rc
		 CROSS APPLY [product].[fnGetSubCategories] (rc.CategoryId) src
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[vSubDomain]'
GO



CREATE VIEW [corelek].[vSubDomain]
AS
SELECT
	[sb].[SubDomainId] AS 'SubDomain.SubDomainId',
	[sb].[CommonName] AS 'SubDomain.CommonName',
	[sb].[DomainUrl] AS 'SubDomain.DomainUrl',
	[sb].[UrlTypeId] AS 'SubDomain.UrlTypeId',
	[sb].[ContentTypeId] AS 'SubDomain.ContentTypeId'
FROM 
	[corelek].[tSubDomain] sb


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[pSubDomainGetAllByChannelAndContentType]'
GO
CREATE PROCEDURE [corelek].[pSubDomainGetAllByChannelAndContentType]
	@ChannelId		INT,
	@ContentTypeId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[corelek].[vSubDomain] sd
		INNER JOIN [corelek].[tSubDomainChannel] sdc ON sdc.[SubDomainId] = sd.[SubDomain.SubDomainId]
	WHERE
		[sdc].[ChannelId] = @ChannelId
		AND
		[sd].[SubDomain.ContentTypeId] = @ContentTypeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vNewsletterSubscriber]'
GO

CREATE VIEW [lekmer].[vNewsletterSubscriber]
AS
SELECT
	[SubscriberId] AS 'NewsletterSubscriber.SubscriberId',
	[ChannelId] AS 'NewsletterSubscriber.ChannelId',
	[Email] AS 'NewsletterSubscriber.Email',
	[SubscriberTypeId] AS 'NewsletterSubscriber.SubscriberTypeId',
	[SentStatus] AS 'NewsletterSubscriber.SentStatus',
	[CreatedDate] AS 'NewsletterSubscriber.CreatedDate',
	[UpdatedDate] AS 'NewsletterSubscriber.UpdatedDate'
FROM
	[lekmer].[tNewsletterSubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterSubscriberGetAllWithNotSentStatus]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberGetAllWithNotSentStatus]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vNewsletterSubscriber] ns
	WHERE
		ns.[NewsletterSubscriber.SentStatus] = 0
		OR 
		ns.[NewsletterSubscriber.SentStatus] IS NULL
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vNewsletterUnsubscriberOption]'
GO


CREATE VIEW [lekmer].[vNewsletterUnsubscriberOption]
AS
SELECT
	[UnsubscriberOprionId] AS 'NewsletterUnsubscriberOption.UnsubscriberOprionId',
	[UnsubscriberId] AS 'NewsletterUnsubscriberOption.UnsubscriberId',
	[NewsletterTypeId] AS 'NewsletterUnsubscriberOption.NewsletterTypeId',
	[InterspireContactList] AS 'NewsletterUnsubscriberOption.InterspireContactList',
	[CreatedDate] AS 'NewsletterUnsubscriberOption.CreatedDate'
FROM
	[lekmer].[tNewsletterUnsubscriberOption]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriberOption_GetByUnsubscriber]'
GO

CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriberOption_GetByUnsubscriber]
	@UnsubscriberId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vNewsletterUnsubscriberOption]
	WHERE
		[NewsletterUnsubscriberOption.UnsubscriberId] = @UnsubscriberId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriberOption_Delete]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriberOption_Delete]
	@UnsubscriberOprionId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[lekmer].[tNewsletterUnsubscriberOption]
	WHERE
		[UnsubscriberOprionId] = @UnsubscriberOprionId

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriber_Save]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_Save]
	@ChannelId		INT,
	@Email			VARCHAR(320),
	@UnregFromAll	BIT,
	@CreatedDate	DATETIME,
	@UpdateDate		DATETIME
AS
BEGIN
	DECLARE @Id INT
	UPDATE
		[lekmer].[tNewsletterUnsubscriber]
	SET
		[UnregFromAll] = @UnregFromAll,
		[UpdatedDate] = @UpdateDate,
		@Id = [UnsubscriberId]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tNewsletterUnsubscriber]
		(
			[ChannelId],
			[Email],
			[UnregFromAll],
			[CreatedDate],
			[UpdatedDate]
		)
		VALUES
		(
			@ChannelId,
			@Email,
			@UnregFromAll,
			@CreatedDate,
			@UpdateDate
		)
		SET @Id = SCOPE_IDENTITY()
	END
	RETURN @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriber_Delete]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_Delete]
	@ChannelId	INT,
	@Email		VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Remove Options
	DELETE
		[lekmer].[tNewsletterUnsubscriberOption]
	FROM
		[lekmer].[tNewsletterUnsubscriber] nu
		INNER JOIN [lekmer].[tNewsletterUnsubscriberOption] nuo ON [nuo].[UnsubscriberId] = [nu].[UnsubscriberId]
	WHERE
		nu.[Email] = @Email AND
		nu.[ChannelId] = @ChannelId
	
	DELETE
		[lekmer].[tNewsletterUnsubscriber]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
		
	RETURN @@ROWCOUNT
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vNewsletterUnsubscriber]'
GO

CREATE VIEW [lekmer].[vNewsletterUnsubscriber]
AS
SELECT
	[UnsubscriberId] AS 'NewsletterUnsubscriber.UnsubscriberId',
	[ChannelId] AS 'NewsletterUnsubscriber.ChannelId',
	[Email] AS 'NewsletterUnsubscriber.Email',
	[UnregFromAll] AS 'NewsletterUnsubscriber.UnregFromAll',
	[CreatedDate] AS 'NewsletterUnsubscriber.CreatedDate',
	[UpdatedDate] AS 'NewsletterUnsubscriber.UpdatedDate'
FROM
	[lekmer].[tNewsletterUnsubscriber]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderProductCampaignSave]'
GO
ALTER PROCEDURE [order].[pOrderProductCampaignSave]
	@Id				INT,
	@CampaignId		INT,
	@OrderItemId	INT,
	@Title			NVARCHAR(500)
AS
BEGIN
	UPDATE
		[order].tOrderProductCampaign
	SET
		CampaignId = @CampaignId,
		OrderItemId = @OrderItemId,
		Title = @Title
	WHERE
		OrderProductCampaignId = @Id
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [order].tOrderProductCampaign (
			CampaignId,
			OrderItemId,
			Title
		)
		VALUES (
			@CampaignId,
			@OrderItemId,
			@Title
		)
		
		SET @Id = SCOPE_IDENTITY();
	END
	
	RETURN @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryRestrictionBrandGetAllWithChannel]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryRestrictionBrandGetAllWithChannel]
AS
BEGIN
	SELECT
		[rb].[ProductRegistryId] 'ProductRegistryId',
		[rb].[BrandId] 'ItemId',
		[rb].[RestrictionReason] 'RestrictionReason',
		[rb].[UserId] 'UserId',
		[rb].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM 
		[lekmer].[tProductRegistryRestrictionBrand] rb
		INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rb].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterSubscriberSetSentStatus]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberSetSentStatus]
	@SubscriberId INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE 
		[lekmer].[tNewsletterSubscriber]
	SET
		[SentStatus] = 1
	WHERE
		[SubscriberId] = @SubscriberId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriber_GetByEmail]'
GO

CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_GetByEmail]
	@ChannelId	INT,
	@Email		VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[lekmer].[vNewsletterUnsubscriber]
	WHERE
		[NewsletterUnsubscriber.ChannelId] = @ChannelId
		AND
		[NewsletterUnsubscriber.Email] = @Email
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryRestrictionBrandGetAllByBrand]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryRestrictionBrandGetAllByBrand]
	@BrandId		INT
AS
BEGIN
	SELECT
		[rb].[ProductRegistryId] 'ProductRegistryId',
		[rb].[BrandId] 'ItemId',
		[rb].[RestrictionReason] 'RestrictionReason',
		[rb].[UserId] 'UserId',
		[rb].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM 
		[lekmer].[tProductRegistryRestrictionBrand] rb
		INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rb].[ProductRegistryId]
	WHERE
		[rb].[BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[pSubDomainGetAllByContentType]'
GO
CREATE PROCEDURE [corelek].[pSubDomainGetAllByContentType]
	@ContentTypeId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[corelek].[vSubDomain] sd
	WHERE
		[sd].[SubDomain.ContentTypeId] = @ContentTypeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryRestrictionProductGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryRestrictionProductGetAllByProduct]
	@ProductId		INT
AS
BEGIN
	SELECT
		[rp].[ProductRegistryId] 'ProductRegistryId',
		[rp].[ProductId] 'ItemId',
		[rp].[RestrictionReason] 'RestrictionReason',
		[rp].[UserId] 'UserId',
		[rp].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM 
		[lekmer].[tProductRegistryRestrictionProduct] rp
		INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rp].[ProductRegistryId]
	WHERE
		[rp].[ProductId] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterUnsubscriberOption_Save]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriberOption_Save]
	@UnsubscriberId INT,
	@NewsletterTypeId INT,
	@InterspireContactList NVARCHAR(100),
	@CreatedDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [lekmer].[tNewsletterUnsubscriberOption]
	(
		[UnsubscriberId],
		[NewsletterTypeId],
		[InterspireContactList],
		[CreatedDate]
	)
	VALUES
	(
		@UnsubscriberId,
		@NewsletterTypeId,
		@InterspireContactList,
		@CreatedDate
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pNewsletterSubscriberSave]'
GO
ALTER PROCEDURE [lekmer].[pNewsletterSubscriberSave]
	@ChannelId INT,
	@Email VARCHAR(320),
	@SubscriberTypeId INT,
	@CreatedDate DATETIME,
	@UpdateDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Id INT
	
	UPDATE
		[lekmer].[tNewsletterSubscriber]
	SET
		[SubscriberTypeId] = @SubscriberTypeId,
		[UpdatedDate] = @UpdateDate,
		@Id = [SubscriberId]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tNewsletterSubscriber]
		( 
			[ChannelId],
			[Email],
			[SubscriberTypeId],
			[CreatedDate],
			[UpdatedDate]
		)
		VALUES
		(
			@ChannelId,
			@Email,
			@SubscriberTypeId,
			@CreatedDate,
			@UpdateDate
		)
		
		SET @Id = SCOPE_IDENTITY()
	END
	
	RETURN @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pMonitorProductDeleteByEmail]'
GO
CREATE PROCEDURE [lekmer].[pMonitorProductDeleteByEmail]
	@ChannelId INT,
	@Email VARCHAR(320)
AS 
BEGIN
	SET NOCOUNT ON
	
	DELETE
		[lekmer].[tMonitorProduct]
	WHERE
		[ChannelId] = @ChannelId
		AND [Email] = @Email
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pNewsletterSubscriberUpdateFromOrders]'
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberUpdateFromOrders]
AS
BEGIN
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			
			DECLARE @CreatedDate DATETIME = GETDATE()

			INSERT INTO
				[lekmer].[tNewsletterSubscriber] (
					[ChannelId],
					[Email],
					[SubscriberTypeId],
					[SentStatus],
					[CreatedDate],
					[UpdatedDate]
				)
			SELECT DISTINCT
				o.[ChannelId],
				o.[Email],
				2, -- OrderBased,
				NULL,
				@CreatedDate,
				@CreatedDate
			FROM
				[order].[tOrder] o WITH (NOLOCK)
			WHERE
				[o].[OrderStatusId] = 4 -- In HY
				AND [o].[Email] LIKE '%@%'
				AND NOT EXISTS
				(
					SELECT 1
					FROM [lekmer].[tNewsletterSubscriber] n
					WHERE [n].[ChannelId] = [o].[ChannelId] AND [n].[Email] = [o].[Email]
				)
				AND NOT EXISTS
				(
					SELECT 1
					FROM [lekmer].[tNewsletterUnsubscriber] un
					WHERE [un].[ChannelId] = [o].[ChannelId] AND [un].[Email] = [o].[Email]
				)

			COMMIT
		END TRY

		BEGIN CATCH
			INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
			VALUES (NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
		END CATCH
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [corelek].[tSubDomain]'
GO
ALTER TABLE [corelek].[tSubDomain] ADD CONSTRAINT [FK_tSubDomain_tSubDomain] FOREIGN KEY ([ContentTypeId]) REFERENCES [corelek].[tSubDomainContentType] ([SubDomainContentTypeId])
ALTER TABLE [corelek].[tSubDomain] ADD CONSTRAINT [FK_tSubDomain_tSubDomainUrlType] FOREIGN KEY ([UrlTypeId]) REFERENCES [corelek].[tSubDomainUrlType] ([SubDomainUrlTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [corelek].[tSubDomainChannel]'
GO
ALTER TABLE [corelek].[tSubDomainChannel] ADD CONSTRAINT [FK_tSubDomainChannel_tSubDomain] FOREIGN KEY ([SubDomainId]) REFERENCES [corelek].[tSubDomain] ([SubDomainId])
ALTER TABLE [corelek].[tSubDomainChannel] ADD CONSTRAINT [FK_tSubDomainChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tNewsletterSubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterSubscriber] ADD CONSTRAINT [FK_tNewsletterSubscriber_tNewslettertSubscriberType] FOREIGN KEY ([SubscriberTypeId]) REFERENCES [lekmer].[tNewsletterSubscriberType] ([NewsletterSubscriberTypeId])
ALTER TABLE [lekmer].[tNewsletterSubscriber] ADD CONSTRAINT [FK_tNewsletterSubscriber_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tNewsletterUnsubscriberOption]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] ADD CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterType] FOREIGN KEY ([NewsletterTypeId]) REFERENCES [lekmer].[tNewsletterType] ([NewsletterTypeId])
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] ADD CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterUnsubscriber] FOREIGN KEY ([UnsubscriberId]) REFERENCES [lekmer].[tNewsletterUnsubscriber] ([UnsubscriberId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tNewsletterUnsubscriber]'
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriber] ADD CONSTRAINT [FK_tNewsletterUnsubscriber_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
