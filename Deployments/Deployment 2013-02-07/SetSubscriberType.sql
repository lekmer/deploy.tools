UPDATE [lekmer].[tNewsletterSubscriber]
SET [SubscriberTypeId] = 1
WHERE [CreatedDate] IN 
(
SELECT [CreatedDate]
FROM [lekmer].[tNewsletterSubscriber]
GROUP BY [CreatedDate]
HAVING COUNT(*) = 1
)

UPDATE [lekmer].[tNewsletterSubscriber]
SET [SubscriberTypeId] = 2
WHERE [CreatedDate] IN 
(
SELECT [CreatedDate]
FROM [lekmer].[tNewsletterSubscriber]
GROUP BY [CreatedDate]
HAVING COUNT(*) > 1
)