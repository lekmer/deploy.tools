select * from core.tChannel

select * from corelek.tSubDomainContentType
select * from corelek.tSubDomainUrlType

select * from corelek.tSubDomain
select * from corelek.tSubDomainChannel

insert into corelek.tSubDomain ( CommonName, DomainUrl, UrlTypeId, ContentTypeId )
values
(      'static.heppo.com',      'm1.heppo.com', 1, 1 ),
(     'media-1.heppo.com',      'm1.heppo.com', 1, 2 ),
(     'media-2.heppo.com',      'm2.heppo.com', 1, 2 ),
( 'media.admin.heppo.com',      'm1.heppo.com', 1, 3 ),
(       'media.heppo.com',      'm1.heppo.com', 1, 4 )

insert into corelek.tSubDomainChannel ( ChannelId, SubDomainId )
select 1, sd.SubDomainId
from corelek.tSubDomain sd
where [ContentTypeId] != 3

insert into corelek.tSubDomainChannel ( ChannelId, SubDomainId )
select 2, sd.SubDomainId
from corelek.tSubDomain sd
where [ContentTypeId] != 3

insert into corelek.tSubDomainChannel ( ChannelId, SubDomainId )
select 3, sd.SubDomainId
from corelek.tSubDomain sd
where [ContentTypeId] != 3

insert into corelek.tSubDomainChannel ( ChannelId, SubDomainId )
select 4, sd.SubDomainId
from corelek.tSubDomain sd
where [ContentTypeId] != 3

insert into corelek.tSubDomainChannel ( ChannelId, SubDomainId )
select 5, sd.SubDomainId
from corelek.tSubDomain sd
where [ContentTypeId] != 3

insert into corelek.tSubDomainChannel ( ChannelId, SubDomainId )
select 1000005, sd.SubDomainId
from corelek.tSubDomain sd
where [ContentTypeId] != 3