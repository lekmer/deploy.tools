/*
Run this script on:

10.150.43.52.Lekmer_live – this database will be modified

to synchronize its data with:

D:\Projects\lmheppo\Scensum\trunk\Release\Release 023\DB\LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 9.0.0 from Red Gate Software Ltd at 21.08.2012 11:43:22

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Add 9 rows to [lekmer].[tEsalesRecommendationType]
INSERT INTO [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId], [Title], [CommonName]) VALUES (1, N'Abandoned Carts', N'AbandonedCarts')
INSERT INTO [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId], [Title], [CommonName]) VALUES (2, N'Recently Viewed', N'RecentlyViewed')
INSERT INTO [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId], [Title], [CommonName]) VALUES (3, N'Top Sellers', N'TopSellers')
INSERT INTO [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId], [Title], [CommonName]) VALUES (4, N'Those Who Bought Also Bought', N'ThoseWhoBoughtAlsoBought')
INSERT INTO [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId], [Title], [CommonName]) VALUES (5, N'Those Who Viewed Also Viewed', N'ThoseWhoViewedAlsoViewed')
INSERT INTO [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId], [Title], [CommonName]) VALUES (6, N'Those Who Viewed Bought', N'ThoseWhoViewedBought')
INSERT INTO [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId], [Title], [CommonName]) VALUES (7, N'Recommend Based On Cart', N'RecommendBasedOnCart')
INSERT INTO [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId], [Title], [CommonName]) VALUES (8, N'Recommend Based On Customer', N'RecommendBasedOnCustomer')
INSERT INTO [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId], [Title], [CommonName]) VALUES (9, N'Recommend Based On Product', N'RecommendBasedOnProduct')

-- Add 4 rows to [integration].[tCdonExportStatus]
SET IDENTITY_INSERT [integration].[tCdonExportStatus] ON
INSERT INTO [integration].[tCdonExportStatus] ([CdonExportStatusId], [CommonName], [Title]) VALUES (1, N'InProgress', N'InProgress')
INSERT INTO [integration].[tCdonExportStatus] ([CdonExportStatusId], [CommonName], [Title]) VALUES (2, N'Finished', N'Finished')
INSERT INTO [integration].[tCdonExportStatus] ([CdonExportStatusId], [CommonName], [Title]) VALUES (3, N'Failed', N'Failed')
INSERT INTO [integration].[tCdonExportStatus] ([CdonExportStatusId], [CommonName], [Title]) VALUES (4, N'InvalidXml', N'InvalidXml')
SET IDENTITY_INSERT [integration].[tCdonExportStatus] OFF
COMMIT TRANSACTION
GO
