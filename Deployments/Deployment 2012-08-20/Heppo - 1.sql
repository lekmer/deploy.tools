/*
Run this script on:

        10.150.43.52.Heppo_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 023\DB\HeppoDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 20.08.2012 13:55:48

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [integration].[tCdonExportStatus]'
GO
CREATE TABLE [integration].[tCdonExportStatus]
(
[CdonExportStatusId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCdonExportStatus] on [integration].[tCdonExportStatus]'
GO
ALTER TABLE [integration].[tCdonExportStatus] ADD CONSTRAINT [PK_tCdonExportStatus] PRIMARY KEY CLUSTERED  ([CdonExportStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[tCdonExport]'
GO
CREATE TABLE [integration].[tCdonExport]
(
[CdonExportId] [int] NOT NULL IDENTITY(1, 1),
[ImportId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Type] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NULL,
[Status] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__tCdonExp__5D7427525942F780] on [integration].[tCdonExport]'
GO
ALTER TABLE [integration].[tCdonExport] ADD CONSTRAINT [PK__tCdonExp__5D7427525942F780] PRIMARY KEY CLUSTERED  ([CdonExportId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tBlockEsalesRecommend]'
GO
CREATE TABLE [lekmer].[tBlockEsalesRecommend]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[EsalesRecommendationTypeId] [int] NULL,
[PanelPath] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BlockEsalesRecommend] on [lekmer].[tBlockEsalesRecommend]'
GO
ALTER TABLE [lekmer].[tBlockEsalesRecommend] ADD CONSTRAINT [PK_BlockEsalesRecommend] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tEsalesRecommendationType]'
GO
CREATE TABLE [lekmer].[tEsalesRecommendationType]
(
[EsalesRecommendationTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_EsalesRecommendationType] on [lekmer].[tEsalesRecommendationType]'
GO
ALTER TABLE [lekmer].[tEsalesRecommendationType] ADD CONSTRAINT [PK_EsalesRecommendationType] PRIMARY KEY CLUSTERED  ([EsalesRecommendationTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tFreeFreightFlagAction]'
GO
CREATE TABLE [lekmer].[tFreeFreightFlagAction]
(
[ProductActionId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFreeFreightFlagAction] on [lekmer].[tFreeFreightFlagAction]'
GO
ALTER TABLE [lekmer].[tFreeFreightFlagAction] ADD CONSTRAINT [PK_tFreeFreightFlagAction] PRIMARY KEY CLUSTERED  ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tFreeFreightFlagActionCurrency]'
GO
CREATE TABLE [lekmer].[tFreeFreightFlagActionCurrency]
(
[ProductActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFreeFreightFlagActionCurrency] on [lekmer].[tFreeFreightFlagActionCurrency]'
GO
ALTER TABLE [lekmer].[tFreeFreightFlagActionCurrency] ADD CONSTRAINT [PK_tFreeFreightFlagActionCurrency] PRIMARY KEY CLUSTERED  ([ProductActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pCdonExportGetLastItem]'
GO
CREATE PROCEDURE [integration].[pCdonExportGetLastItem]
AS
BEGIN
	SELECT 
		TOP (1) ImportId
	FROM
		[integration].[tCdonExport]
	ORDER BY CdonExportId DESC 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pCdonExportSave]'
GO
CREATE PROCEDURE [integration].[pCdonExportSave]
@ImportId			NVARCHAR(50),
@Type				VARCHAR(50),
@StatusCommonName	VARCHAR(50)
AS	
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @StatusId INT
	SET @StatusId = (SELECT CdonExportStatusId FROM [integration].[tCdonExportStatus] WHERE CommonName = @StatusCommonName)
	
	INSERT INTO	[integration].[tCdonExport]
		(
			[ImportId],
			[Type],
			[StartDate],
			[Status]
		)
	VALUES
		(
			@ImportId,
			@Type,
			GETDATE(),
			@StatusId
		)		
	
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pCdonExportUpdate]'
GO
CREATE PROCEDURE [integration].[pCdonExportUpdate]
@CdonExportId		INT,
@StatusCommonName	VARCHAR(50)
AS	
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @StatusId INT
	SET @StatusId = (SELECT CdonExportStatusId FROM [integration].[tCdonExportStatus] WHERE CommonName = @StatusCommonName)
	
	UPDATE
		[integration].[tCdonExport]
	SET
		[Status] = @StatusId
		,EndDate = GETDATE()
	WHERE 
		[CdonExportId] = @CdonExportId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockEsalesRecommendDelete]'
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesRecommendDelete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[lekmer].[tBlockEsalesRecommend]
	WHERE
		[BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockEsalesRecommendSave]'
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesRecommendSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@RecommendationType INT,
	@PanelPath NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tBlockEsalesRecommend]
	SET
		[ColumnCount]	= @ColumnCount,
		[RowCount]		= @RowCount,
		[EsalesRecommendationTypeId] = @RecommendationType,
		[PanelPath]		= @PanelPath
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockEsalesRecommend]
	(
		[BlockId],
		[ColumnCount],
		[RowCount],
		[EsalesRecommendationTypeId],
		[PanelPath]
	)
	VALUES
	(
		@BlockId,
		@ColumnCount,
		@RowCount,
		@RecommendationType,
		@PanelPath
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vEsalesRecommendationType]'
GO

CREATE VIEW [lekmer].[vEsalesRecommendationType]
AS
	SELECT
		ert.[EsalesRecommendationTypeId] AS 'EsalesRecommendationType.Id',
		ert.[CommonName] AS 'EsalesRecommendationType.CommonName',
		ert.[Title] AS 'EsalesRecommendationType.Title'
	FROM
		[lekmer].[tEsalesRecommendationType] ert

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pEsalesRecommendationTypeGetAll]'
GO

CREATE PROCEDURE [lekmer].[pEsalesRecommendationTypeGetAll]
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vEsalesRecommendationType]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFreeFreightFlagActionCurrencyDeleteAll]'
GO

CREATE procedure [lekmer].[pFreeFreightFlagActionCurrencyDeleteAll]
	@ActionId int
AS 
BEGIN
	DELETE FROM [lekmer].[tFreeFreightFlagActionCurrency]
	WHERE ProductActionId = @ActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFreeFreightFlagActionCurrencySave]'
GO

CREATE procedure [lekmer].[pFreeFreightFlagActionCurrencySave]
	@ActionId int,
	@CurrencyId int,
	@Value decimal(16,2)
AS 
BEGIN 
	INSERT 
		[lekmer].[tFreeFreightFlagActionCurrency]
	( 
		ProductActionId,
		CurrencyId,
		Value
	)
	VALUES 
	(
		@ActionId,
		@CurrencyId,
		@Value
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFreeFreightFlagActionDelete]'
GO

CREATE procedure [lekmer].[pFreeFreightFlagActionDelete]
	@ActionId int
AS 
BEGIN
	DELETE FROM [lekmer].[tFreeFreightFlagAction]
	WHERE ProductActionId = @ActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFreeFreightFlagActionSave]'
GO

CREATE PROCEDURE [lekmer].[pFreeFreightFlagActionSave]
	@ActionId int
AS 
BEGIN
		IF EXISTS (SELECT 1 FROM [lekmer].[tFreeFreightFlagAction] WHERE ProductActionId = @ActionId)
			RETURN
		
		INSERT [lekmer].[tFreeFreightFlagAction]
		( 
			ProductActionId
		)
		VALUES 
		(
			@ActionId
		)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vFreeFreightFlagActionCurrency]'
GO



CREATE VIEW [lekmer].[vFreeFreightFlagActionCurrency]
as
	select
		f.ProductActionId AS 'FreeFreightFlagActionCurrency.ActionId',
		f.CurrencyId AS 'FreeFreightFlagActionCurrency.CurrencyId',
		f.Value AS 'CurrencyValue.MonetaryValue',
		c.*
	from
		lekmer.tFreeFreightFlagActionCurrency f
		inner join core.vCustomCurrency c on f.CurrencyId = c.[Currency.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFreeFreightFlagActionCurrencyGetAllByActionId]'
GO

CREATE PROCEDURE [lekmer].[pFreeFreightFlagActionCurrencyGetAllByActionId]
	@ActionId	int
as
begin
	SELECT
		*
	FROM
		[lekmer].[vFreeFreightFlagActionCurrency]
	WHERE 
		[FreeFreightFlagActionCurrency.ActionId] = @ActionId
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockEsalesRecommend]'
GO

CREATE VIEW [lekmer].[vBlockEsalesRecommend]
AS
	SELECT
		b.*,
		ber.[ColumnCount] AS 'BlockEsalesRecommend.ColumnCount',
		ber.[RowCount] AS 'BlockEsalesRecommend.RowCount',
		ber.[EsalesRecommendationTypeId] AS 'BlockEsalesRecommend.EsalesRecommendationTypeId',
		ber.[PanelPath] AS 'BlockEsalesRecommend.PanelPath'
	FROM
		[lekmer].[tBlockEsalesRecommend] ber
		INNER JOIN [sitestructure].[vCustomBlock] b ON b.[Block.BlockId] = ber.[BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockEsalesRecommendGetById]'
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesRecommendGetById]
	@LanguageId INT,
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vBlockEsalesRecommend] AS ber
	WHERE
		ber.[Block.BlockId] = @BlockId
		AND
		ber.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vBlockEsalesRecommendSecure]'
GO

CREATE VIEW [lekmer].[vBlockEsalesRecommendSecure]
AS
	SELECT
		b.*,
		ber.[ColumnCount] AS 'BlockEsalesRecommend.ColumnCount',
		ber.[RowCount] AS 'BlockEsalesRecommend.RowCount',
		ber.[EsalesRecommendationTypeId] AS 'BlockEsalesRecommend.EsalesRecommendationTypeId',
		ber.[PanelPath] AS 'BlockEsalesRecommend.PanelPath'
	FROM
		[lekmer].[tBlockEsalesRecommend] ber
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON b.[Block.BlockId] = ber.[BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBlockEsalesRecommendGetByIdSecure]'
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesRecommendGetByIdSecure]
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vBlockEsalesRecommendSecure] AS ber
	WHERE
		ber.[Block.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vFreeFreightFlagAction]'
GO


CREATE view [lekmer].[vFreeFreightFlagAction]
as
	select
		a.*
	from
		lekmer.tFreeFreightFlagAction f
		inner join campaign.vCustomProductAction a on f.ProductActionId = a.[ProductAction.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pFreeFreightFlagActionGetById]'
GO

CREATE PROCEDURE [lekmer].[pFreeFreightFlagActionGetById]
	@Id int
AS
BEGIN
	SELECT
		*
	FROM
		lekmer.vFreeFreightFlagAction
	WHERE
		[ProductAction.Id] = @Id
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProduct]'
GO






ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO




ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [integration].[tCdonExport]'
GO
ALTER TABLE [integration].[tCdonExport] ADD
CONSTRAINT [FK_tCdonExport_tCdonExportStatus] FOREIGN KEY ([Status]) REFERENCES [integration].[tCdonExportStatus] ([CdonExportStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBlockEsalesRecommend]'
GO
ALTER TABLE [lekmer].[tBlockEsalesRecommend] ADD
CONSTRAINT [FK_BlockEsalesRecommend_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]),
CONSTRAINT [FK_tBlockEsalesRecommend_tEsalesRecommendationType] FOREIGN KEY ([EsalesRecommendationTypeId]) REFERENCES [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tFreeFreightFlagAction]'
GO
ALTER TABLE [lekmer].[tFreeFreightFlagAction] ADD
CONSTRAINT [FK_tFreeFreightFlagAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tFreeFreightFlagActionCurrency]'
GO
ALTER TABLE [lekmer].[tFreeFreightFlagActionCurrency] ADD
CONSTRAINT [FK_tFreeFreightFlagActionCurrency_tFreeFreightFlagAction] FOREIGN KEY ([ProductActionId]) REFERENCES [lekmer].[tFreeFreightFlagAction] ([ProductActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tFreeFreightFlagActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
