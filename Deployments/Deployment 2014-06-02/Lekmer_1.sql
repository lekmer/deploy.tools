SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[tContentMessage]'
GO
ALTER TABLE [lekmer].[tContentMessage] ADD
[IsDropShip] [bit] NOT NULL CONSTRAINT [DF_tContentMessage_IsDropShip] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tContentMessageIncludeSupplier]'
GO
CREATE TABLE [lekmer].[tContentMessageIncludeSupplier]
(
[ContentMessageId] [int] NOT NULL,
[SupplierId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tContentMessageIncludeSupplier] on [lekmer].[tContentMessageIncludeSupplier]'
GO
ALTER TABLE [lekmer].[tContentMessageIncludeSupplier] ADD CONSTRAINT [PK_tContentMessageIncludeSupplier] PRIMARY KEY CLUSTERED  ([ContentMessageId], [SupplierId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pContentMessageIncludeSupplierDelete]'
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludeSupplierDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tContentMessageIncludeSupplier]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pContentMessageIncludesSave]'
GO
ALTER PROCEDURE [lekmer].[pContentMessageIncludesSave]
	@ContentMessageId	INT,
	@ProductIds			VARCHAR(MAX),
	@CategoryIds		VARCHAR(MAX),
	@BrandIds			VARCHAR(MAX),
	@SupplierIds		VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN
	EXEC [lekmer].[pContentMessageIncludeProductDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeCategoryDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeBrandDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeSupplierDelete] @ContentMessageId

	--EXEC [lekmer].[pContentMessageIncludesClear] @ProductIds, @CategoryIds, @BrandIds, @Delimiter
	
	INSERT [lekmer].[tContentMessageIncludeProduct] ([ContentMessageId], [ProductId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p 
	WHERE [p].[ID] NOT IN (SELECT [ProductId] FROM [lekmer].[tContentMessageIncludeProduct] WHERE [ContentMessageId] = @ContentMessageId)
	
	INSERT [lekmer].[tContentMessageIncludeCategory] ([ContentMessageId], [CategoryId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) p
	WHERE [p].[ID] NOT IN (SELECT [CategoryId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [ContentMessageId] = @ContentMessageId)
	
	INSERT [lekmer].[tContentMessageIncludeBrand] ([ContentMessageId], [BrandId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) p
	WHERE [p].[ID] NOT IN (SELECT [BrandId] FROM [lekmer].[tContentMessageIncludeBrand] WHERE [ContentMessageId] = @ContentMessageId)
	
	INSERT [lekmer].[tContentMessageIncludeSupplier] ([ContentMessageId], [SupplierId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@SupplierIds, @Delimiter) p
	WHERE [p].[ID] NOT IN (SELECT [SupplierId] FROM [lekmer].[tContentMessageIncludeSupplier] WHERE [ContentMessageId] = @ContentMessageId)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pContentMessageIncludesGetAll]'
GO
ALTER PROCEDURE [lekmer].[pContentMessageIncludesGetAll]
	@ContentMessageId	INT
AS
BEGIN
	SELECT [ProductId] FROM [lekmer].[tContentMessageIncludeProduct] WHERE [ContentMessageId] = @ContentMessageId
	SELECT [CategoryId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [ContentMessageId] = @ContentMessageId
	SELECT [BrandId] FROM [lekmer].[tContentMessageIncludeBrand] WHERE [ContentMessageId] = @ContentMessageId
	SELECT [SupplierId] FROM [lekmer].[tContentMessageIncludeSupplier] WHERE [ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pContentMessageIncludesClear]'
GO
ALTER PROCEDURE [lekmer].[pContentMessageIncludesClear]
	@ProductIds		VARCHAR(MAX),
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@SupplierIds	VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DELETE ip FROM [lekmer].[tContentMessageIncludeProduct] ip
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter)) p ON [p].[ID] = [ip].[ProductId]

	DELETE ic FROM [lekmer].[tContentMessageIncludeCategory] ic
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter)) c ON [c].[ID] = [ic].[CategoryId]
	
	DELETE ib FROM [lekmer].[tContentMessageIncludeBrand] ib
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter)) b ON [b].[ID] = [ib].[BrandId]
	
	DELETE isup FROM [lekmer].[tContentMessageIncludeSupplier] isup
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@SupplierIds, @Delimiter)) b ON [b].[ID] = [isup].[SupplierId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pContentMessageDelete]'
GO
ALTER PROCEDURE [lekmer].[pContentMessageDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pContentMessageIncludeBrandDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeCategoryDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeProductDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeSupplierDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageTranslationDelete] @ContentMessageId
		
	DELETE FROM [lekmer].[tContentMessage]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vContentMessageSecure]'
GO



ALTER VIEW [lekmer].[vContentMessageSecure]
AS
	SELECT
		[cm].[ContentMessageId]			'ContentMessage.ContentMessageId',
		[cm].[ContentMessageFolderId]	'ContentMessage.ContentMessageFolderId',
		[cm].[CommonName]				'ContentMessage.CommonName',
		[cm].[Message]					'ContentMessage.Message',
		[cm].[IsDropShip]				'ContentMessage.IsDropShip'
	FROM 
		[lekmer].[tContentMessage] cm


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vContentMessage]'
GO



ALTER VIEW [lekmer].[vContentMessage]
AS
	SELECT
		[cm].[ContentMessageId]			'ContentMessage.ContentMessageId',
		[cm].[ContentMessageFolderId]	'ContentMessage.ContentMessageFolderId',
		[cm].[CommonName]			'ContentMessage.CommonName',
		COALESCE ([cmt].[Message], [cm].[Message]) 'ContentMessage.Message',
		[cm].[IsDropShip]				'ContentMessage.IsDropShip',
		[l].[LanguageId]			'ContentMessage.LanguageId'
	FROM 
		[lekmer].[tContentMessage] cm
		CROSS JOIN [core].[tLanguage] AS l
		LEFT JOIN [lekmer].[tContentMessageTranslation] AS cmt ON [cmt].[ContentMessageId] = [cm].[ContentMessageId] AND [cmt].[LanguageId] = l.[LanguageId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pDropShipGetAllByOrder]'
GO
CREATE PROCEDURE [orderlek].[pDropShipGetAllByOrder]
	@Orderid INT
AS
BEGIN
	SELECT
		[ds].*
	FROM
		[orderlek].[vDropShip] ds
	WHERE
		[ds].[DropShip.OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pContentMessageSave]'
GO
ALTER PROCEDURE [lekmer].[pContentMessageSave]
	@ContentMessageId		INT,
	@ContentMessageFolderId	INT,
	@CommonName				VARCHAR(50),
	@Message				NVARCHAR(MAX),
	@IsDropShip				BIT
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same common names are not allowed (return -2)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tContentMessage]
		WHERE [CommonName] = @CommonName
			AND [ContentMessageId] <> @ContentMessageId
	)
	RETURN -2
	
	UPDATE 
		[lekmer].[tContentMessage]
	SET 
		[ContentMessageFolderId] = @ContentMessageFolderId,
		[CommonName] = @CommonName,
		[Message] = @Message,
		[IsDropShip] = @IsDropShip
	WHERE
		[ContentMessageId] = @ContentMessageId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tContentMessage] (
			[ContentMessageFolderId],
			[CommonName],
			[Message],
			[IsDropShip]
		)
		VALUES (
			@ContentMessageFolderId,
			@CommonName,
			@Message,
			@IsDropShip
		)

		SET @ContentMessageId = SCOPE_IDENTITY()
	END

	RETURN @ContentMessageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pContentMessageGetAllByProduct]'
GO
ALTER PROCEDURE [lekmer].[pContentMessageGetAllByProduct]
	@ProductId		INT,
	@LanguageId		INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	
		@Category3Id INT,
		@Category2Id INT,
		@Category1Id INT

	SELECT @Category3Id = [CategoryId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId
	SELECT @Category2Id = [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @Category3Id
	SELECT @Category1Id = [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @Category2Id
	
	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeProduct] WHERE [ProductId] = @ProductId)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeBrand] WHERE [BrandId] = 
			(SELECT [BrandId] FROM [lekmer].[tLekmerProduct] WHERE [ProductId] = @ProductId)
		)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(
		 SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeSupplier] WHERE [SupplierId] = 
			(SELECT [s].[SupplierId] FROM [lekmer].[tLekmerProduct] lp 
			 INNER JOIN [productlek].[tSupplier] s ON [s].[SupplierNo] = [lp].[SupplierId]
			 WHERE [lp].[ProductId] = @ProductId)
		)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @Category3Id)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @Category2Id)

	UNION

	SELECT * 
	FROM [lekmer].[vContentMessage] 
	WHERE [ContentMessage.LanguageId] = @LanguageId AND 
		[ContentMessage.ContentMessageId] IN 
		(SELECT ContentMessageId FROM [lekmer].[tContentMessageIncludeCategory] WHERE [CategoryId] = @Category1Id)

	ORDER BY [ContentMessage.ContentMessageId] DESC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[pDropShipGetAllGroupByOrder]'
GO
ALTER PROCEDURE [orderlek].[pDropShipGetAllGroupByOrder]
AS 
BEGIN 
	SELECT *
	FROM (SELECT
			ROW_NUMBER() OVER (PARTITION BY [ds].[DropShip.OrderId] ORDER BY [ds].[DropShip.OrderId]) AS Number,
			[ds].*
		  FROM
			[orderlek].[vDropShip] ds) temp
	WHERE [temp].[Number] = 1
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tContentMessageIncludeSupplier]'
GO
ALTER TABLE [lekmer].[tContentMessageIncludeSupplier] ADD CONSTRAINT [FK_tContentMessageIncludeSupplier_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
ALTER TABLE [lekmer].[tContentMessageIncludeSupplier] ADD CONSTRAINT [FK_tContentMessageIncludeSupplier_tSupplier] FOREIGN KEY ([SupplierId]) REFERENCES [productlek].[tSupplier] ([SupplierId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
