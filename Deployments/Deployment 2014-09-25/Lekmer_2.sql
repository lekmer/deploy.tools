/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 9/25/2014 3:41:07 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraint FK_tModelFragmentEntity_tModelFragment from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraints from [security].[tRolePrivilege]
ALTER TABLE [security].[tRolePrivilege] DROP CONSTRAINT [FK_tRolePrivilege_tPrivilege]
ALTER TABLE [security].[tRolePrivilege] DROP CONSTRAINT [FK_tRolePrivilege_tRole]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]

-- Drop constraint FK_tModelSetting_tModel from [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModel]

-- Delete rows from [template].[tModelFragmentFunction]
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000157
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000158
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000159
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1000160
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001255
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001258
DELETE FROM [template].[tModelFragmentFunction] WHERE [FunctionId]=1001265
-- Operation applied to 7 rows out of 7

-- Update rows in [template].[tModelFragmentRegion]
UPDATE [template].[tModelFragmentRegion] SET [Ordinal]=60 WHERE [ModelFragmentRegionId]=1000036
UPDATE [template].[tModelFragmentRegion] SET [Ordinal]=70 WHERE [ModelFragmentRegionId]=1000232
-- Operation applied to 2 rows out of 2

-- Add rows to [security].[tPrivilege]
SET IDENTITY_INSERT [security].[tPrivilege] ON
INSERT INTO [security].[tPrivilege] ([PrivilegeId], [CommonName], [Title]) VALUES (1000035, N'Tools.Qliro', N'Tools.Qliro')
INSERT INTO [security].[tPrivilege] ([PrivilegeId], [CommonName], [Title]) VALUES (1000036, N'Tools.Qliro.ReadOnly', N'Tools.Qliro.ReadOnly')
SET IDENTITY_INSERT [security].[tPrivilege] OFF
-- Operation applied to 2 rows out of 2

-- Add row to [template].[tModel]
SET IDENTITY_INSERT [template].[tModel] ON
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName], [DefaultTemplateId]) VALUES (1000061, 1000005, N'Klarna order acknowledgement', N'KlarnaOrderAcknowledgement', NULL)
SET IDENTITY_INSERT [template].[tModel] OFF

-- Add row to [sitestructure].[tBlockType]
INSERT INTO [sitestructure].[tBlockType] ([BlockTypeId], [Title], [CommonName], [Ordinal], [AvailableForAllPageTypes]) VALUES (1000042, N'Klarna order acknowledgement', N'KlarnaOrderAcknowledgement', 0, 1)

-- Add rows to [order].[tPaymentType]
SET IDENTITY_INSERT [order].[tPaymentType] ON
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000006, N'KlarnaCheckout', N'Klarna Checkout', N'27')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000007, N'Maksuturva', N'Maksuturva', N'30')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000008, N'KlarnaSpecialPartPayment', N'Klarna Special Part Payment', N'29')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000009, N'KlarnaAdvanced', N'Klarna Advanced', N'37')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000010, N'KlarnaAdvancedPartPayment', N'Klarna Advanced Part Payment', N'38')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000011, N'KlarnaAdvancedSpecialPartPayment', N'Klarna Advanced Special Part Payment', N'39')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000012, N'QliroInvoice', N'Qliro Invoice', N'0')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000013, N'QliroPartPayment', N'Qliro Part Payment', N'0')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000014, N'QliroSpecialPartPayment', N'Qliro Special Part Payment', N'0')
SET IDENTITY_INSERT [order].[tPaymentType] OFF
-- Operation applied to 9 rows out of 9

-- Add row to [security].[tRolePrivilege]
INSERT INTO [security].[tRolePrivilege] ([RoleId], [PrivilegeId]) VALUES (1, 1000035)

-- Add rows to [template].[tModelFragmentRegion]
SET IDENTITY_INSERT [template].[tModelFragmentRegion] ON
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000268, 1000014, N'Klarna checkout confirmation', 50)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000269, 1000061, N'Head', 10)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000270, 1000061, N'Content', 20)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000271, 1000061, N'Footer', 30)
SET IDENTITY_INSERT [template].[tModelFragmentRegion] OFF
-- Operation applied to 4 rows out of 4

-- Add rows to [template].[tModelFragment]
SET IDENTITY_INSERT [template].[tModelFragment] ON
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001318, 1000268, N'Klarna checkout confirmation', N'KlarnaCheckoutConfirmation', 1000014, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001319, 1000269, N'Head', N'Head', 1000061, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001320, 1000270, N'Content', N'Content', 1000061, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001321, 1000271, N'Footer', N'Footer', 1000061, 0, 10)
SET IDENTITY_INSERT [template].[tModelFragment] OFF
-- Operation applied to 4 rows out of 4

-- Add rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001594, 1000031, 1, N'Form.PaymentTypeId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001595, 1000031, 1, N'Form.KlarnaPartPaymentId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001596, 1000031, 1, N'Form.KlarnaSpecialPartPaymentId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001597, 1000031, 1, N'Form.MaksuturvaSubPaymentId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001598, 1000030, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001599, 1000030, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001600, 1000030, 2, N'isCommonPayment', N'Dibs, KCO, Moneybookers')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001601, 1000030, 2, N'isCommonPaymentAndSelected', N'Dibs, KCO, Moneybookers')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001602, 1000030, 2, N'isKlarnaPayment', N'Any Klarna payment, except KCO')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001603, 1000030, 2, N'isKlarnaPaymentAndSelected', N'Any Klarna payment, except KCO')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001604, 1000030, 1, N'KlarnaInvoiceCharge', N'Only for Klarna payments')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001605, 1000030, 1, N'EID', N'Only for Klarna payments')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001606, 1000030, 2, N'isKlarnaInvoice', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001607, 1000030, 2, N'isKlarnaInvoiceAndSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001608, 1000030, 2, N'isKlarnaPartPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001609, 1000030, 2, N'isKlarnaPartPaymentAndSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001610, 1000030, 1, N'Iterate:PartPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001611, 1000030, 2, N'isKlarnaSpecialPartPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001612, 1000030, 2, N'isKlarnaSpecialPartPaymentAndSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001613, 1000030, 1, N'SpecialPartPayment.Id', N'Only for Klarna Special payment')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001614, 1000030, 2, N'isMaksuturvaPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001615, 1000030, 2, N'isMaksuturvaPaymentAndSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001616, 1000079, 2, N'IsSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001617, 1000079, 1, N'PartPayment.Title', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001618, 1000079, 1, N'PartPayment.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001619, 1001318, 1, N'KlarnaConfirmationSnippet', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001620, 1000053, 1, N'KlarnaCheckoutConfirmation', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001621, 1000053, 2, N'KlarnaConfirmation.IsUsed', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001622, 1001318, 2, N'KlarnaConfirmation.IsUsed', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001623, 84, 1, N'Payment.KlarnaCheckout.Snippet', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001624, 84, 2, N'Payment.KlarnaCheckout.IsSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001625, 84, 1, N'Checkout.KlarnaCheckoutPaymentId', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001626, 84, 1, N'Checkout.UseAlternateCompanyAddress.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001627, 84, 1, N'Checkout.IsKlarnaCheckoutMode.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001628, 84, 1, N'Checkout.UseAlternateCompanyAddress.Value', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001629, 84, 2, N'Checkout.UseAlternateCompanyAddress.Checked', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001630, 84, 1, N'Checkout.IsKlarnaCheckoutMode.Value', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001631, 1000061, 1, N'Checkout.TotalPriceWithFreight', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001632, 1000061, 2, N'isKlarnaPartPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001633, 1000061, 2, N'KlarnaCheckout.IsSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001634, 1000031, 2, N'KlarnaCheckout.IsAvailable', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001635, 1000031, 2, N'KlarnaCheckout.IsSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001637, 1000030, 2, N'isKlarnaCheckout', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001638, 1000030, 2, N'isKlarnaCheckoutAndSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001639, 1000031, 1, N'Form.QliroPartPaymentId.Name', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001640, 1000030, 2, N'isQliroPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001641, 1000030, 2, N'isQliroPaymentAndSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001642, 1000030, 1, N'Qliro.InvoiceCharge', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001643, 1000030, 1, N'Qliro.ClientRef', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001644, 1000030, 2, N'isQliroInvoice', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001645, 1000030, 2, N'isQliroInvoiceAndSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001646, 1000030, 2, N'isQliroPartPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001647, 1000030, 2, N'isQliroPartPaymentAndSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001648, 1000030, 1, N'Iterate:QliroPartPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001649, 1000030, 2, N'isQliroSpecialPartPayment', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001650, 1000030, 2, N'isQliroSpecialPartPaymentAndSelected', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001651, 1000030, 1, N'Qliro.SpecialPartPayment.Id', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
-- Operation applied to 57 rows out of 57

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraint FK_tModelFragmentEntity_tModelFragment to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] WITH NOCHECK ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [security].[tRolePrivilege]
ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [FK_tRolePrivilege_tPrivilege] FOREIGN KEY ([PrivilegeId]) REFERENCES [security].[tPrivilege] ([PrivilegeId])
ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [FK_tRolePrivilege_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])

-- Add constraint FK_tModelSetting_tModel to [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] WITH NOCHECK ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
COMMIT TRANSACTION
GO
