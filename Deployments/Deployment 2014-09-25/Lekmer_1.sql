SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] DROP CONSTRAINT [FK_tLekmerOrder_tOrder]
ALTER TABLE [lekmer].[tLekmerOrder] DROP CONSTRAINT [FK_tLekmerOrder_tOrderAddress]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [order].[tOrderPayment]'
GO
ALTER TABLE [order].[tOrderPayment] DROP CONSTRAINT [FK_tOrderPayment_tOrder1]
ALTER TABLE [order].[tOrderPayment] DROP CONSTRAINT [FK_tOrderPayment_tPaymentType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] DROP CONSTRAINT [PK_tLekmerOrder]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] DROP CONSTRAINT [DF_tLekmerOrder_NeedSendInsuranceInfo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [order].[tOrderPayment]'
GO
ALTER TABLE [order].[tOrderPayment] DROP CONSTRAINT [PK_tOrderPayment]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tOrderPayment_OrderId] from [order].[tOrderPayment]'
GO
DROP INDEX [IX_tOrderPayment_OrderId] ON [order].[tOrderPayment]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tOrderPayment_PaymentTypeId] from [order].[tOrderPayment]'
GO
DROP INDEX [IX_tOrderPayment_PaymentTypeId] ON [order].[tOrderPayment]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tLekmerOrder]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tLekmerOrder]
(
[OrderId] [int] NOT NULL,
[PaymentCost] [decimal] (16, 2) NOT NULL,
[CustomerIdentificationKey] [nchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[FeedbackToken] [uniqueidentifier] NULL,
[AlternateAddressId] [int] NULL,
[NeedSendInsuranceInfo] [bit] NOT NULL CONSTRAINT [DF_tLekmerOrder_NeedSendInsuranceInfo] DEFAULT ((1)),
[CivicNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[OptionalDeliveryMethodId] [int] NULL,
[OptionalFreightCost] [decimal] (16, 2) NULL,
[DiapersDeliveryMethodId] [int] NULL,
[DiapersFreightCost] [decimal] (16, 2) NULL,
[KcoId] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[UserAgent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tLekmerOrder]([OrderId], [PaymentCost], [CustomerIdentificationKey], [FeedbackToken], [AlternateAddressId], [NeedSendInsuranceInfo], [CivicNumber], [OptionalDeliveryMethodId], [OptionalFreightCost], [DiapersDeliveryMethodId], [DiapersFreightCost], [UserAgent]) SELECT [OrderId], [PaymentCost], [CustomerIdentificationKey], [FeedbackToken], [AlternateAddressId], [NeedSendInsuranceInfo], [CivicNumber], [OptionalDeliveryMethodId], [OptionalFreightCost], [DiapersDeliveryMethodId], [DiapersFreightCost], [UserAgent] FROM [lekmer].[tLekmerOrder]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tLekmerOrder]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tLekmerOrder]', N'tLekmerOrder'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tLekmerOrder] on [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD CONSTRAINT [PK_tLekmerOrder] PRIMARY KEY CLUSTERED  ([OrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[tPaymentType]'
GO
ALTER TABLE [order].[tPaymentType] ADD
[Ordinal] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tQliroPaymentType]'
GO
CREATE TABLE [orderlek].[tQliroPaymentType]
(
[QliroPaymentTypeId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[Code] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[RegistrationFee] [decimal] (16, 2) NOT NULL,
[SettlementFee] [decimal] (16, 2) NOT NULL,
[InterestRate] [decimal] (16, 2) NOT NULL,
[InterestType] [int] NOT NULL,
[InterestCalculation] [int] NOT NULL,
[NoOfMonths] [int] NOT NULL,
[MinPurchaseAmount] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tQliroPaymentType] on [orderlek].[tQliroPaymentType]'
GO
ALTER TABLE [orderlek].[tQliroPaymentType] ADD CONSTRAINT [PK_tQliroPaymentType] PRIMARY KEY CLUSTERED  ([QliroPaymentTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tQliroPaymentType_ChannelId_Code] on [orderlek].[tQliroPaymentType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tQliroPaymentType_ChannelId_Code] ON [orderlek].[tQliroPaymentType] ([ChannelId], [Code])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [order].[tOrderPayment]'
GO
CREATE TABLE [order].[tmp_rg_xx_tOrderPayment]
(
[OrderPaymentId] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[PaymentTypeId] [int] NOT NULL,
[Price] [decimal] (16, 2) NOT NULL,
[Vat] [decimal] (16, 2) NOT NULL,
[ReferenceId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Captured] [bit] NULL,
[KlarnaEID] [int] NULL,
[KlarnaPClass] [int] NULL,
[MaksuturvaCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[QliroClientRef] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[QliroPaymentCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [order].[tmp_rg_xx_tOrderPayment] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [order].[tmp_rg_xx_tOrderPayment]([OrderPaymentId], [OrderId], [PaymentTypeId], [Price], [Vat], [ReferenceId], [Captured], [MaksuturvaCode]) SELECT [OrderPaymentId], [OrderId], [PaymentTypeId], [Price], [Vat], [ReferenceId], [Captured], [MaksuturvaCode] FROM [order].[tOrderPayment]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [order].[tmp_rg_xx_tOrderPayment] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[order].[tOrderPayment]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[order].[tmp_rg_xx_tOrderPayment]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [order].[tOrderPayment]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[order].[tmp_rg_xx_tOrderPayment]', N'tOrderPayment'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tOrderPayment] on [order].[tOrderPayment]'
GO
ALTER TABLE [order].[tOrderPayment] ADD CONSTRAINT [PK_tOrderPayment] PRIMARY KEY CLUSTERED  ([OrderPaymentId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrderPayment_OrderId] on [order].[tOrderPayment]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrderPayment_OrderId] ON [order].[tOrderPayment] ([OrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrderPayment_PaymentTypeId] on [order].[tOrderPayment]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrderPayment_PaymentTypeId] ON [order].[tOrderPayment] ([PaymentTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vOrderPayment]'
GO


ALTER VIEW [order].[vOrderPayment]
AS
	SELECT
		[OrderPaymentId] AS 'OrderPayment.OrderPaymentId',
		[OrderId] AS 'OrderPayment.OrderId',
		[PaymentTypeId] AS 'OrderPayment.PaymentTypeId',
		[Price] AS 'OrderPayment.Price',
		[Vat] AS 'OrderPayment.Vat',
		[ReferenceId] AS 'OrderPayment.ReferenceId',
		[Captured] AS 'OrderPayment.Captured',
		[KlarnaEID] AS 'OrderPayment.KlarnaEID',
		[KlarnaPClass] AS 'OrderPayment.KlarnaPClass',
		[MaksuturvaCode] AS 'OrderPayment.MaksuturvaCode',
		[QliroClientRef] AS 'OrderPayment.QliroClientRef',
		[QliroPaymentCode] AS 'OrderPayment.QliroPaymentCode'
	FROM
		[order].[tOrderPayment]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrder]'
GO

ALTER VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		[lo].[PaymentCost] AS [Lekmer.PaymentCost],
		[lo].[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey],
		[lo].[FeedbackToken] AS [Lekmer.FeedbackToken],
		[lo].[AlternateAddressId] AS [Lekmer.AlternateAddressId],
		[lo].[NeedSendInsuranceInfo] AS [Lekmer.NeedSendInsuranceInfo],
		[lo].[CivicNumber] AS [Lekmer.CivicNumber],
		[lo].[OptionalDeliveryMethodId] AS [Lekmer.OptionalDeliveryMethodId],
		[lo].[OptionalFreightCost] AS [Lekmer.OptionalFreightCost],
		[lo].[DiapersDeliveryMethodId] AS [Lekmer.DiapersDeliveryMethodId],
		[lo].[DiapersFreightCost] AS [Lekmer.DiapersFreightCost],
		[lo].[KcoId] AS [Lekmer.KcoId],
		[lo].[UserAgent] AS [Lekmer.UserAgent]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tQliroTransaction]'
GO
CREATE TABLE [orderlek].[tQliroTransaction]
(
[TransactionId] [int] NOT NULL IDENTITY(1, 1),
[ClientRef] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Mode] [int] NULL,
[TransactionTypeId] [int] NOT NULL,
[StatusCode] [int] NULL,
[Created] [datetime] NOT NULL,
[CivicNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Duration] [bigint] NULL,
[OrderId] [int] NULL,
[Amount] [decimal] (16, 2) NULL,
[CurrencyCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PaymentType] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ReservationNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[InvoiceStatus] [int] NULL,
[ReturnCodeId] [int] NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[CustomerMessage] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ResponseContent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tQliroTransaction] on [orderlek].[tQliroTransaction]'
GO
ALTER TABLE [orderlek].[tQliroTransaction] ADD CONSTRAINT [PK_tQliroTransaction] PRIMARY KEY CLUSTERED  ([TransactionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroTransactionCreateCheckOrderStatus]'
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionCreateCheckOrderStatus]
	@ClientRef VARCHAR(50),
	@TransactionTypeId INT,
	@OrderId INT,
	@ReservationNumber VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tQliroTransaction]
			( [ClientRef],
			  [TransactionTypeId],
			  [OrderId],
			  [ReservationNumber],
			  [Created]
			)
	VALUES
			( @ClientRef,
			  @TransactionTypeId,			  
			  @OrderId,
			  @ReservationNumber,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tQliroPendingOrder]'
GO
CREATE TABLE [orderlek].[tQliroPendingOrder]
(
[QliroPendingOrderId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[OrderId] [int] NOT NULL,
[FirstAttempt] [datetime] NOT NULL,
[LastAttempt] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tQliroPendingOrder] on [orderlek].[tQliroPendingOrder]'
GO
ALTER TABLE [orderlek].[tQliroPendingOrder] ADD CONSTRAINT [PK_tQliroPendingOrder] PRIMARY KEY CLUSTERED  ([QliroPendingOrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vQliroPendingOrder]'
GO


CREATE VIEW [orderlek].[vQliroPendingOrder]
AS
SELECT
	[po].[QliroPendingOrderId] AS 'QliroPendingOrder.QliroPendingOrderId',
	[po].[ChannelId] AS 'QliroPendingOrder.ChannelId',
	[po].[OrderId] AS 'QliroPendingOrder.OrderId',
	[po].[FirstAttempt] AS 'QliroPendingOrder.FirstAttempt',
	[po].[LastAttempt] AS 'QliroPendingOrder.LastAttempt'
FROM
	[orderlek].[tQliroPendingOrder] po


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vPaymentType]'
GO

ALTER VIEW [order].[vPaymentType]
AS
SELECT
	pt.PaymentTypeId AS 'PaymentType.PaymentTypeId',
	pt.CommonName AS 'PaymentType.CommonName',
	pt.ErpId AS 'PaymentType.ErpId',
	pt.Title AS 'PaymentType.Title',
	pt.Ordinal AS 'PaymentType.Ordinal',
	cpt.ChannelId AS 'PaymentType.ChannelId'
FROM
	[order].[tPaymentType] pt
	INNER JOIN [order].[tChannelPaymentType] cpt
		ON cpt.PaymentTypeId = pt.PaymentTypeId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomPaymentType]'
GO

ALTER VIEW [order].[vCustomPaymentType]
AS
	SELECT
		pt.*,
		ptp.PaymentCost AS 'PaymentType.PaymentCost'
	FROM
		[order].[vPaymentType] pt
		INNER JOIN [core].[tChannel] ch	
			ON ch.ChannelId = pt.[PaymentType.ChannelId]
		INNER JOIN [lekmer].[tPaymentTypePrice] ptp
			ON ptp.[PaymentTypeId] = pt.[PaymentType.PaymentTypeId]
			AND ch.CountryId = ptp.[CountryId]
			AND ch.CurrencyId = ptp.[CurrencyId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroTransactionSaveCheckOrderStatusResponse]'
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionSaveCheckOrderStatusResponse]
	@TransactionId INT,
	@StatusCode INT,
	@ReturnCodeId INT,
	@InvoiceStatus INT,
	@Message NVARCHAR(MAX),
	@Duration BIGINT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tQliroTransaction]
	SET
		[StatusCode] = @StatusCode,
		[ReturnCodeId] = @ReturnCodeId,
		[InvoiceStatus] = @InvoiceStatus,
		[Message] = @Message,
		[Duration] = @Duration
	WHERE
		[TransactionId] = @TransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroTransactionCreateReservation]'
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionCreateReservation]
	@ClientRef VARCHAR(50),
	@Mode INT,
	@TransactionTypeId INT,
	@CivicNumber VARCHAR(50),
	@OrderId INT,
	@Amount DECIMAL(16,2),
	@CurrencyCode VARCHAR(50),
	@PaymentType VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tQliroTransaction]
			( [ClientRef],
			  [Mode],
			  [TransactionTypeId],
			  [CivicNumber],
			  [OrderId],
			  [Amount],
			  [CurrencyCode],
			  [PaymentType],
			  [Created]
			)
	VALUES
			( @ClientRef,
			  @Mode,
			  @TransactionTypeId,			  
			  @CivicNumber,
			  @OrderId,
			  @Amount,
			  @CurrencyCode,
			  @PaymentType,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pOrderSave]'
GO
ALTER PROCEDURE [lekmer].[pOrderSave]
	@OrderId					INT,
	@PaymentCost				DECIMAL(16,2),
	@CustomerIdentificationKey	NCHAR(50),
	@AlternateAddressId			INT = NULL,
	@CivicNumber				NVARCHAR(50) = NULL,
	@OptionalDeliveryMethodId	INT,
	@OptionalFreightCost		DECIMAL(16, 2),
	@DiapersDeliveryMethodId	INT,
	@DiapersFreightCost			DECIMAL(16, 2),
	@KcoId						NVARCHAR(MAX) = NULL,
	@UserAgent					NVARCHAR(MAX) = NULL
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		[PaymentCost] = @PaymentCost,
		[CustomerIdentificationKey] = @CustomerIdentificationKey,
		[AlternateAddressId] = @AlternateAddressId,
		[CivicNumber] = @CivicNumber,
		[OptionalDeliveryMethodId] = @OptionalDeliveryMethodId,
		[OptionalFreightCost] = @OptionalFreightCost,
		[DiapersDeliveryMethodId] = @DiapersDeliveryMethodId,
		[DiapersFreightCost] = @DiapersFreightCost,
		[UserAgent] = @UserAgent
	WHERE 
		[OrderId] = @OrderId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tLekmerOrder]
		(
			[OrderId],
			[PaymentCost],
			[CustomerIdentificationKey],
			[AlternateAddressId],
			[CivicNumber],
			[OptionalDeliveryMethodId],
			[OptionalFreightCost],
			[DiapersDeliveryMethodId],
			[DiapersFreightCost],
			[KcoId],
			[UserAgent]
		)
		VALUES
		(
			@OrderId,
			@PaymentCost,
			@CustomerIdentificationKey,
			@AlternateAddressId,
			@CivicNumber,
			@OptionalDeliveryMethodId,
			@OptionalFreightCost,
			@DiapersDeliveryMethodId,
			@DiapersFreightCost,
			@KcoId,
			@UserAgent
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroTransactionCreateGetAddress]'
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionCreateGetAddress]
	@ClientRef VARCHAR(50),
	@TransactionTypeId INT,
	@CivicNumber VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tQliroTransaction]
			( [ClientRef],
			  [TransactionTypeId],
			  [CivicNumber],
			  [Created]
			)
	VALUES
			( @ClientRef,
			  @TransactionTypeId,
			  @CivicNumber,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderPaymentSave]'
GO
ALTER PROCEDURE [order].[pOrderPaymentSave]
	@OrderPaymentId INT,
	@OrderId INT,
	@PaymentTypeId INT,
	@Price DECIMAL(16,2),
	@Vat DECIMAL(16,2),
	@ReferenceId VARCHAR(50),
	@Captured BIT = NULL,
	@KlarnaEID INT = NULL,
	@KlarnaPClass INT = NULL,
	@MaksuturvaCode VARCHAR(50) = NULL,
	@QliroClientRef VARCHAR(50) = NULL,
	@QliroPaymentCode VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderPayment]
	SET
		[OrderId] = @OrderId,
		[PaymentTypeId] = @PaymentTypeId,
		[Price] = @Price,
		[Vat] = @VAT,
		[ReferenceId] = @ReferenceId,
		[Captured] = @Captured,
		[KlarnaEID] = @KlarnaEID,
		[KlarnaPClass] = @KlarnaPClass,
		[MaksuturvaCode] = @MaksuturvaCode,
		[QliroClientRef] = @QliroClientRef,
		[QliroPaymentCode] = @QliroPaymentCode
	WHERE
		[OrderPaymentId] = @OrderPaymentId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderPayment]
		(
			[OrderId],
			[PaymentTypeId],
			[Price],
			[Vat],
			[ReferenceId],
			[Captured],
			[KlarnaEID],
			[KlarnaPClass],
			[MaksuturvaCode],
			[QliroClientRef],
			[QliroPaymentCode]
		)
		VALUES
		(
			@OrderId,
			@PaymentTypeId,
			@Price,
			@Vat,
			@ReferenceId,
			@Captured,
			@KlarnaEID,
			@KlarnaPClass,
			@MaksuturvaCode,
			@QliroClientRef,
			@QliroPaymentCode
		)

		SET @OrderPaymentId = CAST(SCOPE_IDENTITY() AS INT)
	END

	RETURN @OrderPaymentId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pPaymentTypeGetAll]'
GO
ALTER PROCEDURE [order].[pPaymentTypeGetAll]
	@ChannelId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomPaymentType]
	WHERE
		[PaymentType.ChannelId] = @ChannelId
	ORDER BY
		[PaymentType.Ordinal],
		[PaymentType.PaymentTypeId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroPendingOrderInsert]'
GO


CREATE PROCEDURE [orderlek].[pQliroPendingOrderInsert]
	@ChannelId INT,
	@OrderId INT,
	@FirstAttempt DATETIME,
	@LastAttempt DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tQliroPendingOrder]
			( [ChannelId],
			  [OrderId],
			  [FirstAttempt],
			  [LastAttempt]
			)
	VALUES
			( @ChannelId,
			  @OrderId,
			  @FirstAttempt,
			  @LastAttempt
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pOrder]'
GO

ALTER PROCEDURE [integration].[pOrder]
	@OrderId int
AS
begin
	set nocount on
	set transaction isolation level read uncommitted
	begin try
		
		--set statistics io on
		--set statistics time on
		
		-- Order
		select 		
			o.OrderId,
            o.FreightCost,
            lo.OptionalFreightCost,
            lo.DiapersFreightCost,
            o.ChannelId as Channel,   
            op.OrderPaymentId, 
            pt.ErpId as PaymentMethod, 
            op.ReferenceId,
            op.Price,            
            --u.VoucherDiscount as VoucherAmount
            a1.ItemsActualPriceIncludingVat - op.Price as VoucherAmount,
			lci.IsCompany, -- NEW
			op.Captured as PaymentCaptured,
			op.[KlarnaEID],
            op.[KlarnaPClass],
            op.[QliroClientRef],
            op.[QliroPaymentCode],
            o.[IP] AS IpAddress,
            o.[CreatedDate]
		from
			[order].[tPaymentType] pt
			inner join [order].[tOrderPayment] op on op.PaymentTypeId = pt.PaymentTypeId
			inner join [order].[tOrder] o on op.OrderId = o.OrderId
			inner join [customerlek].[tCustomerInformation] lci on lci.CustomerId = o.CustomerId
			left join [lekmer].[tLekmerOrder] lo on lo.OrderId = o.OrderId
			cross apply (
				select sum(oi.ActualPriceIncludingVat * oi.Quantity) as 'ItemsActualPriceIncludingVat'
				from [order].[tOrderItem] oi
				where oi.OrderId = o.OrderId
			) as a1
			where
				o.OrderStatusId = 2
				and
				o.OrderId = @OrderId
		
		-- OrderRow // Old
		--select
		--	o.OrderItemId as OrderRowId, 
		--	p.Title, 
		--	o.Quantity, 
		--	p.ProductId,
		--	o.ActualPriceIncludingVat as Price, 
		--	o.OriginalPriceIncludingVat as OrdinaryPrice, 
		--	coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		--from
		--	[order].tOrderItem o
		--	left join [order].tOrderItemProduct p 
		--		on o.OrderItemId = p.OrderItemId
		--	left join lekmer.tOrderItemSize ois
		--		on ois.OrderItemId = o.OrderItemId
		--where
		--		o.OrderId = @OrderId 
				
		-- OrderRow
		select
			o.OrderItemId as OrderRowId, 
			p.Title, 
			o.Quantity, 
			p.ProductId,
			o.ActualPriceIncludingVat as Price, 
			o.OriginalPriceIncludingVat as OrdinaryPrice, 
			coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
			inner join lekmer.tLekmerProduct l
				on p.ProductId = l.ProductId
		where
			o.OrderId = @OrderId 
			and l.ProductTypeId != 2 -- package article
		UNION ALL
		select
			poi.OrderItemId as OrderRowId,
			poip.Title,
			poi.Quantity,
			poip.ProductId,
			poi.PackagePriceIncludingVat as Price,
			poi.OriginalPriceIncludingVat as OrdinaryPrice,
			coalesce(poip.[SizeErpId], poip.ErpId) as HyErpId
		from
			orderlek.tPackageOrderItem poi
			left join orderlek.tPackageOrderItemProduct poip
				on poi.PackageOrderItemId = poip.PackageOrderItemId
		where
			poi.OrderId = @OrderId
			
		-- Customer
		select 		
            c.CustomerId, 
			c.FirstName, 
			c.LastName,
            c.CivicNumber, 
            c.PhoneNumber,
            c.CellPhoneNumber as MobilePhoneNumber, 
            o.Email,
			case g.IsCompany when 1 then '05' else isnull(gt.ErpId, '00') end as Gender --NEW
		from	
			[order].torder o
			inner join [customer].tCustomerInformation c
				on c.CustomerId = o.CustomerId
			left join customerlek.tCustomerInformation g
				on c.CustomerId = g.CustomerId
			left join customerlek.tGenderType gt
				on g.GenderTypeId = gt.GenderTypeId
		where
			o.OrderId = @OrderId
		
		-- BillingAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.BillingAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- DeliveryAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.DeliveryAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- AlternativeAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].tOrder o
			inner join [lekmer].tLekmerOrder lo on lo.OrderId = o.OrderId
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = lo.AlternateAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId


		-- DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join [order].tDeliveryMethod d
				on o.DeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId
			
		-- Optional DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join lekmer.tLekmerOrder lo
				on lo.OrderId = o.OrderId
			inner join [order].tDeliveryMethod d
				on lo.OptionalDeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId
			
		-- Diapers DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join lekmer.tLekmerOrder lo
				on lo.OrderId = o.OrderId
			inner join [order].tDeliveryMethod d
				on lo.DiapersDeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId

	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vQliroPaymentType]'
GO

CREATE VIEW [orderlek].[vQliroPaymentType]
AS
SELECT
	[qpt].[QliroPaymentTypeId] AS 'QliroPaymentType.Id',
	[qpt].[ChannelId] AS 'QliroPaymentType.ChannelId',
	[qpt].[Code] AS 'QliroPaymentType.Code',
	[qpt].[Description] AS 'QliroPaymentType.Description',
	[qpt].[RegistrationFee] AS 'QliroPaymentType.RegistrationFee',
	[qpt].[SettlementFee] AS 'QliroPaymentType.SettlementFee',
	[qpt].[InterestRate] AS 'QliroPaymentType.InterestRate',
	[qpt].[InterestType] AS 'QliroPaymentType.InterestType',
	[qpt].[InterestCalculation] AS 'QliroPaymentType.InterestCalculation',
	[qpt].[NoOfMonths] AS 'QliroPaymentType.NoOfMonths',
	[qpt].[MinPurchaseAmount] AS 'QliroPaymentType.MinPurchaseAmount'
FROM
	[orderlek].[tQliroPaymentType] qpt
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroPaymentTypeGetAllByChannel]'
GO
CREATE PROCEDURE [orderlek].[pQliroPaymentTypeGetAllByChannel]
	@ChannelId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vQliroPaymentType]
	WHERE
		[QliroPaymentType.ChannelId] = @ChannelId
	ORDER BY
		[QliroPaymentType.Id]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroTransactionSaveReservationResponse]'
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionSaveReservationResponse]
	@TransactionId INT,
	@StatusCode INT,
	@ReservationNumber VARCHAR(50),
	@ReturnCodeId INT,
	@InvoiceStatus INT,
	@Message NVARCHAR(MAX),
	@CustomerMessage NVARCHAR(MAX),
	@Duration BIGINT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tQliroTransaction]
	SET
		[StatusCode] = @StatusCode,
		[ReservationNumber] = @ReservationNumber,
		[ReturnCodeId] = @ReturnCodeId,
		[InvoiceStatus] = @InvoiceStatus,
		[Message] = @Message,
		[CustomerMessage] = @CustomerMessage,
		[Duration] = @Duration
	WHERE
		[TransactionId] = @TransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroPaymentTypeDelete]'
GO
CREATE PROCEDURE [orderlek].[pQliroPaymentTypeDelete]
	@QliroPaymentTypeId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [orderlek].[tQliroPaymentType]
	WHERE [QliroPaymentTypeId] = @QliroPaymentTypeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroPendingOrderUpdate]'
GO

CREATE PROCEDURE [orderlek].[pQliroPendingOrderUpdate]
	@QliroPendingOrderId INT,
	@ChannelId INT,
	@OrderId INT,
	@FirstAttempt DATETIME,
	@LastAttempt DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tQliroPendingOrder]
	SET	
		[ChannelId] = @ChannelId,
		[OrderId] = @OrderId,
		[FirstAttempt] = @FirstAttempt,
		[LastAttempt] = @LastAttempt
	WHERE
		[QliroPendingOrderId] = @QliroPendingOrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroPaymentTypeSave]'
GO
CREATE PROCEDURE [orderlek].[pQliroPaymentTypeSave]
	@QliroPaymentTypeId INT,
	@ChannelId INT,
	@Code VARCHAR(50),
	@Description NVARCHAR(100),
	@RegistrationFee DECIMAL(16,2),
	@SettlementFee DECIMAL(16,2),
	@InterestRate DECIMAL(16,2),
	@InterestType INT,
	@InterestCalculation INT,
	@NoOfMonths INT,
	@MinPurchaseAmount DECIMAL(16,2)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Id INT
	
	SET @Id = @QliroPaymentTypeId
	
	UPDATE
		[orderlek].[tQliroPaymentType]
	SET
		[Description] = @Description,
		[RegistrationFee] = @RegistrationFee,
		[SettlementFee] = @SettlementFee,
		[InterestRate] = @InterestRate,
		[InterestType] = @InterestType,
		[InterestCalculation] = @InterestCalculation,
		[NoOfMonths] = @NoOfMonths,
		[MinPurchaseAmount]= @MinPurchaseAmount
	WHERE
		[ChannelId] = @ChannelId
		AND
		[Code] = @Code
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [orderlek].[tQliroPaymentType]
				( [ChannelId],
				  [Code],
				  [Description],
				  [RegistrationFee],
				  [SettlementFee],
				  [InterestRate],
				  [InterestType],
				  [InterestCalculation],
				  [NoOfMonths],
				  [MinPurchaseAmount]
				)
		VALUES
				( @ChannelId,
				  @Code,
				  @Description,
				  @RegistrationFee,
				  @SettlementFee,
				  @InterestRate,
				  @InterestType,
				  @InterestCalculation,
				  @NoOfMonths,
				  @MinPurchaseAmount
				)

		SET @Id = SCOPE_IDENTITY()
	END

	RETURN @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroTransactionSaveResult]'
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionSaveResult]
	@TransactionId INT,
	@StatusCode INT,
	@ReturnCodeId INT,
	@Message NVARCHAR(50),
	@CustomerMessage NVARCHAR(50),
	@Duration BIGINT,
	@ResponseContent NVARCHAR(MAX)
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tQliroTransaction]
	SET
		[StatusCode] = @StatusCode,
		[ReturnCodeId] = @ReturnCodeId,
		[Message] = @Message,
		[CustomerMessage] = @CustomerMessage,
		[Duration] = @Duration,
		[ResponseContent] = @ResponseContent
	WHERE
		[TransactionId] = @TransactionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pQliroPendingOrderGetPendingOrdersForStatusCheck]'
GO

CREATE PROCEDURE [orderlek].[pQliroPendingOrderGetPendingOrdersForStatusCheck]
	@CheckToDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[po].*
	FROM
		[orderlek].[vQliroPendingOrder] po
	WHERE
		[po].[QliroPendingOrder.LastAttempt] < @CheckToDate
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tQliroPaymentType]'
GO
ALTER TABLE [orderlek].[tQliroPaymentType] ADD CONSTRAINT [FK_tQliroPaymentType_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD CONSTRAINT [FK_tLekmerOrder_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
ALTER TABLE [lekmer].[tLekmerOrder] ADD CONSTRAINT [FK_tLekmerOrder_tOrderAddress] FOREIGN KEY ([AlternateAddressId]) REFERENCES [order].[tOrderAddress] ([OrderAddressId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [order].[tOrderPayment]'
GO
ALTER TABLE [order].[tOrderPayment] ADD CONSTRAINT [FK_tOrderPayment_tOrder1] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
ALTER TABLE [order].[tOrderPayment] ADD CONSTRAINT [FK_tOrderPayment_tPaymentType] FOREIGN KEY ([PaymentTypeId]) REFERENCES [order].[tPaymentType] ([PaymentTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
