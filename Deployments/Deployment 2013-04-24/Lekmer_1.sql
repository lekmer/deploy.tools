SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tIconTranslation]'
GO
ALTER TABLE [lekmer].[tIconTranslation] DROP CONSTRAINT [FK_tIconTranslation_tIcon]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProductIcon]'
GO
ALTER TABLE [lekmer].[tLekmerProductIcon] DROP CONSTRAINT [FK_tLekmerProductIcon_tIcon]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tIcon]'
GO
ALTER TABLE [lekmer].[tIcon] DROP CONSTRAINT [FK_tIcon_tImage]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tIcon]'
GO
ALTER TABLE [lekmer].[tIcon] DROP CONSTRAINT [PK_tIcon]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pIconGetAllByProduct]'
GO
DROP PROCEDURE [lekmer].[pIconGetAllByProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pLekmerProductIconSave]'
GO
DROP PROCEDURE [lekmer].[pLekmerProductIconSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pIconGetAllByProductSecure]'
GO
DROP PROCEDURE [lekmer].[pIconGetAllByProductSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pLekmerProductIconDeleteAll]'
GO
DROP PROCEDURE [lekmer].[pLekmerProductIconDeleteAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[tCampaign]'
GO
ALTER TABLE [campaign].[tCampaign] ADD
[WebTitle] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[IconMediaId] [int] NULL,
[ImageMediaId] [int] NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[LinkContentNodeId] [int] NULL,
[UseLandingPage] [bit] NOT NULL CONSTRAINT [DF_tCampaign_UseLandingPage] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tIcon]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tIcon]
(
[IconId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[MediaId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tIcon] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tIcon]([IconId], [Title], [MediaId]) SELECT [IconId], [Title], [MediaId] FROM [lekmer].[tIcon]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [lekmer].[tmp_rg_xx_tIcon] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[lekmer].[tIcon]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[lekmer].[tmp_rg_xx_tIcon]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tIcon]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tIcon]', N'tIcon'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tIcon] on [lekmer].[tIcon]'
GO
ALTER TABLE [lekmer].[tIcon] ADD CONSTRAINT [PK_tIcon] PRIMARY KEY CLUSTERED  ([IconId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCategoryIcon]'
GO
CREATE TABLE [lekmer].[tCategoryIcon]
(
[CategoryId] [int] NOT NULL,
[IconId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCategoryIcon] on [lekmer].[tCategoryIcon]'
GO
ALTER TABLE [lekmer].[tCategoryIcon] ADD CONSTRAINT [PK_tCategoryIcon] PRIMARY KEY CLUSTERED  ([CategoryId], [IconId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [productlek].[tBlockProductSearchEsalesResult]'
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] DROP
COLUMN [RowCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [productlek].[tBlockProductSearchEsalesResult]'
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] DROP
COLUMN ColumnCount
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [productlek].[tBlockProductSearchEsalesResult]'
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] ADD
[SecondaryTemplateId] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*EXEC sp_rename N'[productlek].[tBlockProductSearchEsalesResult].[ColumnCount]', N'SecondaryTemplateId', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO*/


PRINT N'Altering [lekmer].[tIconTranslation]'
GO
ALTER TABLE [lekmer].[tIconTranslation] ADD
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [lekmer].[tIconTranslation] ALTER COLUMN [Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tBrandIcon]'
GO
CREATE TABLE [lekmer].[tBrandIcon]
(
[BrandId] [int] NOT NULL,
[IconId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBrandIcon] on [lekmer].[tBrandIcon]'
GO
ALTER TABLE [lekmer].[tBrandIcon] ADD CONSTRAINT [PK_tBrandIcon] PRIMARY KEY CLUSTERED  ([BrandId], [IconId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[pProductGetIdAllAvaliableForCdonExport]'
GO
ALTER PROCEDURE [export].[pProductGetIdAllAvaliableForCdonExport]
	@MonthPurchasedAgo INT
AS
BEGIN
	SELECT [p1].[ProductId]
	FROM [product].[tProduct] p1
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p1].[ProductId]
	WHERE 
		[p1].[IsDeleted] = 0
		AND [p1].[ProductStatusId] = 0
		AND [lp].[ProductTypeId] = 1
	UNION
	SELECT DISTINCT ([p2].[ProductId])
	FROM [product].[tProduct] p2
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p2].[ProductId]
		 INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[ProductId] = [p2].[ProductId]
		 INNER JOIN [order].[tOrderItem] oi ON [oi].[OrderItemId] = [oip].[OrderItemId]
		 INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [oi].[OrderId]
	WHERE 
		[p2].[IsDeleted] = 0
		AND [p2].[ProductStatusId] = 1
		AND [o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > DATEADD(MONTH, -@MonthPurchasedAgo, GETDATE())
		AND [lp].[ProductTypeId] = 1
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vIconSecure]'
GO
ALTER VIEW [lekmer].[vIconSecure]
AS
SELECT
	ic.IconId AS 'Icon.Id',
	ic.Title AS 'Icon.Title',
	[ic].[Description] AS 'Icon.Description',
	im.*
FROM
	lekmer.tIcon ic INNER JOIN
	media.vCustomImageSecure im ON ic.MediaId = im.[Image.MediaId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCategoryIconGetAllSecure]'
GO
CREATE PROCEDURE [lekmer].[pCategoryIconGetAllSecure]
	@CategoryId	INT
AS 
BEGIN 
	SELECT 
		i.*
	FROM 
		[lekmer].[vIconSecure] i
		INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[IconId] = [i].[Icon.Id]
	WHERE
		[ci].[CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCategoryIconSave]'
GO
CREATE PROCEDURE [lekmer].[pCategoryIconSave]
	@CategoryId	INT,
	@IconId		INT
AS 
BEGIN 
	INSERT [lekmer].[tCategoryIcon]
	VALUES (@CategoryId, @IconId)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[vCampaign]'
GO

ALTER VIEW [campaign].[vCampaign]
AS
	SELECT
		[c].[CampaignId]			AS 'Campaign.Id',
		[c].[CampaignRegistryId]	AS 'Campaign.CampaignRegistryId',
		[c].[FolderId]				AS 'Campaign.FolderId',
		[c].[Title]					AS 'Campaign.Title',
		[c].[CampaignStatusId]		AS 'Campaign.CampaignStatusId',
		[c].[StartDate]				AS 'Campaign.StartDate',
		[c].[EndDate]				AS 'Campaign.EndDate',
		[c].[Priority]				AS 'Campaign.Priority',
		[c].[Exclusive]				AS 'Campaign.Exclusive',
		[c].[LevelId]				AS 'Campaign.LevelId',
		[cl].[Priority]				AS 'Campaign.LevelPriority',
		[c].[WebTitle]				AS 'Campaign.WebTitle',
		[c].[IconMediaId]			AS 'Campaign.IconMediaId',
		[c].[ImageMediaId] 			AS 'Campaign.ImageMediaId', 
		[c].[Description]			AS 'Campaign.Description',
		[c].[LinkContentNodeId]		AS 'Campaign.LinkContentNodeId',
		[c].[UseLandingPage]		AS 'Campaign.UseLandingPage',
		s.*
	FROM
		[campaign].[tCampaign] c
		INNER JOIN [campaign].[vCustomCampaignStatus] s ON [c].[CampaignStatusId] = [s].[CampaignStatus.Id]
		INNER JOIN [campaignlek].[tCampaignLevel] cl ON [cl].[LevelId] = [c].[LevelId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductIconDeleteAll]'
GO
CREATE PROCEDURE [lekmer].[pProductIconDeleteAll]
	@ProductId	int
AS 
BEGIN 
	DELETE 
		[lekmer].[tLekmerProductIcon]
	WHERE
		ProductId = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[vBlockProductSearchEsalesResult]'
GO

ALTER VIEW [productlek].[vBlockProductSearchEsalesResult]
AS
	SELECT
		[BlockId] 'BlockProductSearchEsalesResult.BlockId' ,
		[SecondaryTemplateId] 'BlockProductSearchEsalesResult.SecondaryTemplateId'
	FROM
		[productlek].[tBlockProductSearchEsalesResult]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[fnGetParentCategories]'
GO
CREATE FUNCTION [lekmer].[fnGetParentCategories](
	@CategoryId int
)
RETURNS @tCategoryIds TABLE (CategoryId INT)
AS 
BEGIN
	WITH CategoryIds (CategoryId, ParentCategoryId) AS 
	(
		SELECT [c1].[CategoryId], [c1].[ParentCategoryId]
		FROM [product].[tCategory] c1
		WHERE [c1].[CategoryId] = @CategoryId
		UNION ALL
		SELECT [c2].[CategoryId], [c2].[ParentCategoryId]
		FROM [product].[tCategory] c2 
		JOIN [CategoryIds] ci ON [ci].[ParentCategoryId] = [c2].[CategoryId]
	)
	
	INSERT INTO @tCategoryIds (CategoryId)
	SELECT CategoryId FROM CategoryIds
	UNION ALL
	SELECT @CategoryId
	
	RETURN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductIconGetAllSecure]'
GO
CREATE PROCEDURE [lekmer].[pProductIconGetAllSecure]
	@ProductId	INT,
	@CategoryId	INT,
	@BrandId	INT = NULL
AS 
BEGIN 
	SELECT i.*
	FROM 
		[lekmer].[vIconSecure] i INNER JOIN 
		[lekmer].[tLekmerProductIcon] lpi ON lpi.IconId = i.[Icon.Id]
	WHERE lpi.ProductId = @ProductId

	UNION

	SELECT i.*
	FROM 
		[lekmer].[vIconSecure] i 
		INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[IconId] = [i].[Icon.Id]
	WHERE [bi].[BrandId] = @BrandId
	
	UNION
	
	SELECT i.*
	FROM 
		[lekmer].[vIconSecure] i 
		INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[IconId] = [i].[Icon.Id]
	WHERE [ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](@CategoryId))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pIconTranslationDelete]'
GO
CREATE PROCEDURE [lekmer].[pIconTranslationDelete]
	@IconId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [lekmer].[tIconTranslation]
	WHERE [IconId] = @IconId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageProductSave]'
GO
ALTER PROCEDURE [productlek].[pPackageProductSave]
	@PackageId	INT,
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	EXEC [productlek].[pPackageProductDelete] @PackageId

	INSERT [productlek].[tPackageProduct] (
		[PackageId],
		[ProductId]
	)
	SELECT
		@PackageId,
		[p].[ID]
	FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p
	
	
	DECLARE @AgeFromMonth INT, @AgeToMonth INT
	SET @AgeFromMonth = 0 
	SET @AgeToMonth = 0 
	
	SELECT 
		@AgeFromMonth = MIN([lp].[AgeFromMonth]),
		@AgeToMonth = MAX([lp].[AgeToMonth])
	FROM [lekmer].[tLekmerProduct] lp
	INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p ON p.[ID] = [lp].[ProductId]
	
	UPDATE [lekmer].[tLekmerProduct]
	SET
		[AgeFromMonth] = (CASE WHEN @AgeFromMonth IS NULL THEN 0 ELSE @AgeFromMonth END),
		[AgeToMonth] = CASE WHEN @AgeToMonth IS NULL THEN 0 ELSE @AgeToMonth END
	WHERE [ProductId] = (SELECT [MasterProductId] FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vIconTranslation]'
GO
CREATE VIEW [lekmer].[vIconTranslation]
AS
SELECT 
	[IconId] AS 'IconTranslation.IconId',
	[LanguageId] AS 'IconTranslation.LanguageId',
	[Title] AS 'IconTranslation.Title',
	[Description] AS 'IconTranslation.Description'
FROM 
	[lekmer].[tIconTranslation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pIconTranslationDescriptionGetAll]'
GO
CREATE PROCEDURE [lekmer].[pIconTranslationDescriptionGetAll]
	@IconId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    it.[IconTranslation.IconId] AS 'Id',
		it.[IconTranslation.LanguageId] AS 'LanguageId',
		it.[IconTranslation.Description] AS 'Value'
	FROM
	    [lekmer].[vIconTranslation] it
	WHERE 
		it.[IconTranslation.IconId] = @IconId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pIconTranslationSave]'
GO
CREATE PROCEDURE [lekmer].[pIconTranslationSave]
	@IconId			INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tIconTranslation]
	SET
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[IconId] = @IconId
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [lekmer].[tIconTranslation] (
			[IconId],
			[LanguageId],
			[Title],
			[Description]				
		)
		VALUES (
			@IconId,
			@LanguageId,
			@Title,
			@Description
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pProductExportToCdonNEW]'
GO
ALTER PROCEDURE [integration].[pProductExportToCdonNEW]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set transaction isolation level read uncommitted
	
	declare @Tree table (
		CategoryId int primary key with (ignore_dup_key = on),
		Depth int,
		HasChildren bit
	)
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		
   select ch.ChannelId
         ,ch.Title as Name
         ,replace(ch.Title, 'Lekmer.', '') as Tld
         ,cu.ISO as CurrencyCode
         ,cu.CurrencyId
     from @Channel c
          inner join core.tChannel ch
                  on ch.ChannelId = c.ChannelId
          inner join core.tCurrency cu
                  on cu.CurrencyId = ch.CurrencyId

	
	insert into @Tree values (1000445, 0, 1)	-- Barn och Baby	
	insert into @Tree values (1000494, 0, 1)	-- Inredning
	insert into @Tree values (1000533, 0, 1)	-- Leksaker
	insert into @Tree values (1000856, 0, 1)	-- Underhållning
		
	declare @depth int
	set @depth = 0
	while (@@ROWCOUNT > 0)
	begin
		set @depth = @depth + 1
		
		insert into @Tree
		select c.CategoryId, @depth, 1
		from @Tree t 
			inner join product.tCategory c on c.ParentCategoryId = t.CategoryId
	end
		
	update t set HasChildren = 0
	from @Tree t
		left outer join product.tCategory c on c.ParentCategoryId = t.CategoryId
	where c.CategoryId is null
	
	-- Category
	select c.CategoryId, 	   
	   c.ParentCategoryId,	   
	   c.Title, c.ErpId, t.Depth
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		
	-- Category Translation
	select ct.CategoryId, ct.LanguageId, case when (ct.LanguageId = 1) then c.Title else ct.Title end as Title
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		inner join product.tCategoryTranslation ct on ct.CategoryId = t.CategoryId
	where ct.Title is not null
	
	-- Product to examine
	declare @CategoryLeaves table
	(
		CategoryId int primary key
	)
	insert into @CategoryLeaves
	select CategoryId
	from @Tree
	where Depth = 2 and HasChildren = 0
	
	declare @Product table
	(
		ProductId int primary key
	)
	insert into @Product
	select p.ProductId
	from product.tProduct p
		inner join @CategoryLeaves cl on cl.CategoryId = p.CategoryId
	where p.IsDeleted = 0


   -- Import products using the same third level category	
   if object_id('tempdb..#OnlineCategory') is not null
       drop table #OnlineCategory

   create table #OnlineCategory
   (
      CategoryId int primary key
   )
   
   insert into #OnlineCategory
   select distinct CategoryId
   from product.tProduct p
   where p.ProductStatusId = 0

   if object_id('tempdb..#CategoryLevel') is not null
       drop table #CategoryLevel

   create table #CategoryLevel
   (
      CategoryId int primary key,
      ErpId varchar(13),
      Level1Id int,
      Level2Id int,
      Level3Id int
   )
   	
   insert into #CategoryLevel ( CategoryId, ErpId, Level1Id, Level2Id, Level3Id )
   select c.CategoryId, 
      c.ErpId, 
      convert(int, substring(erpId, 3, 2)) as Level1Id, 
      convert(int, substring(erpId, 6, 3)) as Level2Id, 
      convert(int, substring(erpId, 10, 4)) as Level3Id
   from #OnlineCategory oc
		inner join product.tCategory c on c.CategoryId = oc.CategoryId
   where len(c.ErpId) = 13
 

   if object_id('tempdb..#CategoryMapping') is not null
       drop table #CategoryMapping

   create table #CategoryMapping
   (
      SourceCategoryId int primary key with (ignore_dup_key = on),
      DestinationCategoryId int
   )	

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and cl2.Level2Id = cl.Level2Id
         and not cl2.Level1Id in (30)
   where cl.Level1Id = 30     

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and not cl2.Level1Id in (30)
      inner join product.tCategory c on c.CategoryId = cl.CategoryId
		and not c.Title in ('Övrigt', 'Tillbehör och accessoarer', 'Blandade titlar', 'Reservdelar och tillbehör',
			'Plocklådor')
   where cl.Level1Id = 30


--   if object_id('tempdb..#CategoryMappingPath') is not null
--       drop table #CategoryMappingPath
   
--select cm.DestinationCategoryId, ppc.Title + '/' + pc.Title + '/' + c.Title as Path1, 
--   cm.SourceCategoryId, ppc2.Title + '/' +  pc2.Title + '/' +  c2.Title as Path2
----   ,p.Title
--into #CategoryMappingPath
--from #CategoryMapping cm
--   inner join product.tCategory c on c.CategoryId = cm.DestinationCategoryId
--   inner join product.tCategory pc on pc.CategoryId = c.ParentCategoryId
--   inner join product.tCategory ppc on ppc.CategoryId = pc.ParentCategoryId      
--   inner join product.tCategory c2 on c2.CategoryId = cm.SourceCategoryId
--   inner join product.tCategory pc2 on pc2.CategoryId = c2.ParentCategoryId
--   inner join product.tCategory ppc2 on ppc2.CategoryId = pc2.ParentCategoryId      
   

--select cmp.*, p.ProductId, p.Title
--from #CategoryMappingPath cmp 
--   inner join product.tProduct p on p.CategoryId = cmp.DestinationCategoryId
--where p.ProductStatusId = 0
--order by cmp.Path1, cmp.Path2, p.Title
  
   drop table #CategoryLevel
	
	--insert into @Product
	--select p.ProductId
	--from #CategoryMapping cm
	--   inner join product.tProduct p on p.CategoryId = cm.SourceCategoryId
	--where p.IsDeleted = 0

	
	delete from @Product where ProductId in (
		select ProductId
		from product.tProductImage
		group by ProductId, MediaId
		having count(*) > 1
	)

	delete tp
	from @Product tp
		inner join lekmer.tProductSize ps on ps.ProductId = tp.ProductId

	declare @SpecialCategoryMapping table
	(
		SourceCategoryId int primary key,
		DestinationCategoryId int
	)
	 insert into @SpecialCategoryMapping ( SourceCategoryId, DestinationCategoryId )
	 select c.CategoryId, 1001016
	 from product.tCategory c
	 where ParentCategoryId = 1001015
		and not CategoryId in (1000938
,1001016
,1001017
,1001018
,1001047
,1001062
,1001065
,1001071
,1001072
,1001209
,1001284
,1001355
,1001368
,1002017)

	
	-- Product
   select p.ProductId
         ,lp.HyErpId as ErpId
         ,p.EanCode
         ,p.IsDeleted
         ,case 
				when (scm.DestinationCategoryId is not null) then scm.DestinationCategoryId 
				when (cm.DestinationCategoryId is not null) then cm.DestinationCategoryId 			
			else 
				p.CategoryId 
			end as CategoryId
         ,p.Title
         ,p.ProductStatusId
         ,lp.AgeFromMonth
         ,lp.AgeToMonth
		   ,lp.LekmerErpId as SupplierArticleNumber
		   ,p.[Description]
		   ,lp.BrandId
		   ,p.NumberInStock
     from @Product t
          inner join product.tProduct p on p.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on t.ProductId = lp.ProductId
          left outer join @SpecialCategoryMapping scm on scm.SourceCategoryId = p.CategoryId
          left outer join #CategoryMapping cm on cm.SourceCategoryId = p.CategoryId
			
	  drop table #CategoryMapping
---- Discounts
--declare @PriceListDiscounts table
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2)
--)
--insert into @PriceListDiscounts ( PriceListId, ProductId, DiscountPrice )
--select 
--   case 
--      when (c.CampaignRegistryId = 1) then 1
--      when (c.CampaignRegistryId = 2) then 2
--      when (c.CampaignRegistryId = 3) then 3
--      when (c.CampaignRegistryId = 4) then 4
--      when (c.CampaignRegistryId = 5) then 5
--   else
--      c.CampaignRegistryId
--   end as PriceListId,
--   pdai.ProductId, pdai.DiscountPrice, pda.ProductActionId
--from campaign.tCampaign c
--   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId   
--   inner join lekmer.tProductDiscountAction pda on pda.ProductActionId = pa.ProductActionId
--   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pda.ProductActionId   
--where c.CampaignStatusId = 0
--   and (c.StartDate is null or c.StartDate <= getdate())
--   and (c.EndDate is null or getdate() <= c.EndDate)
--   and pdai.ProductId = 7364


--create table #PriceListDiscount
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2) primary key (PriceListId, ProductId)
--)

--insert into #PriceListDiscount
--select PriceListId, ProductId, min(DiscountPrice)
--from @PriceListDiscounts
--group by PriceListId, ProductId

if object_id('tempdb..#Campaign') is not null
    drop table #Campaign

create table #Campaign
(
   CampaignId int primary key,
   PriceListId int  
)

insert into #Campaign ( CampaignId, PriceListId )
select c.CampaignId, c.CampaignRegistryId
from campaign.tCampaign c
where c.CampaignStatusId = 0
   and (c.StartDate is null or c.StartDate <= getdate())
   and (c.EndDate is null or getdate() <= c.EndDate)



if object_id('tempdb..#CampaignAction') is not null
    drop table #CampaignAction

create table #CampaignAction (
   CampaignId int,
   SortIndex int,   
   ProductActionId int,
   Ordinal int
)

declare @Index int
set @Index = 1

insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
from #Campaign c
   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
   left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
      and pa2.Ordinal < pa.Ordinal
where pa2.ProductActionId is null

while (@@ROWCOUNT > 0)
begin
   set @Index = @Index + 1
      
   insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
   select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
   from #Campaign c      
      inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
         and tca.SortIndex = @Index - 1
      inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
         and pa.Ordinal > tca.Ordinal
      left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
         and pa2.Ordinal > tca.Ordinal
         and pa2.Ordinal < pa.Ordinal
   where pa2.ProductActionId is null
   option (force order)
end

if object_id('tempdb..#CampaignProduct') is not null
    drop table #CampaignProduct

create table #CampaignProduct (
   CampaignId int,
   ProductId int,
   Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
)

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, ip.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId
   
   
	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, p.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		inner join campaign.tPercentagePriceDiscountActionIncludeCategory ic on ic.ProductActionId = ca.ProductActionId
		inner join product.tProduct p on p.CategoryId = ic.CategoryId      

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, pdai.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId

update cp set Price = pli.PriceIncludingVat
from #Campaign c
   inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
   inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
      and pli.ProductId = cp.ProductId

declare @LastIndex int
set @Index = 1
select @LastIndex = max(SortIndex)
from #CampaignAction

while (@Index <= @LastIndex)
begin
		update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1
			inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId         
			inner join campaign.tPercentagePriceDiscountActionIncludeCategory ic on ic.ProductActionId = ca.ProductActionId
			inner join product.tProduct p on p.CategoryId = ic.CategoryId
			inner join #CampaignProduct cp on cp.ProductId = p.ProductId
				and cp.CampaignId = ca.CampaignId
		where ca.SortIndex = @Index

   update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1
      inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId         
      inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
   
   update cp set Price = pdai.DiscountPrice
   -- select cp.Price, pdai.DiscountPrice
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1000001
      inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
      and (cp.Price > pdai.DiscountPrice or cp.Price is null)

   set @Index = @Index + 1
end

delete cp
from #CampaignProduct cp 
where Price is null

update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
   

if object_id('tempdb..#BestCampaignPrice') is not null
    drop table #BestCampaignPrice

create table #BestCampaignPrice (
   ProductId int,
   PriceListId int,
   Price decimal(16, 2)
)
insert into #BestCampaignPrice (ProductId, PriceListId, Price)
select cp.ProductId, c.PriceListId, min(cp.Price) as Price
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
group by cp.ProductId, c.PriceListId

drop table #Campaign
drop table #CampaignAction
drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title

	-- Product Channel
	select pr.ProductId, pr.ProductRegistryId as ChannelId
	from @Channel c
		inner join product.tProductRegistryProduct pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tProduct p on p.ProductId = pr.ProductId
		inner join @Product t on t.ProductId = p.ProductId
		inner join @PriceList pl on pl.ChannelId = c.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		   and pli.ProductId = p.ProductId
	
	select pl.ChannelId, pli.ProductId, 
	   pli.PriceIncludingVat as Price, pli.PriceIncludingVat - pli.PriceExcludingVat as Vat, 
	   pld.Price as DiscountPrice, case when (pld.Price is null) then null else pld.Price * pli.VatPercentage / 100 end as DiscountPriceVat
	--into temp.CdonPriceNewCalc3
	from @PriceList pl
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		inner join @Product t on t.ProductId = pli.ProductId
		left outer join #BestCampaignPrice pld on pld.PriceListId = pl.PriceListId
		   and pld.ProductId = t.ProductId
	
   drop table #BestCampaignPrice
		
	--drop table #PriceListDiscount
	
	-- Product Media
	declare @AllProductImages table (
	   ProductId int,
	   MediaId int,
	   ProductImageGroupId int
	   primary key (ProductId, MediaId, ProductImageGroupId) with (ignore_dup_key = on)
	)

   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
         ,p.MediaId
         ,1 as ProductImageGroupId
   from @Product t
         inner join product.tProduct p on p.ProductId = t.ProductId
   where p.MediaId is not null
	
   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
      ,p.MediaId
      ,p.ProductImageGroupId
   from @Product t            
       inner join product.tProductImage p on p.ProductId = t.ProductId
   
   select pi.ProductId 
		  ,lp.HyErpId AS ErpId
		  ,pi.MediaId
		  ,pi.ProductImageGroupId
		  ,mf.Extension
	 from @AllProductImages pi
	      inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	      inner join media.tMedia m on pi.MediaId = m.MediaId
		   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
   -- Icons				   
	SELECT [lpi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProductIcon] lpi ON [lpi].[ProductId] = [p].[ProductId]
	UNION
	SELECT [bi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[BrandId] = [lp].[BrandId]
	UNION
	SELECT [ci].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [product].[tProduct] p1 ON [p1].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](p1.CategoryId))
      
   -- Product translations (title & description)            
   select pt.ProductId
         ,lp.HyErpId AS ErpId
         ,pt.LanguageId
         ,pt.Title
         ,pt.[Description]
     from @product t 
         inner join product.tProductTranslation pt on pt.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on lp.ProductId = pt.ProductId
    where Title is not null or Description is not null
    
    -- Brands  
   select b.BrandId
         ,b.Title
         ,b.ExternalUrl
         ,b.MediaId
         ,mf.Extension
     from lekmer.tBrand b 
          inner join media.tMedia m on m.MediaId = b.MediaId
          inner join media.tMediaFormat mf on mf.MediaFormatId = m.MediaFormatId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductTitleTranslationGetAllByProduct]'
GO
CREATE PROCEDURE [lekmer].[pProductTitleTranslationGetAllByProduct]
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.Title] AS 'Value'
	FROM
		[lekmer].[vProductTranslation]
	WHERE 
		[Product.Id] = @ProductId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[pAliasGetByCommonNameSecure]'
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 19.09.2008		Time: 11:20
Description:
			Created 
*****************  Version 2  *****************
User: Victor E.		Date: 23.01.2009
Description: Add Path
*****************  Version 3  *****************
User: Yuriy P.		Date: 26.01.2009
Description: Remove Path
*****************  Version 4  *****************
User: Yuriy P.		Date: 19.04.2013
Description: use [vCustomAliasSecure] instead [vCustomAlias]
*/
ALTER PROCEDURE [template].[pAliasGetByCommonNameSecure]
	@CommonName	varchar(100)
AS
begin
	set nocount on

	select
		*
	from
		[template].[vCustomAliasSecure]
	where
		[Alias.CommonName] = @CommonName
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pBlockProductSearchEsalesResultSave]'
GO
ALTER PROCEDURE [productlek].[pBlockProductSearchEsalesResultSave]
	@BlockId INT,
	@SecondaryTemplateId INT = NULL
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[productlek].[tBlockProductSearchEsalesResult]
	SET	
		[SecondaryTemplateId] = @SecondaryTemplateId
	WHERE
		[BlockId] = @BlockId
	
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[productlek].[tBlockProductSearchEsalesResult]
			( [BlockId],
			  [SecondaryTemplateId]
			)
		VALUES
			( @BlockId,
			  @SecondaryTemplateId
			)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pIconTranslationTitleGetAll]'
GO
CREATE PROCEDURE [lekmer].[pIconTranslationTitleGetAll]
	@IconId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [it].[IconTranslation.IconId] AS 'Id',
		[it].[IconTranslation.LanguageId] AS 'LanguageId',
		[it].[IconTranslation.Title] AS 'Value'
	FROM
	    [lekmer].[vIconTranslation] it
	WHERE 
		[it].[IconTranslation.IconId] = @IconId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBrandIconGetAllSecure]'
GO
CREATE PROCEDURE [lekmer].[pBrandIconGetAllSecure]
	@BrandId	INT
AS 
BEGIN 
	SELECT 
		i.*
	FROM 
		[lekmer].[vIconSecure] i
		INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[IconId] = [i].[Icon.Id]
	WHERE
		[bi].[BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pIconSave]'
GO
CREATE PROCEDURE [lekmer].[pIconSave]
	@IconId			INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(255),
	@MediaId		INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[lekmer].[tIcon]
	SET 
		[Title] = @Title,
		[Description] = @Description,
		[MediaId] = @MediaId
	WHERE
		[IconId] = @IconId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tIcon] (
			[Title],
			[Description],
			[MediaId]
		)
		VALUES (
			@Title,
			@Description,
			@MediaId
		)

		SET @IconId = SCOPE_IDENTITY()
	END

	RETURN @IconId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductTagAllForCampaigns]'
GO

CREATE PROCEDURE [lekmer].[pProductTagAllForCampaigns]
AS
BEGIN
	SET NOCOUNT ON

--Create temporary table for holding campaigns to tag products for
CREATE TABLE #Campaigns (
CampaignId int
)

--Get all campaigns online, and with landing page selected (we dont care if landing page exists, we just tag?)
insert into #Campaigns select c.CampaignId from campaign.tCampaign c where c.UseLandingPage = 1 and c.CampaignStatusId = 0

--Delete all tags for these campaigns before inserting new tags
delete from lekmer.tProductTag where TagId in (select t.TagId from lekmer.tTag t inner join lekmer.tTagGroup tg on tg.TagGroupId = t.TagGroupId where tg.CommonName = 'auto-kampanj' and t.CommonName in (select CampaignId from #Campaigns))

--Do the major selection and filter. The logic is copy of lekmer.pProductGetIdAllByCampaignId (keep them identical!!)
INSERT INTO lekmer.tProductTag (ProductId, TagId)
select ProductId, TagId from (
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1
 union
 select ProductId, CampaignId from campaign.tProductAction pa
inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pa.ProductActionId
where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union 
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1
except
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union 
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1

) c
inner join lekmer.tTag t on t.CommonName = c.CampaignId
inner join lekmer.tTagGroup tg on tg.TagGroupId = t.TagGroupId
where tg.CommonName = 'auto-kampanj'


drop table #Campaigns

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pProductExportToCdonOLD]'
GO
ALTER PROCEDURE [integration].[pProductExportToCdonOLD]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set transaction isolation level read uncommitted
	
	declare @Tree table (
		CategoryId int primary key with (ignore_dup_key = on),
		Depth int,
		HasChildren bit
	)
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		
   select ch.ChannelId
         ,ch.Title as Name
         ,replace(ch.Title, 'Lekmer.', '') as Tld
         ,cu.ISO as CurrencyCode
         ,cu.CurrencyId
     from @Channel c
          inner join core.tChannel ch
                  on ch.ChannelId = c.ChannelId
          inner join core.tCurrency cu
                  on cu.CurrencyId = ch.CurrencyId

	
	insert into @Tree values (1000533, 0, 1)	-- Root id for toys.
	
	declare @depth int
	set @depth = 0
	while (@@ROWCOUNT > 0)
	begin
		set @depth = @depth + 1
		
		insert into @Tree
		select c.CategoryId, @depth, 1
		from @Tree t 
			inner join product.tCategory c on c.ParentCategoryId = t.CategoryId
	end
	
	
	---- HACK TO ASSURE WE CAN IMPORT Ryggsäckar
	--insert into @Tree (CategoryId, Depth, HasChildren)
	--select c.CategoryId, 2, 1
	--from product.tCategory c
	--where c.CategoryId in (
	--   1000539,
 --     1000540,
 --     1000546,
 --     1000549,
 --     1000552,
 --     1000557,
 --     1000558,
 --     1000563,
 --     1000564,
 --     1000565,
 --     1000566,
 --     1000840,
 --     1000916)
 --     and c.ParentCategoryId in (1000538, 1000548)
	
	--insert into @Tree (CategoryId, Depth, HasChildren)
	--select c.CategoryId, 1, 0
	--from product.tCategory c
	--where c.CategoryId in (1000538, 1000548)
	---- HACK
	
	update t set HasChildren = 0
	from @Tree t
		left outer join product.tCategory c on c.ParentCategoryId = t.CategoryId
	where c.CategoryId is null
	
	-- Category
	select c.CategoryId, 
	   
	   c.ParentCategoryId,
	   ---- HACK TO ASSURE WE CAN IMPORT Ryggsäckar
	   --case when c.CategoryId in (1000538, 1000548) then 1000533 else c.ParentCategoryId end ParentCategoryId, 
	   
	   c.Title, c.ErpId, t.Depth
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		
	-- Category Translation
	select ct.CategoryId, ct.LanguageId, case when (ct.LanguageId = 1) then c.Title else ct.Title end as Title
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		inner join product.tCategoryTranslation ct on ct.CategoryId = t.CategoryId
	where ct.Title is not null
	
	-- Product to examine
	declare @CategoryLeaves table
	(
		CategoryId int primary key
	)
	insert into @CategoryLeaves
	select CategoryId
	from @Tree
	where Depth = 2 and HasChildren = 0
	
	declare @Product table
	(
		ProductId int primary key
	)
	insert into @Product
	select p.ProductId
	from product.tProduct p
		inner join @CategoryLeaves cl on cl.CategoryId = p.CategoryId
	where p.IsDeleted = 0
	
	delete from @Product where ProductId in (
		select ProductId
		from product.tProductImage
		group by ProductId, MediaId
		having count(*) > 1
	)
	
	-- Product
   select p.ProductId
         ,lp.HyErpId as ErpId
         ,p.EanCode
         ,p.IsDeleted
         ,p.CategoryId
         ,p.Title
         ,p.ProductStatusId
         ,lp.AgeFromMonth
         ,lp.AgeToMonth
		   ,lp.LekmerErpId as SupplierArticleNumber
		   ,p.[Description]
		   ,lp.BrandId
		   ,p.NumberInStock
     from @Product t
          inner join product.tProduct p
                  on p.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp
                  on t.ProductId = lp.ProductId
			
---- Discounts
--declare @PriceListDiscounts table
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2)
--)
--insert into @PriceListDiscounts ( PriceListId, ProductId, DiscountPrice )
--select 
--   case 
--      when (c.CampaignRegistryId = 1) then 1
--      when (c.CampaignRegistryId = 2) then 2
--      when (c.CampaignRegistryId = 3) then 3
--      when (c.CampaignRegistryId = 4) then 4
--      when (c.CampaignRegistryId = 5) then 5
--   else
--      c.CampaignRegistryId
--   end as PriceListId,
--   pdai.ProductId, pdai.DiscountPrice, pda.ProductActionId
--from campaign.tCampaign c
--   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId   
--   inner join lekmer.tProductDiscountAction pda on pda.ProductActionId = pa.ProductActionId
--   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pda.ProductActionId   
--where c.CampaignStatusId = 0
--   and (c.StartDate is null or c.StartDate <= getdate())
--   and (c.EndDate is null or getdate() <= c.EndDate)
--   and pdai.ProductId = 7364


--create table #PriceListDiscount
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2) primary key (PriceListId, ProductId)
--)

--insert into #PriceListDiscount
--select PriceListId, ProductId, min(DiscountPrice)
--from @PriceListDiscounts
--group by PriceListId, ProductId

if object_id('tempdb..#Campaign') is not null
    drop table #Campaign

create table #Campaign
(
   CampaignId int primary key,
   PriceListId int  
)

insert into #Campaign ( CampaignId, PriceListId )
select c.CampaignId, c.CampaignRegistryId
from campaign.tCampaign c
where c.CampaignStatusId = 0
   and (c.StartDate is null or c.StartDate <= getdate())
   and (c.EndDate is null or getdate() <= c.EndDate)



if object_id('tempdb..#CampaignAction') is not null
    drop table #CampaignAction

create table #CampaignAction (
   CampaignId int,
   SortIndex int,   
   ProductActionId int,
   Ordinal int
)

declare @Index int
set @Index = 1

insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
from #Campaign c
   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
   left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
      and pa2.Ordinal < pa.Ordinal
where pa2.ProductActionId is null

while (@@ROWCOUNT > 0)
begin
   set @Index = @Index + 1
      
   insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
   select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
   from #Campaign c      
      inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
         and tca.SortIndex = @Index - 1
      inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
         and pa.Ordinal > tca.Ordinal
      left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
         and pa2.Ordinal > tca.Ordinal
         and pa2.Ordinal < pa.Ordinal
   where pa2.ProductActionId is null
   option (force order)
end

if object_id('tempdb..#CampaignProduct') is not null
    drop table #CampaignProduct

create table #CampaignProduct (
   CampaignId int,
   ProductId int,
   Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
)

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, ip.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, pdai.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId

update cp set Price = pli.PriceIncludingVat
from #Campaign c
   inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
   inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
      and pli.ProductId = cp.ProductId

declare @LastIndex int
set @Index = 1
select @LastIndex = max(SortIndex)
from #CampaignAction

while (@Index <= @LastIndex)
begin
   update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1
      inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId         
      inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
   
   update cp set Price = pdai.DiscountPrice
   -- select cp.Price, pdai.DiscountPrice
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1000001
      inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
      and (cp.Price > pdai.DiscountPrice or cp.Price is null)

   set @Index = @Index + 1
end

delete cp
from #CampaignProduct cp 
where Price is null

update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
   

if object_id('tempdb..#BestCampaignPrice') is not null
    drop table #BestCampaignPrice

create table #BestCampaignPrice (
   ProductId int,
   PriceListId int,
   Price decimal(16, 2)
)
insert into #BestCampaignPrice (ProductId, PriceListId, Price)
select cp.ProductId, c.PriceListId, min(cp.Price) as Price
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
group by cp.ProductId, c.PriceListId

drop table #Campaign
drop table #CampaignAction
drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title

	-- Product Channel
	select pr.ProductId, pr.ProductRegistryId as ChannelId
	from @Channel c
		inner join product.tProductRegistryProduct pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tProduct p on p.ProductId = pr.ProductId
		inner join @Product t on t.ProductId = p.ProductId
		inner join @PriceList pl on pl.ChannelId = c.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		   and pli.ProductId = p.ProductId
	
	select pl.ChannelId, pli.ProductId, 
	   pli.PriceIncludingVat as Price, pli.PriceIncludingVat - pli.PriceExcludingVat as Vat, 
	   pld.Price as DiscountPrice, case when (pld.Price is null) then null else pld.Price * pli.VatPercentage / 100 end as DiscountPriceVat
	--into temp.CdonPriceNewCalc3
	from @PriceList pl
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		inner join @Product t on t.ProductId = pli.ProductId
		left outer join #BestCampaignPrice pld on pld.PriceListId = pl.PriceListId
		   and pld.ProductId = t.ProductId
	
   drop table #BestCampaignPrice
		
	--drop table #PriceListDiscount
	
	-- Product Media
	declare @AllProductImages table (
	   ProductId int,
	   MediaId int,
	   ProductImageGroupId int
	   primary key (ProductId, MediaId, ProductImageGroupId) with (ignore_dup_key = on)
	)

   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
         ,p.MediaId
         ,1 as ProductImageGroupId
   from @Product t
         inner join product.tProduct p on p.ProductId = t.ProductId
   where p.MediaId is not null
	
   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
      ,p.MediaId
      ,p.ProductImageGroupId
   from @Product t            
       inner join product.tProductImage p on p.ProductId = t.ProductId
   
   select pi.ProductId 
		  ,lp.HyErpId AS ErpId
		  ,pi.MediaId
		  ,pi.ProductImageGroupId
		  ,mf.Extension
	 from @AllProductImages pi
	      inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	      inner join media.tMedia m on pi.MediaId = m.MediaId
		   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
   -- Icons				   
	SELECT [lpi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProductIcon] lpi ON [lpi].[ProductId] = [p].[ProductId]
	UNION
	SELECT [bi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[BrandId] = [lp].[BrandId]
	UNION
	SELECT [ci].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [product].[tProduct] p1 ON [p1].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](p1.CategoryId))
      
   -- Product translations (title & description)            
   select pt.ProductId
         ,lp.HyErpId AS ErpId
         ,pt.LanguageId
         ,pt.Title
         ,pt.[Description]
     from @product t 
         inner join product.tProductTranslation pt on pt.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on lp.ProductId = pt.ProductId
    where Title is not null or Description is not null
    
    -- Brands  
   select b.BrandId
         ,b.Title
         ,b.ExternalUrl
         ,b.MediaId
         ,mf.Extension
     from lekmer.tBrand b 
          inner join media.tMedia m on m.MediaId = b.MediaId
          inner join media.tMediaFormat mf on mf.MediaFormatId = m.MediaFormatId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetIdAllByCampaignId]'
GO

CREATE PROCEDURE [lekmer].[pProductGetIdAllByCampaignId]
	@CampaignId		INT
AS
BEGIN
	SET NOCOUNT ON

---The logic also exists in lekmer.pProductTagAllForCampaigns, keep them identical!!

--PercentagePriceDiscountAction - inlclude brand
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--PercentagePriceDiscountAction - inlclude product
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId = @CampaignId
union
 --PercentagePriceDiscountAction - include category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --PercentagePriceDiscountAction - include category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1
 
 
 
 union
 
 
 
 -- ProductDiscount - include product
 select ProductId from campaign.tProductAction pa
inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pa.ProductActionId
where CampaignId = @CampaignId


union


-- FixedDiscount - include brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedDiscount - include product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedDiscount - include category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId

union

-- GiftVardViaMailProductAction - include brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--GiftVardViaMailProductAction - include product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId = @CampaignId
union 
 --GiftVardViaMailProductAction - include category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --GiftVardViaMailProductAction - include category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1



 
except




--PercentagePriceDiscountAction - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--PercentagePriceDiscountAction - exclude product
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId = @CampaignId
union
 --PercentagePriceDiscountAction - exclude category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --PercentagePriceDiscountAction - exclude category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1
 
 
 
 union
 
 
-- FixedDiscount - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedDiscount - exclude product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedDiscount - exclude category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId

union

-- GiftVardViaMailProductAction - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--GiftVardViaMailProductAction - exclude product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId = @CampaignId
union 
 --GiftVardViaMailProductAction - exclude category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --GiftVardViaMailProductAction - exclude category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1
 
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vIcon]'
GO
ALTER VIEW [lekmer].[vIcon]
AS
SELECT
	ic.IconId AS 'Icon.Id',
	COALESCE (it.Title, ic.Title) AS 'Icon.Title',
	COALESCE (it.[Description], ic.[Description]) AS 'Icon.Description', 
	im.*
FROM
	lekmer.tIcon ic INNER JOIN
	media.vCustomImage im ON ic.MediaId = im.[Image.MediaId] LEFT JOIN 
	lekmer.tIconTranslation it ON it.IconId = ic.IconId AND it.LanguageId = im.[Image.LanguageId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pIconDelete]'
GO
CREATE PROCEDURE [lekmer].[pIconDelete]
	@IconId	INT
AS
BEGIN
	DELETE FROM [lekmer].[tLekmerProductIcon]
	WHERE [IconId] = @IconId
	
	DELETE FROM [lekmer].[tBrandIcon]
	WHERE [IconId] = @IconId
	
	DELETE FROM [lekmer].[tCategoryIcon]
	WHERE [IconId] = @IconId
		
	EXEC [lekmer].[pIconTranslationDelete] @IconId	
		
	DELETE FROM [lekmer].[tIcon]
	WHERE [IconId] = @IconId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductIconGetAll]'
GO
CREATE PROCEDURE [lekmer].[pProductIconGetAll]
	@ProductId	INT,
	@CategoryId	INT,
	@BrandId	INT = NULL,
	@LanguageId	INT
AS 
BEGIN 
	SELECT i.*
	FROM 
		[lekmer].[vIcon] i
		INNER JOIN [lekmer].[tLekmerProductIcon] lpi ON lpi.IconId = i.[Icon.Id]
	WHERE
		lpi.ProductId = @ProductId
		AND i.[Image.LanguageId] = @LanguageId
		
	UNION
	
	SELECT i.*
	FROM 
		[lekmer].[vIcon] i
		INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[IconId] = [i].[Icon.Id]
	WHERE
		[bi].[BrandId] = @BrandId
		AND i.[Image.LanguageId] = @LanguageId
		
	UNION
	
	SELECT i.*
	FROM 
		[lekmer].[vIcon] i
		INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[IconId] = [i].[Icon.Id]
	WHERE
		[ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](@CategoryId))
		AND i.[Image.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBrandIconDeleteAll]'
GO
CREATE PROCEDURE [lekmer].[pBrandIconDeleteAll]
	@BrandId	INT
AS 
BEGIN 
	DELETE 
		[lekmer].[tBrandIcon]
	WHERE
		[BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCategoryIconDeleteAll]'
GO
CREATE PROCEDURE [lekmer].[pCategoryIconDeleteAll]
	@CategoryId	INT
AS 
BEGIN 
	DELETE 
		[lekmer].[tCategoryIcon]
	WHERE
		[CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageSave]'
GO
ALTER PROCEDURE [productlek].[pPackageSave]
	@PackageId			INT,
	@MasterProductId	INT,
	@ErpId				VARCHAR(50) = NULL,
	@StatusId			INT,
	@Title				NVARCHAR(256),
	@WebShopTitle		NVARCHAR(256),
	@NumberInStock		INT,
	@CategoryId			INT,
	@Description		NVARCHAR(MAX),
	@PriceXml			XML = NULL
	/*
	'<prices>
		<price priceListId="1" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="2" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="3" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="4" priceIncludingVat="10.99" vat="25.00"/>
	</prices>'
	*/
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	BEGIN
		BEGIN TRANSACTION
			IF @ErpId IS NULL
			BEGIN
				SET @ErpId = (SELECT TOP (1) [pr].[ErpId] 
						  FROM [productlek].[tPackage] p
								INNER JOIN [product].[tProduct] pr ON [pr].[ProductId] = [p].[MasterProductId]
						  ORDER BY [PackageId] DESC)

				IF @ErpId IS NULL
				BEGIN
					SET @ErpId = 'P000001-0000'
				END
				ELSE
				BEGIN
					DECLARE @ErpTemp INT
					SET @ErpTemp = (SELECT CAST(SUBSTRING(@ErpId, 2, 6) AS INT) + 1)

					SET @ErpId = (SELECT 'P' + RIGHT('000000' + CAST(@ErpTemp AS VARCHAR(6)), 6) + '-0000')
				END
			END
			
			-- tProduct
			INSERT INTO [product].[tProduct] (
				[ErpId],
				[IsDeleted],
				[NumberInStock],
				[CategoryId],
				[Title],
				[WebShopTitle],
				[Description],
				[ProductStatusId]
			)
			VALUES (
				@ErpId,
				0, -- IsDeleted
				@NumberInStock,
				@CategoryId,
				@Title,
				@WebShopTitle,
				@Description,
				@StatusId
			)

			SET @MasterProductId = SCOPE_IDENTITY()

			-- tLekmerProduct
			INSERT INTO [lekmer].[tLekmerProduct] (
				[ProductId],
				[IsBookable],
				[AgeFromMonth],
				[AgeToMonth],
				[IsBatteryIncluded],
				[HYErpId],
				[ShowVariantRelations],
				[ProductTypeId]
			)
			VALUES (
				@MasterProductId,
				0, -- IsBookable
				0, -- AgeFromMonth
				0, -- AgeToMonth
				0, -- IsBatteryIncluded
				@ErpId,
				0, -- ShowVariantRelations
				2 -- ProductTypeId = Package
			)
			
			-- tProductUrl
			INSERT INTO [lekmer].[tProductUrl] (
				[ProductId],
				[LanguageId],
				[UrlTitle])
			SELECT
				@MasterProductId,
				[LanguageId],
				CASE WHEN @WebShopTitle IS NOT NULL THEN @WebShopTitle ELSE @Title END
			FROM [core].[tLanguage]

			-- tProductRegistryProduct
			INSERT INTO [product].[tProductRegistryProduct] (
				[ProductId],
				[ProductRegistryId]
			)
			SELECT
				@MasterProductId,
				[pr].[ProductRegistryId]
			FROM  [product].[tProductRegistry] pr
			WHERE NOT EXISTS (SELECT 1 FROM [product].[tProductRegistryProduct] prp
							  WHERE [prp].[ProductId] = @MasterProductId
									AND [prp].[ProductRegistryId] = [pr].[ProductRegistryId])

			-- tPackage
			INSERT INTO [productlek].[tPackage] ([MasterProductId]) SELECT @MasterProductId
			
			SET @PackageId = CAST(SCOPE_IDENTITY() AS INT)
		COMMIT
	END
	ELSE
	BEGIN
		-- tProduct
		UPDATE [product].[tProduct]
		SET [NumberInStock] = @NumberInStock,
			[CategoryId] = @CategoryId,
			[ProductStatusId] = @StatusId,
			[Title] = @Title,
			[WebShopTitle] = @WebShopTitle,
			[Description] = @Description
		WHERE [ProductId] = @MasterProductId
	END
	
	-- tPriceListItem
	DELETE FROM [product].[tPriceListItem] WHERE [ProductId] = @MasterProductId
	
	IF @PriceXml IS NOT NULL
	BEGIN
		INSERT INTO [product].[tPriceListItem] (
				[PriceListId],
				[ProductId],
				[PriceIncludingVat],
				[PriceExcludingVat],
				[VatPercentage]
			)
		SELECT
			c.value('@priceListId[1]', 'int'),
			@MasterProductId,
			c.value('@priceIncludingVat[1]', 'decimal(16,2)'),
			c.value('@priceIncludingVat[1]', 'decimal(16,2)') / (1.0+c.value('@vat[1]', 'decimal(16,2)')/100.0),
			c.value('@vat[1]', 'decimal(16,2)')
		FROM
			@PriceXml.nodes('/prices/price') T(c)
	END
	
	SELECT @PackageId, @MasterProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductIconSave]'
GO
CREATE PROCEDURE [lekmer].[pProductIconSave]
	@ProductId	int,
	@IconId		int
AS 
BEGIN 
	INSERT [lekmer].[tLekmerProductIcon]
	VALUES(@ProductId, @IconId)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[vKlarnaTransaction]'
GO


ALTER VIEW [orderlek].[vKlarnaTransaction]
AS
SELECT
	[kt].[KlarnaTransactionId],
	[kt].[KlarnaShopId],
	[kt].[KlarnaMode],
	
	CASE [kt].[KlarnaMode]
		WHEN 0 THEN 'NO_FLAG'
		WHEN 2 THEN 'KRED_TEST_MODE'
		ELSE CONVERT(VARCHAR(50), [kt].[KlarnaMode])
	END AS 'KlarnaMode.Name',
	
	[kt].[KlarnaTransactionTypeId],
	
	CASE [kt].[KlarnaTransactionTypeId]
		WHEN 1 THEN 'GetAddress'
		WHEN 2 THEN 'ReserveAmount'
		WHEN 3 THEN 'ReserveAmountCompany'
		ELSE CONVERT(VARCHAR(50), [kt].[KlarnaTransactionTypeId])
	END AS 'KlarnaTransactionType.Name',
	
	[kt].[Created],
	[kt].[CivicNumber],
	[kt].[OrderId],
	[kt].[Amount],
	[kt].[CurrencyId],
	
	CASE [kt].[CurrencyId]
		WHEN 0 THEN 'SEK'
		WHEN 1 THEN 'NOK'
		WHEN 2 THEN 'EUR'
		WHEN 3 THEN 'DKK'
		ELSE CONVERT(VARCHAR(50), [kt].[CurrencyId])
	END AS 'Currency.Code',
	
	[kt].[PClassId],
	[kt].[YearlySalary],
	[kt].[Duration],
	[kt].[ReservationNumber],
	[kt].[ResponseCodeId],
	
	CASE [kt].[ResponseCodeId]
		WHEN 1 THEN 'OK'
		WHEN 2 THEN 'NoRisk'
		WHEN 3 THEN 'TimeoutResponse'
		WHEN 4 THEN 'SpecifiedError'
		WHEN 5 THEN 'UnspecifiedError'
		ELSE CONVERT(VARCHAR(50), [kt].[ResponseCodeId])
	END AS 'ResponseCode.Name',
	
	[kt].[KlarnaFaultCode],
	[kt].[KlarnaFaultReason]
FROM
	[orderlek].[tKlarnaTransaction] kt

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignSave]'
GO
ALTER PROCEDURE [campaign].[pCampaignSave]
	@CampaignId			INT,
	@Title				VARCHAR(500),
	@CampaignStatusId	INT,
	@CampaignRegistryId INT,
	@FolderId			INT,
	@StartDate			DATETIME,
	@EndDate			DATETIME,
	@Exclusive			BIT,
	@Priority			INT,
	@LevelId			INT	,
	@WebTitle			VARCHAR(500) =null,
	@IconMediaId		INT = NULL,
	@ImageMediaId		INT = NULL,
	@Description		VARCHAR(MAX) = NULL,
	@LinkContentNodeId	INT = NULL,
	@UseLandingPage		BIT = 0
AS
BEGIN
	UPDATE
		[campaign].[tCampaign]
	SET
		[Title]					= @Title,
		[CampaignStatusId]		= @CampaignStatusId,
		[CampaignRegistryId]	= @CampaignRegistryId,
		[FolderId]				= @FolderId,
		[StartDate]				= @StartDate,
		[EndDate]				= @EndDate,
		[Exclusive]				= @Exclusive,
		[Priority]				= @Priority,
		[LevelId]				= @LevelId,
		[WebTitle]				= @WebTitle,
		[IconMediaId]			= @IconMediaId,
		[ImageMediaId]			= @ImageMediaId,
		[Description]			= @Description,
		[LinkContentNodeId]		= @LinkContentNodeId,
		[UseLandingPage]		= @UseLandingPage
	WHERE
		[CampaignId] = @CampaignId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		SET @Priority = (SELECT MAX(PRIORITY) FROM campaign.tCampaign) + 1
		IF @Priority IS NULL
			BEGIN
				SET @Priority = 1
			END
			
		INSERT campaign.tCampaign (
			[Title],
			[CampaignStatusId],
			[CampaignRegistryId],
			[FolderId],
			[StartDate],
			[EndDate],
			[Exclusive],
			[Priority],
			[LevelId],
			[WebTitle],
			[IconMediaId],
			[ImageMediaId],
			[Description],
			[LinkContentNodeId],
			[UseLandingPage]
		)
		VALUES (
			@Title,
			@CampaignStatusId,
			@CampaignRegistryId,
			@FolderId,
			@StartDate,
			@EndDate,
			@Exclusive,
			@Priority,
			@LevelId,
			@WebTitle,
			@IconMediaId,
			@ImageMediaId,
			@Description,
			@LinkContentNodeId,
			@UseLandingPage	
		)
		
		SET @CampaignId = CAST(SCOPE_IDENTITY() AS INT)
	END
	RETURN @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pIconGetAllWithSizes]'
GO
CREATE PROCEDURE [lekmer].[pIconGetAllWithSizes]
    @Page INT = NULL,
    @PageSize INT,
    @SortBy VARCHAR(20) = NULL,
    @SortDescending BIT = NULL
AS 
    BEGIN
        SET NOCOUNT ON
	
        IF ( generic.fnValidateColumnName(@SortBy) = 0 ) 
        BEGIN
            RAISERROR ( N'Illegal characters in string (parameter @SortBy): %s' , 16 , 1 , @SortBy ) ;
            RETURN
        END

        DECLARE @sql NVARCHAR(MAX)
        DECLARE @sqlCount NVARCHAR(MAX)
        DECLARE @sqlFragment NVARCHAR(MAX)

        SET @sqlFragment = '
			SELECT ROW_NUMBER() OVER (ORDER BY i.[' 
				+ COALESCE(@SortBy, 'Icon.Title') 
				+ ']'
				+ CASE WHEN ( @SortDescending = 1 ) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				*
			FROM [lekmer].[vIconSecure] i'
			
        SET @sql = 'SELECT * FROM
			(' + @sqlFragment + '
			)
			AS SearchResult'
			
        IF @Page != 0 AND @Page IS NOT NULL 
        BEGIN
            SET @sql = @sql + '
			WHERE Number > ' + CAST(( @Page - 1 ) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
        END
		
        SET @sqlCount = 'SELECT COUNT(1) FROM
			(
			' + @sqlFragment + '
			)
			AS CountResults'
			
        EXEC sp_executesql @sqlCount
        EXEC sp_executesql @sql
    END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pProductExportToCdon]'
GO
ALTER PROCEDURE [integration].[pProductExportToCdon]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set transaction isolation level read uncommitted
	
	declare @Tree table (
		CategoryId int primary key with (ignore_dup_key = on),
		Depth int,
		HasChildren bit
	)
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		
   select ch.ChannelId
         ,ch.Title as Name
         ,replace(ch.Title, 'Lekmer.', '') as Tld
         ,cu.ISO as CurrencyCode
         ,cu.CurrencyId
     from @Channel c
          inner join core.tChannel ch
                  on ch.ChannelId = c.ChannelId
          inner join core.tCurrency cu
                  on cu.CurrencyId = ch.CurrencyId

	
	insert into @Tree values (1000445, 0, 1)	-- Barn och Baby	
	insert into @Tree values (1000494, 0, 1)	-- Inredning
	insert into @Tree values (1000533, 0, 1)	-- Leksaker
	insert into @Tree values (1000856, 0, 1)	-- Underhållning
		
	declare @depth int
	set @depth = 0
	while (@@ROWCOUNT > 0)
	begin
		set @depth = @depth + 1
		
		insert into @Tree
		select c.CategoryId, @depth, 1
		from @Tree t 
			inner join product.tCategory c on c.ParentCategoryId = t.CategoryId
	end
		
	update t set HasChildren = 0
	from @Tree t
		left outer join product.tCategory c on c.ParentCategoryId = t.CategoryId
	where c.CategoryId is null
	
	-- Category
	select c.CategoryId, 	   
	   c.ParentCategoryId,	   
	   c.Title, c.ErpId, t.Depth
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		
	-- Category Translation
	select ct.CategoryId, ct.LanguageId, case when (ct.LanguageId = 1) then c.Title else ct.Title end as Title
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		inner join product.tCategoryTranslation ct on ct.CategoryId = t.CategoryId
	where ct.Title is not null
	
	-- Product to examine
	declare @CategoryLeaves table
	(
		CategoryId int primary key
	)
	insert into @CategoryLeaves
	select CategoryId
	from @Tree
	where Depth = 2 and HasChildren = 0
	
	declare @Product table
	(
		ProductId int primary key
	)
	insert into @Product
	select p.ProductId
	from product.tProduct p
		inner join @CategoryLeaves cl on cl.CategoryId = p.CategoryId
	where p.IsDeleted = 0


   -- Import products using the same third level category	
   if object_id('tempdb..#OnlineCategory') is not null
       drop table #OnlineCategory

   create table #OnlineCategory
   (
      CategoryId int primary key
   )
   
   insert into #OnlineCategory
   select distinct CategoryId
   from product.tProduct p
   where p.ProductStatusId = 0

   if object_id('tempdb..#CategoryLevel') is not null
       drop table #CategoryLevel

   create table #CategoryLevel
   (
      CategoryId int primary key,
      ErpId varchar(13),
      Level1Id int,
      Level2Id int,
      Level3Id int
   )
   	
   insert into #CategoryLevel ( CategoryId, ErpId, Level1Id, Level2Id, Level3Id )
   select c.CategoryId, 
      c.ErpId, 
      convert(int, substring(erpId, 3, 2)) as Level1Id, 
      convert(int, substring(erpId, 6, 3)) as Level2Id, 
      convert(int, substring(erpId, 10, 4)) as Level3Id
   from #OnlineCategory oc
		inner join product.tCategory c on c.CategoryId = oc.CategoryId
   where len(c.ErpId) = 13
 

   if object_id('tempdb..#CategoryMapping') is not null
       drop table #CategoryMapping

   create table #CategoryMapping
   (
      SourceCategoryId int primary key with (ignore_dup_key = on),
      DestinationCategoryId int
   )	

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and cl2.Level2Id = cl.Level2Id
         and not cl2.Level1Id in (30)
   where cl.Level1Id = 30     

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and not cl2.Level1Id in (30)
      inner join product.tCategory c on c.CategoryId = cl.CategoryId
		and not c.Title in ('Övrigt', 'Tillbehör och accessoarer', 'Blandade titlar', 'Reservdelar och tillbehör',
			'Plocklådor')
   where cl.Level1Id = 30


--   if object_id('tempdb..#CategoryMappingPath') is not null
--       drop table #CategoryMappingPath
   
--select cm.DestinationCategoryId, ppc.Title + '/' + pc.Title + '/' + c.Title as Path1, 
--   cm.SourceCategoryId, ppc2.Title + '/' +  pc2.Title + '/' +  c2.Title as Path2
----   ,p.Title
--into #CategoryMappingPath
--from #CategoryMapping cm
--   inner join product.tCategory c on c.CategoryId = cm.DestinationCategoryId
--   inner join product.tCategory pc on pc.CategoryId = c.ParentCategoryId
--   inner join product.tCategory ppc on ppc.CategoryId = pc.ParentCategoryId      
--   inner join product.tCategory c2 on c2.CategoryId = cm.SourceCategoryId
--   inner join product.tCategory pc2 on pc2.CategoryId = c2.ParentCategoryId
--   inner join product.tCategory ppc2 on ppc2.CategoryId = pc2.ParentCategoryId      
   

--select cmp.*, p.ProductId, p.Title
--from #CategoryMappingPath cmp 
--   inner join product.tProduct p on p.CategoryId = cmp.DestinationCategoryId
--where p.ProductStatusId = 0
--order by cmp.Path1, cmp.Path2, p.Title
  
   drop table #CategoryLevel
	
	--insert into @Product
	--select p.ProductId
	--from #CategoryMapping cm
	--   inner join product.tProduct p on p.CategoryId = cm.SourceCategoryId
	--where p.IsDeleted = 0

	
	delete from @Product where ProductId in (
		select ProductId
		from product.tProductImage
		group by ProductId, MediaId
		having count(*) > 1
	)

	delete tp
	from @Product tp
		inner join lekmer.tProductSize ps on ps.ProductId = tp.ProductId
	
	-- Brand Restrictions 
	-- Besafe
	delete from @Product where ProductId in (Select ProductId from lekmer.tLekmerProduct where BrandId = 1000588)
	-- Crescent
	delete from @Product where ProductId in (Select ProductId from lekmer.tLekmerProduct where BrandId = 1000565)
		
	-- Product
   select p.ProductId
         ,lp.HyErpId as ErpId
         ,p.EanCode
         ,p.IsDeleted
         ,case 
				when (cm.DestinationCategoryId is not null) then cm.DestinationCategoryId 			
			else 
				p.CategoryId 
			end as CategoryId
         ,p.Title
         ,p.ProductStatusId
         ,lp.AgeFromMonth
         ,lp.AgeToMonth
		   ,lp.LekmerErpId as SupplierArticleNumber
		   ,p.[Description]
		   ,lp.BrandId
		   ,p.NumberInStock
     from @Product t
          inner join product.tProduct p on p.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on t.ProductId = lp.ProductId
          left outer join #CategoryMapping cm on cm.SourceCategoryId = p.CategoryId
			
	  drop table #CategoryMapping

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title


	-- Product Channel
	select pr.ProductId, pr.ProductRegistryId as ChannelId
	from @Channel c
		inner join product.tProductRegistryProduct pr on pr.ProductRegistryId = c.ChannelId
		inner join export.tProductPrice pp on pp.ChannelId = c.ChannelId	
			and pp.ProductId = pr.ProductId
		inner join product.tProduct p on p.ProductId = pr.ProductId
		inner join @Product t on t.ProductId = p.ProductId
		inner join @PriceList pl on pl.ChannelId = c.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		   and pli.ProductId = p.ProductId
	
	-- Price List Price
	select pp.ChannelId, pp.ProductId, 
	   case when (pp.ChannelId = 4) then pp.PriceIncludingVat else ROUND(pp.PriceIncludingVat, 0) end as Price, 
	   case when (pp.ChannelId = 4) then pp.PriceIncludingVat else ROUND(pp.PriceIncludingVat, 0) end * pli.VatPercentage / (100 + pli.VatPercentage) as Vat, 
	   case when (pp.DiscountPriceIncludingVat is not null and pp.ChannelId <> 4) then ROUND(pp.DiscountPriceIncludingVat, 0) else DiscountPriceIncludingVat end as DiscountPrice, 
	   case when (pp.DiscountPriceIncludingVat is not null and pp.ChannelId <> 4) then ROUND(pp.DiscountPriceIncludingVat, 0) else DiscountPriceIncludingVat end * pli.VatPercentage / (100 + pli.VatPercentage) as DiscountPriceVat
	--into temp.CdonPriceNewCalc6New
	from export.tProductPrice pp
		inner join @Product t on t.ProductId = pp.ProductId
		inner join @PriceList pl on pl.ChannelId = pp.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
			and pli.ProductId = pp.ProductId
--	drop table temp.CdonPriceNewCalc6New

	-- Product Media
	declare @AllProductImages table (
	   ProductId int,
	   MediaId int,
	   ProductImageGroupId int
	   primary key (ProductId, MediaId, ProductImageGroupId) with (ignore_dup_key = on)
	)

   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
         ,p.MediaId
         ,1 as ProductImageGroupId
   from @Product t
         inner join product.tProduct p on p.ProductId = t.ProductId
   where p.MediaId is not null
	
   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
      ,p.MediaId
      ,p.ProductImageGroupId
   from @Product t            
       inner join product.tProductImage p on p.ProductId = t.ProductId
   
   select pi.ProductId 
		  ,lp.HyErpId AS ErpId
		  ,pi.MediaId
		  ,pi.ProductImageGroupId
		  ,mf.Extension
	 from @AllProductImages pi
	      inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	      inner join media.tMedia m on pi.MediaId = m.MediaId
		   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
    -- Icons				   
	SELECT [lpi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProductIcon] lpi ON [lpi].[ProductId] = [p].[ProductId]
	UNION
	SELECT [bi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[BrandId] = [lp].[BrandId]
	UNION
	SELECT [ci].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [product].[tProduct] p1 ON [p1].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](p1.CategoryId))
      
   -- Product translations (title & description)            
   select pt.ProductId
         ,lp.HyErpId AS ErpId
         ,pt.LanguageId
         ,pt.Title
         ,pt.[Description]
     from @product t 
         inner join product.tProductTranslation pt on pt.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on lp.ProductId = pt.ProductId
    where Title is not null or Description is not null
    
    -- Brands
   select b.BrandId
         ,b.Title
         ,b.ExternalUrl
         ,b.MediaId
         ,mf.Extension
     from lekmer.tBrand b 
          inner join media.tMedia m on m.MediaId = b.MediaId
          inner join media.tMediaFormat mf on mf.MediaFormatId = m.MediaFormatId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductIconGetAllByCategoryAndBrandSecure]'
GO
CREATE PROCEDURE [lekmer].[pProductIconGetAllByCategoryAndBrandSecure]
	@CategoryId	INT,
	@BrandId	INT = NULL
AS 
BEGIN 
	SELECT i.*
	FROM 
		[lekmer].[vIconSecure] i 
		INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[IconId] = [i].[Icon.Id]
	WHERE [bi].[BrandId] = @BrandId
	UNION
	SELECT i.*
	FROM 
		[lekmer].[vIconSecure] i 
		INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[IconId] = [i].[Icon.Id]
	WHERE [ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](@CategoryId))
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pIconGetByIdSecure]'
GO
CREATE PROCEDURE [lekmer].[pIconGetByIdSecure]
	@IconId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		i.*
	FROM
		[lekmer].[vIconSecure] i
	WHERE
		[i].[Icon.Id] = @IconId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pProductExportToCdonOLD2]'
GO
ALTER PROCEDURE [integration].[pProductExportToCdonOLD2]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set transaction isolation level read uncommitted
	
	declare @Tree table (
		CategoryId int primary key with (ignore_dup_key = on),
		Depth int,
		HasChildren bit
	)
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		
   select ch.ChannelId
         ,ch.Title as Name
         ,replace(ch.Title, 'Lekmer.', '') as Tld
         ,cu.ISO as CurrencyCode
         ,cu.CurrencyId
     from @Channel c
          inner join core.tChannel ch
                  on ch.ChannelId = c.ChannelId
          inner join core.tCurrency cu
                  on cu.CurrencyId = ch.CurrencyId

	
	insert into @Tree values (1000533, 0, 1)	-- Root id for toys.
	
	declare @depth int
	set @depth = 0
	while (@@ROWCOUNT > 0)
	begin
		set @depth = @depth + 1
		
		insert into @Tree
		select c.CategoryId, @depth, 1
		from @Tree t 
			inner join product.tCategory c on c.ParentCategoryId = t.CategoryId
	end
		
	update t set HasChildren = 0
	from @Tree t
		left outer join product.tCategory c on c.ParentCategoryId = t.CategoryId
	where c.CategoryId is null
	
	-- Category
	select c.CategoryId, 	   
	   c.ParentCategoryId,	   
	   c.Title, c.ErpId, t.Depth
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		
	-- Category Translation
	select ct.CategoryId, ct.LanguageId, case when (ct.LanguageId = 1) then c.Title else ct.Title end as Title
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		inner join product.tCategoryTranslation ct on ct.CategoryId = t.CategoryId
	where ct.Title is not null
	
	-- Product to examine
	declare @CategoryLeaves table
	(
		CategoryId int primary key
	)
	insert into @CategoryLeaves
	select CategoryId
	from @Tree
	where Depth = 2 and HasChildren = 0
	
	declare @Product table
	(
		ProductId int primary key
	)
	insert into @Product
	select p.ProductId
	from product.tProduct p
		inner join @CategoryLeaves cl on cl.CategoryId = p.CategoryId
	where p.IsDeleted = 0


   -- Import products using the same third level category	
   if object_id('tempdb..#OnlineCategory') is not null
       drop table #OnlineCategory

   create table #OnlineCategory
   (
      CategoryId int primary key
   )
   
   insert into #OnlineCategory
   select distinct CategoryId
   from product.tProduct p
   where p.ProductStatusId = 0

   if object_id('tempdb..#CategoryLevel') is not null
       drop table #CategoryLevel

   create table #CategoryLevel
   (
      CategoryId int primary key,
      ErpId varchar(13),
      Level1Id int,
      Level2Id int,
      Level3Id int
   )
   	
   insert into #CategoryLevel ( CategoryId, ErpId, Level1Id, Level2Id, Level3Id )
   select c.CategoryId, 
      c.ErpId, 
      convert(int, substring(erpId, 3, 2)) as Level1Id, 
      convert(int, substring(erpId, 6, 3)) as Level2Id, 
      convert(int, substring(erpId, 10, 4)) as Level3Id
   from #OnlineCategory oc
		inner join product.tCategory c on c.CategoryId = oc.CategoryId
   where len(c.ErpId) = 13
 

   if object_id('tempdb..#CategoryMapping') is not null
       drop table #CategoryMapping

   create table #CategoryMapping
   (
      SourceCategoryId int primary key with (ignore_dup_key = on),
      DestinationCategoryId int
   )	

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and cl2.Level2Id = cl.Level2Id
         and not cl2.Level1Id in (30)
   where cl.Level1Id = 30     

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and not cl2.Level1Id in (30)
      inner join product.tCategory c on c.CategoryId = cl.CategoryId
		and not c.Title in ('Övrigt', 'Tillbehör och accessoarer', 'Blandade titlar', 'Reservdelar och tillbehör',
			'Plocklådor')
   where cl.Level1Id = 30


--   if object_id('tempdb..#CategoryMappingPath') is not null
--       drop table #CategoryMappingPath
   
--select cm.DestinationCategoryId, ppc.Title + '/' + pc.Title + '/' + c.Title as Path1, 
--   cm.SourceCategoryId, ppc2.Title + '/' +  pc2.Title + '/' +  c2.Title as Path2
----   ,p.Title
--into #CategoryMappingPath
--from #CategoryMapping cm
--   inner join product.tCategory c on c.CategoryId = cm.DestinationCategoryId
--   inner join product.tCategory pc on pc.CategoryId = c.ParentCategoryId
--   inner join product.tCategory ppc on ppc.CategoryId = pc.ParentCategoryId      
--   inner join product.tCategory c2 on c2.CategoryId = cm.SourceCategoryId
--   inner join product.tCategory pc2 on pc2.CategoryId = c2.ParentCategoryId
--   inner join product.tCategory ppc2 on ppc2.CategoryId = pc2.ParentCategoryId      
   

--select cmp.*, p.ProductId, p.Title
--from #CategoryMappingPath cmp 
--   inner join product.tProduct p on p.CategoryId = cmp.DestinationCategoryId
--where p.ProductStatusId = 0
--order by cmp.Path1, cmp.Path2, p.Title
  
   drop table #CategoryLevel
	
	insert into @Product
	select p.ProductId
	from #CategoryMapping cm
	   inner join product.tProduct p on p.CategoryId = cm.SourceCategoryId
	where p.IsDeleted = 0

	
	delete from @Product where ProductId in (
		select ProductId
		from product.tProductImage
		group by ProductId, MediaId
		having count(*) > 1
	)


	declare @SpecialCategoryMapping table
	(
		SourceCategoryId int primary key,
		DestinationCategoryId int
	)
	 insert into @SpecialCategoryMapping ( SourceCategoryId, DestinationCategoryId )
	 select c.CategoryId, 1001016
	 from product.tCategory c
	 where ParentCategoryId = 1001015
		and not CategoryId in (1000938
,1001016
,1001017
,1001018
,1001047
,1001062
,1001065
,1001071
,1001072
,1001209
,1001284
,1001355
,1001368
,1002017)

	
	-- Product
   select p.ProductId
         ,lp.HyErpId as ErpId
         ,p.EanCode
         ,p.IsDeleted
         ,case 
				when (scm.DestinationCategoryId is not null) then scm.DestinationCategoryId 
				when (cm.DestinationCategoryId is not null) then cm.DestinationCategoryId 			
			else 
				p.CategoryId 
			end as CategoryId
         ,p.Title
         ,p.ProductStatusId
         ,lp.AgeFromMonth
         ,lp.AgeToMonth
		   ,lp.LekmerErpId as SupplierArticleNumber
		   ,p.[Description]
		   ,lp.BrandId
		   ,p.NumberInStock
     from @Product t
          inner join product.tProduct p on p.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on t.ProductId = lp.ProductId
          left outer join @SpecialCategoryMapping scm on scm.SourceCategoryId = p.CategoryId
          left outer join #CategoryMapping cm on cm.SourceCategoryId = p.CategoryId
			
	  drop table #CategoryMapping
---- Discounts
--declare @PriceListDiscounts table
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2)
--)
--insert into @PriceListDiscounts ( PriceListId, ProductId, DiscountPrice )
--select 
--   case 
--      when (c.CampaignRegistryId = 1) then 1
--      when (c.CampaignRegistryId = 2) then 2
--      when (c.CampaignRegistryId = 3) then 3
--      when (c.CampaignRegistryId = 4) then 4
--      when (c.CampaignRegistryId = 5) then 5
--   else
--      c.CampaignRegistryId
--   end as PriceListId,
--   pdai.ProductId, pdai.DiscountPrice, pda.ProductActionId
--from campaign.tCampaign c
--   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId   
--   inner join lekmer.tProductDiscountAction pda on pda.ProductActionId = pa.ProductActionId
--   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pda.ProductActionId   
--where c.CampaignStatusId = 0
--   and (c.StartDate is null or c.StartDate <= getdate())
--   and (c.EndDate is null or getdate() <= c.EndDate)
--   and pdai.ProductId = 7364


--create table #PriceListDiscount
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2) primary key (PriceListId, ProductId)
--)

--insert into #PriceListDiscount
--select PriceListId, ProductId, min(DiscountPrice)
--from @PriceListDiscounts
--group by PriceListId, ProductId

if object_id('tempdb..#Campaign') is not null
    drop table #Campaign

create table #Campaign
(
   CampaignId int primary key,
   PriceListId int  
)

insert into #Campaign ( CampaignId, PriceListId )
select c.CampaignId, c.CampaignRegistryId
from campaign.tCampaign c
where c.CampaignStatusId = 0
   and (c.StartDate is null or c.StartDate <= getdate())
   and (c.EndDate is null or getdate() <= c.EndDate)



if object_id('tempdb..#CampaignAction') is not null
    drop table #CampaignAction

create table #CampaignAction (
   CampaignId int,
   SortIndex int,   
   ProductActionId int,
   Ordinal int
)

declare @Index int
set @Index = 1

insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
from #Campaign c
   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
   left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
      and pa2.Ordinal < pa.Ordinal
where pa2.ProductActionId is null

while (@@ROWCOUNT > 0)
begin
   set @Index = @Index + 1
      
   insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
   select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
   from #Campaign c      
      inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
         and tca.SortIndex = @Index - 1
      inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
         and pa.Ordinal > tca.Ordinal
      left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
         and pa2.Ordinal > tca.Ordinal
         and pa2.Ordinal < pa.Ordinal
   where pa2.ProductActionId is null
   option (force order)
end

if object_id('tempdb..#CampaignProduct') is not null
    drop table #CampaignProduct

create table #CampaignProduct (
   CampaignId int,
   ProductId int,
   Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
)

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, ip.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, pdai.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId

update cp set Price = pli.PriceIncludingVat
from #Campaign c
   inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
   inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
      and pli.ProductId = cp.ProductId

declare @LastIndex int
set @Index = 1
select @LastIndex = max(SortIndex)
from #CampaignAction

while (@Index <= @LastIndex)
begin
   update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1
      inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId         
      inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
   
   update cp set Price = pdai.DiscountPrice
   -- select cp.Price, pdai.DiscountPrice
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1000001
      inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
      and (cp.Price > pdai.DiscountPrice or cp.Price is null)

   set @Index = @Index + 1
end

delete cp
from #CampaignProduct cp 
where Price is null

update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
   

if object_id('tempdb..#BestCampaignPrice') is not null
    drop table #BestCampaignPrice

create table #BestCampaignPrice (
   ProductId int,
   PriceListId int,
   Price decimal(16, 2)
)
insert into #BestCampaignPrice (ProductId, PriceListId, Price)
select cp.ProductId, c.PriceListId, min(cp.Price) as Price
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
group by cp.ProductId, c.PriceListId

drop table #Campaign
drop table #CampaignAction
drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title

	-- Product Channel
	select pr.ProductId, pr.ProductRegistryId as ChannelId
	from @Channel c
		inner join product.tProductRegistryProduct pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tProduct p on p.ProductId = pr.ProductId
		inner join @Product t on t.ProductId = p.ProductId
		inner join @PriceList pl on pl.ChannelId = c.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		   and pli.ProductId = p.ProductId
	
	select pl.ChannelId, pli.ProductId, 
	   pli.PriceIncludingVat as Price, pli.PriceIncludingVat - pli.PriceExcludingVat as Vat, 
	   pld.Price as DiscountPrice, case when (pld.Price is null) then null else pld.Price * pli.VatPercentage / 100 end as DiscountPriceVat
	--into temp.CdonPriceNewCalc3
	from @PriceList pl
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		inner join @Product t on t.ProductId = pli.ProductId
		left outer join #BestCampaignPrice pld on pld.PriceListId = pl.PriceListId
		   and pld.ProductId = t.ProductId
	
   drop table #BestCampaignPrice
		
	--drop table #PriceListDiscount
	
	-- Product Media
	declare @AllProductImages table (
	   ProductId int,
	   MediaId int,
	   ProductImageGroupId int
	   primary key (ProductId, MediaId, ProductImageGroupId) with (ignore_dup_key = on)
	)

   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
         ,p.MediaId
         ,1 as ProductImageGroupId
   from @Product t
         inner join product.tProduct p on p.ProductId = t.ProductId
   where p.MediaId is not null
	
   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
      ,p.MediaId
      ,p.ProductImageGroupId
   from @Product t            
       inner join product.tProductImage p on p.ProductId = t.ProductId
   
   select pi.ProductId 
		  ,lp.HyErpId AS ErpId
		  ,pi.MediaId
		  ,pi.ProductImageGroupId
		  ,mf.Extension
	 from @AllProductImages pi
	      inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	      inner join media.tMedia m on pi.MediaId = m.MediaId
		   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
   -- Icons				   
    SELECT [lpi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProductIcon] lpi ON [lpi].[ProductId] = [p].[ProductId]
	UNION
	SELECT [bi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[BrandId] = [lp].[BrandId]
	UNION
	SELECT [ci].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [product].[tProduct] p1 ON [p1].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](p1.CategoryId))
      
   -- Product translations (title & description)            
   select pt.ProductId
         ,lp.HyErpId AS ErpId
         ,pt.LanguageId
         ,pt.Title
         ,pt.[Description]
     from @product t 
         inner join product.tProductTranslation pt on pt.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on lp.ProductId = pt.ProductId
    where Title is not null or Description is not null
    
    -- Brands  
   select b.BrandId
         ,b.Title
         ,b.ExternalUrl
         ,b.MediaId
         ,mf.Extension
     from lekmer.tBrand b 
          inner join media.tMedia m on m.MediaId = b.MediaId
          inner join media.tMediaFormat mf on mf.MediaFormatId = m.MediaFormatId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pBrandIconSave]'
GO
CREATE PROCEDURE [lekmer].[pBrandIconSave]
	@BrandId	INT,
	@IconId		INT
AS 
BEGIN 
	INSERT [lekmer].[tBrandIcon]
	VALUES (@BrandId, @IconId)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProductIcon]'
GO
ALTER TABLE [lekmer].[tLekmerProductIcon] ADD CONSTRAINT [FK_tLekmerProductIcon_tIcon] FOREIGN KEY ([IconId]) REFERENCES [lekmer].[tIcon] ([IconId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCategoryIcon]'
GO
ALTER TABLE [lekmer].[tCategoryIcon] ADD CONSTRAINT [FK_tCategoryIcon_tIcon] FOREIGN KEY ([IconId]) REFERENCES [lekmer].[tIcon] ([IconId]) ON DELETE CASCADE
ALTER TABLE [lekmer].[tCategoryIcon] ADD CONSTRAINT [FK_tCategoryIcon_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tIconTranslation]'
GO
ALTER TABLE [lekmer].[tIconTranslation] ADD CONSTRAINT [FK_tIconTranslation_tIcon] FOREIGN KEY ([IconId]) REFERENCES [lekmer].[tIcon] ([IconId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBrandIcon]'
GO
ALTER TABLE [lekmer].[tBrandIcon] ADD CONSTRAINT [FK_tBrandIcon_tIcon] FOREIGN KEY ([IconId]) REFERENCES [lekmer].[tIcon] ([IconId]) ON DELETE CASCADE
ALTER TABLE [lekmer].[tBrandIcon] ADD CONSTRAINT [FK_tBrandIcon_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tIcon]'
GO
ALTER TABLE [lekmer].[tIcon] ADD CONSTRAINT [FK_tIcon_tImage] FOREIGN KEY ([MediaId]) REFERENCES [media].[tImage] ([MediaId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tBlockProductSearchEsalesResult]'
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] ADD CONSTRAINT [FK_tBlockProductSearchEsalesResult_tTemplate] FOREIGN KEY ([SecondaryTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO