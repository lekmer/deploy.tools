/*
Run this script on a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 004\DB\LekmerDB    -  This database will be modified. The scripts folder will not be modified.

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 005\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 03.04.2012 8:59:01

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [review]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCategoryTagGroup]'
GO
CREATE TABLE [lekmer].[tCategoryTagGroup]
(
[CategoryId] [int] NOT NULL,
[TagGroupId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCategoryTagGroup] on [lekmer].[tCategoryTagGroup]'
GO
ALTER TABLE [lekmer].[tCategoryTagGroup] ADD CONSTRAINT [PK_tCategoryTagGroup] PRIMARY KEY CLUSTERED  ([CategoryId], [TagGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tBlockBestRatedProductList]'
GO
CREATE TABLE [review].[tBlockBestRatedProductList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[NumberOfItems] [int] NOT NULL,
[CategoryId] [int] NULL,
[RatingId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockBestRatedProductList] on [review].[tBlockBestRatedProductList]'
GO
ALTER TABLE [review].[tBlockBestRatedProductList] ADD CONSTRAINT [PK_tBlockBestRatedProductList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRating]'
GO
CREATE TABLE [review].[tRating]
(
[RatingId] [int] NOT NULL IDENTITY(1, 1),
[RatingFolderId] [int] NOT NULL,
[RatingRegistryId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[CommonForVariants] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingInfo] on [review].[tRating]'
GO
ALTER TABLE [review].[tRating] ADD CONSTRAINT [PK_tRatingInfo] PRIMARY KEY CLUSTERED  ([RatingId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tRating_RatingRegistryId_CommonName] on [review].[tRating]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tRating_RatingRegistryId_CommonName] ON [review].[tRating] ([RatingRegistryId], [CommonName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tBlockBestRatedProductListBrand]'
GO
CREATE TABLE [review].[tBlockBestRatedProductListBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockBestRatedProductListBrand] on [review].[tBlockBestRatedProductListBrand]'
GO
ALTER TABLE [review].[tBlockBestRatedProductListBrand] ADD CONSTRAINT [PK_tBlockBestRatedProductListBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tBlockLatestFeedbackList]'
GO
CREATE TABLE [review].[tBlockLatestFeedbackList]
(
[BlockId] [int] NOT NULL,
[CategoryId] [int] NULL,
[NumberOfItems] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockLatestFeedbackList] on [review].[tBlockLatestFeedbackList]'
GO
ALTER TABLE [review].[tBlockLatestFeedbackList] ADD CONSTRAINT [PK_tBlockLatestFeedbackList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tBlockLatestFeedbackListBrand]'
GO
CREATE TABLE [review].[tBlockLatestFeedbackListBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockLatestFeedbackListBrand] on [review].[tBlockLatestFeedbackListBrand]'
GO
ALTER TABLE [review].[tBlockLatestFeedbackListBrand] ADD CONSTRAINT [PK_tBlockLatestFeedbackListBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tBlockProductFeedback]'
GO
CREATE TABLE [review].[tBlockProductFeedback]
(
[BlockId] [int] NOT NULL,
[AllowReview] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockProductFeedback] on [review].[tBlockProductFeedback]'
GO
ALTER TABLE [review].[tBlockProductFeedback] ADD CONSTRAINT [PK_tBlockProductFeedback] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tBlockRating]'
GO
CREATE TABLE [review].[tBlockRating]
(
[BlockId] [int] NOT NULL,
[RatingId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockRating] on [review].[tBlockRating]'
GO
ALTER TABLE [review].[tBlockRating] ADD CONSTRAINT [PK_tBlockRating] PRIMARY KEY CLUSTERED  ([BlockId], [RatingId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tBlockRatingGroup]'
GO
CREATE TABLE [review].[tBlockRatingGroup]
(
[BlockId] [int] NOT NULL,
[RatingGroupId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockRatingGroup] on [review].[tBlockRatingGroup]'
GO
ALTER TABLE [review].[tBlockRatingGroup] ADD CONSTRAINT [PK_tBlockRatingGroup] PRIMARY KEY CLUSTERED  ([BlockId], [RatingGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingGroup]'
GO
CREATE TABLE [review].[tRatingGroup]
(
[RatingGroupId] [int] NOT NULL IDENTITY(1, 1),
[RatingGroupFolderId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingGroup] on [review].[tRatingGroup]'
GO
ALTER TABLE [review].[tRatingGroup] ADD CONSTRAINT [PK_tRatingGroup] PRIMARY KEY CLUSTERED  ([RatingGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tRatingGroup_CommonName] on [review].[tRatingGroup]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tRatingGroup_CommonName] ON [review].[tRatingGroup] ([CommonName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tBlockRatingItem]'
GO
CREATE TABLE [review].[tBlockRatingItem]
(
[BlockId] [int] NOT NULL,
[RatingItemId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockRatingItem] on [review].[tBlockRatingItem]'
GO
ALTER TABLE [review].[tBlockRatingItem] ADD CONSTRAINT [PK_tBlockRatingItem] PRIMARY KEY CLUSTERED  ([BlockId], [RatingItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingItem]'
GO
CREATE TABLE [review].[tRatingItem]
(
[RatingItemId] [int] NOT NULL IDENTITY(1, 1),
[RatingId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[Score] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingItem] on [review].[tRatingItem]'
GO
ALTER TABLE [review].[tRatingItem] ADD CONSTRAINT [PK_tRatingItem] PRIMARY KEY CLUSTERED  ([RatingItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingFolder]'
GO
CREATE TABLE [review].[tRatingFolder]
(
[RatingFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentRatingFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingFolder] on [review].[tRatingFolder]'
GO
ALTER TABLE [review].[tRatingFolder] ADD CONSTRAINT [PK_tRatingFolder] PRIMARY KEY CLUSTERED  ([RatingFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingRegistry]'
GO
CREATE TABLE [review].[tRatingRegistry]
(
[RatingRegistryId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingRegistry] on [review].[tRatingRegistry]'
GO
ALTER TABLE [review].[tRatingRegistry] ADD CONSTRAINT [PK_tRatingRegistry] PRIMARY KEY CLUSTERED  ([RatingRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tRatingRegistry_CommonName] on [review].[tRatingRegistry]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tRatingRegistry_CommonName] ON [review].[tRatingRegistry] ([CommonName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingGroupFolder]'
GO
CREATE TABLE [review].[tRatingGroupFolder]
(
[RatingGroupFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentRatingGroupFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingGroupFolder] on [review].[tRatingGroupFolder]'
GO
ALTER TABLE [review].[tRatingGroupFolder] ADD CONSTRAINT [PK_tRatingGroupFolder] PRIMARY KEY CLUSTERED  ([RatingGroupFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingGroupCategory]'
GO
CREATE TABLE [review].[tRatingGroupCategory]
(
[RatingGroupId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubCategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingGroupCategory] on [review].[tRatingGroupCategory]'
GO
ALTER TABLE [review].[tRatingGroupCategory] ADD CONSTRAINT [PK_tRatingGroupCategory] PRIMARY KEY CLUSTERED  ([RatingGroupId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingGroupProduct]'
GO
CREATE TABLE [review].[tRatingGroupProduct]
(
[RatingGroupId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingGroupProduct] on [review].[tRatingGroupProduct]'
GO
ALTER TABLE [review].[tRatingGroupProduct] ADD CONSTRAINT [PK_tRatingGroupProduct] PRIMARY KEY CLUSTERED  ([RatingGroupId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingGroupRating]'
GO
CREATE TABLE [review].[tRatingGroupRating]
(
[RatingGroupId] [int] NOT NULL,
[RatingId] [int] NOT NULL,
[Ordinal] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingGroupRating] on [review].[tRatingGroupRating]'
GO
ALTER TABLE [review].[tRatingGroupRating] ADD CONSTRAINT [PK_tRatingGroupRating] PRIMARY KEY CLUSTERED  ([RatingGroupId], [RatingId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingItemProductScore]'
GO
CREATE TABLE [review].[tRatingItemProductScore]
(
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[RatingId] [int] NOT NULL,
[RatingItemId] [int] NOT NULL,
[HitCount] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingItemProductScore] on [review].[tRatingItemProductScore]'
GO
ALTER TABLE [review].[tRatingItemProductScore] ADD CONSTRAINT [PK_tRatingItemProductScore] PRIMARY KEY CLUSTERED  ([ChannelId], [ProductId], [RatingId], [RatingItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingItemProductVote]'
GO
CREATE TABLE [review].[tRatingItemProductVote]
(
[RatingReviewFeedbackId] [int] NOT NULL,
[RatingId] [int] NOT NULL,
[RatingItemId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingItemProductVote] on [review].[tRatingItemProductVote]'
GO
ALTER TABLE [review].[tRatingItemProductVote] ADD CONSTRAINT [PK_tRatingItemProductVote] PRIMARY KEY CLUSTERED  ([RatingReviewFeedbackId], [RatingId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingReviewFeedback]'
GO
CREATE TABLE [review].[tRatingReviewFeedback]
(
[RatingReviewFeedbackId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[OrderId] [int] NULL,
[RatingReviewStatusId] [int] NOT NULL,
[LikeHit] [int] NOT NULL,
[Impropriate] [bit] NULL,
[IPAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[RatingReviewUserId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingReviewFeedback] on [review].[tRatingReviewFeedback]'
GO
ALTER TABLE [review].[tRatingReviewFeedback] ADD CONSTRAINT [PK_tRatingReviewFeedback] PRIMARY KEY CLUSTERED  ([RatingReviewFeedbackId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingItemTranslation]'
GO
CREATE TABLE [review].[tRatingItemTranslation]
(
[RatingItemId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingItemTranslation] on [review].[tRatingItemTranslation]'
GO
ALTER TABLE [review].[tRatingItemTranslation] ADD CONSTRAINT [PK_tRatingItemTranslation] PRIMARY KEY CLUSTERED  ([RatingItemId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingModuleChannel]'
GO
CREATE TABLE [review].[tRatingModuleChannel]
(
[ChannelId] [int] NOT NULL,
[RatingRegistryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingModuleChannel] on [review].[tRatingModuleChannel]'
GO
ALTER TABLE [review].[tRatingModuleChannel] ADD CONSTRAINT [PK_tRatingModuleChannel] PRIMARY KEY CLUSTERED  ([ChannelId], [RatingRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingReviewStatus]'
GO
CREATE TABLE [review].[tRatingReviewStatus]
(
[RatingReviewStatusId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingReviewStatus] on [review].[tRatingReviewStatus]'
GO
ALTER TABLE [review].[tRatingReviewStatus] ADD CONSTRAINT [PK_tRatingReviewStatus] PRIMARY KEY CLUSTERED  ([RatingReviewStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingReviewUser]'
GO
CREATE TABLE [review].[tRatingReviewUser]
(
[RatingReviewUserId] [int] NOT NULL IDENTITY(1, 1),
[CustomerId] [int] NULL,
[Token] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingReviewUser] on [review].[tRatingReviewUser]'
GO
ALTER TABLE [review].[tRatingReviewUser] ADD CONSTRAINT [PK_tRatingReviewUser] PRIMARY KEY CLUSTERED  ([RatingReviewUserId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingReviewFeedbackLike]'
GO
CREATE TABLE [review].[tRatingReviewFeedbackLike]
(
[RatingReviewFeedbackLikeId] [int] NOT NULL,
[RatingReviewFeedbackId] [int] NOT NULL,
[IPAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[RatingReviewUserId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingReviewFeedbackLike] on [review].[tRatingReviewFeedbackLike]'
GO
ALTER TABLE [review].[tRatingReviewFeedbackLike] ADD CONSTRAINT [PK_tRatingReviewFeedbackLike] PRIMARY KEY CLUSTERED  ([RatingReviewFeedbackLikeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tRatingTranslation]'
GO
CREATE TABLE [review].[tRatingTranslation]
(
[RatingId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tRatingInfoTranslation] on [review].[tRatingTranslation]'
GO
ALTER TABLE [review].[tRatingTranslation] ADD CONSTRAINT [PK_tRatingInfoTranslation] PRIMARY KEY CLUSTERED  ([RatingId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[tReview]'
GO
CREATE TABLE [review].[tReview]
(
[ReviewId] [int] NOT NULL IDENTITY(1, 1),
[RatingReviewFeedbackId] [int] NOT NULL,
[AuthorName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [nvarchar] (3000) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tReview_1] on [review].[tReview]'
GO
ALTER TABLE [review].[tReview] ADD CONSTRAINT [PK_tReview_1] PRIMARY KEY CLUSTERED  ([ReviewId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCategoryTagGroupDeleteAll]'
GO
CREATE PROCEDURE [lekmer].[pCategoryTagGroupDeleteAll]
	@CategoryId	INT
AS
BEGIN 
	DELETE FROM [lekmer].[tCategoryTagGroup]
	WHERE CategoryId = @CategoryId
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCategoryTagGroup]'
GO
CREATE VIEW [lekmer].[vCategoryTagGroup]
AS
	SELECT
		CategoryId 'CategoryTagGroup.CategoryId',
		TagGroupId 'CategoryTagGroup.TagGroupId'
	FROM
		[lekmer].[tCategoryTagGroup]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCategoryTagGroupGetAllByCategory]'
GO
CREATE PROCEDURE [lekmer].[pCategoryTagGroupGetAllByCategory]
	@CategoryId INT
AS
BEGIN
	SELECT * FROM [lekmer].[vCategoryTagGroup] ctg
	WHERE ctg.[CategoryTagGroup.CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCategoryTagGroupSave]'
GO
CREATE PROCEDURE [lekmer].[pCategoryTagGroupSave]
	@CategoryId	INT,
	@TagGroupId	INT
AS
BEGIN 
	INSERT INTO [lekmer].[tCategoryTagGroup] (
		CategoryId,
		TagGroupId
	)
	VALUES (
		@CategoryId,
		@TagGroupId
	)
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pLekmerOrderSetFeedbackToken]'
GO

CREATE PROCEDURE [lekmer].[pLekmerOrderSetFeedbackToken]
	@OrderId INT,
	@FeedbackToken UNIQUEIDENTIFIER
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		FeedbackToken = @FeedbackToken
	WHERE 
		OrderId = @OrderId
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductVariantGetIdAllByProduct]'
GO
CREATE PROCEDURE [lekmer].[pProductVariantGetIdAllByProduct]
	@ProductId		INT,
	@RelationListTypeId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		DISTINCT rlp.[ProductId]
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.[RelationListId] = rl.[RelationListId]
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.[RelationListId] = rlp.[RelationListId]
	WHERE
		prl.[ProductId] = @ProductId
		AND rlp.[ProductId] <> @ProductId
		AND rl.[RelationListTypeId] = @RelationListTypeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockBestRatedProductListBrandDeleteAll]'
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListBrandDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockBestRatedProductListBrand]
	WHERE  [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockBestRatedProductListBrandSave]'
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListBrandSave]
	@BlockId INT,
	@BrandId INT
AS
BEGIN
	INSERT [review].[tBlockBestRatedProductListBrand] (
		[BlockId],
		[BrandId]
	)
	VALUES (
		@BlockId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockBestRatedProductListDelete]'
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListDelete]
	@BlockId INT
AS
BEGIN
	EXEC [review].[pBlockBestRatedProductListBrandDeleteAll] @BlockId

	DELETE [review].[tBlockBestRatedProductList]
	WHERE BlockId = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockBestRatedProductList]'
GO


CREATE VIEW [review].[vBlockBestRatedProductList]
AS
	SELECT
		[BlockId] AS 'BlockBestRatedProductList.BlockId',
		[ColumnCount] AS 'BlockBestRatedProductList.ColumnCount',
		[RowCount] AS 'BlockBestRatedProductList.RowCount',
		[NumberOfItems] AS 'BlockBestRatedProductList.NumberOfItems',
		[CategoryId] AS 'BlockBestRatedProductList.CategoryId',
		[RatingId] AS 'BlockBestRatedProductList.RatingId'
	FROM
		[review].[tBlockBestRatedProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockBestRatedProductListSave]'
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@NumberOfItems INT,
	@CategoryId INT,
	@RatingId INT
AS
BEGIN
	UPDATE [review].[tBlockBestRatedProductList]
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount,
		[NumberOfItems] = @NumberOfItems,
		[CategoryId] = @CategoryId,
		[RatingId] = @RatingId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockBestRatedProductList] (
		[BlockId],
		[ColumnCount],
		[RowCount],
		[NumberOfItems],
		[CategoryId],
		[RatingId]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@NumberOfItems,
		@CategoryId,
		@RatingId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockLatestFeedbackListBrandDeleteAll]'
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListBrandDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockLatestFeedbackListBrand]
	WHERE  [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockLatestFeedbackListBrandSave]'
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListBrandSave]
	@BlockId INT,
	@BrandId INT
AS
BEGIN
	INSERT [review].[tBlockLatestFeedbackListBrand] (
		[BlockId],
		[BrandId]
	)
	VALUES (
		@BlockId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockLatestFeedbackListDelete]'
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListDelete]
	@BlockId INT
AS
BEGIN
	EXEC [review].[pBlockLatestFeedbackListBrandDeleteAll] @BlockId

	DELETE [review].[tBlockLatestFeedbackList]
	WHERE BlockId = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockLatestFeedbackList]'
GO

CREATE VIEW [review].[vBlockLatestFeedbackList]
AS
	SELECT
		[BlockId] AS 'BlockLatestFeedbackList.BlockId',
		[CategoryId] AS 'BlockLatestFeedbackList.CategoryId',
		[NumberOfItems] AS 'BlockLatestFeedbackList.NumberOfItems'
	FROM
		[review].[tBlockLatestFeedbackList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockLatestFeedbackListSave]'
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListSave]
	@BlockId INT,
	@CategoryId INT,
	@NumberOfItems INT
AS
BEGIN
	UPDATE [review].[tBlockLatestFeedbackList]
	SET
		[CategoryId] = @CategoryId,
		[NumberOfItems] = @NumberOfItems
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockLatestFeedbackList] (
		[BlockId],
		[CategoryId],
		[NumberOfItems]
	)
	VALUES (
		@BlockId,
		@CategoryId,
		@NumberOfItems
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockProductFeedbackDelete]'
GO
CREATE PROCEDURE [review].[pBlockProductFeedbackDelete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockProductFeedback]
	WHERE BlockId = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockProductFeedback]'
GO

CREATE VIEW [review].[vBlockProductFeedback]
AS
	SELECT
		[BlockId] AS 'BlockProductFeedback.BlockId',
		[AllowReview] AS 'BlockProductFeedback.AllowReview'
	FROM
		[review].[tBlockProductFeedback]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockProductFeedbackSave]'
GO

CREATE PROCEDURE [review].[pBlockProductFeedbackSave]
	@BlockId INT,
	@AllowReview BIT
AS
BEGIN
	UPDATE [review].[tBlockProductFeedback]
	SET
		[AllowReview] = @AllowReview
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockProductFeedback] (
		[BlockId],
		[AllowReview]
	)
	VALUES (
		@BlockId,
		@AllowReview
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockRatingGroupDelete]'
GO

CREATE PROCEDURE [review].[pBlockRatingGroupDelete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockRatingGroup]
	WHERE BlockId = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockRatingGroup]'
GO

CREATE VIEW [review].[vBlockRatingGroup]
AS
	SELECT
		[BlockId] AS 'BlockRatingGroup.BlockId',
		[RatingGroupId] AS 'BlockRatingGroup.RatingGroupId'
	FROM
		[review].[tBlockRatingGroup]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockRatingGroupGetAllByBlock]'
GO

CREATE PROCEDURE [review].[pBlockRatingGroupGetAllByBlock]
	@BlockId	INT
AS
BEGIN
	SELECT 
		brg.*
	FROM 
		[review].[vBlockRatingGroup] AS brg
	WHERE
		brg.[BlockRatingGroup.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockRatingGroupSave]'
GO

CREATE PROCEDURE [review].[pBlockRatingGroupSave]
	@BlockId INT,
	@RatingGroupId INT
AS
BEGIN
	INSERT [review].[tBlockRatingGroup] (
		[BlockId],
		[RatingGroupId]
	)
	VALUES (
		@BlockId,
		@RatingGroupId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockRatingItemDelete]'
GO

CREATE PROCEDURE [review].[pBlockRatingItemDelete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockRatingItem]
	WHERE BlockId = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockRatingItem]'
GO


CREATE VIEW [review].[vBlockRatingItem]
AS
	SELECT
		[BlockId] AS 'BlockRatingItem.BlockId',
		[RatingItemId] AS 'BlockRatingItem.RatingItemId'
	FROM
		[review].[tBlockRatingItem]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockRatingItemGetAllByBlock]'
GO

CREATE PROCEDURE [review].[pBlockRatingItemGetAllByBlock]
	@BlockId	INT
AS
BEGIN
	SELECT 
		bri.*
	FROM 
		[review].[vBlockRatingItem] AS bri
	WHERE
		bri.[BlockRatingItem.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockRatingItemSave]'
GO

CREATE PROCEDURE [review].[pBlockRatingItemSave]
	@BlockId INT,
	@RatingItemId INT
AS
BEGIN
	INSERT [review].[tBlockRatingItem] (
		[BlockId],
		[RatingItemId]
	)
	VALUES (
		@BlockId,
		@RatingItemId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockRating_Delete]'
GO

CREATE PROCEDURE [review].[pBlockRating_Delete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockRating]
	WHERE BlockId = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockRating]'
GO

CREATE VIEW [review].[vBlockRating]
AS
	SELECT
		[BlockId] AS 'BlockRating.BlockId',
		[RatingId] AS 'BlockRating.RatingId'
	FROM
		[review].[tBlockRating]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockRating_GetAllByBlock]'
GO

CREATE PROCEDURE [review].[pBlockRating_GetAllByBlock]
	@BlockId	INT
AS
BEGIN
	SELECT 
		br.*
	FROM 
		[review].[vBlockRating] AS br
	WHERE
		br.[BlockRating.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockRating_Save]'
GO

CREATE PROCEDURE [review].[pBlockRating_Save]
	@BlockId INT,
	@RatingId INT
AS
BEGIN
	INSERT [review].[tBlockRating] (
		[BlockId],
		[RatingId]
	)
	VALUES (
		@BlockId,
		@RatingId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingFolderDelete]'
GO

CREATE PROCEDURE [review].[pRatingFolderDelete]
	@RatingFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [review].[tRatingFolder]
	WHERE [RatingFolderId] = @RatingFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingFolder]'
GO

CREATE VIEW [review].[vRatingFolder]
AS
	SELECT
		rf.[RatingFolderId]			'RatingFolder.RatingFolderId',
		rf.[ParentRatingFolderId]	'RatingFolder.ParentRatingFolderId',
		rf.[Title]					'RatingFolder.Title'
	FROM 
		[review].[tRatingFolder] rf
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingFolderGetAll]'
GO
CREATE PROCEDURE [review].[pRatingFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rf.*
	FROM
		[review].[vRatingFolder] rf
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingFolderGetAllByParent]'
GO
CREATE PROCEDURE [review].[pRatingFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SELECT
		rf.*
	FROM
		[review].[vRatingFolder] rf
	WHERE
		(rf.[RatingFolder.ParentRatingFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		(rf.[RatingFolder.ParentRatingFolderId] IS NULL AND @ParentId IS NULL)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingFolderGetById]'
GO
CREATE PROCEDURE [review].[pRatingFolderGetById]
	@RatingFolderId	INT
AS
BEGIN
	SELECT
		rf.*
	FROM
		[review].[vRatingFolder] rf
	WHERE
		rf.[RatingFolder.RatingFolderId] = @RatingFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingFolderGetTree]'
GO
CREATE PROCEDURE [review].[pRatingFolderGetTree]
	@SelectedId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tTree TABLE (Id INT, ParentId INT, Title NVARCHAR(50))

	INSERT INTO @tTree
	SELECT rf.*
	FROM [review].[vRatingFolder] AS rf
	WHERE rf.[RatingFolder.ParentRatingFolderId] IS NULL

	IF (@SelectedId IS NOT NULL)
	BEGIN
		INSERT INTO @tTree
		SELECT rf.*
		FROM [review].[vRatingFolder] AS rf
		WHERE rf.[RatingFolder.ParentRatingFolderId] = @SelectedId

		DECLARE @ParentId INT
		WHILE (@SelectedId IS NOT NULL)
		BEGIN
			SET @ParentId = (SELECT rf.[RatingFolder.ParentRatingFolderId]
							 FROM [review].[vRatingFolder] AS rf
							 WHERE rf.[RatingFolder.RatingFolderId] = @SelectedId)
							 
			INSERT INTO @tTree
			SELECT rf.*
			FROM [review].[vRatingFolder] AS rf
			WHERE rf.[RatingFolder.ParentRatingFolderId] = @ParentId
			
			SET @SelectedId = @ParentId
		END
	END
		
	SELECT
		Id,
		ParentId,
		Title,
		CAST((CASE WHEN EXISTS (SELECT 1 FROM [review].[vRatingFolder] AS rf
								WHERE rf.[RatingFolder.ParentRatingFolderId] = Id)
		THEN 1 ELSE 0 END) AS BIT) AS 'HasChildren'
	FROM
		@tTree
	ORDER BY
		ParentId ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingFolderSave]'
GO

CREATE PROCEDURE [review].[pRatingFolderSave]
	@RatingFolderId INT,
	@ParentRatingFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [review].[tRatingFolder]
		WHERE [Title] = @Title
			AND [RatingFolderId] <> @RatingFolderId
			AND	((@ParentRatingFolderId IS NULL AND [ParentRatingFolderId] IS NULL)
				OR [ParentRatingFolderId] = @ParentRatingFolderId)
	)
	RETURN -1
	
	UPDATE 
		[review].[tRatingFolder]
	SET 
		[ParentRatingFolderId] = @ParentRatingFolderId,
		[Title] = @Title
	WHERE
		[RatingFolderId] = @RatingFolderId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingFolder] (
			[ParentRatingFolderId],
			[Title]
		)
		VALUES (
			@ParentRatingFolderId,
			@Title
		)

		SET @RatingFolderId = SCOPE_IDENTITY()
	END

	RETURN @RatingFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupCategoryDelete]'
GO

CREATE PROCEDURE [review].[pRatingGroupCategoryDelete]
	@RatingGroupId	INT,
	@CategoryId		INT
AS
BEGIN
	SET NOCOUNT ON

	IF (@RatingGroupId IS NULL AND @CategoryId IS NULL)
	RETURN

	DELETE FROM [review].[tRatingGroupCategory]
	WHERE
		([RatingGroupId] = @RatingGroupId OR @RatingGroupId IS NULL)
		AND
		([CategoryId] = @CategoryId OR @CategoryId IS NULL)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingGroupCategory]'
GO

CREATE VIEW [review].[vRatingGroupCategory]
AS
	SELECT
		rgc.[RatingGroupId]			'RatingGroupCategory.RatingGroupId',
		rgc.[CategoryId]			'RatingGroupCategory.CategoryId',
		rgc.[IncludeSubCategories]	'RatingGroupCategory.IncludeSubCategories'
	FROM 
		[review].[tRatingGroupCategory] rgc
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupCategoryGetAllByCategory]'
GO
CREATE PROCEDURE [review].[pRatingGroupCategoryGetAllByCategory]
	@CategoryId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgc.*
	FROM
		[review].[vRatingGroupCategory] rgc
	WHERE
		rgc.[RatingGroupCategory.CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupCategoryGetAllByGroup]'
GO
CREATE PROCEDURE [review].[pRatingGroupCategoryGetAllByGroup]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgc.*
	FROM
		[review].[vRatingGroupCategory] rgc
	WHERE
		rgc.[RatingGroupCategory.RatingGroupId] = @RatingGroupId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupCategorySave]'
GO

CREATE PROCEDURE [review].[pRatingGroupCategorySave]
	@RatingGroupId INT,
	@CategoryId INT,
	@IncludeSubCategories BIT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[review].[tRatingGroupCategory]
	SET 
		[IncludeSubCategories] = @IncludeSubCategories
	WHERE
		[RatingGroupId] = @RatingGroupId
		AND
		[CategoryId] = @CategoryId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingGroupCategory] (
			[RatingGroupId],
			[CategoryId],
			[IncludeSubCategories]
		)
		VALUES (
			@RatingGroupId,
			@CategoryId,
			@IncludeSubCategories
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupFolderDelete]'
GO

CREATE PROCEDURE [review].[pRatingGroupFolderDelete]
	@RatingGroupFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [review].[tRatingGroupFolder]
	WHERE [RatingGroupFolderId] = @RatingGroupFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingGroupFolder]'
GO


CREATE VIEW [review].[vRatingGroupFolder]
AS
	SELECT
		rgf.[RatingGroupFolderId]		'RatingGroupFolder.RatingGroupFolderId',
		rgf.[ParentRatingGroupFolderId]	'RatingGroupFolder.ParentRatingGroupFolderId',
		rgf.[Title]						'RatingGroupFolder.Title'
	FROM 
		[review].[tRatingGroupFolder] rgf

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupFolderGetAll]'
GO
CREATE PROCEDURE [review].[pRatingGroupFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgf.*
	FROM
		[review].[vRatingGroupFolder] rgf
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupFolderGetAllByParent]'
GO
CREATE PROCEDURE [review].[pRatingGroupFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SELECT
		rgf.*
	FROM
		[review].[vRatingGroupFolder] rgf
	WHERE
		(rgf.[RatingGroupFolder.ParentRatingGroupFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		(rgf.[RatingGroupFolder.ParentRatingGroupFolderId] IS NULL AND @ParentId IS NULL)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupFolderGetById]'
GO
CREATE PROCEDURE [review].[pRatingGroupFolderGetById]
	@RatingGroupFolderId	INT
AS
BEGIN
	SELECT
		rgf.*
	FROM
		[review].[vRatingGroupFolder] rgf
	WHERE
		rgf.[RatingGroupFolder.RatingGroupFolderId] = @RatingGroupFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupFolderGetTree]'
GO
CREATE PROCEDURE [review].[pRatingGroupFolderGetTree]
	@SelectedId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tTree TABLE (Id INT, ParentId INT, Title NVARCHAR(50))

	INSERT INTO @tTree
	SELECT rgf.*
	FROM [review].[vRatingGroupFolder] AS rgf
	WHERE rgf.[RatingGroupFolder.ParentRatingGroupFolderId] IS NULL

	IF (@SelectedId IS NOT NULL)
	BEGIN
		INSERT INTO @tTree
		SELECT rgf.*
		FROM [review].[vRatingGroupFolder] AS rgf
		WHERE rgf.[RatingGroupFolder.ParentRatingGroupFolderId] = @SelectedId

		DECLARE @ParentId INT
		WHILE (@SelectedId IS NOT NULL)
		BEGIN
			SET @ParentId = (SELECT rgf.[RatingGroupFolder.ParentRatingGroupFolderId]
							 FROM [review].[vRatingGroupFolder] AS rgf
							 WHERE rgf.[RatingGroupFolder.RatingGroupFolderId] = @SelectedId)
							 
			INSERT INTO @tTree
			SELECT rgf.*
			FROM [review].[vRatingGroupFolder] AS rgf
			WHERE rgf.[RatingGroupFolder.ParentRatingGroupFolderId] = @ParentId
			
			SET @SelectedId = @ParentId
		END
	END
		
	SELECT
		Id,
		ParentId,
		Title,
		CAST((CASE WHEN EXISTS (SELECT 1 FROM [review].[vRatingGroupFolder] AS rgf
								WHERE rgf.[RatingGroupFolder.ParentRatingGroupFolderId] = Id)
		THEN 1 ELSE 0 END) AS BIT) AS 'HasChildren'
	FROM
		@tTree
	ORDER BY
		ParentId ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupFolderSave]'
GO

CREATE PROCEDURE [review].[pRatingGroupFolderSave]
	@RatingGroupFolderId INT,
	@ParentRatingGroupFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [review].[tRatingGroupFolder]
		WHERE [Title] = @Title
			AND [RatingGroupFolderId] <> @RatingGroupFolderId
			AND	((@ParentRatingGroupFolderId IS NULL AND [ParentRatingGroupFolderId] IS NULL)
				OR [ParentRatingGroupFolderId] = @ParentRatingGroupFolderId)
	)
	RETURN -1
	
	UPDATE 
		[review].[tRatingGroupFolder]
	SET 
		[ParentRatingGroupFolderId] = @ParentRatingGroupFolderId,
		[Title] = @Title
	WHERE
		[RatingGroupFolderId] = @RatingGroupFolderId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingGroupFolder] (
			[ParentRatingGroupFolderId],
			[Title]
		)
		VALUES (
			@ParentRatingGroupFolderId,
			@Title
		)

		SET @RatingGroupFolderId = SCOPE_IDENTITY()
	END

	RETURN @RatingGroupFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupProductDelete]'
GO

CREATE PROCEDURE [review].[pRatingGroupProductDelete]
	@RatingGroupId	INT,
	@ProductId		INT
AS
BEGIN
	SET NOCOUNT ON

	IF (@RatingGroupId IS NULL AND @ProductId IS NULL)
	RETURN

	DELETE FROM [review].[tRatingGroupProduct]
	WHERE
		([RatingGroupId] = @RatingGroupId OR @RatingGroupId IS NULL)
		AND
		([ProductId] = @ProductId OR @ProductId IS NULL)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingGroupProduct]'
GO

CREATE VIEW [review].[vRatingGroupProduct]
AS
	SELECT
		rgp.[RatingGroupId]	'RatingGroupProduct.RatingGroupId',
		rgp.[ProductId]		'RatingGroupProduct.ProductId'
	FROM 
		[review].[tRatingGroupProduct] rgp
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupProductGetAllByGroup]'
GO
CREATE PROCEDURE [review].[pRatingGroupProductGetAllByGroup]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgp.*
	FROM
		[review].[vRatingGroupProduct] rgp
	WHERE
		rgp.[RatingGroupProduct.RatingGroupId] = @RatingGroupId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupProductGetAllByProduct]'
GO
CREATE PROCEDURE [review].[pRatingGroupProductGetAllByProduct]
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgp.*
	FROM
		[review].[vRatingGroupProduct] rgp
	WHERE
		rgp.[RatingGroupProduct.ProductId] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupProductSave]'
GO

CREATE PROCEDURE [review].[pRatingGroupProductSave]
	@RatingGroupId INT,
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tRatingGroupProduct] (
		[RatingGroupId],
		[ProductId]
	)
	VALUES (
		@RatingGroupId,
		@ProductId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupRatingDelete]'
GO

CREATE PROCEDURE [review].[pRatingGroupRatingDelete]
	@RatingGroupId	INT,
	@RatingId		INT
AS
BEGIN
	SET NOCOUNT ON

	IF (@RatingGroupId IS NULL AND @RatingId IS NULL)
	RETURN

	DELETE FROM [review].[tRatingGroupRating]
	WHERE
		([RatingGroupId] = @RatingGroupId OR @RatingGroupId IS NULL)
		AND
		([RatingId] = @RatingId OR @RatingId IS NULL)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingGroupRating]'
GO


CREATE VIEW [review].[vRatingGroupRating]
AS
	SELECT
		rgr.[RatingGroupId]	'RatingGroupRating.RatingGroupId',
		rgr.[RatingId]		'RatingGroupRating.RatingId',
		rgr.[Ordinal]		'RatingGroupRating.Ordinal'
	FROM 
		[review].[tRatingGroupRating] rgr

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupRatingGetAllByGroup]'
GO
CREATE PROCEDURE [review].[pRatingGroupRatingGetAllByGroup]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgr.*
	FROM
		[review].[vRatingGroupRating] rgr
	WHERE
		rgr.[RatingGroupRating.RatingGroupId] = @RatingGroupId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupRatingGetAllByRating]'
GO
CREATE PROCEDURE [review].[pRatingGroupRatingGetAllByRating]
	@RatingId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgr.*
	FROM
		[review].[vRatingGroupRating] rgr
	WHERE
		rgr.[RatingGroupRating.RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroupRatingSave]'
GO

CREATE PROCEDURE [review].[pRatingGroupRatingSave]
	@RatingGroupId INT,
	@RatingId INT,
	@Ordinal INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tRatingGroupRating] (
		[RatingGroupId],
		[RatingId],
		[Ordinal]
	)
	VALUES (
		@RatingGroupId,
		@RatingId,
		@Ordinal
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroup_Delete]'
GO

CREATE PROCEDURE [review].[pRatingGroup_Delete]
	@RatingGroupId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [review].[pRatingGroupCategoryDelete] @RatingGroupId, NULL
	EXEC [review].[pRatingGroupProductDelete] @RatingGroupId, NULL
	EXEC [review].[pRatingGroupRatingDelete] NULL, @RatingGroupId
	
	DELETE FROM [review].[tRatingGroup]
	WHERE [RatingGroupId] = @RatingGroupId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingGroup]'
GO

CREATE VIEW [review].[vRatingGroup]
AS
	SELECT
		rg.[RatingGroupId]			'RatingGroup.RatingGroupId',
		rg.[RatingGroupFolderId]	'RatingGroup.RatingGroupFolderId',
		rg.[CommonName]				'RatingGroup.CommonName',
		rg.[Title]					'RatingGroup.Title',
		rg.[Description]			'RatingGroup.Description'
	FROM 
		[review].[tRatingGroup] rg
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroup_GetAllByFolder]'
GO
CREATE PROCEDURE [review].[pRatingGroup_GetAllByFolder]
	@RatingGroupFolderId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rg.*
	FROM
		[review].[vRatingGroup] rg
	WHERE
		rg.[RatingGroup.RatingGroupFolderId] = @RatingGroupFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroup_GetById]'
GO
CREATE PROCEDURE [review].[pRatingGroup_GetById]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rg.*
	FROM
		[review].[vRatingGroup] rg
	WHERE
		rg.[RatingGroup.RatingGroupId] = @RatingGroupId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroup_Save]'
GO

CREATE PROCEDURE [review].[pRatingGroup_Save]
	@RatingGroupId INT,
	@RatingGroupFolderId INT,
	@CommonName VARCHAR(50),
	@Title NVARCHAR(50),
	@Description NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [review].[tRatingGroup]
		WHERE [Title] = @Title
			AND [RatingGroupId] <> @RatingGroupId
			AND [RatingGroupFolderId] = @RatingGroupFolderId
	)
	RETURN -1
	
	-- Same common names are not allowed (return -2)
	IF EXISTS
	(
		SELECT 1
		FROM [review].[tRatingGroup]
		WHERE [CommonName] = @CommonName AND [RatingGroupId] <> @RatingGroupId
	)
	RETURN -2
	
	UPDATE 
		[review].[tRatingGroup]
	SET 
		[RatingGroupFolderId] = @RatingGroupFolderId,
		[CommonName] = @CommonName,
		[Title] = @Title,
		[Description] = @Description		
	WHERE
		[RatingGroupId] = @RatingGroupId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingGroup] (
			[RatingGroupFolderId],
			[CommonName],
			[Title],
			[Description]
		)
		VALUES (
			@RatingGroupFolderId,
			@CommonName,
			@Title,
			@Description
		)

		SET @RatingGroupId = SCOPE_IDENTITY()
	END

	RETURN @RatingGroupId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingGroup_Search]'
GO
CREATE PROCEDURE [review].[pRatingGroup_Search]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		rg.*
	FROM 
		[review].[vRatingGroup] rg 
	WHERE
		rg.[RatingGroup.Title] LIKE @SearchCriteria ESCAPE '\'
		OR rg.[RatingGroup.CommonName] LIKE @SearchCriteria ESCAPE '\'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemTranslationDelete]'
GO

CREATE PROCEDURE [review].[pRatingItemTranslationDelete]
	@RatingItemId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [review].[tRatingItemTranslation]
	WHERE [RatingItemId] = @RatingItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemTranslationDeleteAll]'
GO

CREATE PROCEDURE [review].[pRatingItemTranslationDeleteAll]
	@RatingId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE rit
	FROM 
		[review].[tRatingItemTranslation] rit
		INNER JOIN [review].[tRatingItem] ri ON ri.[RatingItemId] = rit.[RatingItemId]
	WHERE 
		ri.[RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingItem]'
GO

CREATE VIEW [review].[vRatingItem]
AS
	SELECT
		ri.[RatingItemId]	'RatingItem.RatingItemId',
		ri.[RatingId]		'RatingItem.RatingId',
		COALESCE(rit.[Title], ri.[Title])	'RatingItem.Title',
		COALESCE(rit.[Description], ri.[Description])	'RatingItem.Description',
		ri.[Score]			'RatingItem.Score',
		c.[ChannelId]		'RatingItem.Channel.Id'
	FROM
		[review].[tRatingItem] ri
		INNER JOIN [review].[tRating] r ON r.[RatingId] = ri.[RatingId]

		INNER JOIN [review].[tRatingModuleChannel] rmc ON rmc.[RatingRegistryId] = r.[RatingRegistryId]
		INNER JOIN [core].[tChannel] c ON c.[ChannelId] = rmc.[ChannelId]

		LEFT OUTER JOIN [review].[tRatingItemTranslation] rit ON rit.[RatingItemId] = ri.[RatingItemId] AND rit.[LanguageId] = c.[LanguageId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemGetAllByRating]'
GO

CREATE PROCEDURE [review].[pRatingItemGetAllByRating]
	@RatingId INT,
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		ri.*
	FROM
		[review].[vRatingItem] ri
	WHERE
		ri.[RatingItem.RatingId] = @RatingId
		AND
		ri.[RatingItem.Channel.Id] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingItemSecure]'
GO

CREATE VIEW [review].[vRatingItemSecure]
AS
	SELECT
		ri.[RatingItemId]	'RatingItem.RatingItemId',
		ri.[RatingId]		'RatingItem.RatingId',
		ri.[Title]			'RatingItem.Title',
		ri.[Description]	'RatingItem.Description',
		ri.[Score]			'RatingItem.Score'
	FROM 
		[review].[tRatingItem] ri
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemGetAllByRatingSecure]'
GO
CREATE PROCEDURE [review].[pRatingItemGetAllByRatingSecure]
	@RatingId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		ri.*
	FROM
		[review].[vRatingItemSecure] ri
	WHERE
		ri.[RatingItem.RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemProductScoreDecrement]'
GO

CREATE PROCEDURE [review].[pRatingItemProductScoreDecrement]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ProductId INT
	DECLARE @ChannelId INT
	
	SELECT
		@ProductId = rrf.ProductId,
		@ChannelId = rrf.ChannelId
	FROM [review].[tRatingReviewFeedback] rrf
	WHERE rrf.[RatingReviewFeedbackId] = @RatingReviewFeedbackId
	
	DECLARE @RelationListTypeId INT
	SET @RelationListTypeId = (SELECT RelationListTypeId FROM [product].[tRelationListType] WHERE CommonName = 'Variant')
	
	IF OBJECT_ID('tempdb..#VariantIds') IS NOT NULL
		DROP TABLE #VariantIds
	CREATE TABLE #VariantIds(VariantId INT);
	INSERT INTO #VariantIds EXEC [lekmer].[pProductVariantGetIdAllByProduct] @ProductId, @RelationListTypeId
	
	-- Add current product
	INSERT INTO #VARIANTIDS (VariantId) VALUES (@ProductId)

	UPDATE ps
	SET ps.[HitCount] = ps.[HitCount] - 1
	FROM [review].[tRatingItemProductScore] ps
		INNER JOIN [review].[tRatingItemProductVote] pv ON 
			pv.[RatingId] = ps.[RatingId] 
			AND pv.[RatingItemId] = ps.[RatingItemId]
		INNER JOIN #VariantIds vid ON
			vid.[VariantId] = ps.[ProductId]
	WHERE
		ps.[ChannelId] = @ChannelId
		AND pv.[RatingReviewFeedbackId] = @RatingReviewFeedbackId
	
	DELETE ps
	FROM [review].[tRatingItemProductScore] ps
		INNER JOIN [review].[tRatingItemProductVote] pv ON 
			pv.[RatingId] = ps.[RatingId] 
			AND pv.[RatingItemId] = ps.[RatingItemId]
		INNER JOIN #VariantIds vid ON
			vid.[VariantId] = ps.[ProductId]
	WHERE
		ps.[ChannelId] = @ChannelId
		AND pv.[RatingReviewFeedbackId] = @RatingReviewFeedbackId
		AND ps.[HitCount] <= 0
	
	IF OBJECT_ID('tempdb..#VariantIds') IS NOT NULL
		DROP TABLE #VariantIds
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingItemProductScore]'
GO


CREATE VIEW [review].[vRatingItemProductScore]
AS
	SELECT
		rips.[ChannelId]	'RatingItemProductScore.ChannelId',
		rips.[ProductId]	'RatingItemProductScore.ProductId',
		rips.[RatingId]		'RatingItemProductScore.RatingId',
		rips.[RatingItemId]	'RatingItemProductScore.RatingItemId',
		rips.[HitCount]		'RatingItemProductScore.HitCount'
	FROM 
		[review].[tRatingItemProductScore] rips

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemProductScoreGetSummaryByRatingAndProduct]'
GO

CREATE PROCEDURE [review].[pRatingItemProductScoreGetSummaryByRatingAndProduct]
	@ChannelId INT,
	@RatingId INT,
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		ri.[RatingItem.RatingId] 'RatingItemProductScoreSummary.RatingId',
		ri.[RatingItem.RatingItemId] 'RatingItemProductScoreSummary.RatingItemId',
		ri.[RatingItem.Score] 'RatingItemProductScoreSummary.RatingItemScore',
		ISNULL(a0.[TotalHitCount], 0) 'RatingItemProductScoreSummary.TotalHitCount'
	FROM 
		[review].[vRatingItemSecure] ri
		OUTER APPLY
		(
			SELECT
				SUM(ps.[RatingItemProductScore.HitCount]) 'TotalHitCount'
			FROM
				[review].[vRatingItemProductScore] ps
			WHERE
				ps.[RatingItemProductScore.ChannelId] = @ChannelId
				AND ps.[RatingItemProductScore.RatingId] = ri.[RatingItem.RatingId]
				AND ps.[RatingItemProductScore.RatingItemId] = ri.[RatingItem.RatingItemId]
				AND ps.[RatingItemProductScore.ProductId] = @ProductId
		) a0
	WHERE
		ri.[RatingItem.RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemProductScoreInsert]'
GO

CREATE PROCEDURE [review].[pRatingItemProductScoreInsert]
	@ChannelId INT,
	@ProductId INT,
	@RatingId INT,
	@RatingItemId INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[review].[tRatingItemProductScore]
	SET
		HitCount = HitCount + 1
	WHERE
		ChannelId = @ChannelId
		AND
		ProductId = @ProductId
		AND
		RatingId = @RatingId
		AND
		RatingItemId = @RatingItemId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingItemProductScore] (
			[ChannelId],
			[ProductId],
			[RatingId],
			[RatingItemId],
			[HitCount]
		)
		VALUES (
			@ChannelId,
			@ProductId,
			@RatingId,
			@RatingItemId,
			1
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemProductVoteDelete]'
GO

CREATE PROCEDURE [review].[pRatingItemProductVoteDelete]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [review].[tRatingItemProductVote]
	WHERE [RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemProductVoteInsert]'
GO

CREATE PROCEDURE [review].[pRatingItemProductVoteInsert]
	@RatingReviewFeedbackId INT,
	@RatingId INT,
	@RatingItemId INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tRatingItemProductVote] (
		[RatingReviewFeedbackId],
		[RatingId],
		[RatingItemId]
	)
	VALUES (
		@RatingReviewFeedbackId,
		@RatingId,
		@RatingItemId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemSave]'
GO

CREATE PROCEDURE [review].[pRatingItemSave]
	@RatingItemId INT,
	@RatingId INT,
	@Title NVARCHAR(50),
	@Description NVARCHAR(255),
	@Score INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[review].[tRatingItem]
	SET 
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[RatingItemId] = @RatingItemId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingItem] (
			[RatingId],
			[Title],
			[Description],
			[Score]
		)
		VALUES (
			@RatingId,
			@Title,
			@Description,
			@Score
		)

		SET @RatingItemId = SCOPE_IDENTITY()
	END

	RETURN @RatingItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingItemTranslation]'
GO


CREATE VIEW [review].[vRatingItemTranslation]
AS
SELECT 
	[RatingItemId] AS 'RatingItemTranslation.RatingItemId',
	[LanguageId] AS 'RatingItemTranslation.LanguageId',
	[Title] AS 'RatingItemTranslation.Title',
	[Description] AS 'RatingItemTranslation.Description'
FROM 
	[review].[tRatingItemTranslation]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemTranslationDescriptionGetAll]'
GO

CREATE PROCEDURE [review].[pRatingItemTranslationDescriptionGetAll]
	@RatingItemId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rit.[RatingItemTranslation.RatingItemId] AS 'Id',
		rit.[RatingItemTranslation.LanguageId] AS 'LanguageId',
		rit.[RatingItemTranslation.Description] AS 'Value'
	FROM
	    [review].[vRatingItemTranslation] rit
	WHERE 
		rit.[RatingItemTranslation.RatingItemId] = @RatingItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemTranslationSave]'
GO

CREATE PROCEDURE [review].[pRatingItemTranslationSave]
	@RatingItemId	INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[review].[tRatingItemTranslation]
	SET
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[RatingItemId] = @RatingItemId 
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [review].[tRatingItemTranslation] (
			[RatingItemId],
			[LanguageId],
			[Title],
			[Description]				
		)
		VALUES (
			@RatingItemId,
			@LanguageId,
			@Title,
			@Description
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemTranslationTitleGetAll]'
GO

CREATE PROCEDURE [review].[pRatingItemTranslationTitleGetAll]
	@RatingItemId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rit.[RatingItemTranslation.RatingItemId] AS 'Id',
		rit.[RatingItemTranslation.LanguageId] AS 'LanguageId',
		rit.[RatingItemTranslation.Title] AS 'Value'
	FROM
	    [review].[vRatingItemTranslation] rit
	WHERE 
		rit.[RatingItemTranslation.RatingItemId] = @RatingItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingRegistry]'
GO


CREATE VIEW [review].[vRatingRegistry]
AS
	SELECT
		rr.[RatingRegistryId]	'RatingRegistry.RatingRegistryId',
		rr.[CommonName]			'RatingRegistry.CommonName',
		rr.[Title]				'RatingRegistry.Title'
	FROM 
		[review].[tRatingRegistry] rr
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingRegistryGetAll]'
GO
CREATE PROCEDURE [review].[pRatingRegistryGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rr.*
	FROM
		[review].[vRatingRegistry] rr
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedbackLikeDelete]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedbackLikeDelete]
	@RatingReviewFeedbackId	INT
AS    
BEGIN
	DELETE [review].[tRatingReviewFeedbackLike]
	WHERE [RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pReviewDeleteAll]'
GO

CREATE PROCEDURE [review].[pReviewDeleteAll]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM	[review].[tReview]
	WHERE [RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingReviewFeedback]'
GO

CREATE VIEW [review].[vRatingReviewFeedback]
AS
	SELECT
		rrf.[RatingReviewFeedbackId] 'RatingReviewFeedback.RatingReviewFeedbackId',
		rrf.[ChannelId]				 'RatingReviewFeedback.ChannelId',
		rrf.[ProductId]				 'RatingReviewFeedback.ProductId',
		rrf.[OrderId]				 'RatingReviewFeedback.OrderId',
		rrf.[RatingReviewStatusId]	 'RatingReviewFeedback.RatingReviewStatusId',
		rrf.[LikeHit]				 'RatingReviewFeedback.LikeHit',
		rrf.[Impropriate]			 'RatingReviewFeedback.Impropriate',
		rrf.[IPAddress]				 'RatingReviewFeedback.IPAddress',
		rrf.[CreatedDate] 			 'RatingReviewFeedback.CreatedDate',
		rrf.[RatingReviewUserId]	 'RatingReviewFeedback.RatingReviewUserId'
	FROM 
		[review].[tRatingReviewFeedback] rrf
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_GetByProductAndOrder]'
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_GetByProductAndOrder]
	@ChannelId	INT,
	@ProductId	INT,
	@OrderId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
	WHERE 
		rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
		AND rrf.[RatingReviewFeedback.ProductId] = @ProductId
		AND rrf.[RatingReviewFeedback.OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_Insert]'
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_Insert]
	@ChannelId INT,
	@ProductId INT,
	@OrderId INT,
	@RatingReviewStatusId INT,
	@LikeHit INT,
	@Impropriate BIT,
	@IPAddress VARCHAR(50),
	@CreatedDate DATETIME,
	@RatingReviewUserId INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tRatingReviewFeedback] (
		[ChannelId],
		[ProductId],
		[OrderId],
		[RatingReviewStatusId],
		[LikeHit],
		[Impropriate],
		[IPAddress],
		[CreatedDate],
		[RatingReviewUserId]
	)
	VALUES (
		@ChannelId,
		@ProductId,
		@OrderId,
		@RatingReviewStatusId,
		@LikeHit,
		@Impropriate,
		@IPAddress,
		@CreatedDate,
		@RatingReviewUserId
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_RemoveInappropriateFlag]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_RemoveInappropriateFlag]
	@RatingReviewFeedbackId	INT
AS    
BEGIN
	UPDATE 
		[review].[tRatingReviewFeedback]
	SET 
		Impropriate = 0
	WHERE 
		RatingReviewFeedbackId = @RatingReviewFeedbackId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_Search]'
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_Search]
	@Author					NVARCHAR(50),
	@Message				NVARCHAR(500),
	@StatusId				INT,
	@CreatedFrom			DATETIME,
	@CreatedTo				DATETIME,
	@ProductId				INT,
	@ProductTitle			NVARCHAR(256),
	@OrderId				INT,
	@InappropriateContent	BIT,
	@SortBy					VARCHAR(50) = NULL,
	@SortDescending			BIT = NULL,
	@Page					INT = NULL,
	@PageSize				INT
AS   
BEGIN
	SET NOCOUNT ON
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)
     
    SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' + COALESCE(@SortBy, 'RatingReviewFeedback.CreatedDate')+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			rr.*
		FROM
			[review].[vReviewRecord] rr
		WHERE
			(@ProductId IS NULL OR rr.[RatingReviewFeedback.ProductId] = @ProductId)'
			+ CASE WHEN (@ProductTitle IS NOT NULL) THEN + 'AND rr.[ReviewRecord.Product.Title] LIKE ''%' + REPLACE(@ProductTitle, '''', '''''') + '%''' ELSE '' END
			+ CASE WHEN (@StatusId IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.RatingReviewStatusId] = @StatusId ' ELSE '' END
			+ CASE WHEN (@Message IS NOT NULL) THEN + 'AND (rr.[Review.Title] LIKE ''%' + REPLACE(@Message, '''', '''''') + '%'' OR rr.[Review.Message] LIKE ''%' + REPLACE(@Message, '''', '''''') + '%'')' ELSE '' END
			+ CASE WHEN (@Author IS NOT NULL) THEN + 'AND rr.[Review.AuthorName] = @Author ' ELSE '' END
			+ CASE WHEN (@OrderId IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.OrderId] = @OrderId ' ELSE '' END
			+ CASE WHEN (@InappropriateContent = 1) THEN + 'AND rr.[RatingReviewFeedback.Impropriate] = 1 ' ELSE '' END
			+ CASE WHEN (@CreatedFrom IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.CreatedDate] >= @CreatedFrom ' ELSE '' END
			+ CASE WHEN (@CreatedTo IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.CreatedDate] <= @CreatedTo ' ELSE '' END

     SET @sql = 'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

     IF @Page != 0 AND @Page IS NOT NULL 
     BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
     END
     
     SET @sqlCount = 'SELECT COUNT(1) FROM (' + @sqlFragment + ') AS CountResults'
     
	EXEC sp_executesql @sqlCount,
		N'      
			@Author					NVARCHAR(50),
			@Message				NVARCHAR(500),
			@StatusId				INT,
			@CreatedFrom			DATETIME,
            @CreatedTo				DATETIME,
            @ProductId				INT,
            @ProductTitle			NVARCHAR(256),
            @OrderId				INT',
            @Author,
            @Message,
            @StatusId,
            @CreatedFrom,
            @CreatedTo,
            @ProductId,
            @ProductTitle,
            @OrderId
                             
	EXEC sp_executesql @sql, 
		N'      
			@Author					NVARCHAR(50),
			@Message				NVARCHAR(500),
			@StatusId				INT,
			@CreatedFrom			DATETIME,
            @CreatedTo				DATETIME,
            @ProductId				INT,
            @ProductTitle			NVARCHAR(256),
            @OrderId				INT',
            @Author,
            @Message,
            @StatusId,
            @CreatedFrom,
            @CreatedTo,
            @ProductId,
            @ProductTitle,
            @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_SetStatus]'
GO


CREATE PROCEDURE [review].[pRatingReviewFeedback_SetStatus]
	@RatingReviewFeedbackId	INT,
	@StatusId	INT
AS    
BEGIN
	UPDATE 
		[review].[tRatingReviewFeedback]
	SET 
		RatingReviewStatusId = @StatusId
	WHERE 
		RatingReviewFeedbackId = @RatingReviewFeedbackId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingReviewStatus]'
GO

CREATE VIEW [review].[vRatingReviewStatus]
AS
 SELECT 
         rrs.[RatingReviewStatusId] 'RatingReviewStatus.RatingReviewStatusId', 
         rrs.[CommonName] 'RatingReviewStatus.CommonName',
         rrs.[Title] 'RatingReviewStatus.Title'
 FROM 
         [review].[tRatingReviewStatus] rrs
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewStatusGetAll]'
GO

CREATE PROCEDURE [review].[pRatingReviewStatusGetAll]
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewStatus]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewStatusGetByCommonName]'
GO

CREATE PROCEDURE [review].[pRatingReviewStatusGetByCommonName]
	@CommonName VARCHAR(50)
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewStatus] rrs
	WHERE
		rrs.[RatingReviewStatus.CommonName] = @CommonName
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingReviewUser]'
GO

CREATE VIEW [review].[vRatingReviewUser]
AS
	SELECT
		rru.[RatingReviewUserId]	'RatingReviewUser.RatingReviewUserId',
		rru.[CustomerId]			'RatingReviewUser.CustomerId',
		rru.[Token]					'RatingReviewUser.Token',
		rru.[CreatedDate]			'RatingReviewUser.CreatedDate'
	FROM 
		[review].[tRatingReviewUser] rru
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewUserGetByCustomer]'
GO

CREATE PROCEDURE [review].[pRatingReviewUserGetByCustomer]
	@CustomerId INT
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewUser] rru
	WHERE
		rru.[RatingReviewUser.CustomerId] = @CustomerId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewUserGetById]'
GO

CREATE PROCEDURE [review].[pRatingReviewUserGetById]
	@RatingReviewUserId INT
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewUser] rru
	WHERE
		rru.[RatingReviewUser.RatingReviewUserId] = @RatingReviewUserId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewUserGetByToken]'
GO

CREATE PROCEDURE [review].[pRatingReviewUserGetByToken]
	@Token VARCHAR(50)
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewUser] rru
	WHERE
		rru.[RatingReviewUser.Token] = @Token
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewUserSave]'
GO

CREATE PROCEDURE [review].[pRatingReviewUserSave]
	@RatingReviewUserId INT,
	@CustomerId			INT,
	@Token				VARCHAR(50),
	@CreatedDate		DATETIME
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[review].[tRatingReviewUser]
	SET 
		[CustomerId] = @CustomerId
	WHERE
		[RatingReviewUserId] = @RatingReviewUserId
		AND [CustomerId] IS NULL
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingReviewUser] (
			[CustomerId],
			[Token],
			[CreatedDate]
		)
		VALUES (
			@CustomerId,
			@Token,
			@CreatedDate
		)

		SET @RatingReviewUserId = SCOPE_IDENTITY()
	END

	RETURN @RatingReviewUserId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingTranslationDelete]'
GO

CREATE PROCEDURE [review].[pRatingTranslationDelete]
	@RatingId		INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [review].[tRatingTranslation]
	WHERE [RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingTranslation]'
GO

CREATE VIEW [review].[vRatingTranslation]
AS
SELECT 
	[RatingId] AS 'RatingTranslation.RatingId',
	[LanguageId] AS 'RatingTranslation.LanguageId',
	[Title] AS 'RatingTranslation.Title',
	[Description] AS 'RatingTranslation.Description'
FROM 
	[review].[tRatingTranslation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingTranslationDescriptionGetAll]'
GO

CREATE PROCEDURE [review].[pRatingTranslationDescriptionGetAll]
	@RatingId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rt.[RatingTranslation.RatingId] AS 'Id',
		rt.[RatingTranslation.LanguageId] AS 'LanguageId',
		rt.[RatingTranslation.Description] AS 'Value'
	FROM
	    [review].[vRatingTranslation] rt
	WHERE 
		rt.[RatingTranslation.RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingTranslationSave]'
GO

CREATE PROCEDURE [review].[pRatingTranslationSave]
	@RatingId		INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[review].[tRatingTranslation]
	SET
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[RatingId] = @RatingId 
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [review].[tRatingTranslation] (
			[RatingId],
			[LanguageId],
			[Title],
			[Description]				
		)
		VALUES (
			@RatingId,
			@LanguageId,
			@Title,
			@Description
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingTranslationTitleGetAll]'
GO

CREATE PROCEDURE [review].[pRatingTranslationTitleGetAll]
	@RatingId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rt.[RatingTranslation.RatingId] AS 'Id',
		rt.[RatingTranslation.LanguageId] AS 'LanguageId',
		rt.[RatingTranslation.Title] AS 'Value'
	FROM
	    [review].[vRatingTranslation] rt
	WHERE 
		rt.[RatingTranslation.RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingSecure]'
GO

CREATE VIEW [review].[vRatingSecure]
AS
	SELECT
		r.[RatingId]		'Rating.RatingId',
		r.[RatingFolderId]	'Rating.RatingFolderId',
		r.[RatingRegistryId]'Rating.RatingRegistryId',
		r.[CommonName]		'Rating.CommonName',
		r.[Title]			'Rating.Title',
		r.[Description]		'Rating.Description',
		r.[CommonForVariants]	'Rating.CommonForVariants'
	FROM 
		[review].[tRating] r
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRating_GetAllByFolder]'
GO
CREATE PROCEDURE [review].[pRating_GetAllByFolder]
	@RatingFolderId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRatingSecure] r
	WHERE
		r.[Rating.RatingFolderId] = @RatingFolderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRating]'
GO

CREATE VIEW [review].[vRating]
AS
	SELECT
		r.[RatingId]		'Rating.RatingId',
		r.[RatingFolderId]	'Rating.RatingFolderId',
		r.[RatingRegistryId]'Rating.RatingRegistryId',
		r.[CommonName]		'Rating.CommonName',
		COALESCE(rt.[Title], r.[Title])	'Rating.Title',
		COALESCE(rt.[Description], r.[Description])	'Rating.Description',
		r.[CommonForVariants]	'Rating.CommonForVariants',
		c.[ChannelId]		'Rating.Channel.Id'
	FROM 
		[review].[tRating] r
		INNER JOIN [review].[tRatingModuleChannel] rmc ON rmc.[RatingRegistryId] = r.[RatingRegistryId]
		INNER JOIN [core].[tChannel] c ON c.[ChannelId] = rmc.[ChannelId]
		LEFT OUTER JOIN [review].[tRatingTranslation] rt ON rt.[RatingId] = r.[RatingId] AND rt.[LanguageId] = c.[LanguageId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRating_GetAllByGroup]'
GO

CREATE PROCEDURE [review].[pRating_GetAllByGroup]
	@RatingGroupId INT,
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRatingGroupRating] rgr
		INNER JOIN [review].[vRating] r ON r.[Rating.RatingId] = rgr.[RatingGroupRating.RatingId]
	WHERE
		rgr.[RatingGroupRating.RatingGroupId] = @RatingGroupId
		AND
		r.[Rating.Channel.Id] = @ChannelId
	ORDER BY
		rgr.[RatingGroupRating.Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRating_GetAllByGroupSecure]'
GO
CREATE PROCEDURE [review].[pRating_GetAllByGroupSecure]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRatingGroupRating] rgr
		INNER JOIN [review].[vRatingSecure] r ON r.[Rating.RatingId] = rgr.[RatingGroupRating.RatingId]
	WHERE
		rgr.[RatingGroupRating.RatingGroupId] = @RatingGroupId
	ORDER BY
		rgr.[RatingGroupRating.Ordinal]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRating_GetById]'
GO
CREATE PROCEDURE [review].[pRating_GetById]
	@RatingId INT,
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRating] r
	WHERE
		r.[Rating.RatingId] = @RatingId
		AND
		r.[Rating.Channel.Id] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRating_GetByIdSecure]'
GO
CREATE PROCEDURE [review].[pRating_GetByIdSecure]
	@RatingId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRatingSecure] r
	WHERE
		r.[Rating.RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRating_Save]'
GO

CREATE PROCEDURE [review].[pRating_Save]
	@RatingId INT,
	@RatingFolderId INT,
	@RatingRegistryId INT,
	@CommonName VARCHAR(50),
	@CommonForVariants BIT,
	@Title NVARCHAR(50),
	@Description NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [review].[tRating]
		WHERE [Title] = @Title
			AND [RatingId] <> @RatingId
			AND	[RatingFolderId] = @RatingFolderId
	)
	RETURN -1
	
	-- Same common names are not allowed in same registry (return -2)
	IF EXISTS
	(
		SELECT 1
		FROM [review].[tRating]
		WHERE [CommonName] = @CommonName
			AND [RatingRegistryId] = @RatingRegistryId
			AND [RatingId] <> @RatingId
	)
	RETURN -2
	
	UPDATE 
		[review].[tRating]
	SET 
		[RatingFolderId] = @RatingFolderId,
		[RatingRegistryId] = @RatingRegistryId,
		[CommonName] = @CommonName,
		[Title] = @Title,
		[Description] = @Description,
		[CommonForVariants] = @CommonForVariants
	WHERE
		[RatingId] = @RatingId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRating] (
			[RatingFolderId],
			[RatingRegistryId],
			[CommonName],
			[Title],
			[Description],
			[CommonForVariants]			
		)
		VALUES (
			@RatingFolderId,
			@RatingRegistryId,
			@CommonName,
			@Title,
			@Description,
			@CommonForVariants
		)

		SET @RatingId = SCOPE_IDENTITY()
	END

	RETURN @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRating_Search]'
GO

CREATE PROCEDURE [review].[pRating_Search]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		r.*
	FROM 
		[review].[vRatingSecure] r
	WHERE
		r.[Rating.Title] LIKE @SearchCriteria ESCAPE '\'
		OR r.[Rating.CommonName] LIKE @SearchCriteria ESCAPE '\'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vReview]'
GO

CREATE VIEW [review].[vReview]
AS
	SELECT
		r.[ReviewId] 'Review.ReviewId',
		r.[RatingReviewFeedbackId] 'Review.RatingReviewFeedbackId',
		r.[AuthorName] 'Review.AuthorName',
		r.[Title] 'Review.Title',
		r.[Message] 'Review.Message'
	FROM 
		[review].[tReview] r
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pReviewGetByFeedback]'
GO

CREATE PROCEDURE [review].[pReviewGetByFeedback]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vReview] r
	WHERE
		r.[Review.RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pReviewInsert]'
GO

CREATE PROCEDURE [review].[pReviewInsert]
	@RatingReviewFeedbackId INT,
	@AuthorName NVARCHAR(50),
	@Title NVARCHAR(50),
	@Message NVARCHAR(3000)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tReview] (
		[RatingReviewFeedbackId],
		[AuthorName],
		[Title],
		[Message]
	)
	VALUES (
		@RatingReviewFeedbackId,
		@AuthorName,
		@Title,
		@Message
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemDelete]'
GO

CREATE PROCEDURE [review].[pRatingItemDelete]
	@RatingItemId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [review].[pRatingItemTranslationDelete] @RatingItemId

	DELETE FROM [review].[tRatingItem]
	WHERE [RatingItemId] = @RatingItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemDeleteAll]'
GO

CREATE PROCEDURE [review].[pRatingItemDeleteAll]
	@RatingId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	EXEC [review].[pRatingItemTranslationDeleteAll] @RatingId
	
	DELETE FROM [review].[tRatingItem]
	WHERE [RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vRatingItemProductVote]'
GO




CREATE VIEW [review].[vRatingItemProductVote]
AS
	SELECT
		ripv.[RatingReviewFeedbackId]	'RatingItemProductVote.RatingReviewFeedbackId',
		ripv.[RatingId]					'RatingItemProductVote.RatingId',
		ripv.[RatingItemId]				'RatingItemProductVote.RatingItemId',
		r.*,
		ri.*
	FROM 
		[review].[tRatingItemProductVote] ripv
		INNER JOIN [review].[vRatingReviewFeedback] rrf ON rrf.[RatingReviewFeedback.RatingReviewFeedbackId] = ripv.[RatingReviewFeedbackId]
		INNER JOIN [review].[vRating] r ON r.[Rating.RatingId] = ripv.[RatingId] AND r.[Rating.Channel.Id] = rrf.[RatingReviewFeedback.ChannelId]
		INNER JOIN [review].[vRatingItem] ri ON ri.[RatingItem.RatingItemId] = ripv.[RatingItemId] AND ri.[RatingItem.Channel.Id] = rrf.[RatingReviewFeedback.ChannelId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemProductVoteGetAllByFeedback]'
GO

CREATE PROCEDURE [review].[pRatingItemProductVoteGetAllByFeedback]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		ripv.*
	FROM
		[review].[vRatingItemProductVote] ripv
	WHERE
		ripv.[RatingItemProductVote.RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_Delete]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_Delete]
	@RatingReviewFeedbackId	INT
AS    
BEGIN
	SET NOCOUNT ON
	
	EXEC [review].[pRatingReviewFeedbackLikeDelete] @RatingReviewFeedbackId
	EXEC [review].[pRatingItemProductScoreDecrement] @RatingReviewFeedbackId
	EXEC [review].[pRatingItemProductVoteDelete] @RatingReviewFeedbackId
	EXEC [review].[pReviewDeleteAll] @RatingReviewFeedbackId
	
	DELETE FROM [review].[tRatingReviewFeedback]
	WHERE RatingReviewFeedbackId = @RatingReviewFeedbackId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_GetMostHelpful]'
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_GetMostHelpful]
	@ChannelId				INT,
	@ProductId				INT,
	@RatingId				INT,
	@RatingItemIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@RatingReviewStatusId	INT,
	@IsImpropriate			BIT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		TOP(1) rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
		INNER JOIN [review].[vRatingItemProductVote] ripv 
			ON ripv.[RatingItemProductVote.RatingReviewFeedbackId] = rrf.[RatingReviewFeedback.RatingReviewFeedbackId]
		INNER JOIN [generic].[fnConvertIDListToTable](@RatingItemIds, @Delimiter) ri 
			ON ri.[ID] = ripv.[RatingItemProductVote.RatingItemId]
	WHERE 
		rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
		AND rrf.[RatingReviewFeedback.ProductId] = @ProductId
		AND rrf.[RatingReviewFeedback.RatingReviewStatusId] = @RatingReviewStatusId
		AND rrf.[RatingReviewFeedback.Impropriate] = @IsImpropriate
		AND ripv.[RatingItemProductVote.RatingId] = @RatingId
	ORDER BY rrf.[RatingReviewFeedback.LikeHit] DESC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRating_Delete]'
GO

CREATE PROCEDURE [review].[pRating_Delete]
	@RatingId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [review].[pRatingItemDeleteAll] @RatingId
	EXEC [review].[pRatingGroupRatingDelete] NULL, @RatingId
	EXEC [review].[pRatingTranslationDelete] @RatingId

	DELETE FROM [review].[tRating]
	WHERE [RatingId] = @RatingId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vProductWithoutStatusFilter]'
GO
CREATE VIEW [lekmer].[vProductWithoutStatusFilter]
AS
SELECT     
	p.[ProductId] 'Product.Id',
	p.[ItemsInPackage] 'Product.ItemsInPackage',
	p.[ErpId] 'Product.ErpId',
	p.[EanCode] 'Product.EanCode',
	p.[NumberInStock] 'Product.NumberInStock',
    p.[CategoryId] 'Product.CategoryId',
    COALESCE(pt.[WebShopTitle], p.[WebShopTitle]) 'Product.WebShopTitle',
	COALESCE(pt.[Title], p.[Title]) 'Product.Title',
    p.[ProductStatusId] 'Product.ProductStatusId',
    COALESCE(pt.[ShortDescription], p.[ShortDescription]) 'Product.ShortDescription',
    ch.[ChannelId] 'Product.ChannelId',
    ch.[CurrencyId] 'Product.CurrencyId',
    pmc.[PriceListRegistryId] 'Product.PriceListRegistryId',
    p.[MediaId] 'Product.MediaId'
FROM
	[product].[tProduct] AS p
	/* filetrring */
	INNER JOIN [product].[tProductRegistryProduct] AS prp ON p.[ProductId] = prp.[ProductId]
	INNER JOIN [product].[tProductModuleChannel] AS pmc	ON prp.[ProductRegistryId] = pmc.[ProductRegistryId]
	INNER JOIN [core].[tChannel] AS ch ON pmc.[ChannelId] = ch.[ChannelId]
	LEFT JOIN [product].[tProductTranslation] AS pt ON p.[ProductId] = pt.[ProductId] AND ch.[LanguageId] = pt.[LanguageId]
WHERE
	p.[IsDeleted] = 0
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vReviewRecord]'
GO

CREATE VIEW [review].[vReviewRecord]
AS
	SELECT
		r.*,
		rrf.*,
		(CASE WHEN p.[WebShopTitle] IS NULL THEN p.[Title] ELSE p.[WebShopTitle] END) 'ReviewRecord.Product.Title',
		c.[UserName] 'ReviewRecord.Customer.UserName'
	FROM 
		[review].[vReview] r
		INNER JOIN [review].[vRatingReviewFeedback] rrf ON rrf.[RatingReviewFeedback.RatingReviewFeedbackId] = r.[Review.RatingReviewFeedbackId]
		INNER JOIN [review].[tRatingReviewUser] rru ON rru.[RatingReviewUserId] = rrf.[RatingReviewFeedback.RatingReviewUserId]
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = rrf.[RatingReviewFeedback.ProductId]
		LEFT JOIN [customer].[tCustomerUser] c ON c.[CustomerId] = rru.[CustomerId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pLekmerOrderGetByFeedbackToken]'
GO

CREATE PROCEDURE [lekmer].[pLekmerOrderGetByFeedbackToken]
	@FeedbackToken UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Lekmer.FeedbackToken] = @FeedbackToken
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pLekmerOrderGetLatest]'
GO

CREATE PROCEDURE [lekmer].[pLekmerOrderGetLatest]
	@Date DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.CreatedDate] >= @Date
		AND [Order.CreatedDate] < DATEADD(day, 1, @Date)
		AND [Lekmer.FeedbackToken] IS NULL
		AND [OrderStatus.CommonName] = 'OrderInHY'
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockBestRatedProductListGetById]'
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		bbrpl.*,
		b.*
	FROM 
		[review].[vBlockBestRatedProductList] AS bbrpl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON bbrpl.[BlockBestRatedProductList.BlockId] = b.[Block.BlockId]
	WHERE
		bbrpl.[BlockBestRatedProductList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockBestRatedProductListGetByIdSecure]'
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		bbrpl.*,
		b.*
	FROM 
		[review].[vBlockBestRatedProductList] AS bbrpl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON bbrpl.[BlockBestRatedProductList.BlockId] = b.[Block.BlockId]
	WHERE
		bbrpl.[BlockBestRatedProductList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockLatestFeedbackListGetById]'
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		blfl.*,
		b.*
	FROM 
		[review].[vBlockLatestFeedbackList] AS blfl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON blfl.[BlockLatestFeedbackList.BlockId] = b.[Block.BlockId]
	WHERE
		blfl.[BlockLatestFeedbackList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockLatestFeedbackListGetByIdSecure]'
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		blfl.*,
		b.*
	FROM 
		[review].[vBlockLatestFeedbackList] AS blfl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON blfl.[BlockLatestFeedbackList.BlockId] = b.[Block.BlockId]
	WHERE
		blfl.[BlockLatestFeedbackList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockProductFeedbackGetById]'
GO

CREATE PROCEDURE [review].[pBlockProductFeedbackGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		bpf.*,
		b.*
	FROM 
		[review].[vBlockProductFeedback] AS bpf
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON bpf.[BlockProductFeedback.BlockId] = b.[Block.BlockId]
	WHERE
		bpf.[BlockProductFeedback.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockProductFeedbackGetByIdSecure]'
GO

CREATE PROCEDURE [review].[pBlockProductFeedbackGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		bpf.*,
		b.*
	FROM 
		[review].[vBlockProductFeedback] AS bpf
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON bpf.[BlockProductFeedback.BlockId] = b.[Block.BlockId]
	WHERE
		bpf.[BlockProductFeedback.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCustomProductWithoutStatusFilter]'
GO
CREATE VIEW [lekmer].[vCustomProductWithoutStatusFilter]
AS 
	SELECT
		p.*,
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.[ParentContentNodeId] AS 'Product.ParentContentNodeId',
		i.*
	FROM
		[lekmer].[vProductWithoutStatusFilter] p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] AS ssmc ON ssmc.[ChannelId] = c.[Channel.Id]
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON p.[Product.Id] = pssr.[ProductId] AND ssmc.[SiteStructureRegistryId] = pssr.[SiteStructureRegistryId]
		LEFT JOIN [media].[vCustomImage] AS i ON i.[Image.MediaId] = p.[Product.MediaId] AND i.[Image.LanguageId] = c.[Language.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockBestRatedProductListBrand]'
GO



CREATE VIEW [review].[vBlockBestRatedProductListBrand]
AS
	SELECT
		bbrplb.[BlockId] AS 'BlockBestRatedProductListBrand.BlockId',
		bbrplb.[BrandId] AS 'BlockBestRatedProductListBrand.BrandId',
		b.*
	FROM
		[review].[tBlockBestRatedProductListBrand] bbrplb
		INNER JOIN [lekmer].[vBrand] b ON b.[Brand.BrandId] = bbrplb.[BrandId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockBestRatedProductListBrandGetAllByBlock]'
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListBrandGetAllByBlock]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[review].[vBlockBestRatedProductListBrand] b
	WHERE
		b.[BlockBestRatedProductListBrand.BlockId] = @BlockId
		AND b.[ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockBestRatedProductListBrandSecure]'
GO



CREATE VIEW [review].[vBlockBestRatedProductListBrandSecure]
AS
	SELECT
		bbrplb.[BlockId] AS 'BlockBestRatedProductListBrand.BlockId',
		bbrplb.[BrandId] AS 'BlockBestRatedProductListBrand.BrandId',
		b.*
	FROM
		[review].[tBlockBestRatedProductListBrand] bbrplb
		INNER JOIN [lekmer].[vBrandSecure] b ON b.[Brand.BrandId] = bbrplb.[BrandId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockBestRatedProductListBrandGetAllByBlockSecure]'
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListBrandGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[review].[vBlockBestRatedProductListBrandSecure] b
	WHERE
		b.[BlockBestRatedProductListBrand.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockLatestFeedbackListBrand]'
GO


CREATE VIEW [review].[vBlockLatestFeedbackListBrand]
AS
	SELECT
		blflb.[BlockId] AS 'BlockLatestFeedbackListBrand.BlockId',
		blflb.[BrandId] AS 'BlockLatestFeedbackListBrand.BrandId',
		b.*
	FROM
		[review].[tBlockLatestFeedbackListBrand] blflb
		INNER JOIN [lekmer].[vBrand] b ON b.[Brand.BrandId] = blflb.[BrandId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockLatestFeedbackListBrandGetAllByBlock]'
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListBrandGetAllByBlock]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[review].[vBlockLatestFeedbackListBrand] b
	WHERE
		b.[BlockLatestFeedbackListBrand.BlockId] = @BlockId
		AND b.[ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[vBlockLatestFeedbackListBrandSecure]'
GO


CREATE VIEW [review].[vBlockLatestFeedbackListBrandSecure]
AS
	SELECT
		blflb.[BlockId] AS 'BlockLatestFeedbackListBrand.BlockId',
		blflb.[BrandId] AS 'BlockLatestFeedbackListBrand.BrandId',
		b.*
	FROM
		[review].[tBlockLatestFeedbackListBrand] blflb
		INNER JOIN [lekmer].[vBrandSecure] b ON b.[Brand.BrandId] = blflb.[BrandId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pBlockLatestFeedbackListBrandGetAllByBlockSecure]'
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListBrandGetAllByBlockSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[review].[vBlockLatestFeedbackListBrandSecure] b
	WHERE
		b.[BlockLatestFeedbackListBrand.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetByIdWithoutStatusFilter]'
GO
CREATE PROCEDURE [lekmer].[pProductGetByIdWithoutStatusFilter]
	@ProductId	INT,
	@ChannelId	INT,
	@CustomerId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		*
	FROM
		[lekmer].[vCustomProductWithoutStatusFilter] AS p
		INNER JOIN [product].[vCustomPriceListItem] AS pli ON pli.[Price.ProductId] = p.[Product.Id]
		
	WHERE
		[Product.Id] = @ProductId
		AND [Product.ChannelId] = @ChannelId
		AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice]
			(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingItemProductScoreGetBestRatedProductsByRating]'
GO

CREATE PROCEDURE [review].[pRatingItemProductScoreGetBestRatedProductsByRating]
	@ChannelId		INT,
	@RatingId		INT,
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1),
	@NumberOfItems	INT,
	@CustomerId		INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql NVARCHAR(MAX)
    SET @sql = '
		SELECT 
			TOP(@NumberOfItems) rips.ProductId
		FROM 
			review.tRatingItemProductScore rips
			INNER JOIN review.tRatingItem ri 
				ON ri.RatingItemId = rips.RatingItemId
			INNER JOIN [product].[vCustomProduct] p 
				ON rips.ProductId = p.[Product.Id] 
				AND rips.ChannelId = p.[Product.ChannelId]
			INNER JOIN product.vCustomPriceListItem AS pli
				ON pli.[Price.ProductId] = p.[Product.Id]
				AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)'
			+ CASE WHEN (@CategoryIds IS NOT NULL AND @CategoryIds <> '') THEN 
				+ 'INNER JOIN [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) c ON c.[ID] = p.[Product.CategoryId]' 
			ELSE '' END
			+ CASE WHEN (@BrandIds IS NOT NULL AND @BrandIds <> '') THEN 
				+ 'INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) b ON b.[ID] = p.[Lekmer.BrandId]' 
			ELSE '' END
		+ '
		WHERE 
			rips.ChannelId = @ChannelId
			AND rips.RatingId = @RatingId
		GROUP BY rips.ProductId
		ORDER BY SUM(ri.Score*rips.HitCount) DESC'
     
	EXEC sp_executesql @sql, 
		N'      
			@ChannelId		INT,
			@RatingId		INT,
			@CategoryIds	VARCHAR(MAX),
			@BrandIds		VARCHAR(MAX),
			@Delimiter		CHAR(1),
			@NumberOfItems	INT,
			@CustomerId		INT',
            @ChannelId,
            @RatingId,
            @CategoryIds,
            @BrandIds,
            @Delimiter,
            @NumberOfItems,
            @CustomerId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [review].[pRatingReviewFeedback_GetLatest]'
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_GetLatest]
	@ChannelId				INT,
	@CategoryIds			VARCHAR(MAX),
	@BrandIds				VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@NumberOfItems			INT,
	@RatingReviewStatusId	INT,
	@IsImpropriate			BIT,
	@CustomerId				INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql NVARCHAR(MAX)
    SET @sql = '
		SELECT 
			TOP(@NumberOfItems) rrf.*
		FROM
			[review].[vRatingReviewFeedback] rrf
			INNER JOIN [product].[vCustomProduct] p 
				ON p.[Product.Id] = rrf.[RatingReviewFeedback.ProductId] 
				AND p.[Product.ChannelId] = rrf.[RatingReviewFeedback.ChannelId]
			INNER JOIN product.vCustomPriceListItem AS pli
				ON pli.[Price.ProductId] = p.[Product.Id]
				AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)'
			+ CASE WHEN (@CategoryIds IS NOT NULL AND @CategoryIds <> '') THEN 
				+ 'INNER JOIN [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) c ON c.[ID] = p.[Product.CategoryId]' 
			ELSE '' END
			+ CASE WHEN (@BrandIds IS NOT NULL AND @BrandIds <> '') THEN 
				+ 'INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) b ON b.[ID] = p.[Lekmer.BrandId]' 
			ELSE '' END
		+ '
		WHERE 
			rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
			AND rrf.[RatingReviewFeedback.RatingReviewStatusId] = @RatingReviewStatusId
			AND rrf.[RatingReviewFeedback.Impropriate] = @IsImpropriate
		ORDER BY rrf.[RatingReviewFeedback.CreatedDate] DESC'
     
	EXEC sp_executesql @sql, 
		N'      
			@ChannelId				INT,
			@CategoryIds			VARCHAR(MAX),
			@BrandIds				VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@NumberOfItems			INT,
			@RatingReviewStatusId	INT,
			@IsImpropriate			BIT,
			@CustomerId				INT',
            @ChannelId,
            @CategoryIds,
            @BrandIds,
            @Delimiter,
            @NumberOfItems,
            @RatingReviewStatusId,
            @IsImpropriate,
            @CustomerId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCategoryTagGroup]'
GO
ALTER TABLE [lekmer].[tCategoryTagGroup] ADD
CONSTRAINT [FK_tCategoryTagGroup_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]),
CONSTRAINT [FK_tCategoryTagGroup_tTagGroup] FOREIGN KEY ([TagGroupId]) REFERENCES [lekmer].[tTagGroup] ([TagGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tBlockBestRatedProductList]'
GO
ALTER TABLE [review].[tBlockBestRatedProductList] ADD
CONSTRAINT [FK_tBlockBestRatedProductList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]),
CONSTRAINT [FK_tBlockBestRatedProductList_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]),
CONSTRAINT [FK_tBlockBestRatedProductList_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tBlockBestRatedProductListBrand]'
GO
ALTER TABLE [review].[tBlockBestRatedProductListBrand] ADD
CONSTRAINT [FK_tBlockBestRatedProductListBrand_tBlockBestRatedProductList] FOREIGN KEY ([BlockId]) REFERENCES [review].[tBlockBestRatedProductList] ([BlockId]),
CONSTRAINT [FK_tBlockBestRatedProductListBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tBlockLatestFeedbackList]'
GO
ALTER TABLE [review].[tBlockLatestFeedbackList] ADD
CONSTRAINT [FK_tBlockLatestFeedbackList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]),
CONSTRAINT [FK_tBlockLatestFeedbackList_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tBlockLatestFeedbackListBrand]'
GO
ALTER TABLE [review].[tBlockLatestFeedbackListBrand] ADD
CONSTRAINT [FK_tBlockLatestFeedbackListBrand_tBlockLatestFeedbackList] FOREIGN KEY ([BlockId]) REFERENCES [review].[tBlockLatestFeedbackList] ([BlockId]),
CONSTRAINT [FK_tBlockLatestFeedbackListBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tBlockProductFeedback]'
GO
ALTER TABLE [review].[tBlockProductFeedback] ADD
CONSTRAINT [FK_tBlockProductFeedback_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tBlockRating]'
GO
ALTER TABLE [review].[tBlockRating] ADD
CONSTRAINT [FK_tBlockRating_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]),
CONSTRAINT [FK_tBlockRating_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tBlockRatingGroup]'
GO
ALTER TABLE [review].[tBlockRatingGroup] ADD
CONSTRAINT [FK_tBlockRatingGroup_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]),
CONSTRAINT [FK_tBlockRatingGroup_tRatingGroup] FOREIGN KEY ([RatingGroupId]) REFERENCES [review].[tRatingGroup] ([RatingGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tBlockRatingItem]'
GO
ALTER TABLE [review].[tBlockRatingItem] ADD
CONSTRAINT [FK_tBlockRatingItem_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]),
CONSTRAINT [FK_tBlockRatingItem_tRatingItem] FOREIGN KEY ([RatingItemId]) REFERENCES [review].[tRatingItem] ([RatingItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingGroupRating]'
GO
ALTER TABLE [review].[tRatingGroupRating] ADD
CONSTRAINT [FK_tRatingGroupRating_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId]),
CONSTRAINT [FK_tRatingGroupRating_tRatingGroup] FOREIGN KEY ([RatingGroupId]) REFERENCES [review].[tRatingGroup] ([RatingGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingItem]'
GO
ALTER TABLE [review].[tRatingItem] ADD
CONSTRAINT [FK_tRatingItem_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingItemProductScore]'
GO
ALTER TABLE [review].[tRatingItemProductScore] ADD
CONSTRAINT [FK_tRatingItemProductScore_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId]),
CONSTRAINT [FK_tRatingItemProductScore_tRatingItem] FOREIGN KEY ([RatingItemId]) REFERENCES [review].[tRatingItem] ([RatingItemId]),
CONSTRAINT [FK_tRatingItemProductScore_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId]),
CONSTRAINT [FK_tRatingItemProductScore_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingItemProductVote]'
GO
ALTER TABLE [review].[tRatingItemProductVote] ADD
CONSTRAINT [FK_tRatingItemProductVote_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId]),
CONSTRAINT [FK_tRatingItemProductVote_tRatingItem] FOREIGN KEY ([RatingItemId]) REFERENCES [review].[tRatingItem] ([RatingItemId]),
CONSTRAINT [FK_tRatingItemProductVote_tRatingReviewFeedback] FOREIGN KEY ([RatingReviewFeedbackId]) REFERENCES [review].[tRatingReviewFeedback] ([RatingReviewFeedbackId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingTranslation]'
GO
ALTER TABLE [review].[tRatingTranslation] ADD
CONSTRAINT [FK_tRatingTranslation_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId]),
CONSTRAINT [FK_tRatingInfoTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRating]'
GO
ALTER TABLE [review].[tRating] ADD
CONSTRAINT [FK_tRating_tRatingFolder] FOREIGN KEY ([RatingFolderId]) REFERENCES [review].[tRatingFolder] ([RatingFolderId]),
CONSTRAINT [FK_tRating_tRatingRegistry] FOREIGN KEY ([RatingRegistryId]) REFERENCES [review].[tRatingRegistry] ([RatingRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingFolder]'
GO
ALTER TABLE [review].[tRatingFolder] ADD
CONSTRAINT [FK_tRatingFolder_tRatingFolder] FOREIGN KEY ([ParentRatingFolderId]) REFERENCES [review].[tRatingFolder] ([RatingFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingGroupCategory]'
GO
ALTER TABLE [review].[tRatingGroupCategory] ADD
CONSTRAINT [FK_tRatingGroupCategory_tRatingGroup] FOREIGN KEY ([RatingGroupId]) REFERENCES [review].[tRatingGroup] ([RatingGroupId]),
CONSTRAINT [FK_tRatingGroupCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingGroupProduct]'
GO
ALTER TABLE [review].[tRatingGroupProduct] ADD
CONSTRAINT [FK_tRatingGroupProduct_tRatingGroup] FOREIGN KEY ([RatingGroupId]) REFERENCES [review].[tRatingGroup] ([RatingGroupId]),
CONSTRAINT [FK_tRatingGroupProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingGroup]'
GO
ALTER TABLE [review].[tRatingGroup] ADD
CONSTRAINT [FK_tRatingGroup_tRatingGroupFolder] FOREIGN KEY ([RatingGroupFolderId]) REFERENCES [review].[tRatingGroupFolder] ([RatingGroupFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingGroupFolder]'
GO
ALTER TABLE [review].[tRatingGroupFolder] ADD
CONSTRAINT [FK_tRatingGroupFolder_tRatingGroupFolder] FOREIGN KEY ([ParentRatingGroupFolderId]) REFERENCES [review].[tRatingGroupFolder] ([RatingGroupFolderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingItemTranslation]'
GO
ALTER TABLE [review].[tRatingItemTranslation] ADD
CONSTRAINT [FK_tRatingItemTranslation_tRatingItem] FOREIGN KEY ([RatingItemId]) REFERENCES [review].[tRatingItem] ([RatingItemId]),
CONSTRAINT [FK_tRatingItemTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingModuleChannel]'
GO
ALTER TABLE [review].[tRatingModuleChannel] ADD
CONSTRAINT [FK_tRatingModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId]),
CONSTRAINT [FK_tRatingModuleChannel_tRatingRegistry] FOREIGN KEY ([RatingRegistryId]) REFERENCES [review].[tRatingRegistry] ([RatingRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingReviewFeedbackLike]'
GO
ALTER TABLE [review].[tRatingReviewFeedbackLike] ADD
CONSTRAINT [FK_tRatingReviewFeedbackLike_tRatingReviewFeedback] FOREIGN KEY ([RatingReviewFeedbackId]) REFERENCES [review].[tRatingReviewFeedback] ([RatingReviewFeedbackId]),
CONSTRAINT [FK_tRatingReviewFeedbackLike_tRatingReviewUser] FOREIGN KEY ([RatingReviewUserId]) REFERENCES [review].[tRatingReviewUser] ([RatingReviewUserId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tReview]'
GO
ALTER TABLE [review].[tReview] ADD
CONSTRAINT [FK_tReview_tRatingReviewFeedback] FOREIGN KEY ([RatingReviewFeedbackId]) REFERENCES [review].[tRatingReviewFeedback] ([RatingReviewFeedbackId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingReviewFeedback]'
GO
ALTER TABLE [review].[tRatingReviewFeedback] ADD
CONSTRAINT [FK_tRatingReviewFeedback_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId]),
CONSTRAINT [FK_tRatingReviewFeedback_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]),
CONSTRAINT [FK_tRatingReviewFeedback_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId]),
CONSTRAINT [FK_tRatingReviewFeedback_tRatingReviewStatus] FOREIGN KEY ([RatingReviewStatusId]) REFERENCES [review].[tRatingReviewStatus] ([RatingReviewStatusId]),
CONSTRAINT [FK_tRatingReviewFeedback_tRatingReviewUser] FOREIGN KEY ([RatingReviewUserId]) REFERENCES [review].[tRatingReviewUser] ([RatingReviewUserId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [review].[tRatingReviewUser]'
GO
ALTER TABLE [review].[tRatingReviewUser] ADD
CONSTRAINT [FK_tRatingReviewUser_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
