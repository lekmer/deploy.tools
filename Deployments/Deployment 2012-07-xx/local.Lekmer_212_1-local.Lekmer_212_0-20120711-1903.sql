/*
Run this script on:

(local).Lekmer_212_0    -  This database will be modified

to synchronize it with:

(local).Lekmer_212_1

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 9.0.0 from Red Gate Software Ltd at 11.07.2012 19:03:11

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Add 3 rows to [productlek].[tProductEventStatus]
INSERT INTO [productlek].[tProductEventStatus] ([ProductEventStatusId], [Title], [CommonName]) VALUES (0, N'InQueue', N'InQueue')
INSERT INTO [productlek].[tProductEventStatus] ([ProductEventStatusId], [Title], [CommonName]) VALUES (1, N'InProgress', N'InProgress')
INSERT INTO [productlek].[tProductEventStatus] ([ProductEventStatusId], [Title], [CommonName]) VALUES (2, N'ActionApplied', N'ActionApplied')
COMMIT TRANSACTION
GO
