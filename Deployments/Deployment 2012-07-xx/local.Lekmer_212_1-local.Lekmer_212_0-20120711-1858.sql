/*
Run this script on:

        (local).Lekmer_212_0    -  This database will be modified

to synchronize it with:

        (local).Lekmer_212_1

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 11.07.2012 18:58:45

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [productlek].[tProductChangeEvent]'
GO
CREATE TABLE [productlek].[tProductChangeEvent]
(
[ProductChangeEventId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[EventStatusId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ActionAppliedDate] [datetime] NULL,
[Reference] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductChangeEvent] on [productlek].[tProductChangeEvent]'
GO
ALTER TABLE [productlek].[tProductChangeEvent] ADD CONSTRAINT [PK_tProductChangeEvent] PRIMARY KEY CLUSTERED  ([ProductChangeEventId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vProductChangeEvent]'
GO

CREATE VIEW [productlek].[vProductChangeEvent]
AS
SELECT     
	[pce].[ProductChangeEventId] AS 'ProductChangeEvent.ProductChangeEventId' ,
	[pce].[ProductId] AS 'ProductChangeEvent.ProductId' ,
	[pce].[EventStatusId] AS 'ProductChangeEvent.EventStatusId' ,
	[pce].[CreatedDate] AS 'ProductChangeEvent.CreatedDate' ,
	[pce].[ActionAppliedDate] AS 'ProductChangeEvent.ActionAppliedDate' ,
	[pce].[Reference] AS 'ProductChangeEvent.Reference'
FROM
	[productlek].[tProductChangeEvent] pce
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeEventInsert]'
GO

CREATE PROCEDURE [productlek].[pProductChangeEventInsert]
	@ProductId INT,
	@EventStatusId INT,
	@CreatedDate DATETIME,
	@ActionAppliedDate DATETIME = NULL,
	@Reference NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON

	INSERT [productlek].[tProductChangeEvent]
	(
		[ProductId],
		[EventStatusId],
		[CreatedDate],
		[ActionAppliedDate],
		[Reference]
	)
	VALUES
	(
		@ProductId,
		@EventStatusId,
		@CreatedDate,
		@ActionAppliedDate,
		@Reference
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeEventGetAllInQueue]'
GO
CREATE PROCEDURE [productlek].[pProductChangeEventGetAllInQueue]
	@NumberOfItems INT,
	@EventStatusId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT TOP (@NumberOfItems)
		[pce].*
	FROM
		[productlek].[vProductChangeEvent] pce
	WHERE
		[pce].[ProductChangeEvent.EventStatusId] = @EventStatusId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeEventSetStatusByIdList]'
GO

CREATE PROCEDURE [productlek].[pProductChangeEventSetStatusByIdList]
	@ProductChangeEventIds VARCHAR(MAX) ,
	@Delimiter CHAR(1) ,
	@EventStatusId INT ,
	@ActionAppliedDate DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@ProductChangeEventIds, @Delimiter)

	IF ( @ActionAppliedDate IS NULL ) 
	BEGIN
		UPDATE
			pce
		SET	
			[EventStatusId] = @EventStatusId
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[EventStatusId] != @EventStatusId
	END
	ELSE
	BEGIN
		UPDATE
			pce
		SET	
			[EventStatusId] = @EventStatusId,
			[ActionAppliedDate] = @ActionAppliedDate
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[EventStatusId] != @EventStatusId
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeEventDeleteExpiredItems]'
GO

CREATE PROCEDURE [productlek].[pProductChangeEventDeleteExpiredItems]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ExpirationDate DATETIME
	SET @ExpirationDate = DATEADD(DAY, -2, GETDATE())

	DELETE 
		pce
	FROM
		[productlek].[tProductChangeEvent] pce
	WHERE 
		pce.[EventStatusId] = 2 --ActionApplied
		AND
		pce.[ActionAppliedDate] < @ExpirationDate
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tProductEventStatus]'
GO
CREATE TABLE [productlek].[tProductEventStatus]
(
[ProductEventStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductEventStatus] on [productlek].[tProductEventStatus]'
GO
ALTER TABLE [productlek].[tProductEventStatus] ADD CONSTRAINT [PK_tProductEventStatus] PRIMARY KEY CLUSTERED  ([ProductEventStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tProductChangeEvent]'
GO
ALTER TABLE [productlek].[tProductChangeEvent] ADD
CONSTRAINT [FK_tProductChangeEvent_tProductEventStatus] FOREIGN KEY ([EventStatusId]) REFERENCES [productlek].[tProductEventStatus] ([ProductEventStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
