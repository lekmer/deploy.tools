/*
Run this script on:

        (local).Lekmer_212_0    -  This database will be modified

to synchronize it with:

        (local).Lekmer_212_1

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 09.07.2012 12:13:57

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [productlek]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tBlockProductSearchEsalesResult]'
GO
CREATE TABLE [productlek].[tBlockProductSearchEsalesResult]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NULL,
[RowCount] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockProductSearchEsalesResult] on [productlek].[tBlockProductSearchEsalesResult]'
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] ADD CONSTRAINT [PK_tBlockProductSearchEsalesResult] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vBlockProductSearchEsalesResult]'
GO

CREATE VIEW [productlek].[vBlockProductSearchEsalesResult]
AS
	SELECT
		[BlockId] 'BlockProductSearchEsalesResult.BlockId' ,
		[ColumnCount] 'BlockProductSearchEsalesResult.ColumnCount' ,
		[RowCount] 'BlockProductSearchEsalesResult.RowCount'
	FROM
		[productlek].[tBlockProductSearchEsalesResult]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchEsalesResultSave]'
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultSave]
	@BlockId INT ,
	@ColumnCount INT ,
	@RowCount INT
AS 
BEGIN

	SET NOCOUNT ON

	UPDATE
		[productlek].[tBlockProductSearchEsalesResult]
	SET	
		[ColumnCount] = @ColumnCount ,
		[RowCount] = @RowCount
	WHERE
		[BlockId] = @BlockId
	
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[productlek].[tBlockProductSearchEsalesResult]
			( [BlockId] ,
			  [ColumnCount] ,
			  [RowCount]
			)
		VALUES
			( @BlockId ,
			  @ColumnCount ,
			  @RowCount
			)
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchEsalesResultDelete]'
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultDelete]
	@BlockId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE
		[productlek].[tBlockProductSearchEsalesResult]
	WHERE
		[BlockId] = @BlockId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pTagGetAllIdsByCategory]'
GO
CREATE PROCEDURE [lekmer].[pTagGetAllIdsByCategory]
	@CategoryId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		DISTINCT [pt].[TagId]
	FROM
		[product].[tProduct] p
		INNER JOIN [lekmer].[tProductTag] pt ON [p].[ProductId] = [pt].[ProductId]
	WHERE
		[p].[CategoryId] = @CategoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchEsalesResultGetByIdSecure]'
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		ba.* ,
		b.*
	FROM
		[productlek].[vBlockProductSearchEsalesResult] AS ba
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON ba.[BlockProductSearchEsalesResult.BlockId] = b.[Block.BlockId]
	WHERE
		ba.[BlockProductSearchEsalesResult.BlockId] = @BlockId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchEsalesResultGetById]'
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultGetById]
	@LanguageId INT ,
	@BlockId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		ba.* ,
		b.*
	FROM
		[productlek].[vBlockProductSearchEsalesResult] AS ba
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON ba.[BlockProductSearchEsalesResult.BlockId] = b.[Block.BlockId]
	WHERE
		ba.[BlockProductSearchEsalesResult.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tBlockProductSearchEsalesResult]'
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] ADD
CONSTRAINT [FK_tBlockProductSearchEsalesResult_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
