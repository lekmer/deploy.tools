/*
Run this script on:

        (local).Lekmer_212_GRom    -  This database will be modified

to synchronize it with:

        (local).Lekmer_212_1

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 20.07.2012 15:34:27

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductDiscountAction]'
GO
ALTER TABLE [lekmer].[tProductDiscountAction] DROP
CONSTRAINT [FK_tProductDiscountAction_tCurrency]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductDiscountActionItem]'
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] DROP
CONSTRAINT [FK_tProductDiscountActionItem_tProductDiscountAction],
CONSTRAINT [FK_tProductDiscountActionItem_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tProductDiscountActionItem]'
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] DROP CONSTRAINT [PK_tProductDiscountActionItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tProductDiscountAction]'
GO
ALTER TABLE [lekmer].[tProductDiscountAction] DROP
COLUMN [CurrencyId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tProductDiscountActionItem]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tProductDiscountActionItem]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL CONSTRAINT [DF_tProductDiscountActionItem_CurrencyId] DEFAULT ((1)),
[DiscountPrice] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tProductDiscountActionItem]([ProductActionId], [ProductId], [CurrencyId], [DiscountPrice])
SELECT pdai.[ProductActionId], pdai.[ProductId], pda.[CurrencyId], pdai.[DiscountPrice] 
FROM [lekmer].[tProductDiscountActionItem] pdai
LEFT OUTER JOIN [lekmer].[tProductDiscountAction] pda ON [pdai].[ProductActionId] = [pda].[ProductActionId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tProductDiscountActionItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tProductDiscountActionItem]', N'tProductDiscountActionItem'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductDiscountActionItem] on [lekmer].[tProductDiscountActionItem]'
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] ADD CONSTRAINT [PK_tProductDiscountActionItem] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductDiscountActionItem]'
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] ADD
CONSTRAINT [FK_tProductDiscountActionItem_tProductDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [lekmer].[tProductDiscountAction] ([ProductActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tProductDiscountActionItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE,
CONSTRAINT [FK_tProductDiscountActionItem_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO