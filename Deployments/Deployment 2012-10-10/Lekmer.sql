SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [productlek].[tProductChangeEvent]'
GO
ALTER TABLE [productlek].[tProductChangeEvent] DROP CONSTRAINT[FK_tProductChangeEvent_tProductEventStatus]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [productlek].[tProductChangeEvent]'
GO
ALTER TABLE [productlek].[tProductChangeEvent] DROP CONSTRAINT [PK_tProductChangeEvent]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD
[NeedSendInsuranceInfo] [bit] NOT NULL CONSTRAINT [DF_tLekmerOrder_NeedSendInsuranceInfo] DEFAULT ((1))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tCdonExportRestrictionCategory]'
GO
CREATE TABLE [export].[tCdonExportRestrictionCategory]
(
[ProductRegistryId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[RestrictionReason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCdonExportRestrictionCategory] on [export].[tCdonExportRestrictionCategory]'
GO
ALTER TABLE [export].[tCdonExportRestrictionCategory] ADD CONSTRAINT [PK_tCdonExportRestrictionCategory] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tCdonExportRestrictionBrand]'
GO
CREATE TABLE [export].[tCdonExportRestrictionBrand]
(
[ProductRegistryId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[RestrictionReason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCdonExportRestrictionBrand] on [export].[tCdonExportRestrictionBrand]'
GO
ALTER TABLE [export].[tCdonExportRestrictionBrand] ADD CONSTRAINT [PK_tCdonExportRestrictionBrand] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [productlek].[tProductChangeEvent]'
GO
CREATE TABLE [productlek].[tmp_rg_xx_tProductChangeEvent]
(
[ProductChangeEventId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[EventStatusId] [int] NOT NULL,
[CdonExportEventStatusId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ActionAppliedDate] [datetime] NULL,
[Reference] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [productlek].[tmp_rg_xx_tProductChangeEvent] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [productlek].[tmp_rg_xx_tProductChangeEvent]([ProductChangeEventId], [ProductId], [EventStatusId], [CreatedDate], [ActionAppliedDate], [Reference]) SELECT [ProductChangeEventId], [ProductId], [EventStatusId], [CreatedDate], [ActionAppliedDate], [Reference] FROM [productlek].[tProductChangeEvent]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [productlek].[tmp_rg_xx_tProductChangeEvent] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[productlek].[tProductChangeEvent]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[productlek].[tmp_rg_xx_tProductChangeEvent]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [productlek].[tProductChangeEvent]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[productlek].[tmp_rg_xx_tProductChangeEvent]', N'tProductChangeEvent'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductChangeEvent] on [productlek].[tProductChangeEvent]'
GO
ALTER TABLE [productlek].[tProductChangeEvent] ADD CONSTRAINT [PK_tProductChangeEvent] PRIMARY KEY CLUSTERED  ([ProductChangeEventId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[tCdonExportRestrictionProduct]'
GO
CREATE TABLE [export].[tCdonExportRestrictionProduct]
(
[ProductRegistryId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[RestrictionReason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCdonExportRestrictionProduct] on [export].[tCdonExportRestrictionProduct]'
GO
ALTER TABLE [export].[tCdonExportRestrictionProduct] ADD CONSTRAINT [PK_tCdonExportRestrictionProduct] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductSearch]'
GO


ALTER PROCEDURE [product].[pProductSearch]
	@BrandId int,
	@CategoryId int,
	@Title nvarchar(256),
	@PriceFrom DECIMAL(16,2),
	@PriceTo DECIMAL(16,2),
	@ErpId varchar(max),
	@StatusId int,
	@EanCode varchar(20),
	@ChannelId	int,
	@Page int = NULL,
	@PageSize int,
	@SortBy varchar(20) = NULL,
	@SortDescending bit = NULL
AS
BEGIN
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sqlGeneral nvarchar(max)
	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

	DECLARE @CurrencyId int
	DECLARE @ProductRegistryId int

	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	SELECT @ProductRegistryId = ProductRegistryId FROM product.tProductModuleChannel WHERE ChannelId = @ChannelId

	SET @sqlFragment = '
				SELECT ROW_NUMBER() OVER (ORDER BY p.[' 
		+ COALESCE(@SortBy, 'ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
					p.ProductId,
					pli.*
				FROM
					product.tProduct AS p
					-- get price for current channel
					LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = p.[ProductId] AND PRP.ProductRegistryId = @ProductRegistryId
					LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
					LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[ProductId]
						AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, p.[ProductId], PMC.PriceListRegistryId, NULL)
						
					INNER JOIN lekmer.tLekmerProduct AS LP ON p.ProductId = LP.ProductId
				WHERE
					p.IsDeleted = 0'
		
		+ CASE WHEN (@CategoryId IS NOT NULL) THEN + '
					AND p.[CategoryId] IN (SELECT * FROM product.fnGetSubCategories(@CategoryId))' ELSE '' END 
				
		+ CASE WHEN (@Title IS NOT NULL) THEN + '
					AND CONTAINS(p.*, @Title)' ELSE '' END
		
		+ CASE WHEN (@PriceFrom IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] >= @PriceFrom' ELSE '' END
		
		+ CASE WHEN (@PriceTo IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] <= @PriceTo' ELSE '' END
		
		+ CASE WHEN (@ErpId IS NOT NULL) THEN + '
					AND p.[ErpId] IN (SELECT * FROM generic.fnConvertIDListToTableString(@ErpId,'',''))' ELSE '' END
		
		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
					AND p.[ProductStatusId] = @StatusId' ELSE '' END
		
		+ CASE WHEN (@EanCode IS NOT NULL) THEN + '
					AND p.[EanCode] = @EanCode' ELSE '' END
		+ CASE WHEN (@BrandId IS NOT NULL) THEN + '
					AND LP.[BrandId] = @BrandId' ELSE '' END

	SET @sqlGeneral = '
		IF (@Title IS NOT NULL)
		BEGIN
			SET @Title = generic.fPrepareFulltextSearchParameter(@Title)
		END'
		
	SET @sql = @sqlGeneral + '
		SELECT
			p.*,
			vp.*
		FROM
			(' + @sqlFragment + '
			) AS p
			INNER JOIN product.vCustomProductSecure vp
				on p.ProductId = vp.[Product.Id]'


	IF @Page != 0 AND @Page IS NOT NULL
	BEGIN
		SET @sql = @sql + '
		WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
	END
	
	SET @sqlCount = @sqlGeneral + '
		SELECT COUNT(1) FROM
		(' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	
			@BrandId int,
			@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@StatusId int,
			@EanCode varchar(20),
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@BrandId,
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode,
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId

	EXEC sp_executesql @sql, 
		N'	
			@BrandId int,
			@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@StatusId int,
			@EanCode varchar(20),
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@BrandId,
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode,
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionProductGetAll]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionProductGetAll]
AS
BEGIN
	SELECT [rp].*, [pmc].[ChannelId]
	FROM [export].[tCdonExportRestrictionProduct] rp
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rp].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAllByBlockAndProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductGetIdAllByBlockAndProduct]
	@ChannelId		INT,
	@CustomerId		INT,
	@BlockId		INT,
	@ProductId		INT,
	@ShowVariants	BIT,
	@Page			INT = NULL,
	@PageSize		INT,
	@SortBy			VARCHAR(50) = NULL,
	@SortDescending BIT = NULL
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @RelationListVariantTypeId INT
	SET @RelationListVariantTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'Variant')
		
	DECLARE @maxCount INT
	SET @maxCount = 2000

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sql = '
	DECLARE @tResult TABLE (ProductId INT, SortBy NVARCHAR(MAX))	

	INSERT @tResult	(ProductId, SortBy)
	SELECT DISTINCT TOP (@maxCount) p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN [product].[tBlockProductRelationListItem] bprli ON rl.RelationListTypeId = bprli.RelationListTypeId
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		prl.ProductId = @ProductId
		AND rlp.ProductId <> @ProductId
		' + CASE WHEN (@ShowVariants = 0) 
				THEN ' AND rl.RelationListTypeId <> @RelationListVariantTypeId'
				ELSE ' '
			END + '
		AND p.[Product.ChannelId] = @ChannelId
		AND bprli.BlockId = @BlockId

	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP (@Page * @PageSize) *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > (@Page - 1) * @PageSize
			AND Number <= @Page * @PageSize'
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP @maxCount ProductId FROM @tResult AS SearchResult'
	END

	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT, 
			@CustomerId	INT,
			@BlockId	INT,
			@ProductId	INT,
			@ShowVariants	BIT,
			@Page		INT,
			@PageSize	INT,
			@RelationListVariantTypeId	INT,
			@maxCount	INT',
			@ChannelId,
			@CustomerId,
			@BlockId,
			@ProductId,
			@ShowVariants,
			@Page,
			@PageSize,
			@RelationListVariantTypeId,
			@maxCount
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionCategoryGetAll]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionCategoryGetAll]
AS
BEGIN
	SELECT 
		DISTINCT [src].[CategoryId],
		[rc].[ProductRegistryId],
		[rc].[RestrictionReason],
		[rc].[UserId],
		[rc].[UserId],
		[rc].[CreatedDate],
		[pmc].[ChannelId]
	FROM [export].[tCdonExportRestrictionCategory] rc
		 CROSS APPLY [product].[fnGetSubCategories] ([rc].[CategoryId]) src
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pProductChangeEventInsert]'
GO

ALTER PROCEDURE [productlek].[pProductChangeEventInsert]
	@ProductId INT,
	@EventStatusId INT,
	@CdonExportEventStatusId INT,
	@CreatedDate DATETIME,
	@ActionAppliedDate DATETIME = NULL,
	@Reference NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON

	INSERT [productlek].[tProductChangeEvent]
	(
		[ProductId],
		[EventStatusId],
		[CdonExportEventStatusId],
		[CreatedDate],
		[ActionAppliedDate],
		[Reference]
	)
	VALUES
	(
		@ProductId,
		@EventStatusId,
		@CdonExportEventStatusId,
		@CreatedDate,
		@ActionAppliedDate,
		@Reference
	)

	RETURN SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pLekmerOrderUpdateSendInsuranceInfoFlag]'
GO
CREATE PROCEDURE [lekmer].[pLekmerOrderUpdateSendInsuranceInfoFlag]
	@OrderIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE 
		o
	SET 
		[o].[NeedSendInsuranceInfo] = 0
	FROM 
		[lekmer].[tLekmerOrder] o
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@OrderIds, @Delimiter) AS ol ON [ol].[Id] = [o].[OrderId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pVariationGroupGetAll]'
GO
ALTER PROCEDURE [product].[pVariationGroupGetAll]
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = NULL,
	@SortDescending BIT = NULL
AS 
BEGIN
	SET NOCOUNT ON
	
	IF ( generic.fnValidateColumnName(@SortBy) = 0 ) 
    BEGIN
        RAISERROR ( N'Illegal characters in string (parameter @SortBy): %s' , 16 , 1 , @SortBy ) ;
        RETURN
    END

    DECLARE @sql NVARCHAR(MAX)
    DECLARE @sqlCount NVARCHAR(MAX)
    DECLARE @sqlFragment NVARCHAR(MAX)

    SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY ' 
			+ COALESCE(@SortBy, 'vg.[VariationGroup.Id]') 
			+ CASE WHEN ( @SortDescending = 1 ) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			vg.*
		FROM [product].[vCustomVariationGroup] vg '
		
    SET @sql = 'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'
		
    IF @Page != 0 AND @Page IS NOT NULL 
    BEGIN
        SET @sql = @sql + '
		WHERE Number > ' + CAST(( @Page - 1 ) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
    END
	
    SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
		
    EXEC sp_executesql @sqlCount
    EXEC sp_executesql @sql
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrder]'
GO
ALTER VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		lo.[PaymentCost] AS [Lekmer.PaymentCost],
		lo.[VoucherDiscount] AS [Lekmer.VoucherDiscount],
		lo.[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey],
		lo.[FeedbackToken] AS [Lekmer.FeedbackToken],
		lo.[AlternateAddressId] AS [Lekmer.AlternateAddressId],
		[lo].[NeedSendInsuranceInfo] AS [Lekmer.NeedSendInsuranceInfo]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[vProductChangeEvent]'
GO


ALTER VIEW [productlek].[vProductChangeEvent]
AS
SELECT     
	[pce].[ProductChangeEventId] AS 'ProductChangeEvent.ProductChangeEventId' ,
	[pce].[ProductId] AS 'ProductChangeEvent.ProductId' ,
	[pce].[EventStatusId] AS 'ProductChangeEvent.EventStatusId' ,
	[pce].[CdonExportEventStatusId] AS 'ProductChangeEvent.CdonExportEventStatusId' ,
	[pce].[CreatedDate] AS 'ProductChangeEvent.CreatedDate' ,
	[pce].[ActionAppliedDate] AS 'ProductChangeEvent.ActionAppliedDate' ,
	[pce].[Reference] AS 'ProductChangeEvent.Reference'
FROM
	[productlek].[tProductChangeEvent] pce

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeCdonExportEventGetAllInQueue]'
GO
CREATE PROCEDURE [productlek].[pProductChangeCdonExportEventGetAllInQueue]
	@NumberOfItems INT,
	@CdonExportEventStatusId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT TOP (@NumberOfItems)
		[pce].*
	FROM
		[productlek].[vProductChangeEvent] pce
	WHERE
		[pce].[ProductChangeEvent.CdonExportEventStatusId] = @CdonExportEventStatusId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportRestrictionBrandGetAll]'
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionBrandGetAll]
AS
BEGIN
	SELECT [rb].*, [pmc].[ChannelId]
	FROM [export].[tCdonExportRestrictionBrand] rb
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rb].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductGetIdAllByBlockAndProduct]'
GO
ALTER PROCEDURE [product].[pProductGetIdAllByBlockAndProduct]
		@ChannelId int,
		@CustomerId int,
		@BlockId int,
		@ProductId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(50) = NULL,
		@SortDescending bit = NULL
AS
BEGIN
	DECLARE @maxCount INT
	SET @maxCount = 2000

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sql = '
	DECLARE @tResult TABLE (ProductId INT, SortBy NVARCHAR(MAX))

	INSERT @tResult	(ProductId, SortBy)
	SELECT DISTINCT TOP (@maxCount) p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN [product].[tBlockProductRelationListItem] bprli ON rl.RelationListTypeId = bprli.RelationListTypeId
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		prl.ProductId = @ProductId
		AND p.[Product.ChannelId] = @ChannelId
		AND bprli.BlockId = @BlockId


	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP (@Page * @PageSize) *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > (@Page - 1) * @PageSize
			AND Number <= @Page * @PageSize'
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP @maxCount ProductId FROM @tResult AS SearchResult'
	END
			
	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT, 
			@CustomerId	INT,
			@BlockId	INT,
			@ProductId	INT,
			@Page		INT,
			@PageSize	INT,
			@maxCount	INT',
			@ChannelId,
			@CustomerId,
			@BlockId,
			@ProductId,
			@Page,
			@PageSize,
			@maxCount
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductChangeEventSetCdonExportStatusByIdList]'
GO

CREATE PROCEDURE [productlek].[pProductChangeEventSetCdonExportStatusByIdList]
	@ProductChangeEventIds VARCHAR(MAX) ,
	@Delimiter CHAR(1) ,
	@CdonExportEventStatusId INT ,
	@ActionAppliedDate DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@ProductChangeEventIds, @Delimiter)

	IF ( @ActionAppliedDate IS NULL ) 
	BEGIN
		UPDATE
			pce
		SET	
			[CdonExportEventStatusId] = @CdonExportEventStatusId
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[CdonExportEventStatusId] != @CdonExportEventStatusId
	END
	ELSE
	BEGIN
		UPDATE
			pce
		SET	
			[CdonExportEventStatusId] = @CdonExportEventStatusId,
			[ActionAppliedDate] = @ActionAppliedDate
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[CdonExportEventStatusId] != @CdonExportEventStatusId
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pLekmerOrderGetToSendInsuranceInfo]'
GO
CREATE PROCEDURE [lekmer].[pLekmerOrderGetToSendInsuranceInfo]
	@ChannelId				INT,
	@MaxDaysAfterPurchase	INT,
	@MinDaysAfterPurchase	INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CurrentDate DATETIME
	DECLARE @MaxDate DATETIME
	DECLARE @MinDate DATETIME

	SET @CurrentDate = CONVERT (DATE, GETDATE())
	SET @MaxDate = (SELECT DATEADD(DAY, @MaxDaysAfterPurchase * -1, @CurrentDate))
	SET @MinDate = (SELECT DATEADD(DAY, @MinDaysAfterPurchase * -1, @CurrentDate))

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.ChannelId] = @ChannelId
		AND [Lekmer.NeedSendInsuranceInfo] = 1
		AND [OrderStatus.CommonName] = 'OrderInHY'
		AND [Order.CreatedDate] >= @MaxDate
		AND [Order.CreatedDate] < @MinDate
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pTrackProductChangesLekmer]'
GO

ALTER PROCEDURE [integration].[pTrackProductChangesLekmer]
AS 
BEGIN
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tProductChanges') IS NOT NULL 
		DROP TABLE #tProductChanges
	
	CREATE TABLE #tProductChanges (
		[ProductId] INT NOT NULL
		CONSTRAINT PK_#tProductChanges PRIMARY KEY ( [ProductId] )
	)

	INSERT	#tProductChanges ( [ProductId] )
			SELECT DISTINCT
				[lp].[ProductId]
			FROM
				[integration].[tempProduct] tp
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([tp].[HYarticleId], 5, 12)
		UNION
			SELECT DISTINCT
				[lp].[ProductId]
			FROM
				[integration].[tempProductPrice] tpp
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = SUBSTRING([tpp].[HYarticleId], 5, 12)
	
	IF (SELECT COUNT(*) FROM [#tProductChanges]) < 10000
	BEGIN
		INSERT [productlek].[tProductChangeEvent] ( [ProductId], [EventStatusId], [CdonExportEventStatusId], [CreatedDate], [ActionAppliedDate], [Reference] )
		SELECT [ProductId], 0, 0, GETDATE(), NULL, 'HY import' FROM [#tProductChanges]
	END
	
	DROP TABLE #tProductChanges
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandGetAllByBlock]'
GO
ALTER PROCEDURE [lekmer].[pBrandGetAllByBlock]
	@ChannelId		INT,
	@BlockId		INT,
	@Page			INT = NULL,
	@PageSize		INT
AS
BEGIN
	SET NOCOUNT ON
		
	DECLARE @sql		 NVARCHAR(MAX)
	DECLARE @sqlCount	 NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)
	
	DECLARE @blockIncludeAllBrands BIT
	SELECT 
		@blockIncludeAllBrands = IncludeAllBrands 
	FROM 
		lekmer.tBlockBrandList
	WHERE 
		BlockId = @BlockId
	
	IF (@blockIncludeAllBrands = 1)
		BEGIN
			SET @sqlFragment = '
			(
				SELECT 
					ROW_NUMBER() OVER (ORDER BY cast(lower([b].[Brand.Title]) AS binary) ASC) AS Number,
					[b].*
				FROM 
					[lekmer].[vBrand] b
				WHERE 
					[b].[Brand.BrandId] NOT IN (
													SELECT [prrb].[BrandId] 
													FROM [lekmer].[tProductRegistryRestrictionBrand] prrb
													INNER JOIN [product].[tProductModuleChannel] pmc 
														ON pmc.ProductRegistryId = prrb.ProductRegistryId
														   AND pmc.ChannelId = @ChannelId
												)
					AND [b].[ChannelId] = @ChannelId
			)'
		END
	ELSE
		BEGIN
			SET @sqlFragment = '
			(
				SELECT 
					ROW_NUMBER() OVER (ORDER BY [bb].[Ordinal] ASC) AS Number,
					[b].*
				FROM 
					[lekmer].[tBlockBrandListBrand] bb
					INNER JOIN [lekmer].[vBrand] b ON [bb].[BrandId] = [b].[Brand.BrandId]
				WHERE
					[b].[Brand.BrandId] NOT IN (
													SELECT [prrb].[BrandId] 
													FROM [lekmer].[tProductRegistryRestrictionBrand] prrb
													INNER JOIN [product].[tProductModuleChannel] pmc 
														ON pmc.ProductRegistryId = prrb.ProductRegistryId
														   AND pmc.ChannelId = @ChannelId
												)
					AND [bb].[BlockId] = @BlockId
					AND [b].[ChannelId] = @ChannelId
			)'
		END

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
	WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId	INT, 
			@BlockId	INT,
			@Page		INT,
			@PageSize	INT',
			@ChannelId,
			@BlockId,
			@Page,
			@PageSize

	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT, 
			@BlockId	INT,
			@Page		INT,
			@PageSize	INT',
			@ChannelId,
			@BlockId,
			@Page,
			@PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAllByBrandIdList]'
GO
ALTER PROCEDURE [lekmer].[pProductGetIdAllByBrandIdList]
	@ChannelId		INT,
	@CustomerId		INT,
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1),
	@Page			INT = NULL,
	@PageSize		INT = 1,
	@SortBy			VARCHAR(50) = NULL,
	@SortDescending BIT = NULL
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @maxCount INT
	SET @maxCount = 2000
	
	DECLARE @sql NVARCHAR(MAX)
	
	IF (generic.fnValidateColumnName(@SortBy) = 0)
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	SET @sql = '
	DECLARE @tResult TABLE (ProductId INT, SortBy NVARCHAR(MAX))
	
	INSERT @tResult	(ProductId, SortBy)
	SELECT TOP (@maxCount) p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	FROM [product].[vCustomProduct] p
	INNER JOIN product.vCustomPriceListItem AS pli
		ON pli.[Price.ProductId] = P.[Product.Id]
		AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
			p.[Product.CurrencyId],
			P.[Product.Id],
			p.[Product.PriceListRegistryId],
			@CustomerId
		)
	WHERE
		p.[Product.ChannelId] = @ChannelId
		AND
		EXISTS (
			SELECT * FROM [generic].[fnConvertIDListToTableWithOrdinal](@BrandIds, @Delimiter) AS pl
			WHERE pl.Id = p.[Lekmer.BrandId]
		)

	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP (@Page * @PageSize) *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > (@Page - 1) * @PageSize
			AND Number <= @Page * @PageSize'
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP @maxCount ProductId FROM @tResult AS SearchResult'
	END
			
	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT, 
			@CustomerId	INT,
			@BrandIds	VARCHAR(MAX),
			@Delimiter	CHAR(1),
			@Page		INT,
			@PageSize	INT,
			@maxCount	INT',
			@ChannelId,
			@CustomerId,
			@BrandIds,
			@Delimiter,
			@Page,
			@PageSize,
			@maxCount
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pProductChangeEventDeleteExpiredItems]'
GO

ALTER PROCEDURE [productlek].[pProductChangeEventDeleteExpiredItems]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ExpirationDate DATETIME
	SET @ExpirationDate = DATEADD(DAY, -2, GETDATE())

	DELETE 
		pce
	FROM
		[productlek].[tProductChangeEvent] pce
	WHERE 
		pce.[EventStatusId] = 2 --ActionApplied
		AND
		[pce].[CdonExportEventStatusId] = 2 --ActionApplied
		AND
		pce.[ActionAppliedDate] < @ExpirationDate
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]'
GO
CREATE PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	PRINT @LanguageId
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	PRINT @CurrencyId
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	PRINT @PriceListRegistryId
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	PRINT @SiteStructureRegistryId
	
	SELECT
		[p].[ProductId] 'Product.Id', 
		[p].[ItemsInPackage] 'Product.ItemsInPackage',
		[p].[ErpId] 'Product.ErpId',
		[p].[EanCode] 'Product.EanCode',
		[p].[NumberInStock] 'Product.NumberInStock', 
		[p].[CategoryId] 'Product.CategoryId',
		COALESCE([pt].[WebShopTitle], [p].WebShopTitle) 'Product.WebShopTitle',
		COALESCE([pt].[Title], [p].Title) 'Product.Title',
		[p].[ProductStatusId] 'Product.ProductStatusId',
		COALESCE([pt].[Description], [p].[Description]) 'Product.Description',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription]) 'Product.ShortDescription',
		@ChannelId 'Product.ChannelId', 
		@CurrencyId 'Product.CurrencyId',
		@PriceListRegistryId 'Product.PriceListRegistryId',
		[i].*,
		[pssr].[ParentContentNodeId] 'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId] 'Product.TemplateContentNodeId',
		[lp].[Lekmer.BrandId],
		[lp].[Lekmer.IsBookable],
		[lp].[Lekmer.AgeFromMonth],
		[lp].[Lekmer.AgeToMonth],
		[lp].[Lekmer.IsNewFrom],
		[lp].[Lekmer.IsNewTo],
		COALESCE ([lpt].[Product.Measurement], [lp].[Lekmer.Measurement]) 'Lekmer.Measurement',
		[lp].[Lekmer.BatteryTypeId],
		[lp].[Lekmer.NumberOfBatteries],
		[lp].[Lekmer.IsBatteryIncluded],
		[lp].[Lekmer.ExpectedBackInStock],
		[lp].[Lekmer.CreatedDate],
		[lp].[Lekmer.SizeDeviationId],
		[lp].[Lekmer.HasSizes],
		[lp].[Lekmer.LekmerErpId],
		[lp].[Lekmer.ShowVariantRelations],
		[bt].*,
		[sd].*,
		[pu].[ProductUrl.UrlTitle] 'Lekmer.UrlTitle',
		[rp].[Price] 'Lekmer.RecommendedPrice',
		[pli].*
	FROM
		[product].[tProduct] AS p
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON [lpt].[Product.Id] = [lp].[Lekmer.ProductId] AND [lpt].[Language.Id] = @LanguageId
		LEFT JOIN [lekmer].[vBatteryType] bt ON [bt].[BatteryType.Id] = [lp].[Lekmer.BatteryTypeId]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON [sd].[SizeDeviation.Id] = [lp].[Lekmer.SizeDeviationId]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl	ON [pl].[Id] = [p].[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pReviewGetAllByProduct]'
GO
ALTER PROCEDURE [addon].[pReviewGetAllByProduct]
	@ChannelId	INT,
	@ProductId	INT,
	@Page		INT,
	@PageSize	INT
AS
BEGIN
	SET NOCOUNT ON; 

	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

	SET @sqlFragment = '
	SELECT ROW_NUMBER() OVER (ORDER BY r.[Review.CreatedDate] DESC) AS Number,
		r.*
	FROM
		[addon].[vCustomReview] AS r
	WHERE
		r.[Review.ProductId] = @ProductId AND r.[Review.ChannelId] = @ChannelId AND r.[Review.ReviewStatusId] = 2' /*Approved status id*/

	SET @sql = '
	SELECT * FROM
	(' + @sqlFragment + '
	)
	AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
	END

	SET @sqlCount = '
	SELECT COUNT(1) FROM
	(
	' + @sqlFragment + '
	)
	AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'@ChannelId INT,
		  @ProductId INT,
		  @Page      INT,
		  @PageSize  INT',
		  @ChannelId,
		  @ProductId,
		  @Page,
		  @PageSize

	EXEC sp_executesql @sql, 
		N'@ChannelId INT,
		  @ProductId INT,
		  @Page      INT,
		  @PageSize  INT',
		  @ChannelId,
		  @ProductId,
		  @Page,
		  @PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tProductUrl_LanguageId] on [lekmer].[tProductUrl]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductUrl_LanguageId] ON [lekmer].[tProductUrl] ([LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tProductUrl_ProductId] on [lekmer].[tProductUrl]'
GO
CREATE NONCLUSTERED INDEX [IX_tProductUrl_ProductId] ON [lekmer].[tProductUrl] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tLekmerProduct_BrandId] on [lekmer].[tLekmerProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tLekmerProduct_BrandId] ON [lekmer].[tLekmerProduct] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tBlockBrandListBrand_Ordinal] on [lekmer].[tBlockBrandListBrand]'
GO
CREATE NONCLUSTERED INDEX [IX_tBlockBrandListBrand_Ordinal] ON [lekmer].[tBlockBrandListBrand] ([Ordinal])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [export].[tCdonExportRestrictionCategory]'
GO
ALTER TABLE [export].[tCdonExportRestrictionCategory] ADD CONSTRAINT [FK_tCdonExportRestrictionCategory_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
ALTER TABLE [export].[tCdonExportRestrictionCategory] ADD CONSTRAINT [FK_tCdonExportRestrictionCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [export].[tCdonExportRestrictionBrand]'
GO
ALTER TABLE [export].[tCdonExportRestrictionBrand] ADD CONSTRAINT [FK_tCdonExportRestrictionBrand_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
ALTER TABLE [export].[tCdonExportRestrictionBrand] ADD CONSTRAINT [FK_tCdonExportRestrictionBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tProductChangeEvent]'
GO
ALTER TABLE [productlek].[tProductChangeEvent] ADD CONSTRAINT [FK_tProductChangeEvent_tProductEventStatus] FOREIGN KEY ([EventStatusId]) REFERENCES [productlek].[tProductEventStatus] ([ProductEventStatusId])
ALTER TABLE [productlek].[tProductChangeEvent] ADD CONSTRAINT [FK_tProductChangeEvent_tProductEventStatus_CdonExport] FOREIGN KEY ([CdonExportEventStatusId]) REFERENCES [productlek].[tProductEventStatus] ([ProductEventStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [export].[tCdonExportRestrictionProduct]'
GO
ALTER TABLE [export].[tCdonExportRestrictionProduct] ADD CONSTRAINT [FK_tCdonExportRestrictionProduct_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
ALTER TABLE [export].[tCdonExportRestrictionProduct] ADD CONSTRAINT [FK_tCdonExportRestrictionProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
