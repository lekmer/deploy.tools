SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [customerlek].[tAddress]'
GO
ALTER TABLE [customerlek].[tAddress] ADD
[DoorCode] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[tOrderAddress]'
GO
ALTER TABLE [orderlek].[tOrderAddress] ADD
[DoorCode] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customerlek].[vAddress]'
GO

ALTER VIEW [customerlek].[vAddress]
AS
	SELECT
		[AddressId] AS 'Address.AddressId',
		[CustomerId] AS 'Address.CustomerId',
		[HouseNumber] AS 'Address.HouseNumber',
		[HouseExtension] AS 'Address.HouseExtension',
		[Reference] AS 'Address.Reference',
		[DoorCode] AS 'Address.DoorCode'
	FROM
		[customerlek].[tAddress]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomAddress]'
GO

ALTER VIEW [customer].[vCustomAddress]
AS
	SELECT
		a.*,
		la.[Address.HouseNumber],
		la.[Address.HouseExtension],
		la.[Address.Reference],
		la.[Address.DoorCode]
	FROM
		[customer].[vAddress] a
		LEFT OUTER JOIN [customerlek].[vAddress] la 
			ON la.[Address.AddressId] = a.[Address.AddressId]
			AND la.[Address.CustomerId] = a.[Address.CustomerId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomCustomerSecure]'
GO

ALTER VIEW [customer].[vCustomCustomerSecure]
AS
	SELECT
		C.*,
		CI.[CustomerInformation.GenderTypeId],
		CI.[CustomerInformation.IsCompany],
		CI.[CustomerInformation.AlternateAddressId],
		BA.[Address.HouseNumber] AS 'BillingAddress.HouseNumber',
		BA.[Address.HouseExtension] AS 'BillingAddress.HouseExtension',
		BA.[Address.Reference] AS 'BillingAddress.Reference',
		BA.[Address.DoorCode] AS 'BillingAddress.DoorCode',
		DA.[Address.HouseNumber] AS 'DeliveryAddress.HouseNumber',
		DA.[Address.HouseExtension] AS  'DeliveryAddress.HouseExtension',
		DA.[Address.Reference] AS 'DeliveryAddress.Reference',
		DA.[Address.DoorCode] AS 'DeliveryAddress.DoorCode',
		AA.[Address.AddressId] AS 'AlternateAddress.AddressId',
		AA.[Address.CustomerId] AS  'AlternateAddress.CustomerId',
		AA.[Address.AddressTypeId] AS 'AlternateAddress.AddressTypeId',
		AA.[Address.Addressee] AS 'AlternateAddress.Addressee',
		AA.[Address.StreetAddress] AS 'AlternateAddress.StreetAddress',
		AA.[Address.StreetAddress2] AS 'AlternateAddress.StreetAddress2',
		AA.[Address.PostalCode] AS 'AlternateAddress.PostalCode',
		AA.[Address.City]  AS 'AlternateAddress.City',
		AA.[Address.CountryId] AS 'AlternateAddress.CountryId',
		AA.[Address.PhoneNumber] AS 'AlternateAddress.PhoneNumber',
		AA.[Address.HouseNumber] AS 'AlternateAddress.HouseNumber',
		AA.[Address.HouseExtension] AS 'AlternateAddress.HouseExtension',
		AA.[Address.Reference] AS 'AlternateAddress.Reference',
		AA.[Address.DoorCode] AS 'AlternateAddress.DoorCode'
	FROM
		[customer].[vCustomerSecure] C
		LEFT JOIN [customerlek].vCustomerInformation CI ON CI.[CustomerInformation.CustomerId] = C.[CustomerInformation.InformationId]
		LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = C.[CustomerInformation.DefaultBillingAddressId]
		LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = C.[CustomerInformation.DefaultDeliveryAddressId]
		LEFT JOIN [customer].vCustomAddress AA ON AA.[Address.AddressId] = CI.[CustomerInformation.AlternateAddressId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomCustomer]'
GO

ALTER VIEW [customer].[vCustomCustomer]
AS
	SELECT
		C.*,
		CI.[CustomerInformation.GenderTypeId],
		CI.[CustomerInformation.IsCompany],
		CI.[CustomerInformation.AlternateAddressId],
		BA.[Address.HouseNumber] AS 'BillingAddress.HouseNumber',
		BA.[Address.HouseExtension] AS 'BillingAddress.HouseExtension',
		BA.[Address.Reference] AS 'BillingAddress.Reference',
		BA.[Address.DoorCode] AS 'BillingAddress.DoorCode',
		DA.[Address.HouseNumber] AS 'DeliveryAddress.HouseNumber',
		DA.[Address.HouseExtension] AS 'DeliveryAddress.HouseExtension',
		DA.[Address.Reference] AS 'DeliveryAddress.Reference',
		DA.[Address.DoorCode] AS 'DeliveryAddress.DoorCode',
		AA.[Address.AddressId] AS 'AlternateAddress.AddressId',
		AA.[Address.CustomerId] AS  'AlternateAddress.CustomerId',
		AA.[Address.AddressTypeId] AS 'AlternateAddress.AddressTypeId',
		AA.[Address.Addressee] AS 'AlternateAddress.Addressee',
		AA.[Address.StreetAddress] AS 'AlternateAddress.StreetAddress',
		AA.[Address.StreetAddress2] AS 'AlternateAddress.StreetAddress2',
		AA.[Address.PostalCode] AS 'AlternateAddress.PostalCode',
		AA.[Address.City]  AS 'AlternateAddress.City',
		AA.[Address.CountryId] AS 'AlternateAddress.CountryId',
		AA.[Address.PhoneNumber] AS 'AlternateAddress.PhoneNumber',
		AA.[Address.HouseNumber] AS 'AlternateAddress.HouseNumber',
		AA.[Address.HouseExtension] AS 'AlternateAddress.HouseExtension',
		AA.[Address.Reference] AS 'AlternateAddress.Reference',
		AA.[Address.DoorCode] AS 'AlternateAddress.DoorCode'
	FROM
		[customer].[vCustomer] C
		LEFT JOIN [customerlek].vCustomerInformation CI ON CI.[CustomerInformation.CustomerId] = C.[CustomerInformation.InformationId]
		LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = C.[CustomerInformation.DefaultBillingAddressId]
		LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = C.[CustomerInformation.DefaultDeliveryAddressId]
		LEFT JOIN [customer].vCustomAddress AA ON AA.[Address.AddressId] = CI.[CustomerInformation.AlternateAddressId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[vCartValueRangeConditionCurrency]'
GO
ALTER VIEW [campaignlek].[vCartValueRangeConditionCurrency]
AS
	SELECT
		cvrcc.[ConditionId] AS 'CartValueRangeConditionCurrency.ConditionId',
		cvrcc.[CurrencyId] AS 'CartValueRangeConditionCurrency.CurrencyId',
		cvrcc.[MonetaryValueFrom] AS 'CartValueRangeConditionCurrency.MonetaryValueFrom',
		cvrcc.[MonetaryValueTo] AS 'CartValueRangeConditionCurrency.MonetaryValueTo'
	FROM
		[campaignlek].[tCartValueRangeConditionCurrency] cvrcc
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[vOrderAddress]'
GO



ALTER VIEW [orderlek].[vOrderAddress]
AS
	SELECT
		[OrderAddressId] AS 'OrderAddress.OrderAddressId',
		[HouseNumber] AS 'OrderAddress.HouseNumber',
		[HouseExtension] AS 'OrderAddress.HouseExtension',
		[Reference] AS 'OrderAddress.Reference',
		[DoorCode] AS 'OrderAddress.DoorCode'
	FROM
		[orderlek].[tOrderAddress]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrderAddress]'
GO

ALTER VIEW [order].[vCustomOrderAddress]
AS
	SELECT
		oa.*,
		loa.[OrderAddress.HouseNumber],
		loa.[OrderAddress.HouseExtension],
		loa.[OrderAddress.Reference],
		loa.[OrderAddress.DoorCode]
	FROM
		[order].[vOrderAddress] oa
		LEFT OUTER JOIN [orderlek].[vOrderAddress] loa
			ON loa.[OrderAddress.OrderAddressId] = oa.[OrderAddress.OrderAddressId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[pOrderAddressSave]'
GO

ALTER PROCEDURE [orderlek].[pOrderAddressSave]
	@OrderAddressId INT,
	@HouseNumber	NVARCHAR(50),
	@HouseExtension	NVARCHAR(100),
	@Reference		NVARCHAR(50),
	@DoorCode		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[orderlek].[tOrderAddress]
	SET 
		HouseNumber = @HouseNumber,
		HouseExtension = @HouseExtension,
		Reference = @Reference,
		DoorCode = @DoorCode
	WHERE	
		OrderAddressId = @OrderAddressId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [orderlek].[tOrderAddress]
		(
			OrderAddressId,
			HouseNumber,
			HouseExtension,
			Reference,
			DoorCode
		)
		VALUES
		(
			@OrderAddressId,
			@HouseNumber,
			@HouseExtension,
			@Reference,
			@DoorCode
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customerlek].[pAddressSave]'
GO

ALTER PROCEDURE [customerlek].[pAddressSave]
	@AddressId		INT,
	@CustomerId		INT,
	@Addressee		NVARCHAR(100),
	@StreetAddress	NVARCHAR(200),
	@StreetAddress2 NVARCHAR(200),
	@PostalCode		NVARCHAR(10),
	@City			NVARCHAR(100),
	@CountryId		INT,
	@PhoneNumber	NVARCHAR(20),
	@AddressTypeId	INT,
	@HouseNumber	NVARCHAR(50),
	@HouseExtension	NVARCHAR(100),
	@Reference		NVARCHAR(50),
	@DoorCode		NVARCHAR(50)
AS	
BEGIN
	SET NOCOUNT ON
	
	--Check if customer exist
	IF NOT EXISTS ( 
					SELECT 
						1 
					FROM 
						[customer].[tCustomer] 
					WHERE 
						[tCustomer].[CustomerId] = @CustomerId 
					)
		RETURN -1
	
	UPDATE  [customer].[tAddress]
	SET 
		Addressee = @Addressee,
		StreetAddress = @StreetAddress,
		StreetAddress2 = @StreetAddress2,
		PostalCode = @PostalCode,
		City = @City,
		CountryId = @CountryId,
		PhoneNumber = @PhoneNumber,
		AddressTypeId = @AddressTypeId
	WHERE
		AddressId = @AddressId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customer].[tAddress]
		(
			CustomerId,
			Addressee,
			StreetAddress,
			StreetAddress2,
			PostalCode,
			City,
			CountryId,
			PhoneNumber,
			AddressTypeId
		)
		VALUES
		(
			@CustomerId,
			@Addressee,
			@StreetAddress,
			@StreetAddress2,
			@PostalCode,
			@City,
			@CountryId,
			@PhoneNumber,
			@AddressTypeId
		)
		SET	@AddressId = SCOPE_IDENTITY()
	END
	
	UPDATE  [customerlek].[tAddress]
	SET 
		HouseNumber = @HouseNumber,
		HouseExtension = @HouseExtension,
		Reference = @Reference,
		DoorCode = @DoorCode
	WHERE
		AddressId = @AddressId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customerlek].[tAddress]
		(
			AddressId,
			CustomerId,
			HouseNumber,
			HouseExtension,
			Reference,
			DoorCode
		)
		VALUES
		(
			@AddressId,
			@CustomerId,
			@HouseNumber,
			@HouseExtension,
			@Reference,
			@DoorCode
		)
	END

	RETURN @AddressId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pOrder]'
GO

ALTER PROCEDURE [integration].[pOrder]
	@OrderId int
AS
begin
	set nocount on
	set transaction isolation level read uncommitted
	begin try
		
		--set statistics io on
		--set statistics time on
		
		-- Order
		select 		
			o.OrderId,
            o.FreightCost,
            u.OptionalFreightCost,
            o.ChannelId as Channel,   
            z.OrderPaymentId, 
            v.ErpId as PaymentMethod, 
            z.ReferenceId, 
            z.Price,
            --u.VoucherDiscount as VoucherAmount
            op.ItemsActualPriceIncludingVat - z.Price as VoucherAmount,
			lci.IsCompany -- NEW
		from
			[order].tPaymentType v
			inner join [order].tOrderPayment z
				on z.PaymentTypeId = v.PaymentTypeId
			inner join [order].torder o
				on z.OrderId = o.OrderId
			inner join customerlek.tCustomerInformation lci
				on lci.CustomerId = o.CustomerId
			left join lekmer.tlekmerOrder u
				on o.OrderId = u.OrderId
			cross apply (select sum(oi.ActualPriceIncludingVat * oi.Quantity) as 'ItemsActualPriceIncludingVat' from [order].tOrderItem oi where oi.OrderId = o.OrderId) as op	
			where
				o.OrderStatusId = 2 and
				o.OrderId = @OrderId
		
		-- OrderRow // Old
		--select
		--	o.OrderItemId as OrderRowId, 
		--	p.Title, 
		--	o.Quantity, 
		--	p.ProductId,
		--	o.ActualPriceIncludingVat as Price, 
		--	o.OriginalPriceIncludingVat as OrdinaryPrice, 
		--	coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		--from
		--	[order].tOrderItem o
		--	left join [order].tOrderItemProduct p 
		--		on o.OrderItemId = p.OrderItemId
		--	left join lekmer.tOrderItemSize ois
		--		on ois.OrderItemId = o.OrderItemId
		--where
		--		o.OrderId = @OrderId 
				
		-- OrderRow
		select
			o.OrderItemId as OrderRowId, 
			p.Title, 
			o.Quantity, 
			p.ProductId,
			o.ActualPriceIncludingVat as Price, 
			o.OriginalPriceIncludingVat as OrdinaryPrice, 
			coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
			inner join lekmer.tLekmerProduct l
				on p.ProductId = l.ProductId
		where
			o.OrderId = @OrderId 
			and l.ProductTypeId != 2 -- package article
		UNION ALL
		select
			poi.OrderItemId as OrderRowId,
			poip.Title,
			poi.Quantity,
			poip.ProductId,
			poi.PackagePriceIncludingVat as Price,
			poi.OriginalPriceIncludingVat as OrdinaryPrice,
			coalesce(poip.[SizeErpId], poip.ErpId) as HyErpId
		from
			orderlek.tPackageOrderItem poi
			left join orderlek.tPackageOrderItemProduct poip
				on poi.PackageOrderItemId = poip.PackageOrderItemId
		where
			poi.OrderId = @OrderId
			
		-- Customer
		select 		
            c.CustomerId, 
			c.FirstName, 
			c.LastName,
            c.CivicNumber, 
            c.PhoneNumber,
            c.CellPhoneNumber as MobilePhoneNumber, 
            o.Email,
			case g.IsCompany when 1 then '05' else isnull(gt.ErpId, '00') end as Gender --NEW
		from	
			[order].torder o
			inner join [customer].tCustomerInformation c
				on c.CustomerId = o.CustomerId
			left join customerlek.tCustomerInformation g
				on c.CustomerId = g.CustomerId
			left join customerlek.tGenderType gt
				on g.GenderTypeId = gt.GenderTypeId
		where
			o.OrderId = @OrderId
		
		-- BillingAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.BillingAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- DeliveryAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.DeliveryAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId
		
		-- AlternativeAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber,
			la.Reference as Reference,
			la.HouseNumber as HouseNumber,
			la.HouseExtension as HouseExtension,
			la.DoorCode AS DoorCode
		from	
			[order].tOrder o
			inner join [lekmer].tLekmerOrder lo on lo.OrderId = o.OrderId
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = lo.AlternateAddressId
			left outer join [orderlek].tOrderAddress la
				on la.OrderAddressId = a.OrderAddressId
		where
			o.OrderId = @OrderId


		-- DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join [order].tDeliveryMethod d
				on o.DeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId
			
		-- Optional DeliveryType info
		select distinct
			d.DeliveryMethodId,
			d.CommonName as Title,
			d.ErpId,
			d.TrackingUrl,
			dmp.FreightCost
		from
			[order].tOrder o
			inner join core.tChannel c
				on o.ChannelId = c.ChannelId
			inner join lekmer.tLekmerOrder lo
				on lo.OrderId = o.OrderId
			inner join [order].tDeliveryMethod d
				on lo.OptionalDeliveryMethodId = d.DeliveryMethodId
			inner join [order].tDeliveryMethodPrice dmp
				on d.DeliveryMethodId = dmp.DeliveryMethodId
				and c.CurrencyId = dmp.CurrencyId
				and c.CountryId = dmp.CountryId
		where
			o.OrderId = @OrderId

	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
