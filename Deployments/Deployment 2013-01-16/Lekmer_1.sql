SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD
[CivicNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[tRelationList]'
GO
ALTER TABLE [product].[tRelationList] ALTER COLUMN [Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrder]'
GO
ALTER VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		lo.[PaymentCost] AS [Lekmer.PaymentCost],
		lo.[VoucherDiscount] AS [Lekmer.VoucherDiscount],
		lo.[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey],
		lo.[FeedbackToken] AS [Lekmer.FeedbackToken],
		lo.[AlternateAddressId] AS [Lekmer.AlternateAddressId],
		[lo].[NeedSendInsuranceInfo] AS [Lekmer.NeedSendInsuranceInfo],
		[lo].[CivicNumber] AS [Lekmer.CivicNumber]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pCreateProductRelationVariant]'
GO
ALTER PROCEDURE [integration].[pCreateProductRelationVariant]
AS
BEGIN
	SET NOCOUNT ON 
	
	DECLARE @RelationListTypeId INT
	SELECT @RelationListTypeId = RelationListTypeId FROM product.tRelationListType WHERE CommonName = 'Variant'

	DECLARE @VariantsCount INT,
			@RelationListId INT,
			@ProductId INT,
			@ProductErpId VARCHAR(50),
			@VariantErpId VARCHAR(50)

	-- Data to insert into relation tables
	IF OBJECT_ID('tempdb..#tTmpProducts') IS NOT NULL
		DROP TABLE #tTmpProducts
		
	CREATE TABLE #tTmpProducts (			
		ProductId INT,
		ProductErpId VARCHAR(50),
		VariantErpId VARCHAR(50)
	)
	
	INSERT INTO #tTmpProducts
	SELECT ProductId, HYErpId, SUBSTRING(HYErpId, 1, CHARINDEX('-', HYErpId) - 1) FROM [lekmer].[tLekmerProduct] ORDER BY HYErpId


	-- Relation Lists to delete
	IF OBJECT_ID('tempdb..#tTmpRelationListsToDelete') IS NOT NULL
		DROP TABLE #tTmpRelationListsToDelete
		
	CREATE TABLE #tTmpRelationListsToDelete (			
		RelationListId INT
	)
	
	INSERT INTO #tTmpRelationListsToDelete
	SELECT [rl].[RelationListId] FROM product.tRelationList rl
	LEFT JOIN #tTmpProducts tmpP ON [tmpP].[VariantErpId] = [rl].[Title]
	WHERE [rl].[RelationListTypeId] = @RelationListTypeId AND [tmpP].[VariantErpId] IS NULL
	

	-- Delete redundant Relation Lists
	-- Delete all Product - Relation List where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tProductRelationList
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)

	-- Delete all Relation List - Product where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tRelationListProduct
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)

	-- Delete all Relation List where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tRelationList 
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)
	
	
	-- Delete redundant Relations
	DELETE [product].[tRelationListProduct]
	FROM [product].[tRelationListProduct] rlp
	INNER JOIN [product].[tRelationList] rl ON [rl].[RelationListId] = [rlp].[RelationListId]
	LEFT JOIN #tTmpProducts tmpP ON [tmpP].[VariantErpId] = [rl].[Title] AND [tmpP].[ProductId] = [rlp].[ProductId]
	WHERE [tmpP].[VariantErpId] IS NULL AND [tmpP].[ProductId] IS NULL
	
	DELETE [product].[tProductRelationList]
	FROM [product].[tProductRelationList] prl
	INNER JOIN [product].[tRelationList] rl ON [rl].[RelationListId] = [prl].[RelationListId]
	LEFT JOIN #tTmpProducts tmpP ON [tmpP].[VariantErpId] = [rl].[Title] AND [tmpP].[ProductId] = [prl].[ProductId]
	WHERE [tmpP].[VariantErpId] IS NULL AND [tmpP].[ProductId] IS NULL
	

	-- Add new relation lists and relations
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			[ProductId],
			[ProductErpId],
			[VariantErpId]
		FROM
			#tTmpProducts

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@ProductId,
			@ProductErpId,
			@VariantErpId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			-- Check if product has variants			
			SET @VariantsCount = (SELECT COUNT(ProductId) FROM lekmer.tLekmerProduct WHERE HYErpId LIKE @VariantErpId + '%')
			IF @VariantsCount > 1
			BEGIN
				BEGIN TRANSACTION
					SET @RelationListId = ( SELECT RelationListId FROM product.tRelationList rl
											WHERE rl.Title = @VariantErpId AND rl.RelationListTypeId = @RelationListTypeId )

					IF @RelationListId IS NULL
					BEGIN
						EXEC @RelationListId = [product].[pRelationListSave] NULL, @VariantErpId, @RelationListTypeId
					END

					IF NOT EXISTS (SELECT 1 FROM [product].[tProductRelationList] WHERE [ProductId] = @ProductId AND [RelationListId] = @RelationListId)
					BEGIN 
						EXEC [product].[pProductRelationListSave] @ProductId, @RelationListId
					END
						
					IF NOT EXISTS (SELECT 1 FROM [product].[tRelationListProduct] WHERE [ProductId] = @ProductId AND [RelationListId] = @RelationListId)
					BEGIN
						EXEC [product].[pRelationListProductSave] @RelationListId, @ProductId
					END
				COMMIT
			END
		END TRY		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				NULL,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
		END CATCH

		FETCH NEXT FROM cur_product 
			INTO 
				@ProductId,
				@ProductErpId,
				@VariantErpId
	END

	CLOSE cur_product
	DEALLOCATE cur_product
	
	
	-- Delete Relation Lists with <= 1 relations
	DELETE FROM #tTmpRelationListsToDelete
	
	INSERT INTO #tTmpRelationListsToDelete
	SELECT [rl].[RelationListId] FROM [product].[tRelationList] rl
	INNER JOIN [product].[tRelationListProduct] rlp ON [rlp].[RelationListId] = [rl].[RelationListId]
	GROUP BY [rl].[RelationListId]
	HAVING COUNT([rlp].[ProductId]) <= 1
	
	INSERT INTO #tTmpRelationListsToDelete
	SELECT [rl].[RelationListId] FROM [product].[tRelationList] rl
	INNER JOIN [product].[tProductRelationList] prl ON [prl].[RelationListId] = [rl].[RelationListId]
	GROUP BY [rl].[RelationListId]
	HAVING COUNT([prl].[ProductId]) <= 1
	
	
	-- Delete redundant Relation Lists
	-- Delete all Product - Relation List where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tProductRelationList
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)

	-- Delete all Relation List - Product where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tRelationListProduct
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)

	-- Delete all Relation List where Relation List Id in '#tTmpRelationListsToDelete'
	DELETE FROM product.tRelationList 
	WHERE [RelationListId] IN (SELECT [RelationListId] FROM #tTmpRelationListsToDelete)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pOrderSave]'
GO
ALTER PROCEDURE [lekmer].[pOrderSave]
	@OrderId					INT,
	@PaymentCost				DECIMAL(16,2),
	@VoucherDiscount			DECIMAL(16,2),
	@CustomerIdentificationKey	NCHAR(50),
	@AlternateAddressId			INT = NULL,
	@CivicNumber				NVARCHAR(50) = NULL
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		[PaymentCost] = @PaymentCost,
		[VoucherDiscount] = @VoucherDiscount,
		[CustomerIdentificationKey] = @CustomerIdentificationKey,
		[AlternateAddressId] = @AlternateAddressId,
		[CivicNumber] = @CivicNumber
	WHERE 
		[OrderId] = @OrderId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tLekmerOrder]
		(
			[OrderId],
			[PaymentCost],
			[VoucherDiscount],
			[CustomerIdentificationKey],
			[AlternateAddressId],
			[CivicNumber]
		)
		VALUES
		(
			@OrderId,
			@PaymentCost,
			@VoucherDiscount,
			@CustomerIdentificationKey,
			@AlternateAddressId,
			@CivicNumber
		)
	END
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
