/*
Run this script on a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 026\DB\LekmerDB    -  This database will be modified. The scripts folder will not be modified.

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 027\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 24.09.2012 13:20:15

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [lekmer].[tCartItemFixedDiscountAction]'
GO
CREATE TABLE [lekmer].[tCartItemFixedDiscountAction]
(
[CartActionId] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemFixedDiscountAction] on [lekmer].[tCartItemFixedDiscountAction]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountAction] ADD CONSTRAINT [PK_tCartItemFixedDiscountAction] PRIMARY KEY CLUSTERED  ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemFixedDiscountActionCurrency]'
GO
CREATE TABLE [lekmer].[tCartItemFixedDiscountActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValue] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemFixedDiscountActionCurrency] on [lekmer].[tCartItemFixedDiscountActionCurrency]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionCurrency] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemFixedDiscountActionExcludeBrand]'
GO
CREATE TABLE [lekmer].[tCartItemFixedDiscountActionExcludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemFixedDiscountActionExcludeBrand] on [lekmer].[tCartItemFixedDiscountActionExcludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeBrand] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionExcludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemFixedDiscountActionExcludeCategory]'
GO
CREATE TABLE [lekmer].[tCartItemFixedDiscountActionExcludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemFixedDiscountActionExcludeCategory] on [lekmer].[tCartItemFixedDiscountActionExcludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeCategory] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionExcludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemFixedDiscountActionExcludeProduct]'
GO
CREATE TABLE [lekmer].[tCartItemFixedDiscountActionExcludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemFixedDiscountActionExcludeProduct] on [lekmer].[tCartItemFixedDiscountActionExcludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeProduct] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionExcludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemFixedDiscountActionIncludeBrand]'
GO
CREATE TABLE [lekmer].[tCartItemFixedDiscountActionIncludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemFixedDiscountActionIncludeBrand] on [lekmer].[tCartItemFixedDiscountActionIncludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionIncludeBrand] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionIncludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemFixedDiscountActionIncludeCategory]'
GO
CREATE TABLE [lekmer].[tCartItemFixedDiscountActionIncludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemFixedDiscountActionIncludeCategory] on [lekmer].[tCartItemFixedDiscountActionIncludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionIncludeCategory] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionIncludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemFixedDiscountActionIncludeProduct]'
GO
CREATE TABLE [lekmer].[tCartItemFixedDiscountActionIncludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemFixedDiscountActionIncludeProduct] on [lekmer].[tCartItemFixedDiscountActionIncludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionIncludeProduct] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionIncludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionCurrencyDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionCurrencyDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemFixedDiscountActionCurrency]
	WHERE
		CartActionId = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionCurrencyInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionCurrencyInsert]
	@CartActionId	INT,
	@CurrencyId		INT,
	@MonetaryValue	DECIMAL(16,2)
AS
BEGIN
	INSERT [lekmer].[tCartItemFixedDiscountActionCurrency] (
		CartActionId,
		CurrencyId,
		MonetaryValue
	)
	VALUES (
		@CartActionId,
		@CurrencyId,
		@MonetaryValue
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionDelete]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionDelete]
	@CartActionId INT
AS 
BEGIN
	DELETE FROM [lekmer].[tCartItemFixedDiscountAction]
	WHERE CartActionId = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeBrandDeleteAll]'
GO



CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemFixedDiscountActionExcludeBrand]
	WHERE
		CartActionId = @CartActionId
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeBrandGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[lekmer].[tCartItemFixedDiscountActionExcludeBrand]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeBrandInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeBrandInsert]
	@CartActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemFixedDiscountActionExcludeBrand] (
		CartActionId,
		BrandId
	)
	VALUES (
		@CartActionId,
		@BrandId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeCategoryDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeCategoryDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemFixedDiscountActionExcludeCategory]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		CategoryId,
		IncludeSubcategories
	FROM 
		[lekmer].[tCartItemFixedDiscountActionExcludeCategory]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAllRecursive]'
GO


CREATE procedure [lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAllRecursive]
	@CartActionId		int
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT CategoryId, IncludeSubcategories 
	FROM [lekmer].[tCartItemFixedDiscountActionExcludeCategory]
	WHERE CartActionId = @CartActionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT count(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeCategoryInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeCategoryInsert]
	@CartActionId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	INSERT [lekmer].[tCartItemFixedDiscountActionExcludeCategory] (
		CartActionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES (
		@CartActionId,
		@CategoryId,
		@IncludeSubcategories
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeProductDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemFixedDiscountActionExcludeProduct]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeProductInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemFixedDiscountActionExcludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeBrandDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemFixedDiscountActionIncludeBrand]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeBrandGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[lekmer].[tCartItemFixedDiscountActionIncludeBrand]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeBrandInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeBrandInsert]
	@CartActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemFixedDiscountActionIncludeBrand] (
		CartActionId,
		BrandId
	)
	VALUES (
		@CartActionId,
		@BrandId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeCategoryDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeCategoryDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemFixedDiscountActionIncludeCategory]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		CategoryId,
		IncludeSubcategories
	FROM 
		[lekmer].[tCartItemFixedDiscountActionIncludeCategory]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAllRecursive]'
GO


CREATE procedure [lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAllRecursive]
	@CartActionId		int
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT CategoryId, IncludeSubcategories 
	FROM [lekmer].[tCartItemFixedDiscountActionIncludeCategory]
	WHERE CartActionId = @CartActionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT count(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeCategoryInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeCategoryInsert]
	@CartActionId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	INSERT [lekmer].[tCartItemFixedDiscountActionIncludeCategory] (
		CartActionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES (
		@CartActionId,
		@CategoryId,
		@IncludeSubcategories
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeProductDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemFixedDiscountActionIncludeProduct]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeProductInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemFixedDiscountActionIncludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionSave]'
GO

CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionSave]
	@CartActionId			INT,
	@IncludeAllProducts		BIT
AS
BEGIN
	UPDATE
		[lekmer].[tCartItemFixedDiscountAction]
	SET
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [lekmer].[tCartItemFixedDiscountAction] (
			CartActionId,
			IncludeAllProducts
		)
		VALUES (
			@CartActionId,
			@IncludeAllProducts
		)
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCartItemFixedDiscountActionCurrency]'
GO


CREATE VIEW [lekmer].[vCartItemFixedDiscountActionCurrency]
AS
	SELECT
		ci.[CartActionId]	AS 'CartItemFixedDiscountActionCurrency.CartActionId',
		ci.[CurrencyId]		AS 'CartItemFixedDiscountActionCurrency.CurrencyId',
		ci.[MonetaryValue]	AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[lekmer].[tCartItemFixedDiscountActionCurrency] ci
		INNER JOIN [core].[vCustomCurrency] c ON ci.[CurrencyId] = c.[Currency.Id]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionCurrencyGetByAction]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemFixedDiscountActionCurrency]
	WHERE 
		[CartItemFixedDiscountActionCurrency.CartActionId] = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeProductGetIdAllSecure]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeProductGetIdAllSecure]
	@CartActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[lekmer].[tCartItemFixedDiscountActionExcludeProduct] c
		INNER JOIN [product].[tProduct] p ON c.[ProductId] = p.[ProductId]
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[IsDeleted] = 0
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAllSecure]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAllSecure]
	@CartActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[lekmer].[tCartItemFixedDiscountActionIncludeProduct] c
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = c.[ProductId]
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[IsDeleted] = 0
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCartItemFixedDiscountAction]'
GO



CREATE VIEW [lekmer].[vCartItemFixedDiscountAction]
AS
	SELECT
		cid.[CartActionId]				AS 'CartItemFixedDiscountAction.CartActionId',
		cid.[IncludeAllProducts]		AS 'CartItemFixedDiscountAction.IncludeAllProducts',
		ca.*
	FROM
		[lekmer].[tCartItemFixedDiscountAction] cid
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = cid.[CartActionId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionGetById]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemFixedDiscountAction]
	WHERE
		[CartItemFixedDiscountAction.CartActionId] = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionExcludeProductGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@CartActionId		INT
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		[lekmer].[tCartItemFixedDiscountActionExcludeProduct] c
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = c.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[Product.ChannelId] = @ChannelId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@CartActionId		INT
AS
BEGIN
	SELECT
		p.[Product.Id]
	FROM
		[lekmer].[tCartItemFixedDiscountActionIncludeProduct] c
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = c.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[Product.ChannelId] = @ChannelId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemFixedDiscountAction]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountAction] ADD
CONSTRAINT [FK_tCartItemFixedDiscountAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemFixedDiscountActionCurrency]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionCurrency] ADD
CONSTRAINT [FK_tCartItemFixedDiscountActionCurrency_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemFixedDiscountActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemFixedDiscountActionExcludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeBrand] ADD
CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeBrand_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemFixedDiscountActionExcludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeCategory] ADD
CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeCategory_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemFixedDiscountActionExcludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeProduct] ADD
CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeProduct_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemFixedDiscountActionIncludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionIncludeBrand] ADD
CONSTRAINT [FK_tCartItemFixedDiscountActionIncludeBrand_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemFixedDiscountActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemFixedDiscountActionIncludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionIncludeCategory] ADD
CONSTRAINT [FK_tCartItemFixedDiscountActionIncludeCategory_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemFixedDiscountActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemFixedDiscountActionIncludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionIncludeProduct] ADD
CONSTRAINT [FK_tCartItemFixedDiscountActionIncludeProduct_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemFixedDiscountActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
