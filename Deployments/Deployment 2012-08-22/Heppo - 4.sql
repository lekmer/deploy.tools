INSERT INTO [productlek].[tProductColor] ([ProductId], [ColorId])
SELECT [lp].[ProductId], [c].[ColorId]
FROM [lekmer].[tLekmerProduct] lp
INNER JOIN [productlek].[tColor] c ON [c].[HyErpId] = SUBSTRING([lp].[HYErpId], LEN([lp].[HYErpId])-3, 4)