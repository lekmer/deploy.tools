/*
Run this script on:

10.150.43.52.Lekmer_live – this database will be modified

to synchronize its data with:

D:\Projects\lmheppo\Scensum\trunk\Release\Release 024\DB\LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 9.0.0 from Red Gate Software Ltd at 22.08.2012 16:15:02

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tComponentParameter]
ALTER TABLE [template].[tComponentParameter] DROP CONSTRAINT [FK_tComponentParameter_tComponent]

-- Add rows to [template].[tComponentParameter]
SET IDENTITY_INSERT [template].[tComponentParameter] ON
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000004, 1000003, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000005, 1000003, N'ColumnCount', 0, N'Sets the columns count value.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000006, 1000003, N'RowCount', 0, N'Sets the rows count value')
SET IDENTITY_INSERT [template].[tComponentParameter] OFF
-- Operation applied to 3 rows out of 3

-- Add constraints to [template].[tComponentParameter]
ALTER TABLE [template].[tComponentParameter] ADD CONSTRAINT [FK_tComponentParameter_tComponent] FOREIGN KEY ([ComponentId]) REFERENCES [template].[tComponent] ([ComponentId])
COMMIT TRANSACTION
GO
