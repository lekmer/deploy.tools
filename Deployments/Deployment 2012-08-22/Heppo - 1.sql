/*
Run this script on:

        10.150.43.52.Heppo_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 024\DB\HeppoDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 22.08.2012 16:13:29

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [productlek].[tColor]'
GO
CREATE TABLE [productlek].[tColor]
(
[ColorId] [int] NOT NULL IDENTITY(1, 1),
[HyErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Value] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (255) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tColor] on [productlek].[tColor]'
GO
ALTER TABLE [productlek].[tColor] ADD CONSTRAINT [PK_tColor] PRIMARY KEY CLUSTERED  ([ColorId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tColorTranslation]'
GO
CREATE TABLE [productlek].[tColorTranslation]
(
[ColorId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Value] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tColorTranslation] on [productlek].[tColorTranslation]'
GO
ALTER TABLE [productlek].[tColorTranslation] ADD CONSTRAINT [PK_tColorTranslation] PRIMARY KEY CLUSTERED  ([ColorId], [LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tProductColor]'
GO
CREATE TABLE [productlek].[tProductColor]
(
[ProductId] [int] NOT NULL,
[ColorId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductColor] on [productlek].[tProductColor]'
GO
ALTER TABLE [productlek].[tProductColor] ADD CONSTRAINT [PK_tProductColor] PRIMARY KEY CLUSTERED  ([ProductId], [ColorId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vColor]'
GO
CREATE VIEW [productlek].[vColor]
AS
SELECT     
      [c].[ColorId] AS 'Color.ColorId', 
      [c].[HyErpId] AS 'Color.HyErpId', 
      COALESCE ([ct].[Value], [c].[Value]) AS 'Color.Value',
      [c].[CommonName] AS 'Color.CommonName',
      [l].[LanguageId] AS 'Color.LanguageId'
FROM
      [productlek].[tColor] AS c
      CROSS JOIN [core].[tLanguage] AS l
      LEFT JOIN [productlek].[tColorTranslation] AS ct ON ct.[ColorId] = [c].[ColorId] AND [ct].[LanguageId] = [l].[LanguageId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pColorGetAll]'
GO
CREATE PROCEDURE [productlek].[pColorGetAll]
	@LanguageId INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		[c].*
	FROM 
		[productlek].[vColor] c
	WHERE
		[c].[Color.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductColorGetAll]'
GO
CREATE PROCEDURE [productlek].[pProductColorGetAll]
AS
BEGIN
	SELECT 
		[ColorId] AS 'ProductColor.ColorId' ,
		[ProductId] AS 'ProductColor.ProductId'
	FROM [productlek].[tProductColor]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tColorTranslation]'
GO
ALTER TABLE [productlek].[tColorTranslation] ADD
CONSTRAINT [FK_tColorTranslation_tColor] FOREIGN KEY ([ColorId]) REFERENCES [productlek].[tColor] ([ColorId]),
CONSTRAINT [FK_tColorTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tProductColor]'
GO
ALTER TABLE [productlek].[tProductColor] ADD
CONSTRAINT [FK_tProductColor_tColor] FOREIGN KEY ([ColorId]) REFERENCES [productlek].[tColor] ([ColorId]),
CONSTRAINT [FK_tProductColor_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
