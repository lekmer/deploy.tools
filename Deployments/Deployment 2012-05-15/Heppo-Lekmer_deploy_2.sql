/*
Run this script on:

        (local).Lekmer_212_0    -  This database will be modified

to synchronize it with:

        (local).Lekmer_212_1

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 15.05.2012 11:23:44

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [orderlek]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [customerlek]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[tAddress]'
GO
CREATE TABLE [customerlek].[tAddress]
(
[AddressId] [int] NOT NULL,
[CustomerId] [int] NOT NULL,
[HouseNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[HouseExtension] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tAddress] on [customerlek].[tAddress]'
GO
ALTER TABLE [customerlek].[tAddress] ADD CONSTRAINT [PK_tAddress] PRIMARY KEY CLUSTERED  ([CustomerId], [AddressId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tOrderAddress]'
GO
CREATE TABLE [orderlek].[tOrderAddress]
(
[OrderAddressId] [int] NOT NULL,
[HouseNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[HouseExtension] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tOrderAddress] on [orderlek].[tOrderAddress]'
GO
ALTER TABLE [orderlek].[tOrderAddress] ADD CONSTRAINT [PK_tOrderAddress] PRIMARY KEY CLUSTERED  ([OrderAddressId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[pAddressDelete]'
GO

CREATE PROCEDURE [customerlek].[pAddressDelete]
	@AddressId	INT
AS	
BEGIN
	SET NOCOUNT ON
	
	DELETE [customerlek].[tAddress]
	WHERE [AddressId] = @AddressId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[pAddressSave]'
GO

CREATE PROCEDURE [customerlek].[pAddressSave]
	@AddressId		INT,
	@CustomerId		INT,
	@HouseNumber	NVARCHAR(50),
	@HouseExtension	NVARCHAR(100)
AS	
BEGIN
	SET NOCOUNT ON
	
	UPDATE  [customerlek].[tAddress]
	SET 
		HouseNumber = @HouseNumber,
		HouseExtension = @HouseExtension
	WHERE
		AddressId = @AddressId
		AND
		CustomerId = @CustomerId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customerlek].[tAddress]
		(
			AddressId,
			CustomerId,
			HouseNumber,
			HouseExtension
		)
		VALUES
		(
			@AddressId,
			@CustomerId,
			@HouseNumber,
			@HouseExtension
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderAddressSave]'
GO

CREATE PROCEDURE [orderlek].[pOrderAddressSave]
	@OrderAddressId INT,
	@HouseNumber	NVARCHAR(50),
	@HouseExtension	NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[orderlek].[tOrderAddress]
	SET 
		HouseNumber = @HouseNumber,
		HouseExtension = @HouseExtension
	WHERE	
		OrderAddressId = @OrderAddressId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [orderlek].[tOrderAddress]
		(
			OrderAddressId,
			HouseNumber,
			HouseExtension
		)
		VALUES
		(
			@OrderAddressId,
			@HouseNumber,
			@HouseExtension
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vOrderAddress]'
GO

CREATE VIEW [orderlek].[vOrderAddress]
AS
	SELECT
		[OrderAddressId] AS 'OrderAddress.OrderAddressId',
		[HouseNumber] AS 'OrderAddress.HouseNumber',
		[HouseExtension] AS 'OrderAddress.HouseExtension'
	FROM
		[orderlek].[tOrderAddress]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrderAddress]'
GO

ALTER VIEW [order].[vCustomOrderAddress]
AS
	SELECT
		oa.*,
		loa.[OrderAddress.HouseNumber],
		loa.[OrderAddress.HouseExtension]
	FROM
		[order].[vOrderAddress] oa
		LEFT OUTER JOIN [orderlek].[vOrderAddress] loa
			ON loa.[OrderAddress.OrderAddressId] = oa.[OrderAddress.OrderAddressId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[vAddress]'
GO

CREATE VIEW [customerlek].[vAddress]
AS
	SELECT
		AddressId AS 'Address.AddressId',
		CustomerId AS 'Address.CustomerId',
		HouseNumber AS 'Address.HouseNumber',
		HouseExtension AS 'Address.HouseExtension'
	FROM
		[customerlek].[tAddress]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomAddress]'
GO

ALTER VIEW [customer].[vCustomAddress]
AS
	SELECT
		a.*,
		la.[Address.HouseNumber],
		la.[Address.HouseExtension]
	FROM
		[customer].[vAddress] a
		LEFT OUTER JOIN [customerlek].[vAddress] la 
			ON la.[Address.AddressId] = a.[Address.AddressId]
			AND la.[Address.CustomerId] = a.[Address.CustomerId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomCustomerSecure]'
GO

ALTER VIEW [customer].[vCustomCustomerSecure]
AS
	SELECT
		C.*,
		BA.[Address.HouseNumber] AS 'BillingAddress.HouseNumber',
		BA.[Address.HouseExtension] AS 'BillingAddress.HouseExtension',
		DA.[Address.HouseNumber] AS 'DeliveryAddress.HouseNumber',
		DA.[Address.HouseExtension] AS  'DeliveryAddress.HouseExtension'
	FROM
		[customer].[vCustomerSecure] C
		LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = C.[CustomerInformation.DefaultBillingAddressId]
		LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = C.[CustomerInformation.DefaultDeliveryAddressId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomCustomer]'
GO

ALTER VIEW [customer].[vCustomCustomer]
AS
	SELECT
		C.*,
		BA.[Address.HouseNumber] AS 'BillingAddress.HouseNumber',
		BA.[Address.HouseExtension] AS 'BillingAddress.HouseExtension',
		DA.[Address.HouseNumber] AS 'DeliveryAddress.HouseNumber',
		DA.[Address.HouseExtension] AS  'DeliveryAddress.HouseExtension'
	FROM
		[customer].[vCustomer] C
		LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = C.[CustomerInformation.DefaultBillingAddressId]
		LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = C.[CustomerInformation.DefaultDeliveryAddressId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customerlek].[tAddress]'
GO
ALTER TABLE [customerlek].[tAddress] ADD
CONSTRAINT [FK_tAddress(lek)_tAddress] FOREIGN KEY ([CustomerId], [AddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tOrderAddress]'
GO
ALTER TABLE [orderlek].[tOrderAddress] ADD
CONSTRAINT [FK_tOrderAddress(lek)_tOrderAddress] FOREIGN KEY ([OrderAddressId]) REFERENCES [order].[tOrderAddress] ([OrderAddressId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
