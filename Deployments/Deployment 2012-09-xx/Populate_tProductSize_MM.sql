UPDATE ps
SET [ps].[OverrideMillimeter] = [s].[Millimeter]
FROM [lekmer].[tProductSize] ps
INNER JOIN [lekmer].[tSize] s ON [ps].[SizeId] = [s].[SizeId]