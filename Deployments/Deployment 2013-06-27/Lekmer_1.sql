IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
BEGIN TRANSACTION
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [campaignlek].[tCartActionTargetProductType]'
GO
CREATE TABLE [campaignlek].[tCartActionTargetProductType]
(
[CartActionId] [int] NOT NULL,
[ProductTypeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartActionTargetProductType] on [campaignlek].[tCartActionTargetProductType]'
GO
ALTER TABLE [campaignlek].[tCartActionTargetProductType] ADD CONSTRAINT [PK_tCartActionTargetProductType] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCartActionTargetProductType]'
GO

CREATE VIEW [campaignlek].[vCartActionTargetProductType]
AS
	SELECT
		[CartActionId] AS 'CartActionTargetProductType.CartActionId',
		[ProductTypeId] AS 'CartActionTargetProductType.ProductTypeId'
	FROM
		[campaignlek].[tCartActionTargetProductType] t

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tConditionTargetProductType]'
GO
CREATE TABLE [campaignlek].[tConditionTargetProductType]
(
[ConditionId] [int] NOT NULL,
[ProductTypeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tConditionTargetProductType] on [campaignlek].[tConditionTargetProductType]'
GO
ALTER TABLE [campaignlek].[tConditionTargetProductType] ADD CONSTRAINT [PK_tConditionTargetProductType] PRIMARY KEY CLUSTERED  ([ConditionId], [ProductTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vConditionTargetProductType]'
GO

CREATE VIEW [campaignlek].[vConditionTargetProductType]
AS
	SELECT
		[ConditionId] AS 'ConditionTargetProductType.ConditionId',
		[ProductTypeId] AS 'ConditionTargetProductType.ProductTypeId'
	FROM
		[campaignlek].[tConditionTargetProductType] t

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartActionTargetProductTypeSave]'
GO

CREATE PROCEDURE [campaignlek].[pCartActionTargetProductTypeSave]
	@CartActionId INT,
	@TargetProductTypeIds VARCHAR(MAX),
	@Delimiter CHAR(1)
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@TargetProductTypeIds, @Delimiter)
	
	DELETE [campaignlek].[tCartActionTargetProductType]
	WHERE
		[CartActionId] = @CartActionId
	
	INSERT [campaignlek].[tCartActionTargetProductType] ( [CartActionId], [ProductTypeId] )
	SELECT
		@CartActionId,
		Id
	FROM
		@IdList
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartActionTargetProductTypeGetAllByCampaign]'
GO
CREATE PROCEDURE [campaignlek].[pCartActionTargetProductTypeGetAllByCampaign]
	@CampaignId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[at].*
	FROM
		[campaignlek].[vCartActionTargetProductType] at
		INNER JOIN [campaign].[tCartAction] pa ON [pa].[CartActionId] = [at].[CartActionTargetProductType.CartActionId]
	WHERE
		[pa].[CampaignId] = @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartActionTargetProductTypeDelete]'
GO

CREATE PROCEDURE [campaignlek].[pCartActionTargetProductTypeDelete]
	@CartActionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE [campaignlek].[tCartActionTargetProductType]
	WHERE [CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pConditionTargetProductTypeDelete]'
GO

CREATE PROCEDURE [campaignlek].[pConditionTargetProductTypeDelete]
	@ConditionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE [campaignlek].[tConditionTargetProductType]
	WHERE [ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pConditionTargetProductTypeGetAllByCampaign]'
GO
CREATE PROCEDURE [campaignlek].[pConditionTargetProductTypeGetAllByCampaign]
	@CampaignId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[at].*
	FROM
		[campaignlek].[vConditionTargetProductType] at
		INNER JOIN [campaign].[tCondition] pa ON [pa].[ConditionId] = [at].[ConditionTargetProductType.ConditionId]
	WHERE
		[pa].[CampaignId] = @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pConditionTargetProductTypeSave]'
GO

CREATE PROCEDURE [campaignlek].[pConditionTargetProductTypeSave]
	@ConditionId INT,
	@TargetProductTypeIds VARCHAR(MAX),
	@Delimiter CHAR(1)
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@TargetProductTypeIds, @Delimiter)
	
	DELETE [campaignlek].[tConditionTargetProductType]
	WHERE
		[ConditionId] = @ConditionId
	
	INSERT [campaignlek].[tConditionTargetProductType] ( [ConditionId], [ProductTypeId] )
	SELECT
		@ConditionId,
		Id
	FROM
		@IdList
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartActionTargetProductType]'
GO
ALTER TABLE [campaignlek].[tCartActionTargetProductType] ADD CONSTRAINT [FK_tCartActionTargetProductType_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
ALTER TABLE [campaignlek].[tCartActionTargetProductType] ADD CONSTRAINT [FK_tCartActionTargetProductType_tProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [productlek].[tProductType] ([ProductTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tConditionTargetProductType]'
GO
ALTER TABLE [campaignlek].[tConditionTargetProductType] ADD CONSTRAINT [FK_tConditionTargetProductType_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
ALTER TABLE [campaignlek].[tConditionTargetProductType] ADD CONSTRAINT [FK_tConditionTargetProductType_tProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [productlek].[tProductType] ([ProductTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
/*
Start of RedGate SQL Source Control versioning database-level extended properties.
*/
/*
End of RedGate SQL Source Control versioning database-level extended properties.
*/
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) IF EXISTS(SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
