SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProductTranslation]'
GO
ALTER TABLE [lekmer].[tLekmerProductTranslation] DROP CONSTRAINT [FK_tLekmerProductTranslation_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductUrlHistory]'
GO
ALTER TABLE [lekmer].[tProductUrlHistory] DROP CONSTRAINT [FK_tProductUrlHistory_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [productlek].[tProductColor]'
GO
ALTER TABLE [productlek].[tProductColor] DROP CONSTRAINT [FK_tProductColor_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tProduct]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tBrand]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tBatteryType]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tSizeDeviation]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tProductType]
--ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tStockStatus]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] DROP CONSTRAINT [FK_tProductPopularity_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductUrl]'
GO
ALTER TABLE [lekmer].[tProductUrl] DROP CONSTRAINT [FK_tProductUrl_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductSimilar]'
GO
ALTER TABLE [lekmer].[tProductSimilar] DROP CONSTRAINT [FK_tProductSimilar_tLekmerProduct]
ALTER TABLE [lekmer].[tProductSimilar] DROP CONSTRAINT [FK_tProductSimilar_tLekmerProduct1]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] DROP CONSTRAINT [FK_tProductSize_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProductIcon]'
GO
ALTER TABLE [lekmer].[tLekmerProductIcon] DROP CONSTRAINT [FK_tLekmerProductIcon_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [PK_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_CreatedDate]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_ShowVariantRelations]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_IsAbove60L]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_StockStatusId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_SellOnlyInPackage]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tLekmerProduct_BrandId] from [lekmer].[tLekmerProduct]'
GO
DROP INDEX [IX_tLekmerProduct_BrandId] ON [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tLekmerProduct_HYErpId] from [lekmer].[tLekmerProduct]'
GO
DROP INDEX [IX_tLekmerProduct_HYErpId] ON [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerCategory]'
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD
[DeliveryTimeId] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[tProductPrice]'
GO
ALTER TABLE [export].[tProductPrice] ADD
[IsChanged] [bit] NOT NULL CONSTRAINT [DF_tProductPrice_IsChanged] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tLekmerProduct]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tLekmerProduct]
(
[ProductId] [int] NOT NULL,
[BrandId] [int] NULL,
[IsBookable] [bit] NOT NULL,
[AgeFromMonth] [int] NOT NULL,
[AgeToMonth] [int] NOT NULL,
[IsNewFrom] [datetime] NULL,
[IsNewTo] [datetime] NULL,
[Measurement] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[BatteryTypeId] [int] NULL,
[NumberOfBatteries] [int] NULL,
[IsBatteryIncluded] [bit] NOT NULL,
[ExpectedBackInStock] [datetime] NULL,
[HYErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_tLekmerProduct_CreatedDate] DEFAULT (getdate()),
[SizeDeviationId] [int] NULL,
[LekmerErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ShowVariantRelations] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_ShowVariantRelations] DEFAULT ((0)),
[Weight] [decimal] (18, 3) NULL,
[ProductTypeId] [int] NOT NULL,
[IsAbove60L] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_IsAbove60L] DEFAULT ((0)),
[StockStatusId] [int] NOT NULL CONSTRAINT [DF_tLekmerProduct_StockStatusId] DEFAULT ((0)),
[MonitorThreshold] [int] NULL,
[MaxQuantityPerOrder] [int] NULL,
[DeliveryTimeId] [int] NULL,
[SellOnlyInPackage] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_SellOnlyInPackage] DEFAULT ((0)),
[SupplierId] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[PurchasePrice] [decimal] (16, 2) NULL,
[PurchaseCurrencyId] [int] NULL,
[SupplierArticleNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tLekmerProduct]([ProductId], [BrandId], [IsBookable], [AgeFromMonth], [AgeToMonth], [IsNewFrom], [IsNewTo], [Measurement], [BatteryTypeId], [NumberOfBatteries], [IsBatteryIncluded], [ExpectedBackInStock], [HYErpId], [CreatedDate], [SizeDeviationId], [LekmerErpId], [ShowVariantRelations], [Weight], [ProductTypeId], [IsAbove60L], [StockStatusId], [MonitorThreshold], [MaxQuantityPerOrder], [SellOnlyInPackage]) SELECT [ProductId], [BrandId], [IsBookable], [AgeFromMonth], [AgeToMonth], [IsNewFrom], [IsNewTo], [Measurement], [BatteryTypeId], [NumberOfBatteries], [IsBatteryIncluded], [ExpectedBackInStock], [HYErpId], [CreatedDate], [SizeDeviationId], [LekmerErpId], [ShowVariantRelations], [Weight], [ProductTypeId], [IsAbove60L], [StockStatusId], [MonitorThreshold], [MaxQuantityPerOrder], [SellOnlyInPackage] FROM [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tLekmerProduct]', N'tLekmerProduct'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tLekmerProduct] on [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [PK_tLekmerProduct] PRIMARY KEY CLUSTERED  ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tLekmerProduct_BrandId] on [lekmer].[tLekmerProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tLekmerProduct_BrandId] ON [lekmer].[tLekmerProduct] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tLekmerProduct_HYErpId] on [lekmer].[tLekmerProduct]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tLekmerProduct_HYErpId] ON [lekmer].[tLekmerProduct] ([HYErpId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBrand]'
GO
ALTER TABLE [lekmer].[tBrand] ADD
[DeliveryTimeId] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tDeliveryTime]'
GO
CREATE TABLE [productlek].[tDeliveryTime]
(
[DeliveryTimeId] [int] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tDeliveryTime] on [productlek].[tDeliveryTime]'
GO
ALTER TABLE [productlek].[tDeliveryTime] ADD CONSTRAINT [PK_tDeliveryTime] PRIMARY KEY CLUSTERED  ([DeliveryTimeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tProductRegistryDeliveryTime]'
GO
CREATE TABLE [productlek].[tProductRegistryDeliveryTime]
(
[ProductRegistryId] [int] NOT NULL,
[DeliveryTimeId] [int] NOT NULL,
[From] [int] NULL,
[To] [int] NULL,
[Format] [nvarchar] (25) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductRegistryDeliveryTime] on [productlek].[tProductRegistryDeliveryTime]'
GO
ALTER TABLE [productlek].[tProductRegistryDeliveryTime] ADD CONSTRAINT [PK_tProductRegistryDeliveryTime] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [DeliveryTimeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[tBlockTopList]'
GO
ALTER TABLE [addon].[tBlockTopList] ADD
[LinkContentNodeId] [int] NULL,
[CustomUrl] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductRegistryDeliveryTimeDelete]'
GO
CREATE PROCEDURE [productlek].[pProductRegistryDeliveryTimeDelete]
	@Id INT
AS 
BEGIN
	DELETE FROM [productlek].[tProductRegistryDeliveryTime]
	WHERE [DeliveryTimeId] = @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vDeliveryTimeSecure]'
GO





CREATE VIEW [productlek].[vDeliveryTimeSecure]
AS
SELECT     
      [DeliveryTimeId] AS 'DeliveryTime.Id',
      NULL AS 'DeliveryTime.From',
      NULL AS 'DeliveryTime.To',
      NULL AS 'DeliveryTime.Format',
      NULL AS 'DeliveryTime.ProductRegistryId',
	  NULL AS 'DeliveryTime.ProductRegistryChannelId'
FROM
      [productlek].[tDeliveryTime]




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrandSecure]'
GO





ALTER VIEW [lekmer].[vBrandSecure]
AS
SELECT     
	[B].[BrandId] 'Brand.BrandId',
    [B].[Title] 'Brand.Title',
    [B].[Description] 'Brand.Description',
    [B].[PackageInfo] 'Brand.PackageInfo',
    [B].[ExternalUrl] 'Brand.ExternalUrl',
    [B].[MediaId] 'Brand.MediaId',
    [B].[ErpId] 'Brand.ErpId',
	[B].[MonitorThreshold] 'Brand.MonitorThreshold',
	[B].[MaxQuantityPerOrder] 'Brand.MaxQuantityPerOrder',
	[B].[DeliveryTimeId] 'Brand.DeliveryTimeId',
	[I].*,
	[dt].*,
    NULL 'Brand.ContentNodeId'
FROM
	lekmer.tBrand B
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = B.MediaId
	LEFT JOIN [productlek].[vDeliveryTimeSecure] dt ON [dt].[DeliveryTime.Id] = [B].[DeliveryTimeId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pDeliveryTimeDelete]'
GO
CREATE PROCEDURE [productlek].[pDeliveryTimeDelete]
	@Id INT
AS 
BEGIN
	DELETE FROM [productlek].[tDeliveryTime]
	WHERE [DeliveryTimeId] = @Id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandDelete]'
GO

ALTER PROCEDURE [lekmer].[pBrandDelete]
	@BrandId	INT
AS
BEGIN
	DECLARE @DeliveryTimeId INT
	SET @DeliveryTimeId = (SELECT [DeliveryTimeId] FROM [lekmer].[tBrand] WHERE [BrandId] = @BrandId)
	EXEC [productlek].[pProductRegistryDeliveryTimeDelete] @DeliveryTimeId
	EXEC [productlek].[pDeliveryTimeDelete] @DeliveryTimeId

	EXEC [lekmer].[pProductRegistryRestrictionBrandDelete] @BrandId

	DELETE FROM
		[review].[tBlockBestRatedProductListBrand]
	WHERE 
		[BrandId] = @BrandId

	DELETE FROM 
		[review].[tBlockLatestFeedbackListBrand]
	WHERE 
		[BrandId] = @BrandId

	DELETE FROM 
		[lekmer].[tBlockBrandListBrand]
	WHERE 
		[BrandId] = @BrandId
	
	DELETE FROM 
		[lekmer].[tBlockBrandProductListBrand]
	WHERE 
		[BrandId] = @BrandId
		
	DELETE FROM
		[lekmer].[tBlockProductFilterBrand]
	WHERE 
		[BrandId] = @BrandId	
	
	UPDATE 
		[lekmer].[tLekmerProduct]
	SET 
		[BrandId] = NULL 
	WHERE 
		[BrandId] = @BrandId

	DELETE FROM 
		[lekmer].[tBrandTranslation]
	WHERE
		[BrandId] = @BrandId
			
	DELETE FROM 
		[lekmer].[tBrand]
	WHERE 
		[BrandId] = @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vDeliveryTime]'
GO





-- View

CREATE VIEW [productlek].[vDeliveryTime]
AS
SELECT     
	[dt].[DeliveryTimeId] AS 'DeliveryTime.Id',
	[prdt].[From] AS 'DeliveryTime.From',
	[prdt].[To] AS 'DeliveryTime.To',
	[prdt].[Format] AS 'DeliveryTime.Format',
	[c].[ChannelId] AS 'DeliveryTime.ChannelId',
	NULL AS 'DeliveryTime.ProductRegistryId',
	NULL AS 'DeliveryTime.ProductRegistryChannelId'
FROM
	[productlek].[tDeliveryTime] dt
    CROSS JOIN core.tChannel AS c
    LEFT JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ChannelId]
    LEFT JOIN [product].[tProductRegistry] pr ON [pr].[ProductRegistryId] = [pmc].[ProductRegistryId]
    LEFT JOIN [productlek].[tProductRegistryDeliveryTime] prdt ON [prdt].[ProductRegistryId] = [pr].[ProductRegistryId]
		AND [prdt].[DeliveryTimeId] = [dt].[DeliveryTimeId]





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO





ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	[product].[fProductHasSizes](ProductId) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations',
	[Weight] AS 'Lekmer.Weight',
	[ProductTypeId] AS 'Lekmer.ProductTypeId',
	[IsAbove60L] AS 'Lekmer.IsAbove60L',
	[StockStatusId] AS 'Lekmer.StockStatusId',
	[MonitorThreshold] AS 'Lekmer.MonitorThreshold',
	[MaxQuantityPerOrder] AS 'Lekmer.MaxQuantityPerOrder',
	[DeliveryTimeId] AS 'Lekmer.DeliveryTimeId',
	[SellOnlyInPackage] AS 'Lekmer.SellOnlyInPackage',
	[SupplierId] AS 'Lekmer.SupplierId',
	[PurchasePrice] AS 'Lekmer.PurchasePrice',
	[PurchaseCurrencyId] AS 'Lekmer.PurchaseCurrencyId',
	[SupplierArticleNumber] AS 'Lekmer.SupplierArticleNumber'
FROM
	lekmer.tLekmerProduct AS p




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductInclPackageProducts]'
GO


ALTER VIEW [product].[vCustomProductInclPackageProducts]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		[lp].[Lekmer.MonitorThreshold],
		[lp].[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*,
		[dt].*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [c].[Channel.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tSupplier]'
GO
CREATE TABLE [productlek].[tSupplier]
(
[SupplierId] [int] NOT NULL IDENTITY(1, 1),
[SupplierNo] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ActivePassiveCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Address] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[ZipArea] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[City] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CountryISO] [char] (2) COLLATE Finnish_Swedish_CI_AS NULL,
[LanguageISO] [char] (2) COLLATE Finnish_Swedish_CI_AS NULL,
[VATRate] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Phone1] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Phone2] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tSupplier_1] on [productlek].[tSupplier]'
GO
ALTER TABLE [productlek].[tSupplier] ADD CONSTRAINT [PK_tSupplier_1] PRIMARY KEY CLUSTERED  ([SupplierId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vSupplier]'
GO
CREATE VIEW [productlek].[vSupplier]
AS
SELECT
	[s].[SupplierId] 'Supplier.Id',
	[s].[SupplierNo] 'Supplier.SupplierNo',
	[s].[ActivePassiveCode] 'Supplier.ActivePassiveCode',
	[s].[Name] 'Supplier.Name',
	[s].[Address] 'Supplier.Address',
	[s].[ZipArea] 'Supplier.ZipArea',
	[s].[City] 'Supplier.City',
	[s].[CountryISO] 'Supplier.CountryISO',
	[s].[LanguageISO] 'Supplier.LanguageISO',
	[s].[VATRate] 'Supplier.VATRate',
	[s].[Phone1] 'Supplier.Phone1',
	[s].[Phone2] 'Supplier.Phone2',
	[s].[Email] 'Supplier.Email'
FROM
	[productlek].[tSupplier] s
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[tempProduct]'
GO
ALTER TABLE [integration].[tempProduct] ADD
[PurchasePrice] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[PurchaseCurrency] [varchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[SupplierArticleNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateLekmerProduct]'
GO
-- Stored Procedure

ALTER PROCEDURE [integration].[usp_ImportUpdateLekmerProduct]
AS
BEGIN
	DECLARE 
		@HYChannelNL NVARCHAR(40),
		@ChannelIdNL INT

	SET @HYChannelNL = '005'
	SET @ChannelIdNL = 1000005

	IF OBJECT_ID('tempdb..#tProductUpdate') IS NOT NULL
		DROP TABLE #tProductUpdate
	CREATE TABLE #tProductUpdate (
		[FullHYId]				VARCHAR(50) NOT NULL,
		[ChannelId]				VARCHAR(1) NOT NULL,
		[HYErpId]				VARCHAR(50) NOT NULL,
		[LekmerErpId]			VARCHAR(50) NULL,
		[NoInStock]				NVARCHAR(25) NULL,
		[Weight]				DECIMAL(18, 3) NULL,
		[IsAbove60L]			BIT NULL,
		[StockStatusId]			INT NULL,
		[SupplierId]			NVARCHAR(500) NULL,
		[PurchasePrice]			DECIMAL (16, 2) NULL,
		[PurchaseCurrencyId]	INT NULL,
		[SupplierArticleNumber]	VARCHAR(50) NULL
		
		CONSTRAINT PK_#tProductUpdate PRIMARY KEY(HYErpId) WITH (IGNORE_DUP_KEY=ON)
	)

	INSERT INTO [#tProductUpdate] (
		[FullHYId],
		[ChannelId],
		[HYErpId],
		[LekmerErpId],
		[NoInStock],
		[Weight],
		[IsAbove60L],
		[StockStatusId],
		[SupplierId],
		[PurchasePrice],
		[PurchaseCurrencyId],
		[SupplierArticleNumber]
	)
	SELECT
		[tp].[HYarticleId],
		SUBSTRING([tp].[HYarticleId], 3, 1),
		SUBSTRING([tp].[HYarticleId], 5, 12),
		[tp].[LekmerArtNo],
		[tp].[NoInStock],
		CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), [tp].[Weight]) / 1000),
		CASE WHEN [tp].[PossibleToDisplay] IS NULL OR [tp].[PossibleToDisplay] <> 'G' THEN 0 ELSE 1 END,
		[tp].[Ref1],
		[tp].[SupplierId],
		CONVERT(DECIMAl(16,2), CONVERT(DECIMAl(16,2), [tp].[PurchasePrice]) / 100.0),
		(SELECT [CurrencyId] FROM [core].[tCurrency] WHERE [ISO] = [tp].[PurchaseCurrency]),
		[tp].[SupplierArticleNumber]
		
	FROM
		[integration].[tempProduct] tp


	-- Update tLekmerProduct
	UPDATE 
		[lp]
	SET 
		[lp].[ExpectedBackInStock] = NULL
	FROM 
		[#tProductUpdate] pu
		INNER JOIN [lekmer].[tLekmerProduct] lp				ON [lp].[HYErpId] = [pu].[HYErpId]
		INNER JOIN [product].[tProduct] p					ON [p].[ProductId] = [lp].[ProductId]
		INNER JOIN [product].[tProductRegistryProduct] prp	ON [prp].[ProductId] = [lp].[ProductId]
																AND ([prp].[ProductRegistryId] = [pu].[ChannelId]
																	 OR ([prp].[ProductRegistryId] = @ChannelIdNL AND LEFT([pu].[FullHYId], 3) = @HYChannelNL))

	WHERE 
		[lp].[ExpectedBackInStock] <> LEN([lp].[ExpectedBackInStock])
		AND [pu].[NoInStock] > 0



	UPDATE 
		[lp]
	SET 
		[lp].[LekmerErpId]			= [pu].[LekmerErpId],
		[lp].[Weight]				= [pu].[Weight],
		[lp].[SupplierId]			= [pu].[SupplierId],
		[lp].[PurchasePrice]		= [pu].[PurchasePrice],
		[lp].[PurchaseCurrencyId]	= [pu].[PurchaseCurrencyId],
		[lp].[IsAbove60L]			= [pu].[IsAbove60L],
		[lp].[SupplierArticleNumber]= [pu].[SupplierArticleNumber]
	FROM 
		[#tProductUpdate] pu
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = [pu].[HYErpId]
	WHERE
		[pu].[ChannelId] = 1
		AND
			(
				[lp].[LekmerErpId] IS NULL OR [lp].[LekmerErpId] <> [pu].[LekmerErpId]
				OR [lp].[Weight] IS NULL OR [lp].[Weight] <> [pu].[Weight]
				OR [lp].[SupplierId] IS NULL OR [lp].[SupplierId] <> [pu].[SupplierId]
				OR [lp].[PurchasePrice] IS NULL OR [lp].[PurchasePrice] <> [pu].[PurchasePrice]
				OR [lp].[PurchaseCurrencyId] IS NULL OR [lp].[PurchaseCurrencyId] <> [pu].[PurchaseCurrencyId]
				OR [lp].[IsAbove60L] <> [pu].[IsAbove60L]
				OR [lp].[SupplierArticleNumber] IS NULL OR [lp].[SupplierArticleNumber] <> [pu].[SupplierArticleNumber]
			)


	UPDATE 
		[lp]
	SET
		[lp].[StockStatusId] = [ss].[StockStatusId]
	FROM 
		[#tProductUpdate] pu
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[HYErpId] = [pu].[ChannelId]
		INNER JOIN [productlek].[tStockStatus] ss ON [ss].[ErpId] = [pu].[StockStatusId]
		LEFT OUTER JOIN [lekmer].[tProductSize] ps ON [ps].[ProductId] = [lp].[ProductId]
	WHERE
		[pu].[ChannelId] = 1
		AND [ps].[ProductId] IS NULL -- only products without sizes
		AND [lp].[StockStatusId] != [ss].[StockStatusId]


END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO










ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.IsAbove60L],
		[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		'' AS 'Lekmer.UrlTitle',
		[dt].*,
		NULL AS 'Product.ParentContentNodeId',
		NULL AS 'Lekmer.RecommendedPrice'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		
		LEFT JOIN [productlek].[vDeliveryTimeSecure] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId]









GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductViewWithoutStatusFilter]'
GO


ALTER VIEW [lekmer].[vCustomProductViewWithoutStatusFilter]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		[dt].*,
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductViewWithoutStatusFilter] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [c].[Channel.Id]
	WHERE
		[lp].[Lekmer.SellOnlyInPackage] = 0

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBrand]'
GO


ALTER VIEW [lekmer].[vBrand]
AS
	SELECT     
		[B].[BrandId] 'Brand.BrandId',
		COALESCE ([bt].[BrandTranslation.Title], [B].[Title]) 'Brand.Title',
		COALESCE ([bt].[BrandTranslation.Description], [B].[Description]) 'Brand.Description',		
		COALESCE ([bt].[BrandTranslation.PackageInfo], [B].[PackageInfo]) 'Brand.PackageInfo',		
		[B].[ExternalUrl] 'Brand.ExternalUrl',
		[B].[MediaId] 'Brand.MediaId',
		[B].[ErpId] 'Brand.ErpId',
		[B].[MonitorThreshold] 'Brand.MonitorThreshold',
		[B].[MaxQuantityPerOrder] 'Brand.MaxQuantityPerOrder',
		[B].[DeliveryTimeId] 'Brand.DeliveryTimeId',
		[Ch].[ChannelId],
		[I].*,
		[dt].*,
		[Bssr].[ContentNodeId] 'Brand.ContentNodeId'
	FROM
		lekmer.tBrand AS B
		CROSS JOIN core.tChannel AS Ch
		
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = B.MediaId AND I.[Image.LanguageId] = Ch.LanguageId
		LEFT JOIN lekmer.vBrandTranslation AS bt ON bt.[BrandTranslation.BrandId] = B.BrandId AND bt.[BrandTranslation.LanguageId] = Ch.LanguageId
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [B].[DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [Ch].[ChannelId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = Ch.ChannelId
		LEFT JOIN lekmer.tBrandSiteStructureRegistry Bssr ON Bssr.BrandId = B.BrandId AND Bssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductViewInclPackageProducts]'
GO


ALTER VIEW [product].[vCustomProductViewInclPackageProducts]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		[dt].*,
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [c].[Channel.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCategory]'
GO

ALTER VIEW [product].[vCategory]
AS
SELECT     
	c.CategoryId 'Category.Id',
	c.ParentCategoryId 'Category.ParentCategoryId',
	COALESCE (ct.Title, c.Title) 'Category.Title', 
	c.ErpId 'Category.ErpId',
	t.LanguageId 'Category.LanguageId',
	t.[ChannelId] 'Channel.ChannelId'
FROM
	[product].[tCategory] c
	CROSS JOIN [core].[tChannel] t
	LEFT JOIN [product].[tCategoryTranslation] ct ON ct.CategoryId = c.CategoryId AND ct.LanguageId = t.LanguageId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategory]'
GO


ALTER VIEW [product].[vCustomCategory]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		COALESCE ([lct].[PackageInfo], [l].[PackageInfo]) AS [LekmerCategory.PackageInfo],
		[l].[MonitorThreshold] AS [LekmerCategory.MonitorThreshold],
		[l].[MaxQuantityPerOrder] AS [LekmerCategory.MaxQuantityPerOrder],
		[l].[DeliveryTimeId] AS [LekmerCategory.DeliveryTimeId],
		[dt].*
	FROM
		[product].[vCategory] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]
		LEFT JOIN [productlek].[tLekmerCategoryTranslation] lct ON [lct].[CategoryId] = [c].[Category.Id] AND [lct].[LanguageId] = [c].[Category.LanguageId]
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [l].[DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [c].[Channel.ChannelId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCategoryView]'
GO




ALTER VIEW [product].[vCategoryView]
AS
	SELECT     
		c.*,
		ch.[Channel.Id] 'Category.ChannelId',
		r.[CategoryRegistry.ProductParentContentNodeId],
		r.[CategoryRegistry.ProductTemplateContentNodeId]
	FROM
		[product].[vCustomCategory] AS c
		CROSS JOIN core.vCustomChannel ch
		LEFT JOIN sitestructure.tSiteStructureModuleChannel mch ON mch.ChannelId = ch.[Channel.Id]
		LEFT JOIN product.vCustomCategorySiteStructureRegistry r
			ON mch.SiteStructureRegistryId = r.[CategoryRegistry.SiteStructureRegistryId]
			AND c.[Category.Id] = r.[CategoryRegistry.CategoryId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

exec dev.pRefreshAllViews

PRINT N'Altering [product].[pCategoryGetViewAll]'
GO
ALTER PROCEDURE [product].[pCategoryGetViewAll]
	@ChannelId	int,
	@LanguageId	int
as
begin
	select
		c.*
	from
		product.vCustomCategoryView as c
	where
		[c].[Category.ChannelId] = @ChannelId
		and [c].[Category.LanguageId] = @LanguageId
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vProductRegistryDeliveryTime]'
GO





CREATE VIEW [productlek].[vProductRegistryDeliveryTime]
AS
SELECT
	[prdt].[DeliveryTimeId] AS 'DeliveryTime.Id',
	[prdt].[From] AS 'DeliveryTime.From',
	[prdt].[To] AS 'DeliveryTime.To',
	[prdt].[Format] AS 'DeliveryTime.Format',
	[prdt].[ProductRegistryId] AS 'DeliveryTime.ProductRegistryId',
	[pmc].[ChannelId] AS 'DeliveryTime.ProductRegistryChannelId'
FROM
	[productlek].[tProductRegistryDeliveryTime] AS prdt
	INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [prdt].[ProductRegistryId]




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[vBlockTopList]'
GO
ALTER VIEW [addon].[vBlockTopList]
AS
	SELECT
		[BlockId]					AS 'BlockTopList.BlockId',
		[ColumnCount]				AS 'BlockTopList.ColumnCount',
		[RowCount]					AS 'BlockTopList.RowCount',
		[TotalProductCount]			AS 'BlockTopList.TotalProductCount',
		[IncludeAllCategories]		AS 'BlockTopList.IncludeAllCategories',
		[OrderStatisticsDayCount]	AS 'BlockTopList.OrderStatisticsDayCount',
		[LinkContentNodeId]			AS 'BlockTopList.LinkContentNodeId',
		[CustomUrl]					AS 'BlockTopList.CustomUrl'
	FROM
		[addon].[tBlockTopList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomCategorySecure]'
GO


ALTER VIEW [product].[vCustomCategorySecure]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		[l].[PackageInfo] AS [LekmerCategory.PackageInfo],
		[l].[MonitorThreshold] AS [LekmerCategory.MonitorThreshold],
		[l].[MaxQuantityPerOrder] AS [LekmerCategory.MaxQuantityPerOrder],
		[l].[DeliveryTimeId] AS [LekmerCategory.DeliveryTimeId],
		[dt].*
	FROM
		[product].[vCategorySecure] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]
		LEFT JOIN [productlek].[vDeliveryTimeSecure] dt ON [dt].[DeliveryTime.Id] = [l].[DeliveryTimeId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[pProductPriceSave]'
GO
ALTER PROCEDURE [export].[pProductPriceSave]
	@ChannelId	INT,
	@ProductId	INT,
	@PriceIncludingVat DECIMAL (16, 2),
	@PriceExcludingVat DECIMAL (16, 2),
	@DiscountPriceIncludingVat DECIMAL (16, 2),
	@DiscountPriceExcludingVat DECIMAL (16, 2)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[export].[tProductPrice]
	SET
		[PriceIncludingVat] = @PriceIncludingVat,
		[PriceExcludingVat] = @PriceExcludingVat,
		[DiscountPriceIncludingVat] = @DiscountPriceIncludingVat,
		[DiscountPriceExcludingVat] = @DiscountPriceExcludingVat,
		[IsChanged] = 1
	WHERE
		[ChannelId] = @ChannelId
		AND
		[ProductId] = @ProductId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [export].[tProductPrice]
		(
			[ChannelId],
			[ProductId],
			[PriceIncludingVat],
			[PriceExcludingVat],
			[DiscountPriceIncludingVat],
			[DiscountPriceExcludingVat],
			[IsChanged]
		)
		VALUES
		(
			@ChannelId,
			@ProductId,
			@PriceIncludingVat,
			@PriceExcludingVat,
			@DiscountPriceIncludingVat,
			@DiscountPriceExcludingVat,
			1
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductRecord]'
GO











ALTER VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		[lp].[Lekmer.SupplierId],
		[lp].[Lekmer.PurchasePrice],
		[lp].[Lekmer.PurchaseCurrencyId],
		[lp].[Lekmer.SupplierArticleNumber],
		[dt].*
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		LEFT JOIN [productlek].[vDeliveryTimeSecure] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId]










GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductRecordGetAllByIdList]'
GO
CREATE PROCEDURE [productlek].[pProductRecordGetAllByIdList]
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @PriceListId INT
	SET @PriceListId = (SELECT TOP(1) [PriceListId] FROM [product].[tPriceList])

	SELECT
		[p].*,
		[pli].*
	FROM
		[product].[vCustomProductRecord] p
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl ON [pl].[Id] = [p].[Product.Id]
		LEFT JOIN [product].[vCustomPriceListItem] AS pli ON [pli].[Price.ProductId] = [p].[Product.Id] AND [pli].[Price.PriceListId] = @PriceListId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts]'
GO









ALTER VIEW [lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts]
AS 
	SELECT
		p.*,
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.[ParentContentNodeId] AS 'Product.ParentContentNodeId',
		i.*,
		[dt].*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductWithoutStatusFilter] p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] AS ssmc ON ssmc.[ChannelId] = c.[Channel.Id]
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON p.[Product.Id] = pssr.[ProductId] AND ssmc.[SiteStructureRegistryId] = pssr.[SiteStructureRegistryId]
		LEFT JOIN [media].[vCustomImage] AS i ON i.[Image.MediaId] = p.[Product.MediaId] AND i.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [c].[Channel.Id]








GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[vProductPrice]'
GO

ALTER VIEW [export].[vProductPrice]
AS
	SELECT
		pp.[ChannelId] 'ProductPrice.ChannelId',
		pp.[ProductId] 'ProductPrice.ProductId',
		pp.[PriceIncludingVat] 'ProductPrice.PriceIncludingVat',
		pp.[PriceExcludingVat] 'ProductPrice.PriceExcludingVat',
		pp.[DiscountPriceIncludingVat] 'ProductPrice.DiscountPriceIncludingVat',
		pp.[DiscountPriceExcludingVat] 'ProductPrice.DiscountPriceExcludingVat',
		pp.[IsChanged] 'ProductPrice.IsChanged'
	FROM 
		export.tProductPrice  AS pp
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pProductPriceGetAllChanged]'
GO
CREATE PROCEDURE [export].[pProductPriceGetAllChanged]
	@IsChangedFlagRemove BIT
AS 
BEGIN 
	SELECT
		[c].[CommonName] 'Channel',
		[lp].[HYErpId] 'HYErpId',
		[lp].[SupplierArticleNumber] 'SupplierArtNumber',
		[p].[Title]	'Title',
		[pp].[ProductPrice.PriceIncludingVat] 'OriginalPriceIncludingVat',
		[pp].[ProductPrice.PriceExcludingVat] 'OriginalPriceExcludingVat',
		[pp].[ProductPrice.DiscountPriceIncludingVat] 'DiscountPriceIncludingVat',
		[pp].[ProductPrice.DiscountPriceExcludingVat] 'DiscountPriceExcludingVat'
	FROM
		[export].[vProductPrice] pp
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [pp].[ProductPrice.ProductId]
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [pp].[ProductPrice.ProductId]
		INNER JOIN [core].[tChannel] c ON [c].[ChannelId] = [pp].[ProductPrice.ChannelId]
	WHERE
		[pp].[ProductPrice.IsChanged] = 1

	IF (@IsChangedFlagRemove = 1)
	BEGIN
		UPDATE [export].[tProductPrice]
		SET [IsChanged] = 0
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductRegistryDeliveryTimeSave]'
GO
CREATE PROCEDURE [productlek].[pProductRegistryDeliveryTimeSave]
	@Id					INT,
	@ProductRegistryId	INT,
	@FromDays			INT,
	@ToDays				INT,
	@Format				NVARCHAR(25)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[productlek].[tProductRegistryDeliveryTime]
	SET
		[From] = @FromDays,
		[To] = @ToDays,
		[Format] = @Format
	WHERE
		[DeliveryTimeId] = @Id AND
		[ProductRegistryId] = @ProductRegistryId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [productlek].[tProductRegistryDeliveryTime] (
			[ProductRegistryId] ,
			[DeliveryTimeId] ,
			[From] ,
			[To],
			[Format]
		)
		VALUES (
			@ProductRegistryId,
			@Id,
			@FromDays,
			@ToDays,
			@Format
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[tempSupplier]'
GO
CREATE TABLE [integration].[tempSupplier]
(
[SupplierNo] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ActivePassiveCode] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Name] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[Address] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[ZipArea] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[City] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CountryISO] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[LanguageISO] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[VATRate] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Phone1] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Phone2] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tempSupplier] on [integration].[tempSupplier]'
GO
ALTER TABLE [integration].[tempSupplier] ADD CONSTRAINT [PK_tempSupplier] PRIMARY KEY CLUSTERED  ([SupplierNo])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pImportSupplier]'
GO
CREATE PROCEDURE [integration].[pImportSupplier]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			-- update existing suppliers
			UPDATE 
				s
			SET  
				[ActivePassiveCode] = [ts].[ActivePassiveCode],
				[Name] = [ts].[Name],
				[Address] = [ts].[Address],
				[ZipArea] = [ts].[ZipArea],
				[City] = [ts].[City],
				[CountryISO] = [ts].[CountryISO],
				[LanguageISO] = [ts].[LanguageISO],
				[VATRate] = [ts].[VATRate],
				[Phone1] = CASE LTRIM(RTRIM([ts].[Phone1])) WHEN '' THEN NULL ELSE LTRIM(RTRIM([ts].[Phone1])) END,
				[Phone2] = CASE LTRIM(RTRIM([ts].[Phone2])) WHEN '' THEN NULL ELSE LTRIM(RTRIM([ts].[Phone2])) END,
				[Email] = [ts].[Email]
			FROM 
				[productlek].[tSupplier] s
				INNER JOIN [integration].[tempSupplier] ts ON [ts].[SupplierNo] = [s].[SupplierNo]
			
			-- insert new suppliers
			INSERT INTO 
				[productlek].[tSupplier] (
					[SupplierNo],
					[ActivePassiveCode],
					[Name],
					[Address],
					[ZipArea],
					[City],
					[CountryISO],
					[LanguageISO],
					[VATRate],
					[Phone1],
					[Phone2],
					[Email]
				)
				SELECT
					[ts].[SupplierNo],
					[ts].[ActivePassiveCode],
					[ts].[Name],
					[ts].[Address],
					[ts].[ZipArea],
					[ts].[City],
					[ts].[CountryISO],
					[ts].[LanguageISO],
					[ts].[VATRate],
					CASE LTRIM(RTRIM([ts].[Phone1])) WHEN '' THEN NULL ELSE LTRIM(RTRIM([ts].[Phone1])) END,
					CASE LTRIM(RTRIM([ts].[Phone2])) WHEN '' THEN NULL ELSE LTRIM(RTRIM([ts].[Phone2])) END,
					[ts].[Email]
				FROM
					[integration].[tempSupplier] ts
				WHERE
					[ts].[SupplierNo] NOT IN (SELECT [s].[SupplierNo] FROM [productlek].[tSupplier] s)

		COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		INSERT INTO [integration].[integrationLog] (Data, [Message], [Date], OcuredInProcedure)
		VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductRegistryDeliveryTimeGetAllById]'
GO
CREATE PROCEDURE [productlek].[pProductRegistryDeliveryTimeGetAllById]
	@DeliveryTimeId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    *
	FROM
	    [productlek].[vProductRegistryDeliveryTime]
	WHERE
		[DeliveryTime.Id] = @DeliveryTimeId
	ORDER BY
		[DeliveryTime.ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductLekmer]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateProductLekmer]
AS
BEGIN
	SET NOCOUNT ON 

	-- Delte all ***Missing*** (tmp)
	DELETE FROM integration.tempProduct WHERE ArticleCodeTitle= '***Missing***'

	-- tCategory
	EXEC [integration].[usp_UpdateTCategoryLekmer] -- creates the items if necessary
	EXEC [integration].[usp_UpdateCategoryTitleLekmer] -- update titles if necessary
	
	-- tBrand
	EXEC [integration].[usp_UpdateBrandLekmer]
	EXEC [integration].[usp_UpdateBrandLekmerCreationDate] -- adds the date the brand was created
	
	-- tProduct
	EXEC [integration].[usp_UpdateProductLekmer]
	EXEC [integration].[usp_ImportUpdateLekmerProduct]
	
	-- tPriceListItem
	EXEC [integration].[usp_UpdateProductPRICELekmer]
	
	-- tProductSize
	EXEC [integration].[usp_UpdateSizeLekmer]

	--------------------------------------

	DECLARE 
		@HYArticleNumberFull NVARCHAR(50),
		@HYArticleNumberColorSize NVARCHAR(50),
		@HYArticleNumberColor NVARCHAR(50),
		@HYSizeId NVARCHAR(50),
		@EanCode NVARCHAR(50),
		@NoInStock NVARCHAR(250),
		@ArticleTitle NVARCHAR(250),
		@ArticleTitleFixed NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@BrandId INT,
		@HYSizeValue NVARCHAR(50),
		@Weight NVARCHAR(50),
		@PossibleToDisplay NVARCHAR(100),
		@StockStatusErpId NVARCHAR(50),
		@StockStatusId INT,
		@SupplierId NVARCHAR(500),
		@PurchasePrice VARCHAR(100),
		@PurchaseCurrency VARCHAR(10),
		@SupplierArticleNumber NVARCHAR(50),
		
		@CategoryId INT,
		@Price DECIMAL,
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@LekmerErpId NVARCHAR(25),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL,
		
		@NewFromDate DATETIME,
		@NewToDate DATETIME,

		-- Variables for Channel
		@HYChannel NVARCHAR(40),
		@ChannelId INT,
		@ProductRegistryId INT,
		@PriceListId INT,

		@TradeDoublerProductGroup NVARCHAR(50),

		@ProductIsNew BIT,
		@ProductPriceNotExist BIT,
		@ProductRegistryNotExist BIT,
		
		@PurchaseCurrencyId INT
	
	SET @Vat = 25.0
	
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 4, @NewFromDate)

	------------------------------------

	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			tp.HYarticleId,
			tp.ArticleTitle,
			tp.SizeId,
			(tp.Price/100),
			tp.NoInStock,
			tp.ArticleGroupId,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.BrandId,
			tp.EanCode,
			tp.LekmerArtNo,
			[tp].[Weight],
			[tp].[PossibleToDisplay],
			[tp].[Ref1], -- StockStatusErpId
			[tp].[SupplierId],
			[tp].[PurchasePrice],
			[tp].[PurchaseCurrency],
			[tp].[SupplierArticleNumber]
		FROM
			[integration].tempProduct tp
		--WHERE NOT EXISTS (SELECT 1
		--				  FROM lekmer.tLekmerProduct lp, product.tPriceListItem prp
		--				  WHERE lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
		--					AND lp.ProductId = prp.ProductId
		--					AND SUBSTRING(tp.HYarticleId, 3,1) = prp.PriceListId)

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@HYArticleNumberFull,
			@ArticleTitle,
			@HYSizeValue,
			@Price,
			@NoInStock,
			@VarugruppId,
			@VaruklassId,
			@VarukodId,
			@HYBrandId,
			@EanCode,
			@LekmerErpId,
			@Weight,
			@PossibleToDisplay,
			@StockStatusErpId,
			@SupplierId,
			@PurchasePrice,
			@PurchaseCurrency,
			@SupplierArticleNumber

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			-- Split @HYArticleNumberFull
			-- 001-0000001-1017-108
			SET @HYChannel = SUBSTRING(@HYArticleNumberFull, 1, 3)                  -- [001]-0000001-1017-108
			SET @HYArticleNumberColorSize = SUBSTRING(@HYArticleNumberFull, 5, 17)  -- 001-[0000001-1017-108]
			SET @HYArticleNumberColor = SUBSTRING(@HYArticleNumberFull, 5, 12)      -- 001-[0000001-1017]-108
			SET @HYSizeId = SUBSTRING(@HYArticleNumberFull, 18, 3)                  -- 001-0000001-1017-[108]

			-- find channel id
			SET @ChannelId = (SELECT c.[ChannelId] FROM [lekmer].[tLekmerChannel] c WHERE c.[ErpId] = @HYChannel)
			IF @ChannelId IS NULL
			BEGIN
				-- undefined channel, skip the row
				GOTO NEXT_ROW
			END

			-- find product registry id
			SET @ProductRegistryId = (SELECT [pmc].[ProductRegistryId] FROM [product].[tProductModuleChannel] pmc WHERE [pmc].[ChannelId] = @ChannelId)
			
			-- find price list id
			SET @PriceListId = (
				SELECT TOP 1
					pl.[PriceListId]
				FROM
					[product].[tProductModuleChannel] pmc
					INNER JOIN [product].[tPriceList] pl ON pl.[PriceListRegistryId] = pmc.[PriceListRegistryId]
				WHERE
					pmc.[ChannelId] = @ChannelId
					AND pl.[PriceListStatusId] = 0 -- online
			)

			-- find stock status id
			SET @StockStatusId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [ErpId] = @StockStatusErpId)
			SET @StockStatusId = ISNULL(@StockStatusId, 0) -- Active by default

			-- find product id
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYArticleNumberColor)
			
			---- Skip processing the same @HYErpId + @Channel few times
			--IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			--BEGIN
			--	GOTO PRODUCT_END
			--END ELSE BEGIN
			--	SET	@PrevHYErpId = @HYErpId
			--	SET	@PrevChannel = @Channel
			--END
			
			-- By default product, price and registry already exist
			SET @ProductIsNew = 0
			SET @ProductPriceNotExist = 0
			SET @ProductRegistryNotExist = 0
			
			IF @ProductId IS NULL
			BEGIN
				-- New product, need to prepare some variables
				SET @ProductIsNew = 1
				SET @ProductPriceNotExist = 1
				SET @ProductRegistryNotExist = 1
				
				SET @CategoryId = (SELECT CategoryId 
							   FROM product.tCategory 
							   WHERE ErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, ''))
			
				SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)
				SET @PurchaseCurrencyId = (SELECT [CurrencyId] FROM [core].[tCurrency] WHERE [ISO] = @PurchaseCurrency)
			END
			ELSE
			BEGIN
				-- Product already exist
				
				-- Lets check if product has price
				IF NOT EXISTS ( SELECT 1 FROM product.tPriceListItem prp
								WHERE
									prp.ProductId = @ProductId
									AND
									prp.PriceListId = @PriceListId )
				BEGIN
					SET @ProductPriceNotExist = 1
				END
				
				-- Lets check if product exists in registry
				IF NOT EXISTS ( SELECT 1 FROM product.tProductRegistryProduct
								WHERE
									ProductId = @ProductId
									AND
									ProductRegistryId = @ProductRegistryId )
				BEGIN
					SET @ProductRegistryNotExist = 1
				END
			END
			
			
			-- Now lets do changes
			
			BEGIN TRANSACTION
			
			-- New product
			IF @ProductId IS NULL
			BEGIN
				SET @Data = 'NEW: HYID ' + @HYArticleNumberFull
				
				SET @ArticleTitleFixed = [generic].[fStripIllegalSymbols](@ArticleTitle)
				
				-- tProduct
				INSERT INTO [product].tProduct (
					ErpId,
					EanCode,
					IsDeleted,
					NumberInStock,
					CategoryId,
					Title,
					[Description],
					ProductStatusId
				)
				VALUES (
					@HYArticleNumberColor,
					@EanCode,
					0, -- IsDeleted
					@NoInStock,
					@CategoryId,
					@ArticleTitleFixed,
					'', -- Description
					1 -- Offline
				)
											
				SET @ProductId = SCOPE_IDENTITY()
				
				SET @Data = 'NEW: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
	
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId,
					HYErpId,
					BrandId,
					IsBookable,
					AgeFromMonth,
					AgeToMonth,
					IsNewFrom,
					IsNewTo,
					IsBatteryIncluded,
					ExpectedBackInStock,
					LekmerErpId,
					ShowVariantRelations,
					[Weight],
					[ProductTypeId],
					[IsAbove60L],
					[StockStatusId],
					[SupplierId],
					[PurchasePrice],
					[PurchaseCurrencyId],
					[SupplierArticleNumber]
				)
				VALUES (
					@ProductId, 
					@HYArticleNumberColor,
					@BrandId,
					0, -- IsBookable, 
					0, -- AgeFromMonth
					0, -- AgeToMonth
					@NewFromDate,
					@NewToDate,
					0, -- IsBatteryIncluded
					NULL, --ExpectedBackInStock
					@LekmerErpId,
					1, -- ShowVariantRelations
					CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), @Weight) / 1000),
					1,
					CASE WHEN @PossibleToDisplay IS NULL OR @PossibleToDisplay <> 'G' THEN 0 ELSE 1 END,
					@StockStatusId,
					@SupplierId,
					CONVERT(DECIMAl(16,2), CONVERT(DECIMAl(16,2), @PurchasePrice) / 100.0),
					@PurchaseCurrencyId,
					@SupplierArticleNumber
				)
			END
			
			------------------------------------------------------------------------------------------
			-- tProductRegistryProduct
			-- Insert when product is new and registry not exist
			--	or when price is not exists and registry not exist
			
			-- If price already exists, that could mean product also exists in registry or not if it is restricted
			-- So we need to add product to registry only when price is not yet set
			
			IF @ProductPriceNotExist = 1 AND @ProductRegistryNotExist = 1
			BEGIN
				SET @Data = 'AddRegistry: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
			
				INSERT INTO product.tProductRegistryProduct (
					ProductId,
					ProductRegistryId
				)
				VALUES (
					@ProductId,
					@ProductRegistryId
				)
			END
				
			------------------------------------------------------------------------------------------
			-- tPriceListItem
			-- Insert when not exist
				
			IF @ProductPriceNotExist = 1
			BEGIN
				SET @Data = 'AddPrice: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
			
				INSERT INTO product.tPriceListItem (
					PriceListId,
					ProductId,
					PriceIncludingVat,
					PriceExcludingVat,
					VatPercentage
				)
				VALUES (
					@PriceListId,
					@ProductId, 
					@Price, 
					@Price / (1.0+@Vat/100.0),
					@Vat
				)
			END

			------------------------------------------------------------------------------------------ 
			-- tTradeDoublerProductGroupMapping
			-- Insert when product is new
				
			IF @ProductIsNew = 1
			BEGIN
				SET @Data = 'AddTradeDoubler: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
			
				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '')
													   AND ChannelId = @ChannelId)

				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId
										 AND tTDPGM.ChannelId = @ChannelId)
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							@ChannelId
						)
					END
				END 
			END

			COMMIT

			------------------------------------------------------------------------------------------ 
			-- ProductSize
			-- Run update for each article
			
			SET @Data = 'RunSizeUpdate: HYID ' + @HYArticleNumberFull + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
				
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [integration].[usp_ImportUpdateProductSizesLekmer]
				@HYChannel,
				@ProductId,
				@HYArticleNumberColorSize,
				@HYSizeId,
				@HYSizeValue,
				@NoInStock,
				@Weight,
				@StockStatusId
			
		END TRY
		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
				
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)

		END CATCH

		NEXT_ROW:

		FETCH NEXT FROM cur_product 
			INTO 
				@HYArticleNumberFull,
				@ArticleTitle,
				@HYSizeValue,
				@Price,
				@NoInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@HYBrandId,
				@EanCode,
				@LekmerErpId,
				@Weight,
				@PossibleToDisplay,
				@StockStatusErpId,
				@SupplierId,
				@PurchasePrice,
				@PurchaseCurrency,
				@SupplierArticleNumber

	END
	
	CLOSE cur_product
	DEALLOCATE cur_product
	
	EXEC [integration].[usp_UpdateProductBrandLekmer]
	EXEC [integration].[usp_ChannelRestrictionsProducts]
	EXEC [integration].[usp_REATagProduct]
	
	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]
	
	-- Update stock status /active-passive/online-offline /*before package update !!!*/
	EXEC [integration].[usp_UpdateProductStatusLekmer]
	
	-- Add 'Outlet' tag
	-- Disabled 2013-10-31
	--EXEC [integration].[usp_OutletTagProduct]
	
	-- Update package stock
	EXEC [productlek].[pPackageUpdate]
	
	-- Track products chnages to eSales export
	EXEC [integration].[pTrackProductChangesLekmer]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]'
GO
ALTER PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	SELECT
		[p].[ProductId] 'Product.Id', 
		[p].[ItemsInPackage] 'Product.ItemsInPackage',
		[p].[ErpId] 'Product.ErpId',
		[p].[EanCode] 'Product.EanCode',
		[p].[NumberInStock] 'Product.NumberInStock', 
		[p].[CategoryId] 'Product.CategoryId',
		COALESCE([pt].[WebShopTitle], [p].WebShopTitle) 'Product.WebShopTitle',
		COALESCE([pt].[Title], [p].Title) 'Product.Title',
		[p].[ProductStatusId] 'Product.ProductStatusId',
		COALESCE([pt].[Description], [p].[Description]) 'Product.Description',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription]) 'Product.ShortDescription',
		@ChannelId 'Product.ChannelId', 
		@CurrencyId 'Product.CurrencyId',
		@PriceListRegistryId 'Product.PriceListRegistryId',
		[i].*,
		[pssr].[ParentContentNodeId] 'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId] 'Product.TemplateContentNodeId',
		[lp].[Lekmer.BrandId],
		[lp].[Lekmer.IsBookable],
		[lp].[Lekmer.AgeFromMonth],
		[lp].[Lekmer.AgeToMonth],
		[lp].[Lekmer.IsNewFrom],
		[lp].[Lekmer.IsNewTo],
		COALESCE ([lpt].[Product.Measurement], [lp].[Lekmer.Measurement]) 'Lekmer.Measurement',
		[lp].[Lekmer.BatteryTypeId],
		[lp].[Lekmer.NumberOfBatteries],
		[lp].[Lekmer.IsBatteryIncluded],
		[lp].[Lekmer.ExpectedBackInStock],
		[lp].[Lekmer.CreatedDate],
		[lp].[Lekmer.SizeDeviationId],
		[lp].[Lekmer.HasSizes],
		[lp].[Lekmer.LekmerErpId],
		[lp].[Lekmer.ShowVariantRelations],
		[lp].[Lekmer.Weight],
		[lp].[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		[lp].[Lekmer.MonitorThreshold],
		[lp].[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		[dt].*,
		[bt].*,
		[sd].*,
		[pu].[ProductUrl.UrlTitle] 'Lekmer.UrlTitle',
		[rp].[Price] 'Lekmer.RecommendedPrice',
		[pli].*
	FROM
		[product].[tProduct] AS p
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON [lpt].[Product.Id] = [lp].[Lekmer.ProductId] AND [lpt].[Language.Id] = @LanguageId
		LEFT JOIN [lekmer].[vBatteryType] bt ON [bt].[BatteryType.Id] = [lp].[Lekmer.BatteryTypeId]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON [sd].[SizeDeviation.Id] = [lp].[Lekmer.SizeDeviationId]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		LEFT JOIN [productlek].[vDeliveryTime] AS dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = @ChannelId
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl	ON [pl].[Id] = [p].[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
		AND [lp].[Lekmer.SellOnlyInPackage] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pCategorySave]'
GO
ALTER PROCEDURE [lekmer].[pCategorySave]
	@CategoryId					INT,
	@Title						NVARCHAR(50),
	@AllowMultipleSizesPurchase	BIT,
	@PackageInfo				NVARCHAR(MAX),
	@MonitorThreshold			INT,
	@MaxQuantityPerOrder		INT,
	@DeliveryTimeId				INT = NULL
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tCategory]
	SET
		[Title] = @Title
	WHERE
		[CategoryId] = @CategoryId
	
	SET NOCOUNT OFF
		
	UPDATE
		[lekmer].[tLekmerCategory]
	SET
		[AllowMultipleSizesPurchase] = @AllowMultipleSizesPurchase,
		[PackageInfo] = @PackageInfo,
		[MonitorThreshold] = @MonitorThreshold,
		[MaxQuantityPerOrder] = @MaxQuantityPerOrder,
		[DeliveryTimeId] = @DeliveryTimeId
	WHERE
		[CategoryId] = @CategoryId
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			[lekmer].[tLekmerCategory]
		( 
			[CategoryId],
			[AllowMultipleSizesPurchase],
			[PackageInfo],
			[MonitorThreshold],
			[MaxQuantityPerOrder],
			[DeliveryTimeId]
		)
		VALUES 
		(
			@CategoryId,
			@AllowMultipleSizesPurchase,
			@PackageInfo,
			@MonitorThreshold,
			@MaxQuantityPerOrder,
			@DeliveryTimeId
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSave]'
GO
ALTER PROCEDURE [lekmer].[pProductSave]
	@ProductId				INT,
	@ItemsInPackage			INT,
	@ErpId					VARCHAR(50),
	@EANCode				VARCHAR(20),
	@Title					NVARCHAR(256),
	@WebShopTitle			NVARCHAR(256),
	@ProductStatusId		INT,
	@Description			NVARCHAR(MAX),
	@ShortDescription		NVARCHAR(MAX),
	@CategoryId				INT,
	@MediaId				INT,
	@BrandId				INT,
	@AgeFromMonth			INT,
	@AgeToMonth				INT,
	@IsBookable				BIT,
	@IsNewFrom				DATETIME,
	@IsNewTo				DATETIME,
	@Measurement			NVARCHAR(MAX),
	@BatteryTypeId			INT,
	@NumberOfBatteries		INT,
	@IsBatteryIncluded		BIT,
	@ExpectedBackInStock	DATETIME,
	@CreatedDate			DATETIME,
	@SizeDeviationId		INT,
	@ShowVariantRelations	BIT,
	@MonitorThreshold		INT,
	@MaxQuantityPerOrder	INT,
	@DeliveryTimeId			INT = NULL,
	@SellOnlyInPackage		BIT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tProduct]
	SET
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	WHERE
		[ProductId] = @ProductId
		
	UPDATE
		[lekmer].[tLekmerProduct]
	SET
		[BrandId]				= @BrandId,
		[AgeFromMonth]			= @AgeFromMonth,
		[AgeToMonth]			= @AgeToMonth,
		[IsBookable]			= @IsBookable,
		[IsNewFrom]				= @IsNewFrom,
		[IsNewTo]				= @IsNewTo,
		[Measurement]			= @Measurement,
		[BatteryTypeId]			= @BatteryTypeId,
		[NumberOfBatteries]		= @NumberOfBatteries,
		[IsBatteryIncluded]		= @IsBatteryIncluded,
		[ExpectedBackInStock]	= @ExpectedBackInStock,
		[CreatedDate]			= @CreatedDate,
		[SizeDeviationId]		= @SizeDeviationId,
		[ShowVariantRelations]	= @ShowVariantRelations,
		[MonitorThreshold]		= @MonitorThreshold,
		[MaxQuantityPerOrder]	= @MaxQuantityPerOrder,
		[DeliveryTimeId]		= @DeliveryTimeId,
		[SellOnlyInPackage]		= @SellOnlyInPackage
	WHERE
		[ProductId] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductGetAllByIdListWithoutAnyFilter]'
GO
CREATE PROCEDURE [productlek].[pProductGetAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)

	SELECT
		[p].[ProductId]			'Product.Id',
		[p].[ErpId]				'Product.ErpId',
		[p].[EanCode]			'Product.EanCode',
		[p].[CategoryId]		'Product.CategoryId',
		[p].[ProductStatusId]	'Product.ProductStatusId',
		[p].[NumberInStock]		'Product.NumberInStock',
		[p].[ItemsInPackage]	'Product.ItemsInPackage',
		COALESCE([pt].[Title], [p].[Title])							'Product.Title',
		COALESCE([pt].[WebShopTitle], [p].[WebShopTitle])			'Product.WebShopTitle',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription])	'Product.ShortDescription',
		[lp].[Lekmer.BrandId],
		[lp].[Lekmer.IsBookable],
		[lp].[Lekmer.IsNewFrom],
		[lp].[Lekmer.IsNewTo],
		[lp].[Lekmer.CreatedDate],
		[lp].[Lekmer.LekmerErpId],
		[lp].[Lekmer.HasSizes],
		[lp].[Lekmer.ShowVariantRelations],
		[lp].[Lekmer.Weight],
		[lp].[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		[lp].[Lekmer.MonitorThreshold],
		[lp].[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		[pu].[ProductUrl.UrlTitle]		'Lekmer.UrlTitle',
		[rp].[Price]					'Lekmer.RecommendedPrice',
		[pssr].[ParentContentNodeId]	'Product.ParentContentNodeId',
		[i].*,
		[dt].*,
		[pli].*
	FROM
		[product].[tProduct] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = @ChannelId
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl ON [pl].[Id] = [p].[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND [pli].[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pCategoryGetViewById]'
GO
ALTER PROCEDURE [product].[pCategoryGetViewById]
	@CategoryId	int,
	@ChannelId	int
as
begin
	select
		c.*
	from
		product.vCustomCategoryView as c
	where
		c.[Category.Id] = @CategoryId
		and [c].[Category.ChannelId] = @ChannelId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandSave]'
GO
ALTER PROCEDURE [lekmer].[pBrandSave]
	@BrandId				INT,
	@Title					VARCHAR(500),
	@MediaId				INT = NULL,
	@Description 			VARCHAR(MAX) = NULL,
	@ExternalUrl 			VARCHAR(500) = NULL,
	@PackageInfo 			NVARCHAR(MAX) = NULL,
	@MonitorThreshold		INT = NULL,
	@MaxQuantityPerOrder	INT = NULL,
	@DeliveryTimeId			INT = NULL
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM lekmer.tBrand WHERE Title = @Title AND BrandId <> @BrandId)
		RETURN -1

	UPDATE
		[lekmer].[tBrand]
	SET
		[Title] = @Title,	
		[MediaId] = @MediaId,
		[Description] = @Description,
		[ExternalUrl] = @ExternalUrl,
		[PackageInfo] = @PackageInfo,
		[MonitorThreshold] = @MonitorThreshold,
		[MaxQuantityPerOrder] = @MaxQuantityPerOrder,
		[DeliveryTimeId] = @DeliveryTimeId
	WHERE
		[BrandId] = @BrandId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tBrand]
		( 
			[Title],
			[MediaId],
			[Description],
			[ExternalUrl],
			[PackageInfo],
			[MonitorThreshold],
			[MaxQuantityPerOrder],
			[DeliveryTimeId]
		)
		VALUES
		(
			@Title,
			@MediaId ,
			@Description,
			@ExternalUrl,
			@PackageInfo,
			@MonitorThreshold,
			@MaxQuantityPerOrder,
			@DeliveryTimeId
		)
		SET @BrandId = scope_identity()						
	END 
	
	RETURN @BrandId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pDeliveryTimeInsert]'
GO
CREATE PROCEDURE [productlek].[pDeliveryTimeInsert]
AS
BEGIN
	INSERT INTO [productlek].[tDeliveryTime] DEFAULT VALUES
	RETURN CAST(SCOPE_IDENTITY() AS INT)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pSupplierGetAll]'
GO
CREATE PROCEDURE [productlek].[pSupplierGetAll]
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[productlek].[vSupplier]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pBlockTopListSave]'
GO
ALTER PROCEDURE [addon].[pBlockTopListSave]
	@BlockId					INT,
	@ColumnCount				INT,
	@RowCount					INT,
	@TotalProductCount			INT,
	@IncludeAllCategories		BIT,
	@OrderStatisticsDayCount	INT,
	@LinkContentNodeId			INT = NULL,
	@CustomUrl					NVARCHAR(MAX) = NULL
AS
BEGIN
	UPDATE [addon].[tBlockTopList]
	SET
		[ColumnCount]				= @ColumnCount,
		[RowCount]					= @RowCount,
		[TotalProductCount]			= @TotalProductCount,
		[IncludeAllCategories]		= @IncludeAllCategories,
		[OrderStatisticsDayCount]	= @OrderStatisticsDayCount,
		[LinkContentNodeId]			= @LinkContentNodeId,
		[CustomUrl]					= @CustomUrl
	WHERE
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [addon].[tBlockTopList] (
		[BlockId],
		[ColumnCount],
		[RowCount],
		[TotalProductCount],
		[IncludeAllCategories],
		[OrderStatisticsDayCount],
		[LinkContentNodeId],
		[CustomUrl]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@TotalProductCount,
		@IncludeAllCategories,
		@OrderStatisticsDayCount,
		@LinkContentNodeId,
		@CustomUrl
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAllWithoutAnyFilter]'
GO
ALTER PROCEDURE [lekmer].[pProductGetIdAllWithoutAnyFilter]
	@Page				INT = NULL,
	@PageSize			INT,
	@SellOnlyInPackage	BIT = NULL
AS
BEGIN
	DECLARE @sqlCount NVARCHAR(MAX)	
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sqlCount = '
		SELECT COUNT(1)
		FROM
			[product].[tProduct] AS p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		WHERE
			[p].[IsDeleted] = 0'
	IF (@SellOnlyInPackage IS NOT NULL)
	BEGIN
		SET @sqlCount = @sqlCount + ' AND [lp].[SellOnlyInPackage] = ' + CAST(@SellOnlyInPackage AS VARCHAR(10))
	END

	SET @sql = '
		SELECT * FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY [p].[ProductId]) AS Number,
				[p].[ProductId]
			FROM
				[product].[tProduct] AS p
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			WHERE
				[p].[IsDeleted] = 0
				' + CASE WHEN @SellOnlyInPackage IS NOT NULL  THEN + ' AND [lp].[SellOnlyInPackage] = ' + CAST(@SellOnlyInPackage AS VARCHAR(10)) ELSE '' END + '
		) AS Result'
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + '
			AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END

	EXEC (@sqlCount)
	EXEC (@sql)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerCategory]'
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD CONSTRAINT [FK_tLekmerCategory_tDeliveryTime] FOREIGN KEY ([DeliveryTimeId]) REFERENCES [productlek].[tDeliveryTime] ([DeliveryTimeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tBrand]'
GO
ALTER TABLE [lekmer].[tBrand] ADD CONSTRAINT [FK_tBrand_tDeliveryTime] FOREIGN KEY ([DeliveryTimeId]) REFERENCES [productlek].[tDeliveryTime] ([DeliveryTimeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tProductRegistryDeliveryTime]'
GO
ALTER TABLE [productlek].[tProductRegistryDeliveryTime] ADD CONSTRAINT [FK_tProductRegistryDeliveryTime_tDeliveryTime] FOREIGN KEY ([DeliveryTimeId]) REFERENCES [productlek].[tDeliveryTime] ([DeliveryTimeId])
ALTER TABLE [productlek].[tProductRegistryDeliveryTime] ADD CONSTRAINT [FK_tProductRegistryDeliveryTime_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tDeliveryTime] FOREIGN KEY ([DeliveryTimeId]) REFERENCES [productlek].[tDeliveryTime] ([DeliveryTimeId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tBatteryType] FOREIGN KEY ([BatteryTypeId]) REFERENCES [lekmer].[tBatteryType] ([BatteryTypeId]) ON DELETE SET NULL
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tSizeDeviation] FOREIGN KEY ([SizeDeviationId]) REFERENCES [lekmer].[tSizeDeviation] ([SizeDeviationId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [productlek].[tProductType] ([ProductTypeId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tStockStatus] FOREIGN KEY ([StockStatusId]) REFERENCES [productlek].[tStockStatus] ([StockStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tProductColor]'
GO
ALTER TABLE [productlek].[tProductColor] ADD CONSTRAINT [FK_tProductColor_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] ADD CONSTRAINT [FK_tProductPopularity_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductUrl]'
GO
ALTER TABLE [lekmer].[tProductUrl] ADD CONSTRAINT [FK_tProductUrl_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProductTranslation]'
GO
ALTER TABLE [lekmer].[tLekmerProductTranslation] ADD CONSTRAINT [FK_tLekmerProductTranslation_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductSimilar]'
GO
ALTER TABLE [lekmer].[tProductSimilar] ADD CONSTRAINT [FK_tProductSimilar_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
ALTER TABLE [lekmer].[tProductSimilar] ADD CONSTRAINT [FK_tProductSimilar_tLekmerProduct1] FOREIGN KEY ([SimilarProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] ADD CONSTRAINT [FK_tProductSize_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductUrlHistory]'
GO
ALTER TABLE [lekmer].[tProductUrlHistory] ADD CONSTRAINT [FK_tProductUrlHistory_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProductIcon]'
GO
ALTER TABLE [lekmer].[tLekmerProductIcon] ADD CONSTRAINT [FK_tLekmerProductIcon_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tBlockTopList]'
GO
ALTER TABLE [addon].[tBlockTopList] ADD CONSTRAINT [FK_tBlockTopList_tContentNode] FOREIGN KEY ([LinkContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
