SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [campaignlek].[tCampaignActionConfigurator]'
GO
ALTER TABLE [campaignlek].[tCampaignActionConfigurator] ADD
[IncludeAllProducts] [bit] NOT NULL CONSTRAINT [DF_tCampaignActionConfigurator_IncludeAllProducts] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[tCampaignActionExcludeCategory]'
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeCategory] ADD
[IncludeSubCategories] [bit] NOT NULL CONSTRAINT [DF_tCampaignActionExcludeCategory_IncludeSubCategories] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[tCampaignActionIncludeCategory]'
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeCategory] ADD
[IncludeSubCategories] [bit] NOT NULL CONSTRAINT [DF_tCampaignActionIncludeCategory_IncludeSubCategories] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
