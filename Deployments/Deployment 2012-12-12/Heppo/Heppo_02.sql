ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD [ConfigId] INT NULL
GO
DECLARE @ConfigId INT
DECLARE @ProductActionId INT
DECLARE @IncludeAllProducts BIT
	
DECLARE cur_action CURSOR FAST_FORWARD FOR
SELECT
	[ppda].[ProductActionId],
	[ppda].[IncludeAllProducts]
FROM
	[campaign].[tPercentagePriceDiscountAction] ppda
ORDER BY [ppda].[ProductActionId]

OPEN cur_action
FETCH NEXT FROM cur_action
	INTO
		@ProductActionId,
		@IncludeAllProducts

WHILE @@FETCH_STATUS = 0
BEGIN
	-- Create new Config
	INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
	SET @ConfigId = SCOPE_IDENTITY()
	
	-- Set Config to action
	UPDATE [campaign].[tPercentagePriceDiscountAction] 
	SET [ConfigId] = @ConfigId 
	WHERE [ProductActionId] = @ProductActionId
	
	-- Update Config with 'IncludeAllProducts'
	UPDATE [campaignlek].[tCampaignActionConfigurator] 
	SET [IncludeAllProducts] = @IncludeAllProducts 
	WHERE [CampaignActionConfiguratorId] = @ConfigId
	
	-- Add include\exclude items to Config from action
	INSERT INTO [campaignlek].[tCampaignActionExcludeProduct] ([ConfigId], [ProductId])
	SELECT @ConfigId, [ProductId]
	FROM [campaign].[tPercentagePriceDiscountActionExcludeProduct] 
	WHERE [ProductActionId] = @ProductActionId
	
	INSERT INTO [campaignlek].[tCampaignActionExcludeCategory] ([ConfigId], [CategoryId])
	SELECT @ConfigId, [CategoryId]
	FROM [campaign].[tPercentagePriceDiscountActionExcludeCategory]
	WHERE [ProductActionId] = @ProductActionId
	
	INSERT INTO [campaignlek].[tCampaignActionExcludeBrand] ([ConfigId], [BrandId])
	SELECT @ConfigId, [BrandId]
	FROM [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]
	WHERE [ProductActionId] = @ProductActionId

	INSERT INTO [campaignlek].[tCampaignActionIncludeProduct] ([ConfigId], [ProductId])
	SELECT @ConfigId, [ProductId]
	FROM [campaign].[tPercentagePriceDiscountActionIncludeProduct]
	WHERE [ProductActionId] = @ProductActionId
	
	INSERT INTO [campaignlek].[tCampaignActionIncludeCategory] ([ConfigId], [CategoryId])
	SELECT @ConfigId, [CategoryId]
	FROM [campaign].[tPercentagePriceDiscountActionIncludeCategory]
	WHERE [ProductActionId] = @ProductActionId
	
	INSERT INTO [campaignlek].[tCampaignActionIncludeBrand] ([ConfigId], [BrandId])
	SELECT @ConfigId, [BrandId]
	FROM [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]
	WHERE [ProductActionId] = @ProductActionId

	FETCH NEXT FROM cur_action 
		INTO 
			@ProductActionId,
			@IncludeAllProducts
END

CLOSE cur_action
DEALLOCATE cur_action


ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ALTER COLUMN ConfigId INT NOT NULL










--run data move script

--Create table tPercentagePriceDiscountAction select ConfigId
 
--after alter view [vPercentagePriceDiscountAction] run refresh views
