SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionIncludeCategory]'
GO
--ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionCategory_tPercentagePriceDiscountAction]
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionCategory_tCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionExcludeProduct]'
GO
--ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeProduct_tPercentagePriceDiscountAction]
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeProduct_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]'
GO
--ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionIncludeBrand] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionIncludeBrand_tPercentagePriceDiscountAction]
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionIncludeBrand] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionIncludeBrand_tBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionIncludeProduct]'
GO
--ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionProduct_tPercentagePriceDiscountAction]
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionProduct_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]'
GO
--ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionExcludeBrand] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeBrand_tPercentagePriceDiscountAction]
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionExcludeBrand] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeBrand_tBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionExcludeCategory]'
GO
--ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeCategory_tPercentagePriceDiscountAction]
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeCategory_tCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tPercentagePriceDiscountActionIncludeCategory]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory] DROP CONSTRAINT [PK_tPercentagePriceDiscountActionCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tPercentagePriceDiscountActionExcludeProduct]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct] DROP CONSTRAINT [PK_tPercentagePriceDiscountActionExcludeProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionIncludeBrand] DROP CONSTRAINT [PK_tPercentagePriceDiscountActionIncludeBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tPercentagePriceDiscountActionIncludeProduct]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct] DROP CONSTRAINT [PK_tPercentagePriceDiscountActionProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionExcludeBrand] DROP CONSTRAINT [PK_tPercentagePriceDiscountActionExcludeBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tPercentagePriceDiscountActionExcludeCategory]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory] DROP CONSTRAINT [PK_tPercentagePriceDiscountActionExcludeCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionIncludeCategoryGetIdAll]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionIncludeCategoryGetIdAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionIncludeCategoryInsert]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionIncludeCategoryInsert]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionExcludeProductGetIdAll]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeProductGetIdAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pPercentagePriceDiscountActionIncludeBrandInsert]'
GO
DROP PROCEDURE [campaignlek].[pPercentagePriceDiscountActionIncludeBrandInsert]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionIncludeProductInsert]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionIncludeProductInsert]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionIncludeCategoryGetIdAllSecure]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionIncludeCategoryGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionExcludeCategoryGetIdAll]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeCategoryGetIdAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pPercentagePriceDiscountActionIncludeBrandDeleteAll]'
GO
DROP PROCEDURE [campaignlek].[pPercentagePriceDiscountActionIncludeBrandDeleteAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pPercentagePriceDiscountActionExcludeBrandDeleteAll]'
GO
DROP PROCEDURE [campaignlek].[pPercentagePriceDiscountActionExcludeBrandDeleteAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionExcludeProductDeleteAll]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeProductDeleteAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pPercentagePriceDiscountActionExcludeBrandGetIdAll]'
GO
DROP PROCEDURE [campaignlek].[pPercentagePriceDiscountActionExcludeBrandGetIdAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pPercentagePriceDiscountActionExcludeBrandInsert]'
GO
DROP PROCEDURE [campaignlek].[pPercentagePriceDiscountActionExcludeBrandInsert]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[pPercentagePriceDiscountActionIncludeBrandGetIdAll]'
GO
DROP PROCEDURE [campaignlek].[pPercentagePriceDiscountActionIncludeBrandGetIdAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionIncludeProductGetIdAll]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionIncludeProductGetIdAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionExcludeCategoryDeleteAll]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeCategoryDeleteAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionIncludeProductDeleteAll]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionIncludeProductDeleteAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionExcludeCategoryGetIdAllSecure]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeCategoryGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionIncludeCategoryDeleteAll]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionIncludeCategoryDeleteAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionExcludeCategoryInsert]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeCategoryInsert]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionExcludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionIncludeProductGetIdAllSecure]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionIncludeProductGetIdAllSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[pPercentagePriceDiscountActionExcludeProductInsert]'
GO
DROP PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeProductInsert]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[tPercentagePriceDiscountActionExcludeCategory]'
GO
DROP TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]'
GO
DROP TABLE [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[tPercentagePriceDiscountActionIncludeProduct]'
GO
DROP TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]'
GO
DROP TABLE [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[tPercentagePriceDiscountActionExcludeProduct]'
GO
DROP TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [campaign].[tPercentagePriceDiscountActionIncludeCategory]'
GO
DROP TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCartItemsGroupValueCondition]'
GO
CREATE TABLE [campaignlek].[tCartItemsGroupValueCondition]
(
[ConditionId] [int] NOT NULL,
[ConfigId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemsGroupValueCondition] on [campaignlek].[tCartItemsGroupValueCondition]'
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueCondition] ADD CONSTRAINT [PK_tCartItemsGroupValueCondition] PRIMARY KEY CLUSTERED  ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCartItemsGroupValueConditionCurrency]'
GO
CREATE TABLE [campaignlek].[tCartItemsGroupValueConditionCurrency]
(
[ConditionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemsGroupValueConditionCurrency] on [campaignlek].[tCartItemsGroupValueConditionCurrency]'
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueConditionCurrency] ADD CONSTRAINT [PK_tCartItemsGroupValueConditionCurrency] PRIMARY KEY CLUSTERED  ([ConditionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemsGroupValueConditionSave]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionSave]
	@ConditionId		INT,
	@IncludeAllProducts	BIT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tCartItemsGroupValueCondition]
	SET
		ConfigId = @ConfigId
	WHERE
		[ConditionId] = @ConditionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaignlek].[tCartItemsGroupValueCondition] (
			ConditionId,
			ConfigId
		)
		VALUES (
			@ConditionId,
			@ConfigId
		)
	END
	
	UPDATE 
		[campaignlek].[tCampaignActionConfigurator]
	SET 
		IncludeAllProducts = @IncludeAllProducts
	WHERE 
		[CampaignActionConfiguratorId] = @ConfigId
	
	RETURN @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[fnGetPercentagePriceDiscountActionCategories]'
GO
CREATE FUNCTION [integration].[fnGetPercentagePriceDiscountActionCategories](
	@ProductActionId INT,
	@IsIncludeCategories BIT
)
RETURNS @tCategories TABLE (CategoryId INT)
AS 
BEGIN
	IF @IsIncludeCategories = 1
	BEGIN
		INSERT @tCategories ([CategoryId])
		SELECT 
			[ic].[CategoryId] 
		FROM 
			[campaignlek].[tCampaignActionIncludeCategory] ic
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ic].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
		UNION
		SELECT 
			[sc].[CategoryId] 
		FROM 
			[campaignlek].[tCampaignActionIncludeCategory] ic
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ic].[ConfigId]
			CROSS APPLY [product].[fnGetSubCategories] ([ic].[CategoryId]) sc
		WHERE
			a.ProductActionId = @ProductActionId
			AND [ic].[IncludeSubCategories] = 1
	END
	ELSE
	BEGIN
		INSERT @tCategories ([CategoryId])
		SELECT 
			[ec].[CategoryId] 
		FROM 
			[campaignlek].[tCampaignActionExcludeCategory] ec
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ec].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
		UNION
		SELECT 
			[sc].[CategoryId] 
		FROM 
			[campaignlek].[tCampaignActionExcludeCategory] ec
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ec].[ConfigId]
			CROSS APPLY [product].[fnGetSubCategories] ([ec].[CategoryId]) sc
		WHERE
			a.ProductActionId = @ProductActionId
			AND [ec].[IncludeSubCategories] = 1
	END
	
	RETURN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemsGroupValueConditionCurrencyInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionCurrencyInsert]
	@ConditionId	INT,
	@CurrencyId		INT,
	@Value			DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tCartItemsGroupValueConditionCurrency] (
		ConditionId,
		CurrencyId,
		Value
	)
	VALUES (
		@ConditionId,
		@CurrencyId,
		@Value
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeCategoryGetIdAllRecursive]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryGetIdAllRecursive]
	@ConfigId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryIteration (CategoryId, IncludeSubCategories)
	SELECT CategoryId, IncludeSubCategories
	FROM campaignlek.tCampaignActionExcludeCategory
	WHERE ConfigId = @ConfigId

	DECLARE @tCategoryResult TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryResult (CategoryId, IncludeSubCategories)
	SELECT CategoryId, IncludeSubCategories FROM @tCategoryIteration
	
	DECLARE @CategoryId INT, @IncludeSubCategories BIT

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 CategoryId FROM @tCategoryIteration)
		SET @IncludeSubCategories = (SELECT TOP 1 IncludeSubCategories FROM @tCategoryIteration)
		
		IF (@IncludeSubCategories = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult ([CategoryId], [IncludeSubCategories])
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE [CategoryId] = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCartItemsGroupValueCondition]'
GO
CREATE VIEW [campaignlek].[vCartItemsGroupValueCondition]
AS
	SELECT
		cigvc.[ConditionId] AS 'CartItemsGroupValueCondition.ConditionId',
		cac.[IncludeAllProducts] AS 'CartItemsGroupValueCondition.IncludeAllProducts',
		cigvc.[ConfigId] AS 'CartItemsGroupValueCondition.ConfigId',
		cc.*
	FROM
		[campaignlek].[tCartItemsGroupValueCondition] cigvc
		INNER JOIN [campaign].[vCustomCondition] cc ON cc.[Condition.Id] = cigvc.[ConditionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = cigvc.[ConfigId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemsGroupValueConditionGetById]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionGetById]
	@ConditionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartItemsGroupValueCondition]
	WHERE
		[CartItemsGroupValueCondition.ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemsGroupValueConditionCurrencyDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionCurrencyDeleteAll]
	@ConditionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCartItemsGroupValueConditionCurrency]
	WHERE
		[ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemsGroupValueConditionDelete]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionDelete]
	@ConditionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaignlek].[tCartItemsGroupValueCondition] WHERE [ConditionId] = @ConditionId)

	EXEC [campaignlek].[pCampaignActionIncludeBrandDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeBrandDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeCategoryDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeCategoryDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeProductDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeProductDeleteAll] @ConfigId
	
	EXEC [campaignlek].[pCartItemsGroupValueConditionCurrencyDeleteAll] @ConditionId

	DELETE FROM [campaignlek].[tCartItemsGroupValueCondition]
	WHERE [ConditionId] = @ConditionId
	
	DELETE FROM [campaignlek].[tCampaignActionConfigurator]
	WHERE CampaignActionConfiguratorId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pReportOutOfStockRelationLists]'
GO
CREATE PROCEDURE [integration].[pReportOutOfStockRelationLists]
AS
BEGIN
	DECLARE @tRelationListProduct TABLE (
			RelationListId INT,
			RelationListTitle NVARCHAR(256),
			RelationListCommonName VARCHAR(50),
			ProductId INT,
			ProductErpId VARCHAR(50),
			ProductTitle NVARCHAR(256))

	INSERT INTO @tRelationListProduct (
		[RelationListId],
		[RelationListTitle],
		[RelationListCommonName],
		[ProductId],
		[ProductErpId],
		[ProductTitle])
	SELECT
		[rl].[RelationListId] 'RelationListId',
		[rl].[Title] 'RelationListTitle',
		[rlt].[CommonName] 'RelationListCommonName',
		[rlp].[ProductId] 'ProductId',
		[p].[ErpId],
		[p].[Title]
	FROM [product].[tRelationList] rl
		 INNER JOIN [product].[tRelationListType] rlt ON [rlt].[RelationListTypeId] = [rl].[RelationListTypeId]
		 INNER JOIN [product].[tRelationListProduct] rlp ON [rlp].[RelationListId] = [rl].[RelationListId]
		 INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [rlp].[ProductId]
		 CROSS APPLY (
				SELECT SUM(ps.[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId]
			) s
	WHERE
		(([s].[NumberInStock] IS NULL AND [p].[NumberInStock] < 1)
		OR
		([s].[NumberInStock] IS NOT NULL AND [s].[NumberInStock] < 1))
		AND 
		[rl].[RelationListTypeId] <> 1000001
	ORDER BY
		[rl].[RelationListId]


	SELECT * FROM @tRelationListProduct

	SELECT
		[rl].[RelationListId] 'RelationListId',
		[rl].[Title] 'RelationListTitle',
		[rlt].[CommonName] 'RelationListCommonName',
		[rlp].[ProductId] 'ProductId',
		[p].[ErpId] 'ProductErpId',
		[p].[Title] 'ProductTitle'
	FROM [product].[tRelationList] rl
		 INNER JOIN [product].[tRelationListType] rlt ON [rlt].[RelationListTypeId] = [rl].[RelationListTypeId]
		 INNER JOIN [product].[tProductRelationList] rlp ON [rlp].[RelationListId] = [rl].[RelationListId]
		 INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [rlp].[ProductId]
	WHERE
		[rl].[RelationListId] IN (SELECT [RelationListId] FROM @tRelationListProduct)
	ORDER BY
		[rl].[RelationListId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vCartItemsGroupValueConditionCurrency]'
GO
CREATE VIEW [campaignlek].[vCartItemsGroupValueConditionCurrency]
AS
	SELECT
		cigvcc.[ConditionId] AS 'CartItemsGroupValueConditionCurrency.ConditionId',
		cigvcc.[CurrencyId] AS 'CartItemsGroupValueConditionCurrency.CurrencyId',
		cigvcc.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tCartItemsGroupValueConditionCurrency] cigvcc
		INNER JOIN [core].[vCustomCurrency] c ON cigvcc.[CurrencyId] = c.[Currency.Id]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeCategoryGetIdAllRecursive]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryGetIdAllRecursive]
	@ConfigId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryIteration (CategoryId, IncludeSubCategories)
	SELECT CategoryId, IncludeSubCategories
	FROM campaignlek.tCampaignActionIncludeCategory
	WHERE ConfigId = @ConfigId

	DECLARE @tCategoryResult TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryResult (CategoryId, IncludeSubCategories)
	SELECT CategoryId, IncludeSubCategories FROM @tCategoryIteration
	
	DECLARE @CategoryId INT, @IncludeSubCategories BIT

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 CategoryId FROM @tCategoryIteration)
		SET @IncludeSubCategories = (SELECT TOP 1 IncludeSubCategories FROM @tCategoryIteration)
		
		IF (@IncludeSubCategories = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult ([CategoryId], [IncludeSubCategories])
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE [CategoryId] = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCartItemsGroupValueConditionCurrencyGetByCondition]'
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionCurrencyGetByCondition]
	@ConditionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartItemsGroupValueConditionCurrency]
	WHERE 
		[CartItemsGroupValueConditionCurrency.ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartItemsGroupValueConditionCurrency]'
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueConditionCurrency] ADD CONSTRAINT [FK_tCartItemsGroupValueConditionCurrency_tCartItemsGroupValueCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaignlek].[tCartItemsGroupValueCondition] ([ConditionId])
ALTER TABLE [campaignlek].[tCartItemsGroupValueConditionCurrency] ADD CONSTRAINT [FK_tCartItemsGroupValueConditionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCartItemsGroupValueCondition]'
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueCondition] ADD CONSTRAINT [FK_tCartItemsGroupValueCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
ALTER TABLE [campaignlek].[tCartItemsGroupValueCondition] ADD CONSTRAINT [FK_tCartItemsGroupValueCondition_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
