SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [campaignlek].[pCampaignActionIncludeCategoryInsert]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryInsert]
	@ConfigId				INT,
	@CategoryId				INT,
	@IncludeSubCategories	BIT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionIncludeCategory] (
		ConfigId,
		CategoryId,
		IncludeSubCategories
	)
	VALUES (
		@ConfigId,
		@CategoryId,
		@IncludeSubCategories
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailProductActionSave]'
GO

ALTER PROCEDURE [campaignlek].[pGiftCardViaMailProductActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT,
	@SendingInterval	INT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailProductAction]
	SET
		SendingInterval = @SendingInterval,
		ConfigId = @ConfigId
	WHERE
		ProductActionId = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaignlek].[tGiftCardViaMailProductAction] (
			ProductActionId,
			SendingInterval,
			ConfigId
		)
		VALUES (
			@ProductActionId,
			@SendingInterval,
			@ConfigId
		)
	END
	
	UPDATE 
		[campaignlek].[tCampaignActionConfigurator]
	SET 
		IncludeAllProducts = @IncludeAllProducts
	WHERE 
		[CampaignActionConfiguratorId] = @ConfigId
	
	RETURN @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pAvailGetProductDiscountPriceOnSite]'
GO
ALTER PROCEDURE [lekmer].[pAvailGetProductDiscountPriceOnSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--set statistics io on
	--set statistics time on
	
	SET NOCOUNT ON;

	DECLARE
		@Channel_SE INT,
		@Channel_NO INT,
		@Channel_DA INT,
		@Channel_FI INT,
		@Channel_FR INT,
		@Channel_NL INT,
		@PriceListId_FI INT,
		@PriceListId_FR INT,
		@PriceListId_NL INT,
		@ProductActionTypeId_Discount INT,
		@ProductActionTypeId_Percentage INT
		
	SELECT @Channel_SE = ChannelId FROM core.tChannel WHERE CommonName = 'Sweden'
	SELECT @Channel_NO = ChannelId FROM core.tChannel WHERE CommonName = 'Norway'
	SELECT @Channel_DA = ChannelId FROM core.tChannel WHERE CommonName = 'Denmark'
	SELECT @Channel_FI = ChannelId FROM core.tChannel WHERE CommonName = 'Finland'
	SELECT @Channel_FR = ChannelId FROM core.tChannel WHERE CommonName = 'France'
	SELECT @Channel_NL = ChannelId FROM core.tChannel WHERE CommonName = 'Netherlands'
	
	SELECT @ProductActionTypeId_Discount = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'ProductDiscount'
	
	SELECT @ProductActionTypeId_Percentage = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'PercentagePriceDiscount'
	
	SELECT @PriceListId_FI = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'Finland'
	
	SELECT @PriceListId_FR = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'France'
	
	SELECT @PriceListId_NL = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'Netherlands'
	
	---- #Campaign
	
	IF OBJECT_ID('tempdb..#Campaign') IS NOT NULL
		DROP TABLE #Campaign

	CREATE TABLE #Campaign
	(
	   CampaignId INT PRIMARY KEY,
	   PriceListId INT
	)

	INSERT INTO #Campaign (
		CampaignId,
		PriceListId
	)
	SELECT
		c.CampaignId,
		pl.PriceListId
	FROM
		campaign.tCampaign c
		LEFT OUTER JOIN campaign.tCampaignModuleChannel cmc ON cmc.CampaignRegistryId = c.CampaignRegistryId
		LEFT OUTER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = cmc.ChannelId
		LEFT OUTER JOIN product.tPriceList pl ON pl.PriceListRegistryId = pmc.PriceListRegistryId
	WHERE
		c.CampaignStatusId = 0
		AND (c.StartDate IS NULL OR c.StartDate <= GETDATE())
		AND (c.EndDate IS NULL OR GETDATE() <= c.EndDate)
		AND cmc.ChannelId in (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI, @Channel_FR, @Channel_NL)

	---- #CampaignAction
	
	IF OBJECT_ID('tempdb..#CampaignAction') IS NOT NULL
		DROP TABLE #CampaignAction

	CREATE TABLE #CampaignAction (
	   CampaignId INT,
	   SortIndex INT,   
	   ProductActionId INT,
	   Ordinal INT
	)

	DECLARE @Index INT
	SET @Index = 1

	INSERT INTO #CampaignAction (
		CampaignId,
		SortIndex,
		ProductActionId,
		Ordinal
	)
	SELECT
		pa.CampaignId,
		@index,
		pa.ProductActionId,
		pa.Ordinal
	FROM
		#Campaign c
		INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId
		LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal < pa.Ordinal --WTF?
	WHERE
		pa2.ProductActionId IS NULL

	WHILE (@@ROWCOUNT > 0) 
	BEGIN
		SET @Index = @Index + 1
  
		INSERT INTO #CampaignAction (
			CampaignId,
			SortIndex,
			ProductActionId,
			Ordinal
		)
		SELECT
			pa.CampaignId,
			@index,
			pa.ProductActionId,
			pa.Ordinal
		FROM
			#Campaign c
			INNER JOIN #CampaignAction tca ON tca.CampaignId = c.CampaignId AND tca.SortIndex = @Index - 1
			INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId AND pa.Ordinal > tca.Ordinal
			LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal > tca.Ordinal AND pa2.Ordinal < pa.Ordinal
		WHERE
			pa2.ProductActionId IS NULL
		OPTION (FORCE ORDER)
	END


	---- #CampaignProduct

	IF OBJECT_ID('tempdb..#CampaignProduct') IS NOT NULL
		DROP TABLE #CampaignProduct

	CREATE TABLE #CampaignProduct (
		CampaignId INT,
		PriceListId INT,
		ProductId INT,
		Price DECIMAL(16, 2) PRIMARY KEY (CampaignId, ProductId) WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		ip.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON ppda.ProductActionId = ca.ProductActionId
		INNER JOIN [campaignlek].[tCampaignActionIncludeProduct] ip ON ip.[ConfigId] = [ppda].[ConfigId]

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		pdai.ProductId,
		NULL
	from
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = c.PriceListId AND pl.CurrencyId = pdai.CurrencyId

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		p.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
					INNER JOIN product.tProduct p ON p.CategoryId = ic.CategoryId

	-- Set product prices

	UPDATE cp
	SET Price = pli.PriceIncludingVat
	FROM
		#Campaign c
		INNER JOIN #CampaignProduct cp ON cp.CampaignId = c.CampaignId
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = c.PriceListId AND pli.ProductId = cp.ProductId

	DECLARE @LastIndex INT
	SET @Index = 1
	SELECT @LastIndex = MAX(SortIndex) FROM #CampaignAction

	WHILE (@Index <= @LastIndex)
	BEGIN
		UPDATE cp
		SET Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Percentage
			INNER JOIN campaign.tPercentagePriceDiscountAction ppda ON ppda.ProductActionId = pa.ProductActionId
			INNER JOIN [campaignlek].[tCampaignActionIncludeProduct] ip ON ip.[ConfigId] = [ppda].[ConfigId]
			INNER JOIN #CampaignProduct cp ON cp.ProductId = ip.ProductId AND cp.CampaignId = ca.CampaignId
		WHERE
			ca.SortIndex = @Index

		UPDATE cp
		SET Price = pdai.DiscountPrice
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Discount
			INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
			INNER JOIN #CampaignProduct cp ON cp.ProductId = pdai.ProductId AND cp.CampaignId = ca.CampaignId
			INNER JOIN product.tPriceList pl ON pl.PriceListId = cp.PriceListId AND pl.CurrencyId = pdai.CurrencyId
		WHERE
			ca.SortIndex = @Index
			AND (cp.Price > pdai.DiscountPrice OR cp.Price IS NULL)

		SET @Index = @Index + 1
	END

	DELETE cp FROM #CampaignProduct cp WHERE Price IS NULL

	UPDATE cp
	SET price = CASE
					WHEN (c.PriceListId = @PriceListId_FI) THEN Price
					ELSE ROUND(Price, 0)
				END
	FROM
		#CampaignProduct cp
		INNER JOIN #Campaign c ON c.CampaignId = cp.CampaignId

	---- #BestCampaignPrice

	IF OBJECT_ID('tempdb..#BestCampaignPrice') IS NOT NULL
		DROP TABLE #BestCampaignPrice

	CREATE TABLE #BestCampaignPrice (
		ProductId INT,
		PriceListId INT,
		CampaignId INT,
		Price DECIMAL(16, 2)
	)

	INSERT INTO #BestCampaignPrice (
		ProductId,
		PriceListId,
		CampaignId,
		Price) -- Fel
	SELECT
		cp.ProductId,
		cp.PriceListId,
		cp.CampaignId,
		cp.Price
	FROM
		#CampaignProduct cp
		LEFT OUTER JOIN #CampaignProduct cp2 
			ON cp2.PriceListId = cp.PriceListId
				AND cp2.ProductId = cp.ProductId
				AND (cp2.Price < cp.Price OR cp2.Price = cp.Price AND cp2.CampaignId > cp.CampaignId)      
	WHERE cp2.ProductId IS NULL

	DROP TABLE #Campaign
	DROP TABLE #CampaignAction
	DROP TABLE #CampaignProduct

	---- @PriceList
	
	DECLARE @PriceList TABLE (
		PriceListId INT PRIMARY KEY,
		ChannelId INT
	)

	INSERT INTO @PriceList 
	SELECT
		pl.PriceListId,
		c.ChannelId
	FROM
		core.tChannel c
		INNER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = c.ChannelId
		INNER JOIN product.tProductRegistry pr ON pr.ProductRegistryId = pmc.ProductRegistryId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = pr.ProductRegistryId
	WHERE
		c.ChannelId IN (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI, @Channel_FR, @Channel_NL)

	---- Result

	SELECT 
		pl.ChannelId,
		--ll.HYErpId,
		pli.ProductId,
		--pli.PriceListId,
		--pli.PriceIncludingVat AS ListPrice,
		--bcp.Price AS DiscountPrice,
		CASE 
			WHEN (bcp.Price IS NULL OR bcp.Price > pli.PriceIncludingVat) THEN pli.PriceIncludingVat
			ELSE bcp.Price 
		END AS SitePrice
		--CAST(((1-(bcp.Price/pli.PriceIncludingVat)) * 100) AS DECIMAL (18,2)) AS 'Total discount %',
	FROM
		@PriceList pl
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = pl.PriceListId
		INNER JOIN product.tProduct t ON t.ProductId = pli.ProductId
		LEFT OUTER JOIN #BestCampaignPrice bcp ON bcp.PriceListId = pl.PriceListId AND bcp.ProductId = t.ProductId
		LEFT OUTER JOIN campaign.tCampaign c ON c.CampaignId = bcp.CampaignId
		INNER JOIN lekmer.tLekmerProduct ll ON ll.ProductId = pli.ProductId
	WHERE
		c.CampaignId IS NOT NULL
	ORDER BY
		pl.ChannelId
		
   DROP TABLE #BestCampaignPrice
   
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pPercentagePriceDiscountActionGetById]'
GO
ALTER PROCEDURE [campaign].[pPercentagePriceDiscountActionGetById]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaign].[vCustomPercentagePriceDiscountAction]
	WHERE
		[PercentagePriceDiscountAction.ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_REATagProduct]'
GO
ALTER PROCEDURE [integration].[usp_REATagProduct]

AS
BEGIN	
	begin try
	begin transaction

		-- 1. untag all products as "rea (Sale)"
		-- 2. get all active campaigns 
		-- 3. get all actions related to those active campaigns
		-- 4. tag all products assosiated with these Actions/campaigns with Rea Tag
		declare @TagId int
		set @TagId = (select TagId from lekmer.tTag where Value = 'Sale')
		
		delete from lekmer.tProductTag
		where TagId = @TagId
			
		insert into lekmer.tProductTag(ProductId, TagId)
		select distinct
			z.ProductId,
			@TagId
		from								-- lekmer.tProductDiscountActionItem innehåller alla discout priser
			(select coalesce(d.ProductId , b.ProductId) ProductId --,c.ProductActionId
				from campaign.tProductAction c
					left join lekmer.tProductDiscountActionItem b on c.ProductActionId = b.ProductActionId
					LEFT JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [c].[ProductActionId]
					INNER JOIN [campaignlek].[tCampaignActionIncludeProduct] d ON d.[ConfigId] = ppda.[ConfigId] -- only include product!
				where CampaignId in (
									select CampaignId from campaign.tCampaign
									where EndDate > GETDATE()
									and CampaignStatusId = 0
									)
								) z
		where
			z.ProductId is not null
		
		--rollback
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionExcludeCategoryInsert]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryInsert]
	@ConfigId				INT,
	@CategoryId				INT,
	@IncludeSubCategories	BIT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionExcludeCategory] (
		ConfigId,
		CategoryId,
		IncludeSubCategories
	)
	VALUES (
		@ConfigId,
		@CategoryId,
		@IncludeSubCategories
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[fnGetPercentagePriceDiscountActionAffectedProducts]'
GO
ALTER FUNCTION [integration].[fnGetPercentagePriceDiscountActionAffectedProducts](
	@ProductActionId INT
)
RETURNS @tAffectedProducts TABLE (ProductId INT)
AS 
BEGIN

	INSERT INTO @tAffectedProducts (ProductId)
	
	      -- include products from categories
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN [integration].[fnGetPercentagePriceDiscountActionCategories] (@ProductActionId, 1) ic ON ic.CategoryId = p.CategoryId
			
	UNION -- include products
	
		SELECT
			ProductId
		FROM
			[campaignlek].[tCampaignActionIncludeProduct] ip
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ip].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
	
	UNION -- include brand
	
		SELECT
			ProductId
		FROM
			[lekmer].[tLekmerProduct] lp
			INNER JOIN [campaignlek].[tCampaignActionIncludeBrand] ib ON [ib].[BrandId] = [lp].[BrandId]
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ib].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId

	EXCEPT -- exclude products from categories
		
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN [integration].[fnGetPercentagePriceDiscountActionCategories] (@ProductActionId, 0) ec ON ec.CategoryId = p.CategoryId

	EXCEPT -- exclude products
	
		SELECT
			ProductId
		FROM
			[campaignlek].[tCampaignActionExcludeProduct] ep
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ep].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
			
	EXCEPT -- exclude brand
	
		SELECT
			ProductId
		FROM
			[lekmer].[tLekmerProduct] lp
			INNER JOIN [campaignlek].[tCampaignActionExcludeBrand] eb ON [eb].[BrandId] = [lp].[BrandId]
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [eb].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId

	RETURN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pTagProductSale]'
GO
ALTER PROCEDURE [integration].[pTagProductSale]

AS
BEGIN
	DECLARE @SalesTagId INT
		SET @SalesTagId = (SELECT TagId FROM lekmer.tTag WHERE Value = 'Sale')

	DECLARE @ActiveCampaigns TABLE 
	(
		CampaignId INT PRIMARY KEY WITH (IGNORE_DUP_KEY = ON)
	)
	
	DECLARE @ProductWithDiscountPrices TABLE
	(
		ChannelId INT,
		ProductId INT,
		SitePrice DECIMAL (16,2),
		PRIMARY KEY(ChannelId, ProductId)
	)	
	INSERT INTO @ProductWithDiscountPrices
	EXEC [lekmer].[pAvailGetProductDiscountPriceOnSite]
	
	
	INSERT INTO @ActiveCampaigns (CampaignId)
	SELECT
		c.CampaignId
	FROM 
		campaign.tCampaign c
	WHERE
		c.EndDate > GETDATE()
		AND c.StartDate < GETDATE()
		AND CampaignStatusId = 0

	DECLARE @ProductsToBeTaged TABLE 
	(
		ProductId INT PRIMARY KEY WITH (IGNORE_DUP_KEY = ON)
	)
	
	DECLARE @IncludeAllProducts BIT
	
	/*--------------------------------------------*/
	/*CartContainsCondition*/

	DECLARE @CartContainsConditionId INT
	
	DECLARE CartContainsCondition_cursor CURSOR FOR 
	SELECT
		con.ConditionId,
		ccc.IncludeAllProducts
	FROM
		addon.tCartContainsCondition ccc
		INNER JOIN campaign.tCondition con ON con.ConditionId = ccc.ConditionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = con.CampaignId

	OPEN CartContainsCondition_cursor

	FETCH NEXT FROM CartContainsCondition_cursor INTO @CartContainsConditionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetCartContainsConditionAffectedProducts (@CartContainsConditionId)
	
		FETCH NEXT FROM CartContainsCondition_cursor INTO @CartContainsConditionId, @IncludeAllProducts
	END

	CLOSE CartContainsCondition_cursor;
	DEALLOCATE CartContainsCondition_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*CartItemPriceAction*/

	DECLARE @CartItemPriceActionId INT
	
	DECLARE CartItemPriceAction_cursor CURSOR FOR 
	SELECT
		ca.CartActionId,
		cipa.IncludeAllProducts
	FROM
		addon.tCartItemPriceAction cipa
		INNER JOIN campaign.tCartAction ca ON ca.CartActionId = cipa.CartActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = ca.CampaignId

	OPEN CartItemPriceAction_cursor

	FETCH NEXT FROM CartItemPriceAction_cursor INTO @CartItemPriceActionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetCartItemPriceActionAffectedProducts (@CartItemPriceActionId)
	
		FETCH NEXT FROM CartItemPriceAction_cursor INTO @CartItemPriceActionId, @IncludeAllProducts
	END

	CLOSE CartItemPriceAction_cursor;
	DEALLOCATE CartItemPriceAction_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*PercentagePriceDiscountAction*/

	DECLARE @PercentagePriceDiscountActionId INT
	
	DECLARE PercentagePriceDiscountAction_cursor CURSOR FOR 
	SELECT
		pa.ProductActionId,
		cac.IncludeAllProducts
	FROM
		campaign.tPercentagePriceDiscountAction ppda
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON [cac].[CampaignActionConfiguratorId] = [ppda].[ConfigId]
		INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ppda.ProductActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = pa.CampaignId

	OPEN PercentagePriceDiscountAction_cursor

	FETCH NEXT FROM PercentagePriceDiscountAction_cursor INTO @PercentagePriceDiscountActionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetPercentagePriceDiscountActionAffectedProducts (@PercentagePriceDiscountActionId)
	
		FETCH NEXT FROM PercentagePriceDiscountAction_cursor INTO @PercentagePriceDiscountActionId, @IncludeAllProducts
	END

	CLOSE PercentagePriceDiscountAction_cursor;
	DEALLOCATE PercentagePriceDiscountAction_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*ProductDiscountAction*/
	
	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT 
		ProductId
	FROM 
		lekmer.tProductDiscountActionItem pdai
		INNER JOIN lekmer.tProductDiscountAction pda ON pda.ProductActionId = pdai.ProductActionId
		INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = pda.ProductActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = pa.CampaignId

	/*--------------------------------------------*/

UpdateTags:
	
	BEGIN TRY
	BEGIN TRANSACTION

		DELETE FROM lekmer.tProductTag WHERE TagId = @SalesTagId

		INSERT INTO lekmer.tProductTag(ProductId, TagId)
		SELECT
			p.ProductId,
			@SalesTagId
		FROM
			@ProductsToBeTaged p												
			left join @ProductWithDiscountPrices d
				on p.ProductId = d.ProductId
				and d.ChannelId = 1			
			inner join product.tPriceListItem i
				on d.ProductId = i.ProductId
				and i.PriceListId = 1
		WHERE
			d.SitePrice < i.PriceIncludingVat -- Only products with lower price than original price get tagged, lekmer uses campigns diffrently...
		OR
			d.ProductId is null
		
		
	COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH

	RETURN

	/*--------------------------------------------*/

AllProductsAffected:

	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT ProductId FROM product.tProduct

	GOTO UpdateTags

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pPercentagePriceDiscountActionSave]'
GO
ALTER PROCEDURE [campaign].[pPercentagePriceDiscountActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT,
	@DiscountAmount		DECIMAL(16, 2),
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaign].[tPercentagePriceDiscountAction]
	SET
		DiscountAmount = @DiscountAmount,
		ConfigId = @ConfigId
	WHERE
		ProductActionId = @ProductActionId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaign].[tPercentagePriceDiscountAction] (
			ProductActionId,
			DiscountAmount,
			ConfigId
		)
		VALUES (
			@ProductActionId,
			@DiscountAmount,
			@ConfigId
		)
	END
	
	UPDATE 
		[campaignlek].[tCampaignActionConfigurator]
	SET 
		IncludeAllProducts = @IncludeAllProducts
	WHERE 
		[CampaignActionConfiguratorId] = @ConfigId
	
	RETURN @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pPercentagePriceDiscountActionDelete]'
GO
ALTER PROCEDURE [campaign].[pPercentagePriceDiscountActionDelete]
	@ProductActionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaign].[tPercentagePriceDiscountAction] WHERE ProductActionId = @ProductActionId)

	EXEC [campaignlek].[pCampaignActionIncludeBrandDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeBrandDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeCategoryDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeCategoryDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeProductDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeProductDeleteAll] @ConfigId
	
	DELETE FROM [campaign].[tPercentagePriceDiscountAction]
	WHERE ProductActionId = @ProductActionId
	
	DELETE FROM [campaignlek].[tCampaignActionConfigurator]
	WHERE CampaignActionConfiguratorId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pProductPriceOnSite]'
GO
ALTER PROCEDURE [integration].[pProductPriceOnSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--set statistics io on
	--set statistics time on
	
	SET NOCOUNT ON;

	DECLARE
		@Channel_SE INT,
		@Channel_NO INT,
		@Channel_DA INT,
		@Channel_FI INT,
		@Channel_FR INT,
		@Channel_NL INT,
		@PriceListId_FI INT,
		@PriceListId_FR INT,
		@PriceListId_NL INT,
		@ProductActionTypeId_Discount INT,
		@ProductActionTypeId_Percentage INT
		
	SELECT @Channel_SE = ChannelId FROM core.tChannel WHERE CommonName = 'Sweden'
	SELECT @Channel_NO = ChannelId FROM core.tChannel WHERE CommonName = 'Norway'
	SELECT @Channel_DA = ChannelId FROM core.tChannel WHERE CommonName = 'Denmark'
	SELECT @Channel_FI = ChannelId FROM core.tChannel WHERE CommonName = 'Finland'
	SELECT @Channel_FR = ChannelId FROM core.tChannel WHERE CommonName = 'France'
	SELECT @Channel_NL = ChannelId FROM core.tChannel WHERE CommonName = 'Netherlands'
	
	SELECT @ProductActionTypeId_Discount = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'ProductDiscount'
	
	SELECT @ProductActionTypeId_Percentage = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'PercentagePriceDiscount'
	
	SELECT @PriceListId_FI = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'Finland'
	
	SELECT @PriceListId_FR = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'France'
	
	SELECT @PriceListId_NL = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'Netherlands'
	
	---- #Campaign
	
	IF OBJECT_ID('tempdb..#Campaign') IS NOT NULL
		DROP TABLE #Campaign

	CREATE TABLE #Campaign
	(
	   CampaignId INT PRIMARY KEY,
	   PriceListId INT
	)

	INSERT INTO #Campaign (
		CampaignId,
		PriceListId
	)
	SELECT
		c.CampaignId,
		pl.PriceListId
	FROM
		campaign.tCampaign c
		LEFT OUTER JOIN campaign.tCampaignModuleChannel cmc ON cmc.CampaignRegistryId = c.CampaignRegistryId
		LEFT OUTER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = cmc.ChannelId
		LEFT OUTER JOIN product.tPriceList pl ON pl.PriceListRegistryId = pmc.PriceListRegistryId
	WHERE
		c.CampaignStatusId = 0
		AND (c.StartDate IS NULL OR c.StartDate <= GETDATE())
		AND (c.EndDate IS NULL OR GETDATE() <= c.EndDate)
		AND cmc.ChannelId in (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI, @Channel_FR, @Channel_NL)

	---- #CampaignAction
	
	IF OBJECT_ID('tempdb..#CampaignAction') IS NOT NULL
		DROP TABLE #CampaignAction

	CREATE TABLE #CampaignAction (
	   CampaignId INT,
	   SortIndex INT,   
	   ProductActionId INT,
	   Ordinal INT
	)

	DECLARE @Index INT
	SET @Index = 1

	INSERT INTO #CampaignAction (
		CampaignId,
		SortIndex,
		ProductActionId,
		Ordinal
	)
	SELECT
		pa.CampaignId,
		@index,
		pa.ProductActionId,
		pa.Ordinal
	FROM
		#Campaign c
		INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId
		LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal < pa.Ordinal --WTF?
	WHERE
		pa2.ProductActionId IS NULL

	WHILE (@@ROWCOUNT > 0) 
	BEGIN
		SET @Index = @Index + 1
  
		INSERT INTO #CampaignAction (
			CampaignId,
			SortIndex,
			ProductActionId,
			Ordinal
		)
		SELECT
			pa.CampaignId,
			@index,
			pa.ProductActionId,
			pa.Ordinal
		FROM
			#Campaign c
			INNER JOIN #CampaignAction tca ON tca.CampaignId = c.CampaignId AND tca.SortIndex = @Index - 1
			INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId AND pa.Ordinal > tca.Ordinal
			LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal > tca.Ordinal AND pa2.Ordinal < pa.Ordinal
		WHERE
			pa2.ProductActionId IS NULL
		OPTION (FORCE ORDER)
	END


	---- #CampaignProduct

	IF OBJECT_ID('tempdb..#CampaignProduct') IS NOT NULL
		DROP TABLE #CampaignProduct

	CREATE TABLE #CampaignProduct (
		CampaignId INT,
		PriceListId INT,
		ProductId INT,
		Price DECIMAL(16, 2) PRIMARY KEY (CampaignId, ProductId) WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		ip.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [ca].[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		pdai.ProductId,
		NULL
	from
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = c.PriceListId AND pl.CurrencyId = pdai.CurrencyId

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		p.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
					INNER JOIN product.tProduct p ON p.CategoryId = ic.CategoryId

	-- Set product prices

	UPDATE cp
	SET Price = pli.PriceIncludingVat
	FROM
		#Campaign c
		INNER JOIN #CampaignProduct cp ON cp.CampaignId = c.CampaignId
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = c.PriceListId AND pli.ProductId = cp.ProductId

	DECLARE @LastIndex INT
	SET @Index = 1
	SELECT @LastIndex = MAX(SortIndex) FROM #CampaignAction

	WHILE (@Index <= @LastIndex)
	BEGIN
		UPDATE cp
		SET Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Percentage
			INNER JOIN campaign.tPercentagePriceDiscountAction ppda ON ppda.ProductActionId = pa.ProductActionId
			INNER JOIN [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]
			INNER JOIN #CampaignProduct cp ON cp.ProductId = ip.ProductId AND cp.CampaignId = ca.CampaignId
		WHERE
			ca.SortIndex = @Index

		UPDATE cp
		SET Price = pdai.DiscountPrice
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Discount
			INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
			INNER JOIN #CampaignProduct cp ON cp.ProductId = pdai.ProductId AND cp.CampaignId = ca.CampaignId
			INNER JOIN product.tPriceList pl ON pl.PriceListId = cp.PriceListId AND pl.CurrencyId = pdai.CurrencyId
		WHERE
			ca.SortIndex = @Index
			AND (cp.Price > pdai.DiscountPrice OR cp.Price IS NULL)

		SET @Index = @Index + 1
	END

	DELETE cp FROM #CampaignProduct cp WHERE Price IS NULL

	UPDATE cp
	SET price = CASE
					WHEN (c.PriceListId = @PriceListId_FI) or (c.PriceListId = @PriceListId_FR) or (c.PriceListId = @PriceListId_NL)
					THEN Price 
					ELSE ROUND(Price, 0)					
				END
	FROM
		#CampaignProduct cp
		INNER JOIN #Campaign c ON c.CampaignId = cp.CampaignId

	---- #BestCampaignPrice

	IF OBJECT_ID('tempdb..#BestCampaignPrice') IS NOT NULL
		DROP TABLE #BestCampaignPrice

	CREATE TABLE #BestCampaignPrice (
		ProductId INT,
		PriceListId INT,
		CampaignId INT,
		Price DECIMAL(16, 2)
	)

	INSERT INTO #BestCampaignPrice (
		ProductId,
		PriceListId,
		CampaignId,
		Price) -- Fel
	SELECT
		cp.ProductId,
		cp.PriceListId,
		cp.CampaignId,
		cp.Price
	FROM
		#CampaignProduct cp
		LEFT OUTER JOIN #CampaignProduct cp2 
			ON cp2.PriceListId = cp.PriceListId
				AND cp2.ProductId = cp.ProductId
				AND (cp2.Price < cp.Price OR cp2.Price = cp.Price AND cp2.CampaignId > cp.CampaignId)      
	WHERE cp2.ProductId IS NULL

	DROP TABLE #Campaign
	DROP TABLE #CampaignAction
	DROP TABLE #CampaignProduct

	---- @PriceList
	
	DECLARE @PriceList TABLE (
		PriceListId INT PRIMARY KEY,
		ChannelId INT
	)

	INSERT INTO @PriceList 
	SELECT
		pl.PriceListId,
		c.ChannelId
	FROM
		core.tChannel c
		INNER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = c.ChannelId
		INNER JOIN product.tProductRegistry pr ON pr.ProductRegistryId = pmc.ProductRegistryId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = pr.ProductRegistryId
	WHERE
		c.ChannelId IN (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI)

	---- Result

	SELECT 
		ch.CommonName,
		ll.HYErpId,
		t.Title as Product,
		pli.PriceIncludingVat as ListPrice,
		rr.Title as Campaign,
		CASE 
			WHEN (bcp.Price IS NULL OR bcp.Price > pli.PriceIncludingVat) THEN pli.PriceIncludingVat
			ELSE bcp.Price 
		END AS SitePrice,
		CAST(((1-(bcp.Price/pli.PriceIncludingVat)) * 100) AS DECIMAL (18,2)) AS 'Total discount %',
		prs.Title as ProductStatus		
	FROM
		@PriceList pl
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = pl.PriceListId
		INNER JOIN product.tProduct t ON t.ProductId = pli.ProductId
		LEFT OUTER JOIN #BestCampaignPrice bcp ON bcp.PriceListId = pl.PriceListId AND bcp.ProductId = t.ProductId
		LEFT OUTER JOIN campaign.tCampaign c ON c.CampaignId = bcp.CampaignId
		INNER JOIN lekmer.tLekmerProduct ll ON ll.ProductId = pli.ProductId
		INNER JOIN product.tPriceList ch on ch.PriceListId = pli.PriceListId
		LEFT JOIN campaign.tCampaign rr on rr.CampaignId = bcp.CampaignId
		INNER JOIN product.tProductStatus prs on prs.ProductStatusId = t.ProductStatusId
	WHERE
		c.CampaignId IS NOT NULL
	ORDER BY
		pl.ChannelId
		
   DROP TABLE #BestCampaignPrice
   
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		caec.CategoryId,
		caec.IncludeSubCategories
	FROM 
		[campaignlek].[tCampaignActionExcludeCategory] caec
		INNER JOIN [product].[tCategory] c ON [c].[CategoryId] = [caec].[CategoryId]
	WHERE 
		caec.ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		caic.CategoryId,
		caic.IncludeSubCategories
	FROM 
		[campaignlek].[tCampaignActionIncludeCategory] caic
		INNER JOIN [product].[tCategory] c ON [c].[CategoryId] = [caic].[CategoryId]
	WHERE 
		caic.ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
