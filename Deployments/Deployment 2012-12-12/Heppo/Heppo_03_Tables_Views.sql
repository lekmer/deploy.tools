SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionIncludeCategory]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionCategory_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] DROP CONSTRAINT [FK_tPercentagePriceDiscountAction_tProductAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionExcludeProduct]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeProduct_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionIncludeBrand] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionIncludeBrand_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionIncludeProduct]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionProduct_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionExcludeBrand] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeBrand_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionExcludeCategory]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeCategory_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] DROP CONSTRAINT [PK_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] DROP CONSTRAINT [DF_tPercentagePriceDiscountAction_IncludeAllProducts]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[tGiftCardViaMailProductAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] DROP
COLUMN [IncludeAllProducts]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [campaign].[tPercentagePriceDiscountAction]'
GO
CREATE TABLE [campaign].[tmp_rg_xx_tPercentagePriceDiscountAction]
(
[ProductActionId] [int] NOT NULL,
[DiscountAmount] [decimal] (16, 2) NOT NULL,
[ConfigId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [campaign].[tmp_rg_xx_tPercentagePriceDiscountAction]([ProductActionId], [DiscountAmount], [ConfigId]) SELECT [ProductActionId], [DiscountAmount], [ConfigId] FROM [campaign].[tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [campaign].[tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[campaign].[tmp_rg_xx_tPercentagePriceDiscountAction]', N'tPercentagePriceDiscountAction'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPercentagePriceDiscountAction] on [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD CONSTRAINT [PK_tPercentagePriceDiscountAction] PRIMARY KEY CLUSTERED  ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[vPercentagePriceDiscountAction]'
GO
ALTER VIEW [campaign].[vPercentagePriceDiscountAction]
AS
	SELECT
		ppda.[ProductActionId] AS 'PercentagePriceDiscountAction.ProductActionId',
		cac.[IncludeAllProducts] AS 'PercentagePriceDiscountAction.IncludeAllProducts',
		ppda.[DiscountAmount] AS 'PercentagePriceDiscountAction.DiscountAmount',
		ppda.[ConfigId] AS 'PercentagePriceDiscountAction.ConfigId',
		pa.*
	FROM
		[campaign].[tPercentagePriceDiscountAction] ppda
		INNER JOIN [campaign].[vCustomProductAction] pa on pa.[ProductAction.Id] = ppda.[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = ppda.[ConfigId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[vGiftCardViaMailProductAction]'
GO
ALTER VIEW [campaignlek].[vGiftCardViaMailProductAction]
AS
	SELECT
		gcpa.[ProductActionId] AS 'GiftCardViaMailProductAction.ProductActionId',
		cac.[IncludeAllProducts] AS 'GiftCardViaMailProductAction.IncludeAllProducts',
		gcpa.[SendingInterval] AS 'GiftCardViaMailProductAction.SendingInterval',
		gcpa.[ConfigId] AS 'GiftCardViaMailProductAction.ConfigId',
		pa.*
	FROM
		[campaignlek].[tGiftCardViaMailProductAction] gcpa
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = gcpa.[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = gcpa.[ConfigId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD CONSTRAINT [FK_tPercentagePriceDiscountAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD CONSTRAINT [FK_tPercentagePriceDiscountAction_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO