SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [campaignlek].[tPercentagePriceDiscountActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionExcludeBrand] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeBrand_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionIncludeCategory]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionCategory_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionIncludeProduct]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionProduct_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionExcludeCategory]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeCategory_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaignlek].[tPercentagePriceDiscountActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tPercentagePriceDiscountActionIncludeBrand] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionIncludeBrand_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] DROP CONSTRAINT [FK_tPercentagePriceDiscountAction_tProductAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tPercentagePriceDiscountActionExcludeProduct]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct] DROP CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeProduct_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] DROP CONSTRAINT [PK_tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] DROP CONSTRAINT [DF_tPercentagePriceDiscountAction_IncludeAllProducts]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[tGiftCardViaMailProductAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] DROP
COLUMN [IncludeAllProducts]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [campaign].[tPercentagePriceDiscountAction]'
GO
CREATE TABLE [campaign].[tmp_rg_xx_tPercentagePriceDiscountAction]
(
[ProductActionId] [int] NOT NULL,
[DiscountAmount] [decimal] (16, 2) NOT NULL,
[ConfigId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [campaign].[tmp_rg_xx_tPercentagePriceDiscountAction]([ProductActionId], [DiscountAmount], [ConfigId]) SELECT [ProductActionId], [DiscountAmount], [ConfigId] FROM [campaign].[tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [campaign].[tPercentagePriceDiscountAction]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[campaign].[tmp_rg_xx_tPercentagePriceDiscountAction]', N'tPercentagePriceDiscountAction'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPercentagePriceDiscountAction] on [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD CONSTRAINT [PK_tPercentagePriceDiscountAction] PRIMARY KEY CLUSTERED  ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pPercentagePriceDiscountActionSave]'
GO
ALTER PROCEDURE [campaign].[pPercentagePriceDiscountActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT,
	@DiscountAmount		DECIMAL(16, 2),
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaign].[tPercentagePriceDiscountAction]
	SET
		DiscountAmount = @DiscountAmount,
		ConfigId = @ConfigId
	WHERE
		ProductActionId = @ProductActionId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaign].[tPercentagePriceDiscountAction] (
			ProductActionId,
			DiscountAmount,
			ConfigId
		)
		VALUES (
			@ProductActionId,
			@DiscountAmount,
			@ConfigId
		)
	END
	
	UPDATE 
		[campaignlek].[tCampaignActionConfigurator]
	SET 
		IncludeAllProducts = @IncludeAllProducts
	WHERE 
		[CampaignActionConfiguratorId] = @ConfigId
	
	RETURN @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pAvailGetProductDiscountPriceOnSite]'
GO
ALTER PROCEDURE [lekmer].[pAvailGetProductDiscountPriceOnSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--set statistics io on
	--set statistics time on

	SET NOCOUNT ON;

	DECLARE
		@Channel_SE INT,
		@Channel_NO INT,
		@Channel_DA INT,
		@Channel_FI INT,
		@PriceListId_FI INT,
		@ProductActionTypeId_Discount INT,
		@ProductActionTypeId_Percentage INT

	SELECT @Channel_SE = ChannelId FROM core.tChannel WHERE CommonName = 'Sweden'
	SELECT @Channel_NO = ChannelId FROM core.tChannel WHERE CommonName = 'Norway'
	SELECT @Channel_DA = ChannelId FROM core.tChannel WHERE CommonName = 'Denmark'
	SELECT @Channel_FI = ChannelId FROM core.tChannel WHERE CommonName = 'Finland'

	SELECT @ProductActionTypeId_Discount = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'ProductDiscount'

	SELECT @ProductActionTypeId_Percentage = ProductActionTypeId
	FROM campaign.tProductActionType
	WHERE CommonName = 'PercentagePriceDiscount'

	SELECT @PriceListId_FI = PriceListId
	FROM product.tPriceList
	WHERE CommonName = 'Finland'

	---- #Campaign

	IF OBJECT_ID('tempdb..#Campaign') IS NOT NULL
		DROP TABLE #Campaign

	CREATE TABLE #Campaign
	(
	   CampaignId INT PRIMARY KEY,
	   PriceListId INT
	)

	INSERT INTO #Campaign (
		CampaignId,
		PriceListId
	)
	SELECT
		c.CampaignId,
		pl.PriceListId
	FROM
		campaign.tCampaign c
		LEFT OUTER JOIN campaign.tCampaignModuleChannel cmc ON cmc.CampaignRegistryId = c.CampaignRegistryId
		LEFT OUTER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = cmc.ChannelId
		LEFT OUTER JOIN product.tPriceList pl ON pl.PriceListRegistryId = pmc.PriceListRegistryId
	WHERE
		c.CampaignStatusId = 0
		AND (c.StartDate IS NULL OR c.StartDate <= GETDATE())
		AND (c.EndDate IS NULL OR GETDATE() <= c.EndDate)
		AND cmc.ChannelId in (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI)

	---- #CampaignAction

	IF OBJECT_ID('tempdb..#CampaignAction') IS NOT NULL
		DROP TABLE #CampaignAction

	CREATE TABLE #CampaignAction (
	   CampaignId INT,
	   SortIndex INT,   
	   ProductActionId INT,
	   Ordinal INT
	)

	DECLARE @Index INT
	SET @Index = 1

	INSERT INTO #CampaignAction (
		CampaignId,
		SortIndex,
		ProductActionId,
		Ordinal
	)
	SELECT
		pa.CampaignId,
		@index,
		pa.ProductActionId,
		pa.Ordinal
	FROM
		#Campaign c
		INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId
		LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal < pa.Ordinal --WTF?
	WHERE
		pa2.ProductActionId IS NULL

	WHILE (@@ROWCOUNT > 0) 
	BEGIN
		SET @Index = @Index + 1

		INSERT INTO #CampaignAction (
			CampaignId,
			SortIndex,
			ProductActionId,
			Ordinal
		)
		SELECT
			pa.CampaignId,
			@index,
			pa.ProductActionId,
			pa.Ordinal
		FROM
			#Campaign c
			INNER JOIN #CampaignAction tca ON tca.CampaignId = c.CampaignId AND tca.SortIndex = @Index - 1
			INNER JOIN campaign.tProductAction pa ON pa.CampaignId = c.CampaignId AND pa.Ordinal > tca.Ordinal
			LEFT OUTER JOIN campaign.tProductAction pa2 ON pa2.CampaignId = pa.CampaignId AND pa2.Ordinal > tca.Ordinal AND pa2.Ordinal < pa.Ordinal
		WHERE
			pa2.ProductActionId IS NULL
		OPTION (FORCE ORDER)
	END


	---- #CampaignProduct

	IF OBJECT_ID('tempdb..#CampaignProduct') IS NOT NULL
		DROP TABLE #CampaignProduct

	CREATE TABLE #CampaignProduct (
		CampaignId INT,
		PriceListId INT,
		ProductId INT,
		Price DECIMAL(16, 2) PRIMARY KEY (CampaignId, ProductId) WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		ip.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON ppda.ProductActionId = ca.ProductActionId
		INNER JOIN [campaignlek].[tCampaignActionIncludeProduct] ip ON ip.[ConfigId] = [ppda].[ConfigId]

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		pdai.ProductId,
		NULL
	from
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = c.PriceListId AND pl.CurrencyId = pdai.CurrencyId

	INSERT INTO #CampaignProduct (
		CampaignId,
		PriceListId,
		ProductId,
		Price
	)
	SELECT
		c.CampaignId,
		c.PriceListId,
		p.ProductId,
		NULL
	FROM
		#Campaign c
		INNER JOIN #CampaignAction ca ON ca.CampaignId = c.CampaignId
		CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
					INNER JOIN product.tProduct p ON p.CategoryId = ic.CategoryId

	-- Set product prices

	UPDATE cp
	SET Price = pli.PriceIncludingVat
	FROM
		#Campaign c
		INNER JOIN #CampaignProduct cp ON cp.CampaignId = c.CampaignId
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = c.PriceListId AND pli.ProductId = cp.ProductId

	DECLARE @LastIndex INT
	SET @Index = 1
	SELECT @LastIndex = MAX(SortIndex) FROM #CampaignAction

	WHILE (@Index <= @LastIndex)
	BEGIN
		UPDATE cp
		SET Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Percentage
			INNER JOIN campaign.tPercentagePriceDiscountAction ppda ON ppda.ProductActionId = pa.ProductActionId
			INNER JOIN [campaignlek].[tCampaignActionIncludeProduct] ip ON ip.[ConfigId] = [ppda].[ConfigId]
			INNER JOIN #CampaignProduct cp ON cp.ProductId = ip.ProductId AND cp.CampaignId = ca.CampaignId
		WHERE
			ca.SortIndex = @Index

		UPDATE cp
		SET Price = pdai.DiscountPrice
		FROM
			#CampaignAction ca
			INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ca.ProductActionId AND pa.ProductActionTypeId = @ProductActionTypeId_Discount
			INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = ca.ProductActionId
			INNER JOIN #CampaignProduct cp ON cp.ProductId = pdai.ProductId AND cp.CampaignId = ca.CampaignId
			INNER JOIN product.tPriceList pl ON pl.PriceListId = cp.PriceListId AND pl.CurrencyId = pdai.CurrencyId
		WHERE
			ca.SortIndex = @Index
			AND (cp.Price > pdai.DiscountPrice OR cp.Price IS NULL)

		SET @Index = @Index + 1
	END

	DELETE cp FROM #CampaignProduct cp WHERE Price IS NULL

	UPDATE cp
	SET price = CASE
					WHEN (c.PriceListId = @PriceListId_FI) THEN Price
					ELSE ROUND(Price, 0)
				END
	FROM
		#CampaignProduct cp
		INNER JOIN #Campaign c ON c.CampaignId = cp.CampaignId

	---- #BestCampaignPrice

	IF OBJECT_ID('tempdb..#BestCampaignPrice') IS NOT NULL
		DROP TABLE #BestCampaignPrice

	CREATE TABLE #BestCampaignPrice (
		ProductId INT,
		PriceListId INT,
		CampaignId INT,
		Price DECIMAL(16, 2)
	)

	INSERT INTO #BestCampaignPrice (
		ProductId,
		PriceListId,
		CampaignId,
		Price) -- Fel
	SELECT
		cp.ProductId,
		cp.PriceListId,
		cp.CampaignId,
		cp.Price
	FROM
		#CampaignProduct cp
		LEFT OUTER JOIN #CampaignProduct cp2 
			ON cp2.PriceListId = cp.PriceListId
				AND cp2.ProductId = cp.ProductId
				AND (cp2.Price < cp.Price OR cp2.Price = cp.Price AND cp2.CampaignId > cp.CampaignId)      
	WHERE cp2.ProductId IS NULL

	DROP TABLE #Campaign
	DROP TABLE #CampaignAction
	DROP TABLE #CampaignProduct

	---- @PriceList

	DECLARE @PriceList TABLE (
		PriceListId INT PRIMARY KEY,
		ChannelId INT
	)

	INSERT INTO @PriceList 
	SELECT
		pl.PriceListId,
		c.ChannelId
	FROM
		core.tChannel c
		INNER JOIN product.tProductModuleChannel pmc ON pmc.ChannelId = c.ChannelId
		INNER JOIN product.tProductRegistry pr ON pr.ProductRegistryId = pmc.ProductRegistryId
		INNER JOIN product.tPriceList pl ON pl.PriceListId = pr.ProductRegistryId
	WHERE
		c.ChannelId IN (@Channel_SE, @Channel_NO, @Channel_DA, @Channel_FI)

	---- Result

	SELECT 
		pl.ChannelId,
		--ll.HYErpId,
		pli.ProductId,
		--pli.PriceListId,
		--pli.PriceIncludingVat AS ListPrice,
		--bcp.Price AS DiscountPrice,
		CASE 
			WHEN (bcp.Price IS NULL OR bcp.Price > pli.PriceIncludingVat) THEN pli.PriceIncludingVat
			ELSE bcp.Price 
		END AS SitePrice
		--CAST(((1-(bcp.Price/pli.PriceIncludingVat)) * 100) AS DECIMAL (18,2)) AS 'Total discount %',
	FROM
		@PriceList pl
		INNER JOIN product.tPriceListItem pli ON pli.PriceListId = pl.PriceListId
		INNER JOIN product.tProduct t ON t.ProductId = pli.ProductId
		LEFT OUTER JOIN #BestCampaignPrice bcp ON bcp.PriceListId = pl.PriceListId AND bcp.ProductId = t.ProductId
		LEFT OUTER JOIN campaign.tCampaign c ON c.CampaignId = bcp.CampaignId
		INNER JOIN lekmer.tLekmerProduct ll ON ll.ProductId = pli.ProductId
	WHERE
		c.CampaignId IS NOT NULL
	ORDER BY
		pl.ChannelId

   DROP TABLE #BestCampaignPrice

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_REATagProduct]'
GO
ALTER PROCEDURE [integration].[usp_REATagProduct]

AS
BEGIN
	
	DECLARE @SalesTagId INT
		SET @SalesTagId = (SELECT TagId FROM lekmer.tTag WHERE Value = 'Sale')
	
	DECLARE @ActiveCampaigns TABLE 
	(
		CampaignId int primary key with (ignore_dup_key = on)
	)
	INSERT INTO @ActiveCampaigns (CampaignId)
	SELECT
		c.CampaignId
	FROM 
		campaign.tCampaign c
	WHERE
		c.EndDate > GETDATE()
	AND c.StartDate < GETDATE() --
	AND CampaignStatusId = 0
	
	
	-- Campaigns that are not to be tagged for one reason or another, poor solution but temporary.
	DECLARE @ExclusionCampaigns TABLE 
	(
		CampaignId int primary key with (ignore_dup_key = on)
	)
	INSERT INTO @ExclusionCampaigns (CampaignId)
	VALUES 
	(1000421),(1000418),(1000419),(1000420),
	-- 'Brands (SE)'
	(1000418),(1000490),(1000491),(1000492),(1000493),(1000494),(1000495),(1000496),(1000497),(1000498),(1000499),(1000500),
	-- 'Brands (NO)'
	(1000419),(1000501),(1000502),(1000503),(1000504),(1000505),(1000506),(1000507),(1000508),(1000509),(1000510),(1000511),
	-- 'Brands (DK)'
	(1000421),(1000523),(1000524),(1000525),(1000526),(1000527),(1000528),(1000529),(1000530),(1000531),(1000532),(1000533),
	-- 'Brands (FI)'
	(1000420),(1000512),(1000513),(1000514),(1000515),(1000516),(1000517),(1000518),(1000519),(1000520),(1000521),(1000522),
	
	-- 'Campaigns (SE)'
	(1001486),
	-- 'Campaigns (NO)'
	(1001487),
	-- 'Campaigns (DK)'
	(1001488),
	-- 'Campaigns (FI)'
	(1001489)

	
	DELETE 
		a
	FROM
		@ActiveCampaigns a inner join @ExclusionCampaigns e on a.CampaignId = e.CampaignId
			
			
	DECLARE @ProductsToBeTaged TABLE 
	(
		ProductId int primary key with (ignore_dup_key = on)
	)
	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT 
		A0.ProductId
	FROM 
		campaign.tProductAction c
		LEFT JOIN lekmer.tProductDiscountActionItem b 
			ON c.ProductActionId = b.ProductActionId
		LEFT JOIN [campaign].[tPercentagePriceDiscountAction] ppda
			ON [ppda].[ProductActionId] = [c].[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionIncludeProduct] d
			ON d.[ConfigId] = ppda.[ConfigId]
		INNER JOIN @ActiveCampaigns a
			ON c.CampaignId = a.CampaignId
		CROSS APPLY
		(
			select coalesce(d.ProductId , b.ProductId) ProductId
		) A0
	WHERE
		A0.ProductId IS NOT NULL
	
	
	DECLARE @ProductWithDiscountPrices TABLE
	(
		ChannelId INT,
		ProductId INT,
		SitePrice DECIMAL (16,2),
		PRIMARY KEY(ChannelId, ProductId)
	)
	
	INSERT INTO @ProductWithDiscountPrices
	EXEC [lekmer].[pAvailGetProductDiscountPriceOnSite]
	
	
	
	BEGIN TRY
	BEGIN TRANSACTION
		
		DELETE FROM lekmer.tProductTag WHERE TagId = @SalesTagId		
				
		INSERT INTO lekmer.tProductTag(ProductId, TagId)
		SELECT
			p.ProductId,
			@SalesTagId
		FROM
			@ProductsToBeTaged p
			inner join @ProductWithDiscountPrices d
				on p.ProductId = d.ProductId
				and d.ChannelId = 1			
			inner join product.tPriceListItem i
				on d.ProductId = i.ProductId
				and i.PriceListId = 1
		WHERE
			d.SitePrice < i.PriceIncludingVat -- Only products with lower price than original price get tagged, lekmer uses campigns diffrently...
				
					
	COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionIncludeCategoryInsert]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryInsert]
	@ConfigId				INT,
	@CategoryId				INT,
	@IncludeSubCategories	BIT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionIncludeCategory] (
		ConfigId,
		CategoryId,
		IncludeSubCategories
	)
	VALUES (
		@ConfigId,
		@CategoryId,
		@IncludeSubCategories
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionExcludeCategoryInsert]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryInsert]
	@ConfigId				INT,
	@CategoryId				INT,
	@IncludeSubCategories	BIT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionExcludeCategory] (
		ConfigId,
		CategoryId,
		IncludeSubCategories
	)
	VALUES (
		@ConfigId,
		@CategoryId,
		@IncludeSubCategories
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pCampaignInfoToCdon]'
GO
ALTER procedure [integration].[pCampaignInfoToCdon]
as
begin
	set nocount on
	
	set transaction isolation level read uncommitted
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		

	if object_id('tempdb..#Campaign') is not null
		 drop table #Campaign

	create table #Campaign
	(
		CampaignId int primary key,
		PriceListId int  
	)

	insert into #Campaign ( CampaignId, PriceListId )
	select c.CampaignId, c.CampaignRegistryId
	from campaign.tCampaign c
	where c.CampaignStatusId = 0
		and (c.StartDate is null or c.StartDate <= getdate())
		and (c.EndDate is null or getdate() <= c.EndDate)



	if object_id('tempdb..#CampaignAction') is not null
		 drop table #CampaignAction

	create table #CampaignAction (
		CampaignId int,
		SortIndex int,   
		ProductActionId int,
		Ordinal int
	)

	declare @Index int
	set @Index = 1

	insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
	select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
	from #Campaign c
		inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
		left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
			and pa2.Ordinal < pa.Ordinal
	where pa2.ProductActionId is null

	while (@@ROWCOUNT > 0)
	begin
		set @Index = @Index + 1
	      
		insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
		select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
		from #Campaign c      
			inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
				and tca.SortIndex = @Index - 1
			inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
				and pa.Ordinal > tca.Ordinal
			left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
				and pa2.Ordinal > tca.Ordinal
				and pa2.Ordinal < pa.Ordinal
		where pa2.ProductActionId is null
		option (force order)
	end

	if object_id('tempdb..#CampaignProduct') is not null
		 drop table #CampaignProduct

	create table #CampaignProduct (
		CampaignId int,
		ProductId int,
		Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
	)

	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, ip.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [ca].[ProductActionId]
		inner join [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]
		
	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, p.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
					INNER JOIN product.tProduct p ON p.CategoryId = ic.CategoryId

	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, pdai.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
		inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId

	update cp set Price = pli.PriceIncludingVat
	from #Campaign c
		inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
		inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
			and pli.ProductId = cp.ProductId

	declare @LastIndex int
	set @Index = 1
	select @LastIndex = max(SortIndex)
	from #CampaignAction

	while (@Index <= @LastIndex)
	begin   
		update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1
			inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
			CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
						inner join product.tProduct p on p.CategoryId = ic.CategoryId
						inner join #CampaignProduct cp on cp.ProductId = p.ProductId and cp.CampaignId = ca.CampaignId
		where ca.SortIndex = @Index

		update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1
			inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
			inner join [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]
			inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
				and cp.CampaignId = ca.CampaignId
		where ca.SortIndex = @Index
	   
		update cp set Price = pdai.DiscountPrice
		-- select cp.Price, pdai.DiscountPrice
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1000001
			inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
			inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
				and cp.CampaignId = ca.CampaignId
			inner join #Campaign c on c.CampaignId = cp.CampaignId
			inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId
		where ca.SortIndex = @Index
			and (cp.Price > pdai.DiscountPrice or cp.Price is null)

		set @Index = @Index + 1
	end

	delete cp
	from #CampaignProduct cp 
	where Price is null

	update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
	from #CampaignProduct cp
		inner join #Campaign c on c.CampaignId = cp.CampaignId
	   

	if object_id('tempdb..#BestCampaignPrice') is not null
		 drop table #BestCampaignPrice

	create table #BestCampaignPrice (
		ProductId int,
		PriceListId int primary key (ProductId, PriceListId) with (ignore_dup_key = on),
		CampaignId int,
		Price decimal(16, 2)
	)
	insert into #BestCampaignPrice (ProductId, PriceListId, CampaignId, Price)
	select cp.ProductId, c.PriceListId, cp.CampaignId, cp.Price
	from #CampaignProduct cp
		inner join #Campaign c on c.CampaignId = cp.CampaignId
	order by cp.ProductId, c.PriceListId, cp.Price	

	drop table #Campaign
	drop table #CampaignAction
	drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title

	
	select case when (p.ProductStatusId = 0) then 'Online' else 'Offline' end as Status,
		p.Title as Product,
--		p.ErpId,
		lp.HyErpId AS ErpId,
		ca.ErpId as CategoryErpId,
		ca.Title as Category,
		p.NumberInStock,
		pl.CommonName as PriceList, 
	   pli.PriceIncludingVat as PriceListPrice, 
	   isnull(pld.Price, pli.PriceIncludingVat) as Price,
	   pld.Price as DiscountPrice, 
	   pli.PriceIncludingVat - pld.Price as Discount,
	   convert(int, round(convert(money, pli.PriceIncludingVat - pld.Price) * 100 / pli.PriceIncludingVat, 0)) as DiscountPercentage,
	   c.Title as Campaign,
	   c.StartDate as CampaignStart,
	   c.EndDate as CampaignEnd
	--into temp.CdonPriceNewCalc3
	from @PriceList tpl
		inner join product.tPriceList pl on pl.PriceListId = tpl.PriceListId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		left outer join #BestCampaignPrice pld on pld.PriceListId = pl.PriceListId
		   and pld.ProductId = pli.ProductId
		left outer join campaign.tCampaign c on c.CampaignId = pld.CampaignId
		inner join product.tProduct p on p.ProductId = pli.ProductId
      inner join lekmer.tLekmerProduct lp on lp.ProductId = pli.ProductId
      inner join product.tCategory ca on ca.CategoryId = p.CategoryId            

	
	drop table #BestCampaignPrice
		
  -- select pi.ProductId 
		--  ,lp.HyErpId AS ErpId
		--  ,pi.MediaId
		--  ,pi.ProductImageGroupId
		--  ,mf.Extension
	 --from @AllProductImages pi
	 --     inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	 --     inner join media.tMedia m on pi.MediaId = m.MediaId
		--   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		caic.CategoryId,
		caic.IncludeSubCategories
	FROM 
		[campaignlek].[tCampaignActionIncludeCategory] caic
		INNER JOIN [product].[tCategory] c ON [c].[CategoryId] = [caic].[CategoryId]
	WHERE 
		caic.ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pGiftCardViaMailProductActionSave]'
GO

ALTER PROCEDURE [campaignlek].[pGiftCardViaMailProductActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT,
	@SendingInterval	INT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailProductAction]
	SET
		SendingInterval = @SendingInterval,
		ConfigId = @ConfigId
	WHERE
		ProductActionId = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaignlek].[tGiftCardViaMailProductAction] (
			ProductActionId,
			SendingInterval,
			ConfigId
		)
		VALUES (
			@ProductActionId,
			@SendingInterval,
			@ConfigId
		)
	END
	
	UPDATE 
		[campaignlek].[tCampaignActionConfigurator]
	SET 
		IncludeAllProducts = @IncludeAllProducts
	WHERE 
		[CampaignActionConfiguratorId] = @ConfigId
	
	RETURN @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pPercentagePriceDiscountActionDelete]'
GO
ALTER PROCEDURE [campaign].[pPercentagePriceDiscountActionDelete]
	@ProductActionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaign].[tPercentagePriceDiscountAction] WHERE ProductActionId = @ProductActionId)

	EXEC [campaignlek].[pCampaignActionIncludeBrandDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeBrandDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeCategoryDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeCategoryDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeProductDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeProductDeleteAll] @ConfigId
	
	DELETE FROM [campaign].[tPercentagePriceDiscountAction]
	WHERE ProductActionId = @ProductActionId
	
	DELETE FROM [campaignlek].[tCampaignActionConfigurator]
	WHERE CampaignActionConfiguratorId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP VIEW [campaign].[vPercentagePriceDiscountAction]
GO
PRINT N'Creating [campaign].[vPercentagePriceDiscountAction]'
GO
CREATE VIEW [campaign].[vPercentagePriceDiscountAction]
AS
	SELECT
		ppda.[ProductActionId] AS 'PercentagePriceDiscountAction.ProductActionId',
		cac.[IncludeAllProducts] AS 'PercentagePriceDiscountAction.IncludeAllProducts',
		ppda.[DiscountAmount] AS 'PercentagePriceDiscountAction.DiscountAmount',
		ppda.[ConfigId] AS 'PercentagePriceDiscountAction.ConfigId',
		pa.*
	FROM
		[campaign].[tPercentagePriceDiscountAction] ppda
		INNER JOIN [campaign].[vCustomProductAction] pa on pa.[ProductAction.Id] = ppda.[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = ppda.[ConfigId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

exec dev.pRefreshAllViews
GO

PRINT N'Altering [campaign].[pPercentagePriceDiscountActionGetById]'
GO
ALTER PROCEDURE [campaign].[pPercentagePriceDiscountActionGetById]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaign].[vCustomPercentagePriceDiscountAction]
	WHERE
		[PercentagePriceDiscountAction.ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[vGiftCardViaMailProductAction]'
GO
ALTER VIEW [campaignlek].[vGiftCardViaMailProductAction]
AS
	SELECT
		gcpa.[ProductActionId] AS 'GiftCardViaMailProductAction.ProductActionId',
		cac.[IncludeAllProducts] AS 'GiftCardViaMailProductAction.IncludeAllProducts',
		gcpa.[SendingInterval] AS 'GiftCardViaMailProductAction.SendingInterval',
		gcpa.[ConfigId] AS 'GiftCardViaMailProductAction.ConfigId',
		pa.*
	FROM
		[campaignlek].[tGiftCardViaMailProductAction] gcpa
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = gcpa.[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = gcpa.[ConfigId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[fnGetPercentagePriceDiscountActionAffectedProducts]'
GO
ALTER FUNCTION [integration].[fnGetPercentagePriceDiscountActionAffectedProducts](
	@ProductActionId INT
)
RETURNS @tAffectedProducts TABLE (ProductId INT)
AS 
BEGIN

	INSERT INTO @tAffectedProducts (ProductId)
	
	      -- include products from categories
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN [integration].[fnGetPercentagePriceDiscountActionCategories] (@ProductActionId, 1) ic ON ic.CategoryId = p.CategoryId
			
	UNION -- include products
	
		SELECT
			ProductId
		FROM
			[campaignlek].[tCampaignActionIncludeProduct] ip
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ip].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
	
	UNION -- include brand
	
		SELECT
			ProductId
		FROM
			[lekmer].[tLekmerProduct] lp
			INNER JOIN [campaignlek].[tCampaignActionIncludeBrand] ib ON [ib].[BrandId] = [lp].[BrandId]
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ib].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId

	EXCEPT -- exclude products from categories
		
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN [integration].[fnGetPercentagePriceDiscountActionCategories] (@ProductActionId, 0) ec ON ec.CategoryId = p.CategoryId

	EXCEPT -- exclude products
	
		SELECT
			ProductId
		FROM
			[campaignlek].[tCampaignActionExcludeProduct] ep
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ep].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
			
	EXCEPT -- exclude brand
	
		SELECT
			ProductId
		FROM
			[lekmer].[tLekmerProduct] lp
			INNER JOIN [campaignlek].[tCampaignActionExcludeBrand] eb ON [eb].[BrandId] = [lp].[BrandId]
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [eb].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId

	RETURN
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]'
GO
ALTER PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		caec.CategoryId,
		caec.IncludeSubCategories
	FROM 
		[campaignlek].[tCampaignActionExcludeCategory] caec
		INNER JOIN [product].[tCategory] c ON [c].[CategoryId] = [caec].[CategoryId]
	WHERE 
		caec.ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pTagProductSale]'
GO
ALTER PROCEDURE [integration].[pTagProductSale]

AS
BEGIN
	DECLARE @SalesTagId INT
		SET @SalesTagId = (SELECT TagId FROM lekmer.tTag WHERE Value = 'Sale')

	DECLARE @ActiveCampaigns TABLE 
	(
		CampaignId INT PRIMARY KEY WITH (IGNORE_DUP_KEY = ON)
	)

	INSERT INTO @ActiveCampaigns (CampaignId)
	SELECT
		c.CampaignId
	FROM 
		campaign.tCampaign c
	WHERE
		c.EndDate > GETDATE()
		AND c.StartDate < GETDATE()
		AND CampaignStatusId = 0

	DECLARE @ProductsToBeTaged TABLE 
	(
		ProductId INT PRIMARY KEY WITH (IGNORE_DUP_KEY = ON)
	)
	
	DECLARE @IncludeAllProducts BIT
	
	/*--------------------------------------------*/
	/*CartContainsCondition*/

	DECLARE @CartContainsConditionId INT
	
	DECLARE CartContainsCondition_cursor CURSOR FOR 
	SELECT
		con.ConditionId,
		ccc.IncludeAllProducts
	FROM
		addon.tCartContainsCondition ccc
		INNER JOIN campaign.tCondition con ON con.ConditionId = ccc.ConditionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = con.CampaignId

	OPEN CartContainsCondition_cursor

	FETCH NEXT FROM CartContainsCondition_cursor INTO @CartContainsConditionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetCartContainsConditionAffectedProducts (@CartContainsConditionId)
	
		FETCH NEXT FROM CartContainsCondition_cursor INTO @CartContainsConditionId, @IncludeAllProducts
	END

	CLOSE CartContainsCondition_cursor;
	DEALLOCATE CartContainsCondition_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*CartItemPriceAction*/

	DECLARE @CartItemPriceActionId INT
	
	DECLARE CartItemPriceAction_cursor CURSOR FOR 
	SELECT
		ca.CartActionId,
		cipa.IncludeAllProducts
	FROM
		addon.tCartItemPriceAction cipa
		INNER JOIN campaign.tCartAction ca ON ca.CartActionId = cipa.CartActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = ca.CampaignId

	OPEN CartItemPriceAction_cursor

	FETCH NEXT FROM CartItemPriceAction_cursor INTO @CartItemPriceActionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetCartItemPriceActionAffectedProducts (@CartItemPriceActionId)
	
		FETCH NEXT FROM CartItemPriceAction_cursor INTO @CartItemPriceActionId, @IncludeAllProducts
	END

	CLOSE CartItemPriceAction_cursor;
	DEALLOCATE CartItemPriceAction_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*PercentagePriceDiscountAction*/

	DECLARE @PercentagePriceDiscountActionId INT
	
	DECLARE PercentagePriceDiscountAction_cursor CURSOR FOR 
	SELECT
		pa.ProductActionId,
		cac.IncludeAllProducts
	FROM
		campaign.tPercentagePriceDiscountAction ppda
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON [cac].[CampaignActionConfiguratorId] = [ppda].[ConfigId]
		INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = ppda.ProductActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = pa.CampaignId

	OPEN PercentagePriceDiscountAction_cursor

	FETCH NEXT FROM PercentagePriceDiscountAction_cursor INTO @PercentagePriceDiscountActionId, @IncludeAllProducts

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IncludeAllProducts = 1 BREAK

		INSERT INTO @ProductsToBeTaged (ProductId)
		SELECT * FROM integration.fnGetPercentagePriceDiscountActionAffectedProducts (@PercentagePriceDiscountActionId)
	
		FETCH NEXT FROM PercentagePriceDiscountAction_cursor INTO @PercentagePriceDiscountActionId, @IncludeAllProducts
	END

	CLOSE PercentagePriceDiscountAction_cursor;
	DEALLOCATE PercentagePriceDiscountAction_cursor;

	IF @IncludeAllProducts = 1 GOTO AllProductsAffected

	/*--------------------------------------------*/
	/*ProductDiscountAction*/
	
	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT 
		ProductId
	FROM 
		lekmer.tProductDiscountActionItem pdai
		INNER JOIN lekmer.tProductDiscountAction pda ON pda.ProductActionId = pdai.ProductActionId
		INNER JOIN campaign.tProductAction pa ON pa.ProductActionId = pda.ProductActionId
		INNER JOIN @ActiveCampaigns cam ON cam.CampaignId = pa.CampaignId

	/*--------------------------------------------*/

UpdateTags:
	
	BEGIN TRY
	BEGIN TRANSACTION

		DELETE FROM lekmer.tProductTag WHERE TagId = @SalesTagId

		INSERT INTO lekmer.tProductTag(ProductId, TagId)
		SELECT
			p.ProductId,
			@SalesTagId
		FROM
			@ProductsToBeTaged p												
				
	COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH

	RETURN

	/*--------------------------------------------*/

AllProductsAffected:

	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT ProductId FROM product.tProduct

	GOTO UpdateTags

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pProductPriceOnSite]'
GO
ALTER PROCEDURE [integration].[pProductPriceOnSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
if object_id('tempdb..#Campaign') is not null
    drop table #Campaign

create table #Campaign
(
   CampaignId int primary key,
   PriceListId int  
)

insert into #Campaign ( CampaignId, PriceListId )
select c.CampaignId, c.CampaignRegistryId
from campaign.tCampaign c
where c.CampaignStatusId = 0
   and (c.StartDate is null or c.StartDate <= getdate())
   and (c.EndDate is null or getdate() <= c.EndDate)



if object_id('tempdb..#CampaignAction') is not null
    drop table #CampaignAction

create table #CampaignAction (
   CampaignId int,
   SortIndex int,   
   ProductActionId int,
   Ordinal int
)

declare @Index int
set @Index = 1

insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
from #Campaign c
   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
   left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
      and pa2.Ordinal < pa.Ordinal
where pa2.ProductActionId is null

while (@@ROWCOUNT > 0)
begin
   set @Index = @Index + 1
      
   insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
   select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
   from #Campaign c      
      inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
         and tca.SortIndex = @Index - 1
      inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
         and pa.Ordinal > tca.Ordinal
      left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
         and pa2.Ordinal > tca.Ordinal
         and pa2.Ordinal < pa.Ordinal
   where pa2.ProductActionId is null
   option (force order)
end

if object_id('tempdb..#CampaignProduct') is not null
    drop table #CampaignProduct

create table #CampaignProduct (
   CampaignId int,
   PriceListId int,
   ProductId int,
   Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
)

insert into #CampaignProduct ( CampaignId, PriceListId, ProductId, Price )
select c.CampaignId, c.PriceListId, ip.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [ca].[ProductActionId]
   inner join [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]
   
insert into #CampaignProduct ( CampaignId, PriceListId, ProductId, Price )
select c.CampaignId, c.PriceListId, pdai.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
   inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId

insert into #CampaignProduct ( CampaignId, PriceListId, ProductId, Price )
select c.CampaignId, c.PriceListId, p.ProductId, null
from #Campaign c
	 inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
	 CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
				 INNER JOIN product.tProduct p ON p.CategoryId = ic.CategoryId
		 
update cp set Price = pli.PriceIncludingVat
from #Campaign c
   inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
   inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
      and pli.ProductId = cp.ProductId

declare @LastIndex int
set @Index = 1
select @LastIndex = max(SortIndex)
from #CampaignAction

while (@Index <= @LastIndex)
begin
   update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1
      inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
      inner join [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]
      inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
   
   update cp set Price = pdai.DiscountPrice
   -- select cp.Price, pdai.DiscountPrice
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1000001
      inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId      
      inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
         and cp.CampaignId = ca.CampaignId
       inner join product.tPriceList pl on pl.PriceListId = cp.PriceListId and pl.CurrencyId = pdai.CurrencyId
   where ca.SortIndex = @Index
      and (cp.Price > pdai.DiscountPrice or cp.Price is null)

   set @Index = @Index + 1
end

delete cp
from #CampaignProduct cp 
where Price is null

update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
   

if object_id('tempdb..#BestCampaignPrice') is not null
    drop table #BestCampaignPrice

create table #BestCampaignPrice (
   ProductId int,
   PriceListId int,
   CampaignId int,
   Price decimal(16, 2)
)

insert into #BestCampaignPrice (ProductId, PriceListId, CampaignId, Price)
select cp.ProductId, cp.PriceListId, cp.CampaignId, cp.Price
from #CampaignProduct cp
   left outer join #CampaignProduct cp2 on cp2.PriceListId = cp.PriceListId
      and cp2.ProductId = cp.ProductId
      and (cp2.Price < cp.Price or cp2.Price = cp.Price and cp2.CampaignId > cp.CampaignId)      
where cp2.ProductId is null

drop table #Campaign
drop table #CampaignAction
drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int		
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from core.tChannel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title
	
	select hh.CommonName, ll.HYErpId, 
		--pli.ProductId, 
	   t.Title as Product,
	   pli.PriceIncludingVat as ListPrice, 
	   --c.CampaignId, 
	   c.Title as Campaign, bcp.Price as DiscountPrice, 
	   case when (bcp.Price is null or bcp.Price > pli.PriceIncludingVat) then pli.PriceIncludingVat else bcp.Price end as SitePrice,
	   cast(((1-(bcp.Price/pli.PriceIncludingVat)) * 100) as decimal (18,2)) as 'Total discount %'
	--into temp.CdonPriceNewCalc3
	from @PriceList pl
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		inner join product.tProduct t on t.ProductId = pli.ProductId
		left outer join #BestCampaignPrice bcp on bcp.PriceListId = pl.PriceListId
		   and bcp.ProductId = t.ProductId
		left outer join campaign.tCampaign c on c.CampaignId = bcp.CampaignId
		inner join core.tChannel hh on hh.ChannelId = pl.ChannelId
		inner join lekmer.tLekmerProduct ll on ll.ProductId = pli.ProductId
	where
		c.CampaignId is not null --
	order by
		pl.ChannelId --
   drop table #BestCampaignPrice
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaign].[tPercentagePriceDiscountAction]'
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] DROP CONSTRAINT [FK_tPercentagePriceDiscountAction_tProductAction]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD CONSTRAINT [FK_tPercentagePriceDiscountAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD CONSTRAINT [FK_tPercentagePriceDiscountAction_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
