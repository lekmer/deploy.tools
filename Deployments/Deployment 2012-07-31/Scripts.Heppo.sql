/*
Run this script on:

        10.150.43.52.Heppo_live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 021\DB\HeppoDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 31.07.2012 10:26:41

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductDiscountAction]'
GO
ALTER TABLE [lekmer].[tProductDiscountAction] DROP
CONSTRAINT [FK_tProductDiscountAction_tCurrency]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductDiscountActionItem]'
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] DROP
CONSTRAINT [FK_tProductDiscountActionItem_tProductDiscountAction],
CONSTRAINT [FK_tProductDiscountActionItem_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tProductDiscountActionItem]'
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] DROP CONSTRAINT [PK_tProductDiscountActionItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tProductDiscountActionItem]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tProductDiscountActionItem]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL CONSTRAINT [DF_tProductDiscountActionItem_CurrencyId] DEFAULT ((1)),
[DiscountPrice] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--!!!
INSERT INTO [lekmer].[tmp_rg_xx_tProductDiscountActionItem]([ProductActionId], [ProductId], [CurrencyId], [DiscountPrice])
SELECT pdai.[ProductActionId], pdai.[ProductId], pda.[CurrencyId], pdai.[DiscountPrice] 
FROM [lekmer].[tProductDiscountActionItem] pdai
LEFT OUTER JOIN [lekmer].[tProductDiscountAction] pda ON [pdai].[ProductActionId] = [pda].[ProductActionId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tProductDiscountActionItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tProductDiscountActionItem]', N'tProductDiscountActionItem'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductDiscountActionItem] on [lekmer].[tProductDiscountActionItem]'
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] ADD CONSTRAINT [PK_tProductDiscountActionItem] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignActionExcludeBrand]'
GO
CREATE TABLE [campaignlek].[tCampaignActionExcludeBrand]
(
[ConfigId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignActionExcludeBrand] on [campaignlek].[tCampaignActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeBrand] ADD CONSTRAINT [PK_tCampaignActionExcludeBrand] PRIMARY KEY CLUSTERED  ([ConfigId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignActionConfigurator]'
GO
CREATE TABLE [campaignlek].[tCampaignActionConfigurator]
(
[CampaignActionConfiguratorId] [int] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignActionConfigurator] on [campaignlek].[tCampaignActionConfigurator]'
GO
ALTER TABLE [campaignlek].[tCampaignActionConfigurator] ADD CONSTRAINT [PK_tCampaignActionConfigurator] PRIMARY KEY CLUSTERED  ([CampaignActionConfiguratorId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignActionExcludeCategory]'
GO
CREATE TABLE [campaignlek].[tCampaignActionExcludeCategory]
(
[ConfigId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignActionExcludeCategory] on [campaignlek].[tCampaignActionExcludeCategory]'
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeCategory] ADD CONSTRAINT [PK_tCampaignActionExcludeCategory] PRIMARY KEY CLUSTERED  ([ConfigId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignActionExcludeProduct]'
GO
CREATE TABLE [campaignlek].[tCampaignActionExcludeProduct]
(
[ConfigId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignActionExcludeProduct] on [campaignlek].[tCampaignActionExcludeProduct]'
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeProduct] ADD CONSTRAINT [PK_tCampaignActionExcludeProduct] PRIMARY KEY CLUSTERED  ([ConfigId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignActionIncludeBrand]'
GO
CREATE TABLE [campaignlek].[tCampaignActionIncludeBrand]
(
[ConfigId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignActionIncludeBrand] on [campaignlek].[tCampaignActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeBrand] ADD CONSTRAINT [PK_tCampaignActionIncludeBrand] PRIMARY KEY CLUSTERED  ([ConfigId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignActionIncludeCategory]'
GO
CREATE TABLE [campaignlek].[tCampaignActionIncludeCategory]
(
[ConfigId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignActionIncludeCategory] on [campaignlek].[tCampaignActionIncludeCategory]'
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeCategory] ADD CONSTRAINT [PK_tCampaignActionIncludeCategory] PRIMARY KEY CLUSTERED  ([ConfigId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignActionIncludeProduct]'
GO
CREATE TABLE [campaignlek].[tCampaignActionIncludeProduct]
(
[ConfigId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignActionIncludeProduct] on [campaignlek].[tCampaignActionIncludeProduct]'
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeProduct] ADD CONSTRAINT [PK_tCampaignActionIncludeProduct] PRIMARY KEY CLUSTERED  ([ConfigId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tGiftCardViaMailCartAction]'
GO
CREATE TABLE [campaignlek].[tGiftCardViaMailCartAction]
(
[CartActionId] [int] NOT NULL,
[SendingInterval] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tGiftCardViaMailCartAction] on [campaignlek].[tGiftCardViaMailCartAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartAction] ADD CONSTRAINT [PK_tGiftCardViaMailCartAction] PRIMARY KEY CLUSTERED  ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tGiftCardViaMailCartActionCurrency]'
GO
CREATE TABLE [campaignlek].[tGiftCardViaMailCartActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tGiftCardViaMailCartActionCurrency] on [campaignlek].[tGiftCardViaMailCartActionCurrency]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartActionCurrency] ADD CONSTRAINT [PK_tGiftCardViaMailCartActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tGiftCardViaMailInfo]'
GO
CREATE TABLE [campaignlek].[tGiftCardViaMailInfo]
(
[GiftCardViaMailInfoId] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[CampaignId] [int] NOT NULL,
[DiscountValue] [decimal] (16, 2) NOT NULL,
[DateToSend] [datetime] NOT NULL,
[StatusId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tGiftCardViaMailInfo] on [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD CONSTRAINT [PK_tGiftCardViaMailInfo] PRIMARY KEY CLUSTERED  ([GiftCardViaMailInfoId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tGiftCardViaMailInfoStatus]'
GO
CREATE TABLE [campaignlek].[tGiftCardViaMailInfoStatus]
(
[StatusId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tGiftCardViaMailInfoStatus] on [campaignlek].[tGiftCardViaMailInfoStatus]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfoStatus] ADD CONSTRAINT [PK_tGiftCardViaMailInfoStatus] PRIMARY KEY CLUSTERED  ([StatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tGiftCardViaMailProductAction]'
GO
CREATE TABLE [campaignlek].[tGiftCardViaMailProductAction]
(
[ProductActionId] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL,
[SendingInterval] [int] NOT NULL,
[ConfigId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tGiftCardViaMailProductAction] on [campaignlek].[tGiftCardViaMailProductAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] ADD CONSTRAINT [PK_tGiftCardViaMailProductAction] PRIMARY KEY CLUSTERED  ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tGiftCardViaMailProductActionCurrency]'
GO
CREATE TABLE [campaignlek].[tGiftCardViaMailProductActionCurrency]
(
[ProductActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tGiftCardViaMailProductActionCurrency] on [campaignlek].[tGiftCardViaMailProductActionCurrency]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductActionCurrency] ADD CONSTRAINT [PK_tGiftCardViaMailProductActionCurrency] PRIMARY KEY CLUSTERED  ([ProductActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tProductDiscountAction]'
GO
ALTER TABLE [lekmer].[tProductDiscountAction] DROP
COLUMN [CurrencyId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tVoucherAction]'
GO
ALTER TABLE [lekmer].[tVoucherAction] ALTER COLUMN [DiscountValue] [decimal] (16, 2) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tVoucherActionCurrency]'
GO
CREATE TABLE [lekmer].[tVoucherActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tVoucherActionCurrency] on [lekmer].[tVoucherActionCurrency]'
GO
ALTER TABLE [lekmer].[tVoucherActionCurrency] ADD CONSTRAINT [PK_tVoucherActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeBrandDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeBrandDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionExcludeBrand]
	WHERE
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeBrandGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeBrandGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tCampaignActionExcludeBrand]
	WHERE 
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeBrandInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeBrandInsert]
	@ConfigId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionExcludeBrand] (
		ConfigId,
		BrandId
	)
	VALUES (
		@ConfigId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeCategoryDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionExcludeCategory]
	WHERE
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		CategoryId
	FROM 
		[campaignlek].[tCampaignActionExcludeCategory]
	WHERE 
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeCategoryInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryInsert]
	@ConfigId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionExcludeCategory] (
		ConfigId,
		CategoryId
	)
	VALUES (
		@ConfigId,
		@CategoryId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeProductDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeProductDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionExcludeProduct]
	WHERE
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeProductInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeProductInsert]
	@ConfigId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionExcludeProduct] (
		ConfigId,
		ProductId
	)
	VALUES (
		@ConfigId,
		@ProductId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeBrandDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeBrandDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionIncludeBrand]
	WHERE
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeBrandGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeBrandGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tCampaignActionIncludeBrand]
	WHERE 
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeBrandInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeBrandInsert]
	@ConfigId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionIncludeBrand] (
		ConfigId,
		BrandId
	)
	VALUES (
		@ConfigId,
		@BrandId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeCategoryDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionIncludeCategory]
	WHERE
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		CategoryId
	FROM 
		[campaignlek].[tCampaignActionIncludeCategory]
	WHERE 
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeCategoryInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryInsert]
	@ConfigId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionIncludeCategory] (
		ConfigId,
		CategoryId
	)
	VALUES (
		@ConfigId,
		@CategoryId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeProductDeleteAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeProductDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionIncludeProduct]
	WHERE
		ConfigId = @ConfigId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeProductInsert]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeProductInsert]
	@ConfigId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionIncludeProduct] (
		ConfigId,
		ProductId
	)
	VALUES (
		@ConfigId,
		@ProductId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailCartActionCurrencyDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionCurrencyDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tGiftCardViaMailCartActionCurrency]
	WHERE
		CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailCartActionCurrencyInsert]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionCurrencyInsert]
	@CartActionId	INT,
	@CurrencyId		INT,
	@Value			DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tGiftCardViaMailCartActionCurrency] (
		CartActionId,
		CurrencyId,
		Value
	)
	VALUES (
		@CartActionId,
		@CurrencyId,
		@Value
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailCartActionDelete]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pGiftCardViaMailCartActionCurrencyDeleteAll] @CartActionId

	DELETE FROM [campaignlek].[tGiftCardViaMailCartAction]
	WHERE CartActionId = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailCartActionSave]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionSave]
	@CartActionId		INT,
	@SendingInterval	INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailCartAction]
	SET
		SendingInterval = @SendingInterval
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tGiftCardViaMailCartAction] (
			CartActionId,
			SendingInterval
		)
		VALUES (
			@CartActionId,
			@SendingInterval
		)
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailInfoGetAllByOrder]'
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoGetAllByOrder]
	@OrderId INT
AS
BEGIN
	SELECT 
		gcvmi.[GiftCardViaMailInfoId] AS 'GiftCardViaMailInfo.GiftCardViaMailInfoId',
		gcvmi.[OrderId] AS 'GiftCardViaMailInfo.OrderId',
		gcvmi.[CampaignId] AS 'GiftCardViaMailInfo.CampaignId',
		gcvmi.[DiscountValue] AS 'GiftCardViaMailInfo.DiscountValue',
		gcvmi.[DateToSend] AS 'GiftCardViaMailInfo.DateToSend',
		gcvmi.[StatusId] AS 'GiftCardViaMailInfo.StatusId',
		o.[ChannelId] AS 'GiftCardViaMailInfo.ChannelId'
	FROM
		[campaignlek].[tGiftCardViaMailInfo] gcvmi
		INNER JOIN [order].[tOrder] o ON o.[OrderId] = gcvmi.[OrderId]
	WHERE 
		gcvmi.OrderId = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailInfoGetAllToSend]'
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoGetAllToSend]
AS
BEGIN
	SELECT 
		gcvmi.[GiftCardViaMailInfoId] AS 'GiftCardViaMailInfo.GiftCardViaMailInfoId',
		gcvmi.[OrderId] AS 'GiftCardViaMailInfo.OrderId',
		gcvmi.[CampaignId] AS 'GiftCardViaMailInfo.CampaignId',
		gcvmi.[DiscountValue] AS 'GiftCardViaMailInfo.DiscountValue',
		gcvmi.[DateToSend] AS 'GiftCardViaMailInfo.DateToSend',
		gcvmi.[StatusId] AS 'GiftCardViaMailInfo.StatusId',
		o.[ChannelId] AS 'GiftCardViaMailInfo.ChannelId'
	FROM
		[campaignlek].[tGiftCardViaMailInfo] gcvmi
		INNER JOIN [order].[tOrder] o ON o.[OrderId] = gcvmi.[OrderId]
		INNER JOIN [order].[tOrderStatus] os ON o.[OrderStatusId] = os.[OrderStatusId]
	WHERE os.[CommonName] = 'OrderInHY'
		  AND gcvmi.[StatusId] = (SELECT StatusId FROM [campaignlek].[tGiftCardViaMailInfoStatus] WHERE CommonName = 'ReadyToSend')
		  AND gcvmi.[DateToSend] < GETDATE()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailInfoInsert]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoInsert]
	@OrderId			INT,
	@CampaignId			INT,
	@DiscountValue		DECIMAL(16,2),
	@DateToSend			DATETIME,
	@StatusCommonName	VARCHAR(50)
AS
BEGIN
	INSERT [campaignlek].[tGiftCardViaMailInfo] (
		OrderId,
		CampaignId,
		DiscountValue,
		DateToSend,
		StatusId
	)
	VALUES (
		@OrderId,
		@CampaignId,
		@DiscountValue,
		@DateToSend,
		(SELECT StatusId FROM [campaignlek].[tGiftCardViaMailInfoStatus] WHERE CommonName = @StatusCommonName)
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailInfoUpdate]'
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoUpdate]
	@GiftCardViaMailInfoId	INT,
	@StatusCommonName		VARCHAR(50)
AS
BEGIN
	UPDATE [campaignlek].[tGiftCardViaMailInfo]
	SET StatusId = (SELECT StatusId FROM [campaignlek].[tGiftCardViaMailInfoStatus] WHERE CommonName = @StatusCommonName)
	WHERE GiftCardViaMailInfoId = @GiftCardViaMailInfoId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailProductActionCurrencyDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionCurrencyDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tGiftCardViaMailProductActionCurrency]
	WHERE
		ProductActionId = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailProductActionCurrencyInsert]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionCurrencyInsert]
	@ProductActionId	INT,
	@CurrencyId			INT,
	@Value				DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tGiftCardViaMailProductActionCurrency] (
		ProductActionId,
		CurrencyId,
		Value
	)
	VALUES (
		@ProductActionId,
		@CurrencyId,
		@Value
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailProductActionDelete]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionDelete]
	@ProductActionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaignlek].[tGiftCardViaMailProductAction] WHERE ProductActionId = @ProductActionId)

	EXEC [campaignlek].[pCampaignActionIncludeBrandDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeBrandDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeCategoryDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeCategoryDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeProductDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeProductDeleteAll] @ConfigId
	
	EXEC [campaignlek].[pGiftCardViaMailProductActionCurrencyDeleteAll] @ProductActionId

	DELETE FROM [campaignlek].[tGiftCardViaMailProductAction]
	WHERE ProductActionId = @ProductActionId
	
	DELETE FROM [campaignlek].[tCampaignActionConfigurator]
	WHERE CampaignActionConfiguratorId = @ConfigId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailProductActionSave]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT,
	@SendingInterval	INT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailProductAction]
	SET
		IncludeAllProducts = @IncludeAllProducts,
		SendingInterval = @SendingInterval,
		ConfigId = @ConfigId
	WHERE
		ProductActionId = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaignlek].[tGiftCardViaMailProductAction] (
			ProductActionId,
			IncludeAllProducts,
			SendingInterval,
			ConfigId
		)
		VALUES (
			@ProductActionId,
			@IncludeAllProducts,
			@SendingInterval,
			@ConfigId
		)
	END
	
	RETURN @ConfigId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDiscountActionItemInsert]'
GO
ALTER PROCEDURE [lekmer].[pProductDiscountActionItemInsert]
	@ActionId			int,
	@ProductId			int,
	@CurrencyId			int,
	@DiscountPrice		decimal(16,2)
AS 
BEGIN 
	INSERT
		lekmer.tProductDiscountActionItem
	(
		ProductActionId,
		ProductId,
		CurrencyId,
		DiscountPrice
	)
	VALUES
	(
		@ActionId,
		@ProductId,
		@CurrencyId,
		@DiscountPrice
	)
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDiscountActionSave]'
GO
ALTER PROCEDURE [lekmer].[pProductDiscountActionSave]
	@ActionId	int
AS 
BEGIN
	IF (NOT EXISTS (SELECT * FROM lekmer.tProductDiscountAction WHERE ProductActionId = @ActionId))
	BEGIN
		INSERT 
			lekmer.tProductDiscountAction
		(
			ProductActionId
		)
		VALUES
		(
			@ActionId
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pVoucherActionValueDeleteAll]'
GO

create procedure [lekmer].[pVoucherActionValueDeleteAll]
	@CartActionId int
as
begin
	DELETE
		lekmer.tVoucherActionCurrency
	WHERE
		CartActionId = @CartActionId
end




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pVoucherActionValueSave]'
GO

CREATE procedure [lekmer].[pVoucherActionValueSave]
	@CartActionId int,
	@CurrencyId int,
	@Value decimal(16,2)
AS 
BEGIN
	UPDATE
		[lekmer].[tVoucherActionCurrency]
	SET 
		[Value] = @Value
	WHERE 
		CartActionId = @CartActionId AND 
		CurrencyId = @CurrencyId
		
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			[lekmer].[tVoucherActionCurrency]
		( 
			CartActionId,
			CurrencyId,
			[Value]
		)
		VALUES 
		(
			@CartActionId,
			@CurrencyId,
			@Value
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeProductGetIdAllSecure]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeProductGetIdAllSecure]
	@ConfigId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tCampaignActionExcludeProduct] caep
		INNER JOIN [product].[tProduct] p ON caep.[ProductId] = p.[ProductId]
	WHERE
		caep.[ConfigId] = @ConfigId
		AND p.[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeProductGetIdAllSecure]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeProductGetIdAllSecure]
	@ConfigId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tCampaignActionIncludeProduct] caep
		INNER JOIN [product].[tProduct] p ON caep.[ProductId] = p.[ProductId]
	WHERE
		caep.[ConfigId] = @ConfigId
		AND p.[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vProductDiscountActionItem]'
GO


CREATE VIEW [lekmer].[vProductDiscountActionItem]
as
	select
		p.ProductActionId AS 'ProductDiscountActionItem.ActionId',
		p.ProductId AS 'ProductDiscountActionItem.ProductId',
		p.CurrencyId AS 'ProductDiscountActionItem.CurrencyId',
		p.DiscountPrice AS 'CurrencyValue.MonetaryValue',
		c.*
	from
		lekmer.tProductDiscountActionItem p
		inner join core.vCustomCurrency c on p.CurrencyId = c.[Currency.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDiscountActionItemGetAllSecure]'
GO
ALTER PROCEDURE [lekmer].[pProductDiscountActionItemGetAllSecure]
	@ActionId		int
AS
BEGIN
	SELECT 
		A.*
	FROM 
		lekmer.vProductDiscountActionItem A
		INNER JOIN product.tProduct P ON A.[ProductDiscountActionItem.ProductId] = P.ProductId
	WHERE 
		A.[ProductDiscountActionItem.ActionId] = @ActionId AND 
		P.IsDeleted = 0
	ORDER BY
		[P].[ErpId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vVoucherActionCurrency]'
GO

CREATE VIEW [lekmer].[vVoucherActionCurrency]
as
	select
		v.CartActionId AS 'VoucherAction.ActionId',
		v.CurrencyId AS 'VoucherAction.CurrencyId',
		v.Value AS 'CurrencyValue.MonetaryValue',
		c.*
	from
		lekmer.tVoucherActionCurrency v
		inner join core.vCustomCurrency c on v.CurrencyId = c.[Currency.Id]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vGiftCardViaMailCartActionCurrency]'
GO

CREATE VIEW [campaignlek].[vGiftCardViaMailCartActionCurrency]
AS
	SELECT
		gccac.[CartActionId] AS 'GiftCardViaMailCartActionCurrency.CartActionId',
		gccac.[CurrencyId] AS 'GiftCardViaMailCartActionCurrency.CurrencyId',
		gccac.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tGiftCardViaMailCartActionCurrency] gccac
		INNER JOIN [core].[vCustomCurrency] c ON gccac.[CurrencyId] = c.[Currency.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailCartActionCurrencyGetByAction]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vGiftCardViaMailCartActionCurrency]
	WHERE 
		[GiftCardViaMailCartActionCurrency.CartActionId] = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vGiftCardViaMailCartAction]'
GO


CREATE VIEW [campaignlek].[vGiftCardViaMailCartAction]
AS
	SELECT
		gcca.[CartActionId] AS 'GiftCardViaMailCartAction.CartActionId',
		gcca.[SendingInterval] AS 'GiftCardViaMailCartAction.SendingInterval',
		pa.*
	FROM
		[campaignlek].[tGiftCardViaMailCartAction] gcca
		INNER JOIN [campaign].[vCustomCartAction] pa ON pa.[CartAction.Id] = gcca.[CartActionId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailCartActionGetById]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vGiftCardViaMailCartAction]
	WHERE
		[GiftCardViaMailCartAction.CartActionId] = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vGiftCardViaMailProductActionCurrency]'
GO

CREATE VIEW [campaignlek].[vGiftCardViaMailProductActionCurrency]
AS
	SELECT
		gcpac.[ProductActionId] AS 'GiftCardViaMailProductActionCurrency.ProductActionId',
		gcpac.[CurrencyId] AS 'GiftCardViaMailProductActionCurrency.CurrencyId',
		gcpac.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tGiftCardViaMailProductActionCurrency] gcpac
		INNER JOIN [core].[vCustomCurrency] c ON gcpac.[CurrencyId] = c.[Currency.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailProductActionCurrencyGetByAction]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionCurrencyGetByAction]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vGiftCardViaMailProductActionCurrency]
	WHERE 
		[GiftCardViaMailProductActionCurrency.ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vGiftCardViaMailProductAction]'
GO



CREATE VIEW [campaignlek].[vGiftCardViaMailProductAction]
AS
	SELECT
		gcpa.[ProductActionId] AS 'GiftCardViaMailProductAction.ProductActionId',
		gcpa.[IncludeAllProducts] AS 'GiftCardViaMailProductAction.IncludeAllProducts',
		gcpa.[SendingInterval] AS 'GiftCardViaMailProductAction.SendingInterval',
		gcpa.[ConfigId] AS 'GiftCardViaMailProductAction.ConfigId',
		pa.*
	FROM
		[campaignlek].[tGiftCardViaMailProductAction] gcpa
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = gcpa.[ProductActionId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailProductActionGetById]'
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionGetById]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vGiftCardViaMailProductAction]
	WHERE
		[GiftCardViaMailProductAction.ProductActionId] = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vProductDiscountAction]'
GO

ALTER view [lekmer].[vProductDiscountAction]
as
	select
		a.*
	from
		lekmer.tProductDiscountAction p
		inner join campaign.vCustomProductAction a on p.ProductActionId = a.[ProductAction.Id]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCustomVoucherActionCurrency]'
GO


create view [lekmer].[vCustomVoucherActionCurrency]
as
	select
		*
	from
		[lekmer].[vVoucherActionCurrency]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pVoucherActionValuesGetByActionId]'
GO

CREATE PROCEDURE [lekmer].[pVoucherActionValuesGetByActionId]
	@ActionId int
as
begin
	SELECT
		*
	FROM
		lekmer.vCustomVoucherActionCurrency
	WHERE 
		[VoucherAction.ActionId] = @ActionId
end


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionExcludeProductGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeProductGetIdAll]
	@ChannelId	INT,
	@CustomerId	INT,
	@ConfigId	INT
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		[campaignlek].[tCampaignActionExcludeProduct] caep
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = caep.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		caep.[ConfigId] = @ConfigId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignActionIncludeProductGetIdAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeProductGetIdAll]
	@ChannelId	INT,
	@CustomerId	INT,
	@ConfigId	INT
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		[campaignlek].[tCampaignActionIncludeProduct] caep
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = caep.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		caep.[ConfigId] = @ConfigId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDiscountActionItemGetAll]'
GO
ALTER PROCEDURE [lekmer].[pProductDiscountActionItemGetAll]
	@ChannelId		int,
	@CustomerId		int,
	@ActionId		int
AS
BEGIN
	SELECT
		A.*
	FROM
		lekmer.vProductDiscountActionItem A
		INNER JOIN product.vCustomProduct P ON A.[ProductDiscountActionItem.ProductId] = P.[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		A.[ProductDiscountActionItem.ActionId] = @ActionId
		and P.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeBrand] ADD
CONSTRAINT [FK_tCampaignActionExcludeBrand_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId]),
CONSTRAINT [FK_tCampaignActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignActionExcludeCategory]'
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeCategory] ADD
CONSTRAINT [FK_tCampaignActionExcludeCategory_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId]),
CONSTRAINT [FK_tCampaignActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignActionExcludeProduct]'
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeProduct] ADD
CONSTRAINT [FK_tCampaignActionExcludeProduct_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId]),
CONSTRAINT [FK_tCampaignActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeBrand] ADD
CONSTRAINT [FK_tCampaignActionIncludeBrand_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId]),
CONSTRAINT [FK_tCampaignActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignActionIncludeCategory]'
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeCategory] ADD
CONSTRAINT [FK_tCampaignActionIncludeCategory_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId]),
CONSTRAINT [FK_tCampaignActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tCampaignActionIncludeProduct]'
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeProduct] ADD
CONSTRAINT [FK_tCampaignActionIncludeProduct_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId]),
CONSTRAINT [FK_tCampaignActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailProductAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductAction] ADD
CONSTRAINT [FK_tGiftCardViaMailProductAction_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId]),
CONSTRAINT [FK_tGiftCardViaMailProductAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailCartAction]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartAction] ADD
CONSTRAINT [FK_GiftCardViaMailCartAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailCartActionCurrency]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartActionCurrency] ADD
CONSTRAINT [FK_tGiftCardViaMailCartActionCurrency_tGiftCardViaMailCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tGiftCardViaMailCartAction] ([CartActionId]),
CONSTRAINT [FK_tGiftCardViaMailCartActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD
CONSTRAINT [FK_tGiftCardViaMailInfo_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId]),
CONSTRAINT [FK_tGiftCardViaMailInfo_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId]),
CONSTRAINT [FK_tGiftCardViaMailInfo_tGiftCardViaMailInfo] FOREIGN KEY ([StatusId]) REFERENCES [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailProductActionCurrency]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailProductActionCurrency] ADD
CONSTRAINT [FK_tGiftCardViaMailProductActionCurrency_tGiftCardViaMailProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tGiftCardViaMailProductAction] ([ProductActionId]),
CONSTRAINT [FK_tGiftCardViaMailProductActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductDiscountActionItem]'
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] ADD
CONSTRAINT [FK_tProductDiscountActionItem_tProductDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [lekmer].[tProductDiscountAction] ([ProductActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tProductDiscountActionItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE,
CONSTRAINT [FK_tProductDiscountActionItem_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tVoucherActionCurrency]'
GO
ALTER TABLE [lekmer].[tVoucherActionCurrency] ADD
CONSTRAINT [FK_tVoucherActionCurrency_tVoucherAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tVoucherAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tVoucherActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
