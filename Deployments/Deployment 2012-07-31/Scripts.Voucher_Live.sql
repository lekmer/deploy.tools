/*
Run this script on:

        10.150.43.52.Voucher_Live    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\External.Voucher.System\trunk\Release\Release 003\VoucherWebServiceV2\DB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 31.07.2012 12:41:43

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [backoffice].[pVoucherBatchCreate]'
GO
CREATE PROCEDURE [backoffice].[pVoucherBatchCreate]
	@ValidFrom		DATETIME,
	@Prefix			VARCHAR (32),
	@Suffix			VARCHAR (32),
	@Quantity		INT,
	@DiscountValue	DECIMAL,
	@AppName		NVARCHAR(250),
	@NbrOfToken		INT,
	@CreatedBy		NVARCHAR(250)
AS
BEGIN
	DECLARE @DiscountTypeId	INT
	SET @DiscountTypeId = (SELECT DiscountTypeId FROM [product].[tDiscountType] WHERE DiscountType = 'GiftCard')
	
	DECLARE @ChannelGroupId	INT
	SET @ChannelGroupId = ( SELECT TOP(1) chg.ChannelGroupId
							FROM [product].[tChannelGroup] chg
							INNER JOIN [product].[tSite] s ON s.[SiteId] = chg.[SiteId]
							WHERE s.[SiteTitle] = @AppName)

	INSERT INTO [product].[tVoucherInfo] (
		ValidFrom,
		ValidTo,
		Prefix,
		Suffix,
		Quantity,
		DiscountTypeId,
		DiscountValue,
		IsActive,
		ChannelGroupId,
		DiscountTitle,
		[Description],
		NbrOfToken,
		OriginalStartQuantityPerCode,
		ValidInDays,
		CreatedBy
	)
	SELECT
		(CAST(@ValidFrom AS DATETIME)),
		NULL,
		@Prefix,
		@Suffix,
		@Quantity,
		@DiscountTypeId,
		@DiscountValue,
		1,
		@ChannelGroupId,
		'GiftCardViaEmail',
		'Gift card via email batch',
		@NbrOfToken,
		1,
		NULL,
		@CreatedBy
		
	SELECT @@IDENTITY
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [backoffice].[pVoucherBatchGiftCardGetByChannelCampaignDiscountValue]'
GO
CREATE PROCEDURE [backoffice].[pVoucherBatchGiftCardGetByChannelCampaignDiscountValue]
	@DiscountValue	DECIMAL(18,0),
	@AppName		NVARCHAR(250),
	@CreatedBy		NVARCHAR(250)
AS
BEGIN
	SELECT 
		vi.[VoucherInfoId],
		vi.[Quantity]
	FROM
		[product].[tVoucherInfo] vi
		INNER JOIN [product].[tChannelGroup] chg ON chg.[ChannelGroupId] = vi.[ChannelGroupId]
		INNER JOIN [product].[tSite] s ON s.[SiteId] = chg.[SiteId]
	WHERE
		vi.[DiscountTypeId] = (SELECT DiscountTypeId FROM [product].[tDiscountType] WHERE DiscountType = 'GiftCard')
		AND vi.[DiscountValue] = @DiscountValue
		AND vi.[IsActive] = 1
		AND vi.[CreatedBy] = @CreatedBy
		AND s.[SiteTitle] = @AppName
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [backoffice].[pVoucherCountGetByVoucherInfo]'
GO
CREATE PROCEDURE [backoffice].[pVoucherCountGetByVoucherInfo]
	@VoucherInfoId	INT
AS
BEGIN
	SELECT
		COUNT(*)
	FROM
		[product].[tVoucher]
	WHERE
		VoucherInfoId = @VoucherInfoId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
