SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [lekmer].[pProductTagsDeleteByCampaign]'
GO
DROP PROCEDURE [lekmer].[pProductTagsDeleteByCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductTagsGetAllByCampaign]'
GO
CREATE PROCEDURE [lekmer].[pProductTagsGetAllByCampaign]
	@CampaignId	INT
AS
BEGIN
	SELECT [pt].[ProductTagId]
	FROM [lekmer].[tProductTag] pt
		  INNER JOIN [lekmer].[tProductTagUsage] ptu ON [ptu].[ProductTagId] = [pt].[ProductTagId]
		  WHERE [ptu].[ObjectId] = @CampaignId
			    AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTagUsage] ptu1
								WHERE [ptu1].[ProductTagId] = [pt].[ProductTagId]
								HAVING COUNT(1) > 1)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageUpdate]'
GO
ALTER PROCEDURE [productlek].[pPackageUpdate]
AS
BEGIN
	/*
	1. Update package stock numbers
		package.stock = MIN (products.stock)
		product.stock = SUM (product-sizes.stock) || product.stock
		
	2.	OBSOLETE
		Put packages offline when they are out of stock
		When
			package is online
			package is passive
			stock is 0
		Then
			set package offline
	*/

	SET NOCOUNT ON

	DECLARE @PackageId INT
	DECLARE @MasterProductId INT
	DECLARE @NumberInStock INT

	DECLARE cur_package CURSOR FAST_FORWARD
	FOR
		SELECT
			[PackageId],
			[MasterProductId]
		FROM
			[productlek].[tPackage] p

	OPEN cur_package

	FETCH NEXT FROM cur_package
	INTO @PackageId, @MasterProductId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @NumberInStock = (
			SELECT
				MIN(COALESCE(a1.[NumberInStock], p.NumberInStock))
			FROM
				[productlek].[tPackageProduct] pp
				INNER JOIN [product].[tProduct] p ON p.[ProductId] = pp.[ProductId]
				CROSS APPLY (
					SELECT
						SUM(ps.[NumberInStock]) AS 'NumberInStock'
					FROM
						[lekmer].[tProductSize] ps
					WHERE
						[ps].[ProductId] = [p].[ProductId]
				) a1
			WHERE
				[pp].[PackageId] = @PackageId
		)

		IF @NumberInStock IS NOT NULL
		BEGIN
			UPDATE
				[product].[tProduct]
			SET
				[NumberInStock] = @NumberInStock
			WHERE
				[ProductId] = @MasterProductId
				AND
				[NumberInStock] != @NumberInStock
		END

		FETCH NEXT FROM cur_package
		INTO @PackageId, @MasterProductId
	END

	CLOSE cur_package
	DEALLOCATE cur_package
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductTagsDeleteByIds]'
GO
CREATE PROCEDURE [lekmer].[pProductTagsDeleteByIds]
	@ProductTagIds	VARCHAR(MAX),
	@Delimiter		CHAR(1) = ','
AS
BEGIN
	DELETE pt
	FROM [lekmer].[tProductTag] pt
	INNER JOIN [generic].[fnConvertIDListToTable](@ProductTagIds, @Delimiter) AS t ON [t].[ID] = [pt].[ProductTagId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pOfflineProductScript]'
GO
ALTER PROCEDURE [integration].[pOfflineProductScript]

AS
BEGIN	
	IF OBJECT_ID('tempdb..#tProductsToUpdateStatusIncrement') IS NOT NULL
	DROP TABLE #tProductsToUpdateStatusIncrement

	CREATE TABLE #tProductsToUpdateStatusIncrement
	(
		ProductId INT NOT NULL,
		CONSTRAINT PK_#tProductsToUpdateStatusIncrement PRIMARY KEY(ProductId)
	)

	DECLARE @Counter INT
	DECLARE @TotalAmountProducts INT
	DECLARE @HighestCurrentProductId INT
	DECLARE @RowSize INT
	SET @Counter = 1
	SET @TotalAmountProducts = (SELECT COUNT(ProductId) FROM product.tProduct)
	SET @HighestCurrentProductId = 1
	SET @RowSize = 200

	WHILE @counter <= @TotalAmountProducts
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			-- Fetch @RowSize products at the time
			INSERT INTO #tProductsToUpdateStatusIncrement(ProductId)
			SELECT TOP (@RowSize)
				ProductId
			FROM
				product.tProduct
			WHERE
				ProductId > @HighestCurrentProductId
				--and isdeleted = false (0?)
			ORDER BY
				ProductId


			-- Get highest productId from tmpTable
			SET @HighestCurrentProductId = (SELECT MAX(ProductId) FROM #tProductsToUpdateStatusIncrement)


			-- set products online that fulfill the criterias
			UPDATE
				p
			SET
				ProductStatusId = 0
			FROM
				product.tProduct p
				INNER JOIN #tProductsToUpdateStatusIncrement i ON p.ProductId = i.ProductId
				LEFT JOIN lekmer.tProductSize s ON p.ProductId = s.ProductId
			WHERE
				p.Mediaid IS NOT NULL
				AND
				p.ProductStatusId = 1
				AND
				[Description] IS NOT NULL
				AND
				(
					(p.NumberInStock > 0 AND s.ProductId IS NULL)
					OR
					(s.NumberInStock > 0)
				)


			-- set products offline that fulfill the criterias
			UPDATE
				p
			SET
				ProductStatusId = 1
			FROM
				#tProductsToUpdateStatusIncrement i
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [i].[ProductId]
				INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [lp].[ProductId]
				INNER JOIN product.tCategory c1 ON c1.CategoryId = p.CategoryId
				INNER JOIN product.tCategory c2 ON c1.ParentCategoryId = c2.CategoryId
				INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
			WHERE
				(
					c3.[ErpId] = 'C_50' -- Barnkläder
					OR
					c1.[ErpId] = 'C_10-323-2200' -- Barn & Baby > Graviditet & amning > Gravidkläder & tillbehör
					OR
					c1.[ErpId] = 'C_10-323-2202' -- Barn & Baby > Graviditet & amning > Amningskläder
				)
				AND
				p.ProductStatusId = 0
				AND
				p.NumberInStock = 0
				AND
				[lp].[ProductTypeId] = 1
				AND NOT EXISTS
				(
					SELECT 1
					FROM lekmer.tProductSize ps
					WHERE
						ps.ProductId = i.ProductId
						AND
						ps.NumberInStock > 0
				)


			SET @Counter = @Counter + @RowSize
			COMMIT

			-- Clear tmp table
			DELETE FROM #tProductsToUpdateStatusIncrement

		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			-- LOG here
			INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
			VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
		END CATCH
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
