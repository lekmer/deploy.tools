SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [customerlek].[pClearCustomerData]'
GO
CREATE PROCEDURE [customerlek].[pClearCustomerData]
	@ChannelId	INT,
	@Email		NVARCHAR(MAX)
AS
BEGIN
BEGIN TRANSACTION

	DELETE mp FROM [lekmer].[tMonitorProduct] mp WHERE [mp].[Email] = @Email

	DELETE sc FROM [orderlek].[tSavedCart] sc WHERE [sc].[Email] = @Email

	UPDATE r
	SET [r].[Email] = (CASE WHEN [r].[Email] IS NOT NULL  THEN 'removed@removed.se' ELSE NULL END)
	FROM [review].[tReview] r
	WHERE [r].[Email] = @Email

	IF NOT EXISTS (SELECT 1 FROM [lekmer].[tNewsletterUnsubscriber] WHERE [Email] = @Email)
	BEGIN
		INSERT INTO [lekmer].[tNewsletterUnsubscriber] ([ChannelId],[Email],[SentStatus],[CreatedDate],[UpdatedDate])
		VALUES(@ChannelId,@Email,NULL,GETDATE(),GETDATE())
	END
	DECLARE @Id INT
	SET @Id = (SELECT [UnsubscriberId] FROM [lekmer].[tNewsletterUnsubscriber] WHERE [Email] = @Email)
	INSERT INTO [lekmer].[tNewsletterUnsubscriberOption]
	SELECT @Id, [t].[NewsletterTypeId], GETDATE() FROM [lekmer].[tNewsletterType] t
	WHERE NOT EXISTS (SELECT 1 FROM [lekmer].[tNewsletterUnsubscriberOption] WHERE [UnsubscriberId] = @Id AND [NewsletterTypeId] = [t].[NewsletterTypeId])
	------------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------------
	-- Order
	UPDATE a
	SET 
		a.Addressee = (CASE WHEN a.Addressee IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.StreetAddress = (CASE WHEN a.StreetAddress IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.StreetAddress2 = (CASE WHEN a.StreetAddress2 IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.PostalCode = (CASE WHEN a.PostalCode IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.City = (CASE WHEN a.City IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.PhoneNumber = (CASE WHEN a.PhoneNumber IS NOT NULL  THEN 'Removed' ELSE NULL END)
	FROM [order].[tOrderAddress] a
	INNER JOIN [order].[tOrder] o ON [o].[DeliveryAddressId] = [a].[OrderAddressId] OR [o].[BillingAddressId] = [a].[OrderAddressId]
	WHERE [o].[Email] = @Email
	--------------------------------------------------------------------------------------------------
	UPDATE a
	SET 
		a.Addressee = (CASE WHEN a.Addressee IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.StreetAddress = (CASE WHEN a.StreetAddress IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.StreetAddress2 = (CASE WHEN a.StreetAddress2 IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.PostalCode = (CASE WHEN a.PostalCode IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.City = (CASE WHEN a.City IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.PhoneNumber = (CASE WHEN a.PhoneNumber IS NOT NULL  THEN 'Removed' ELSE NULL END)
	FROM [order].[tOrderAddress] a
	INNER JOIN [lekmer].[tLekmerOrder] lo ON [lo].[AlternateAddressId] = [a].[OrderAddressId]
	INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [lo].[OrderId]
	WHERE [o].[Email] = @Email
	--------------------------------------------------------------------------------------------------
	UPDATE a
	SET 
		a.HouseNumber = (CASE WHEN a.HouseNumber IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.HouseExtension = (CASE WHEN a.HouseExtension IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.Reference = (CASE WHEN a.Reference IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.DoorCode = (CASE WHEN a.DoorCode IS NOT NULL  THEN 'Removed' ELSE NULL END)
	FROM [orderlek].[tOrderAddress] a
	INNER JOIN [order].[tOrder] o ON [o].[DeliveryAddressId] = [a].[OrderAddressId] OR [o].[BillingAddressId] = [a].[OrderAddressId]
	WHERE [o].[Email] = @Email
	--------------------------------------------------------------------------------------------------
	UPDATE a
	SET 
		a.HouseNumber = (CASE WHEN a.HouseNumber IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.HouseExtension = (CASE WHEN a.HouseExtension IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.Reference = (CASE WHEN a.Reference IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.DoorCode = (CASE WHEN a.DoorCode IS NOT NULL  THEN 'Removed' ELSE NULL END)
	FROM [orderlek].[tOrderAddress] a
	INNER JOIN [lekmer].[tLekmerOrder] lo ON [lo].[AlternateAddressId] = [a].[OrderAddressId]
	INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [lo].[OrderId]
	WHERE [o].[Email] = @Email
	--------------------------------------------------------------------------------------------------
	UPDATE lo
	SET 
		lo.CivicNumber = (CASE WHEN lo.CivicNumber IS NOT NULL  THEN 'Removed' ELSE NULL END),
		[lo].[NeedSendInsuranceInfo] = 0
	FROM [lekmer].[tLekmerOrder] lo 
	INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [lo].[OrderId]
	WHERE [o].[Email] = @Email
	--------------------------------------------------------------------------------------------------
	UPDATE o
	SET 
		o.Email = (CASE WHEN o.Email IS NOT NULL  THEN 'removed@removed.se' ELSE NULL END)
	FROM [order].[tOrder] o 
	WHERE [o].[Email] = @Email
	------------------------------------------------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------------------------------------
	-- Customer
	UPDATE a
	SET 
		a.HouseNumber = (CASE WHEN a.HouseNumber IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.HouseExtension = (CASE WHEN a.HouseExtension IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.Reference = (CASE WHEN a.Reference IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.DoorCode = (CASE WHEN a.DoorCode IS NOT NULL  THEN 'Removed' ELSE NULL END)
	FROM [customerlek].[tAddress] a
	INNER JOIN [customer].[tCustomerInformation] ci ON [ci].[CustomerId] = [a].[CustomerId]
	WHERE [ci].[Email] = @Email
	--------------------------------------------------------------------------------------------------
	UPDATE a
	SET 
		a.Addressee = (CASE WHEN a.Addressee IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.StreetAddress = (CASE WHEN a.StreetAddress IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.StreetAddress2 = (CASE WHEN a.StreetAddress2 IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.PostalCode = (CASE WHEN a.PostalCode IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.City = (CASE WHEN a.City IS NOT NULL  THEN 'Removed' ELSE NULL END),
		a.PhoneNumber = (CASE WHEN a.PhoneNumber IS NOT NULL  THEN 'Removed' ELSE NULL END)
	FROM [customer].[tAddress] a
	INNER JOIN [customer].[tCustomerInformation] ci ON [ci].[CustomerId] = [a].[CustomerId]
	WHERE [ci].[Email] = @Email
	--------------------------------------------------------------------------------------------------
	UPDATE ci
	SET 
		ci.FirstName = (CASE WHEN ci.FirstName IS NOT NULL  THEN 'Removed' ELSE NULL END),
		ci.LastName = (CASE WHEN ci.LastName IS NOT NULL  THEN 'Removed' ELSE NULL END),
		ci.CivicNumber = (CASE WHEN ci.CivicNumber IS NOT NULL  THEN 'Removed' ELSE NULL END),
		ci.PhoneNumber = (CASE WHEN ci.PhoneNumber IS NOT NULL  THEN 'Removed' ELSE NULL END),
		ci.CellPhoneNumber = (CASE WHEN ci.CellPhoneNumber IS NOT NULL  THEN 'Removed' ELSE NULL END),
		ci.Email = (CASE WHEN ci.Email IS NOT NULL  THEN 'removed@removed.se' ELSE NULL END)
	FROM [customer].[tCustomerInformation] ci
	WHERE [ci].[Email] = @Email
	--------------------------------------------------------------------------------------------------
	UPDATE c
	SET 
		c.ErpId = (CASE WHEN c.ErpId IS NOT NULL  THEN 'Removed' ELSE NULL END)
	FROM [customer].[tCustomer] c
	INNER JOIN [customer].[tCustomerInformation] ci ON [ci].[CustomerId] = [c].[CustomerId]
	WHERE [ci].[Email] = @Email

COMMIT
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
