SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[tLekmerOrder]'
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD
[UserAgent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vCustomOrder]'
GO
ALTER VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		[lo].[PaymentCost] AS [Lekmer.PaymentCost],
		[lo].[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey],
		[lo].[FeedbackToken] AS [Lekmer.FeedbackToken],
		[lo].[AlternateAddressId] AS [Lekmer.AlternateAddressId],
		[lo].[NeedSendInsuranceInfo] AS [Lekmer.NeedSendInsuranceInfo],
		[lo].[CivicNumber] AS [Lekmer.CivicNumber],
		[lo].[OptionalDeliveryMethodId] AS [Lekmer.OptionalDeliveryMethodId],
		[lo].[OptionalFreightCost] AS [Lekmer.OptionalFreightCost],
		[lo].[DiapersDeliveryMethodId] AS [Lekmer.DiapersDeliveryMethodId],
		[lo].[DiapersFreightCost] AS [Lekmer.DiapersFreightCost],
		[lo].[UserAgent] AS [Lekmer.UserAgent]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pOrderSave]'
GO
ALTER PROCEDURE [lekmer].[pOrderSave]
	@OrderId					INT,
	@PaymentCost				DECIMAL(16,2),
	@CustomerIdentificationKey	NCHAR(50),
	@AlternateAddressId			INT = NULL,
	@CivicNumber				NVARCHAR(50) = NULL,
	@OptionalDeliveryMethodId	INT,
	@OptionalFreightCost		DECIMAL(16, 2),
	@DiapersDeliveryMethodId	INT,
	@DiapersFreightCost			DECIMAL(16, 2),
	@UserAgent					NVARCHAR(MAX) = NULL
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		[PaymentCost] = @PaymentCost,
		[CustomerIdentificationKey] = @CustomerIdentificationKey,
		[AlternateAddressId] = @AlternateAddressId,
		[CivicNumber] = @CivicNumber,
		[OptionalDeliveryMethodId] = @OptionalDeliveryMethodId,
		[OptionalFreightCost] = @OptionalFreightCost,
		[DiapersDeliveryMethodId] = @DiapersDeliveryMethodId,
		[DiapersFreightCost] = @DiapersFreightCost,
		[UserAgent] = @UserAgent
	WHERE 
		[OrderId] = @OrderId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tLekmerOrder]
		(
			[OrderId],
			[PaymentCost],
			[CustomerIdentificationKey],
			[AlternateAddressId],
			[CivicNumber],
			[OptionalDeliveryMethodId],
			[OptionalFreightCost],
			[DiapersDeliveryMethodId],
			[DiapersFreightCost],
			[UserAgent]
		)
		VALUES
		(
			@OrderId,
			@PaymentCost,
			@CustomerIdentificationKey,
			@AlternateAddressId,
			@CivicNumber,
			@OptionalDeliveryMethodId,
			@OptionalFreightCost,
			@DiapersDeliveryMethodId,
			@DiapersFreightCost,
			@UserAgent
		)
	END
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
