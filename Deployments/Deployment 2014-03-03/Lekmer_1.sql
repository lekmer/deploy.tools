SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [orderlek].[tMaksuturvaCompensationError]'
GO
CREATE TABLE [orderlek].[tMaksuturvaCompensationError]
(
[MaksuturvaCompensationErrorId] [int] NOT NULL IDENTITY(1, 1),
[ChannelCommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[FetchDate] [datetime] NOT NULL,
[LastAttempt] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tMaksuturvaCompensationError] on [orderlek].[tMaksuturvaCompensationError]'
GO
ALTER TABLE [orderlek].[tMaksuturvaCompensationError] ADD CONSTRAINT [PK_tMaksuturvaCompensationError] PRIMARY KEY CLUSTERED  ([MaksuturvaCompensationErrorId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pMaksuturvaCompensationErrorGet]'
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaCompensationErrorGet]
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[MaksuturvaCompensationErrorId]	'MaksuturvaCompensationError.MaksuturvaCompensationErrorId',
		[ChannelCommonName] 'MaksuturvaCompensationError.ChannelCommonName',
		[FetchDate] 'MaksuturvaCompensationError.FetchDate',
		[LastAttempt] 'MaksuturvaCompensationError.LastAttempt'
	FROM
		[orderlek].[tMaksuturvaCompensationError]
	ORDER BY
		[MaksuturvaCompensationErrorId] ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pMaksuturvaCompensationErrorDelete]'
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaCompensationErrorDelete]
	@MaksuturvaCompensationErrorId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DELETE [orderlek].[tMaksuturvaCompensationError]
	WHERE [MaksuturvaCompensationErrorId] = @MaksuturvaCompensationErrorId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pMaksuturvaCompensationErrorInsert]'
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaCompensationErrorInsert]
	@ChannelCommonName	VARCHAR(50),
	@FetchDate			DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [orderlek].[tMaksuturvaCompensationError] (
		[ChannelCommonName],
		[FetchDate]
	)
	VALUES (
		@ChannelCommonName,
		@FetchDate
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pMaksuturvaCompensationErrorUpdate]'
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaCompensationErrorUpdate]
	@MaksuturvaCompensationErrorId	INT,
	@LastAttempt					DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tMaksuturvaCompensationError]
	SET	
		[LastAttempt] = @LastAttempt
	WHERE
		[MaksuturvaCompensationErrorId] = @MaksuturvaCompensationErrorId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
