INSERT INTO [campaignlek].[tCampaignLevel] ([Title], [Priority])
VALUES ('First Level Campaign',1),('Second Level Campaign',2)
SELECT * FROM [campaignlek].[tCampaignLevel]

UPDATE [campaign].[tCampaign] SET [LevelId] = 1 WHERE [LevelId] IS NULL
ALTER TABLE [campaign].[tCampaign] ALTER COLUMN [LevelId] INT NOT NULL