SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [corelek]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tCampaignFlag]'
GO
ALTER TABLE [lekmer].[tCampaignFlag] DROP CONSTRAINT[FK_tCampaignFlag_tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tCartCampaign]'
GO
ALTER TABLE [campaign].[tCartCampaign] DROP CONSTRAINT[FK_tCartCampaign_tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] DROP CONSTRAINT[FK_tGiftCardViaMailInfo_tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tProductCampaign]'
GO
ALTER TABLE [campaign].[tProductCampaign] DROP CONSTRAINT[FK_tProductCampaign_tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tCondition]'
GO
ALTER TABLE [campaign].[tCondition] DROP CONSTRAINT[FK_tCondition_tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [campaign].[tCampaign]'
GO
ALTER TABLE [campaign].[tCampaign] DROP CONSTRAINT[FK_tCampaign_tCampaignRegistry]
ALTER TABLE [campaign].[tCampaign] DROP CONSTRAINT[FK_tCampaign_tCampaignFolder]
ALTER TABLE [campaign].[tCampaign] DROP CONSTRAINT[FK_tCampaign_tCampaignStatus]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tCampaign]'
GO
ALTER TABLE [campaign].[tCampaign] DROP CONSTRAINT [PK_tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [campaign].[tCampaign]'
GO
ALTER TABLE [campaign].[tCampaign] DROP CONSTRAINT [DF_tCampaign_Exclusive]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tCampaign_CampaignRegistryId] from [campaign].[tCampaign]'
GO
DROP INDEX [IX_tCampaign_CampaignRegistryId] ON [campaign].[tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tCampaign_CampaignStatusId] from [campaign].[tCampaign]'
GO
DROP INDEX [IX_tCampaign_CampaignStatusId] ON [campaign].[tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[tCacheUpdate]'
GO
CREATE TABLE [corelek].[tCacheUpdate]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[ManagerName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[UpdateType] [int] NOT NULL,
[Key] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_tCacheUpdate_CreationDate] DEFAULT (getdate()),
[InsertionDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCacheUpdate] on [corelek].[tCacheUpdate]'
GO
ALTER TABLE [corelek].[tCacheUpdate] ADD CONSTRAINT [PK_tCacheUpdate] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [campaign].[tCampaign]'
GO
CREATE TABLE [campaign].[tmp_rg_xx_tCampaign]
(
[CampaignId] [int] NOT NULL IDENTITY(1, 1),
[CampaignRegistryId] [int] NOT NULL,
[FolderId] [int] NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CampaignStatusId] [int] NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Exclusive] [bit] NOT NULL CONSTRAINT [DF_tCampaign_Exclusive] DEFAULT ((0)),
[Priority] [int] NOT NULL,
[LevelId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [campaign].[tmp_rg_xx_tCampaign] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [campaign].[tmp_rg_xx_tCampaign]([CampaignId], [CampaignRegistryId], [FolderId], [Title], [CampaignStatusId], [StartDate], [EndDate], [Exclusive], [Priority]) SELECT [CampaignId], [CampaignRegistryId], [FolderId], [Title], [CampaignStatusId], [StartDate], [EndDate], [Exclusive], [Priority] FROM [campaign].[tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [campaign].[tmp_rg_xx_tCampaign] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[campaign].[tCampaign]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[campaign].[tmp_rg_xx_tCampaign]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [campaign].[tCampaign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[campaign].[tmp_rg_xx_tCampaign]', N'tCampaign'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaign] on [campaign].[tCampaign]'
GO
ALTER TABLE [campaign].[tCampaign] ADD CONSTRAINT [PK_tCampaign] PRIMARY KEY CLUSTERED  ([CampaignId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tCampaign_CampaignRegistryId] on [campaign].[tCampaign]'
GO
CREATE NONCLUSTERED INDEX [IX_tCampaign_CampaignRegistryId] ON [campaign].[tCampaign] ([CampaignRegistryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tCampaign_CampaignStatusId] on [campaign].[tCampaign]'
GO
CREATE NONCLUSTERED INDEX [IX_tCampaign_CampaignStatusId] ON [campaign].[tCampaign] ([CampaignStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tCampaignLevel]'
GO
CREATE TABLE [campaignlek].[tCampaignLevel]
(
[LevelId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Priority] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCampaignLevel] on [campaignlek].[tCampaignLevel]'
GO
ALTER TABLE [campaignlek].[tCampaignLevel] ADD CONSTRAINT [PK_tCampaignLevel] PRIMARY KEY CLUSTERED  ([LevelId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[vCampaign]'
GO
ALTER VIEW [campaign].[vCampaign]
AS
	SELECT
		[c].[CampaignId]			AS 'Campaign.Id',
		[c].[CampaignRegistryId]	AS 'Campaign.CampaignRegistryId',
		[c].[FolderId]				AS 'Campaign.FolderId',
		[c].[Title]					AS 'Campaign.Title',
		[c].[CampaignStatusId]		AS 'Campaign.CampaignStatusId',
		[c].[StartDate]				AS 'Campaign.StartDate',
		[c].[EndDate]				AS 'Campaign.EndDate',
		[c].[Priority]				AS 'Campaign.Priority',
		[c].[Exclusive]				AS 'Campaign.Exclusive',
		[c].[LevelId]				AS 'Campaign.LevelId',
		[cl].[Priority]				AS 'Campaign.LevelPriority',
		s.*
	FROM
		[campaign].[tCampaign] c
		INNER JOIN [campaign].[vCustomCampaignStatus] s ON [c].[CampaignStatusId] = [s].[CampaignStatus.Id]
		INNER JOIN [campaignlek].[tCampaignLevel] cl ON [cl].[LevelId] = [c].[LevelId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCartCampaignGetIdAll]'
GO
ALTER PROCEDURE [campaign].[pCartCampaignGetIdAll]
	@ChannelId INT
AS
BEGIN
	SELECT
		[c].[Campaign.Id]
	FROM
		[campaign].[vCustomCartCampaign] c
		INNER JOIN [campaign].[tCampaignModuleChannel] m ON [m].[CampaignRegistryId] = [c].[Campaign.CampaignRegistryId]
	WHERE
		[m].[ChannelId] = @ChannelId
		AND [c].[Campaign.CampaignStatusId] = 0
	ORDER BY
		[c].[Campaign.LevelPriority],
		[c].[Campaign.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[pCacheUpdateDeleteOld]'
GO

CREATE PROCEDURE [corelek].[pCacheUpdateDeleteOld]
	@ToDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[corelek].[tCacheUpdate]
	WHERE
		[CreationDate] < @ToDate
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pProductGetIdAllAvaliableForCdonExport]'
GO
CREATE PROCEDURE [export].[pProductGetIdAllAvaliableForCdonExport]
AS
BEGIN
	SELECT [p1].[ProductId]
	FROM [product].[tProduct] p1
	WHERE 
		[p1].[IsDeleted] = 0
		AND [p1].[ProductStatusId] = 0
	UNION
	SELECT DISTINCT ([p2].[ProductId]) FROM [product].[tProduct] p2
	INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[ProductId] = [p2].[ProductId]
	INNER JOIN [order].[tOrderItem] oi ON [oi].[OrderItemId] = [oip].[OrderItemId]
	INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [oi].[OrderId]
	WHERE 
		[p2].[IsDeleted] = 0
		AND [p2].[ProductStatusId] = 1
		AND [o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > DATEADD(MONTH, -6, GETDATE())
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [integration].[pProductVariantGetIdAllByProductForCdonExport]'
GO
CREATE PROCEDURE [integration].[pProductVariantGetIdAllByProductForCdonExport]
	@ProductId		INT,
	@RelationListTypeId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #ProductIds(ProductId INT)
	INSERT INTO [#ProductIds]([ProductId])EXEC [export].[pProductGetIdAllAvaliableForCdonExport]
	
	SELECT
		DISTINCT rlp.[ProductId]
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.[RelationListId] = rl.[RelationListId]
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.[RelationListId] = rlp.[RelationListId]
	WHERE
		prl.[ProductId] = @ProductId
		AND rlp.[ProductId] <> @ProductId
		AND rl.[RelationListTypeId] = @RelationListTypeId
		AND rlp.[ProductId] IN (
			SELECT DISTINCT ([up].[ProductId])
			FROM (SELECT * FROM #ProductIds) up)

	DROP TABLE #ProductIds
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[pRatingReviewFeedback_Search]'
GO

ALTER PROCEDURE [review].[pRatingReviewFeedback_Search]
	@Author					NVARCHAR(50),
	@Message				NVARCHAR(500),
	@StatusId				INT,
	@CreatedFrom			DATETIME,
	@CreatedTo				DATETIME,
	@ProductId				INT,
	@ProductTitle			NVARCHAR(256),
	@OrderId				INT,
	@InappropriateContent	BIT,
	@SortBy					VARCHAR(50) = NULL,
	@SortDescending			BIT = NULL,
	@Page					INT = NULL,
	@PageSize				INT
AS   
BEGIN
	SET NOCOUNT ON
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)
     
    SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' + COALESCE(@SortBy, 'RatingReviewFeedback.CreatedDate')+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			rr.*
		FROM
			[review].[vReviewRecord] rr
		WHERE
			(@ProductId IS NULL OR rr.[RatingReviewFeedback.ProductId] = @ProductId)'
			+ CASE WHEN (@ProductTitle IS NOT NULL) THEN + 'AND rr.[ReviewRecord.Product.Title] LIKE ''%' + REPLACE(@ProductTitle, '''', '''''') + '%''' ELSE '' END
			+ CASE WHEN (@StatusId IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.RatingReviewStatusId] = @StatusId ' ELSE '' END
			+ CASE WHEN (@Message IS NOT NULL) THEN + 'AND (rr.[Review.Title] LIKE ''%' + REPLACE(@Message, '''', '''''') + '%'' OR rr.[Review.Message] LIKE ''%' + REPLACE(@Message, '''', '''''') + '%'')' ELSE '' END
			+ CASE WHEN (@Author IS NOT NULL) THEN + 'AND rr.[Review.AuthorName] = @Author ' ELSE '' END
			+ CASE WHEN (@OrderId IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.OrderId] = @OrderId ' ELSE '' END
			+ CASE WHEN (@InappropriateContent = 1) THEN + 'AND rr.[RatingReviewFeedback.Impropriate] = 1 ' ELSE '' END
			+ CASE WHEN (@CreatedFrom IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.CreatedDate] >= @CreatedFrom ' ELSE '' END
			+ CASE WHEN (@CreatedTo IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.CreatedDate] <= @CreatedTo ' ELSE '' END

     SET @sql = 'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

     IF @Page != 0 AND @Page IS NOT NULL 
     BEGIN
		SET @sql = @sql + '
		WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
     END
     
     SET @sqlCount = 'SELECT COUNT(1) FROM (' + @sqlFragment + ') AS CountResults'
     
	EXEC sp_executesql @sqlCount,
		N'      
			@Author					NVARCHAR(50),
			@Message				NVARCHAR(500),
			@StatusId				INT,
			@CreatedFrom			DATETIME,
            @CreatedTo				DATETIME,
            @ProductId				INT,
            @ProductTitle			NVARCHAR(256),
            @OrderId				INT,
            @Page					INT,
            @PageSize				INT',
            @Author,
            @Message,
            @StatusId,
            @CreatedFrom,
            @CreatedTo,
            @ProductId,
            @ProductTitle,
            @OrderId,
            @Page,
            @PageSize
                             
	EXEC sp_executesql @sql, 
		N'      
			@Author					NVARCHAR(50),
			@Message				NVARCHAR(500),
			@StatusId				INT,
			@CreatedFrom			DATETIME,
            @CreatedTo				DATETIME,
            @ProductId				INT,
            @ProductTitle			NVARCHAR(256),
            @OrderId				INT,
            @Page					INT,
            @PageSize				INT',
            @Author,
            @Message,
            @StatusId,
            @CreatedFrom,
            @CreatedTo,
            @ProductId,
            @ProductTitle,
            @OrderId,
            @Page,
            @PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vSize]'
GO


ALTER VIEW [lekmer].[vSize]
AS
SELECT
	[SizeId] AS 'Size.Id',
	[ErpId] AS 'Size.ErpId',
	[ErpTitle] AS 'Size.ErpTitle',
	[EU] AS 'Size.EU',
	[EUTitle] AS 'Size.EUTitle',
	[USMale] AS 'Size.USMale',
	[USFemale] AS 'Size.USFemale',
	[UKMale] AS 'Size.UKMale',
	[UKFemale] AS 'Size.UKFemale',
	[Millimeter] AS 'Size.Millimeter'
FROM
	[lekmer].[tSize]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[pCacheUpdateGetFromId]'
GO

CREATE PROCEDURE [corelek].[pCacheUpdateGetFromId]
	@FromId	BIGINT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[cu].[Id],
		[cu].[ManagerName],
		[cu].[UpdateType],
		[cu].[Key],
		[cu].[CreationDate]
	FROM
		[corelek].[tCacheUpdate] cu
	WHERE
		cu.[Id] > @FromId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [corelek].[pCacheUpdateSave]'
GO

CREATE PROCEDURE [corelek].[pCacheUpdateSave]
	@ManagerName	VARCHAR(100),
	@UpdateType		INT,
	@Key			VARCHAR(500),
	@CreationDate	DATETIME
AS
BEGIN
	SET NOCOUNT ON

	INSERT [corelek].[tCacheUpdate]
	(
		[ManagerName],
		[UpdateType],
		[Key],
		[CreationDate],
		[InsertionDate]
	)
	VALUES
	(
		@ManagerName,
		@UpdateType,
		@Key,
		@CreationDate,
		GETUTCDATE()
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pProductCampaignGetIdAll]'
GO
ALTER PROCEDURE [campaign].[pProductCampaignGetIdAll]
	@ChannelId INT
AS
BEGIN
	SELECT
		[c].[Campaign.Id]
	FROM
		[campaign].[vCustomProductCampaign] c
		INNER JOIN [campaign].[tCampaignModuleChannel] m ON [m].[CampaignRegistryId] = [c].[Campaign.CampaignRegistryId]
	WHERE
		[m].[ChannelId] = @ChannelId
		AND [c].[Campaign.CampaignStatusId] = 0
	ORDER BY
		[c].[Campaign.LevelPriority],
		[c].[Campaign.Priority]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignSave]'
GO
ALTER PROCEDURE [campaign].[pCampaignSave]
	@CampaignId			INT,
	@Title				VARCHAR(500),
	@CampaignStatusId	INT,
	@CampaignRegistryId INT,
	@FolderId			INT,
	@StartDate			DATETIME,
	@EndDate			DATETIME,
	@Exclusive			BIT,
	@Priority			INT,
	@LevelId			INT	
AS
BEGIN
	UPDATE
		[campaign].[tCampaign]
	SET
		[Title]					= @Title,
		[CampaignStatusId]		= @CampaignStatusId,
		[CampaignRegistryId]	= @CampaignRegistryId,
		[FolderId]				= @FolderId,
		[StartDate]				= @StartDate,
		[EndDate]				= @EndDate,
		[Exclusive]				= @Exclusive,
		[Priority]				= @Priority,
		[LevelId]				= @LevelId
	WHERE
		[CampaignId] = @CampaignId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		SET @Priority = (SELECT MAX(PRIORITY) FROM campaign.tCampaign) + 1
		IF @Priority IS NULL
			BEGIN
				SET @Priority = 1
			END
			
		INSERT campaign.tCampaign (
			[Title],
			[CampaignStatusId],
			[CampaignRegistryId],
			[FolderId],
			[StartDate],
			[EndDate],
			[Exclusive],
			[Priority],
			[LevelId]
		)
		VALUES (
			@Title,
			@CampaignStatusId,
			@CampaignRegistryId,
			@FolderId,
			@StartDate,
			@EndDate,
			@Exclusive,
			@Priority,
			@LevelId
		)
		
		SET @CampaignId = CAST(SCOPE_IDENTITY() AS INT)
	END
	RETURN @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pProductChangeCdonExportEventGetAllInQueue]'
GO
ALTER PROCEDURE [productlek].[pProductChangeCdonExportEventGetAllInQueue]
	@NumberOfItems INT,
	@CdonExportEventStatusId INT
AS 
BEGIN
	SET NOCOUNT ON

	CREATE TABLE #ProductIds(ProductId INT)
	INSERT INTO [#ProductIds]([ProductId])EXEC [export].[pProductGetIdAllAvaliableForCdonExport]

	SELECT TOP (@NumberOfItems)
		[pce].*
	FROM
		[productlek].[vProductChangeEvent] pce
	WHERE
		[pce].[ProductChangeEvent.CdonExportEventStatusId] = @CdonExportEventStatusId
		AND [pce].[ProductChangeEvent.ProductId] IN (
			SELECT DISTINCT ([up].[ProductId])
			FROM (SELECT * FROM #ProductIds) up)
	
	DROP TABLE #ProductIds
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pCampaignLevelGetAll]'
GO
CREATE PROCEDURE [campaignlek].[pCampaignLevelGetAll]
AS
BEGIN
	SELECT
		[LevelId]	'CampaignLevel.Id',
		[Title]		'CampaignLevel.Title',
		[Priority]	'CampaignLevel.Priority'
	FROM
		[campaignlek].[tCampaignLevel]
	ORDER BY
		[Priority] ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAllWithoutAnyFilterForCdonExport]'
GO
ALTER PROCEDURE [lekmer].[pProductGetIdAllWithoutAnyFilterForCdonExport]
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	CREATE TABLE #ProductIds(ProductId INT)
	INSERT INTO [#ProductIds]([ProductId])EXEC [export].[pProductGetIdAllAvaliableForCdonExport]

	SELECT COUNT(DISTINCT ([up].[ProductId]))
	FROM (SELECT * FROM #ProductIds) up

	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY [up].[ProductId]) AS Number,
			[up].[ProductId]
		FROM (SELECT * FROM #ProductIds) up
	) As Result
	WHERE Number > (@Page - 1) * @PageSize
		AND Number <= @Page * @PageSize
		
	DROP TABLE #ProductIds
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCartDeleteExpiredItems]'
GO

CREATE PROCEDURE [orderlek].[pCartDeleteExpiredItems]
	@LifeCycleDays INT
AS
BEGIN
	SET NOCOUNT ON;

	-- Protection of Fools
	IF @LifeCycleDays < 30
		SET @LifeCycleDays = 30
	
	DECLARE	@ExpirationDate DATETIME = DATEADD(DAY, -@LifeCycleDays, DATEDIFF(DAY, 0, GETDATE()))
	DECLARE	@BatchSize INT = 1000

	IF EXISTS ( SELECT * FROM tempdb..sysobjects WHERE id = OBJECT_ID('tempdb..#DeleteItems') ) 
		DROP TABLE #DeleteItems

	CREATE TABLE #DeleteItems
	(
	  CartId INT,
	  PRIMARY KEY ( CartId )
	)

	WHILE 1 = 1
	BEGIN

		INSERT	INTO [#DeleteItems]
				SELECT TOP ( @BatchSize ) c.[CartId]
				FROM [order].[tCart] c
				WHERE c.[CreatedDate] < @ExpirationDate
					
		IF @@ROWCOUNT = 0
			BREAK

		DELETE lci --tLekmerCartItem
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			LEFT OUTER JOIN [lekmer].[tLekmerCartItem] lci ON [lci].[CartItemId] = [ci].[CartItemId]
			
		DELETE ci --tCartItem
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			
		DELETE cio --CartItemOption
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [campaignlek].[tCartItemOption] cio ON [cio].[CartId] = [di].[CartId]
			
		DELETE c --tCart
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			
		TRUNCATE TABLE [#DeleteItems]
	END

	DROP TABLE [#DeleteItems]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pProductChangeEventDeleteExpiredItems]'
GO

ALTER PROCEDURE [productlek].[pProductChangeEventDeleteExpiredItems]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ExpirationDate DATETIME
	SET @ExpirationDate = DATEADD(DAY, -5, GETDATE())

	DELETE 
		pce
	FROM
		[productlek].[tProductChangeEvent] pce
	WHERE 
		pce.[CreatedDate] < @ExpirationDate
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBrandGetAllSecureList]'
GO

ALTER PROCEDURE [lekmer].[pBrandGetAllSecureList]
    @Page INT = NULL,
    @PageSize INT,
    @SortBy VARCHAR(20) = NULL,
    @SortDescending BIT = NULL
AS 
    BEGIN
        SET NOCOUNT ON
	
        IF ( generic.fnValidateColumnName(@SortBy) = 0 ) 
        BEGIN
            RAISERROR ( N'Illegal characters in string (parameter @SortBy): %s' , 16 , 1 , @SortBy ) ;
            RETURN
        END

        DECLARE @sql NVARCHAR(MAX)
        DECLARE @sqlCount NVARCHAR(MAX)
        DECLARE @sqlFragment NVARCHAR(MAX)

        SET @sqlFragment = '
			SELECT ROW_NUMBER() OVER (ORDER BY cast(lower(s.[' 
				+ COALESCE(@SortBy, 'Brand.Title') 
				+ ']) AS binary)'
				+ CASE WHEN ( @SortDescending = 1 ) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				*
			FROM [lekmer].[vBrandSecure] s'
			
        SET @sql = 'SELECT * FROM
			(' + @sqlFragment + '
			)
			AS SearchResult'
			
        IF @Page != 0 AND @Page IS NOT NULL 
        BEGIN
            SET @sql = @sql + '
			WHERE Number > ( @Page - 1 ) * @PageSize AND Number <= @Page * @PageSize'
        END
		
        SET @sqlCount = 'SELECT COUNT(1) FROM
			(
			' + @sqlFragment + '
			)
			AS CountResults'
			
        EXEC sp_executesql @sqlCount, N'@Page INT, @PageSize INT', @Page, @PageSize
        EXEC sp_executesql @sql, N'@Page INT, @PageSize INT', @Page, @PageSize
    END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaign].[tProductCampaign]'
GO
ALTER TABLE [campaign].[tProductCampaign] ADD CONSTRAINT [FK_tProductCampaign_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaign].[tCondition]'
GO
ALTER TABLE [campaign].[tCondition] ADD CONSTRAINT [FK_tCondition_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCampaignFlag]'
GO
ALTER TABLE [lekmer].[tCampaignFlag] ADD CONSTRAINT [FK_tCampaignFlag_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tGiftCardViaMailInfo]'
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfo] ADD CONSTRAINT [FK_tGiftCardViaMailInfo_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaign].[tCartCampaign]'
GO
ALTER TABLE [campaign].[tCartCampaign] ADD CONSTRAINT [FK_tCartCampaign_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaign].[tCampaign]'
GO
ALTER TABLE [campaign].[tCampaign] ADD CONSTRAINT [FK_tCampaign_tCampaignRegistry] FOREIGN KEY ([CampaignRegistryId]) REFERENCES [campaign].[tCampaignRegistry] ([CampaignRegistryId])
ALTER TABLE [campaign].[tCampaign] ADD CONSTRAINT [FK_tCampaign_tCampaignFolder] FOREIGN KEY ([FolderId]) REFERENCES [campaign].[tCampaignFolder] ([FolderId])
ALTER TABLE [campaign].[tCampaign] ADD CONSTRAINT [FK_tCampaign_tCampaignStatus] FOREIGN KEY ([CampaignStatusId]) REFERENCES [campaign].[tCampaignStatus] ([CampaignStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
