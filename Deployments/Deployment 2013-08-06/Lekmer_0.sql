SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [productlek].[tPackageProduct]'
GO
ALTER TABLE [productlek].[tPackageProduct] DROP CONSTRAINT [FK_tPackageProduct_tPackage]
ALTER TABLE [productlek].[tPackageProduct] DROP CONSTRAINT [FK_tPackageProduct_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [productlek].[tPackageProduct]'
GO
ALTER TABLE [productlek].[tPackageProduct] DROP CONSTRAINT [PK_tPackageProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [productlek].[tPackageProduct]'
GO
CREATE TABLE [productlek].[tmp_rg_xx_tPackageProduct]
(
[PackageProductId] [int] NOT NULL IDENTITY(1, 1),
[PackageId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [productlek].[tmp_rg_xx_tPackageProduct]([PackageId], [ProductId]) SELECT [PackageId], [ProductId] FROM [productlek].[tPackageProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [productlek].[tPackageProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[productlek].[tmp_rg_xx_tPackageProduct]', N'tPackageProduct'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPackageProduct] on [productlek].[tPackageProduct]'
GO
ALTER TABLE [productlek].[tPackageProduct] ADD CONSTRAINT [PK_tPackageProduct] PRIMARY KEY CLUSTERED  ([PackageProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageDecreaseNumberInStockByProduct]'
GO
ALTER PROCEDURE [productlek].[pPackageDecreaseNumberInStockByProduct]
	@ProductId	INT
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @tmpPackage TABLE (PackageId INT, MasterProductId INT, NumberInStock INT)
	INSERT INTO @tmpPackage (PackageId, MasterProductId)
	SELECT DISTINCT [pak].[PackageId], [pak].[MasterProductId] FROM [productlek].[tPackage] pak
		INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
		WHERE [pp].[ProductId] = @ProductId
		
	DECLARE @tmpPackageProduct TABLE (PackageId INT, ProductId INT, ItemsCount INT)	
	INSERT INTO @tmpPackageProduct (PackageId, ProductId, ItemsCount)
	SELECT DISTINCT [pak].[PackageId], [pp].[ProductId], [a0].[ItemsCount] FROM @tmpPackage pak
		INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
		OUTER APPLY
		(
			SELECT
				COUNT([pp0].[ProductId]) 'ItemsCount'
			FROM
				[productlek].[tPackageProduct] pp0
			WHERE
				[pp0].[PackageId] = [pak].[PackageId]
				AND [pp0].[ProductId] = [pp].[ProductId]
		) a0
	
	;WITH temp AS (
		SELECT  [pak].[PackageId], MIN(p1.[NumberInStock] / [pp].[ItemsCount]) as newNumberInStock
		FROM @tmpPackage pak
		INNER JOIN @tmpPackageProduct pp ON [pp].[PackageId] = [pak].[PackageId]
		INNER JOIN [product].[tProduct] p1 ON [p1].[ProductId] = [pp].[ProductId]
		GROUP BY [pak].[PackageId])
	UPDATE pak
	SET [pak].[NumberInStock] = newNumberInStock
	FROM @tmpPackage pak 
	INNER JOIN [temp] t ON [t].[PackageId] = [pak].[PackageId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDecreaseNumberInStock]'
GO
ALTER PROCEDURE [lekmer].[pProductDecreaseNumberInStock]
	@ProductId			INT,
	@Quantity			INT
AS 
BEGIN
	SET NOCOUNT ON

	-- Update Product NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @ProductId
		
	-- Update NumberInStock of Packages that contain current product
	EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageWithSizesDecreaseNumberInStock]'
GO
--DROP PROCEDURE [lekmer].[pPackageWithSizesDecreaseNumberInStock]

ALTER PROCEDURE [productlek].[pPackageWithSizesDecreaseNumberInStock]
	@PackageMasterProductId	INT,
	@Quantity				INT,
	@ItemsWithoutSizes		VARCHAR(max),
	@ItemsWithSizes			XML,
	@Delimiter				CHAR(1)
	
AS 
BEGIN
	SET NOCOUNT ON

	-- Update Product/Package NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @PackageMasterProductId

	-- products without sizes 
	DECLARE @tmpPackageProductWithoutSizes TABLE (ProductId INT)
	INSERT INTO @tmpPackageProductWithoutSizes (ProductId)
	SELECT [Id] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ItemsWithoutSizes, @Delimiter)
	
	-- Update NumberInStock of Packages that contain current product
	DECLARE @tmpProductId INT
	WHILE ((SELECT count(*) FROM @tmpPackageProductWithoutSizes) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProductWithoutSizes)

		-- Update Package Products NumberInSock
		UPDATE p
		SET
			[p].[NumberInStock] = (CASE WHEN [p].[NumberInStock] - @Quantity < 0 THEN 0
										ELSE [p].[NumberInStock] - @Quantity
								   END)
		FROM [product].[tProduct] p
		WHERE [p].[ProductId] = @tmpProductId
		
		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId
		DELETE TOP(1) @tmpPackageProductWithoutSizes WHERE [ProductId] = @tmpProductId
	END
	
	
	-- products with sizes 
	DECLARE @tmpPackageProductWithSizes TABLE (ProductId INT, SizeId INT)
	INSERT INTO @tmpPackageProductWithSizes (ProductId, [SizeId])
	SELECT c.value('@productId[1]', 'int'), c.value('@sizeId[1]', 'int') FROM @ItemsWithSizes.nodes('/items/item') T(c)

	-- Update NumberInStock of Packages that contain current product
	DECLARE @tmpSizeId INT
	WHILE ((SELECT count(*) FROM @tmpPackageProductWithSizes) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProductWithSizes)
		SET @tmpSizeId = (SELECT TOP 1 [SizeId] FROM @tmpPackageProductWithSizes)

		-- Update ProductSize NumberInSock
		UPDATE
			ps
		SET
			[ps].[NumberInStock] = (CASE WHEN [ps].[NumberInStock] - @Quantity < 0 THEN 0
										 ELSE [ps].[NumberInStock] - @Quantity END)
		FROM
			[lekmer].[tProductSize] ps
		WHERE [ps].[ProductId] = @tmpProductId AND [ps].[SizeId] = @tmpSizeId

		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId
		DELETE TOP(1) @tmpPackageProductWithSizes WHERE [ProductId] = @tmpProductId
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeDecreaseNumberInStock]'
GO
ALTER PROCEDURE [lekmer].[pProductSizeDecreaseNumberInStock]
	@ProductId INT,
	@SizeId INT,
	@Quantity INT
AS
BEGIN
	SET NOCOUNT ON

	-- Update ProductSize NumberInSock
	UPDATE
		[lekmer].[tProductSize]
	SET
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0 
								ELSE [NumberInStock] - @Quantity END)
	WHERE
		[ProductId] = @ProductId
		AND [SizeId] = @SizeId

	-- Update NumberInStock of Packages that contain current product	
	EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pPackageWithoutSizesDecreaseNumberInStock]'
GO
--DROP PROCEDURE [lekmer].[pPackageWithoutSizesDecreaseNumberInStock]

ALTER PROCEDURE [productlek].[pPackageWithoutSizesDecreaseNumberInStock]
	@PackageMasterProductId	INT,
	@Quantity				INT
AS 
BEGIN
	SET NOCOUNT ON

	-- Update Product/Package NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @PackageMasterProductId

	DECLARE @PackageId INT
	SET @PackageId = (SELECT [PackageId] FROM [productlek].[tPackage] WHERE [MasterProductId] = @PackageMasterProductId)

	DECLARE @tmpPackageProduct TABLE (ProductId INT)
	INSERT INTO @tmpPackageProduct (ProductId)
	SELECT [ProductId] FROM [productlek].[tPackageProduct] WHERE [PackageId] = @PackageId

	-- Update NumberInStock of Packages that contain current product
	DECLARE @tmpProductId INT
	WHILE ((SELECT count(*) FROM @tmpPackageProduct) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProduct)
		
		-- Update Package Products NumberInSock
		UPDATE p
		SET [p].[NumberInStock] = (CASE WHEN [p].[NumberInStock] - @Quantity < 0 THEN 0
										ELSE [p].[NumberInStock] - @Quantity
								   END)
		FROM [product].[tProduct] p
		WHERE [p].[ProductId] = @tmpProductId
		
		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId
		DELETE TOP(1) @tmpPackageProduct WHERE [ProductId] = @tmpProductId
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tPackageProduct]'
GO
ALTER TABLE [productlek].[tPackageProduct] ADD CONSTRAINT [FK_tPackageProduct_tPackage] FOREIGN KEY ([PackageId]) REFERENCES [productlek].[tPackage] ([PackageId])
ALTER TABLE [productlek].[tPackageProduct] ADD CONSTRAINT [FK_tPackageProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
