/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 8/6/2013 10:00:45 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModel]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModelSettingType]

-- Update row in [template].[tModelSetting]
UPDATE [template].[tModelSetting] SET [Title]=N'Max display count (obsolete)' WHERE [ModelSettingId]=25

-- Add rows to [template].[tModelSetting]
SET IDENTITY_INSERT [template].[tModelSetting] ON
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000014, 54, N'Adjacent pages count', N'AdjacentPages', 2)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000015, 54, N'Side pages count', N'SidePages', 2)
SET IDENTITY_INSERT [template].[tModelSetting] OFF
-- Operation applied to 2 rows out of 2

-- Add constraints to [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
ALTER TABLE [template].[tModelSetting] ADD CONSTRAINT [FK_tModelSetting_tModelSettingType] FOREIGN KEY ([ModelSettingTypeId]) REFERENCES [template].[tModelSettingType] ([ModelSettingTypeId])
COMMIT TRANSACTION
GO
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001163, 107, 2, N'ViewAllMode.Allowed', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001164, 107, 2, N'Page.IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001165, 107, 2, N'Page.IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001166, 107, 1, N'Page.ViewAll.Link', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001167, 107, 1, N'Page.Prev.Link', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001168, 107, 1, N'Page.Next.Link', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
GO
SET IDENTITY_INSERT [template].[tModelFragment] ON
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001328, 93, N'Pages break', N'PagesBreak', 54, 40, 5)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001329, 93, N'Page link separator', N'PageLinkSeparator', 54, 30, 5)
SET IDENTITY_INSERT [template].[tModelFragment] OFF
GO

