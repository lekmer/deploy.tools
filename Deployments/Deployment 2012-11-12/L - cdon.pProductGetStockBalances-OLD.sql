USE [Lekmer]
GO
/****** Object:  StoredProcedure [cdon].[pProductGetStockBalances]    Script Date: 11/12/2012 15:41:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [cdon].[pProductGetStockBalances] 
   @ArticleNumbers varchar(4000)
as
begin
   select lp.HyErpId as ArticleNumber, p.NumberInStock as CurrentStockBalance
     from generic.fStringToStringTable(@ArticleNumbers, ',', 1) a
         inner join lekmer.tLekmerProduct lp on a.SepString = lp.HyErpId
         inner join product.tProduct p on p.ProductId = lp.ProductId
end