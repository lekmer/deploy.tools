USE [Heppo]
GO
/****** Object:  StoredProcedure [cdon].[pProductGetStockBalances]    Script Date: 11/12/2012 14:54:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [cdon].[pProductGetStockBalances] 
   @ArticleNumbers varchar(4000)
as
begin
   	
	select 
		a.SepString as ArticleNumber,
		coalesce(ps.NumberInStock, p.NumberInStock) as CurrentStockBalance
	from 
		generic.fStringToStringTable(@ArticleNumbers, ',', 1) a
        left join lekmer.tLekmerProduct lp on Replace(a.SepString, '-251', '') = lp.HyErpId
        left join lekmer.tProductSize ps on a.SepString = ps.ErpId
        left join product.tProduct p on lp.ProductId = p.ProductId
           
end