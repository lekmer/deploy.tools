USE [Lekmer]
GO
/****** Object:  StoredProcedure [cdon].[pProductTryDecreaseNumberInStock]    Script Date: 11/12/2012 15:42:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [cdon].[pProductTryDecreaseNumberInStock]
	@ArticleNumber varchar(50),
	@QuantityToTakeFromStock int
as
begin
   begin transaction
      declare @ProductId int
      declare @CurrentQuantity int
      
      select @ProductId = lp.ProductId
            ,@CurrentQuantity = p.NumberInStock 
        from lekmer.tLekmerProduct lp
             inner join product.tProduct p on p.ProductId = lp.ProductId
       where lp.HyErpId = @ArticleNumber;
      
      if(@CurrentQuantity is null or @CurrentQuantity = 0)
      begin
         select @ArticleNumber as ArticleNumber
               ,0 as QuantityTakenFromStock
               ,0 as NewStockBalance
               ,cast(0 as bit) as Success
      end
      else
      begin
         if(@CurrentQuantity - @QuantityToTakeFromStock < 0)
         begin     
            update product.tProduct
               set NumberInStock = 0
             where ProductId = @ProductId
             
             select @ArticleNumber as ArticleNumber
                   ,@CurrentQuantity as QuantityTakenFromStock
                   ,0 as NewStockBalance
                   ,cast(1 as bit) as Success
         end
         else
         begin
            update product.tProduct
               set NumberInStock = @CurrentQuantity - @QuantityToTakeFromStock
             where ProductId = @ProductId
             
             select @ArticleNumber as ArticleNumber
                   ,@QuantityToTakeFromStock as QuantityTakenFromStock
                   ,@CurrentQuantity - @QuantityToTakeFromStock as NewStockBalance
                   ,cast(1 as bit) as Success
         end
      end
   commit
end

