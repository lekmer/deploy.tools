USE [Heppo]
GO
/****** Object:  StoredProcedure [cdon].[pProductTryDecreaseNumberInStock]    Script Date: 11/12/2012 14:54:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [cdon].[pProductTryDecreaseNumberInStock]
	@ArticleNumber varchar(50),
	@QuantityToTakeFromStock int
as
begin
   begin transaction
    
    declare @ProductId int
    declare @CurrentQuantity int
    declare @HasSize bit
    
    if @ArticleNumber like '%-251'
    begin		
		set @HasSize = 0
    end       
      
	select @ProductId = lp.ProductId
		,@CurrentQuantity = p.NumberInStock 
	from lekmer.tLekmerProduct lp
		 inner join product.tProduct p on p.ProductId = lp.ProductId
	where lp.HyErpId = Replace(@ArticleNumber, '-251', '')
       
       if @ProductId is null
       begin
			select 
			@ProductId = ps.ProductId,
            @CurrentQuantity = ps.NumberInStock 
			from lekmer.tProductSize ps				 
		   where ps.ErpId = @ArticleNumber
		   
		   set @HasSize = 1
       end
      
      if(@CurrentQuantity is null or @CurrentQuantity = 0)
      begin
         select @ArticleNumber as ArticleNumber
               ,0 as QuantityTakenFromStock
               ,0 as NewStockBalance
               ,cast(0 as bit) as Success
      end
      else
      begin
         if(@CurrentQuantity - @QuantityToTakeFromStock < 0)
         begin
         
			if @HasSize = 0
			begin
			
				update product.tProduct
				set NumberInStock = 0
				where ProductId = @ProductId
				
			end
            else
            begin
            
				update lekmer.tProductSize
				set NumberInStock = 0
				where ErpId = @ArticleNumber
				
            end
                         
             select @ArticleNumber as ArticleNumber
                   ,@CurrentQuantity as QuantityTakenFromStock
                   ,0 as NewStockBalance
                   ,cast(1 as bit) as Success
         end
         else
         begin
         
			if @HasSize = 0
			begin
			
				update product.tProduct
				set NumberInStock = @CurrentQuantity - @QuantityToTakeFromStock
				where ProductId = @ProductId
				
			end
            else
            begin
				
				update lekmer.tProductSize
				set NumberInStock = @CurrentQuantity - @QuantityToTakeFromStock
				where ErpId = @ArticleNumber			
				
            end
            
            
             
             select @ArticleNumber as ArticleNumber
                   ,@QuantityToTakeFromStock as QuantityTakenFromStock
                   ,@CurrentQuantity - @QuantityToTakeFromStock as NewStockBalance
                   ,cast(1 as bit) as Success
         end
      end
   commit
end

