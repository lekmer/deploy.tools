USE [Heppo]
GO
/****** Object:  StoredProcedure [cdon].[pProductGetStockBalances]    Script Date: 11/12/2012 14:58:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [cdon].[pProductGetStockBalances]
	@ArticleNumbers VARCHAR(4000)
AS 
BEGIN
	SELECT
		a.SepString AS ArticleNumber,
		COALESCE(ps.NumberInStock, p.NumberInStock) AS CurrentStockBalance
	FROM
		generic.fStringToStringTable(@ArticleNumbers, ',', 1) a
		LEFT JOIN lekmer.tProductSize ps ON a.SepString = ps.ErpId
		LEFT JOIN lekmer.tLekmerProduct lp ON SUBSTRING(a.SepString, 1, 11) = lp.HyErpId  -- [201166-0014]-251
		LEFT JOIN product.tProduct p ON lp.ProductId = p.ProductId
END