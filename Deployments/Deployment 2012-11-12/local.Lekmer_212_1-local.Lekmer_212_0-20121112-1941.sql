SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [corelek].[pRemoveFromCacheProduct]'
GO

CREATE PROCEDURE [corelek].[pRemoveFromCacheProduct]
	@ProductId INT,
	@SizeId INT
AS
BEGIN
	SET NOCOUNT ON

	IF @SizeId IS NOT NULL
	BEGIN
		INSERT INTO [corelek].[tCacheUpdate] ( [ManagerName], [UpdateType], [Key], [CreationDate], [InsertionDate] )
		VALUES
		( 'ProductSizeKey_IProductSize', 1, 'ProductSize_' + CONVERT(VARCHAR(50), @ProductId) + '_' + CONVERT(VARCHAR(50), @SizeId), GETUTCDATE(), GETUTCDATE() ),
		( 'ProductSizeCollectionKey_Collection`1', 1, 'ProductSizeCollection_' + CONVERT(VARCHAR(50), @ProductId), GETUTCDATE(), GETUTCDATE() )
	END
	ELSE
	BEGIN
		INSERT INTO [corelek].[tCacheUpdate]
		SELECT 'ProductKey_IProduct', 1, CONVERT(VARCHAR(50), [ChannelId]) + '-' + CONVERT(VARCHAR(50), @ProductId), GETUTCDATE(), GETUTCDATE() FROM [core].[tChannel]
	END
	
	INSERT INTO [corelek].[tCacheUpdate]
	SELECT 'ProductKey_IProductView', 1, CONVERT(VARCHAR(50), [ChannelId]) + '-' + CONVERT(VARCHAR(50), @ProductId), GETUTCDATE(), GETUTCDATE() FROM [core].[tChannel]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [cdon].[pProductGetStockBalances]'
GO
ALTER PROCEDURE [cdon].[pProductGetStockBalances]
	@ArticleNumbers VARCHAR(4000)
AS 
BEGIN
	SELECT
		a.SepString AS ArticleNumber,
		COALESCE(ps.NumberInStock, p.NumberInStock) AS CurrentStockBalance
	FROM
		generic.fStringToStringTable(@ArticleNumbers, ',', 1) a
		LEFT JOIN lekmer.tProductSize ps ON a.SepString = ps.ErpId
		LEFT JOIN lekmer.tLekmerProduct lp ON SUBSTRING(a.SepString, 1, 12) = lp.HyErpId  -- [0008706-1017]-223  // SELECT SUBSTRING('0008706-1017-223', 1, 12)
		LEFT JOIN product.tProduct p ON lp.ProductId = p.ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDecreaseNumberInStock]'
GO
ALTER PROCEDURE [lekmer].[pProductDecreaseNumberInStock]
	@ProductId INT,
	@Quantity INT
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		product.tProduct
	SET	
		NumberInStock = ( CASE WHEN NumberInStock - @Quantity < 0 THEN 0
							   ELSE NumberInStock - @Quantity
						  END )
	WHERE
		ProductId = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [cdon].[pProductTryDecreaseNumberInStock]'
GO

ALTER PROCEDURE [cdon].[pProductTryDecreaseNumberInStock]
	@ArticleNumber VARCHAR(50),
	@QuantityToTakeFromStock INT
AS 
BEGIN
	BEGIN TRANSACTION
    
	DECLARE	@ProductId INT
	DECLARE	@SizeId INT
	DECLARE	@NumberInStockCurrent INT


	-- Product with size ?
	SELECT
		@ProductId = [ProductId],
		@SizeId = [SizeId],
		@NumberInStockCurrent = [NumberInStock]
	FROM
		[lekmer].[tProductSize] ps
	WHERE
		ps.[ErpId] = @ArticleNumber


	-- Product without size ?
	IF @SizeId IS NULL
	SELECT
		@ProductId = lp.[ProductId],
		@NumberInStockCurrent = p.[NumberInStock]
	FROM
		[lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = lp.[ProductId]
	WHERE
		lp.[HYErpId] = SUBSTRING(@ArticleNumber, 1, 12)  -- [0008706-1017]-223  // SELECT SUBSTRING('0008706-1017-223', 1, 12)


	-- Product doesn't exist or has zero stock ?
	IF ( @NumberInStockCurrent IS NULL OR @NumberInStockCurrent = 0 ) 
	BEGIN
		SELECT
			@ArticleNumber AS ArticleNumber,
			0 AS QuantityTakenFromStock,
			0 AS NewStockBalance,
			CAST(0 AS BIT) AS Success
	END
	ELSE 
	BEGIN
		IF @SizeId IS NULL
			EXEC [lekmer].[pProductDecreaseNumberInStock] @ProductId, @QuantityToTakeFromStock
		ELSE
			EXEC [lekmer].[pProductSizeDecreaseNumberInStock] @ProductId, @SizeId, @QuantityToTakeFromStock

		IF ( @NumberInStockCurrent - @QuantityToTakeFromStock < 0 )
		BEGIN
			SELECT
				@ArticleNumber AS ArticleNumber,
				@NumberInStockCurrent AS QuantityTakenFromStock,
				0 AS NewStockBalance,
				CAST(1 AS BIT) AS Success
		END
		ELSE
		BEGIN
			SELECT
				@ArticleNumber AS ArticleNumber,
				@QuantityToTakeFromStock AS QuantityTakenFromStock,
				@NumberInStockCurrent - @QuantityToTakeFromStock AS NewStockBalance,
				CAST(1 AS BIT) AS Success
		END
		
		--Clear cache
		EXEC [corelek].[pRemoveFromCacheProduct] @ProductId, @SizeId
		
		--TrackProductChanges
		INSERT [productlek].[tProductChangeEvent] ( [ProductId], [EventStatusId], [CdonExportEventStatusId], [CreatedDate], [ActionAppliedDate], [Reference] )
		VALUES (@ProductId, 0, 0, GETDATE(), NULL, 'cdon product try decrease number in stock')
	END
	COMMIT
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
