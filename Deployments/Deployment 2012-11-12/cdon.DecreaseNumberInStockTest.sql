BEGIN TRANSACTION
	DECLARE @Now DATETIME = GETUTCDATE()
	
	--1038205 441154-0029 10
	
	SELECT p.[ProductId], p.[ErpId], p.[NumberInStock]
	FROM [product].[tProduct] p WHERE p.[ProductId] = 1038205
	
	EXEC [cdon].[pProductTryDecreaseNumberInStock] @ArticleNumber = '441154-0029-251', @QuantityToTakeFromStock = 5
	
	SELECT p.[ProductId], p.[ErpId], p.[NumberInStock]
	FROM [product].[tProduct] p WHERE p.[ProductId] = 1038205
	
	SELECT * FROM [corelek].[tCacheUpdate] WHERE [CreationDate] > @Now

ROLLBACK

BEGIN TRANSACTION

	--1038205 441154-0029 10
	
	SELECT p.[ProductId], p.[ErpId], p.[NumberInStock]
	FROM [product].[tProduct] p WHERE p.[ProductId] = 1038205
	
	EXEC [cdon].[pProductTryDecreaseNumberInStock] @ArticleNumber = '441154-0029-251', @QuantityToTakeFromStock = 15
	
	SELECT p.[ProductId], p.[ErpId], p.[NumberInStock]
	FROM [product].[tProduct] p WHERE p.[ProductId] = 1038205

ROLLBACK

BEGIN TRANSACTION

	--1039277 441236-0046 0
	
	SELECT p.[ProductId], p.[ErpId], p.[NumberInStock]
	FROM [product].[tProduct] p WHERE p.[ProductId] = 1039277
	
	EXEC [cdon].[pProductTryDecreaseNumberInStock] @ArticleNumber = '441236-0046-251', @QuantityToTakeFromStock = 15
	
	SELECT p.[ProductId], p.[ErpId], p.[NumberInStock]
	FROM [product].[tProduct] p WHERE p.[ProductId] = 1039277

ROLLBACK


BEGIN TRANSACTION

	--1038995 212126-0099-120 16
	
	SELECT ps.[ProductId], ps.[ErpId], ps.[NumberInStock]
	FROM [lekmer].[tProductSize] ps WHERE ps.[ErpId] = '212126-0099-120'
	
	EXEC [cdon].[pProductTryDecreaseNumberInStock] @ArticleNumber = '212126-0099-120', @QuantityToTakeFromStock = 10
	
	SELECT ps.[ProductId], ps.[ErpId], ps.[NumberInStock]
	FROM [lekmer].[tProductSize] ps WHERE ps.[ErpId] = '212126-0099-120'

ROLLBACK

BEGIN TRANSACTION

	--1038995 212126-0099-120 16
	
	SELECT ps.[ProductId], ps.[ErpId], ps.[NumberInStock]
	FROM [lekmer].[tProductSize] ps WHERE ps.[ErpId] = '212126-0099-120'
	
	EXEC [cdon].[pProductTryDecreaseNumberInStock] @ArticleNumber = '212126-0099-120', @QuantityToTakeFromStock = 20
	
	SELECT ps.[ProductId], ps.[ErpId], ps.[NumberInStock]
	FROM [lekmer].[tProductSize] ps WHERE ps.[ErpId] = '212126-0099-120'

ROLLBACK

BEGIN TRANSACTION

	--1039302 208065-0014-130 0
	
	SELECT ps.[ProductId], ps.[ErpId], ps.[NumberInStock]
	FROM [lekmer].[tProductSize] ps WHERE ps.[ErpId] = '208065-0014-130'
	
	EXEC [cdon].[pProductTryDecreaseNumberInStock] @ArticleNumber = '208065-0014-130', @QuantityToTakeFromStock = 10
	
	SELECT ps.[ProductId], ps.[ErpId], ps.[NumberInStock]
	FROM [lekmer].[tProductSize] ps WHERE ps.[ErpId] = '208065-0014-130'

ROLLBACK


BEGIN TRANSACTION

	-- 999999-9999-999 0
	
	EXEC [cdon].[pProductTryDecreaseNumberInStock] @ArticleNumber = '999999-9999-251', @QuantityToTakeFromStock = 10
	EXEC [cdon].[pProductTryDecreaseNumberInStock] @ArticleNumber = '999999-9999-999', @QuantityToTakeFromStock = 10

ROLLBACK