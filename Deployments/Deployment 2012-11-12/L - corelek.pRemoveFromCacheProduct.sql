USE [Lekmer]
GO
/****** Object:  StoredProcedure [corelek].[pRemoveFromCacheProduct]    Script Date: 11/12/2012 15:40:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [corelek].[pRemoveFromCacheProduct]
	@ProductId INT,
	@SizeId INT
AS
BEGIN
	SET NOCOUNT ON

	IF @SizeId IS NOT NULL
	BEGIN
		INSERT INTO [corelek].[tCacheUpdate] ( [ManagerName], [UpdateType], [Key], [CreationDate], [InsertionDate] )
		VALUES
		( 'ProductSizeKey_IProductSize', 1, 'ProductSize_' + CONVERT(VARCHAR(50), @ProductId) + '_' + CONVERT(VARCHAR(50), @SizeId), GETUTCDATE(), GETUTCDATE() ),
		( 'ProductSizeCollectionKey_Collection`1', 1, 'ProductSizeCollection_' + CONVERT(VARCHAR(50), @ProductId), GETUTCDATE(), GETUTCDATE() )
	END
	ELSE
	BEGIN
		INSERT INTO [corelek].[tCacheUpdate]
		SELECT 'ProductKey_IProduct', 1, CONVERT(VARCHAR(50), [ChannelId]) + '-' + CONVERT(VARCHAR(50), @ProductId), GETUTCDATE(), GETUTCDATE() FROM [core].[tChannel]
	END
	
	INSERT INTO [corelek].[tCacheUpdate]
	SELECT 'ProductKey_IProductView', 1, CONVERT(VARCHAR(50), [ChannelId]) + '-' + CONVERT(VARCHAR(50), @ProductId), GETUTCDATE(), GETUTCDATE() FROM [core].[tChannel]
END
