﻿SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [integration].[usp_UpdateProductPRICE]'
GO
ALTER PROCEDURE [integration].[usp_UpdateProductPRICE]
AS
BEGIN
	declare @Vat decimal
	SET @Vat = 25
	
	DECLARE 
		@HYChannelNL NVARCHAR(40),
		@ChannelIdNL INT
	
	SET @HYChannelNL = '005'
	SET @ChannelIdNL = 1000005
	
	begin try
	begin transaction
-- kalla sp som skapar prislista
		-- tProduct
		
		UPDATE
		--product.tPriceListItem 
			pli
		SET 
			PriceIncludingVat = (t.price/100),
			PriceExcludingVat = ((t.price/100) * ((100 - @Vat)/100)),
			VatPercentage = @Vat
		FROM 
			[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYarticleId, 5,17)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId
					   AND (pli.PriceListId = left(t.HYarticleId, 3) 
							OR (pli.PriceListId = @ChannelIdNL AND t.HYarticleId = @HYChannelNL)) --Dangerous!
			/*[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = t.HYarticleId		
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId*/	

		WHERE 		
			pli.PriceIncludingVat <> (t.price/100)
		
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
        /*delete pli -- funkar på update också
		from product.tPriceListItem pli
        inner join lekmer.tLekmerProduct ll on pli.ProductId = ll.ProductId*/
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProduct]'
GO

ALTER PROCEDURE [integration].[usp_ImportUpdateProduct]

AS
BEGIN
		--raiserror('errorMSG', 16, 1)
		-- tCategory proc kallas
		print 'Start tCategory'
		exec [integration].[usp_UpdateTCategory] -- skapar om artiklar om det behövs
		print 'End tCategory'
		print 'Start tUpdateBrand'
		exec [integration].usp_UpdateBrand
		print 'End tUpdateBrand'

		
		-- tProduct
		print 'Start update product'
		UPDATE 
			p
		SET 
			Title = tp.ArticleTitle, 
			--ItemsInPackage = null,
			EanCode = tp.EanCode, 
			--IsDeleted = 0, 
			--ColorId SAKNAS
			--SizeId SAKNAS
			NumberInStock = tp.NoInStock,
			-- Category kanske måste ändras om det skapar problem
			-- man ändrar den då till typ
			-- select catagoryId from tcategory where erpid = varukl+varugrupp+varukod 
			CategoryId = (select categoryId from product.tCategory where ErpId = 'C_'+tp.ArticleClassId+'-'+tp.ArticleGroupId+'-'+tp.ArticleCodeId)
			--CategoryId = tp.ArticleCodeId,	--varukodID det är den mest specifika!!
			--WebShopTitle = null,
			--[Description] = '',
			--ShortDescription = null,
			--MediaId = null, -- Måste sättas
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,17)	
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join [product].tProductRegistryProduct pli
					on pli.ProductId = lp.ProductId and
					pli.ProductRegistryId = substring(tp.HYarticleId, 3,1)
		WHERE 
			p.Title <> tp.ArticleTitle OR
			p.EanCode <> tp.EanCode OR
			p.NumberInStock <> tp.NoInStock
		 
		print 'End product update'
		
		---------------------------------------------
		print 'Start lekmer product update'
		exec integration.usp_ImportUpdateLekmerProduct
		print 'End lekmer product update'
	--------------------------------------		
		-- tPriceListItem - Update Prices from integration.tempProductPrice
		print 'Start price update'
		exec [integration].[usp_UpdateProductPRICE]
		print 'End price update'
	--------------------------------------
	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@EanCode nvarchar(50),
				@NoInStock nvarchar(250),
				@ArticleTitle nvarchar(250),
				@ProductId int,
				@HYBrandId nvarchar(25),
				
				@IsBookable bit,
				@ExpectedBackInStock datetime,
				@IsDeleted bit,
				@CategoryId int,
				@Description nvarchar(250),
				@Price decimal,
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10),
				
				@Data nvarchar(4000),
				@Vat decimal
				SET @Vat = 25
				SET @IsDeleted = 0
				SET @Description = ''
				SET @ExpectedBackInStock = null
				SET @IsBookable = 0
		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				------------------------------------
		
		declare cur_product cursor fast_forward for
			select
				tp.HYarticleId,
				tp.EanCode,
				tp.NoInStock,
				tp.ArticleTitle,
				(tp.Price/100),
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId,
				tp.BrandId
			from
				[integration].tempProduct tp
			where not exists(select 1
						-- parar lekmerErpId med temp tabell erpId (klipper fok) sen parar
						-- tProductregistry på lekmerProductId på det ErpId sen parar också foken med productRegProduct
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = substring(tp.HYarticleId, 5,17) and
							lp.ProductId = prp.ProductId and
							substring(tp.HYarticleId, 3,1) = prp.ProductRegistryId)
			
			
			
		--print 'Open Cursor'
		open cur_product
		fetch next from cur_product
			into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1) -- Fok här är nu 001 eller 002 osv beroende på vilket land artikeln tillhör
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)			
				
				
				-- if, kolla om eprid utan fok finns, om det gör hämta dens produktid och lägg in de produkt idet + fok
				-- i tproductRegistryProduct	
				
				if not exists(select 1 -- om erpid inte finns i tlekmerProduct finns det inte i nån fok, lägg in den då, annars lägg bara in i pReg
							from lekmer.tLekmerProduct lp
							where lp.HYErpId = @HYarticleIdNoFoK)
				begin	
					print 'insert into product.tproduct'
					INSERT INTO -- MediaId saknas
						[product].tProduct(ErpId, EanCode, IsDeleted, NumberInStock, CategoryId, Title, [Description], ProductStatusId)
					SELECT 
						@HYarticleIdNoFoK, -- @HYArticleId
						@EanCode, 
						@IsDeleted, --0
						@NoInStock, 
						(SELECT CategoryId FROM product.tCategory WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId), -- koll här
						@ArticleTitle, 
						@Description, --''
						0 --case when @NoInStock > 0 then 0 else 1 end (Ändrat alla produkter sätts som offline när de sätts in)
												
					set @ProductId = SCOPE_IDENTITY()
					
					-- Data that was sent into insert
					SET @Data = 'NEW: HYArticleId ' + @HYarticleId + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId as varchar(10))
		
					--IF @nrInStock > 0 -- om antal är mer än 0
					--begin
						--SET @IsBookable = 0
					--end
						
					print 'insert into lekmer.tlekmerProduct'		
					insert into lekmer.tLekmerProduct(ProductId, HYErpId, BrandId, IsBookable, AgeFromMonth, AgeToMonth, 
								IsNewFrom, IsNewTo, IsBatteryIncluded, ExpectedBackInStock, ShowVariantRelations, [ProductTypeId])
					values(
					@ProductId, 
					@HYarticleIdNoFoK, --@HYarticleId 
					(Select b.BrandId from lekmer.tBrand b where b.ErpId = @HYBrandId),
					@IsBookable, 
					0, 
					0, 
					Convert(varchar, GETDATE(), 112),
					DATEADD(WK, 1, Convert(varchar, GETDATE(), 112)), 
					0, 
					@ExpectedBackInStock,
					0, -- ShowVariantRelations
					1 -- ProductTypeId
					)
					print 'insert into product.tPriceListItem'
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, @ProductId, @Price, (@Price * ((100-@Vat)/100)), @Vat)
					print 'insert into product.tProductReg'		
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct (ProductId, ProductRegistryId)
					values(@ProductId, @Fok) -- Fok är färsäljningskanalen
		
					--commit
				end	
				else if not exists (select 1
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = @HYarticleIdNoFoK
							and lp.ProductId = prp.ProductId 
							and prp.ProductRegistryId = @Fok)
				begin
					select @ProductId = ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFoK
					set @Data = 'EXISTING: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					print 'insert into product.tproductRegProg (in else)'	
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct (ProductId, ProductRegistryId)
					values(@ProductId, @Fok)
					print 'insert into product.rPriceList (in else)'	
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFoK),
					@Price, (@Price * ((100-@Vat)/100)), @Vat)
					
					--SET @Data = 'HYArticleId ' + @HYarticleId + 'EanCode ' + @EanCode + 'CategoryId ' +cast(@CategoryId as nvarchar)
				end
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_product into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		end
		
		close cur_product
		deallocate cur_product
		


END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_UpdateProductPRICELekmer]'
GO

ALTER PROCEDURE [integration].[usp_UpdateProductPRICELekmer]

AS
BEGIN
	
	if object_id('tempdb..#tProductPriceIncludingVatLess') is not null
		drop table #tProductPriceIncludingVatLess
		
	if object_id('tempdb..#tProductPriceIncludingVatMore') is not null
		drop table #tProductPriceIncludingVatMore
		
	create table #tProductPriceIncludingVatLess
	(
		ProductId int not null,
		PriceListId int not null,
		PriceIncludingVat decimal(16,2) not null,
		PriceExcludingVat decimal(16,2) not null,
		VatPercentage decimal(16,2) not null
		constraint PK_#tProductPriceIncludingVatLess primary key(ProductId, PricelistId)
	)
	
	create table #tProductPriceIncludingVatMore
	(
		ProductId int not null,
		PriceListId int not null,
		PriceIncludingVat decimal(16,2) not null,
		PriceExcludingVat decimal(16,2) not null,
		VatPercentage decimal(16,2) not null
		constraint PK_#tProductPriceIncludingVatMore primary key(ProductId, PricelistId)
	)
	
	begin try
	begin transaction
		
	declare @Vat decimal
		set @Vat = 25.0	
		
		insert #tProductPriceIncludingVatLess(ProductId, PriceListId, PriceIncludingVat,
		PriceExcludingVat, VatPercentage)		
		select distinct
			pli.productid,
			pli.pricelistid,
			cast(t.price/100.0 as decimal(16,2)), -- PriceIncludingVat
			cast(((t.price/100.0) / (1.0+(25.0/100.0))) as decimal(16,2)), -- PRiceExclusingvat
			@Vat
			--select *
		from 
		[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYarticleId, 5,12)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId and
					pli.PriceListId = case left(t.HYarticleId, 3)
										when '001' then 1
										when '002' then 2
										when '003' then 3
										when '004' then 4
										when '005' then 1000005 
									  end
		WHERE
			pli.PriceIncludingVat <> cast(t.price/100.0 as decimal(16,2))
			and cast(t.price/100.0 as decimal(16,2)) < pli.PriceIncludingVat
			
		
		-- UPDATE 
		Update
			ppli
		set
			PriceExcludingVat = t.PriceExcludingVat
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatLess t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid

			
		-- UPDATE  (REAL)
		Update
			ppli
		set
			ppli.PriceIncludingVat = t.PriceIncludingVat,
			--ppli.PriceExcludingVat = t.PriceExcludingVat, 
			ppli.VatPercentage = t.VatPercentage
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatLess t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid
					
		
		
		--- DEL 2  om nya priser är mer än nuvarande priset
		insert #tProductPriceIncludingVatMore(ProductId, PriceListId, PriceIncludingVat,
		PriceExcludingVat, VatPercentage)		
		select distinct
			pli.productid,
			pli.pricelistid,
			cast(t.price/100.0 as decimal(16,2)), -- PriceIncludingVat
			cast(((t.price/100.0) / (1.0+(25.0/100.0))) as decimal(16,2)), -- PRiceExclusingvat
			@Vat 
		from 
		[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYarticleId, 5,12)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId and
					pli.PriceListId = case left(t.HYarticleId, 3)
										when '001' then 1
										when '002' then 2
										when '003' then 3
										when '004' then 4
										when '005' then 1000005
									  end
		WHERE
			pli.PriceIncludingVat <> cast(t.price/100.0 as decimal(16,2))
			and cast(t.price/100.0 as decimal(16,2)) > pli.PriceIncludingVat	
			
		
		-- UPDATE 
		Update
			ppli
		set
			ppli.PriceIncludingVat = t.PriceIncludingVat
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatMore t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid

			
		-- UPDATE  (REAL)
		Update
			ppli
		set
			ppli.PriceExcludingVat = t.PriceExcludingVat, 
			ppli.VatPercentage = t.VatPercentage
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatMore t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid
		/*
		declare @Vat decimal
		set @Vat = 25.0
		UPDATE
			pli
		SET 
			PriceIncludingVat = (t.price/100.0),
			PriceExcludingVat = ((t.price/100.0) / (1.0+(@Vat/100.0))), --case för moms HYarticleid
														-- lägg till en case sats som kollar i vilken
														-- fok det rör sig om
			VatPercentage = @Vat
		FROM 
			[integration].tempProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYarticleId, 5,17)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId and
					pli.PriceListId = case left(t.HYarticleId, 3)
										when '001' then 1
										when '002' then 2
										when '003' then 3
										when '004' then 4
										when '005' then 1000005
									  end

		WHERE 		
			pli.PriceIncludingVat <> (t.price/100.0)
		*/
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
        /*delete pli -- funkar på update också
		from product.tPriceListItem pli
        inner join lekmer.tLekmerProduct ll on pli.ProductId = ll.ProductId*/
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductLekmer]'
GO
ALTER PROCEDURE [integration].[usp_ImportUpdateProductLekmer]
AS
BEGIN
	SET NOCOUNT ON 

	-- Delte all ***Missing*** (tmp)
	DELETE FROM integration.tempProduct WHERE ArticleCodeTitle= '***Missing***'

	-- tCategory
	EXEC [integration].[usp_UpdateTCategoryLekmer] -- creates the items if necessary
	EXEC [integration].[usp_UpdateCategoryTitleLekmer] -- update titles if necessary
	
	-- tBrand
	EXEC [integration].[usp_UpdateBrandLekmer]
	EXEC [integration].[usp_UpdateBrandLekmerCreationDate] -- adds the date the brand was created
	
	-- tProduct
	EXEC [integration].[usp_UpdateProductLekmer]
	EXEC [integration].[usp_ImportUpdateLekmerProduct]
	
	-- tPriceListItem
	EXEC [integration].[usp_UpdateProductPRICELekmer]
	
	-- tProductSize
	EXEC [integration].[usp_UpdateSizeLekmer]

	--------------------------------------

	DECLARE 
		@HYArticleNoFull NVARCHAR(50),
		@HYArticleNoColorSize NVARCHAR(50),
		@HYArticleNoColor NVARCHAR(50),
		@HYSizeId NVARCHAR(50),
		@EanCode NVARCHAR(50),
		@NoInStock NVARCHAR(250),
		@ArticleTitle NVARCHAR(250),
		@ProductId INT,
		@HYBrandId NVARCHAR(25),
		@BrandId INT,
		@HYSizeValue NVARCHAR(50),
		@Weight NVARCHAR(50),
		
		@CategoryId INT,
		@Price DECIMAL,
		@VaruklassId NVARCHAR(10),
		@VarugruppId NVARCHAR(10),
		@VarukodId NVARCHAR(10),
		@LekmerErpId NVARCHAR(25),
		
		@Data NVARCHAR(4000),
		@Vat DECIMAL,
		
		@NewFromDate DATETIME,
		@NewToDate DATETIME,

		-- Variables for Channel
		@HYChannel NVARCHAR(40),
		
		@TradeDoublerProductGroup NVARCHAR(50),
		@ChannelId INT,
		@IsProductRegistry BIT,
		@NeedToInsertProductRelationData BIT,
		
		@HYChannelNL NVARCHAR(40),
		@ChannelIdNL INT
	
	SET @Vat = 25.0
	
	SET @NewFromDate = CONVERT(DATE, GETDATE())
	SET @NewToDate   = DATEADD(WEEK, 4, @NewFromDate)
	
	SET @HYChannelNL = '005'
	SET @ChannelIdNL = 1000005	

	------------------------------------

	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT
			tp.HYarticleId,
			tp.ArticleTitle,
			tp.SizeId,
			(tp.Price/100),
			tp.NoInStock,
			tp.ArticleGroupId,
			tp.ArticleClassId,
			tp.ArticleCodeId,
			tp.BrandId,
			tp.EanCode,
			tp.LekmerArtNo,
			[tp].[Weight]
		FROM
			[integration].tempProduct tp
		--WHERE NOT EXISTS (SELECT 1
		--				  FROM lekmer.tLekmerProduct lp, product.tPriceListItem prp
		--				  WHERE lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
		--					AND lp.ProductId = prp.ProductId
		--					AND SUBSTRING(tp.HYarticleId, 3,1) = prp.PriceListId)

	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO
			@HYArticleNoFull,
			@ArticleTitle,
			@HYSizeValue,
			@Price,
			@NoInStock,
			@VarugruppId,
			@VaruklassId,
			@VarukodId,
			@HYBrandId,
			@EanCode,
			@LekmerErpId,
			@Weight

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY
			SET @IsProductRegistry = 0
			SET @NeedToInsertProductRelationData = 0
			
			-- Split @HYArticleNoFull
			-- 001-0000001-1017-108
			SET @HYChannel = SUBSTRING(@HYArticleNoFull, 1, 3)              -- [001]-0000001-1017-108
			SET @HYArticleNoColorSize = SUBSTRING(@HYArticleNoFull, 5, 17)  -- 001-[0000001-1017-108]
			SET @HYSizeId = SUBSTRING(@HYArticleNoFull, 18, 3)              -- 001-0000001-1017-[108]
			SET @HYArticleNoColor = SUBSTRING(@HYArticleNoFull, 5, 12)      -- 001-[0000001-1017]-108
		
			SET @ProductId = (SELECT ProductId FROM lekmer.tLekmerProduct WHERE HYErpId = @HYArticleNoColor)
			IF @ProductId IS NOT NULL
			BEGIN
				IF EXISTS (SELECT 1 FROM product.tProductRegistryProduct
						   WHERE ProductId = @ProductId
								 AND (ProductRegistryId = @HYChannel 
									  OR ([ProductRegistryId] = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!
				BEGIN
					SET @IsProductRegistry = 1
					GOTO PRODUCT_END
				END
			END
			
			PRODUCT_START:
			
			---- Skip processing the same @HYErpId + @Channel few times
			--IF @PrevHYErpId = @HYErpId AND @PrevChannel = @Channel
			--BEGIN
			--	GOTO PRODUCT_END
			--END ELSE BEGIN
			--	SET	@PrevHYErpId = @HYErpId
			--	SET	@PrevChannel = @Channel
			--END
			
			BEGIN TRANSACTION
			
			SET @CategoryId = (SELECT CategoryId 
							   FROM product.tCategory 
							   WHERE ErpId = 'C_' + ISNULL(@VaruklassId, '') + '-' + ISNULL(@VarugruppId, '') + '-' + ISNULL(@VarukodId, ''))
			
			SET @BrandId = (SELECT BrandId FROM lekmer.tBrand WHERE ErpId = @HYBrandId)

			IF @ProductId IS NULL
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNoFull + ' EanCode ' + @EanCode
				
				-- tProduct
				INSERT INTO [product].tProduct (
					ErpId,
					EanCode,
					IsDeleted,
					NumberInStock,
					CategoryId,
					Title,
					[Description],
					ProductStatusId
				)
				VALUES (
					@HYArticleNoColor,
					@EanCode,
					0, -- IsDeleted
					@NoInStock,
					@CategoryId,
					@ArticleTitle, 
					'', -- Description
					1 -- Offline
				)
											
				SET @ProductId = SCOPE_IDENTITY()
				
				SET @Data = 'NEW: HYArticleId ' + @HYArticleNoFull + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId AS VARCHAR(10))
	
				-- tLekmerProduct
				INSERT INTO lekmer.tLekmerProduct (
					ProductId,
					HYErpId,
					BrandId,
					IsBookable,
					AgeFromMonth,
					AgeToMonth,
					IsNewFrom,
					IsNewTo,
					IsBatteryIncluded,
					ExpectedBackInStock,
					LekmerErpId,
					ShowVariantRelations,
					[Weight],
					[ProductTypeId]
				)
				VALUES (
					@ProductId, 
					@HYArticleNoColor,
					@BrandId,
					0, -- IsBookable, 
					0, -- AgeFromMonth
					0, -- AgeToMonth
					@NewFromDate,
					@NewToDate,
					0, -- IsBatteryIncluded
					NULL, --ExpectedBackInStock
					@LekmerErpId,
					1, -- ShowVariantRelations
					CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), @Weight) / 1000),
					1
				)

			END	 -- Product already exist in tLekmerProduct
			ELSE IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem prp 
								WHERE prp.ProductId = @ProductId
									  AND (prp.PriceListId = @HYChannel 
										   OR (prp.PriceListId = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!(PriceListId)
			BEGIN
				SET @NeedToInsertProductRelationData = 1
				
				SET @Data = 'EXISTING: @HYArticleNoColorSize ' + @HYArticleNoColorSize + ' @HYChannel ' + @HYChannel + ' ProductId' + CAST(@ProductId as varchar(10))
			END
			
			IF @NeedToInsertProductRelationData = 1
			BEGIN
				------------------------------------------------------------------------------------------
				-- tProductRegistryProduct
				
				IF NOT EXISTS (SELECT 1 FROM product.tProductRegistryProduct
							   WHERE ProductId = @ProductId
									 AND (ProductRegistryId = @HYChannel 
										  OR ([ProductRegistryId] = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!
				BEGIN
					INSERT INTO product.tProductRegistryProduct (
						ProductId,
						ProductRegistryId
					)
					VALUES (
						@ProductId,
						CASE 
							WHEN @HYChannel = @HYChannelNL THEN @ChannelIdNL
							ELSE @HYChannel
						END
					)
				END				
				
				------------------------------------------------------------------------------------------
				-- tPriceListItem
				-- PriceListId and @HYChannel have same values

				IF NOT EXISTS (SELECT 1 FROM product.tPriceListItem
							   WHERE ProductId = @ProductId
									 AND (PriceListId = @HYChannel 
									      OR (PriceListId = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!(PriceListId)
				BEGIN
					INSERT INTO product.tPriceListItem (
						PriceListId,
						ProductId,
						PriceIncludingVat,
						PriceExcludingVat,
						VatPercentage
					)
					VALUES (
						CASE 
							WHEN @HYChannel = @HYChannelNL THEN @ChannelIdNL
							ELSE @HYChannel
						END,
						@ProductId, 
						@Price, 
						@Price / (1.0+@Vat/100.0),
						@Vat
					)
				END

				------------------------------------------------------------------------------------------ 
				-- tTradeDoublerProductGroupMapping

				SET @ChannelId = CAST(@HYChannel AS INT)
				SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId
												 FROM integration.tTradeDoublerProductGroupMapping
												 WHERE HYArticleClassId = 'C_' + ISNULL(@VaruklassId, '')
													   AND (ChannelId = @ChannelId 
															OR (ChannelId = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!

				IF @TradeDoublerProductGroup IS NOT NULL AND @ChannelId > 0
				BEGIN
					IF NOT EXISTS (SELECT 1 FROM lekmer.tTradeDoublerProductGroupMapping tTDPGM
								   WHERE tTDPGM.ProductId = @ProductId
										 AND (tTDPGM.ChannelId = @ChannelId
											  OR (tTDPGM.ChannelId = @ChannelIdNL AND @HYChannel = @HYChannelNL))) --Dangerous!
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping (
							ProductId,
							ProductGroupId,
							ChannelId
						)
						VALUES (
							@ProductId,
							@TradeDoublerProductGroup,
							CASE 
								WHEN @HYChannel = @HYChannelNL THEN @ChannelIdNL
								ELSE @ChannelId
							END
						)
					END
				END 
			END

			COMMIT

			PRODUCT_END:
			
			------------------------------------------------------------------------------------------ 
			-- ProductSize
				
			IF @ProductId IS NOT NULL -- Must be TRUE always
			EXEC [integration].[usp_ImportUpdateProductSizesLekmer]
				@HYChannel,
				@ProductId,
				@HYArticleNoColorSize,
				@HYSizeId,
				@HYSizeValue,
				@NoInStock,
				@Weight
			
		END TRY
		
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			
			INSERT INTO [integration].[integrationLog] (
				Data,
				[Message],
				[Date],
				OcuredInProcedure
			)
			VALUES (
				@Data,
				ERROR_MESSAGE(),
				GETDATE(),
				ERROR_PROCEDURE()
			)
				
			-- Reset identity column counter
			DBCC CHECKIDENT ('product.tProduct', reseed, 0)
			DBCC CHECKIDENT ('product.tProduct', reseed)

		END CATCH

		FETCH NEXT FROM cur_product 
			INTO 
				@HYArticleNoFull,
				@ArticleTitle,
				@HYSizeValue,
				@Price,
				@NoInStock,
				@VarugruppId,
				@VaruklassId,
				@VarukodId,
				@HYBrandId,
				@EanCode,
				@LekmerErpId,
				@Weight

	END
	
	CLOSE cur_product
	DEALLOCATE cur_product
	
	EXEC [integration].[usp_UpdateProductBrandLekmer]
	--exec [integration].[usp_FokRestrictionsProducts] -- Outdated, soon to be removed, [usp_ChannelRestrictionsProducts] in its place
	EXEC [integration].[usp_ChannelRestrictionsProducts]
	EXEC [integration].[usp_REATagProduct]
	
	-- Create 'Variant' Product Relation 
	EXEC [integration].[pCreateProductRelationVariant]
	
	-- Track products chnages to eSales export
	EXEC [integration].[pTrackProductChangesLekmer]
	
	EXEC [productlek].[pPackageUpdate]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[usp_ImportUpdateProductHeppo]'
GO

ALTER PROCEDURE [integration].[usp_ImportUpdateProductHeppo]

AS
BEGIN
		--raiserror('errorMSG', 16, 1)
		-- tCategory proc kallas
		print 'Start tCategory'
		exec [integration].[usp_UpdateTCategoryHeppo] -- skapar om artiklar om det behövs
		print 'End tCategory'
		print 'Start tUpdateBrand'
		exec [integration].usp_UpdateBrandHeppo
		print 'End tUpdateBrand'

		
		-- tProduct
		print 'Start update product'
		UPDATE 
			p
		SET 
			Title = tp.ArticleTitle, 
			--ItemsInPackage = null,
			EanCode = tp.EanCode, 
			--IsDeleted = 0, 
			--ColorId SAKNAS
			--SizeId SAKNAS
			NumberInStock = tp.NoInStock,
			-- Category kanske måste ändras om det skapar problem
			-- man ändrar den då till typ
			-- select catagoryId from tcategory where erpid = varukl+varugrupp+varukod 
			CategoryId = (select categoryId from product.tCategory where ErpId = 'C_'+tp.ArticleClassId+'-'+tp.ArticleGroupId+'-'+tp.ArticleCodeId)
			--CategoryId = tp.ArticleCodeId,	--varukodID det är den mest specifika!!
			--WebShopTitle = null,
			--[Description] = '',
			--ShortDescription = null,
			--MediaId = null, -- Måste sättas
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,11)	-- 5,17 innan
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join [product].tProductRegistryProduct pli
					on pli.ProductId = lp.ProductId and
					pli.ProductRegistryId = substring(tp.HYarticleId, 3,1)
		WHERE 
			p.Title <> tp.ArticleTitle OR
			p.EanCode <> tp.EanCode OR
			p.NumberInStock <> tp.NoInStock
		 
		print 'End product update'
		
		---------------------------------------------
		--print 'Start lekmer product update'
		--exec integration.usp_ImportUpdateLekmerProduct
		--print 'End lekmer product update'
	--------------------------------------		
		-- tPriceListItem - Update Prices from integration.tempProductPrice
		print 'Start price update'
		exec [integration].[usp_UpdateProductPRICE]
		print 'End price update'
	--------------------------------------
	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@HYarticleIdNoFokNoSize nvarchar (50),
				@HYarticleSize nvarchar(50),
				@EanCode nvarchar(50),
				@NoInStock nvarchar(250),
				@ArticleTitle nvarchar(250),
				@ProductId int,
				@HYBrandId nvarchar(25),
				
				@IsBookable bit,
				@ExpectedBackInStock datetime,
				@IsDeleted bit,
				@CategoryId int,
				@Description nvarchar(250),
				@Price decimal,
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10),
				
				@Data nvarchar(4000),
				@Vat decimal
				SET @Vat = 25
				SET @IsDeleted = 0
				SET @Description = ''
				SET @ExpectedBackInStock = null
				SET @IsBookable = 0
		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				------------------------------------
		
		declare cur_product cursor fast_forward for
			select
				tp.HYarticleId,
				tp.EanCode,
				tp.NoInStock,
				tp.ArticleTitle,
				(tp.Price/100),
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId,
				tp.BrandId
			from
				[integration].tempProduct tp
			where not exists(select 1
						-- parar lekmerErpId med temp tabell erpId (klipper fok) sen parar
						-- tProductregistry på lekmerProductId på det ErpId sen parar också foken med productRegProduct
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = substring(tp.HYarticleId, 5,11) and
							lp.ProductId = prp.ProductId and
							substring(tp.HYarticleId, 3,1) = prp.ProductRegistryId)
			
			
			
		--print 'Open Cursor'
		open cur_product
		fetch next from cur_product
			into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1) -- Fok här är nu 001 eller 002 osv beroende på vilket land artikeln tillhör
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)			
				SET @HYarticleSize = substring(@HYarticleId, 17,3)
				SET @HYarticleIdNoFokNoSize = substring(@HYarticleId, 5,11)
				
				-- if, kolla om eprid utan fok finns, om det gör hämta dens produktid och lägg in de produkt idet + fok
				-- i tproductRegistryProduct	
				
				if not exists(select 1 -- om erpid inte finns i tlekmerProduct finns det inte i nån fok, lägg in den då, annars lägg bara in i pReg
							from lekmer.tLekmerProduct lp
							where lp.HYErpId = @HYarticleIdNoFokNoSize)
				begin	
					print 'insert into product.tproduct'
					INSERT INTO -- MediaId saknas
						[product].tProduct(ErpId, EanCode, IsDeleted, NumberInStock, CategoryId, Title, [Description], ProductStatusId)
					SELECT 
						@HYarticleIdNoFokNoSize, -- @HYArticleId
						@EanCode, 
						@IsDeleted, --0
						@NoInStock, 
						(SELECT CategoryId FROM product.tCategory WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId), -- koll här
						@ArticleTitle, 
						@Description, --''
						0 --case when @NoInStock > 0 then 0 else 1 end (Ändrat alla produkter sätts som offline när de sätts in)
												
					set @ProductId = SCOPE_IDENTITY()
					
					-- Data that was sent into insert
					SET @Data = 'NEW: HYArticleId ' + @HYarticleId + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId as varchar(10))
		
					--IF @nrInStock > 0 -- om antal är mer än 0
					--begin
						--SET @IsBookable = 0
					--end
						
					print 'insert into lekmer.tlekmerProduct'		
					insert into lekmer.tLekmerProduct(ProductId, HYErpId, BrandId, IsBookable, AgeFromMonth, AgeToMonth, 
								IsNewFrom, IsNewTo, IsBatteryIncluded, ExpectedBackInStock, ShowVariantRelations, ProductTypeId)
					values(
					@ProductId, 
					@HYarticleIdNoFokNoSize,
					(Select b.BrandId from lekmer.tBrand b where b.ErpId = @HYBrandId),
					@IsBookable, 
					0, 
					0, 
					Convert(varchar, GETDATE(), 112),
					DATEADD(WK, 1, Convert(varchar, GETDATE(), 112)), 
					0, 
					@ExpectedBackInStock,
					0, -- ShowVariantRelations
					1 -- ProductTypeId
					)
					print 'insert into product.tPriceListItem'
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, @ProductId, @Price, (@Price * ((100-@Vat)/100)), @Vat)
					print 'insert into product.tProductReg'		
					-- tProductRegistryProduct
					EXEC [lekmer].[pProductRegistryProductInsert]  @ProductId, NULL,NULL, @Fok, ','
					
				
					--commit
				end	
				else if not exists (select 1
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = @HYarticleIdNoFokNoSize -- @HYarticleNoFok innan
							and lp.ProductId = prp.ProductId 
							and prp.ProductRegistryId = @Fok)
				begin
					select @ProductId = ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize --@HYarticleIdNoFoK innan
					set @Data = 'EXISTING: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					print 'insert into product.tproductRegProg (in else)'	
					-- tProductRegistryProduct
					EXEC [lekmer].[pProductRegistryProductInsert]  @ProductId, NULL,NULL, @Fok, ','
					
					print 'insert into product.rPriceList (in else)'	
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize), --@HYarticleIdNoFoK innan),
					@Price, (@Price * ((100-@Vat)/100)), @Vat)
					
				end
				
				
				-- placeringen funkar inte, kommer bara gå in hit om produkten inte är
				-- i alla 4 foks...
				-- om den specifika sizen av produkten inte finns i tProductSize
				/*
				if not exists (select 1
						from lekmer.tProductSize ps
						where ps.ErpId = @HYarticleIdNoFoK)
				begin
				set @Data = 'NEW tProductSIZE: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						@ProductId,
						1, --(select SizeId from lekmer.tSize where ErpId = @HYarticleSize),
						@HYarticleIdNoFoK,
						@NoInStock
					
				end
			*/
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_product into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		end
		
		close cur_product
		deallocate cur_product
		

		-- hantera storlekarna på bilderna
		print '[integration].[usp_ImportUpdateProductSizesHeppo]  körs'
		exec [integration].[usp_ImportUpdateProductSizesHeppo]
				
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pProductStockReductionCdon]'
GO
ALTER PROCEDURE [integration].[pProductStockReductionCdon]
	@ProductId INT
AS
BEGIN			
	BEGIN TRY
		BEGIN TRANSACTION
			IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId)
			BEGIN
				EXEC [lekmer].[pProductDecreaseNumberInStock] @ProductId, 1
			END
		COMMIT		
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
