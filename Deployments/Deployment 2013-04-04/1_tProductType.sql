/****** Object:  Table [productlek].[tProductType]    Script Date: 04/04/2013 00:49:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [productlek].[tProductType](
	[ProductTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[CommonName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tProductType] PRIMARY KEY CLUSTERED 
(
	[ProductTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [productlek].[tProductType] ON
INSERT [productlek].[tProductType] ([ProductTypeId], [Title], [CommonName]) VALUES (1, N'Product', N'product')
INSERT [productlek].[tProductType] ([ProductTypeId], [Title], [CommonName]) VALUES (2, N'Package', N'package')
SET IDENTITY_INSERT [productlek].[tProductType] OFF