SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [orderlek].[tPackageOrderItemProduct]'
GO
CREATE TABLE [orderlek].[tPackageOrderItemProduct]
(
[PackageOrderItemId] [int] NOT NULL,
[OrderItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[EanCode] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[SizeId] [int] NULL,
[SizeErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPackageOrderItemProduct] on [orderlek].[tPackageOrderItemProduct]'
GO
ALTER TABLE [orderlek].[tPackageOrderItemProduct] ADD CONSTRAINT [PK_tPackageOrderItemProduct] PRIMARY KEY CLUSTERED  ([PackageOrderItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tPackage]'
GO
CREATE TABLE [productlek].[tPackage]
(
[PackageId] [int] NOT NULL IDENTITY(1, 1),
[MasterProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPackage] on [productlek].[tPackage]'
GO
ALTER TABLE [productlek].[tPackage] ADD CONSTRAINT [PK_tPackage] PRIMARY KEY CLUSTERED  ([PackageId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tCartItemPackageElement]'
GO
CREATE TABLE [orderlek].[tCartItemPackageElement]
(
[CartItemPackageElementId] [int] NOT NULL IDENTITY(1, 1),
[CartItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPackageElement] on [orderlek].[tCartItemPackageElement]'
GO
ALTER TABLE [orderlek].[tCartItemPackageElement] ADD CONSTRAINT [PK_tCartItemPackageElement] PRIMARY KEY CLUSTERED  ([CartItemPackageElementId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tBlockProductType]'
GO
CREATE TABLE [productlek].[tBlockProductType]
(
[BlockId] [int] NOT NULL,
[ProductTypeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockProductType] on [productlek].[tBlockProductType]'
GO
ALTER TABLE [productlek].[tBlockProductType] ADD CONSTRAINT [PK_tBlockProductType] PRIMARY KEY CLUSTERED  ([BlockId], [ProductTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tBlockPackageProductList]'
GO
CREATE TABLE [productlek].[tBlockPackageProductList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockPackageProductList] on [productlek].[tBlockPackageProductList]'
GO
ALTER TABLE [productlek].[tBlockPackageProductList] ADD CONSTRAINT [PK_tBlockPackageProductList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[tPackageOrderItem]'
GO
CREATE TABLE [orderlek].[tPackageOrderItem]
(
[PackageOrderItemId] [int] NOT NULL IDENTITY(1, 1),
[OrderItemId] [int] NOT NULL,
[OrderId] [int] NOT NULL,
[Quantity] [int] NOT NULL,
[PackagePriceIncludingVat] [decimal] (16, 2) NOT NULL,
[ActualPriceIncludingVat] [decimal] (16, 2) NOT NULL,
[OriginalPriceIncludingVat] [decimal] (16, 2) NOT NULL,
[VAT] [decimal] (16, 2) NOT NULL,
[OrderItemStatusId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPackageOrderItem] on [orderlek].[tPackageOrderItem]'
GO
ALTER TABLE [orderlek].[tPackageOrderItem] ADD CONSTRAINT [PK_tPackageOrderItem] PRIMARY KEY CLUSTERED  ([PackageOrderItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tProductActionTargetProductType]'
GO
CREATE TABLE [campaignlek].[tProductActionTargetProductType]
(
[ProductActionId] [int] NOT NULL,
[ProductTypeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tProductActionTargetProductType] on [campaignlek].[tProductActionTargetProductType]'
GO
ALTER TABLE [campaignlek].[tProductActionTargetProductType] ADD CONSTRAINT [PK_tProductActionTargetProductType] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tPackageProduct]'
GO
CREATE TABLE [productlek].[tPackageProduct]
(
[PackageId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tPackageProduct] on [productlek].[tPackageProduct]'
GO
ALTER TABLE [productlek].[tPackageProduct] ADD CONSTRAINT [PK_tPackageProduct] PRIMARY KEY CLUSTERED  ([PackageId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageDecreaseNumberInStockByProduct]'
GO
CREATE PROCEDURE [productlek].[pPackageDecreaseNumberInStockByProduct]
	@ProductId	INT,
	@NewNumberInStock INT
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] <= @NewNumberInStock THEN [NumberInStock]
								ELSE @NewNumberInStock
						   END)
	WHERE
		[ProductId] IN (SELECT DISTINCT [pak].[MasterProductId]
						FROM [productlek].[tPackage] pak
							 INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
						WHERE [pp].[ProductId] = @ProductId)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vBlockPackageProductList]'
GO
CREATE VIEW [productlek].[vBlockPackageProductList]
AS
	SELECT
		[BlockId] AS 'BlockPackageProductList.BlockId',
		[ColumnCount] AS 'BlockPackageProductList.ColumnCount',
		[RowCount] AS 'BlockPackageProductList.RowCount'
	FROM
		[productlek].[tBlockPackageProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductTypeSaveByBlock]'
GO

CREATE PROCEDURE [productlek].[pProductTypeSaveByBlock]
	@BlockId INT,
	@ProductTypeIds VARCHAR(MAX),
	@Delimiter CHAR(1)
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@ProductTypeIds, @Delimiter)
	
	DELETE [productlek].[tBlockProductType]
	WHERE
		[BlockId] = @BlockId
	
	INSERT [productlek].[tBlockProductType] ( [BlockId], [ProductTypeId] )
	SELECT
		@BlockId,
		Id
	FROM
		@IdList
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCartItemPackageElementDeleteByCartItem]'
GO

CREATE PROCEDURE [orderlek].[pCartItemPackageElementDeleteByCartItem]
	@CartItemId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[orderlek].[tCartItemPackageElement]
	WHERE
		[CartItemId] = @CartItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductGetAllByPackageSecure]'
GO
CREATE PROCEDURE [productlek].[pProductGetAllByPackageSecure]
	@ChannelId		INT,
	@PackageId		INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @CurrencyId INT, @ProductRegistryId INT

	SELECT @CurrencyId = [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId
	SELECT @ProductRegistryId = [ProductRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId

	SELECT
		[p].*,
		[pli].*
	FROM
		[product].[vCustomProductSecure] p
		INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[ProductId] = [p].[Product.Id]
		LEFT JOIN [product].[tProductRegistryProduct] prp ON [prp].[ProductId] = [p].[Product.Id] AND [prp].[ProductRegistryId] = @ProductRegistryId
		LEFT JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [prp].[ProductRegistryId] AND [pmc].[ChannelId] = @ChannelId
		LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[Product.Id]
														  AND [pli].[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
																							@CurrencyId,
																							[p].[Product.Id],
																							[pmc].[PriceListRegistryId],
																							NULL)
	WHERE
		[pp].[PackageId] = @PackageId
		AND [p].[Product.IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageWithSizesGetNumberInStock]'
GO
CREATE PROCEDURE [productlek].[pPackageWithSizesGetNumberInStock]
	@ItemsWithoutSizes		VARCHAR(max),
	@ItemsWithSizes			XML,
	@Delimiter				CHAR(1)
AS 
BEGIN
	SET NOCOUNT ON
	DECLARE @ItemsWithoutSizesNumInStock INT
	DECLARE @ItemsWithSizesNumInStock INT

	-- products without sizes 
	DECLARE @tmpPackageProductWithoutSizes TABLE (ProductId INT)
	INSERT INTO @tmpPackageProductWithoutSizes (ProductId)
	SELECT [Id] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ItemsWithoutSizes, @Delimiter)
	
	SET @ItemsWithoutSizesNumInStock =
	(SELECT
		MIN([p].[NumberInStock])
	FROM 
		[product].[tProduct] p
		INNER JOIN @tmpPackageProductWithoutSizes pp ON [pp].[ProductId] = [p].[ProductId])
	
	
	-- products with sizes 
	DECLARE @tmpPackageProductWithSizes TABLE (ProductId INT, SizeId INT)
	INSERT INTO @tmpPackageProductWithSizes (ProductId, [SizeId])
	SELECT c.value('@productId[1]', 'int'), c.value('@sizeId[1]', 'int') FROM @ItemsWithSizes.nodes('/items/item') T(c)
	
	SET @ItemsWithSizesNumInStock =
	(SELECT
		MIN([ps].[NumberInStock])
	FROM 
		[lekmer].[tProductSize] ps
		INNER JOIN @tmpPackageProductWithSizes pp ON [pp].[ProductId] = [ps].[ProductId] AND [pp].[SizeId] = [ps].[SizeId])
		
	RETURN CASE WHEN @ItemsWithoutSizesNumInStock < @ItemsWithSizesNumInStock THEN @ItemsWithoutSizesNumInStock ELSE @ItemsWithSizesNumInStock END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageDelete]'
GO
CREATE PROCEDURE [productlek].[pPackageDelete]
	@PackageId	INT
AS
BEGIN
	DECLARE @MasterProductId INT 
	SET @MasterProductId = (SELECT [MasterProductId] FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	
	UPDATE [product].[tProduct]
	SET [IsDeleted] = 1
	WHERE [ProductId] = @MasterProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vProductActionTargetProductType]'
GO

CREATE VIEW [campaignlek].[vProductActionTargetProductType]
AS
	SELECT
		[ProductActionId] AS 'ProductActionTargetProductType.ProductActionId',
		[ProductTypeId] AS 'ProductActionTargetProductType.ProductTypeId'
	FROM
		[campaignlek].[tProductActionTargetProductType] t

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pProductActionTargetProductTypeGetAllByCampaign]'
GO
CREATE PROCEDURE [campaignlek].[pProductActionTargetProductTypeGetAllByCampaign]
	@CampaignId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[at].*
	FROM
		[campaignlek].[vProductActionTargetProductType] at
		INNER JOIN [campaign].[tProductAction] pa ON [pa].[ProductActionId] = [at].[ProductActionTargetProductType.ProductActionId]
	WHERE
		[pa].[CampaignId] = @CampaignId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vPackageOrderItem]'
GO

CREATE VIEW [orderlek].[vPackageOrderItem]
AS
SELECT
	[poi].[PackageOrderItemId] 'PackageOrderItem.PackageOrderItemId',
	[poi].[OrderItemId] 'PackageOrderItem.OrderItemId',
	[poi].[OrderId] 'PackageOrderItem.OrderId',
	[poi].[Quantity] 'PackageOrderItem.Quantity',
	[poi].[PackagePriceIncludingVat] 'PackageOrderItem.PackagePriceIncludingVat',
	[poi].[ActualPriceIncludingVat] 'PackageOrderItem.ActualPriceIncludingVat',
	[poi].[OriginalPriceIncludingVat] 'PackageOrderItem.OriginalPriceIncludingVat',
	[poi].[VAT] 'PackageOrderItem.VAT',
	[poip].[ProductId] 'PackageOrderItem.ProductId',
	[poip].[ErpId] 'PackageOrderItem.ErpId',
	[poip].[EanCode] 'PackageOrderItem.EanCode',
	[poip].[Title] 'PackageOrderItem.Title',
	[poip].[SizeId] 'PackageOrderItem.SizeId',
	[poip].[SizeErpId] 'PackageOrderItem.SizeErpId',
	s.*
FROM 
	[orderlek].[tPackageOrderItem] poi
	INNER JOIN [orderlek].[tPackageOrderItemProduct] poip ON [poip].[PackageOrderItemId] = [poi].[PackageOrderItemId]
	INNER JOIN [order].[vCustomOrderItemStatus] s ON [poi].[OrderItemStatusId] = [s].[OrderItemStatus.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pPackageOrderItemGetAllByOrderItem]'
GO
CREATE PROCEDURE [orderlek].[pPackageOrderItemGetAllByOrderItem]
	@OrderItemId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vPackageOrderItem]
	WHERE
		[PackageOrderItem.OrderItemId] = @OrderItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vProductType]'
GO
CREATE VIEW [productlek].[vProductType]
AS
SELECT     
      [ProductTypeId] 'ProductType.Id',
      [Title] 'ProductType.Title',
      [CommonName] 'ProductType.CommonName'
FROM
      [productlek].[tProductType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageWithSizesDecreaseNumberInStock]'
GO
--DROP PROCEDURE [lekmer].[pPackageWithSizesDecreaseNumberInStock]

CREATE PROCEDURE [productlek].[pPackageWithSizesDecreaseNumberInStock]
	@PackageMasterProductId	INT,
	@Quantity				INT,
	@ItemsWithoutSizes		VARCHAR(max),
	@ItemsWithSizes			XML,
	@Delimiter				CHAR(1)
	
AS 
BEGIN
	SET NOCOUNT ON
	DECLARE @NewNumberInStock INT

	-- Update Product/Package NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @PackageMasterProductId

	-- products without sizes 
	DECLARE @tmpPackageProductWithoutSizes TABLE (ProductId INT)
	INSERT INTO @tmpPackageProductWithoutSizes (ProductId)
	SELECT [Id] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ItemsWithoutSizes, @Delimiter)
	
	-- Update Package Products NumberInSock		
	UPDATE
		p
	SET
		[p].[NumberInStock] = (CASE WHEN [p].[NumberInStock] - @Quantity < 0 THEN 0
									ELSE [p].[NumberInStock] - @Quantity
							   END)
	FROM
		[product].[tProduct] p
		INNER JOIN @tmpPackageProductWithoutSizes pp ON [pp].[ProductId] = [p].[ProductId]
	
	-- Update NumberInStock of Packages that contain current product
	DECLARE @tmpProductId INT
	WHILE ((SELECT count(*) FROM @tmpPackageProductWithoutSizes) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProductWithoutSizes)
		SET @NewNumberInStock = (SELECT [NumberInStock] FROM [product].[tProduct] WHERE [ProductId] = @tmpProductId)
		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId, @NewNumberInStock
		DELETE @tmpPackageProductWithoutSizes WHERE [ProductId] = @tmpProductId
	END
	
	
	-- products with sizes 
	DECLARE @tmpPackageProductWithSizes TABLE (ProductId INT, SizeId INT)
	INSERT INTO @tmpPackageProductWithSizes (ProductId, [SizeId])
	SELECT c.value('@productId[1]', 'int'), c.value('@sizeId[1]', 'int') FROM @ItemsWithSizes.nodes('/items/item') T(c)
	
	-- Update ProductSize NumberInSock
	UPDATE
		ps
	SET
		[ps].[NumberInStock] = (CASE WHEN [ps].[NumberInStock] - @Quantity < 0 THEN 0
									 ELSE [ps].[NumberInStock] - @Quantity END)
	FROM
		[lekmer].[tProductSize] ps
		INNER JOIN @tmpPackageProductWithSizes pp ON [pp].[ProductId] = [ps].[ProductId] AND [pp].[SizeId] = [ps].[SizeId]
		
	-- Update NumberInStock of Packages that contain current product
	WHILE ((SELECT count(*) FROM @tmpPackageProductWithSizes) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProductWithSizes)
		SET @NewNumberInStock = (SELECT SUM([NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] WHERE [ProductId] = @tmpProductId)
		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId, @NewNumberInStock
		DELETE @tmpPackageProductWithSizes WHERE [ProductId] = @tmpProductId
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockPackageProductListSave]'
GO
CREATE PROCEDURE [productlek].[pBlockPackageProductListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT
AS
BEGIN
	UPDATE [productlek].[tBlockPackageProductList]
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [productlek].[tBlockPackageProductList] (
		[BlockId],
		[ColumnCount],
		[RowCount]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageProductDelete]'
GO
CREATE PROCEDURE [productlek].[pPackageProductDelete]
	@PackageId	INT
AS
BEGIN
	DELETE FROM [productlek].[tPackageProduct]
	WHERE [PackageId] = @PackageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vPackageSecure]'
GO


CREATE VIEW [productlek].[vPackageSecure]
AS
SELECT
	[p].[PackageId] 'Package.PackageId',
	[p].[MasterProductId] 'Package.MasterProductId',
	[cpr].*
FROM
	[productlek].[tPackage] p
	INNER JOIN [product].[vCustomProductRecord] cpr ON [cpr].[Product.Id] = [p].[MasterProductId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageGetByProductIdSecure]'
GO
CREATE PROCEDURE [productlek].[pPackageGetByProductIdSecure]
	@ProductId	INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;	
	
	DECLARE @CurrencyId INT, @PriceListRegistryId INT, @PriceListId INT

	SELECT @CurrencyId = [CurrencyId], @PriceListRegistryId = [PriceListRegistryId]
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@ProductId, @ChannelId)
	
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @ProductId, @PriceListRegistryId, NULL))
	
	SELECT
		*
	FROM
		[productlek].[vPackageSecure] p 
		LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[Package.MasterProductId]
														  AND [pli].[Price.PriceListId] = @PriceListId
	WHERE
		[p].[Package.MasterProductId] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageGetByIdSecure]'
GO
CREATE PROCEDURE [productlek].[pPackageGetByIdSecure]
	@PackageId	INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;	
	
	DECLARE @ProductId INT, @CurrencyId INT, @PriceListRegistryId INT, @PriceListId INT
	
	SELECT @ProductId = [MasterProductId] FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId
	
	SELECT @CurrencyId = [CurrencyId], @PriceListRegistryId = [PriceListRegistryId]
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@ProductId, @ChannelId)
	
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @ProductId, @PriceListRegistryId, NULL))
	
	SELECT
		*
	FROM
		[productlek].[vPackageSecure] p 
		LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[Package.MasterProductId]
														  AND [pli].[Price.PriceListId] = @PriceListId
	WHERE
		[p].[Package.PackageId] = @PackageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageProductSave]'
GO
CREATE PROCEDURE [productlek].[pPackageProductSave]
	@PackageId	INT,
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	EXEC [productlek].[pPackageProductDelete] @PackageId

	INSERT [productlek].[tPackageProduct] (
		[PackageId],
		[ProductId]
	)
	SELECT
		@PackageId,
		[p].[ID]
	FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductGetIdAllByPackageMasterProduct]'
GO
CREATE PROCEDURE [productlek].[pProductGetIdAllByPackageMasterProduct]
		@ChannelId		INT,
		@CustomerId		INT,
		@ProductId		INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @PackageId INT
	SET @PackageId = (SELECT [PackageId] FROM [productlek].[tPackage] WHERE [MasterProductId] = @ProductId)

	SELECT p.[Product.Id]
	FROM
		[productlek].[tPackageProduct] pp
		INNER JOIN [product].[vCustomProduct] p ON pp.ProductId = [p].[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		pp.[PackageId] = @PackageId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[vCartItemPackageElement]'
GO


CREATE VIEW [orderlek].[vCartItemPackageElement]
AS
SELECT
	[cipe].[CartItemPackageElementId] AS 'CartItemPackageElement.CartItemPackageElementId',
	[cipe].[CartItemId] AS 'CartItemPackageElement.CartItemId',
	[cipe].[ProductId] AS 'CartItemPackageElement.ProductId',
	[cipe].[SizeId] AS 'CartItemPackageElement.SizeId',
	[cipe].[ErpId] AS 'CartItemPackageElement.ErpId'
FROM
	[orderlek].[tCartItemPackageElement] cipe

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pPackageOrderItemSave]'
GO
CREATE PROCEDURE [orderlek].[pPackageOrderItemSave]
	@PackageOrderItemId			INT,
	@OrderItemId				INT,
	@OrderId					INT,
	@ProductId					INT,
	@Quantity					INT,
	@PackagePriceIncludingVat	DECIMAL(16,2),
	@ActualPriceIncludingVat	DECIMAL(16,2),
	@OriginalPriceIncludingVat	DECIMAL(16,2),
	@VAT						DECIMAL(16,2),
	@ErpId						VARCHAR(50),
	@EanCode					VARCHAR(20),
	@Title						NVARCHAR(50),
	@OrderItemStatusId			INT = 1,
	@SizeId						INT,
	@SizeErpId					VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[orderlek].[tPackageOrderItem]
	SET
		[OrderItemId]				= @OrderItemId,
		[OrderId]					= @OrderId,
		[Quantity]					= @Quantity,
		[PackagePriceIncludingVat]	= @PackagePriceIncludingVat,
		[ActualPriceIncludingVat]	= @ActualPriceIncludingVat,
		[OriginalPriceIncludingVat] = @OriginalPriceIncludingVat,
		[VAT]						= @VAT,
		[OrderItemStatusId]			= @OrderItemStatusId
	WHERE
		[PackageOrderItemId] = @PackageOrderItemId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [orderlek].[tPackageOrderItem]
		(
			[OrderItemId],
			[OrderId],
			[Quantity],
			[PackagePriceIncludingVat],
			[ActualPriceIncludingVat],
			[OriginalPriceIncludingVat],
			[VAT],
			[OrderItemStatusId]
		)
		VALUES
		(
			@OrderItemId,
			@OrderId,
			@Quantity,
			@PackagePriceIncludingVat,
			@ActualPriceIncludingVat,
			@OriginalPriceIncludingVat,
			@VAT,
			@OrderItemStatusId
		)

		SET @PackageOrderItemId = SCOPE_IDENTITY();
		
		INSERT INTO [orderlek].[tPackageOrderItemProduct]
		(
			[PackageOrderItemId],
			[OrderItemId],
			[ProductId],
			[ErpId],
			[EanCode],
			[Title],
			[SizeId],
			[SizeErpId]
		)
		VALUES
		(
			@PackageOrderItemId,
			@OrderItemId,
			@ProductId,
			@ErpId,
			@EanCode,
			@Title,
			@SizeId,
			@SizeErpId
		)
	END
	
	RETURN @PackageOrderItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductRegistryProductGetRestrictionsByProductId]'
GO
CREATE PROCEDURE [productlek].[pProductRegistryProductGetRestrictionsByProductId]
	@ProductId INT
AS
BEGIN
	SELECT
		[pr].[ProductRegistryId]
	FROM
		[product].[tProductRegistry] pr
	WHERE NOT EXISTS (SELECT 1 FROM [product].[tProductRegistryProduct] prp WHERE [prp].[ProductRegistryId] = [pr].[ProductRegistryId] AND [prp].[ProductId] = @ProductId)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockPackageProductListGetById]'
GO
CREATE PROCEDURE [productlek].[pBlockPackageProductListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		ppl.*,
		b.*
	FROM 
		[productlek].[vBlockPackageProductList] AS ppl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON ppl.[BlockPackageProductList.BlockId] = b.[Block.BlockId]
	WHERE
		ppl.[BlockPackageProductList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCartItemPackageElementSave]'
GO
CREATE PROCEDURE [orderlek].[pCartItemPackageElementSave]
	@CartItemPackageElementId INT,
	@CartItemId INT,
	@ProductId INT,
	@SizeId INT,
	@ErpId VARCHAR(50)
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[orderlek].[tCartItemPackageElement]
	SET	
		[CartItemId] = @CartItemId,
		[ProductId] = @ProductId,
		[SizeId] = @SizeId,
		[ErpId] = @ErpId
	WHERE
		[CartItemPackageElementId] = @CartItemPackageElementId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	INTO [orderlek].[tCartItemPackageElement]
				( [CartItemId],
				  [ProductId],
				  [SizeId],
				  [ErpId]
           		)
		VALUES
				( @CartItemId,
				  @ProductId,
				  @SizeId,
				  @ErpId
           		)

		SET @CartItemPackageElementId = SCOPE_IDENTITY()
	END
	
	RETURN @CartItemPackageElementId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductTypeDeleteByBlock]'
GO

CREATE PROCEDURE [productlek].[pProductTypeDeleteByBlock]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE [productlek].[tBlockProductType]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pProductActionTargetProductTypeSave]'
GO

CREATE PROCEDURE [campaignlek].[pProductActionTargetProductTypeSave]
	@ProductActionId INT,
	@TargetProductTypeIds VARCHAR(MAX),
	@Delimiter CHAR(1)
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@TargetProductTypeIds, @Delimiter)
	
	DELETE [campaignlek].[tProductActionTargetProductType]
	WHERE
		[ProductActionId] = @ProductActionId
	
	INSERT [campaignlek].[tProductActionTargetProductType] ( [ProductActionId], [ProductTypeId] )
	SELECT
		@ProductActionId,
		Id
	FROM
		@IdList
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pPackageOrderItemGetAllByOrder]'
GO
CREATE PROCEDURE [orderlek].[pPackageOrderItemGetAllByOrder]
	@OrderId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vPackageOrderItem]
	WHERE
		[PackageOrderItem.OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageUpdate]'
GO
CREATE PROCEDURE [productlek].[pPackageUpdate]
AS
BEGIN
	DECLARE @PackageId INT
	DECLARE @MinNumberInStock INT

	DECLARE cur_package CURSOR FAST_FORWARD FOR
	SELECT [PackageId] FROM [productlek].[tPackage]

	OPEN cur_package

	FETCH NEXT FROM cur_package 
	INTO @PackageId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @MinNumberInStock = (SELECT
									 MIN(COALESCE(p.NumberInStock, a1.[NumberInStock]))
								 FROM [product].[tProduct] p
								 CROSS APPLY (SELECT SUM(ps.[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] ps WHERE [ps].[ProductId] = [p].[ProductId]) a1
								 WHERE [p].[ProductId] IN (SELECT [pp].[ProductId] FROM [productlek].[tPackageProduct] pp WHERE [pp].[PackageId] = @PackageId))
		
		UPDATE p
		SET [p].[NumberInStock] = @MinNumberInStock
		FROM [product].[tProduct] p
			 INNER JOIN [productlek].[tPackage] pac ON [pac].[MasterProductId] = [p].[ProductId]
		WHERE [pac].[PackageId] = @PackageId

		FETCH NEXT FROM cur_package INTO @PackageId
	END

	CLOSE cur_package
	DEALLOCATE cur_package
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pCartItemPackageElementGetAllByCart]'
GO
CREATE PROCEDURE [orderlek].[pCartItemPackageElementGetAllByCart]
	@CartId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	SELECT
		[cipe].*
	FROM
		[orderlek].[vCartItemPackageElement] cipe
		INNER JOIN [order].[tCartItem] ci ON ci.[CartItemId] = [cipe].[CartItemPackageElement.CartItemId]
	WHERE
		[ci].[CartId] = @CartId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockPackageProductListGetByIdSecure]'
GO
CREATE PROCEDURE [productlek].[pBlockPackageProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		ppl.*,
		b.*
	FROM 
		[productlek].[vBlockPackageProductList] AS ppl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON ppl.[BlockPackageProductList.BlockId] = b.[Block.BlockId]
	WHERE
		ppl.[BlockPackageProductList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageSearch]'
GO
CREATE PROCEDURE [productlek].[pPackageSearch]
	@CategoryId		INT,
	@Title			NVARCHAR(256),
	@BrandId		INT,
	@StatusId		INT,
	@PriceFrom		DECIMAL(16,2),
	@PriceTo		DECIMAL(16,2),
	@ErpId			VARCHAR(MAX),
	@ChannelId		INT,
	@Page			INT = NULL,
	@PageSize		INT,
	@SortBy			VARCHAR(20) = NULL,
	@SortDescending BIT = NULL
AS
BEGIN
	DECLARE @sqlGeneral NVARCHAR(MAX), @sql NVARCHAR(MAX), @sqlCount NVARCHAR(MAX), @sqlFragment NVARCHAR(MAX)
	DECLARE @CurrencyId INT, @ProductRegistryId INT

	SELECT @CurrencyId = [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId
	SELECT @ProductRegistryId = [ProductRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId
	
	SET @sqlFragment = '
				SELECT ROW_NUMBER() OVER (ORDER BY [p].[' 
		+ COALESCE(@SortBy, 'ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
					[pak].[PackageId] ''Package.PackageId'',
					[pak].[MasterProductId] ''Package.MasterProductId'',
					[p].[ProductId],
					[pli].*
				FROM
					[productlek].[tPackage] pak
					INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [pak].[MasterProductId]
					LEFT JOIN [product].[tProductRegistryProduct] prp ON [prp].[ProductId] = [p].[ProductId] AND [prp].[ProductRegistryId] = @ProductRegistryId
					LEFT JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [prp].[ProductRegistryId] AND [pmc].[ChannelId] = @ChannelId
					LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[ProductId]
																	  AND [pli].[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
																										@CurrencyId,
																										[p].[ProductId],
																										[pmc].[PriceListRegistryId],
																										NULL)
					INNER JOIN [lekmer].[tLekmerProduct] AS lp ON [lp].[ProductId] = [p].[ProductId]
				WHERE
					[p].[IsDeleted] = 0'
		
		+ CASE WHEN (@CategoryId IS NOT NULL) THEN + '
					AND [p].[CategoryId] IN (SELECT * FROM product.fnGetSubCategories(@CategoryId))' ELSE '' END 
				
		+ CASE WHEN (@Title IS NOT NULL) THEN + '
					AND CONTAINS([p].*, @Title)' ELSE '' END
		
		+ CASE WHEN (@PriceFrom IS NOT NULL) THEN + '
					AND [pli].[Price.PriceIncludingVAT] >= @PriceFrom' ELSE '' END
		
		+ CASE WHEN (@PriceTo IS NOT NULL) THEN + '
					AND [pli].[Price.PriceIncludingVAT] <= @PriceTo' ELSE '' END
		
		+ CASE WHEN (@ErpId IS NOT NULL) THEN + '
					AND [p].[ErpId] IN (SELECT * FROM generic.fnConvertIDListToTableString(@ErpId,'',''))' ELSE '' END
		
		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
					AND [p].[ProductStatusId] = @StatusId' ELSE '' END
		
		+ CASE WHEN (@BrandId IS NOT NULL) THEN + '
					AND [lp].[BrandId] = @BrandId' ELSE '' END

	SET @sqlGeneral = '
		IF (@Title IS NOT NULL)
		BEGIN
			SET @Title = generic.fPrepareFulltextSearchParameter(@Title)
		END'
		
	SET @sql = @sqlGeneral + '
		SELECT
			p.*,
			vp.*
		FROM
			(' + @sqlFragment + '
			) AS p
			INNER JOIN [product].[vCustomProductRecord] vp on [vp].[Product.Id] = [p].[ProductId]'

	IF @Page != 0 AND @Page IS NOT NULL
	BEGIN
		SET @sql = @sql + '
		WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
	END
	
	SET @sqlCount = @sqlGeneral + '
		SELECT COUNT(1) FROM
		(' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	
			@CategoryId int,
			@Title nvarchar(256),
			@BrandId int,
			@StatusId int,
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@ChannelId	int,			
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@CategoryId,
			@Title,
			@BrandId,
			@StatusId,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@ChannelId,			
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId

	EXEC sp_executesql @sql, 
		N'	
			@CategoryId int,
			@Title nvarchar(256),
			@BrandId int,
			@StatusId int,
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@ChannelId	int,			
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@CategoryId,
			@Title,
			@BrandId,
			@StatusId,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@ChannelId,			
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductTypeGetAll]'
GO
CREATE PROCEDURE [productlek].[pProductTypeGetAll]
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[productlek].[vProductType]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageWithoutSizesDecreaseNumberInStock]'
GO
--DROP PROCEDURE [lekmer].[pPackageWithoutSizesDecreaseNumberInStock]

CREATE PROCEDURE [productlek].[pPackageWithoutSizesDecreaseNumberInStock]
	@PackageMasterProductId	INT,
	@Quantity				INT
AS 
BEGIN
	SET NOCOUNT ON
	DECLARE @NewNumberInStock INT

	-- Update Product/Package NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @PackageMasterProductId

	DECLARE @PackageId INT
	SET @PackageId = (SELECT [PackageId] FROM [productlek].[tPackage] WHERE [MasterProductId] = @PackageMasterProductId)

	DECLARE @tmpPackageProduct TABLE (ProductId INT)
	INSERT INTO @tmpPackageProduct (ProductId)
	SELECT DISTINCT [ProductId] FROM [productlek].[tPackageProduct] WHERE [PackageId] = @PackageId

	-- Update Package Products NumberInSock		
	UPDATE
		p
	SET
		[p].[NumberInStock] = (CASE WHEN [p].[NumberInStock] - @Quantity < 0 THEN 0
									ELSE [p].[NumberInStock] - @Quantity
							   END)
	FROM
		[product].[tProduct] p
		INNER JOIN @tmpPackageProduct pp ON [pp].[ProductId] = [p].[ProductId]
	
	-- Update NumberInStock of Packages that contain current product
	DECLARE @tmpProductId INT
	WHILE ((SELECT count(*) FROM @tmpPackageProduct) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProduct)
		SET @NewNumberInStock = (SELECT [NumberInStock] FROM [product].[tProduct] WHERE [ProductId] = @tmpProductId)
		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId, @NewNumberInStock
		DELETE @tmpPackageProduct WHERE [ProductId] = @tmpProductId
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductTypeGetIdAllByBlock]'
GO
CREATE PROCEDURE [productlek].[pProductTypeGetIdAllByBlock]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		[bpt].[ProductTypeId]
	FROM
		[productlek].[tBlockProductType] bpt
	WHERE
		[bpt].[BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageSave]'
GO
CREATE PROCEDURE [productlek].[pPackageSave]
	@PackageId			INT,
	@MasterProductId	INT,
	@ErpId				VARCHAR(50) = NULL,
	@StatusId			INT,
	@Title				NVARCHAR(256),
	@NumberInStock		INT,
	@CategoryId			INT,
	@Description		NVARCHAR(MAX),
	@PriceXml			XML = NULL
	/*
	'<prices>
		<price priceListId="1" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="2" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="3" priceIncludingVat="10.99" vat="25.00"/>
		<price priceListId="4" priceIncludingVat="10.99" vat="25.00"/>
	</prices>'
	*/
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	BEGIN
		BEGIN TRANSACTION
			IF @ErpId IS NULL
			BEGIN
				SET @ErpId = (SELECT TOP (1) [pr].[ErpId] 
						  FROM [productlek].[tPackage] p
								INNER JOIN [product].[tProduct] pr ON [pr].[ProductId] = [p].[MasterProductId]
						  ORDER BY [PackageId] DESC)

				IF @ErpId IS NULL
				BEGIN
					SET @ErpId = 'P000001-0000'
				END
				ELSE
				BEGIN
					DECLARE @ErpTemp INT
					SET @ErpTemp = (SELECT CAST(SUBSTRING(@ErpId, 2, 6) AS INT) + 1)

					SET @ErpId = (SELECT 'P' + RIGHT('000000' + CAST(@ErpTemp AS VARCHAR(6)), 6) + '-0000')
				END
			END
			
			-- tProduct
			INSERT INTO [product].[tProduct] (
				[ErpId],
				[IsDeleted],
				[NumberInStock],
				[CategoryId],
				[Title],
				[Description],
				[ProductStatusId]
			)
			VALUES (
				@ErpId,
				0, -- IsDeleted
				@NumberInStock,
				@CategoryId,
				@Title,
				@Description,
				@StatusId
			)

			SET @MasterProductId = SCOPE_IDENTITY()

			-- tLekmerProduct
			INSERT INTO [lekmer].[tLekmerProduct] (
				[ProductId],
				[IsBookable],
				[AgeFromMonth],
				[AgeToMonth],
				[IsBatteryIncluded],
				[HYErpId],
				[ShowVariantRelations],
				[ProductTypeId]
			)
			VALUES (
				@MasterProductId,
				0, -- IsBookable
				0, -- AgeFromMonth
				0, -- AgeToMonth
				0, -- IsBatteryIncluded
				@ErpId,
				0, -- ShowVariantRelations
				2 -- ProductTypeId = Package
			)
			
			-- tProductUrl
			INSERT INTO [lekmer].[tProductUrl] (
				[ProductId],
				[LanguageId],
				[UrlTitle])
			SELECT @MasterProductId, [LanguageId] ,@Title FROM [core].[tLanguage]

			-- tProductRegistryProduct
			INSERT INTO [product].[tProductRegistryProduct] (
				[ProductId],
				[ProductRegistryId]
			)
			SELECT
				@MasterProductId,
				[pr].[ProductRegistryId]
			FROM  [product].[tProductRegistry] pr
			WHERE NOT EXISTS (SELECT 1 FROM [product].[tProductRegistryProduct] prp
							  WHERE [prp].[ProductId] = @MasterProductId
									AND [prp].[ProductRegistryId] = [pr].[ProductRegistryId])

			-- tPackage
			INSERT INTO [productlek].[tPackage] ([MasterProductId]) SELECT @MasterProductId
			
			SET @PackageId = CAST(SCOPE_IDENTITY() AS INT)
		COMMIT
	END
	ELSE
	BEGIN
		-- tProduct
		UPDATE [product].[tProduct]
		SET [NumberInStock] = @NumberInStock,
			[CategoryId] = @CategoryId,
			[ProductStatusId] = @StatusId,
			[Title] = @Title,
			[Description] = @Description
		WHERE [ProductId] = @MasterProductId
	END
	
	-- tPriceListItem
	DELETE FROM [product].[tPriceListItem] WHERE [ProductId] = @MasterProductId
	
	IF @PriceXml IS NOT NULL
	BEGIN
		INSERT INTO [product].[tPriceListItem] (
				[PriceListId],
				[ProductId],
				[PriceIncludingVat],
				[PriceExcludingVat],
				[VatPercentage]
			)
		SELECT
			c.value('@priceListId[1]', 'int'),
			@MasterProductId,
			c.value('@priceIncludingVat[1]', 'decimal(16,2)'),
			c.value('@priceIncludingVat[1]', 'decimal(16,2)') / (1.0+c.value('@vat[1]', 'decimal(16,2)')/100.0),
			c.value('@vat[1]', 'decimal(16,2)')
		FROM
			@PriceXml.nodes('/prices/price') T(c)
	END
	
	SELECT @PackageId, @MasterProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pProductActionTargetProductTypeDelete]'
GO

CREATE PROCEDURE [campaignlek].[pProductActionTargetProductTypeDelete]
	@ProductActionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE [campaignlek].[tProductActionTargetProductType]
	WHERE [ProductActionId] = @ProductActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockPackageProductListDelete]'
GO
CREATE PROCEDURE [productlek].[pBlockPackageProductListDelete]
	@BlockId INT
AS
BEGIN
	DELETE [productlek].[tBlockPackageProductList]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pPackageOrderItemDelete]'
GO
CREATE PROCEDURE [orderlek].[pPackageOrderItemDelete]
	@OrderItemId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [orderlek].[tPackageOrderItemProduct]
	WHERE [OrderItemId] = @OrderItemId
	
	DELETE [orderlek].[tPackageOrderItem]
	WHERE [OrderItemId] = @OrderItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [orderlek].[tPackageOrderItem]'
GO
ALTER TABLE [orderlek].[tPackageOrderItem] ADD CONSTRAINT [CK_tPackageOrderItem_Quantity] CHECK (([Quantity]>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tPackage]'
GO
ALTER TABLE [productlek].[tPackage] WITH NOCHECK  ADD CONSTRAINT [FK_tPackage_tProduct] FOREIGN KEY ([MasterProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tPackageOrderItemProduct]'
GO
ALTER TABLE [orderlek].[tPackageOrderItemProduct] ADD CONSTRAINT [FK_tPackageOrderItemProduct_tPackageOrderItem] FOREIGN KEY ([PackageOrderItemId]) REFERENCES [orderlek].[tPackageOrderItemProduct] ([PackageOrderItemId])
ALTER TABLE [orderlek].[tPackageOrderItemProduct] ADD CONSTRAINT [FK_tPackageOrderItemProduct_tOrderItem] FOREIGN KEY ([OrderItemId]) REFERENCES [order].[tOrderItem] ([OrderItemId])
ALTER TABLE [orderlek].[tPackageOrderItemProduct] ADD CONSTRAINT [FK_tPackageOrderItemProduct_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tPackageProduct]'
GO
ALTER TABLE [productlek].[tPackageProduct] ADD CONSTRAINT [FK_tPackageProduct_tPackage] FOREIGN KEY ([PackageId]) REFERENCES [productlek].[tPackage] ([PackageId])
ALTER TABLE [productlek].[tPackageProduct] ADD CONSTRAINT [FK_tPackageProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tCartItemPackageElement]'
GO
ALTER TABLE [orderlek].[tCartItemPackageElement] ADD CONSTRAINT [FK_tCartItemPackageElement_tCartItem] FOREIGN KEY ([CartItemId]) REFERENCES [order].[tCartItem] ([CartItemId])
ALTER TABLE [orderlek].[tCartItemPackageElement] ADD CONSTRAINT [FK_tCartItemPackageElement_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tBlockProductType]'
GO
ALTER TABLE [productlek].[tBlockProductType] ADD CONSTRAINT [FK_tBlockProductType_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tBlockPackageProductList]'
GO
ALTER TABLE [productlek].[tBlockPackageProductList] ADD CONSTRAINT [FK_tBlockPackageProductList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [orderlek].[tPackageOrderItem]'
GO
ALTER TABLE [orderlek].[tPackageOrderItem] ADD CONSTRAINT [FK_tPackageOrderItem_tOrderItem] FOREIGN KEY ([OrderItemId]) REFERENCES [order].[tOrderItem] ([OrderItemId])
ALTER TABLE [orderlek].[tPackageOrderItem] ADD CONSTRAINT [FK_tPackageOrderItem_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
ALTER TABLE [orderlek].[tPackageOrderItem] ADD CONSTRAINT [FK_tPackageOrderItem_tOrderItemStatus] FOREIGN KEY ([OrderItemStatusId]) REFERENCES [order].[tOrderItemStatus] ([OrderItemStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tProductActionTargetProductType]'
GO
ALTER TABLE [campaignlek].[tProductActionTargetProductType] ADD CONSTRAINT [FK_tProductActionTargetProductType_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Disabling constraints on [productlek].[tPackage]'
GO
ALTER TABLE [productlek].[tPackage] NOCHECK CONSTRAINT [FK_tPackage_tProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
