SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [order].[tOrderItemProduct]'
GO
ALTER TABLE [order].[tOrderItemProduct] DROP CONSTRAINT [FK_tOrderItemProduct_tOrderItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [order].[tOrderItemProduct]'
GO
ALTER TABLE [order].[tOrderItemProduct] DROP CONSTRAINT [PK_tOrderItemProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tOrderItemProduct_ProductId] from [order].[tOrderItemProduct]'
GO
DROP INDEX [IX_tOrderItemProduct_ProductId] ON [order].[tOrderItemProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tOrderItemProduct_ErpId] from [order].[tOrderItemProduct]'
GO
DROP INDEX [IX_tOrderItemProduct_ErpId] ON [order].[tOrderItemProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [order].[tOrderItemProduct]'
GO
CREATE TABLE [order].[tmp_rg_xx_tOrderItemProduct]
(
[OrderItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[EanCode] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ProductTypeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [order].[tmp_rg_xx_tOrderItemProduct]([OrderItemId], [ProductId], [ErpId], [EanCode], [Title], [ProductTypeId]) SELECT [OrderItemId], [ProductId], [ErpId], [EanCode], [Title], 1 FROM [order].[tOrderItemProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [order].[tOrderItemProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[order].[tmp_rg_xx_tOrderItemProduct]', N'tOrderItemProduct'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tOrderItemProduct] on [order].[tOrderItemProduct]'
GO
ALTER TABLE [order].[tOrderItemProduct] ADD CONSTRAINT [PK_tOrderItemProduct] PRIMARY KEY CLUSTERED  ([OrderItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrderItemProduct_ProductId] on [order].[tOrderItemProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrderItemProduct_ProductId] ON [order].[tOrderItemProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tOrderItemProduct_ErpId] on [order].[tOrderItemProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tOrderItemProduct_ErpId] ON [order].[tOrderItemProduct] ([ErpId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO
ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	[product].[fProductHasSizes](ProductId) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations',
	[Weight] AS 'Lekmer.Weight',
	[ProductTypeId] AS 'Lekmer.ProductTypeId'
FROM
	lekmer.tLekmerProduct AS p
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProduct]'
GO
ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductViewWithoutStatusFilter]'
GO
ALTER VIEW [lekmer].[vCustomProductViewWithoutStatusFilter]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductViewWithoutStatusFilter] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductWithoutStatusFilter]'
GO
ALTER VIEW [lekmer].[vCustomProductWithoutStatusFilter]
AS 
	SELECT
		p.*,
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.[ParentContentNodeId] AS 'Product.ParentContentNodeId',
		i.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductWithoutStatusFilter] p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] AS ssmc ON ssmc.[ChannelId] = c.[Channel.Id]
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON p.[Product.Id] = pssr.[ProductId] AND ssmc.[SiteStructureRegistryId] = pssr.[SiteStructureRegistryId]
		LEFT JOIN [media].[vCustomImage] AS i ON i.[Image.MediaId] = p.[Product.MediaId] AND i.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pCartItemSave]'
GO
ALTER PROCEDURE [order].[pCartItemSave]
	@CartItemId INT,
	@CartId INT,
	@ProductId INT,
	@Quantity INT,
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[order].tCartItem
	SET	
		Quantity = @Quantity
	WHERE
		CartItemId = @CartItemId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	INTO [order].tCartItem
				( CartId,
				  ProductId,
				  Quantity,
				  CreatedDate            
           		)
		VALUES
				( @CartId,
				  @ProductId,
				  @Quantity,
				  @CreatedDate
           		)

		SET @CartItemId = SCOPE_IDENTITY()
	END
	
	RETURN @CartItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO
ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.LekmerErpId],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId',
		NULL AS 'Lekmer.RecommendedPrice'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vFilterProduct]'
GO

ALTER VIEW [lekmer].[vFilterProduct]
AS
	SELECT
		p.[ProductId] 'FilterProduct.ProductId',
		p.[CategoryId] 'FilterProduct.CategoryId',
		COALESCE(pt.[Title], p.[Title]) 'FilterProduct.Title',
		lp.[BrandId] 'FilterProduct.BrandId',
		lp.[AgeFromMonth] 'FilterProduct.AgeFromMonth',
		lp.[AgeToMonth] 'FilterProduct.AgeToMonth',
		lp.[IsNewFrom] 'FilterProduct.IsNewFrom',
		ch.[ChannelId] 'FilterProduct.ChannelId',
		ch.[CurrencyId] 'FilterProduct.CurrencyId',
		pmc.[PriceListRegistryId] 'FilterProduct.PriceListRegistryId',
		lpp.[ProductPopularity.Popularity] AS 'FilterProduct.Popularity',
		CASE 
			WHEN EXISTS ( SELECT 1 FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId] )
			THEN (SELECT ISNULL(SUM(ps.[NumberInStock]), 0) FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId] AND ps.[NumberInStock] >= 0)
			ELSE p.[NumberInStock]
		END AS 'FilterProduct.NumberInStock',
		lp.[ProductTypeId] AS 'FilterProduct.ProductTypeId'
	FROM
		[product].[tProduct] p
		INNER JOIN [lekmer].[tLekmerProduct] lp ON p.[ProductId] = lp.[ProductId]

		INNER JOIN [product].[tProductRegistryProduct] AS prp ON p.[ProductId] = prp.[ProductId]
		INNER JOIN [product].[tProductModuleChannel] AS pmc ON prp.[ProductRegistryId] = pmc.[ProductRegistryId]
		INNER JOIN [core].[tChannel] AS ch ON pmc.[ChannelId] = ch.[ChannelId]

		LEFT JOIN [product].[tProductTranslation] AS pt ON p.[ProductId] = pt.[ProductId] AND ch.[LanguageId] = pt.[LanguageId]
		LEFT JOIN [lekmer].[vProductPopularity] lpp on pmc.[ChannelId] = lpp.[ProductPopularity.ChannelId] AND p.[ProductId] = lpp.[ProductPopularity.ProductId]

	WHERE
		p.[IsDeleted] = 0
		AND p.[ProductStatusId] = 0

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pFilterProductGetAll]'
GO
ALTER PROCEDURE [lekmer].[pFilterProductGetAll]
	@CustomerId INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		/*Select only necessary columns (see data mapper)*/
		[FilterProduct.ProductId],
		[FilterProduct.CategoryId],
		--[FilterProduct.Title] ,
		[FilterProduct.BrandId] ,
		[FilterProduct.AgeFromMonth] ,
		[FilterProduct.AgeToMonth] ,
		[FilterProduct.IsNewFrom] ,
		--[FilterProduct.ChannelId] ,
		--[FilterProduct.CurrencyId] ,
		--[FilterProduct.PriceListRegistryId] ,
		[FilterProduct.Popularity] ,
		--[Price.PriceListId] ,
		--[Price.ProductId] ,
		[pp].[DiscountPriceIncludingVat] AS 'Price.DiscountPriceIncludingVat',
		[Price.PriceIncludingVat],
		--[Price.PriceExcludingVat] ,
		--[Price.VatPercentage]
		[FilterProduct.NumberInStock],
		[FilterProduct.ProductTypeId]
	FROM
		lekmer.vFilterProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[FilterProduct.ProductId]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[FilterProduct.CurrencyId],
				p.[FilterProduct.ProductId],
				p.[FilterProduct.PriceListRegistryId],
				@CustomerId
			)
		LEFT JOIN [export].[tProductPrice] pp
			ON p.[FilterProduct.ProductId] = pp.[ProductId]
			AND p.[FilterProduct.ChannelId] = pp.[ChannelId]
	WHERE
		[FilterProduct.ChannelId] = @ChannelId
	ORDER BY
		[FilterProduct.Title]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductRecord]'
GO
ALTER VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId]
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderItemSave]'
GO
ALTER PROCEDURE [order].[pOrderItemSave]
	@OrderItemId				INT,
	@OrderId					INT,
	@ProductId					INT,
	@Quantity					INT,
	@ActualPriceIncludingVat	DECIMAL(16,2),
	@OriginalPriceIncludingVat	DECIMAL(16,2),
	@VAT						DECIMAL(16,2),
	@ErpId						VARCHAR(50),
	@EanCode					VARCHAR(20),
	@Title						NVARCHAR(50),
	@OrderItemStatusId			INT = 1,
	@ProductTypeId				INT
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderItem]
	SET
		OrderId = @OrderId,
		Quantity = @Quantity,
		ActualPriceIncludingVat = @ActualPriceIncludingVat,
		OriginalPriceIncludingVat = @OriginalPriceIncludingVat,
		VAT = @VAT,
		OrderItemStatusId = @OrderItemStatusId
	WHERE
		OrderItemId = @OrderItemId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderItem]
		(
			OrderId,
			Quantity,
			ActualPriceIncludingVat,
			OriginalPriceIncludingVat,
			VAT,
			OrderItemStatusId
		)
		VALUES
		(
			@OrderId,
			@Quantity,
			@ActualPriceIncludingVat,
			@OriginalPriceIncludingVat,
			@VAT,
			@OrderItemStatusId
		)

		SET @OrderItemId = SCOPE_IDENTITY();
		
		INSERT INTO [order].[tOrderItemProduct]
		(
			OrderItemId,
			ProductId,
			ErpId,
			EanCode,
			Title,
			[ProductTypeId]
		)
		VALUES
		(
			@OrderItemId,
			@ProductId,
			@ErpId,
			@EanCode,
			@Title,
			@ProductTypeId
		)
	END
	
	RETURN @OrderItemId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductView]'
GO
ALTER VIEW [product].[vCustomProductView]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[vOrderItem]'
GO
ALTER VIEW [order].[vOrderItem]
AS
SELECT
	OI.OrderItemId AS 'OrderItem.OrderItemId',
	OI.OrderId AS 'OrderItem.OrderId',
	OI.Quantity AS 'OrderItem.Quantity',
	OI.OriginalPriceIncludingVat AS 'OrderItem.OriginalPriceIncludingVat',
	OI.ActualPriceIncludingVat AS 'OrderItem.ActualPriceIncludingVat',
	OI.VAT AS 'OrderItem.VAT',
	OIP.ProductId AS 'OrderItem.ProductId',
	OIP.ErpId AS 'OrderItem.ErpId',
	OIP.EanCode AS 'OrderItem.EanCode',
	OIP.Title AS 'OrderItem.Title',
	[OIP].[ProductTypeId] AS 'OrderItem.ProductTypeId',
	s.*
FROM 
	[order].[tOrderItem] OI
	INNER JOIN [order].[tOrderItemProduct] OIP ON OI.OrderItemId = OIP.OrderItemId
	INNER JOIN [order].vCustomOrderItemStatus s ON oi.OrderItemStatusId = s.[OrderItemStatus.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductSearch]'
GO


ALTER PROCEDURE [product].[pProductSearch]
	@ProductTypeId INT,
	@BrandId int,
	@CategoryId int,
	@Title nvarchar(256),
	@PriceFrom DECIMAL(16,2),
	@PriceTo DECIMAL(16,2),
	@ErpId varchar(max),
	@StatusId int,
	@EanCode varchar(20),
	@ChannelId	int,
	@Page int = NULL,
	@PageSize int,
	@SortBy varchar(20) = NULL,
	@SortDescending bit = NULL
AS
BEGIN
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sqlGeneral nvarchar(max)
	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

	DECLARE @CurrencyId int
	DECLARE @ProductRegistryId int

	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	SELECT @ProductRegistryId = ProductRegistryId FROM product.tProductModuleChannel WHERE ChannelId = @ChannelId

	SET @sqlFragment = '
				SELECT ROW_NUMBER() OVER (ORDER BY p.[' 
		+ COALESCE(@SortBy, 'ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
					p.ProductId,
					pli.*
				FROM
					product.tProduct AS p
					-- get price for current channel
					LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = p.[ProductId] AND PRP.ProductRegistryId = @ProductRegistryId
					LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
					LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[ProductId]
						AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, p.[ProductId], PMC.PriceListRegistryId, NULL)
						
					INNER JOIN lekmer.tLekmerProduct AS LP ON p.ProductId = LP.ProductId
				WHERE
					p.IsDeleted = 0'
		
		+ CASE WHEN (@CategoryId IS NOT NULL) THEN + '
					AND p.[CategoryId] IN (SELECT * FROM product.fnGetSubCategories(@CategoryId))' ELSE '' END 
				
		+ CASE WHEN (@Title IS NOT NULL) THEN + '
					AND CONTAINS(p.*, @Title)' ELSE '' END
		
		+ CASE WHEN (@PriceFrom IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] >= @PriceFrom' ELSE '' END
		
		+ CASE WHEN (@PriceTo IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] <= @PriceTo' ELSE '' END
		
		+ CASE WHEN (@ErpId IS NOT NULL) THEN + '
					AND p.[ErpId] IN (SELECT * FROM generic.fnConvertIDListToTableString(@ErpId,'',''))' ELSE '' END
		
		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
					AND p.[ProductStatusId] = @StatusId' ELSE '' END
		
		+ CASE WHEN (@EanCode IS NOT NULL) THEN + '
					AND p.[EanCode] = @EanCode' ELSE '' END
		+ CASE WHEN (@ProductTypeId IS NOT NULL) THEN + '
					AND LP.[ProductTypeId] = @ProductTypeId' ELSE '' END
		+ CASE WHEN (@BrandId IS NOT NULL) THEN + '
					AND LP.[BrandId] = @BrandId' ELSE '' END

	SET @sqlGeneral = '
		IF (@Title IS NOT NULL)
		BEGIN
			SET @Title = generic.fPrepareFulltextSearchParameter(@Title)
		END'
		
	SET @sql = @sqlGeneral + '
		SELECT
			p.*,
			vp.*
		FROM
			(' + @sqlFragment + '
			) AS p
			INNER JOIN product.vCustomProductSecure vp
				on p.ProductId = vp.[Product.Id]'


	IF @Page != 0 AND @Page IS NOT NULL
	BEGIN
		SET @sql = @sql + '
		WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
	END
	
	SET @sqlCount = @sqlGeneral + '
		SELECT COUNT(1) FROM
		(' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	
			@ProductTypeId int,
			@BrandId int,
			@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@StatusId int,
			@EanCode varchar(20),
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@ProductTypeId,
			@BrandId,
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode,
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId

	EXEC sp_executesql @sql, 
		N'	
			@ProductTypeId int,
			@BrandId int,
			@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@StatusId int,
			@EanCode varchar(20),
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@ProductTypeId,
			@BrandId,
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode,
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDeletePopularity]'
GO

ALTER PROCEDURE [lekmer].[pProductDeletePopularity]
	@ChannelId	INT
AS	
BEGIN
	SET NOCOUNT ON	
	
	DELETE  [lekmer].[tProductPopularity]
	WHERE ChannelId = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderItemDelete]'
GO
ALTER PROCEDURE [order].[pOrderItemDelete]
	@OrderItemId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [order].tOrderProductCampaign
	WHERE OrderItemId = @OrderItemId
	
	DELETE [order].[tOrderItemProduct]
	WHERE OrderItemId = @OrderItemId
	
	DELETE [order].[tOrderItem]
	WHERE OrderItemId = @OrderItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pCartItemGetAllByCart]'
GO
ALTER PROCEDURE [order].[pCartItemGetAllByCart]
	@ChannelId INT,
	@CustomerId INT,
	@CartId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[order].vCustomCartItem c
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = c.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				c.[Product.CurrencyId],
				c.[Product.Id],
				c.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		c.[CartItem.CartId] = @CartId
		AND c.[Product.ChannelId] = @ChannelId
	ORDER BY
		c.[CartItem.CreatedDate] ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]'
GO
ALTER PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	SELECT
		[p].[ProductId] 'Product.Id', 
		[p].[ItemsInPackage] 'Product.ItemsInPackage',
		[p].[ErpId] 'Product.ErpId',
		[p].[EanCode] 'Product.EanCode',
		[p].[NumberInStock] 'Product.NumberInStock', 
		[p].[CategoryId] 'Product.CategoryId',
		COALESCE([pt].[WebShopTitle], [p].WebShopTitle) 'Product.WebShopTitle',
		COALESCE([pt].[Title], [p].Title) 'Product.Title',
		[p].[ProductStatusId] 'Product.ProductStatusId',
		COALESCE([pt].[Description], [p].[Description]) 'Product.Description',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription]) 'Product.ShortDescription',
		@ChannelId 'Product.ChannelId', 
		@CurrencyId 'Product.CurrencyId',
		@PriceListRegistryId 'Product.PriceListRegistryId',
		[i].*,
		[pssr].[ParentContentNodeId] 'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId] 'Product.TemplateContentNodeId',
		[lp].[Lekmer.BrandId],
		[lp].[Lekmer.IsBookable],
		[lp].[Lekmer.AgeFromMonth],
		[lp].[Lekmer.AgeToMonth],
		[lp].[Lekmer.IsNewFrom],
		[lp].[Lekmer.IsNewTo],
		COALESCE ([lpt].[Product.Measurement], [lp].[Lekmer.Measurement]) 'Lekmer.Measurement',
		[lp].[Lekmer.BatteryTypeId],
		[lp].[Lekmer.NumberOfBatteries],
		[lp].[Lekmer.IsBatteryIncluded],
		[lp].[Lekmer.ExpectedBackInStock],
		[lp].[Lekmer.CreatedDate],
		[lp].[Lekmer.SizeDeviationId],
		[lp].[Lekmer.HasSizes],
		[lp].[Lekmer.LekmerErpId],
		[lp].[Lekmer.ShowVariantRelations],
		[lp].[Lekmer.Weight],
		[lp].[Lekmer.ProductTypeId],
		[bt].*,
		[sd].*,
		[pu].[ProductUrl.UrlTitle] 'Lekmer.UrlTitle',
		[rp].[Price] 'Lekmer.RecommendedPrice',
		[pli].*
	FROM
		[product].[tProduct] AS p
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON [lpt].[Product.Id] = [lp].[Lekmer.ProductId] AND [lpt].[Language.Id] = @LanguageId
		LEFT JOIN [lekmer].[vBatteryType] bt ON [bt].[BatteryType.Id] = [lp].[Lekmer.BatteryTypeId]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON [sd].[SizeDeviation.Id] = [lp].[Lekmer.SizeDeviationId]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl	ON [pl].[Id] = [p].[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pCartItemDelete]'
GO

ALTER PROCEDURE [order].[pCartItemDelete]
	@CartItemId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[order].tCartItem
	WHERE
		CartItemId = @CartItemId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductDecreaseNumberInStock]'
GO
ALTER PROCEDURE [lekmer].[pProductDecreaseNumberInStock]
	@ProductId			INT,
	@Quantity			INT
AS 
BEGIN
	SET NOCOUNT ON
	DECLARE @NewNumberInStock INT

	-- Update Product/Package NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @ProductId
		
	-- Update NumberInStock of Packages that contain current product
	SET @NewNumberInStock = (SELECT [NumberInStock] FROM [product].[tProduct] WHERE [ProductId] = @ProductId)
	EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @ProductId, @NewNumberInStock
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeDecreaseNumberInStock]'
GO
ALTER PROCEDURE [lekmer].[pProductSizeDecreaseNumberInStock]
	@ProductId INT,
	@SizeId INT,
	@Quantity INT
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @NewNumberInStock INT
	
	-- Update ProductSize NumberInSock
	UPDATE
		[lekmer].[tProductSize]
	SET
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0 
								ELSE [NumberInStock] - @Quantity END)
	WHERE
		[ProductId] = @ProductId
		AND [SizeId] = @SizeId
	
	SET @NewNumberInStock = (SELECT SUM([NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId)
	EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @ProductId, @NewNumberInStock
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[pCartDeleteExpiredItems]'
GO

ALTER PROCEDURE [orderlek].[pCartDeleteExpiredItems]
	@LifeCycleDays INT
AS
BEGIN
	SET NOCOUNT ON;

	-- Protection of Fools
	IF @LifeCycleDays < 30
		SET @LifeCycleDays = 30
	
	DECLARE	@ExpirationDate DATETIME = DATEADD(DAY, -@LifeCycleDays, DATEDIFF(DAY, 0, GETDATE()))
	DECLARE	@BatchSize INT = 1000

	IF EXISTS ( SELECT * FROM tempdb..sysobjects WHERE id = OBJECT_ID('tempdb..#DeleteItems') ) 
		DROP TABLE #DeleteItems

	CREATE TABLE #DeleteItems
	(
	  CartId INT,
	  PRIMARY KEY ( CartId )
	)

	WHILE 1 = 1
	BEGIN

		INSERT	INTO [#DeleteItems]
				SELECT TOP ( @BatchSize ) c.[CartId]
				FROM [order].[tCart] c
				WHERE c.[CreatedDate] < @ExpirationDate
					
		IF @@ROWCOUNT = 0
			BREAK

		DELETE cipe --tCartItemPackageElement
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			LEFT OUTER JOIN [orderlek].[tCartItemPackageElement] cipe ON [cipe].[CartItemId] = [ci].[CartItemId]

		DELETE lci --tLekmerCartItem
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			LEFT OUTER JOIN [lekmer].[tLekmerCartItem] lci ON [lci].[CartItemId] = [ci].[CartItemId]
			
		DELETE ci --tCartItem
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			
		DELETE cio --CartItemOption
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [campaignlek].[tCartItemOption] cio ON [cio].[CartId] = [di].[CartId]
			
		DELETE c --tCart
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			
		TRUNCATE TABLE [#DeleteItems]
	END

	DROP TABLE [#DeleteItems]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductSearchAdvanced]'
GO

ALTER PROCEDURE [product].[pProductSearchAdvanced]
	@ChannelId	INT,
	@SearchCriteriaInclusiveList XML,
	@SearchCriteriaExclusiveList XML,
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = 'ErpId',
	@SortDescending BIT = NULL
AS
BEGIN
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @ParsedProductTypeId INT
	DECLARE @ParsedBrandId INT
	DECLARE @ParsedCategoryId INT
	DECLARE @ParsedTitle NVARCHAR(256)
	DECLARE @ParsedPriceFrom DECIMAL(16,2)
	DECLARE @ParsedPriceTo DECIMAL(16,2)
	DECLARE @ParsedErpId VARCHAR(max)
	DECLARE @ParsedStatusId INT
	DECLARE @ParsedEanCode VARCHAR(20)
	
	DECLARE CriteriaInclusiveCursor CURSOR
		FOR SELECT 
					T.a.value('(@productTypeId)', 'INT') AS 'productTypeId',
					T.a.value('(@brandId)', 'INT') AS 'brandId',
					T.a.value('(@categoryId)', 'INT') AS 'categoryId',
					T.a.value('(@title)', 'NVARCHAR(256)') AS 'title',
					T.a.value('(@priceFrom)', 'DECIMAL(16,2)') AS 'priceFrom',
					T.a.value('(@priceTo)', 'DECIMAL(16,2)') AS 'priceTo',
					T.a.value('(@erpId)', 'VARCHAR(max)') AS 'erpId',
					T.a.value('(@statusId)', 'INT') AS 'statusId',
					T.a.value('(@eanCode)', 'VARCHAR(20)') AS 'eanCode'
			FROM    @SearchCriteriaInclusiveList.nodes('criterias/criteria') AS T ( a )
	
	OPEN CriteriaInclusiveCursor
	FETCH NEXT FROM CriteriaInclusiveCursor INTO @ParsedProductTypeId, @ParsedBrandId, @ParsedCategoryId, @ParsedTitle, @ParsedPriceFrom, @ParsedPriceTo, @ParsedERPId, @ParsedStatusId, @ParsedEANCode
	CLOSE CriteriaInclusiveCursor
	DEALLOCATE CriteriaInclusiveCursor

	IF ( @@FETCH_STATUS = 0 )
	BEGIN
		exec [product].[pProductSearch]
			@ProductTypeId = @ParsedProductTypeId,
			@BrandId = @ParsedBrandId,
			@CategoryId = @ParsedCategoryId,
			@Title = @ParsedTitle,
			@PriceFrom = @ParsedPriceFrom,
			@PriceTo = @ParsedPriceTo,
			@ErpId = @ParsedERPId,
			@StatusId  = @ParsedStatusId,
			@EanCode  = @ParsedEANCode,
			@ChannelId	 = @ChannelId,
			@Page = @Page,
			@PageSize  = @PageSize,
			@SortBy = @SortBy,
			@SortDescending = @SortDescending
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [order].[tOrderItemProduct]'
GO
ALTER TABLE [order].[tOrderItemProduct] ADD CONSTRAINT [FK_tOrderItemProduct_tOrderItem] FOREIGN KEY ([OrderItemId]) REFERENCES [order].[tOrderItem] ([OrderItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
