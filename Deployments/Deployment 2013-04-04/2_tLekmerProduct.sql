SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductSimilar]'
GO
ALTER TABLE [lekmer].[tProductSimilar] DROP CONSTRAINT [FK_tProductSimilar_tLekmerProduct]
ALTER TABLE [lekmer].[tProductSimilar] DROP CONSTRAINT [FK_tProductSimilar_tLekmerProduct1]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductUrl]'
GO
ALTER TABLE [lekmer].[tProductUrl] DROP CONSTRAINT [FK_tProductUrl_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductUrlHistory]'
GO
ALTER TABLE [lekmer].[tProductUrlHistory] DROP CONSTRAINT [FK_tProductUrlHistory_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [productlek].[tProductColor]'
GO
ALTER TABLE [productlek].[tProductColor] DROP CONSTRAINT [FK_tProductColor_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProductIcon]'
GO
ALTER TABLE [lekmer].[tLekmerProductIcon] DROP CONSTRAINT [FK_tLekmerProductIcon_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tProduct]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tBrand]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tBatteryType]
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [FK_tLekmerProduct_tSizeDeviation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] DROP CONSTRAINT [FK_tProductPopularity_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] DROP CONSTRAINT [FK_tProductSize_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tLekmerProductTranslation]'
GO
ALTER TABLE [lekmer].[tLekmerProductTranslation] DROP CONSTRAINT [FK_tLekmerProductTranslation_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [PK_tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_CreatedDate]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] DROP CONSTRAINT [DF_tLekmerProduct_ShowVariantRelations]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tLekmerProduct_BrandId] from [lekmer].[tLekmerProduct]'
GO
DROP INDEX [IX_tLekmerProduct_BrandId] ON [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tLekmerProduct_HYErpId] from [lekmer].[tLekmerProduct]'
GO
DROP INDEX [IX_tLekmerProduct_HYErpId] ON [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tLekmerProduct]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tLekmerProduct]
(
[ProductId] [int] NOT NULL,
[BrandId] [int] NULL,
[IsBookable] [bit] NOT NULL,
[AgeFromMonth] [int] NOT NULL,
[AgeToMonth] [int] NOT NULL,
[IsNewFrom] [datetime] NULL,
[IsNewTo] [datetime] NULL,
[Measurement] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[BatteryTypeId] [int] NULL,
[NumberOfBatteries] [int] NULL,
[IsBatteryIncluded] [bit] NOT NULL,
[ExpectedBackInStock] [datetime] NULL,
[HYErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_tLekmerProduct_CreatedDate] DEFAULT (getdate()),
[SizeDeviationId] [int] NULL,
[LekmerErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ShowVariantRelations] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_ShowVariantRelations] DEFAULT ((0)),
[Weight] [decimal] (18, 3) NULL,
[ProductTypeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tLekmerProduct]([ProductId], [BrandId], [IsBookable], [AgeFromMonth], [AgeToMonth], [IsNewFrom], [IsNewTo], [Measurement], [BatteryTypeId], [NumberOfBatteries], [IsBatteryIncluded], [ExpectedBackInStock], [HYErpId], [CreatedDate], [SizeDeviationId], [LekmerErpId], [ShowVariantRelations], [Weight], [ProductTypeId]) SELECT [ProductId], [BrandId], [IsBookable], [AgeFromMonth], [AgeToMonth], [IsNewFrom], [IsNewTo], [Measurement], [BatteryTypeId], [NumberOfBatteries], [IsBatteryIncluded], [ExpectedBackInStock], [HYErpId], [CreatedDate], [SizeDeviationId], [LekmerErpId], [ShowVariantRelations], [Weight], 1 FROM [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tLekmerProduct]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tLekmerProduct]', N'tLekmerProduct'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tLekmerProduct] on [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [PK_tLekmerProduct] PRIMARY KEY CLUSTERED  ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tLekmerProduct_BrandId] on [lekmer].[tLekmerProduct]'
GO
CREATE NONCLUSTERED INDEX [IX_tLekmerProduct_BrandId] ON [lekmer].[tLekmerProduct] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tLekmerProduct_HYErpId] on [lekmer].[tLekmerProduct]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tLekmerProduct_HYErpId] ON [lekmer].[tLekmerProduct] ([HYErpId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductPopularity]'
GO
ALTER TABLE [lekmer].[tProductPopularity] ADD CONSTRAINT [FK_tProductPopularity_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tProductColor]'
GO
ALTER TABLE [productlek].[tProductColor] ADD CONSTRAINT [FK_tProductColor_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductUrlHistory]'
GO
ALTER TABLE [lekmer].[tProductUrlHistory] ADD CONSTRAINT [FK_tProductUrlHistory_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProductTranslation]'
GO
ALTER TABLE [lekmer].[tLekmerProductTranslation] ADD CONSTRAINT [FK_tLekmerProductTranslation_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductUrl]'
GO
ALTER TABLE [lekmer].[tProductUrl] ADD CONSTRAINT [FK_tProductUrl_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProductIcon]'
GO
ALTER TABLE [lekmer].[tLekmerProductIcon] ADD CONSTRAINT [FK_tLekmerProductIcon_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tBatteryType] FOREIGN KEY ([BatteryTypeId]) REFERENCES [lekmer].[tBatteryType] ([BatteryTypeId]) ON DELETE SET NULL
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tSizeDeviation] FOREIGN KEY ([SizeDeviationId]) REFERENCES [lekmer].[tSizeDeviation] ([SizeDeviationId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductSimilar]'
GO
ALTER TABLE [lekmer].[tProductSimilar] ADD CONSTRAINT [FK_tProductSimilar_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
ALTER TABLE [lekmer].[tProductSimilar] ADD CONSTRAINT [FK_tProductSimilar_tLekmerProduct1] FOREIGN KEY ([SimilarProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] ADD CONSTRAINT [FK_tProductSize_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
