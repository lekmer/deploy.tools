INSERT INTO [campaignlek].[tProductActionTargetProductType]
		( [ProductActionId],
		  [ProductTypeId]
		)
SELECT
	pa.[ProductActionId],
	pt.[ProductTypeId]
FROM
	[campaign].[tProductAction] pa
	INNER JOIN [campaign].[tProductActionType] pat ON [pat].[ProductActionTypeId] = [pa].[ProductActionTypeId],
	[productlek].[tProductType] pt
WHERE
	(
		pat.[CommonName] = 'PercentagePriceDiscount'
		OR
		pat.[CommonName] = 'FixedDiscount'
		OR
		pat.[CommonName] = 'GiftCardViaMailProduct'
	)
	AND
	pt.[CommonName] = 'Product'
	AND NOT EXISTS (
		SELECT 1
		FROM [campaignlek].[tProductActionTargetProductType] tp 
		WHERE
			tp.[ProductActionId] = pa.[ProductActionId] 
			AND 
			tp.[ProductTypeId] = pt.[ProductTypeId]
	)