/*
Run this script on a database with the same schema as:

LekmerDB � the database with this schema will be modified

to synchronize its data with:

LekmerDB � this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 4/4/2013 1:28:22 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraints from [security].[tRolePrivilege]
ALTER TABLE [security].[tRolePrivilege] DROP CONSTRAINT [FK_tRolePrivilege_tPrivilege]
ALTER TABLE [security].[tRolePrivilege] DROP CONSTRAINT [FK_tRolePrivilege_tRole]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]

-- Drop constraint FK_tModelSetting_tModel from [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] DROP CONSTRAINT [FK_tModelSetting_tModel]

-- Drop constraints from [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tEntity]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tFunctionType]

-- Update row in [template].[tModelFragmentFunction]
UPDATE [template].[tModelFragmentFunction] SET [CommonName]=N'Product.CampaignInfoPriceHidden' WHERE [FunctionId]=1000133

-- Add row to [security].[tPrivilege]
SET IDENTITY_INSERT [security].[tPrivilege] ON
INSERT INTO [security].[tPrivilege] ([PrivilegeId], [CommonName], [Title]) VALUES (1000005, N'Assortment.Package', N'Assortment.Package')
SET IDENTITY_INSERT [security].[tPrivilege] OFF

-- Add row to [sitestructure].[tBlockType]
INSERT INTO [sitestructure].[tBlockType] ([BlockTypeId], [Title], [CommonName], [Ordinal], [AvailableForAllPageTypes]) VALUES (1000039, N'Package product list', N'PackageProductList', 0, 1)

-- Add rows to [template].[tEntityFunction]
SET IDENTITY_INSERT [template].[tEntityFunction] ON
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000173, 1, 2, N'ProductType.IsProduct', N'Is product has type PRODUCT')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000174, 1, 2, N'ProductType.IsPackage', N'Is product has type PACKAGE')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000175, 5, 2, N'ProductType.IsProduct', N'Is product has type PRODUCT')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000176, 5, 2, N'ProductType.IsPackage', N'Is product has type PACKAGE')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000177, 1000003, 1, N'Size.ProductId', N'Size.ProductId')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000178, 1, 2, N'Product.IsPackageWithSizes', N'Product.IsPackageWithSizes')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000179, 5, 2, N'Product.IsPackageWithSizes', N'Product.IsPackageWithSizes')
SET IDENTITY_INSERT [template].[tEntityFunction] OFF
-- Operation applied to 7 rows out of 7

-- Add row to [template].[tModel]
SET IDENTITY_INSERT [template].[tModel] ON
INSERT INTO [template].[tModel] ([ModelId], [ModelFolderId], [Title], [CommonName]) VALUES (1000058, 21, N'Block package product list', N'BlockPackageProductList')
SET IDENTITY_INSERT [template].[tModel] OFF

-- Add rows to [security].[tRolePrivilege]
INSERT INTO [security].[tRolePrivilege] ([RoleId], [PrivilegeId]) VALUES (1, 1000005)
INSERT INTO [security].[tRolePrivilege] ([RoleId], [PrivilegeId]) VALUES (217, 1000005)
-- Operation applied to 2 rows out of 2

-- Add rows to [template].[tModelFragmentRegion]
SET IDENTITY_INSERT [template].[tModelFragmentRegion] ON
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000200, 1000058, N'Content', 10)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000201, 1000058, N'Product list', 20)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000202, 1000058, N'Product', 30)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000203, 1000058, N'Head', 40)
INSERT INTO [template].[tModelFragmentRegion] ([ModelFragmentRegionId], [ModelId], [Title], [Ordinal]) VALUES (1000204, 31, N'Package Product', 1020)
SET IDENTITY_INSERT [template].[tModelFragmentRegion] OFF
-- Operation applied to 5 rows out of 5

-- Add rows to [template].[tModelFragment]
SET IDENTITY_INSERT [template].[tModelFragment] ON
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001229, 1000200, N'Content', N'Content', 1000058, 0, 20)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001230, 1000201, N'Product list', N'ProductList', 1000058, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001231, 1000201, N'Product row', N'ProductRow', 1000058, 10, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001232, 1000201, N'Empty space', N'EmptySpace', 1000058, 20, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001233, 1000202, N'Product', N'Product', 1000058, 0, 20)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001234, 1000203, N'Head', N'Head', 1000058, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001236, 1000204, N'Package product list', N'PackageProductList', 31, 0, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001237, 1000204, N'Package product', N'PackageProduct', 31, 10, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001238, 1000204, N'Package product size list', N'PackageProductSizeList', 31, 20, 10)
INSERT INTO [template].[tModelFragment] ([ModelFragmentId], [ModelFragmentRegionId], [Title], [CommonName], [ModelId], [Ordinal], [Size]) VALUES (1001239, 1000204, N'Package product size', N'PackageProductSize', 31, 30, 10)
SET IDENTITY_INSERT [template].[tModelFragment] OFF
-- Operation applied to 10 rows out of 10

-- Add rows to [template].[tModelFragmentEntity]
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001229, 15)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001229, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001229, 33)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001229, 34)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001230, 15)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001230, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001230, 33)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001230, 34)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001231, 15)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001231, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001231, 33)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001231, 34)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001232, 15)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001232, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001232, 33)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001232, 34)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001233, 1)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001233, 15)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001233, 17)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001233, 33)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001233, 34)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001234, 15)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001234, 34)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001237, 5)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (1001239, 1000003)
-- Operation applied to 25 rows out of 25

-- Add rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000823, 1001229, 1, N'ProductList', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000824, 1001229, 1, N'Paging', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000825, 1001230, 1, N'Iterate:ProductRow', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000826, 1001230, 1, N'Paging', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000827, 1001231, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000828, 1001231, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000829, 1001231, 1, N'Iterate:Product', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000830, 1001232, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000831, 1001232, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000832, 1001233, 2, N'IsFirst', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000833, 1001233, 2, N'IsLast', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000834, 47, 2, N'IsPackageWithSizes', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000835, 47, 1, N'PackageProductList', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000836, 1001236, 1, N'Iterate:PackageProduct', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000837, 1001237, 1, N'PackageProductSizeList', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000838, 1001238, 1, N'Iterate:PackageProductSize', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000839, 1000062, 2, N'CartItem.IsPackageWithSizes', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000840, 1000062, 1, N'CartItem.PackageElementsHashCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000841, 37, 2, N'CartItem.IsPackageWithSizes', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000842, 37, 1, N'CartItem.PackageElementsHashCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000843, 1001180, 1, N'CartItem.PackageElementsHashCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1000844, 1001180, 2, N'CartItem.IsPackageWithSizes', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF
-- Operation applied to 22 rows out of 22

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [security].[tRolePrivilege]
ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [FK_tRolePrivilege_tPrivilege] FOREIGN KEY ([PrivilegeId]) REFERENCES [security].[tPrivilege] ([PrivilegeId])
ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [FK_tRolePrivilege_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])

-- Add constraint FK_tModelSetting_tModel to [template].[tModelSetting]
ALTER TABLE [template].[tModelSetting] WITH CHECK ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
COMMIT TRANSACTION
GO
