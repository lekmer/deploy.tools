SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [export].[tCdonExportProductMapping]'
GO
CREATE TABLE [export].[tCdonExportProductMapping]
(
[CdonProductId] [int] NOT NULL,
[CdonArticleId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportProductMappingGetAll]'
GO
CREATE PROCEDURE [export].[pCdonExportProductMappingGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[export].[tCdonExportProductMapping]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [export].[pCdonExportProductMappingSave]'
GO
CREATE PROCEDURE [export].[pCdonExportProductMappingSave]
	@CdonProductId	INT,
	@CdonArticleId	INT
AS	
BEGIN
	SET NOCOUNT ON
	
	IF NOT EXISTS (SELECT 1 FROM [export].[tCdonExportProductMapping] WHERE [CdonArticleId] = @CdonArticleId)
		BEGIN
			INSERT INTO	[export].[tCdonExportProductMapping]
				(
					[CdonProductId],
					[CdonArticleId]
				)
			VALUES
				(
					@CdonProductId,
					@CdonArticleId
				)
			
			SELECT @CdonProductId
		END
	ELSE 
		BEGIN
			SELECT [CdonProductId] 
			FROM [export].[tCdonExportProductMapping] 
			WHERE [CdonArticleId] = @CdonArticleId
		END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [export].[tCdonExportProductMapping]'
GO
ALTER TABLE [export].[tCdonExportProductMapping] ADD CONSTRAINT [UC_CdonProductId] UNIQUE NONCLUSTERED  ([CdonProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [export].[tCdonExportProductMapping] ADD CONSTRAINT [UC_CdonArticleId] UNIQUE NONCLUSTERED  ([CdonArticleId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
