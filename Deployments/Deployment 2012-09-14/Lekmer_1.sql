/*
Run this script on:

        (local).Lekmer_live_1    -  This database will be modified

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 026\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 13.09.2012 19:50:50

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[tProductSize]'
GO
ALTER TABLE [lekmer].[tProductSize] ADD
[OverrideMillimeter] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemPercentageDiscountAction]'
GO
CREATE TABLE [lekmer].[tCartItemPercentageDiscountAction]
(
[CartActionId] [int] NOT NULL,
[DiscountAmount] [decimal] (16, 2) NOT NULL,
[IncludeAllProducts] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPercentageDiscountAction] on [lekmer].[tCartItemPercentageDiscountAction]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountAction] ADD CONSTRAINT [PK_tCartItemPercentageDiscountAction] PRIMARY KEY CLUSTERED  ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemPercentageDiscountActionExcludeBrand]'
GO
CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPercentageDiscountActionExcludeBrand] on [lekmer].[tCartItemPercentageDiscountActionExcludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeBrand] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionExcludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemPercentageDiscountActionExcludeCategory]'
GO
CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPercentageDiscountActionExcludeCategory] on [lekmer].[tCartItemPercentageDiscountActionExcludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeCategory] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionExcludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemPercentageDiscountActionExcludeProduct]'
GO
CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPercentageDiscountActionExcludeProduct] on [lekmer].[tCartItemPercentageDiscountActionExcludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeProduct] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionExcludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemPercentageDiscountActionIncludeBrand]'
GO
CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPercentageDiscountActionIncludeBrand] on [lekmer].[tCartItemPercentageDiscountActionIncludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeBrand] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionIncludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemPercentageDiscountActionIncludeCategory]'
GO
CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPercentageDiscountActionIncludeCategory] on [lekmer].[tCartItemPercentageDiscountActionIncludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeCategory] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionIncludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tCartItemPercentageDiscountActionIncludeProduct]'
GO
CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPercentageDiscountActionIncludeProduct] on [lekmer].[tCartItemPercentageDiscountActionIncludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeProduct] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionIncludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionDelete]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionDelete]
	@CartActionId INT
AS 
BEGIN
	DELETE FROM [lekmer].[tCartItemPercentageDiscountAction]
	WHERE CartActionId = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeBrandDeleteAll]'
GO



CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemPercentageDiscountActionExcludeBrand]
	WHERE
		CartActionId = @CartActionId
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeBrandGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionExcludeBrand]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeBrandInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeBrandInsert]
	@CartActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemPercentageDiscountActionExcludeBrand] (
		CartActionId,
		BrandId
	)
	VALUES (
		@CartActionId,
		@BrandId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemPercentageDiscountActionExcludeCategory]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		CategoryId,
		IncludeSubcategories
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionExcludeCategory]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAllRecursive]'
GO


CREATE procedure [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAllRecursive]
	@CartActionId		int
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT CategoryId, IncludeSubcategories 
	FROM [lekmer].[tCartItemPercentageDiscountActionExcludeCategory]
	WHERE CartActionId = @CartActionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT count(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryInsert]
	@CartActionId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	INSERT [lekmer].[tCartItemPercentageDiscountActionExcludeCategory] (
		CartActionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES (
		@CartActionId,
		@CategoryId,
		@IncludeSubcategories
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeProductDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemPercentageDiscountActionExcludeProduct]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeProductInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemPercentageDiscountActionExcludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeBrandDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemPercentageDiscountActionIncludeBrand]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeBrandGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionIncludeBrand]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeBrandInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeBrandInsert]
	@CartActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemPercentageDiscountActionIncludeBrand] (
		CartActionId,
		BrandId
	)
	VALUES (
		@CartActionId,
		@BrandId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemPercentageDiscountActionIncludeCategory]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		CategoryId,
		IncludeSubcategories
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionIncludeCategory]
	WHERE 
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAllRecursive]'
GO


CREATE procedure [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryGetIdAllRecursive]
	@CartActionId		int
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryIteration (tCategoryId, tSub)
	SELECT CategoryId, IncludeSubcategories 
	FROM [lekmer].[tCartItemPercentageDiscountActionIncludeCategory]
	WHERE CartActionId = @CartActionId

	DECLARE @CategoryId int, @Sub bit
	DECLARE @tCategoryResult TABLE (tCategoryId INT, tSub BIT)
	INSERT INTO @tCategoryResult (tCategoryId, tSub)
	SELECT tCategoryId, tSub 
	FROM @tCategoryIteration

	WHILE ((SELECT count(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 tCategoryId FROM @tCategoryIteration)
		SET @Sub = (SELECT TOP 1 tSub FROM @tCategoryIteration)
		
		IF (@Sub = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT CategoryId, ParentCategoryId
				FROM  product.tCategory
				WHERE ParentCategoryId = @CategoryId
				UNION ALL
				SELECT C.CategoryId, C.ParentCategoryId
				FROM product.tCategory C 
				JOIN Category OuterC ON OuterC.CategoryId = C.ParentCategoryId
			)
			INSERT INTO @tCategoryResult (tCategoryId, tSub)
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE tCategoryId = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeCategoryInsert]
	@CartActionId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	INSERT [lekmer].[tCartItemPercentageDiscountActionIncludeCategory] (
		CartActionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES (
		@CartActionId,
		@CategoryId,
		@IncludeSubcategories
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeProductDeleteAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemPercentageDiscountActionIncludeProduct]
	WHERE
		CartActionId = @CartActionId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeProductInsert]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemPercentageDiscountActionIncludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionSave]'
GO

CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionSave]
	@CartActionId			INT,
	@DiscountAmount			DECIMAL(16,2),
	@IncludeAllProducts		BIT
AS
BEGIN
	UPDATE
		[lekmer].[tCartItemPercentageDiscountAction]
	SET
		DiscountAmount = @DiscountAmount,
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [lekmer].[tCartItemPercentageDiscountAction] (
			CartActionId,
			DiscountAmount,
			IncludeAllProducts
		)
		VALUES (
			@CartActionId,
			@DiscountAmount,
			@IncludeAllProducts
		)
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeSave]'
GO

ALTER PROCEDURE [lekmer].[pProductSizeSave]
	@ProductId				INT,
	@SizeId					INT,
	@OverrideMillimeter		INT,
	@MillimeterDeviation	INT,
	@OverrideEU				DECIMAL(3, 1)
AS 
BEGIN 
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tProductSize]
	SET
		MillimeterDeviation = @MillimeterDeviation,
		OverrideEU			= @OverrideEU,
		OverrideMillimeter	= @OverrideMillimeter
	WHERE 
		ProductId = @ProductId AND
		SizeId = @SizeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAllSecure]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAllSecure]
	@CartActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[lekmer].[tCartItemPercentageDiscountActionExcludeProduct] c
		INNER JOIN [product].[tProduct] p ON c.[ProductId] = p.[ProductId]
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[IsDeleted] = 0
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAllSecure]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAllSecure]
	@CartActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[lekmer].[tCartItemPercentageDiscountActionIncludeProduct] c
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = c.[ProductId]
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[IsDeleted] = 0
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vProductSize]'
GO
ALTER VIEW [lekmer].[vProductSize]
AS
SELECT     
	ps.[ProductId] AS 'ProductSize.ProductId',
	ps.[SizeId] AS 'ProductSize.SizeId',
	ps.[ErpId] AS 'ProductSize.ErpId',
	ps.[NumberInStock] AS 'ProductSize.NumberInStock',
	ps.[MillimeterDeviation] AS 'ProductSize.MillimeterDeviation',
	ps.[OverrideEU] AS 'ProductSize.OverrideEU',
	ps.[OverrideMillimeter] AS 'ProductSize.OverrideMillimeter',
	s.*
FROM
	[lekmer].[tProductSize] ps
	INNER JOIN [lekmer].[vSize] s ON ps.[SizeId] = s.[Size.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCartItemPercentageDiscountAction]'
GO



CREATE VIEW [lekmer].[vCartItemPercentageDiscountAction]
AS
	SELECT
		cid.[CartActionId]				AS 'CartItemPercentageDiscountAction.CartActionId',
		cid.[DiscountAmount]	AS 'CartItemPercentageDiscountAction.DiscountAmount',
		cid.[IncludeAllProducts]		AS 'CartItemPercentageDiscountAction.IncludeAllProducts',
		ca.*
	FROM
		[lekmer].[tCartItemPercentageDiscountAction] cid
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = cid.[CartActionId]



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionGetById]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemPercentageDiscountAction]
	WHERE
		[CartItemPercentageDiscountAction.CartActionId] = @CartActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@CartActionId		INT
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		[lekmer].[tCartItemPercentageDiscountActionExcludeProduct] c
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = c.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[Product.ChannelId] = @ChannelId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAll]'
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@CartActionId		INT
AS
BEGIN
	SELECT
		p.[Product.Id]
	FROM
		[lekmer].[tCartItemPercentageDiscountActionIncludeProduct] c
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = c.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[Product.ChannelId] = @ChannelId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemPercentageDiscountAction]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountAction] ADD
CONSTRAINT [FK_tCartItemPercentageDiscountAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemPercentageDiscountActionExcludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeBrand] ADD
CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeBrand_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemPercentageDiscountActionExcludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeCategory] ADD
CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeCategory_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemPercentageDiscountActionExcludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeProduct] ADD
CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeProduct_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemPercentageDiscountActionIncludeBrand]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeBrand] ADD
CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeBrand_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemPercentageDiscountActionIncludeCategory]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeCategory] ADD
CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeCategory_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tCartItemPercentageDiscountActionIncludeProduct]'
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeProduct] ADD
CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeProduct_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE,
CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
