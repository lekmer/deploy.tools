SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [customer].[tAddress]'
GO
ALTER TABLE [customer].[tAddress] DROP CONSTRAINT [FK_tAddress_tCustomerInformation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderPaymentDeleteByOrder]'
GO
CREATE PROCEDURE [orderlek].[pOrderPaymentDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [order].[tOrderPayment]
	WHERE [OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderAddressDelete]'
GO
CREATE PROCEDURE [orderlek].[pOrderAddressDelete]
	@OrderAddressId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [orderlek].[tOrderAddress]
	WHERE [OrderAddressId] = @OrderAddressId
	
	DELETE [order].[tOrderAddress]
	WHERE [OrderAddressId] = @OrderAddressId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderStatusUpdate]'
GO
CREATE PROCEDURE [orderlek].[pOrderStatusUpdate]
	@OrderId INT,
	@OrderStatusId INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE
		[order].[tOrder]
	SET
		OrderStatusId = @OrderStatusId
	WHERE
		OrderId = @OrderId
		AND OrderStatusId <> @OrderStatusId

	DECLARE @OrderItemStatusId INT
	SET @OrderItemStatusId = (SELECT [OrderItemStatusId] FROM [order].[tOrderItemStatus] WHERE [CommonName] = (SELECT [CommonName] FROM [order].[tOrderStatus] WHERE [OrderStatusId] = @OrderStatusId))

	UPDATE
		[order].[tOrderItem]
	SET
		OrderItemStatusId = @OrderItemStatusId
	WHERE
		[OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderCommentDeleteByOrder]'
GO
CREATE PROCEDURE [orderlek].[pOrderCommentDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [order].[tOrderComment]
	WHERE [OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pMonitorProductGetNextPortion]'
GO
ALTER PROCEDURE [lekmer].[pMonitorProductGetNextPortion]
	@PreviousMaxId	INT,
	@Quantity		INT
AS 
BEGIN 
	SELECT TOP (@Quantity)
		*
	FROM 
		[lekmer].[tMonitorProduct] mp
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = mp.[ProductId]
	WHERE
		mp.[MonitorProductId] > @PreviousMaxId
		AND
		p.[ProductStatusId] = 0 -- Online only
	ORDER BY 
		mp.[MonitorProductId] ASC
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[pCustomerInformationDeleteByCustomer]'
GO
CREATE PROCEDURE [customerlek].[pCustomerInformationDeleteByCustomer]
	@CustomerId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [customerlek].[tCustomerInformation]
	WHERE [CustomerId] = @CustomerId
	
	DELETE [customer].[tCustomerInformation]
	WHERE [CustomerId] = @CustomerId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderDelete]'
GO
CREATE PROCEDURE [orderlek].[pOrderDelete]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [lekmer].[tLekmerOrder]
	WHERE [OrderId] = @OrderId
	
	DELETE [order].[tOrder]
	WHERE [OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderAuditDeleteByOrder]'
GO
CREATE PROCEDURE [orderlek].[pOrderAuditDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [order].[tOrderAudit]
	WHERE [OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pLekmerOrderGetByKCOId]'
GO
CREATE PROCEDURE [orderlek].[pLekmerOrderGetByKCOId]
	@KCOId VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Lekmer.KCOId] = @KCOId
		AND [OrderStatus.CommonName] = 'KCOPaymentPending'
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[pCustomerDelete]'
GO
CREATE PROCEDURE [customerlek].[pCustomerDelete]
	@CustomerId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [customer].[tCustomerUser]
	WHERE [CustomerId] = @CustomerId
	
	DELETE [customer].[tCustomerComment]
	WHERE [CustomerId] = @CustomerId
	
	DELETE [customer].[tCustomerGroupCustomer]
	WHERE [CustomerId] = @CustomerId
	
	DELETE [customer].[tCustomer]
	WHERE [CustomerId] = @CustomerId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pGiftCardViaMailInfoDeleteByOrder]'
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [campaignlek].[tGiftCardViaMailInfo]
	WHERE [OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pGetKlarnaCheckoutOrders]'
GO
CREATE PROCEDURE [orderlek].[pGetKlarnaCheckoutOrders]
	@Days INT,
	@OrderStatus VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Current date.
	DECLARE @CurrentDate AS DATETIME
	SET @CurrentDate = GETDATE()

	-- Deletion date.
	DECLARE @DeletionDate AS DATETIME
	SET @DeletionDate = DATEADD(DAY, -@Days, @CurrentDate)

	-- Get old deleted orders
	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.CreatedDate] < @DeletionDate
		AND [OrderStatus.CommonName] = @OrderStatus
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[pAddressDeleteByCustomer]'
GO
CREATE PROCEDURE [customerlek].[pAddressDeleteByCustomer]
	@CustomerId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [customerlek].[tAddress]
	WHERE [CustomerId] = @CustomerId
	
	DELETE [customer].[tAddress]
	WHERE [CustomerId] = @CustomerId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [orderlek].[pOrderCartCampaignDeleteByOrder]'
GO
CREATE PROCEDURE [orderlek].[pOrderCartCampaignDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE [order].[tOrderCartCampaign]
	WHERE [OrderId] = @OrderId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customer].[tAddress]'
GO
ALTER TABLE [customer].[tAddress] ADD CONSTRAINT [FK_tAddress_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
