/*
Run this script on a database with the same schema as:

LekmerDB – the database with this schema will be modified

to synchronize its data with:

LekmerDB – this scripts folder will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 10.2.0 from Red Gate Software Ltd at 2/7/2014 10:04:12 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Add rows to [order].[tOrderStatus]
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (1, N'Payment pending', N'PaymentPending')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (2, N'Payment confirmed', N'PaymentConfirmed')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (3, N'Canceled', N'Canceled')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (4, N'Order in HY', N'OrderInHY')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (5, N'Error moving order into HY', N'OrderError')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (6, N'Order row limit exceeded', N'OrderRowItemOverflow')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (7, N'Rejected By Payment Provider', N'RejectedByPaymentProvider')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (8, N'KCO deleted', N'KCODeleted')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (9, N'KCO payment pending', N'KCOPaymentPending')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (10, N'Payment on hold', N'PaymentOnHold')
-- Operation applied to 10 rows out of 10

-- Add rows to [order].[tOrderItemStatus]
INSERT INTO [order].[tOrderItemStatus] ([OrderItemStatusId], [Title], [CommonName]) VALUES (1, N'Payment pending', N'PaymentPending')
INSERT INTO [order].[tOrderItemStatus] ([OrderItemStatusId], [Title], [CommonName]) VALUES (2, N'Payment confirmed', N'PaymentConfirmed')
INSERT INTO [order].[tOrderItemStatus] ([OrderItemStatusId], [Title], [CommonName]) VALUES (3, N'Canceled', N'Canceled')
INSERT INTO [order].[tOrderItemStatus] ([OrderItemStatusId], [Title], [CommonName]) VALUES (4, N'Rejected By Payment Provider', N'RejectedByPaymentProvider')
INSERT INTO [order].[tOrderItemStatus] ([OrderItemStatusId], [Title], [CommonName]) VALUES (5, N'KCO deleted', N'KCODeleted')
INSERT INTO [order].[tOrderItemStatus] ([OrderItemStatusId], [Title], [CommonName]) VALUES (6, N'KCO payment pending', N'KCOPaymentPending')
INSERT INTO [order].[tOrderItemStatus] ([OrderItemStatusId], [Title], [CommonName]) VALUES (7, N'Payment on hold', N'PaymentOnHold')
-- Operation applied to 7 rows out of 7

-- Add row to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (1001145, 1000030, 2, N'IsVisible', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
COMMIT TRANSACTION
GO
