SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [productlek].[pPackageGetByProductIdSecure]'
GO
DROP PROCEDURE [productlek].[pPackageGetByProductIdSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pRefreshProductListingTemporaryData]'
GO
DROP PROCEDURE [lekmer].[pRefreshProductListingTemporaryData]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pRefreshCustomerGroupProductPriceListTemporaryData]'
GO
DROP PROCEDURE [lekmer].[pRefreshCustomerGroupProductPriceListTemporaryData]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerProduct]'
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD
[SellOnlyInPackage] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_SellOnlyInPackage] DEFAULT ((0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vLekmerProduct]'
GO


ALTER VIEW [lekmer].[vLekmerProduct]
AS
SELECT
	ProductId AS 'Lekmer.ProductId',
	BrandId AS 'Lekmer.BrandId',
	IsBookable AS 'Lekmer.IsBookable',
	AgeFromMonth AS 'Lekmer.AgeFromMonth',
	AgeToMonth AS 'Lekmer.AgeToMonth',
	IsNewFrom AS 'Lekmer.IsNewFrom',
	IsNewTo AS 'Lekmer.IsNewTo',
	Measurement AS 'Lekmer.Measurement',
	BatteryTypeId AS 'Lekmer.BatteryTypeId',
	NumberOfBatteries AS 'Lekmer.NumberOfBatteries',
	IsBatteryIncluded AS 'Lekmer.IsBatteryIncluded',
	ExpectedBackInStock AS 'Lekmer.ExpectedBackInStock',
	CreatedDate AS 'Lekmer.CreatedDate',
	SizeDeviationId AS 'Lekmer.SizeDeviationId',
	[product].[fProductHasSizes](ProductId) AS 'Lekmer.HasSizes',
	LekmerErpId AS 'Lekmer.LekmerErpId',
	ShowVariantRelations AS 'Lekmer.ShowVariantRelations',
	[Weight] AS 'Lekmer.Weight',
	[ProductTypeId] AS 'Lekmer.ProductTypeId',
	[IsAbove60L] AS 'Lekmer.IsAbove60L',
	[StockStatusId] AS 'Lekmer.StockStatusId',
	[MonitorThreshold] AS 'Lekmer.MonitorThreshold',
	[MaxQuantityPerOrder] AS 'Lekmer.MaxQuantityPerOrder',
	[SellOnlyInPackage] AS 'Lekmer.SellOnlyInPackage'
FROM
	lekmer.tLekmerProduct AS p

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [product].[vCustomProductInclPackageProducts]'
GO
CREATE VIEW [product].[vCustomProductInclPackageProducts]
AS 
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		[lp].[Lekmer.MonitorThreshold],
		[lp].[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.SellOnlyInPackage],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.ParentContentNodeId AS 'Product.ParentContentNodeId',
		I.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProduct] p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN sitestructure.tSiteStructureModuleChannel AS ssmc ON ssmc.ChannelId = c.[Channel.Id]
		LEFT JOIN product.tProductSiteStructureRegistry AS pssr	ON p.[Product.Id] = pssr.ProductId	AND ssmc.SiteStructureRegistryId = pssr.SiteStructureRegistryId
		LEFT JOIN media.vCustomImage AS I ON I.[Image.MediaId] = p.[Product.MediaId] AND I.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageProductGetAllByIdList]'
GO

CREATE PROCEDURE [productlek].[pPackageProductGetAllByIdList]
	@ChannelId		INT,
	@CustomerId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[p].*,
		[pli].*
	FROM
		[product].[vCustomProductInclPackageProducts] p
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl 
			ON pl.Id = p.[Product.Id]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		p.[Product.ChannelId] = @ChannelId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProduct]'
GO
ALTER VIEW [product].[vCustomProduct]
AS 
	SELECT
		[p].*
	FROM
		[product].[vCustomProductInclPackageProducts] p
	WHERE
		[p].[Lekmer.SellOnlyInPackage] = 0
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSave]'
GO
ALTER PROCEDURE [lekmer].[pProductSave]
	@ProductId				INT,
	@ItemsInPackage			INT,
	@ErpId					VARCHAR(50),
	@EANCode				VARCHAR(20),
	@Title					NVARCHAR(256),
	@WebShopTitle			NVARCHAR(256),
	@ProductStatusId		INT,
	@Description			NVARCHAR(MAX),
	@ShortDescription		NVARCHAR(MAX),
	@CategoryId				INT,
	@MediaId				INT,
	@BrandId				INT,
	@AgeFromMonth			INT,
	@AgeToMonth				INT,
	@IsBookable				BIT,
	@IsNewFrom				DATETIME,
	@IsNewTo				DATETIME,
	@Measurement			NVARCHAR(MAX),
	@BatteryTypeId			INT,
	@NumberOfBatteries		INT,
	@IsBatteryIncluded		BIT,
	@ExpectedBackInStock	DATETIME,
	@CreatedDate			DATETIME,
	@SizeDeviationId		INT,
	@ShowVariantRelations	BIT,
	@MonitorThreshold		INT,
	@MaxQuantityPerOrder	INT,
	@SellOnlyInPackage		BIT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tProduct]
	SET
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	WHERE
		[ProductId] = @ProductId
		
	UPDATE
		[lekmer].[tLekmerProduct]
	SET
		[BrandId]				= @BrandId,
		[AgeFromMonth]			= @AgeFromMonth,
		[AgeToMonth]			= @AgeToMonth,
		[IsBookable]			= @IsBookable,
		[IsNewFrom]				= @IsNewFrom,
		[IsNewTo]				= @IsNewTo,
		[Measurement]			= @Measurement,
		[BatteryTypeId]			= @BatteryTypeId,
		[NumberOfBatteries]		= @NumberOfBatteries,
		[IsBatteryIncluded]		= @IsBatteryIncluded,
		[ExpectedBackInStock]	= @ExpectedBackInStock,
		[CreatedDate]			= @CreatedDate,
		[SizeDeviationId]		= @SizeDeviationId,
		[ShowVariantRelations]	= @ShowVariantRelations,
		[MonitorThreshold]		= @MonitorThreshold,
		[MaxQuantityPerOrder]	= @MaxQuantityPerOrder,
		[SellOnlyInPackage]		= @SellOnlyInPackage
	WHERE
		[ProductId] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAllWithoutStatusFilter]'
GO


ALTER PROCEDURE [lekmer].[pProductGetIdAllWithoutStatusFilter]
	@ChannelId INT,
	@CustomerId INT,
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	DECLARE @sqlCountFilter NVARCHAR(MAX)

	IF @CustomerId is not null
		BEGIN
			RAISERROR ('Current version doesn''t support CustomerId parameter.',10,1)
		END


	SELECT COUNT(*)
		FROM
			product.tProduct AS p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
			INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
			INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
		WHERE
			Ch.ChannelId = @ChannelId
			AND P.IsDeleted = 0
			AND [lp].[SellOnlyInPackage] = 0


	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY p.[ProductId]) AS Number,
			p.[ProductId]
		FROM
			product.tProduct AS p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
			INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
			INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
		WHERE
			Ch.ChannelId = @ChannelId
			AND P.IsDeleted = 0
			AND [lp].[SellOnlyInPackage] = 0
	) As Result
	WHERE Number > (@Page - 1) * @PageSize
		AND Number <= @Page * @PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [product].[vCustomProductViewInclPackageProducts]'
GO
CREATE VIEW [product].[vCustomProductViewInclPackageProducts]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.SellOnlyInPackage],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[product].[vProductView] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageProductGetViewAllByIdList]'
GO

CREATE PROCEDURE [productlek].[pPackageProductGetViewAllByIdList]
	@ChannelId		INT,
	@CustomerId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[p].*,
		[pli].*
	FROM
		[product].[vCustomProductViewInclPackageProducts] p
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl
			ON pl.Id = p.[Product.Id]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts]'
GO







CREATE VIEW [lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts]
AS 
	SELECT
		p.*,
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.SellOnlyInPackage],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.[ParentContentNodeId] AS 'Product.ParentContentNodeId',
		i.*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductWithoutStatusFilter] p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] AS ssmc ON ssmc.[ChannelId] = c.[Channel.Id]
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON p.[Product.Id] = pssr.[ProductId] AND ssmc.[SiteStructureRegistryId] = pssr.[SiteStructureRegistryId]
		LEFT JOIN [media].[vCustomImage] AS i ON i.[Image.MediaId] = p.[Product.MediaId] AND i.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]






GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [statistics].[pTopSellerGetAll]'
GO
ALTER procedure [statistics].[pTopSellerGetAll]
	@ChannelId int,
	@DateTimeFrom datetime,
	@DateTimeTo datetime,
	@TopCount int,
	@StatusIds		varchar(max)
as
begin
	declare @ProductIds table(ProductId int, Quantity int)
	
	insert
		@ProductIds
	(
		ProductId,
		Quantity
	)
	select top(@TopCount)
		p.ProductId,
		sum(oi.Quantity)
	from
		product.tProduct p
		inner join [order].tOrderItemProduct oip WITH(NOLOCK) on p.ProductId = oip.ProductId
		inner join [order].tOrderItem oi WITH(NOLOCK) on oi.OrderItemId = oip.OrderItemId
		inner join [order].tOrder o WITH(NOLOCK) on oi.OrderId = o.OrderId
	where
		o.ChannelId = @ChannelId
		and o.CreatedDate between isnull(@DateTimeFrom, o.CreatedDate) and isnull(@DateTimeTo, o.CreatedDate)
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
	group by
		p.ProductId
	order by
		sum(oi.Quantity) desc

	select
		p.*,
		pli.*,
		i.Quantity as 'TopSeller.Quantity'
	from
		@ProductIds as i 
		--inner join [product].[vCustomProduct] as p on p.[Product.Id] = i.ProductId
		--Need to include offline products
		inner join [lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts] as p on p.[Product.Id] = i.ProductId
		inner join [product].[vCustomPriceListItem] as pli
			on pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				null
			)
	where
		p.[Product.ChannelId] = @ChannelId
	order by
		i.Quantity DESC
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductSecure]'
GO




ALTER  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.IsAbove60L],
		[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.SellOnlyInPackage],
		'' AS 'Lekmer.UrlTitle',
		NULL AS 'Product.ParentContentNodeId',
		NULL AS 'Lekmer.RecommendedPrice'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[vPackage]'
GO

ALTER VIEW [productlek].[vPackage]
AS
	SELECT     
		[p].[PackageId] 'Package.PackageId',
		[p].[MasterProductId] 'Package.MasterProductId',
		COALESCE ([pt].[PackageTranslation.GeneralInfo], [p].[GeneralInfo]) 'Package.GeneralInfo',
		[ch].[ChannelId]
	FROM
		[productlek].[tPackage] p
		CROSS JOIN [core].[tChannel] ch
		LEFT JOIN [productlek].[vPackageTranslation] pt ON [pt].[PackageTranslation.PackageId] = [p].[PackageId]
			AND [pt].[PackageTranslation.LanguageId] = [ch].[LanguageId]
		
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductRecord]'
GO








ALTER VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.SellOnlyInPackage]
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [export].[pProductGetIdAllAvaliableForCdonExport]'
GO
ALTER PROCEDURE [export].[pProductGetIdAllAvaliableForCdonExport]
	@MonthPurchasedAgo INT
AS
BEGIN
	SELECT [p1].[ProductId]
	FROM [product].[tProduct] p1
		 INNER JOIN [lekmer].[tLekmerProduct] lp1 ON [lp1].[ProductId] = [p1].[ProductId]
	WHERE 
		[p1].[IsDeleted] = 0
		AND [lp1].[SellOnlyInPackage] = 0
		AND [p1].[ProductStatusId] = 0
		AND [lp1].[ProductTypeId] = 1
	UNION
	SELECT DISTINCT ([p2].[ProductId])
	FROM [product].[tProduct] p2
		 INNER JOIN [lekmer].[tLekmerProduct] lp2 ON [lp2].[ProductId] = [p2].[ProductId]
		 INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[ProductId] = [p2].[ProductId]
		 INNER JOIN [order].[tOrderItem] oi ON [oi].[OrderItemId] = [oip].[OrderItemId]
		 INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [oi].[OrderId]
	WHERE 
		[p2].[IsDeleted] = 0
		AND [lp2].[SellOnlyInPackage] = 0
		AND [p2].[ProductStatusId] = 1
		AND [o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > DATEADD(MONTH, -@MonthPurchasedAgo, GETDATE())
		AND [lp2].[ProductTypeId] = 1
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomProductView]'
GO
ALTER VIEW [product].[vCustomProductView]
AS
	SELECT
		[p].*
	FROM
		[product].[vCustomProductViewInclPackageProducts] p
	WHERE
		[p].[Lekmer.SellOnlyInPackage] = 0
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vFilterProduct]'
GO


ALTER VIEW [lekmer].[vFilterProduct]
AS
	SELECT
		p.[ProductId] 'FilterProduct.ProductId',
		p.[CategoryId] 'FilterProduct.CategoryId',
		COALESCE(pt.[Title], p.[Title]) 'FilterProduct.Title',
		lp.[BrandId] 'FilterProduct.BrandId',
		lp.[AgeFromMonth] 'FilterProduct.AgeFromMonth',
		lp.[AgeToMonth] 'FilterProduct.AgeToMonth',
		lp.[IsNewFrom] 'FilterProduct.IsNewFrom',
		ch.[ChannelId] 'FilterProduct.ChannelId',
		ch.[CurrencyId] 'FilterProduct.CurrencyId',
		pmc.[PriceListRegistryId] 'FilterProduct.PriceListRegistryId',
		lpp.[ProductPopularity.Popularity] AS 'FilterProduct.Popularity',
		CASE 
			WHEN EXISTS ( SELECT 1 FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId] )
			THEN (SELECT ISNULL(SUM(ps.[NumberInStock]), 0) FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId] AND ps.[NumberInStock] >= 0)
			ELSE p.[NumberInStock]
		END AS 'FilterProduct.NumberInStock',
		lp.[ProductTypeId] AS 'FilterProduct.ProductTypeId'
	FROM
		[product].[tProduct] p
		INNER JOIN [lekmer].[tLekmerProduct] lp ON p.[ProductId] = lp.[ProductId]

		INNER JOIN [product].[tProductRegistryProduct] AS prp ON p.[ProductId] = prp.[ProductId]
		INNER JOIN [product].[tProductModuleChannel] AS pmc ON prp.[ProductRegistryId] = pmc.[ProductRegistryId]
		INNER JOIN [core].[tChannel] AS ch ON pmc.[ChannelId] = ch.[ChannelId]

		LEFT JOIN [product].[tProductTranslation] AS pt ON p.[ProductId] = pt.[ProductId] AND ch.[LanguageId] = pt.[LanguageId]
		LEFT JOIN [lekmer].[vProductPopularity] lpp on pmc.[ChannelId] = lpp.[ProductPopularity.ChannelId] AND p.[ProductId] = lpp.[ProductPopularity.ProductId]

	WHERE
		p.[IsDeleted] = 0
		AND [lp].[SellOnlyInPackage] = 0
		AND p.[ProductStatusId] = 0


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductGetInclPackageProductsByIdWithoutStatusFilter]'
GO
CREATE PROCEDURE [productlek].[pProductGetInclPackageProductsByIdWithoutStatusFilter]
	@ProductId	INT,
	@ChannelId	INT,
	@CustomerId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		*
	FROM
		[lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts] AS p
		INNER JOIN [product].[vCustomPriceListItem] AS pli ON pli.[Price.ProductId] = p.[Product.Id]
	WHERE
		[Product.Id] = @ProductId
		AND [Product.ChannelId] = @ChannelId
		AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice]
			(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]'
GO
ALTER PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	SELECT
		[p].[ProductId] 'Product.Id', 
		[p].[ItemsInPackage] 'Product.ItemsInPackage',
		[p].[ErpId] 'Product.ErpId',
		[p].[EanCode] 'Product.EanCode',
		[p].[NumberInStock] 'Product.NumberInStock', 
		[p].[CategoryId] 'Product.CategoryId',
		COALESCE([pt].[WebShopTitle], [p].WebShopTitle) 'Product.WebShopTitle',
		COALESCE([pt].[Title], [p].Title) 'Product.Title',
		[p].[ProductStatusId] 'Product.ProductStatusId',
		COALESCE([pt].[Description], [p].[Description]) 'Product.Description',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription]) 'Product.ShortDescription',
		@ChannelId 'Product.ChannelId', 
		@CurrencyId 'Product.CurrencyId',
		@PriceListRegistryId 'Product.PriceListRegistryId',
		[i].*,
		[pssr].[ParentContentNodeId] 'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId] 'Product.TemplateContentNodeId',
		[lp].[Lekmer.BrandId],
		[lp].[Lekmer.IsBookable],
		[lp].[Lekmer.AgeFromMonth],
		[lp].[Lekmer.AgeToMonth],
		[lp].[Lekmer.IsNewFrom],
		[lp].[Lekmer.IsNewTo],
		COALESCE ([lpt].[Product.Measurement], [lp].[Lekmer.Measurement]) 'Lekmer.Measurement',
		[lp].[Lekmer.BatteryTypeId],
		[lp].[Lekmer.NumberOfBatteries],
		[lp].[Lekmer.IsBatteryIncluded],
		[lp].[Lekmer.ExpectedBackInStock],
		[lp].[Lekmer.CreatedDate],
		[lp].[Lekmer.SizeDeviationId],
		[lp].[Lekmer.HasSizes],
		[lp].[Lekmer.LekmerErpId],
		[lp].[Lekmer.ShowVariantRelations],
		[lp].[Lekmer.Weight],
		[lp].[Lekmer.ProductTypeId],
		[lp].[Lekmer.IsAbove60L],
		[lp].[Lekmer.StockStatusId],
		[lp].[Lekmer.MonitorThreshold],
		[lp].[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.SellOnlyInPackage],
		[bt].*,
		[sd].*,
		[pu].[ProductUrl.UrlTitle] 'Lekmer.UrlTitle',
		[rp].[Price] 'Lekmer.RecommendedPrice',
		[pli].*
	FROM
		[product].[tProduct] AS p
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON [lpt].[Product.Id] = [lp].[Lekmer.ProductId] AND [lpt].[Language.Id] = @LanguageId
		LEFT JOIN [lekmer].[vBatteryType] bt ON [bt].[BatteryType.Id] = [lp].[Lekmer.BatteryTypeId]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON [sd].[SizeDeviation.Id] = [lp].[Lekmer.SizeDeviationId]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@ProductIds, @Delimiter) AS pl	ON [pl].[Id] = [p].[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
		AND [lp].[Lekmer.SellOnlyInPackage] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pProductGetIdAllByPackageMasterProduct]'
GO
ALTER PROCEDURE [productlek].[pProductGetIdAllByPackageMasterProduct]
		@ChannelId		INT,
		@CustomerId		INT,
		@ProductId		INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @PackageId INT
	SET @PackageId = (SELECT [PackageId] FROM [productlek].[tPackage] WHERE [MasterProductId] = @ProductId)

	SELECT p.[Product.Id]
	FROM
		[productlek].[tPackageProduct] pp
		INNER JOIN [product].[vCustomProductInclPackageProducts] p ON pp.ProductId = [p].[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		pp.[PackageId] = @PackageId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductViewWithoutStatusFilter]'
GO
ALTER VIEW [lekmer].[vCustomProductViewWithoutStatusFilter]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS 'Lekmer.Measurement',
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.SellOnlyInPackage],
		bt.*,
		sd.*,
		pu.[ProductUrl.UrlTitle]  AS 'Lekmer.UrlTitle',
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductViewWithoutStatusFilter] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id]
		LEFT JOIN [lekmer].[vBatteryType] bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
	WHERE
		[lp].[Lekmer.SellOnlyInPackage] = 0


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vCustomProductWithoutStatusFilter]'
GO


ALTER VIEW [lekmer].[vCustomProductWithoutStatusFilter]
AS 
	SELECT
		[p].*
	FROM
		[lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts]  p
	WHERE
		[p].[Lekmer.SellOnlyInPackage] = 0

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductGetInclPackageProductsById]'
GO
CREATE PROCEDURE [productlek].[pProductGetInclPackageProductsById]
	@ProductId	INT,
	@CustomerId INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;	
	
	SELECT *		
	FROM 
		[product].[vCustomProductInclPackageProducts] AS p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		[Product.Id] = @ProductId
		AND [Product.ChannelId] = @ChannelId	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pProductGetViewInclPackageProductsById]'
GO
CREATE PROCEDURE [productlek].[pProductGetViewInclPackageProductsById]
	@ProductId	INT,
	@CustomerId INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;	
	
	SELECT *		
	FROM 
		[product].[vCustomProductViewInclPackageProducts] AS p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		p.[Product.Id] = @ProductId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAll]'
GO


ALTER PROCEDURE [lekmer].[pProductGetIdAll]
	@ChannelId INT,
	@CustomerId INT,
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	DECLARE @sqlCountFilter NVARCHAR(MAX)
	
	IF @CustomerId is not null
		BEGIN
			RAISERROR ('Current version doesn''t support CustomerId parameter.',10,1)
		END


	SELECT COUNT(*)
		FROM
			product.tProduct AS p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
			INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
			INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
		WHERE
			Ch.ChannelId = @ChannelId
			AND P.IsDeleted = 0
			AND [lp].[SellOnlyInPackage] = 0
			AND P.ProductStatusId = 0


	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY p.[ProductId]) AS Number,
			p.[ProductId]
		FROM
			product.tProduct AS p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
			INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
			INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
		WHERE
			Ch.ChannelId = @ChannelId
			AND P.IsDeleted = 0
			AND [lp].[SellOnlyInPackage] = 0
			AND P.ProductStatusId = 0
	) As Result
	WHERE Number > (@Page - 1) * @PageSize
		AND Number <= @Page * @PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAllWithoutAnyFilter]'
GO


ALTER PROCEDURE [lekmer].[pProductGetIdAllWithoutAnyFilter]
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	DECLARE @sqlCountFilter NVARCHAR(MAX)

	SELECT COUNT(*)
		FROM
			product.tProduct AS p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		WHERE
			P.IsDeleted = 0
			AND [lp].[SellOnlyInPackage] = 0

	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY p.[ProductId]) AS Number,
			p.[ProductId]
		FROM
			product.tProduct AS p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		WHERE
			P.IsDeleted = 0
			AND [lp].[SellOnlyInPackage] = 0
	) As Result
	WHERE Number > (@Page - 1) * @PageSize
		AND Number <= @Page * @PageSize
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageGetByMasterProductSecure]'
GO
CREATE PROCEDURE [productlek].[pPackageGetByMasterProductSecure]
	@MasterProductId	INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;	
	
	DECLARE @CurrencyId INT, @PriceListRegistryId INT, @PriceListId INT

	SELECT @CurrencyId = [CurrencyId], @PriceListRegistryId = [PriceListRegistryId]
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@MasterProductId, @ChannelId)
	
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @MasterProductId, @PriceListRegistryId, NULL))
	
	SELECT
		*
	FROM
		[productlek].[vPackageSecure] p 
		LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[Package.MasterProductId]
														  AND [pli].[Price.PriceListId] = @PriceListId
	WHERE
		[p].[Package.MasterProductId] = @MasterProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageMasterProductIdsGetByPackageProduct]'
GO
CREATE PROCEDURE [productlek].[pPackageMasterProductIdsGetByPackageProduct]
	@ProductId	INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	     [Package.MasterProductId]
	FROM
	    [productlek].[vPackage]
	WHERE
		[ChannelId] = @ChannelId
		AND [Package.PackageId] IN (
			SELECT [PackageId] FROM [productlek].[tPackageProduct] WHERE [ProductId] = @ProductId
		)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
