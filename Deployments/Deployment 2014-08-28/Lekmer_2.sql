-- tBlockProductList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,10000,NULL
FROM [product].[tBlockProductList]

UPDATE [sitestructurelek].[tBlockSetting]
SET [sitestructurelek].[tBlockSetting].[ColumnCountMobile] = [lekmer].[tBlockProductList].[ColumnCountMobile],
    [sitestructurelek].[tBlockSetting].[RowCountMobile] = [lekmer].[tBlockProductList].[RowCountMobile]
FROM [sitestructurelek].[tBlockSetting]
INNER JOIN [lekmer].[tBlockProductList] ON [sitestructurelek].[tBlockSetting].[BlockId] = [lekmer].[tBlockProductList].[BlockId]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockProductImageList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,100,NULL
FROM [product].[tBlockProductImageList]

INSERT INTO [productlek].[tBlockProductImageList] ([BlockId], [ProductImageGroupId])
SELECT [BlockId], [ProductImageGroupId]
FROM [product].[tBlockProductImageList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockCategoryProductList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,100,NULL
FROM [product].[tBlockCategoryProductList]

INSERT INTO [productlek].[tBlockCategoryProductList] ([BlockId],[ProductSortOrderId])
SELECT [BlockId], [ProductSortOrderId]
FROM [product].[tBlockCategoryProductList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockProductRelationList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,100,NULL
FROM [product].[tBlockProductRelationList]

INSERT INTO [productlek].[tBlockProductRelationList] ([BlockId], [ProductSortOrderId]) 
SELECT [BlockId], [ProductSortOrderId]
FROM [product].[tBlockProductRelationList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockProductSearchResult
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,20,NULL
FROM [product].[tBlockProductSearchResult]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockBestRatedProductList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,[NumberOfItems],NULL
FROM [review].[tBlockBestRatedProductList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockPackageProductList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,10,NULL
FROM [productlek].[tBlockPackageProductList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockSharedWishList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,1000,NULL
FROM [lekmer].[tBlockSharedWishList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockReviewMyWishList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,1000,NULL
FROM [lekmer].[tBlockReviewMyWishList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockProductSimilarList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,10000,NULL
FROM [lekmer].[tBlockProductSimilarList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockEsalesRecommend
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,10000,NULL
FROM [lekmer].[tBlockEsalesRecommend]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockEsalesAds
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,10000,NULL
FROM [lekmer].[tBlockEsalesAds]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockBrandTopList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,[TotalBrandCount],NULL
FROM [lekmer].[tBlockBrandTopList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockBrandProductList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,10000,NULL
FROM [lekmer].[tBlockBrandProductList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockBrandList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,10000,NULL
FROM [lekmer].[tBlockBrandList]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockAvailCombine
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,100,NULL
FROM [lekmer].[tBlockAvailCombine]
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tBlockTopList
INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
SELECT [BlockId],[ColumnCount],NULL,[RowCount],NULL,[TotalProductCount],NULL
FROM [addon].[tBlockTopList]