SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [sitestructurelek]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tBlockProductImageList]'
GO
CREATE TABLE [productlek].[tBlockProductImageList]
(
[BlockId] [int] NOT NULL,
[ProductImageGroupId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockProductImageList] on [productlek].[tBlockProductImageList]'
GO
ALTER TABLE [productlek].[tBlockProductImageList] ADD CONSTRAINT [PK_tBlockProductImageList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tBlockProductImageList_ProductImageGroupId] on [productlek].[tBlockProductImageList]'
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductImageList_ProductImageGroupId] ON [productlek].[tBlockProductImageList] ([ProductImageGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tBlockProductRelationList]'
GO
CREATE TABLE [productlek].[tBlockProductRelationList]
(
[BlockId] [int] NOT NULL,
[ProductSortOrderId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockProductRelationList] on [productlek].[tBlockProductRelationList]'
GO
ALTER TABLE [productlek].[tBlockProductRelationList] ADD CONSTRAINT [PK_tBlockProductRelationList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tBlockProductRelationList_ProductSortOrderId] on [productlek].[tBlockProductRelationList]'
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductRelationList_ProductSortOrderId] ON [productlek].[tBlockProductRelationList] ([ProductSortOrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[tBlockCategoryProductList]'
GO
CREATE TABLE [productlek].[tBlockCategoryProductList]
(
[BlockId] [int] NOT NULL,
[ProductSortOrderId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockCategoryProductList] on [productlek].[tBlockCategoryProductList]'
GO
ALTER TABLE [productlek].[tBlockCategoryProductList] ADD CONSTRAINT [PK_tBlockCategoryProductList] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [sitestructurelek].[tBlockSetting]'
GO
CREATE TABLE [sitestructurelek].[tBlockSetting]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[ColumnCountMobile] [int] NULL,
[RowCount] [int] NOT NULL,
[RowCountMobile] [int] NULL,
[TotalItemCount] [int] NOT NULL,
[TotalItemCountMobile] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tBlockSetting] on [sitestructurelek].[tBlockSetting]'
GO
ALTER TABLE [sitestructurelek].[tBlockSetting] ADD CONSTRAINT [PK_tBlockSetting] PRIMARY KEY CLUSTERED  ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [sitestructurelek].[vBlockSetting]'
GO


CREATE VIEW [sitestructurelek].[vBlockSetting]
AS
SELECT   
	   [bs].[BlockId]				'BlockSetting.BlockId'
      ,[bs].[ColumnCount]			'BlockSetting.ColumnCount'
      ,[bs].[ColumnCountMobile]		'BlockSetting.ColumnCountMobile'
      ,[bs].[RowCount]				'BlockSetting.RowCount'
      ,[bs].[RowCountMobile]		'BlockSetting.RowCountMobile'
      ,[bs].[TotalItemCount]		'BlockSetting.TotalItemCount'
      ,[bs].[TotalItemCountMobile]	'BlockSetting.TotalItemCountMobile'
FROM 
	[sitestructurelek].[tBlockSetting] AS bs


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [sitestructurelek].[pBlockSettingSave]'
GO
CREATE PROCEDURE [sitestructurelek].[pBlockSettingSave]
	@BlockId				INT,
	@ColumnCount			INT,
	@ColumnCountMobile		INT = NULL,
	@RowCount				INT,
	@RowCountMobile			INT = NULL,
	@TotalItemCount			INT,
	@TotalItemCountMobile	INT = NULL
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[sitestructurelek].[tBlockSetting]
	SET	
		 [ColumnCount]			= @ColumnCount
		,[ColumnCountMobile]	= @ColumnCountMobile
		,[RowCount]				= @RowCount
		,[RowCountMobile]		= @RowCountMobile
		,[TotalItemCount]		= @TotalItemCount
		,[TotalItemCountMobile]	= @TotalItemCountMobile
	WHERE
		[BlockId] = @BlockId
  
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[sitestructurelek].[tBlockSetting] (
			 [BlockId]
			,[ColumnCount]
			,[ColumnCountMobile]
			,[RowCount]
			,[RowCountMobile]
			,[TotalItemCount]
			,[TotalItemCountMobile]
		)
		VALUES (
			 @BlockId
			,@ColumnCount
			,@ColumnCountMobile
			,@RowCount
			,@RowCountMobile
			,@TotalItemCount
			,@TotalItemCountMobile
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductListGetByIdSecure]'
GO
CREATE PROCEDURE [productlek].[pBlockProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT
		[b].*,
		[bs].*
	FROM
		[sitestructure].[vCustomBlockSecure] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vBlockProductRelationList]'
GO
CREATE VIEW [productlek].[vBlockProductRelationList]
AS
	SELECT 
		[bprl].[BlockId] 'BlockProductRelationList.BlockId',
		[pso].*,
		[bs].*
	FROM 
		[productlek].[tBlockProductRelationList] bprl
		INNER JOIN product.vCustomProductSortOrder pso ON [pso].[ProductSortOrder.Id] = [bprl].[ProductSortOrderId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bprl].[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductRelationListGetByIdSecure]'
GO
CREATE PROCEDURE [productlek].[pBlockProductRelationListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT
		[bprl].*,
		[b].*
	FROM
		[productlek].[vBlockProductRelationList] bprl
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON [bprl].[BlockProductRelationList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bprl].[BlockProductRelationList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vBlockProductImageList]'
GO
CREATE VIEW [productlek].[vBlockProductImageList]
AS
	SELECT 
		[bpil].[BlockId] 'BlockProductImageList.BlockId',
		[bpil].[ProductImageGroupId] 'BlockProductImageList.ProductImageGroupId',
		[bs].*
	FROM 
		[productlek].[tBlockProductImageList] bpil
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bpil].[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductImageListGetByIdSecure]'
GO
CREATE PROCEDURE [productlek].[pBlockProductImageListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT
		[bpil].*,
		[b].*
	FROM
		[productlek].[vBlockProductImageList] bpil
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON [bpil].[BlockProductImageList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bpil].[BlockProductImageList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [sitestructurelek].[pBlockSettingDelete]'
GO
CREATE PROCEDURE [sitestructurelek].[pBlockSettingDelete]
	@BlockId INT
AS
BEGIN
	DELETE [sitestructurelek].[tBlockSetting]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[vBlockCategoryProductList]'
GO
CREATE VIEW [productlek].[vBlockCategoryProductList]
AS
SELECT 
	[bcpl].[BlockId] AS 'BlockCategoryProductList.BlockId',
	[bcpl].[ProductSortOrderId] AS 'BlockCategoryProductList.ProductSortOrderId',
    [pso].*,
    [bs].*
  FROM [productlek].[tBlockCategoryProductList] bcpl
  INNER JOIN [product].[vCustomProductSortOrder] pso ON [pso].[ProductSortOrder.Id] = [bcpl].[ProductSortOrderId]
  INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bcpl].[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockCategoryProductListSave]'
GO
CREATE PROCEDURE [productlek].[pBlockCategoryProductListSave]
	@BlockId			INT,
	@ProductSortOrderId	INT
AS
BEGIN
	UPDATE
		[productlek].[tBlockCategoryProductList]
	SET
		[ProductSortOrderId] = @ProductSortOrderId
	WHERE
		[BlockId] = @BlockId
			
	IF  @@ROWCOUNT = 0 
	BEGIN
		INSERT [productlek].[tBlockCategoryProductList] (
			[BlockId],
			[ProductSortOrderId]
		)
		VALUES (
			@BlockId,
			@ProductSortOrderId
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductImageListGetById]'
GO
CREATE PROCEDURE [productlek].[pBlockProductImageListGetById]
	@LanguageId	INT,
	@BlockId	INT
AS
BEGIN
	SELECT
		[bpil].*,
		[b].*
	FROM 
		[productlek].[vBlockProductImageList] bpil
		INNER JOIN [sitestructure].[vCustomBlock] b ON [bpil].[BlockProductImageList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bpil].[BlockProductImageList.BlockId] = @BlockId 
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductImageListSave]'
GO
CREATE PROCEDURE [productlek].[pBlockProductImageListSave]
	@BlockId				INT,
	@ProductImageGroupId	INT
AS
BEGIN
	UPDATE
		[productlek].[tBlockProductImageList]
	SET
		[ProductImageGroupId] = @ProductImageGroupId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT = 0
		BEGIN
			INSERT [productlek].[tBlockProductImageList] (
				[BlockId],
				[ProductImageGroupId]
			)
			VALUES (
				@BlockId,
				@ProductImageGroupId
			)
		END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockCategoryProductListDelete]'
GO
CREATE PROCEDURE [productlek].[pBlockCategoryProductListDelete]
	@BlockId	INT
AS
BEGIN
	DELETE [product].[tBlockCategoryProductListCategory]
	WHERE [BlockId] = @BlockId

	DELETE [productlek].[tBlockCategoryProductList]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchResultGetById]'
GO
CREATE PROCEDURE [productlek].[pBlockProductSearchResultGetById]
    @LanguageId INT,
	@BlockId	INT
AS BEGIN
	SELECT
		[b].*,
		[bs].*
	FROM
		[sitestructure].[vCustomBlock] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId 
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductImageListDelete]'
GO
CREATE PROCEDURE [productlek].[pBlockProductImageListDelete]
	@BlockId	INT
AS
BEGIN
	DELETE FROM [productlek].[tBlockProductImageList]
	WHERE BlockId = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductRelationListSave]'
GO
CREATE PROCEDURE [productlek].[pBlockProductRelationListSave]
	@BlockId			INT,
	@ProductSortOrderId	INT
AS
BEGIN
	UPDATE
		[productlek].[tBlockProductRelationList]
	SET
		[ProductSortOrderId] = @ProductSortOrderId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT [productlek].[tBlockProductRelationList] (
			[BlockId],
			[ProductSortOrderId]
		)
		VALUES (
			@BlockId,
			@ProductSortOrderId
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockCategoryProductListGetById]'
GO
CREATE PROCEDURE [productlek].[pBlockCategoryProductListGetById]
	@LanguageId	INT,
	@BlockId	INT
AS
BEGIN
	SELECT
		[bcpl].*,
		[b].*
	FROM 
		[productlek].[vBlockCategoryProductList] bcpl
		INNER JOIN [sitestructure].[vCustomBlock] b ON [bcpl].[BlockCategoryProductList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bcpl].[BlockCategoryProductList.BlockId] = @BlockId 
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockCategoryProductListGetByIdSecure]'
GO
CREATE PROCEDURE [productlek].[pBlockCategoryProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT
		[bcpl].*,
		[b].*
	FROM 
		[productlek].[vBlockCategoryProductList] bcpl
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON [bcpl].[BlockCategoryProductList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bcpl].[BlockCategoryProductList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductRelationListGetById]'
GO
CREATE PROCEDURE [productlek].[pBlockProductRelationListGetById]
	@LanguageId	INT,
	@BlockId	INT
AS
BEGIN
	SELECT
		[bprl].*,
		[b].*
	FROM 
		[productlek].[vBlockProductRelationList] bprl
		INNER JOIN [sitestructure].[vCustomBlock] b ON [bprl].[BlockProductRelationList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bprl].[BlockProductRelationList.BlockId] = @BlockId 
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductListGetById]'
GO
CREATE PROCEDURE [productlek].[pBlockProductListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT
		[b].*,
		[bs].*
	FROM
		[sitestructure].[vCustomBlock] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId 
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductRelationListDelete]'
GO
CREATE PROCEDURE [productlek].[pBlockProductRelationListDelete]
	@BlockId	INT
AS
BEGIN
	DELETE FROM [productlek].[tBlockProductRelationList]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pBlockProductSearchResultGetByIdSecure]'
GO
CREATE PROCEDURE [productlek].[pBlockProductSearchResultGetByIdSecure]
	@BlockId	INT
AS BEGIN
	SELECT
		[b].*,
		[bs].*
	FROM
		[sitestructure].[vCustomBlockSecure] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tBlockProductImageList]'
GO
ALTER TABLE [productlek].[tBlockProductImageList] ADD CONSTRAINT [FK_tBlockProductImageList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
ALTER TABLE [productlek].[tBlockProductImageList] ADD CONSTRAINT [FK_tBlockProductImageList_tProductImageGroup] FOREIGN KEY ([ProductImageGroupId]) REFERENCES [product].[tProductImageGroup] ([ProductImageGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tBlockProductRelationList]'
GO
ALTER TABLE [productlek].[tBlockProductRelationList] ADD CONSTRAINT [FK_tBlockProductRelationList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
ALTER TABLE [productlek].[tBlockProductRelationList] ADD CONSTRAINT [FK_tBlockProductRelationList_tProductSortOrder] FOREIGN KEY ([ProductSortOrderId]) REFERENCES [product].[tProductSortOrder] ([ProductSortOrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [product].[tBlockProductRelationListItem]'
GO
--ALTER TABLE [product].[tBlockProductRelationListItem] ADD CONSTRAINT [FK_tBlockProductRelationListItem_tBlockProductRelationList] FOREIGN KEY ([BlockId]) REFERENCES [productlek].[tBlockProductRelationList] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [sitestructurelek].[tBlockSetting]'
GO
ALTER TABLE [sitestructurelek].[tBlockSetting] ADD CONSTRAINT [FK_tBlockSetting_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [product].[tBlockCategoryProductListCategory]'
GO
--ALTER TABLE [product].[tBlockCategoryProductListCategory] ADD CONSTRAINT [FK_tBlockCategoryProductListCategory_tBlockCategoryProductList] FOREIGN KEY ([BlockId]) REFERENCES [productlek].[tBlockCategoryProductList] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [productlek].[tBlockCategoryProductList]'
GO
ALTER TABLE [productlek].[tBlockCategoryProductList] ADD CONSTRAINT [FK_tBlockCategoryProductList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
ALTER TABLE [productlek].[tBlockCategoryProductList] ADD CONSTRAINT [FK_tBlockCategoryProductList_tProductSortOrder] FOREIGN KEY ([ProductSortOrderId]) REFERENCES [product].[tProductSortOrder] ([ProductSortOrderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
