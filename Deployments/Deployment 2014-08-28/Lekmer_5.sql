ALTER TABLE [product].[tBlockProductRelationListItem] ADD CONSTRAINT [FK_tBlockProductRelationListItem_tBlockProductRelationList] FOREIGN KEY ([BlockId]) REFERENCES [productlek].[tBlockProductRelationList] ([BlockId])
ALTER TABLE [product].[tBlockCategoryProductListCategory] ADD CONSTRAINT [FK_tBlockCategoryProductListCategory_tBlockCategoryProductList] FOREIGN KEY ([BlockId]) REFERENCES [productlek].[tBlockCategoryProductList] ([BlockId])

EXEC [dev].[pRefreshAllViews]