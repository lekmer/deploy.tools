SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [product].[tBlockProductListProduct]'
GO
ALTER TABLE [product].[tBlockProductListProduct] DROP CONSTRAINT [FK_tBlockProductListProduct_tBlockProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [product].[tBlockCategoryProductListCategory]'
GO
ALTER TABLE [product].[tBlockCategoryProductListCategory] DROP CONSTRAINT [FK_tBlockCategoryProductListCategory_tBlockCategoryProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [product].[tBlockProductRelationListItem]'
GO
ALTER TABLE [product].[tBlockProductRelationListItem] DROP CONSTRAINT [FK_tBlockProductRelationListItem_tBlockProductRelationList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [product].[tBlockProductList]'
GO
ALTER TABLE [product].[tBlockProductList] DROP CONSTRAINT [FK_tBlockProductList_tBlock]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tBlockProductList]'
GO
ALTER TABLE [lekmer].[tBlockProductList] DROP CONSTRAINT [FK_tBlockProductList_tBlockProductList1]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tBlockReviewMyWishList]'
GO
ALTER TABLE [lekmer].[tBlockReviewMyWishList] DROP CONSTRAINT [FK_tBlockReviewMyWishList_tBlock]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [product].[tBlockCategoryProductList]'
GO
ALTER TABLE [product].[tBlockCategoryProductList] DROP CONSTRAINT [FK_tBlockCategoryProductList_tBlock]
ALTER TABLE [product].[tBlockCategoryProductList] DROP CONSTRAINT [FK_tBlockCategoryProductList_tProductSortOrder]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [product].[tBlockProductSearchResult]'
GO
ALTER TABLE [product].[tBlockProductSearchResult] DROP CONSTRAINT [FK_tBlockProductSearchResult_tBlock]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [product].[tBlockProductRelationList]'
GO
ALTER TABLE [product].[tBlockProductRelationList] DROP CONSTRAINT [FK_tBlockProductRelationList_tBlock]
ALTER TABLE [product].[tBlockProductRelationList] DROP CONSTRAINT [FK_tBlockProductRelationList_tProductSortOrder]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tBlockSharedWishList]'
GO
ALTER TABLE [lekmer].[tBlockSharedWishList] DROP CONSTRAINT [FK_tBlockSharedWishList_tBlock]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [product].[tBlockProductImageList]'
GO
ALTER TABLE [product].[tBlockProductImageList] DROP CONSTRAINT [FK_tBlockProductImageList_tBlock]
ALTER TABLE [product].[tBlockProductImageList] DROP CONSTRAINT [FK_tBlockProductImageList_tProductImageGroup]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [productlek].[tBlockPackageProductList]'
GO
ALTER TABLE [productlek].[tBlockPackageProductList] DROP CONSTRAINT [FK_tBlockPackageProductList_tBlock]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [product].[tBlockProductList]'
GO
ALTER TABLE [product].[tBlockProductList] DROP CONSTRAINT [PK_tBlockProductList_1]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tBlockReviewMyWishList]'
GO
ALTER TABLE [lekmer].[tBlockReviewMyWishList] DROP CONSTRAINT [PK_tBlockReviewMyWishList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tBlockProductList]'
GO
ALTER TABLE [lekmer].[tBlockProductList] DROP CONSTRAINT [PK_tBlockProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [product].[tBlockCategoryProductList]'
GO
ALTER TABLE [product].[tBlockCategoryProductList] DROP CONSTRAINT [PK_tBlockCategoryProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [product].[tBlockProductSearchResult]'
GO
ALTER TABLE [product].[tBlockProductSearchResult] DROP CONSTRAINT [PK_tBlockProductSearchResult]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [product].[tBlockProductRelationList]'
GO
ALTER TABLE [product].[tBlockProductRelationList] DROP CONSTRAINT [PK_tBlockProductRelationList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tBlockSharedWishList]'
GO
ALTER TABLE [lekmer].[tBlockSharedWishList] DROP CONSTRAINT [PK_tBlockSharedWishList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [product].[tBlockProductImageList]'
GO
ALTER TABLE [product].[tBlockProductImageList] DROP CONSTRAINT [PK_tBlockProductImageList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [productlek].[tBlockPackageProductList]'
GO
ALTER TABLE [productlek].[tBlockPackageProductList] DROP CONSTRAINT [PK_tBlockPackageProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tBlockProductRelationList_ProductSortOrderId] from [product].[tBlockProductRelationList]'
GO
DROP INDEX [IX_tBlockProductRelationList_ProductSortOrderId] ON [product].[tBlockProductRelationList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_tBlockProductImageList_ProductImageGroupId] from [product].[tBlockProductImageList]'
GO
DROP INDEX [IX_tBlockProductImageList_ProductImageGroupId] ON [product].[tBlockProductImageList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductSearchResultGetById]'
GO
DROP PROCEDURE [product].[pBlockProductSearchResultGetById]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pBlockSharedWishListDelete]'
GO
DROP PROCEDURE [lekmer].[pBlockSharedWishListDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductListDelete]'
GO
DROP PROCEDURE [product].[pBlockProductListDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductSearchResultGetByIdSecure]'
GO
DROP PROCEDURE [product].[pBlockProductSearchResultGetByIdSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductImageListGetById]'
GO
DROP PROCEDURE [product].[pBlockProductImageListGetById]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductSearchResultDelete]'
GO
DROP PROCEDURE [product].[pBlockProductSearchResultDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vCustomBlockProductSearchResult]'
GO
DROP VIEW [product].[vCustomBlockProductSearchResult]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vBlockProductSearchResult]'
GO
DROP VIEW [product].[vBlockProductSearchResult]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductListGetById]'
GO
DROP PROCEDURE [product].[pBlockProductListGetById]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductRelationListGetByIdSecure]'
GO
DROP PROCEDURE [product].[pBlockProductRelationListGetByIdSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductRelationListSave]'
GO
DROP PROCEDURE [product].[pBlockProductRelationListSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockCategoryProductListSave]'
GO
DROP PROCEDURE [product].[pBlockCategoryProductListSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockCategoryProductListGetById]'
GO
DROP PROCEDURE [product].[pBlockCategoryProductListGetById]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [productlek].[pBlockPackageProductListSave]'
GO
DROP PROCEDURE [productlek].[pBlockPackageProductListSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [productlek].[fnGetProductsWithoutAnyFilter]'
GO
DROP FUNCTION [productlek].[fnGetProductsWithoutAnyFilter]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [productlek].[pBlockPackageProductListDelete]'
GO
DROP PROCEDURE [productlek].[pBlockPackageProductListDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductImageListSave]'
GO
DROP PROCEDURE [product].[pBlockProductImageListSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pBlockReviewMyWishListSave]'
GO
DROP PROCEDURE [lekmer].[pBlockReviewMyWishListSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductSearchResultSave]'
GO
DROP PROCEDURE [product].[pBlockProductSearchResultSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pBlockReviewMyWishListDelete]'
GO
DROP PROCEDURE [lekmer].[pBlockReviewMyWishListDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductImageListGetByIdSecure]'
GO
DROP PROCEDURE [product].[pBlockProductImageListGetByIdSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vCustomBlockProductImageList]'
GO
DROP VIEW [product].[vCustomBlockProductImageList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pBlockSharedWishListSave]'
GO
DROP PROCEDURE [lekmer].[pBlockSharedWishListSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockCategoryProductListDelete]'
GO
DROP PROCEDURE [product].[pBlockCategoryProductListDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductListSave]'
GO
DROP PROCEDURE [product].[pBlockProductListSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [productlek].[vBlockPackageProductList]'
GO
DROP VIEW [productlek].[vBlockPackageProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductRelationListGetById]'
GO
DROP PROCEDURE [product].[pBlockProductRelationListGetById]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vCustomBlockProductRelationList]'
GO
DROP VIEW [product].[vCustomBlockProductRelationList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vBlockProductRelationList]'
GO
DROP VIEW [product].[vBlockProductRelationList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductImageListDelete]'
GO
DROP PROCEDURE [product].[pBlockProductImageListDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockCategoryProductListGetByIdSecure]'
GO
DROP PROCEDURE [product].[pBlockCategoryProductListGetByIdSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vCustomBlockCategoryProductList]'
GO
DROP VIEW [product].[vCustomBlockCategoryProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vBlockCategoryProductList]'
GO
DROP VIEW [product].[vBlockCategoryProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vBlockProductImageList]'
GO
DROP VIEW [product].[vBlockProductImageList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductListGetByIdSecure]'
GO
DROP PROCEDURE [product].[pBlockProductListGetByIdSecure]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vCustomBlockProductList]'
GO
DROP VIEW [product].[vCustomBlockProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[vBlockProductList]'
GO
DROP VIEW [product].[vBlockProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pBlockProductListSave]'
GO
DROP PROCEDURE [lekmer].[pBlockProductListSave]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[pBlockProductRelationListDelete]'
GO
DROP PROCEDURE [product].[pBlockProductRelationListDelete]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [productlek].[tBlockPackageProductList]'
GO
DROP TABLE [productlek].[tBlockPackageProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[tBlockProductImageList]'
GO
DROP TABLE [product].[tBlockProductImageList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[tBlockSharedWishList]'
GO
DROP TABLE [lekmer].[tBlockSharedWishList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[tBlockProductRelationList]'
GO
DROP TABLE [product].[tBlockProductRelationList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[tBlockProductSearchResult]'
GO
DROP TABLE [product].[tBlockProductSearchResult]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[tBlockProductList]'
GO
DROP TABLE [lekmer].[tBlockProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[tBlockReviewMyWishList]'
GO
DROP TABLE [lekmer].[tBlockReviewMyWishList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[tBlockCategoryProductList]'
GO
DROP TABLE [product].[tBlockCategoryProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [product].[tBlockProductList]'
GO
DROP TABLE [product].[tBlockProductList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockProductSimilarList]'
GO
ALTER TABLE [lekmer].[tBlockProductSimilarList] DROP
COLUMN [ColumnCount],
COLUMN [RowCount],
COLUMN [TotalProductCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[tBlockTopList]'
GO
ALTER TABLE [addon].[tBlockTopList] DROP
COLUMN [ColumnCount],
COLUMN [RowCount],
COLUMN [TotalProductCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockEsalesRecommend]'
GO
ALTER TABLE [lekmer].[tBlockEsalesRecommend] DROP
COLUMN [ColumnCount],
COLUMN [RowCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[tBlockBestRatedProductList]'
GO
ALTER TABLE [review].[tBlockBestRatedProductList] DROP
COLUMN [ColumnCount],
COLUMN [RowCount],
COLUMN [NumberOfItems]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tLekmerBlock]'
GO
ALTER TABLE [lekmer].[tLekmerBlock] ADD
[CanBeUsedAsFallbackOption] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockAvailCombine]'
GO
ALTER TABLE [lekmer].[tBlockAvailCombine] DROP
COLUMN [ColumnCount],
COLUMN [RowCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockBrandList]'
GO
ALTER TABLE [lekmer].[tBlockBrandList] DROP
COLUMN [ColumnCount],
COLUMN [RowCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockBrandTopList]'
GO
ALTER TABLE [lekmer].[tBlockBrandTopList] DROP
COLUMN [ColumnCount],
COLUMN [RowCount],
COLUMN [TotalBrandCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockBrandProductList]'
GO
ALTER TABLE [lekmer].[tBlockBrandProductList] DROP
COLUMN [ColumnCount],
COLUMN [RowCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[tBlockEsalesAds]'
GO
ALTER TABLE [lekmer].[tBlockEsalesAds] DROP
COLUMN [ColumnCount],
COLUMN [RowCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[vCustomBlock]'
GO
ALTER VIEW [sitestructure].[vCustomBlock]
AS
SELECT
	[b].*,
	[l].[StartDate] AS 'Block.StartDate',
	[l].[EndDate] AS 'Block.EndDate',
	[l].[StartDailyIntervalMinutes] AS 'Block.StartDailyIntervalMinutes',
	[l].[EndDailyIntervalMinutes] AS 'Block.EndDailyIntervalMinutes',
	[l].[ShowOnDesktop] AS 'Block.ShowOnDesktop',
	[l].[ShowOnMobile] AS 'Block.ShowOnMobile',
	[l].[CanBeUsedAsFallbackOption] AS 'Block.CanBeUsedAsFallbackOption'
FROM
	[sitestructure].[vBlock] b
	LEFT JOIN [lekmer].[tLekmerBlock] l ON b.[Block.BlockId] = l.[BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockSharedWishListGetById]'
GO
ALTER PROCEDURE [lekmer].[pBlockSharedWishListGetById]
	@BlockId INT,
	@LanguageId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[b].*,
		[bs].*
	FROM
		[sitestructure].[vCustomBlock] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[vCustomBlockSecure]'
GO
ALTER VIEW [sitestructure].[vCustomBlockSecure]
AS
SELECT
	[b].*,
	[l].[StartDate] AS 'Block.StartDate',
	[l].[EndDate] AS 'Block.EndDate',
	[l].[StartDailyIntervalMinutes] AS 'Block.StartDailyIntervalMinutes',
	[l].[EndDailyIntervalMinutes] AS 'Block.EndDailyIntervalMinutes',
	[l].[ShowOnDesktop] AS 'Block.ShowOnDesktop',
	[l].[ShowOnMobile] AS 'Block.ShowOnMobile',
	[l].[CanBeUsedAsFallbackOption] AS 'Block.CanBeUsedAsFallbackOption'
FROM
	[sitestructure].[vBlockSecure] b
	LEFT JOIN [lekmer].[tLekmerBlock] l ON b.[Block.BlockId] = l.[BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockAvailCombineGetById]'
GO
ALTER PROCEDURE [lekmer].[pBlockAvailCombineGetById]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		*
	FROM 
		[lekmer].[tBlockAvailCombine] bac
		INNER JOIN [sitestructure].[vCustomBlock] b ON [bac].[BlockId] = [b].[Block.BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE 
		[bac].[BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockBrandTopListSave]'
GO
ALTER PROCEDURE [lekmer].[pBlockBrandTopListSave]
	@BlockId INT,
	@IncludeAllCategories BIT,
	@OrderStatisticsDayCount INT,
	@LinkContentNodeId INT
AS
BEGIN
	UPDATE lekmer.tBlockBrandTopList
	SET
		[IncludeAllCategories] = @IncludeAllCategories,
		[OrderStatisticsDayCount] = @OrderStatisticsDayCount,
		[LinkContentNodeId] = @LinkContentNodeId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0 RETURN
		
	INSERT lekmer.tBlockBrandTopList (
		[BlockId],
		[IncludeAllCategories],
		[OrderStatisticsDayCount],
		[LinkContentNodeId]
	)
	VALUES (
		@BlockId,
		@IncludeAllCategories,
		@OrderStatisticsDayCount,
		@LinkContentNodeId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockProductSimilarList]'
GO



ALTER VIEW [lekmer].[vBlockProductSimilarList]
AS
	SELECT
		[BlockId] as 'BlockProductSimilarList.BlockId',
		[PriceRangeType] as 'BlockProductSimilarList.PriceRangeType'
	FROM
		[lekmer].[tBlockProductSimilarList]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO


EXECUTE sp_refreshview N'[lekmer].[vBlockProductSimilarList]';
EXECUTE sp_refreshview N'[lekmer].[vCustomBlockProductSimilarList]';


IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockProductSimilarListGetById]'
GO

ALTER PROCEDURE [lekmer].[pBlockProductSimilarListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		[psl].*,
		[b].*,
		[bs].*
	FROM 
		[lekmer].[vCustomBlockProductSimilarList] AS psl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON psl.[BlockProductSimilarList.BlockId] = b.[Block.BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		psl.[BlockProductSimilarList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[tDropShip]'
GO
ALTER TABLE [orderlek].[tDropShip] ADD
[CsvFileName] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[PdfFileName] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[vDropShip]'
GO




ALTER VIEW [orderlek].[vDropShip]
AS
SELECT
	[ds].[DropShipId] 'DropShip.Id',
	[ds].[ExternalOrderId] 'DropShip.ExternalOrderId',
	[ds].[OfferId] 'DropShip.OfferId',
	[ds].[Email] 'DropShip.Email',
	[ds].[Phone] 'DropShip.Phone',
	[ds].[SocialSecurityNumber] 'DropShip.SocialSecurityNumber',
	[ds].[BillingFirstName] 'DropShip.BillingFirstName',
	[ds].[BillingLastName] 'DropShip.BillingLastName',
	[ds].[BillingCompany] 'DropShip.BillingCompany',
	[ds].[BillingCo] 'DropShip.BillingCo',
	[ds].[BillingStreet] 'DropShip.BillingStreet',
	[ds].[BillingStreetNr] 'DropShip.BillingStreetNr',
	[ds].[BillingEntrance] 'DropShip.BillingEntrance',
	[ds].[BillingFloor] 'DropShip.BillingFloor',
	[ds].[BillingApartmentNumber] 'DropShip.BillingApartmentNumber',
	[ds].[BillingZip] 'DropShip.BillingZip',
	[ds].[BillingCity] 'DropShip.BillingCity',
	[ds].[BillingCountry] 'DropShip.BillingCountry',
	[ds].[DeliveryFirstName] 'DropShip.DeliveryFirstName',
	[ds].[DeliveryLastName] 'DropShip.DeliveryLastName',
	[ds].[DeliveryCompany] 'DropShip.DeliveryCompany',
	[ds].[DeliveryCo] 'DropShip.DeliveryCo',
	[ds].[DeliveryStreet] 'DropShip.DeliveryStreet',
	[ds].[DeliveryStreetNr] 'DropShip.DeliveryStreetNr',
	[ds].[DeliveryFloor] 'DropShip.DeliveryFloor',
	[ds].[DeliveryEntrance] 'DropShip.DeliveryEntrance',
	[ds].[DeliveryZip] 'DropShip.DeliveryZip',
	[ds].[DeliveryCity] 'DropShip.DeliveryCity',
	[ds].[DeliveryCountry] 'DropShip.DeliveryCountry',
	[ds].[Discount] 'DropShip.Discount',
	[ds].[Comment] 'DropShip.Comment',
	[ds].[SupplierNo] 'DropShip.SupplierNo',
	[ds].[OrderId] 'DropShip.OrderId',
	[ds].[OrderDate] 'DropShip.OrderDate',
	[ds].[CustomerNumber] 'DropShip.CustomerNumber',
	[ds].[NotificationNumber] 'DropShip.NotificationNumber',
	[ds].[PaymentMethod] 'DropShip.PaymentMethod',
	[ds].[ProductArticleNumber] 'DropShip.ProductArticleNumber',
	[ds].[ProductTitle] 'DropShip.ProductTitle',
	[ds].[Quantity] 'DropShip.Quantity',
	[ds].[Price] 'DropShip.Price',
	[ds].[ProductDiscount] 'DropShip.ProductDiscount',
	[ds].[ProductSoldSum] 'DropShip.ProductSoldSum',
	[ds].[ProductStatus] 'DropShip.ProductStatus',
	[ds].[InvoiceDate] 'DropShip.InvoiceDate',
	[ds].[VATproc] 'DropShip.VATproc',
	[ds].[CreatedDate] 'DropShip.CreatedDate',
	[ds].[SendDate] 'DropShip.SendDate',
	[ds].[CsvFileName] 'DropShip.CsvFileName',
	[ds].[PdfFileName] 'DropShip.PdfFileName'
FROM
	[orderlek].[tDropShip] ds



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockBrandProductListGetByIdSecure]'
GO
ALTER PROCEDURE [lekmer].[pBlockBrandProductListGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SELECT 
		[ba].*,
		[b].*,
		[bs].*,
		[ps].*
	FROM 
		[lekmer].[tBlockBrandProductList] ba
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON ba.[BlockId] = b.[Block.BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
		INNER JOIN product.vCustomProductSortOrder ps ON ps.[ProductSortOrder.Id] = ba.[ProductSortOrderId]
	WHERE
		ba.[BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[vBlockBestRatedProductList]'
GO

ALTER VIEW [review].[vBlockBestRatedProductList]
AS
	SELECT
		[BlockId] AS 'BlockBestRatedProductList.BlockId',
		[CategoryId] AS 'BlockBestRatedProductList.CategoryId',
		[RatingId] AS 'BlockBestRatedProductList.RatingId',
		[bs].*
	FROM
		[review].[tBlockBestRatedProductList] bbrpl
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bbrpl].[BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockBrandTopList]'
GO
ALTER VIEW [lekmer].[vBlockBrandTopList]
AS
	SELECT
		b.[BlockId] AS 'BlockBrandTopList.BlockId',
		b.[IncludeAllCategories] AS 'BlockBrandTopList.IncludeAllCategories',
		b.[OrderStatisticsDayCount] AS 'BlockBrandTopList.OrderStatisticsDayCount',
		b.[LinkContentNodeId] AS 'BlockBrandTopList.LinkContentNodeId',
		[bs].*
	FROM
		[lekmer].[tBlockBrandTopList] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockBrandList]'
GO
ALTER VIEW [lekmer].[vBlockBrandList]
AS
SELECT 
	[bbl].[BlockId] 'BlockBrandList.BlockId',
	[bbl].[IncludeAllBrands] 'BlockBrandList.IncludeAllBrands',
	[bbl].[LinkContentNodeId] 'BlockBrandList.LinkContentNodeId',
	[bs].*
FROM 
	[lekmer].[tBlockBrandList] bbl
	INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bbl].[BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockBrandListSave]'
GO
ALTER PROCEDURE [lekmer].[pBlockBrandListSave]
	@BlockId			INT,
	@IncludeAllBrands	BIT,
	@LinkContentNodeId	INT
AS 
BEGIN 
	UPDATE lekmer.tBlockBrandList
	SET 
		IncludeAllBrands = @IncludeAllBrands,
		LinkContentNodeId = @LinkContentNodeId
	WHERE 
		BlockId = @BlockId
		
	IF @@ROWCOUNT <> 0
		RETURN
		
	INSERT lekmer.tBlockBrandList (
		BlockId,
		IncludeAllBrands,
		LinkContentNodeId
	)
	VALUES (
		@BlockId,
		@IncludeAllBrands,
		@LinkContentNodeId
	)
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCustomBlockProductRelationListItem]'
GO
ALTER VIEW [product].[vCustomBlockProductRelationListItem]
AS
	SELECT
		*
	FROM
		[product].[vBlockProductRelationListItem]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pBlockProductRelationListItemGetAllByBlock]'
GO
ALTER PROCEDURE [product].[pBlockProductRelationListItemGetAllByBlock]
	@BlockId	INT
AS
BEGIN
	SELECT
		*
	FROM 
		[product].[vCustomBlockProductRelationListItem] brli
	WHERE
		[brli].[BlockProductRelationListItem.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockBrandListDelete]'
GO
ALTER PROCEDURE [lekmer].[pBlockBrandListDelete]
	@BlockId INT
AS 
BEGIN
	DELETE [lekmer].[tBlockBrandListBrand]
	WHERE [BlockId] = @BlockId

	DELETE [lekmer].[tBlockBrandList]
	WHERE [BlockId] = @BlockId
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockReviewMyWishListGetById]'
GO
ALTER PROCEDURE [lekmer].[pBlockReviewMyWishListGetById]
	@BlockId	INT,
	@LanguageId	INT
AS	
BEGIN
	SET NOCOUNT ON
	SELECT
		[b].*,
		[bs].*
	FROM
		[sitestructure].[vCustomBlock] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
		AND [b].[Block.LanguageId] = @LanguageId 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pBlockTopListSave]'
GO
ALTER PROCEDURE [addon].[pBlockTopListSave]
	@BlockId					INT,
	@IncludeAllCategories		BIT,
	@OrderStatisticsDayCount	INT,
	@LinkContentNodeId			INT = NULL,
	@CustomUrl					NVARCHAR(MAX) = NULL
AS
BEGIN
	UPDATE [addon].[tBlockTopList]
	SET
		[IncludeAllCategories]		= @IncludeAllCategories,
		[OrderStatisticsDayCount]	= @OrderStatisticsDayCount,
		[LinkContentNodeId]			= @LinkContentNodeId,
		[CustomUrl]					= @CustomUrl
	WHERE
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [addon].[tBlockTopList] (
		[BlockId],
		[IncludeAllCategories],
		[OrderStatisticsDayCount],
		[LinkContentNodeId],
		[CustomUrl]
	)
	VALUES (
		@BlockId,
		@IncludeAllCategories,
		@OrderStatisticsDayCount,
		@LinkContentNodeId,
		@CustomUrl
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockReviewMyWishListGetByIdSecure]'
GO
ALTER PROCEDURE [lekmer].[pBlockReviewMyWishListGetByIdSecure]
	@BlockId INT
AS	
BEGIN
	SET NOCOUNT ON
	SELECT
		[b].*,
		[bs].*
	FROM
		[sitestructure].[vCustomBlockSecure] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pBlockPackageProductListGetByIdSecure]'
GO
ALTER PROCEDURE [productlek].[pBlockPackageProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		[b].*,
		[bs].*
	FROM 
		[sitestructure].[vCustomBlockSecure] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pBlockProductRelationListItemSave]'
GO
ALTER PROCEDURE [product].[pBlockProductRelationListItemSave]
	@BlockId			INT,
	@RelationListTypeId	INT
AS
BEGIN
	INSERT INTO [product].[tBlockProductRelationListItem]
	VALUES (@BlockId, @RelationListTypeId) 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pLekmerBlockSave]'
GO
ALTER PROCEDURE [lekmer].[pLekmerBlockSave]
	@BlockId					INT,
	@StartDate					DATETIME = NULL,
	@EndDate					DATETIME = NULL,
	@StartDailyIntervalMinutes	INT = NULL,
	@EndDailyIntervalMinutes	INT = NULL,
	@ShowOnDesktop				BIT = NULL,
	@ShowOnMobile				BIT = NULL,
	@CanBeUsedAsFallbackOption	BIT = NULL
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tLekmerBlock]
	SET	
		[StartDate]					= @StartDate,
		[EndDate]					= @EndDate,
		[StartDailyIntervalMinutes] = @StartDailyIntervalMinutes,
		[EndDailyIntervalMinutes]	= @EndDailyIntervalMinutes,
		[ShowOnDesktop]				= @ShowOnDesktop,
		[ShowOnMobile]				= @ShowOnMobile,
		[CanBeUsedAsFallbackOption] = @CanBeUsedAsFallbackOption
	WHERE
		[BlockId] = @BlockId
  
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[lekmer].[tLekmerBlock] (
			[BlockId],
			[StartDate],
			[EndDate],
			[StartDailyIntervalMinutes],
			[EndDailyIntervalMinutes],
			[ShowOnDesktop],
			[ShowOnMobile],
			[CanBeUsedAsFallbackOption]
		)
		VALUES (
			@BlockId,
			@StartDate,
			@EndDate,
			@StartDailyIntervalMinutes,
			@EndDailyIntervalMinutes,
			@ShowOnDesktop,
			@ShowOnMobile,
			@CanBeUsedAsFallbackOption
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [orderlek].[pDropShipUpdate]'
GO
ALTER PROCEDURE [orderlek].[pDropShipUpdate]
	@DropShipId		INT,
	@CsvFileName	NVARCHAR(100),
	@PdfFileName	NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tDropShip]
	SET
		[SendDate] = GETDATE(),
		[CsvFileName] = @CsvFileName,
		[PdfFileName] = @PdfFileName
	WHERE
		[DropShipId] = @DropShipId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockBrandProductListDelete]'
GO
ALTER PROCEDURE [lekmer].[pBlockBrandProductListDelete]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandProductList]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockEsalesRecommendSave]'
GO
ALTER PROCEDURE [lekmer].[pBlockEsalesRecommendSave]
	@BlockId INT,
	@RecommendationType INT,
	@PanelPath NVARCHAR(MAX),
	@FallbackPanelPath NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tBlockEsalesRecommend]
	SET
		[EsalesRecommendationTypeId] = @RecommendationType,
		[PanelPath]		= @PanelPath,
		[FallbackPanelPath] = @FallbackPanelPath
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockEsalesRecommend]
	(
		[BlockId],
		[EsalesRecommendationTypeId],
		[PanelPath],
		[FallbackPanelPath]		
	)
	VALUES
	(
		@BlockId,
		@RecommendationType,
		@PanelPath,
		@FallbackPanelPath
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockEsalesRecommend]'
GO
ALTER VIEW [lekmer].[vBlockEsalesRecommend]
AS
	SELECT
		[b].*,
		[ber].[EsalesRecommendationTypeId] AS 'BlockEsalesRecommend.EsalesRecommendationTypeId',
		[ber].[PanelPath] AS 'BlockEsalesRecommend.PanelPath',
		[ber].[FallbackPanelPath] AS 'BlockEsalesRecommend.FallbackPanelPath',
		[bs].*
	FROM
		[lekmer].[tBlockEsalesRecommend] ber
		INNER JOIN [sitestructure].[vCustomBlock] b ON b.[Block.BlockId] = ber.[BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockEsalesAds]'
GO

ALTER VIEW [lekmer].[vBlockEsalesAds]
AS
	SELECT
		[b].*,
		[bea].[PanelPath] AS 'BlockEsalesAds.PanelPath',
		[bea].[Format] AS 'BlockEsalesAds.Format',
		[bea].[Gender] AS 'BlockEsalesAds.Gender',
		[bea].[ProductCategory] AS 'BlockEsalesAds.ProductCategory',
		[bs].*
	FROM
		[lekmer].[tBlockEsalesAds] bea
		INNER JOIN [sitestructure].[vCustomBlock] b ON b.[Block.BlockId] = bea.[BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[pBlockTopListDelete]'
GO
ALTER PROCEDURE [addon].[pBlockTopListDelete]
	@BlockId INT
AS
BEGIN
	DELETE addon.tBlockTopListCategory
	WHERE BlockId = @BlockId
	
	DELETE addon.tBlockTopListProduct
	WHERE BlockId = @BlockId

	DELETE addon.tBlockTopList
	WHERE BlockId = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[vBlockTopList]'
GO
ALTER VIEW [addon].[vBlockTopList]
AS
	SELECT
		[BlockId]					AS 'BlockTopList.BlockId',
		[IncludeAllCategories]		AS 'BlockTopList.IncludeAllCategories',
		[OrderStatisticsDayCount]	AS 'BlockTopList.OrderStatisticsDayCount',
		[LinkContentNodeId]			AS 'BlockTopList.LinkContentNodeId',
		[CustomUrl]					AS 'BlockTopList.CustomUrl'
	FROM
		[addon].[tBlockTopList]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [addon].[vCustomBlockTopList]'
GO
ALTER VIEW [addon].[vCustomBlockTopList]
AS
	SELECT
		*
	FROM
		[addon].[vBlockTopList] btl
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [btl].[BlockTopList.BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]'
GO
ALTER PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	SELECT
		[p].[ProductId]						'Product.Id',
		[p].[ErpId]							'Product.ErpId',
		[p].[EanCode]						'Product.EanCode',
		[p].[CategoryId]					'Product.CategoryId',
		[p].[ProductStatusId]				'Product.ProductStatusId',
		[p].[NumberInStock]					'Product.NumberInStock',
		[p].[ItemsInPackage]				'Product.ItemsInPackage',
		[p].[MediaId]						'Product.MediaId',
		[p].[IsDeleted]						'Product.IsDeleted',
		COALESCE([pt].[Title], [p].[Title])							'Product.Title',
		COALESCE([pt].[WebShopTitle], [p].[WebShopTitle])			'Product.WebShopTitle',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription])	'Product.ShortDescription',
		COALESCE([pt].[Description], [p].[Description])				'Product.Description',
		[lp].[Lekmer.BrandId]				'Lekmer.BrandId',
		[lp].[Lekmer.IsBookable]			'Lekmer.IsBookable',
		[lp].[Lekmer.IsNewFrom]				'Lekmer.IsNewFrom',
		[lp].[Lekmer.IsNewTo]				'Lekmer.IsNewTo',
		[lp].[Lekmer.CreatedDate]			'Lekmer.CreatedDate',
		[lp].[Lekmer.LekmerErpId]			'Lekmer.LekmerErpId',
		[lp].[Lekmer.HasSizes]				'Lekmer.HasSizes',
		[lp].[Lekmer.ShowVariantRelations]	'Lekmer.ShowVariantRelations',
		[lp].[Lekmer.Weight]				'Lekmer.Weight',
		[lp].[Lekmer.ProductTypeId]			'Lekmer.ProductTypeId',
		[lp].[Lekmer.IsAbove60L]			'Lekmer.IsAbove60L',
		[lp].[Lekmer.StockStatusId]			'Lekmer.StockStatusId',
		[lp].[Lekmer.MonitorThreshold]		'Lekmer.MonitorThreshold',
		[lp].[Lekmer.MaxQuantityPerOrder]	'Lekmer.MaxQuantityPerOrder',
		[lp].[Lekmer.DeliveryTimeId]		'Lekmer.DeliveryTimeId',
		[lp].[Lekmer.SellOnlyInPackage]		'Lekmer.SellOnlyInPackage',
		[lp].[Lekmer.IsDropShip]			'Lekmer.IsDropShip',
		[pu].[ProductUrl.UrlTitle]			'Lekmer.UrlTitle',
		[rp].[Price]						'Lekmer.RecommendedPrice',
		[pssr].[ParentContentNodeId]		'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId]		'Product.TemplateContentNodeId',
		[i].*,
		[dt].*,
		COALESCE ([lpt].[Product.Measurement], [lp].[Lekmer.Measurement]) 'Lekmer.Measurement',
		[lp].[Lekmer.AgeFromMonth],
		[lp].[Lekmer.AgeToMonth],
		[lp].[Lekmer.BatteryTypeId],
		[lp].[Lekmer.NumberOfBatteries],
		[lp].[Lekmer.IsBatteryIncluded],
		[lp].[Lekmer.ExpectedBackInStock],
		[lp].[Lekmer.SizeDeviationId],
		[bt].*,
		[sd].*,
		@ChannelId				'Product.ChannelId', 
		@CurrencyId				'Product.CurrencyId',
		@PriceListRegistryId	'Product.PriceListRegistryId',
		[pli].*
	FROM
		[generic].[fnConvertIDListToTableWithOrdinal] (@ProductIds, @Delimiter) AS pl
		INNER JOIN [product].[tProduct] AS p ON [p].[ProductId] = [pl].[Id]
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [productlek].[vDeliveryTime] AS dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = @ChannelId
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON [lpt].[Product.Id] = [p].[ProductId] AND [lpt].[Language.Id] = @LanguageId
		LEFT JOIN [lekmer].[vBatteryType] bt ON [bt].[BatteryType.Id] = [lp].[Lekmer.BatteryTypeId]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON [sd].[SizeDeviation.Id] = [lp].[Lekmer.SizeDeviationId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
		AND [lp].[Lekmer.SellOnlyInPackage] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [review].[pBlockBestRatedProductListSave]'
GO

ALTER PROCEDURE [review].[pBlockBestRatedProductListSave]
	@BlockId INT,
	@CategoryId INT,
	@RatingId INT
AS
BEGIN
	UPDATE [review].[tBlockBestRatedProductList]
	SET
		[CategoryId] = @CategoryId,
		[RatingId] = @RatingId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockBestRatedProductList] (
		[BlockId],
		[CategoryId],
		[RatingId]
	)
	VALUES (
		@BlockId,
		@CategoryId,
		@RatingId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockEsalesRecommendSecure]'
GO
ALTER VIEW [lekmer].[vBlockEsalesRecommendSecure]
AS
	SELECT
		[b].*,
		[ber].[EsalesRecommendationTypeId] AS 'BlockEsalesRecommend.EsalesRecommendationTypeId',
		[ber].[PanelPath] AS 'BlockEsalesRecommend.PanelPath',
		[ber].[FallbackPanelPath] AS 'BlockEsalesRecommend.FallbackPanelPath',
		[bs].*
	FROM
		[lekmer].[tBlockEsalesRecommend] ber
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON b.[Block.BlockId] = ber.[BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockProductSimilarListSave]'
GO
ALTER PROCEDURE [lekmer].[pBlockProductSimilarListSave]
	@BlockId INT,
	@PriceRangeType INT
AS
BEGIN
	UPDATE [lekmer].[tBlockProductSimilarList]
	SET
		[PriceRangeType] = @PriceRangeType
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockProductSimilarList] (
		[BlockId],
		[PriceRangeType]
	)
	VALUES (
		@BlockId,
		@PriceRangeType
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockBrandProductListGetById]'
GO
ALTER PROCEDURE [lekmer].[pBlockBrandProductListGetById]
	@LanguageId	INT,
	@BlockId	INT
AS
BEGIN
	SELECT
		[ba].*,
		[b].*,
		[bs].*,
		[ps].*
	FROM
		[lekmer].[tBlockBrandProductList] ba
		INNER JOIN [sitestructure].[vCustomBlock] b ON ba.[BlockId] = b.[Block.BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
		INNER JOIN product.vCustomProductSortOrder ps ON ps.[ProductSortOrder.Id] = ba.[ProductSortOrderId]
	WHERE
		ba.[BlockId] = @BlockId 
		AND b.[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pProductGetAllByIdListWithoutAnyFilter]'
GO
ALTER PROCEDURE [productlek].[pProductGetAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	SELECT
		[p].[ProductId]						'Product.Id',
		[p].[ErpId]							'Product.ErpId',
		[p].[EanCode]						'Product.EanCode',
		[p].[CategoryId]					'Product.CategoryId',
		[p].[ProductStatusId]				'Product.ProductStatusId',
		[p].[NumberInStock]					'Product.NumberInStock',
		[p].[ItemsInPackage]				'Product.ItemsInPackage',
		[p].[MediaId]						'Product.MediaId',
		[p].[IsDeleted]						'Product.IsDeleted',
		COALESCE([pt].[Title], [p].[Title])							'Product.Title',
		COALESCE([pt].[WebShopTitle], [p].[WebShopTitle])			'Product.WebShopTitle',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription])	'Product.ShortDescription',
		COALESCE([pt].[Description], [p].[Description])				'Product.Description',
		[lp].[Lekmer.BrandId]				'Lekmer.BrandId',
		[lp].[Lekmer.IsBookable]			'Lekmer.IsBookable',
		[lp].[Lekmer.IsNewFrom]				'Lekmer.IsNewFrom',
		[lp].[Lekmer.IsNewTo]				'Lekmer.IsNewTo',
		[lp].[Lekmer.CreatedDate]			'Lekmer.CreatedDate',
		[lp].[Lekmer.LekmerErpId]			'Lekmer.LekmerErpId',
		[lp].[Lekmer.HasSizes]				'Lekmer.HasSizes',
		[lp].[Lekmer.ShowVariantRelations]	'Lekmer.ShowVariantRelations',
		[lp].[Lekmer.Weight]				'Lekmer.Weight',
		[lp].[Lekmer.ProductTypeId]			'Lekmer.ProductTypeId',
		[lp].[Lekmer.IsAbove60L]			'Lekmer.IsAbove60L',
		[lp].[Lekmer.StockStatusId]			'Lekmer.StockStatusId',
		[lp].[Lekmer.MonitorThreshold]		'Lekmer.MonitorThreshold',
		[lp].[Lekmer.MaxQuantityPerOrder]	'Lekmer.MaxQuantityPerOrder',
		[lp].[Lekmer.DeliveryTimeId]		'Lekmer.DeliveryTimeId',
		[lp].[Lekmer.SellOnlyInPackage]		'Lekmer.SellOnlyInPackage',
		[lp].[Lekmer.IsDropShip]			'Lekmer.IsDropShip',
		[pu].[ProductUrl.UrlTitle]			'Lekmer.UrlTitle',
		[rp].[Price]						'Lekmer.RecommendedPrice',
		[pssr].[ParentContentNodeId]		'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId]		'Product.TemplateContentNodeId',
		[i].*,
		[dt].*,
		[pli].*
	FROM
		[generic].[fnConvertIDListToTableWithOrdinal] (@ProductIds, @Delimiter) AS pl
		INNER JOIN [product].[tProduct] AS p ON [p].[ProductId] = [pl].[Id]
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = @ChannelId
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND [pli].[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockAvailCombineDelete]'
GO
ALTER PROCEDURE [lekmer].[pBlockAvailCombineDelete]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockAvailCombine]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockProductSimilarListGetByIdSecure]'
GO

ALTER PROCEDURE [lekmer].[pBlockProductSimilarListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		[psl].*,
		[b].*,
		[bs].*
	FROM 
		lekmer.vCustomBlockProductSimilarList AS psl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON psl.[BlockProductSimilarList.BlockId] = b.[Block.BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		psl.[BlockProductSimilarList.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [productlek].[pBlockPackageProductListGetById]'
GO
ALTER PROCEDURE [productlek].[pBlockPackageProductListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		[b].*,
		[bs].*
	FROM 
		[sitestructure].[vCustomBlock] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[vBlockEsalesAdsSecure]'
GO

ALTER VIEW [lekmer].[vBlockEsalesAdsSecure]
AS
	SELECT
		[b].*,
		[bea].[PanelPath] AS 'BlockEsalesAds.PanelPath',
		[bea].[Format] AS 'BlockEsalesAds.Format',
		[bea].[Gender] AS 'BlockEsalesAds.Gender',
		[bea].[ProductCategory] AS 'BlockEsalesAds.ProductCategory',
		[bs].*
	FROM
		[lekmer].[tBlockEsalesAds] bea
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON b.[Block.BlockId] = bea.[BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockAvailCombineSave]'
GO
ALTER PROCEDURE [lekmer].[pBlockAvailCombineSave]
	@BlockId INT,
	@UseCartPredictions BIT,
	@UseClickStreamPredictions BIT,
	@UseLogPurchase BIT,
	@UsePersonalPredictions BIT,
	@UseProductSearchPredictions BIT,
	@UseProductsPredictions BIT,
	@UseProductsPredictionsFromClicksCategory BIT,
	@UseProductsPredictionsFromClicksProduct BIT,
	@UseLogClickedOn BIT,
	@GetClickHistoryFromCookie BIT
AS
BEGIN
	UPDATE
		[lekmer].[tBlockAvailCombine]
	SET
		UseCartPredictions = @UseCartPredictions,
		UseClickStreamPredictions = @UseClickStreamPredictions,
		UseLogPurchase = @UseLogPurchase,
		UsePersonalPredictions = @UsePersonalPredictions,
		UseProductSearchPredictions = @UseProductSearchPredictions,
		UseProductsPredictions = @UseProductsPredictions,
		UseProductsPredictionsFromClicksCategory = @UseProductsPredictionsFromClicksCategory,
		UseProductsPredictionsFromClicksProduct = @UseProductsPredictionsFromClicksProduct,
		UseLogClickedOn=@UseLogClickedOn,
		GetClickHistoryFromCookie=@GetClickHistoryFromCookie
	WHERE
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockAvailCombine] (
		BlockId,
		UseCartPredictions,
		UseClickStreamPredictions,
		UseLogPurchase,
		UsePersonalPredictions,
		UseProductSearchPredictions,
		UseProductsPredictions,
		UseProductsPredictionsFromClicksCategory,
		UseProductsPredictionsFromClicksProduct,
		UseLogClickedOn,
		GetClickHistoryFromCookie
	)
	VALUES (
		@BlockId,
		@UseCartPredictions,
		@UseClickStreamPredictions,
		@UseLogPurchase,
		@UsePersonalPredictions,
		@UseProductSearchPredictions,
		@UseProductsPredictions,
		@UseProductsPredictionsFromClicksCategory,
		@UseProductsPredictionsFromClicksProduct,
		@UseLogClickedOn,
		@GetClickHistoryFromCookie
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockEsalesAdsSave]'
GO

ALTER PROCEDURE [lekmer].[pBlockEsalesAdsSave]
	@BlockId INT,
	@PanelPath NVARCHAR(MAX),
	@Format NVARCHAR(50),
	@Gender NVARCHAR(50),
	@ProductCategory NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tBlockEsalesAds]
	SET
		[PanelPath]		= @PanelPath,
		[Format]		= @Format,
		[Gender]		= @Gender,
		[ProductCategory] = @ProductCategory
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockEsalesAds]
	(
		[BlockId],
		[PanelPath],
		[Format],
		[Gender],
		[ProductCategory]
	)
	VALUES
	(
		@BlockId,
		@PanelPath,
		@Format,
		@Gender,
		@ProductCategory
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pCreateBrandPagesByTitleSE]'
GO
ALTER PROCEDURE [integration].[pCreateBrandPagesByTitleSE]
	@BrandTitle nvarchar(50)

AS
begin
	set nocount on
		
	begin try
		begin transaction
		
		DECLARE @tmp TABLE (ContentNodeId int not null)
		DECLARE @tmpBlockId TABLE (BlockId int not null)
		
		-- tContentNode
		insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
					Ordinal, CommonName, AccessId, SiteStructureRegistryId)
		output inserted.ContentNodeId INTO @tmp
		select
			1000540, -- Varumärken SE
			3, -- ?
			1, --Offline
			@BrandTitle,
			128,
			LOWER(@BrandTitle),
			1, --All
			1 -- se

		
		-- tContentPage
		insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
					UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
		select
			(select top 1 ContentNodeId from @tmp),
			1, -- se
			1000038, -- BrandsDetailPage
			37, -- Xinnan null
			@BrandTitle,
			REPLACE(LOWER(@BrandTitle), ' ', '-'),
			1, -- Content page
			0,
			1000541 -- Xinnan null
			
		-- tBlock
		-- Brand Profile
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			18, --BrandList
			0,
			1000013,
			'brand info',
			1,
			1,
			1000108
			
					-- Brand profile extra content
					insert into lekmer.tBlockBrandList(BlockId, IncludeAllBrands, LinkContentNodeId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						0,
						NULL
					INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
					SELECT 
						(select top 1 BlockId from @tmpBlockId),
						1,
						NULL,
						1,
						NULL,
						10000,
						NULL
				
						-- Brand profile BrandlistBrand extra content
					insert into lekmer.tBlockBrandListBrand(BlockId, BrandId, Ordinal)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle), -- NY
						10

					-- töm listan
					delete from @tmpBlockId
		/*	
		-- Image
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			1, 
			0,
			1000018,
			'Image',
			2,
			1,
			1000096
			
					-- Image extra content
					insert into sitestructure.tBlockRichText(BlockId, Content)
					select
						(select top 1 BlockId from @tmpBlockId),
						NULL
		
					-- töm listan
					delete from @tmpBlockId
		*/		
		/*		
		-- Brand Info
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			18, --BrandList
			0,
			1000018,
			'brand info',
			3,
			1,
			1000095
				
					-- Brand Info extra content
					insert into lekmer.tBlockBrandList(BlockId, ColumnCount, [RowCount], IncludeAllBrands, LinkContentNodeId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						1,
						1,
						0,
						NULL
					
					-- Brand Info BrandListBrand extra content
					insert into lekmer.tBlockBrandListBrand(BlockId, BrandId, Ordinal)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle), -- NY
						10
						
					-- töm listan
					delete from @tmpBlockId
		*/
		-- Heppo tipsar
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			17, 
			0,
			1000019,
			'Heppo tipsar', -- en case här beroende på channel
			1,
			1,
			1000134
			
					-- Heppo Tipsar extra content
					insert into lekmer.tBlockBrandProductList(BlockId, ProductSortOrderId)
					select (select top 1 BlockId from @tmpBlockId), 2
					INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
					SELECT (select top 1 BlockId from @tmpBlockId), 2, NULL, 2, NULL, 10000, NULL
					
					-- Heppo Tipsar BrandProductListBrand extra content
					insert into lekmer.tBlockBrandProductListBrand(BlockId, BrandId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle) -- NY

					
					-- töm listan
					delete from @tmpBlockId
			
		-- produkter från brand
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			17, 
			0,
			1000021,
			'Produkter från ' + @BrandTitle, -- en case här beroende på channel
			1,
			1,
			1000133
			
					-- produkter från brand extra content
					insert into lekmer.tBlockBrandProductList(BlockId, ProductSortOrderId)
					select (select top 1 BlockId from @tmpBlockId), 3
					INSERT INTO [sitestructurelek].[tBlockSetting] ([BlockId],[ColumnCount],[ColumnCountMobile],[RowCount],[RowCountMobile],[TotalItemCount],[TotalItemCountMobile])
					SELECT (select top 1 BlockId from @tmpBlockId), 4, NULL, 3, NULL, 10000, NULL
					
					--produkter från brand BrandProductListBrand extra content
					insert into lekmer.tBlockBrandProductListBrand(BlockId, BrandId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle) -- NY
						
						
					-- töm listan
					delete from @tmpBlockId
	
					
				---------------------------
				-- CHILDREN - MAN
				---------------------------
				DECLARE @tmpChildrenMan TABLE (ContentNodeId int not null)
				
				-- tContentNode
				insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
							Ordinal, CommonName, AccessId, SiteStructureRegistryId)
				output inserted.ContentNodeId INTO @tmpChildrenMan
				select
					(select top 1 ContentNodeId from @tmp), -- förlärder är ovan
					3, -- ?
					0, --Offline
					@BrandTitle + ' för män',
					2,
					LOWER(@BrandTitle)+'ForMan',
					1, --All
					1 -- se

				
				-- tContentPage
				insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
							UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
				select
					(select top 1 ContentNodeId from @tmpChildrenMan),
					1, -- se
					1000024, 
					1000050,
					@BrandTitle,
					(REPLACE(LOWER(@BrandTitle), ' ', '-') + '-for-man'),
					1, -- Content page
					0,
					NULL
					
						-- tBlock
						-- Brand/category filter
						insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
									AccessId, TemplateId)
						output inserted.BlockId INTO @tmpBlockId
						select
							(select top 1 ContentNodeId from @tmpChildrenMan),
							1000001, 
							0,
							1000010,
							'Brand/category filter',
							1,
							1,
							1000171
							
									-- Brand/category filter extra content
									insert into lekmer.tBlockProductFilter(BlockId, DefaultCategoryId)			
									select
										(select top 1 BlockId from @tmpBlockId),
										1000060
									
									-- Brand/category filterBrand extra content
									insert into lekmer.tBlockProductFilterBrand(BlockId, BrandId)
									select
										(select top 1 BlockId from @tmpBlockId),
										(select BrandId from lekmer.tBrand where Title = @BrandTitle)
									
									-- töm listan
									delete from @tmpBlockId
						

			commit			
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockBrandProductListSave]'
GO
ALTER PROCEDURE [lekmer].[pBlockBrandProductListSave]
	@BlockId			INT,
	@ProductSortOrderId	INT
AS
BEGIN
	UPDATE
		[lekmer].[tBlockBrandProductList]
	SET
		[ProductSortOrderId]	= @ProductSortOrderId
	WHERE
		[BlockId]		= @BlockId
			
	IF  @@ROWCOUNT = 0 
		BEGIN
			INSERT [lekmer].[tBlockBrandProductList] (
				[BlockId],
				[ProductSortOrderId]
			)
			VALUES (
				@BlockId,
				@ProductSortOrderId
			)
		END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pBlockSharedWishListGetByIdSecure]'
GO
ALTER PROCEDURE [lekmer].[pBlockSharedWishListGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[b].*,
		[bs].*
	FROM
		[sitestructure].[vCustomBlockSecure] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pBlockProductRelationListItemDelete]'
GO
ALTER PROCEDURE [product].[pBlockProductRelationListItemDelete]
	@BlockId	INT
AS
BEGIN
	DELETE FROM [product].[tBlockProductRelationListItem]
	WHERE [BlockId] = @BlockId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [product].[tBlockProductListProduct]'
GO
ALTER TABLE [product].[tBlockProductListProduct] ADD CONSTRAINT [FK_tBlockProductListProduct_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
