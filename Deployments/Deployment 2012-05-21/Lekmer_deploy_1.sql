/*
Run this script on a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 012\DB\LekmerDB    -  This database will be modified. The scripts folder will not be modified.

to synchronize it with a database with the schema represented by:

        D:\Projects\lmheppo\Scensum\trunk\Release\Release 013\DB\LekmerDB

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 21.05.2012 11:52:02

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [customerlek].[tCustomerInformation]'
GO
CREATE TABLE [customerlek].[tCustomerInformation]
(
[CustomerId] [int] NOT NULL,
[GenderTypeId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCustomerInformation] on [customerlek].[tCustomerInformation]'
GO
ALTER TABLE [customerlek].[tCustomerInformation] ADD CONSTRAINT [PK_tCustomerInformation] PRIMARY KEY CLUSTERED  ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[tGenderType]'
GO
CREATE TABLE [customerlek].[tGenderType]
(
[GenderTypeId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tGenderType] on [customerlek].[tGenderType]'
GO
ALTER TABLE [customerlek].[tGenderType] ADD CONSTRAINT [PK_tGenderType] PRIMARY KEY CLUSTERED  ([GenderTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tGenderType_ErpId] on [customerlek].[tGenderType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tGenderType_ErpId] ON [customerlek].[tGenderType] ([ErpId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customerlek].[pAddressDelete]'
GO

ALTER PROCEDURE [customerlek].[pAddressDelete]
	@AddressId	INT
AS	
BEGIN
	SET NOCOUNT ON
	
	-- Delete lekmer part
	DELETE [customerlek].[tAddress]
	WHERE [AddressId] = @AddressId
	
	DELETE [customer].[tAddress]
	WHERE AddressId = @AddressId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customerlek].[pAddressSave]'
GO

ALTER PROCEDURE [customerlek].[pAddressSave]
	@AddressId		INT,
	@CustomerId		INT,
	@Addressee		NVARCHAR(100),
	@StreetAddress	NVARCHAR(200),
	@StreetAddress2 NVARCHAR(200),
	@PostalCode		NVARCHAR(10),
	@City			NVARCHAR(100),
	@CountryId		INT,
	@PhoneNumber	NVARCHAR(20),
	@AddressTypeId	INT,
	@HouseNumber	NVARCHAR(50),
	@HouseExtension	NVARCHAR(100)
AS	
BEGIN
	SET NOCOUNT ON
	
	--Check if customer exist
	IF NOT EXISTS ( 
					SELECT 
						1 
					FROM 
						[customer].[tCustomer] 
					WHERE 
						[tCustomer].[CustomerId] = @CustomerId 
					)
		RETURN -1
	
	UPDATE  [customer].[tAddress]
	SET 
		Addressee = @Addressee,
		StreetAddress = @StreetAddress,
		StreetAddress2 = @StreetAddress2,
		PostalCode = @PostalCode,
		City = @City,
		CountryId = @CountryId,
		PhoneNumber = @PhoneNumber,
		AddressTypeId = @AddressTypeId
	WHERE
		AddressId = @AddressId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customer].[tAddress]
		(
			CustomerId,
			Addressee,
			StreetAddress,
			StreetAddress2,
			PostalCode,
			City,
			CountryId,
			PhoneNumber,
			AddressTypeId
		)
		VALUES
		(
			@CustomerId,
			@Addressee,
			@StreetAddress,
			@StreetAddress2,
			@PostalCode,
			@City,
			@CountryId,
			@PhoneNumber,
			@AddressTypeId
		)
		SET	@AddressId = SCOPE_IDENTITY()
	END
	
	UPDATE  [customerlek].[tAddress]
	SET 
		HouseNumber = @HouseNumber,
		HouseExtension = @HouseExtension
	WHERE
		AddressId = @AddressId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customerlek].[tAddress]
		(
			AddressId,
			CustomerId,
			HouseNumber,
			HouseExtension
		)
		VALUES
		(
			@AddressId,
			@CustomerId,
			@HouseNumber,
			@HouseExtension
		)
	END

	RETURN @AddressId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[pCustomerInformationSave]'
GO

CREATE PROCEDURE [customerlek].[pCustomerInformationSave]
	@CustomerId INT,
	@FirstName NVARCHAR(50),
	@LastName NVARCHAR(50),
	@CivicNumber NVARCHAR(50),
	@PhoneNumber NVARCHAR(50),
	@CellPhoneNumber NVARCHAR(50),
	@Email VARCHAR(320),
	@CreatedDate DATETIME,
	@DefaultBillingAddressId INT = NULL,
	@DefaultDeliveryAddressId INT = NULL,
	@GenderTypeId INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[customer].[tCustomerInformation]
	SET    
		FirstName = @FirstName, 
		LastName = @LastName,
		CivicNumber = @CivicNumber,
		PhoneNumber = @PhoneNumber,
		CellPhoneNumber = @CellPhoneNumber,
		Email = @Email,
		DefaultBillingAddressId = @DefaultBillingAddressId,
		DefaultDeliveryAddressId = @DefaultDeliveryAddressId
	WHERE 
		CustomerId = @CustomerId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO	[customer].[tCustomerInformation]
		(
			CustomerId,
			FirstName, 
			LastName,
			CivicNumber,
			PhoneNumber,
			CellPhoneNumber,
			Email,
			CreatedDate,
			DefaultBillingAddressId,
			DefaultDeliveryAddressId
		)
		VALUES
		(
			@CustomerId,
			@FirstName, 
			@LastName,
			@CivicNumber,
			@PhoneNumber,
			@CellPhoneNumber,
			@Email,
			@CreatedDate,
			@DefaultBillingAddressId,
			@DefaultDeliveryAddressId
		)
	END
	
	-- Save Lekmer part
	
	UPDATE
		[customerlek].[tCustomerInformation]
	SET    
		GenderTypeId = @GenderTypeId
	WHERE 
		CustomerId = @CustomerId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO	[customerlek].[tCustomerInformation]
		(
			CustomerId,
			GenderTypeId
		)
		VALUES
		(
			@CustomerId,
			@GenderTypeId
		)
	END

	RETURN @CustomerId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[vGenderType]'
GO

CREATE VIEW [customerlek].[vGenderType]
AS
	SELECT     
		GT.GenderTypeId AS 'GenderType.GenderTypeId',
		GT.ErpId AS 'GenderType.ErpId',
		GT.Title AS 'GenderType.Title',
		GT.CommonName AS 'GenderType.CommonName'
	FROM
		[customerlek].[tGenderType] as GT
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[pGenderTypeGetAll]'
GO

CREATE PROCEDURE [customerlek].[pGenderTypeGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[customerlek].[vGenderType]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductSizeSave]'
GO

ALTER PROCEDURE [lekmer].[pProductSizeSave]
	@ProductId				INT,
	@SizeId					INT,
	@Millimeter				INT,
	@MillimeterDeviation	INT,
	@OverrideEU				DECIMAL(3, 1)
AS 
BEGIN 
	UPDATE
		[lekmer].[tProductSize]
	SET
		MillimeterDeviation = @MillimeterDeviation,
		OverrideEU			= @OverrideEU
	WHERE 
		ProductId = @ProductId AND
		SizeId = @SizeId
		
	UPDATE
		[lekmer].[tSize]
	SET
		Millimeter = @Millimeter
	WHERE 
		SizeId = @SizeId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [customerlek].[vCustomerInformation]'
GO


CREATE VIEW [customerlek].[vCustomerInformation]
AS
	SELECT
		CustomerId AS 'CustomerInformation.CustomerId',
		GenderTypeId AS 'CustomerInformation.GenderTypeId'
	FROM
		[customerlek].[tCustomerInformation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomCustomer]'
GO


ALTER VIEW [customer].[vCustomCustomer]
AS
	SELECT
		C.*,
		CI.[CustomerInformation.GenderTypeId],
		BA.[Address.HouseNumber] AS 'BillingAddress.HouseNumber',
		BA.[Address.HouseExtension] AS 'BillingAddress.HouseExtension',
		DA.[Address.HouseNumber] AS 'DeliveryAddress.HouseNumber',
		DA.[Address.HouseExtension] AS  'DeliveryAddress.HouseExtension'
	FROM
		[customer].[vCustomer] C
		LEFT JOIN [customerlek].vCustomerInformation CI ON CI.[CustomerInformation.CustomerId] = C.[CustomerInformation.InformationId]
		LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = C.[CustomerInformation.DefaultBillingAddressId]
		LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = C.[CustomerInformation.DefaultDeliveryAddressId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[vCustomCustomerSecure]'
GO


ALTER VIEW [customer].[vCustomCustomerSecure]
AS
	SELECT
		C.*,
		CI.[CustomerInformation.GenderTypeId],
		BA.[Address.HouseNumber] AS 'BillingAddress.HouseNumber',
		BA.[Address.HouseExtension] AS 'BillingAddress.HouseExtension',
		DA.[Address.HouseNumber] AS 'DeliveryAddress.HouseNumber',
		DA.[Address.HouseExtension] AS  'DeliveryAddress.HouseExtension'
	FROM
		[customer].[vCustomerSecure] C
		LEFT JOIN [customerlek].vCustomerInformation CI ON CI.[CustomerInformation.CustomerId] = C.[CustomerInformation.InformationId]
		LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = C.[CustomerInformation.DefaultBillingAddressId]
		LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = C.[CustomerInformation.DefaultDeliveryAddressId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customerlek].[tCustomerInformation]'
GO
ALTER TABLE [customerlek].[tCustomerInformation] ADD
CONSTRAINT [FK_tCustomerInformation_tCustomerInformation1] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomerInformation] ([CustomerId]),
CONSTRAINT [FK_tCustomerInformation_tGenderType] FOREIGN KEY ([GenderTypeId]) REFERENCES [customerlek].[tGenderType] ([GenderTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
