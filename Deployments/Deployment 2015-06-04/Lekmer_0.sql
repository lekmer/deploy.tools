SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [lekmer].[tVoucherAction]'
GO
ALTER TABLE [lekmer].[tVoucherAction] ADD
[AllowSpecialOffer] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pReportProductsMissingImages]'
GO
ALTER PROCEDURE [integration].[pReportProductsMissingImages]
AS
BEGIN
	WITH UniqueErpIdLekmer AS
	(
		SELECT
			ROW_NUMBER() OVER(PARTITION BY [lp].[HYErpId] ORDER BY [lp].[HYErpId]) RowNo,
			[lp].[HYErpId],
			[psp].[Lagerplats],
			[p].[Title],
			[lp].[BrandId],
			[lp].[LekmerErpId],
			[ps].[ErpId],
			ISNULL([a1].[NumberInStock], [p].[NumberInStock]) 'NumberInStock',
			lp.[PurchasePrice],
			lp.[PurchasePrice] * ISNULL([a1].[NumberInStock], [p].[NumberInStock]) 'TotalValue'
		FROM
			[product].[tProduct] p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			INNER JOIN [product].[tCategory] c ON [c].[CategoryId] = [p].[CategoryId]
			INNER JOIN [product].[tCategory] c2 ON [c2].[CategoryId] = [c].[ParentCategoryId]
			INNER JOIN [product].[tCategory] c3 ON [c3].[CategoryId] = [c2].[ParentCategoryId]
			LEFT JOIN [lekmer].[tProductSize] ps ON [ps].[ProductId] = [p].[ProductId] -- <-- fel
			LEFT JOIN [integration].[tProductStockPosition] psp ON [ps].[ErpId] = [psp].[HYarticleId]
			CROSS APPLY (SELECT SUM([tps].[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] tps WHERE [tps].[ProductId] = [p].[ProductId]) a1
		WHERE
			[p].[MediaId] IS NULL
			AND ([p].[NumberInStock] > 0 AND [ps].[NumberInStock] IS NULL OR [ps].[NumberInStock] > 0)
			AND [c3].[CategoryId] != 1001310 --'barnkläder'
			AND [p].[IsDeleted] = 0
	)

	SELECT
		[u].[HYErpId],
		[u].[Lagerplats],
		[u].[Title],
		[b].[Title] AS BrandTitle,
		[u].[LekmerErpId] AS LekmerArticleNumber,
		[u].[NumberInStock],
		[u].[PurchasePrice],
		[u].[TotalValue]
	FROM
		UniqueErpIdLekmer u
		LEFT OUTER JOIN [lekmer].[tBrand] b ON [b].[BrandId] = [u].[BrandId]
	WHERE
		[u].[RowNo] = 1
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pVoucherActionSave]'
GO
ALTER PROCEDURE [lekmer].[pVoucherActionSave]
	@ActionId	int,
	@Fixed bit,
	@Percentage bit,
	@DiscountValue decimal(16,2),
	@AllowSpecialOffer BIT = NULL
AS 
BEGIN 
	UPDATE
		lekmer.tVoucherAction
	SET
		Fixed = @Fixed,
		Percentage = @Percentage,
		DiscountValue = @DiscountValue,
		[AllowSpecialOffer] = @AllowSpecialOffer
	WHERE
		CartActionId = @ActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT 
			lekmer.tVoucherAction
		(
			CartActionId,
			Fixed,
			Percentage,
			DiscountValue,
			[AllowSpecialOffer]
		)
		VALUES
		(
			@ActionId,
			@Fixed,
			@Percentage,
			@DiscountValue,
			@AllowSpecialOffer
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [integration].[pReportProductsChildrenClothsMissingImages]'
GO
ALTER PROCEDURE [integration].[pReportProductsChildrenClothsMissingImages]
AS
BEGIN
	WITH UniqueErpIdLekmer AS
	(
		SELECT
			ROW_NUMBER() OVER(PARTITION BY [lp].[HYErpId] ORDER BY [lp].[HYErpId]) RowNo,
			[lp].[HYErpId],
			[psp].[Lagerplats],
			[p].[Title],
			[lp].[BrandId],
			[lp].[LekmerErpId],
			[ps].[ErpId],
			ISNULL([a1].[NumberInStock], [p].[NumberInStock]) 'NumberInStock',
			lp.[PurchasePrice],
			lp.[PurchasePrice] * ISNULL([a1].[NumberInStock], [p].[NumberInStock]) 'TotalValue'
		FROM
			[product].[tProduct] p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			INNER JOIN [product].[tCategory] c ON [c].[CategoryId] = [p].[CategoryId]
			INNER JOIN [product].[tCategory] c2 ON [c2].[CategoryId] = [c].[ParentCategoryId]
			INNER JOIN [product].[tCategory] c3 ON [c3].[CategoryId] = [c2].[ParentCategoryId]
			LEFT JOIN [lekmer].[tProductSize] ps ON [ps].[ProductId] = [p].[ProductId] -- <-- fel
			LEFT JOIN [integration].[tProductStockPosition] psp ON COALESCE([ps].[ErpId], [lp].[HYErpId] + '-000') = [psp].[HYarticleId]
			CROSS APPLY (SELECT SUM([tps].[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] tps WHERE [tps].[ProductId] = [p].[ProductId]) a1
		WHERE
			[p].[MediaId] IS NULL
			AND ([p].[NumberInStock] > 0 AND [ps].[NumberInStock] IS NULL OR [ps].[NumberInStock] > 0)
			AND [c3].[CategoryId] = 1001310 --'barnkläder'
			AND [p].[IsDeleted] = 0
	)

	SELECT
		[u].[HYErpId],
		[u].[Lagerplats],
		[u].[Title],
		[b].[Title] AS BrandTitle,
		[u].[LekmerErpId] AS SupplierArticleNumber,
		[u].[NumberInStock],
		[u].[PurchasePrice],
		[u].[TotalValue]
	FROM
		UniqueErpIdLekmer u
		LEFT OUTER JOIN [lekmer].[tBrand] b ON [b].[BrandId] = [u].[BrandId]
	WHERE
		[u].[RowNo] = 1
	ORDER BY
		[u].[ErpId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
