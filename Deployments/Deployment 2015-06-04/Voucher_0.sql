SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [backoffice].[pReportHeppoGetUsedVocherInfo]'
GO
DROP PROCEDURE [backoffice].[pReportHeppoGetUsedVocherInfo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [heppo].[pReportManagerRenameCode]'
GO
DROP PROCEDURE [heppo].[pReportManagerRenameCode]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [heppo].[pReportManagerGetVocherInfoByVoucherInfoIdOrVocherCode]'
GO
DROP PROCEDURE [heppo].[pReportManagerGetVocherInfoByVoucherInfoIdOrVocherCode]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [backoffice].[pReportManagerHeppoGenerateCodes]'
GO
DROP PROCEDURE [backoffice].[pReportManagerHeppoGenerateCodes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [heppo].[pReportManagerGenerateCodes]'
GO
DROP PROCEDURE [heppo].[pReportManagerGenerateCodes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [backoffice].[pReportManagerHeppoGetVocherInfoByBackIdOrVocherCode]'
GO
DROP PROCEDURE [backoffice].[pReportManagerHeppoGetVocherInfoByBackIdOrVocherCode]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [archive].[tVoucherInfo]'
GO
ALTER TABLE [archive].[tVoucherInfo] ADD
[SpecialOffer] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[tVoucherInfo]'
GO
ALTER TABLE [product].[tVoucherInfo] ADD
[SpecialOffer] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [backoffice].[pVoucherBatch]'
GO

ALTER PROCEDURE [backoffice].[pVoucherBatch]
	@ValidFrom		datetime,
	@ValidTo		datetime,
	@Prefix			varchar (32),
	@Suffix			varchar (32),
	@Quantity		int,
	@DiscountTypeId	int,
	@DiscountValue	decimal,
	@IsActive		bit,
	@ChannelGroupId	int,
	@DiscountTitle	nvarchar (250),
	@Description	nvarchar (250),
	@NbrOfToken		int,
	@OriginalStartQuantityPerCode	int,
	@ValidInDays	int,
	@CreatedBy		nvarchar(250) = NULL,
	@SpecialOffer	BIT = NULL
	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
	set nocount on
	begin try
		begin transaction
		
		-- Voucher 'GiftCard'
		IF (@DiscountTypeId = 3 AND @OriginalStartQuantityPerCode > 1)
		BEGIN
			RAISERROR ('The voucher info with discount type = "GiftCard" should has the @OriginalStartQuantityPerCode = 1.', 16, 1)
		END
		
		declare @Return int
		set @Return = 1
		
		insert into product.tVoucherInfo(
			ValidFrom,
			ValidTo,
			Prefix,
			Suffix,
			Quantity,
			DiscountTypeId,
			DiscountValue,
			IsActive,
			ChannelGroupId,
			DiscountTitle,
			[Description],
			NbrOfToken,
			OriginalStartQuantityPerCode,
			ValidInDays,
			CreatedBy,
			[SpecialOffer])
		select
			(CAST(@ValidFrom AS datetime)),
			(CAST(@ValidTo AS datetime)),
			@Prefix,
			@Suffix,
			@Quantity,
			@DiscountTypeId,
			@DiscountValue,
			@IsActive,
			@ChannelGroupId,
			@DiscountTitle,
			@Description,
			@NbrOfToken,
			@OriginalStartQuantityPerCode,
			@ValidInDays,
			@CreatedBy,
			@SpecialOffer
		
		set @Return = 0

	commit transaction
	return @Return
	end try
	begin catch
		set @Return = 5
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', @ErrMsg, GETDATE(), @SP)
		
		RETURN @Return
	END CATCH		 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [backoffice].[pVoucherCheck]'
GO

ALTER PROCEDURE [backoffice].[pVoucherCheck]
	@VoucherCode			NVARCHAR (50),
	@SiteApplicationName	NVARCHAR(40)
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		DECLARE @SiteId INT
		SET @SiteId = 0
		SELECT @SiteId = siteId FROM product.tSite WHERE SiteTitle = @SiteApplicationName

		IF @SiteId != 0
			BEGIN
				IF EXISTS 
				(SELECT * FROM product.tVoucher v
					INNER JOIN product.tVoucherInfo vi on v.VoucherInfoId = vi.VoucherInfoId
					INNER JOIN product.tChannelGroup chg on chg.ChannelGroupId = vi.ChannelGroupId
				 WHERE
					VoucherCode = @VoucherCode
					AND (QuantityLeft > 0 ) 
					AND ((vi.ValidTo IS NULL OR GETDATE() < (vi.ValidTo)) 
						  AND (v.ValidTo IS NULL OR GETDATE() < (v.ValidTo)))										
					AND (vi.IsActive = 1)
					AND (v.IsActive = 1 OR v.IsActive IS NULL)	
					AND (vi.ValidFrom < GETDATE())
					AND (v.Reserved = 0 OR v.Reserved IS NULL)
					AND chg.SiteId = @SiteId
					-- Voucher 'GiftCard'
					AND (vi.DiscountTypeId <> 3 OR v.AmountLeft > 0)
				 )
					BEGIN
						SELECT 
							vi.DiscountValue, 
							vi.DiscountTypeId,
							vi.VoucherInfoId,
							v.AmountLeft,
							vi.[SpecialOffer]
						FROM 
							product.tVoucher v 
							INNER JOIN product.tVoucherInfo vi ON v.VoucherInfoId = vi.VoucherInfoId
						WHERE 
							v.VoucherCode = @VoucherCode 
					END				
			END
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
					
	END CATCH	 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [archive].[pArchiveVouchers]'
GO

ALTER PROCEDURE [archive].[pArchiveVouchers]

AS
BEGIN
	SET NOCOUNT ON
		
		DECLARE @ArchivingSuccessful BIT
		DECLARE @Date DATETIME
		SET @Date = DATEADD(day, -14, GETDATE())
		SET @ArchivingSuccessful = 1

		DECLARE @VoucherBatchesToArchive TABLE 
		(
			VoucherInfoId	int	primary key with (ignore_dup_key = on)
		)		

		-- Expired Batches that have validTo date in voucherInfo
		INSERT INTO @VoucherBatchesToArchive (VoucherInfoId)
		SELECT
			VoucherInfoId
		FROM
			product.tVoucherInfo
		WHERE
			ValidTo IS NOT NULL
		AND	ValidTo < @Date


		-- Expired Batches that have validTo date
		INSERT INTO @VoucherBatchesToArchive (VoucherInfoId)
		SELECT
			vi.VoucherInfoId
		FROM
			product.tVoucherInfo vi
		WHERE
			vi.ValidTo IS NULL
		AND	EXISTS 
				(
					SELECT 1 FROM product.tVoucher v WHERE v.VoucherInfoId = vi.VoucherInfoId AND v.ValidTo < @Date
				)
		

		DECLARE @NumberOfVouchersInBatch INT
		DECLARE @VoucherInfoId INT
		DECLARE @BatchArchiveSize INT
		SET @BatchArchiveSize = 1000

		-- Loop through all batches
		DECLARE VoucherInfo CURSOR FAST_FORWARD FOR
		SELECT VoucherInfoId
		FROM @VoucherBatchesToArchive
		OPEN VoucherInfo;
		FETCH NEXT FROM VoucherInfo INTO @VoucherInfoId;
			WHILE @@FETCH_STATUS = 0
			BEGIN
				print '--- Archiving  BatchId ' + cast(@VoucherInfoId as nvarchar(10)) + '---'
				SET @NumberOfVouchersInBatch = (select count(*) from product.tVoucher where VoucherInfoId = @VoucherInfoId)

				BEGIN TRY
					BEGIN TRANSACTION

					-- if voucherid not in archive
					IF @VoucherInfoId NOT IN (SELECT VoucherInfoId FROM archive.tVoucherInfo)
					BEGIN
						-- Copy the batch
						INSERT INTO archive.tVoucherInfo
						(
							VoucherInfoId,
							ValidFrom,
							ValidTo,
							Prefix,
							Suffix,
							Quantity,
							DiscountTypeId,
							DiscountValue,
							IsActive,
							ChannelGroupId,
							DiscountTitle,
							[Description],
							NbrOfToken,
							OriginalStartQuantityPerCode,
							ValidInDays,
							CreatedBy,
							[CategoryId],
							[SpecialOffer]
						)
						SELECT
							VoucherInfoId,
							ValidFrom,
							ValidTo,
							Prefix,
							Suffix,
							Quantity,
							DiscountTypeId,
							DiscountValue,
							IsActive,
							ChannelGroupId,
							DiscountTitle,
							[Description],
							NbrOfToken,
							OriginalStartQuantityPerCode,
							ValidInDays,
							CreatedBy,
							[CategoryId],
							[SpecialOffer]
						FROM
							product.tVoucherInfo
						WHERE
							VoucherInfoId = @VoucherInfoId
					END

						-- WHILE LOOP
					WHILE @NumberOfVouchersInBatch > 0
					BEGIN
						print 'BatchId: ' + cast(@VoucherInfoId as nvarchar(10)) + ' Vouchers left to archive: ' + cast(@NumberOfVouchersInBatch as nvarchar(10))
						-- Copy @BatchArchiveSize to Archive table
						INSERT INTO archive.tVoucher
						(
							VoucherId,
							VoucherInfoId,
							VoucherCode,
							QuantityLeft,
							ValidTo,
							IsActive,
							Reserved,
							AmountLeft
						)
						SELECT TOP (@BatchArchiveSize)
							VoucherId,
							VoucherInfoId,
							VoucherCode,
							QuantityLeft,
							ValidTo,
							IsActive,
							Reserved,
							AmountLeft
						FROM
							product.tVoucher v
						WHERE
							v.VoucherInfoId = @VoucherInfoId
							
						-- Delete these codes from tVocher
						DELETE TOP (@BatchArchiveSize) v
						FROM product.tVoucher v
						WHERE v.VoucherInfoId = @VoucherInfoId

						SET @NumberOfVouchersInBatch = @NumberOfVouchersInBatch - @BatchArchiveSize
					END

					-- Delete the batch (from tVoucherInfo)
					IF @VoucherInfoId IN (SELECT VoucherInfoId FROM archive.tVoucherInfo)
					BEGIN
						DELETE FROM product.tVoucherInfo
						WHERE VoucherInfoId = @VoucherInfoId
						print 'BatchId: ' + cast(@VoucherInfoId as nvarchar(10)) + ' Archived and Deleted from main schema'
					END

					COMMIT TRANSACTION

				END TRY
				BEGIN CATCH
					
					IF @@trancount > 0 ROLLBACK TRANSACTION

					SET @ArchivingSuccessful = 0

					DECLARE @ErrMsg NVARCHAR(2048), @SP NVARCHAR(256), @Severity INT, @State INT
					SELECT @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
					INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
					VALUES('BatchId: ' + cast(@VoucherInfoId as nvarchar(10)) + 'falied', @ErrMsg, GETDATE(), @SP)

				END CATCH
				
				FETCH NEXT FROM VoucherInfo INTO @VoucherInfoId;
			END;

		CLOSE VoucherInfo;
		DEALLOCATE VoucherInfo;

		IF @ArchivingSuccessful = 1
		BEGIN
			print 'Vouchers have been archived successfully'
		END
		ELSE
		BEGIN
			print 'Not all vouchers have been archived successfully, please check you logs for more information'
		END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [backoffice].[pVoucherConsume]'
GO

ALTER PROCEDURE [backoffice].[pVoucherConsume]
	@VoucherCode	NVARCHAR (50),
	@OrderId		INT,
	@UsedAmount		DECIMAL(18,0) = NULL
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION

			-- Voucher 'GiftCard'
			IF EXISTS (SELECT 1
					   FROM product.tVoucher v
					   INNER JOIN product.tVoucherInfo vi on vi.VoucherInfoId = v.VoucherInfoId
					   WHERE v.VoucherCode = @VoucherCode
					   AND vi.DiscountTypeId = 3)
				BEGIN
					DECLARE @AmountLeft DECIMAL(18,0)
					SELECT @AmountLeft = v.AmountLeft - @UsedAmount FROM product.tVoucher v WHERE v.VoucherCode = @VoucherCode
										
					UPDATE 
						v 
					SET 
						v.AmountLeft = @AmountLeft
						,v.QuantityLeft = (CASE WHEN @AmountLeft <= 0 THEN v.QuantityLeft - 1 ELSE v.QuantityLeft END)
					FROM
						product.tVoucher v
					WHERE
						v.VoucherCode = @VoucherCode
				END
			-- Voucher other
			ELSE
				BEGIN
					UPDATE 
						v 
					SET 
						v.QuantityLeft = (v.QuantityLeft - 1)
					FROM
						product.tVoucher v
					WHERE
						v.VoucherCode = @VoucherCode
				END
			
			SELECT 
				vi.DiscountValue, 
				dt.DiscountTypeId,
				vi.VoucherInfoId,
				v.AmountLeft,
				vi.[SpecialOffer]
			FROM 
				product.tVoucher v 
				INNER JOIN product.tVoucherInfo vi ON v.VoucherInfoId = vi.VoucherInfoId
				INNER JOIN product.tDiscountType dt ON vi.DiscountTypeId = dt.DiscountTypeId
			WHERE 
				v.VoucherCode = @VoucherCode 
		
			INSERT INTO product.tVoucherLog(VoucherId, UsageDate, OrderId, AmountUsed)
			SELECT
				(SELECT VoucherId FROM product.tVoucher WHERE VoucherCode = @VoucherCode),
				GETDATE(),
				@OrderId,
				@UsedAmount
									
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		if @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [product].[tVoucherErrorLog](Data, [Message], [Date], OcuredInProcedure)
			   VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [backoffice].[pVoucherBatchCreate]'
GO
ALTER PROCEDURE [backoffice].[pVoucherBatchCreate]
	@ValidFrom		DATETIME,
	@Prefix			VARCHAR (32),
	@Suffix			VARCHAR (32),
	@Quantity		INT,
	@DiscountValue	DECIMAL,
	@AppName		NVARCHAR(250),
	@NbrOfToken		INT,
	@CreatedBy		NVARCHAR(250),
	@SpecialOffer	BIT = NULL
AS
BEGIN
	DECLARE @DiscountTypeId	INT
	SET @DiscountTypeId = (SELECT DiscountTypeId FROM [product].[tDiscountType] WHERE DiscountType = 'GiftCard')
	
	DECLARE @ChannelGroupId	INT
	SET @ChannelGroupId = ( SELECT TOP(1) chg.ChannelGroupId
							FROM [product].[tChannelGroup] chg
							INNER JOIN [product].[tSite] s ON s.[SiteId] = chg.[SiteId]
							WHERE s.[SiteTitle] = @AppName)

	INSERT INTO [product].[tVoucherInfo] (
		ValidFrom,
		ValidTo,
		Prefix,
		Suffix,
		Quantity,
		DiscountTypeId,
		DiscountValue,
		IsActive,
		ChannelGroupId,
		DiscountTitle,
		[Description],
		NbrOfToken,
		OriginalStartQuantityPerCode,
		ValidInDays,
		CreatedBy,
		[SpecialOffer]
	)
	SELECT
		(CAST(@ValidFrom AS DATETIME)),
		NULL,
		@Prefix,
		@Suffix,
		@Quantity,
		@DiscountTypeId,
		@DiscountValue,
		1,
		@ChannelGroupId,
		'GiftCardViaEmail',
		'Gift card via email batch',
		@NbrOfToken,
		1,
		NULL,
		@CreatedBy,
		@SpecialOffer
		
	SELECT @@IDENTITY
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [backoffice].[pReportManagerLekmerGenerateCodes]'
GO
-- Authenticate user  (SHA1)
	-- username OK
	-- password OK
	-- online OK
-- Create Batch With username
-- Create codes
-- Create a new Log Table to log all events (CreateVoucherLog) (VoucherInfoId, date, user, ?)
-- Testa på test först!
ALTER PROCEDURE [backoffice].[pReportManagerLekmerGenerateCodes]
	@ValidFrom		datetime,
	@ValidTo		datetime,
	--@Prefix			nvarchar(50) = NULL,
	--@Suffix			nvarchar(50) = NULL,
	@Quantity		int,
	@DiscountTypeId	int, 
	@DiscountValue	decimal,
	--@IsActive		int,
	@ChannelGroupId	int,
	@Description	nvarchar(250) = NULL,
	@DiscountTitle	nvarchar(250) = NULL,
	--@NbrOfToken		int,
	@OriginalStartQuantityPerCode	int, 
	--@ValidInDays	int,
	@Username		nvarchar(50),
	@Password		varchar(50),
	@SpecialOffer	BIT = NULL
AS
begin
	set nocount on
	
	DECLARE @tmpTable TABLE (BatchId int not null)
	DECLARE @BatchId int
	DECLARE	@Hexbin varbinary(max)
	DECLARE @tmpPassword nvarchar(50)
	DECLARE @ValidateRestrictions nvarchar(70)
	DECLARE @MaximumAmount decimal
	DECLARE @MaximumPercantage int
	
	begin try
		begin transaction

		set @ValidFrom = replace(cast(convert(varchar, @ValidFrom, 102) as varchar), '.', '-') + ' 00:00:00'
		set @ValidTo = replace(cast(convert(varchar, @ValidTo, 102) as varchar), '.', '-') + ' 00:00:00'
		set @ValidTo = DATEADD(SS,86399,@ValidTo)
		
		set @MaximumAmount = 0
		set @MaximumPercantage = 0
		
		if @DiscountTypeId = 4 -- Code
		begin
			SET @DiscountValue = 0 -- No discount
		end
		
		if @DiscountTypeId = 1 -- Percentage
		begin
			set @MaximumPercantage = @DiscountValue
		end
		else
		begin
			set @MaximumAmount = @DiscountValue
		end
		
		
		-- Authenticate user
		set @tmpPassword = (select [Password] from Lekmer.[security].tSystemUser where Username = @Username)
		set @Hexbin = (select (hashbytes('SHA1', @Password)))
		
		set @ValidateRestrictions = (select [user].[fValidateVoucherCreationRestrictions]
				(@Username,@MaximumAmount,@MaximumPercantage,@Quantity,@OriginalStartQuantityPerCode))		
		
	
		if @tmpPassword = (CONVERT(varchar(max), @Hexbin, 2)) -- Password OK
			and (select StatusId from Lekmer.[security].tSystemUser where Username = @Username) = 0 -- Online status OK
			and (@ValidateRestrictions is not null)  -- ValidateRestrictions OK
		begin
		
			-- Create a Batch
			insert into product.tVoucherInfo (
				ValidFrom,
				ValidTo,
				Prefix,
				Suffix,
				Quantity,
				DiscountTypeId,
				DiscountValue,
				IsActive,
				ChannelGroupId,
				[Description],
				DiscountTitle,
				NbrOfToken,
				OriginalStartQuantityPerCode,
				CreatedBy,
				[SpecialOffer]
				--,ValidInDays
			)
			output inserted.VoucherInfoId INTO @tmpTable
			values (
				@ValidFrom, -- lägg till 00:00:00 -- Convert(varchar, GETDATE(), 112)
				@ValidTo,
				'',
				'',
				@Quantity, -- Quantity hur många koder som ska skapas
				@DiscountTypeId,
				@DiscountValue,
				1,
				@ChannelGroupId,
				@Description,
				@DiscountTitle,
				10,
				@OriginalStartQuantityPerCode,
				@Username,
				@SpecialOffer
			)
			
			set @BatchId = (select top 1 BatchId from @tmpTable)
			print @BatchId
			delete from @tmpTable
			
			exec [backoffice].[pVoucherGenerateCodes] @BatchId, @Quantity, null, null
		end
		
		commit	
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		insert into [product].[tVoucherCreationLog](VoucherInfoId, [Date], CreatedBy, [Message])
		values(@BatchId, GETDATE(), @Username, ERROR_MESSAGE() + ' ' + ERROR_PROCEDURE())
		

	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pReportManagerGenerateCodes]'
GO

ALTER PROCEDURE [lekmer].[pReportManagerGenerateCodes]
	@ValidFrom		datetime,
	@ValidTo		datetime,
	--@Prefix			nvarchar(50) = NULL,
	--@Suffix			nvarchar(50) = NULL,
	@Quantity		int,
	@DiscountTypeId	int, 
	@DiscountValue	decimal,
	--@IsActive		int,
	@ChannelGroupId	int,
	@Description	nvarchar(250) = NULL,
	@DiscountTitle	nvarchar(250) = NULL,
	--@NbrOfToken		int,
	@OriginalStartQuantityPerCode	int, 
	--@ValidInDays	int,
	@Username		nvarchar(50),
	@Password		varchar(50),
	@VoucherCategoryId INT,
	@SpecialOffer	BIT = NULL
AS
begin
	set nocount on
	
	DECLARE @tmpTable TABLE (BatchId int not null)
	DECLARE @BatchId int
	DECLARE	@Hexbin varbinary(max)
	DECLARE @tmpPassword nvarchar(50)
	DECLARE @ValidateRestrictions nvarchar(70)
	DECLARE @MaximumAmount decimal
	DECLARE @MaximumPercantage int
	
	begin try
		begin transaction

		set @ValidFrom = replace(cast(convert(varchar, @ValidFrom, 102) as varchar), '.', '-') + ' 00:00:00'
		set @ValidTo = replace(cast(convert(varchar, @ValidTo, 102) as varchar), '.', '-') + ' 00:00:00'
		set @ValidTo = DATEADD(SS,86399,@ValidTo)
		
		set @MaximumAmount = 0
		set @MaximumPercantage = 0
		
		if @DiscountTypeId = 4 -- Code
		begin
			SET @DiscountValue = 0 -- No discount
		end
		
		if @DiscountTypeId = 1 -- Percentage
		begin
			set @MaximumPercantage = @DiscountValue
		end
		else
		begin
			set @MaximumAmount = @DiscountValue
		end
		
		
		-- Authenticate user
		set @tmpPassword = (select [Password] from Lekmer.[security].tSystemUser where Username = @Username)
		set @Hexbin = (select (hashbytes('SHA1', @Password)))
		
		set @ValidateRestrictions = (select [user].[fValidateVoucherCreationRestrictions]
				(@Username,@MaximumAmount,@MaximumPercantage,@Quantity,@OriginalStartQuantityPerCode))		
		
	
		if @tmpPassword = (CONVERT(varchar(max), @Hexbin, 2)) -- Password OK
			and (select StatusId from Lekmer.[security].tSystemUser where Username = @Username) = 0 -- Online status OK
			and (@ValidateRestrictions is not null)  -- ValidateRestrictions OK
		begin
		
			-- Create a Batch
			insert into product.tVoucherInfo 
			(
				ValidFrom,
				ValidTo,
				Prefix,
				Suffix,
				Quantity,
				DiscountTypeId, 
				DiscountValue,
				IsActive,
				ChannelGroupId,
				[Description], 
				DiscountTitle,
				NbrOfToken,
				OriginalStartQuantityPerCode,
				CreatedBy,
				CategoryId,
				[SpecialOffer]
			 ) --,ValidInDays)
			output inserted.VoucherInfoId INTO @tmpTable
			values
			(
				@ValidFrom, -- lägg till 00:00:00 -- Convert(varchar, GETDATE(), 112)
				@ValidTo,
				'',
				'',
				@Quantity, -- Quantity hur många koder som ska skapas
				@DiscountTypeId,
				@DiscountValue,
				1,
				@ChannelGroupId,
				@Description,
				@DiscountTitle,
				10,
				@OriginalStartQuantityPerCode,
				@Username,
				@VoucherCategoryId,
				@SpecialOffer
			)
			
			set @BatchId = (select top 1 BatchId from @tmpTable)
			print @BatchId
			delete from @tmpTable
			
			exec [backoffice].[pVoucherGenerateCodes] @BatchId, @Quantity, null, null
		end
		
		commit	
	end try
	begin catch
		if @@trancount > 0 rollback transaction
		
		insert into [product].[tVoucherCreationLog](VoucherInfoId, [Date], CreatedBy, [Message])
		values(@BatchId, GETDATE(), @Username, ERROR_MESSAGE() + ' ' + ERROR_PROCEDURE())
		

	end catch		 
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
