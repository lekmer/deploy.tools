/*
Run this script on:

        (local).Lekmer_212_0    -  This database will be modified

to synchronize it with:

        (local).Lekmer_212_1

You are recommended to back up your database before running this script

Script created by SQL Compare version 9.0.0 from Red Gate Software Ltd at 13.06.2012 15:13:35

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [lekmer].[tVoucherCondition]'
GO
ALTER TABLE [lekmer].[tVoucherCondition] DROP
CONSTRAINT [FK_tVoucherCondition_tCondition]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [lekmer].[tVoucherBatches]'
GO
ALTER TABLE [lekmer].[tVoucherBatches] DROP
CONSTRAINT [FK_tVoucherBatches_tCondition]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tVoucherCondition]'
GO
ALTER TABLE [lekmer].[tVoucherCondition] DROP CONSTRAINT [PK_tVoucherCondition]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [lekmer].[tVoucherBatches]'
GO
ALTER TABLE [lekmer].[tVoucherBatches] DROP CONSTRAINT [PK_tVoucherBatches_1]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pVoucherConditionBatcheInsert]'
GO
DROP PROCEDURE [lekmer].[pVoucherConditionBatcheInsert]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pVoucherConditionDeleteAll]'
GO
DROP PROCEDURE [lekmer].[pVoucherConditionDeleteAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[pVoucherConditionGetBatcheIdsByConditionId]'
GO
DROP PROCEDURE [lekmer].[pVoucherConditionGetBatcheIdsByConditionId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [lekmer].[tVoucherBatches]'
GO
DROP TABLE [lekmer].[tVoucherBatches]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [lekmer].[tVoucherCondition]'
GO
CREATE TABLE [lekmer].[tmp_rg_xx_tVoucherCondition]
(
[ConditionId] [int] NOT NULL,
[IncludeAllBatchIds] [bit] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [lekmer].[tmp_rg_xx_tVoucherCondition]([ConditionId]) SELECT [ConditionId] FROM [lekmer].[tVoucherCondition]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [lekmer].[tVoucherCondition]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[lekmer].[tmp_rg_xx_tVoucherCondition]', N'tVoucherCondition'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tVoucherCondition] on [lekmer].[tVoucherCondition]'
GO
ALTER TABLE [lekmer].[tVoucherCondition] ADD CONSTRAINT [PK_tVoucherCondition] PRIMARY KEY CLUSTERED  ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pVoucherConditionSave]'
GO
ALTER PROCEDURE [lekmer].[pVoucherConditionSave]
	@ConditionId INT,
	@IncludeAllBatchIds BIT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tVoucherCondition]
	SET	
		[IncludeAllBatchIds] = @IncludeAllBatchIds
	WHERE
		[ConditionId] = @ConditionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [lekmer].[tVoucherCondition]
		( 
			[ConditionId],
			[IncludeAllBatchIds]
		)
		VALUES 
		(
			@ConditionId,
			@IncludeAllBatchIds
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tVoucherBatchesInclude]'
GO
CREATE TABLE [lekmer].[tVoucherBatchesInclude]
(
[ConditionId] [int] NOT NULL,
[BatchId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tVoucherBatchesInclude] on [lekmer].[tVoucherBatchesInclude]'
GO
ALTER TABLE [lekmer].[tVoucherBatchesInclude] ADD CONSTRAINT [PK_tVoucherBatchesInclude] PRIMARY KEY CLUSTERED  ([ConditionId], [BatchId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[tVoucherBatchesExclude]'
GO
CREATE TABLE [lekmer].[tVoucherBatchesExclude]
(
[ConditionId] [int] NOT NULL,
[BatchId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tVoucherBatchesExclude] on [lekmer].[tVoucherBatchesExclude]'
GO
ALTER TABLE [lekmer].[tVoucherBatchesExclude] ADD CONSTRAINT [PK_tVoucherBatchesExclude] PRIMARY KEY CLUSTERED  ([ConditionId], [BatchId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pVoucherActionDelete]'
GO
ALTER PROCEDURE [lekmer].[pVoucherActionDelete]
	@CartActionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE FROM [lekmer].tVoucherAction
	WHERE CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pVoucherConditionBatchIncludeInsert]'
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionBatchIncludeInsert]
	@ConditionId INT,
	@BatchId INT
AS 
BEGIN 
	SET NOCOUNT ON

	INSERT lekmer.tVoucherBatchesInclude
	( 
		ConditionId,
		BatchId
	)
	VALUES 
	(
		@ConditionId,
		@BatchId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pVoucherConditionBatchExcludeInsert]'
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionBatchExcludeInsert]
	@ConditionId INT,
	@BatchId INT
AS 
BEGIN 
	SET NOCOUNT ON

	INSERT lekmer.tVoucherBatchesExclude
	( 
		ConditionId,
		BatchId
	)
	VALUES 
	(
		@ConditionId,
		@BatchId
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pVoucherConditionDeleteBatchesAll]'
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionDeleteBatchesAll]
	@ConditionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE FROM lekmer.tVoucherBatchesInclude
	WHERE ConditionId = @ConditionId
	
	DELETE FROM lekmer.tVoucherBatchesExclude
	WHERE ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pVoucherConditionGetBatchIdsIncludeByConditionId]'
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionGetBatchIdsIncludeByConditionId]
	@ConditionId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		BatchId
	FROM
		lekmer.[tVoucherBatchesInclude]
	WHERE
		[ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pVoucherConditionGetBatchIdsExcludeByConditionId]'
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionGetBatchIdsExcludeByConditionId]
	@ConditionId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		BatchId
	FROM
		lekmer.[tVoucherBatchesExclude]
	WHERE
		[ConditionId] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pVoucherDiscountActionGetById]'
GO
ALTER PROCEDURE [lekmer].[pVoucherDiscountActionGetById]
	@ActionId int
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		CCA.*,
		VA.*
	FROM
		campaign.vCustomCartAction as CCA
		left join lekmer.tVoucherAction VA on VA.CartActionId=CCA.[CartAction.Id]
	WHERE
		[CartAction.Id] = @ActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pVoucherConditionDelete]'
GO
ALTER PROCEDURE [lekmer].[pVoucherConditionDelete]
	@ConditionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE FROM lekmer.tVoucherBatchesExclude
	WHERE ConditionId = @ConditionId
	
	DELETE FROM lekmer.[tVoucherBatchesInclude]
	WHERE ConditionId = @ConditionId

	DELETE FROM lekmer.tVoucherCondition
	WHERE ConditionId = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[vVoucherCondition]'
GO

CREATE VIEW [lekmer].[vVoucherCondition]
AS
	SELECT
		c.*,
		[vc].[IncludeAllBatchIds] AS 'VoucherCondition.IncludeAllBatchIds'
	FROM
		[lekmer].[tVoucherCondition] vc
		INNER JOIN [campaign].[vCustomCondition] c ON [vc].[ConditionId] = [c].[Condition.Id]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pVoucherConditionGetById]'
GO
ALTER PROCEDURE [lekmer].[pVoucherConditionGetById]
	@ConditionId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		vc.*
	FROM
		[lekmer].[vVoucherCondition] vc
	WHERE
		[vc].[Condition.Id] = @ConditionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tVoucherBatchesExclude]'
GO
ALTER TABLE [lekmer].[tVoucherBatchesExclude] ADD
CONSTRAINT [FK_tVoucherBatchesExclude_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tVoucherBatchesInclude]'
GO
ALTER TABLE [lekmer].[tVoucherBatchesInclude] ADD
CONSTRAINT [FK_tVoucherBatchesInclude_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [lekmer].[tVoucherCondition]'
GO
ALTER TABLE [lekmer].[tVoucherCondition] ADD
CONSTRAINT [FK_tVoucherCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
