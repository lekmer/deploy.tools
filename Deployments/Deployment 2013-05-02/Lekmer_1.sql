SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [campaignlek].[tFixedPriceAction]'
GO
CREATE TABLE [campaignlek].[tFixedPriceAction]
(
[ProductActionId] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL CONSTRAINT [DF_tFixedPriceAction_IncludeAllProducts] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedPriceAction] on [campaignlek].[tFixedPriceAction]'
GO
ALTER TABLE [campaignlek].[tFixedPriceAction] ADD CONSTRAINT [PK_tFixedPriceAction] PRIMARY KEY CLUSTERED  ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedPriceActionIncludeBrand]'
GO
CREATE TABLE [campaignlek].[tFixedPriceActionIncludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedPriceActionIncludeBrand] on [campaignlek].[tFixedPriceActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeBrand] ADD CONSTRAINT [PK_tFixedPriceActionIncludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedPriceActionExcludeBrand]'
GO
CREATE TABLE [campaignlek].[tFixedPriceActionExcludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedPriceActionExcludeBrand] on [campaignlek].[tFixedPriceActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeBrand] ADD CONSTRAINT [PK_tFixedPriceActionExcludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedPriceActionExcludeProduct]'
GO
CREATE TABLE [campaignlek].[tFixedPriceActionExcludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedPriceActionExcludeProduct] on [campaignlek].[tFixedPriceActionExcludeProduct]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeProduct] ADD CONSTRAINT [PK_tFixedPriceActionExcludeProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedPriceActionIncludeCategory]'
GO
CREATE TABLE [campaignlek].[tFixedPriceActionIncludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedPriceActionIncludeCategory] on [campaignlek].[tFixedPriceActionIncludeCategory]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeCategory] ADD CONSTRAINT [PK_tFixedPriceActionIncludeCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedPriceActionIncludeProduct]'
GO
CREATE TABLE [campaignlek].[tFixedPriceActionIncludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedPriceActionIncludeProduct] on [campaignlek].[tFixedPriceActionIncludeProduct]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeProduct] ADD CONSTRAINT [PK_tFixedPriceActionIncludeProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedPriceActionCurrency]'
GO
CREATE TABLE [campaignlek].[tFixedPriceActionCurrency]
(
[ProductActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedPriceActionCurrency] on [campaignlek].[tFixedPriceActionCurrency]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionCurrency] ADD CONSTRAINT [PK_tFixedPriceActionCurrency] PRIMARY KEY CLUSTERED  ([ProductActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[tFixedPriceActionExcludeCategory]'
GO
CREATE TABLE [campaignlek].[tFixedPriceActionExcludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tFixedPriceActionExcludeCategory] on [campaignlek].[tFixedPriceActionExcludeCategory]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeCategory] ADD CONSTRAINT [PK_tFixedPriceActionExcludeCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeBrandGetIdAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tFixedPriceActionExcludeBrand]
	WHERE 
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageRestrictionsUpdate]'
GO
CREATE PROCEDURE [productlek].[pPackageRestrictionsUpdate]
	@ProductId			INT,
	@BrandId			INT,
	@CategoryId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN
	-- Get Product Ids that need add to package restirctions.
	DECLARE @ProductRegIds [lekmer].[ProductRegistryProducts]

	IF @ProductId IS NOT NULL
	BEGIN
		INSERT INTO @ProductRegIds (ProductId, ProductRegistryId)
		SELECT p.[MasterProductId], pr.ID
		FROM (SELECT DISTINCT [pak].[MasterProductId]
				FROM [productlek].[tPackage] pak
				INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
				WHERE [pp].[ProductId] = @ProductId) p,
			  [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	IF @BrandId IS NOT NULL
	BEGIN
		INSERT INTO @ProductRegIds (ProductId, ProductRegistryId)
		SELECT p.[MasterProductId], pr.ID
		FROM (SELECT DISTINCT [pak].[MasterProductId]
				FROM [productlek].[tPackage] pak
				INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [pp].[ProductId]
				WHERE [lp].[BrandId] = @BrandId) p,
			  [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	IF @CategoryId IS NOT NULL
	BEGIN
		INSERT INTO @ProductRegIds (ProductId, ProductRegistryId)
		SELECT p.[MasterProductId], pr.ID
		FROM (SELECT DISTINCT [pak].[MasterProductId]
				FROM [productlek].[tPackage] pak
				INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
				INNER JOIN [product].[tProduct] lp ON [lp].[ProductId] = [pp].[ProductId]
				WHERE [lp].[CategoryId] IN (SELECT CategoryId FROM [product].[fnGetSubCategories] (@CategoryId))) p,
			  [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter) pr
	END

	-- Add package restrictions
	INSERT INTO [lekmer].[tProductRegistryRestrictionProduct] (
		[ProductRegistryId] ,
		[ProductId] ,
		[RestrictionReason] ,
		[UserId] ,
		[CreatedDate]
	)
	SELECT
		[ProductRegistryId],
		[ProductId],
		'Package has restricted include product/product category/product brand',
		NULL,
		GETDATE()
	FROM
		@ProductRegIds
	
	
	DELETE prp FROM product.tProductRegistryProduct prp
	INNER JOIN @ProductRegIds prIds ON prIds.ProductId = [prp].[ProductId] AND [prIds].[ProductRegistryId] = [prp].[ProductRegistryId]
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryProductDeleteByProduct]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryProductDeleteByProduct]
	@ProductId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN 
	DELETE 
		[product].[tProductRegistryProduct]
	WHERE 
		[ProductId] = @ProductId
		AND [ProductRegistryId] IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter))
		
	EXEC [productlek].[pPackageRestrictionsUpdate] @ProductId, NULL, NULL, @ProductRegistryIds, @Delimiter
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeProductInsert]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeProductInsert]
	@ProductActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionExcludeProduct] (
		ProductActionId,
		ProductId
	)
	VALUES (
		@ProductActionId,
		@ProductId
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeCategoryGetIdAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		CategoryId
	FROM 
		[campaignlek].[tFixedPriceActionExcludeCategory]
	WHERE 
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryProductDeleteByBrand]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryProductDeleteByBrand]
	@BrandId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN 
	DELETE prp 
	FROM [product].[tProductRegistryProduct] prp
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = prp.[ProductId]
	WHERE lp.[BrandId] = @BrandId
		  AND prp.[ProductRegistryId] IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter))
		  
	EXEC [productlek].[pPackageRestrictionsUpdate] NULL, @BrandId, NULL, @ProductRegistryIds, @Delimiter
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeCategoryDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeCategoryDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionIncludeCategory]
	WHERE
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeCategoryGetIdAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		CategoryId
	FROM 
		[campaignlek].[tFixedPriceActionIncludeCategory]
	WHERE 
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeProductGetIdAllSecure]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeProductGetIdAllSecure]
	@ProductActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tFixedPriceActionExcludeProduct] fdaep
		INNER JOIN [product].[tProduct] p ON fdaep.[ProductId] = p.[ProductId]
	WHERE
		fdaep.[ProductActionId] = @ProductActionId
		AND p.[IsDeleted] = 0
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeProductDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeProductDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionExcludeProduct]
	WHERE
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vFixedPriceActionCurrency]'
GO




CREATE VIEW [campaignlek].[vFixedPriceActionCurrency]
AS
	SELECT
		fdac.[ProductActionId] AS 'FixedPriceActionCurrency.ProductActionId',
		fdac.[CurrencyId] AS 'FixedPriceActionCurrency.CurrencyId',
		fdac.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tFixedPriceActionCurrency] fdac
		INNER JOIN [core].[vCustomCurrency] c ON fdac.[CurrencyId] = c.[Currency.Id]




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [productlek].[pPackageSetOfflineByIncludeProduct]'
GO
CREATE PROCEDURE [productlek].[pPackageSetOfflineByIncludeProduct]
	@ProductId	INT
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @PackageMasterProductIds TABLE(ProductId INT)
	
	INSERT INTO @PackageMasterProductIds ([ProductId])
	SELECT DISTINCT [pak].[MasterProductId]
	FROM [productlek].[tPackage] pak
		 INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
	WHERE [pp].[ProductId] = @ProductId
	
	UPDATE
		[product].[tProduct]
	SET	
		[ProductStatusId] = 1
	WHERE
		[ProductId] IN (SELECT [ProductId] FROM @PackageMasterProductIds)
	
	SELECT [ProductId] FROM @PackageMasterProductIds
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductGetIdAllByCampaignId]'
GO

ALTER PROCEDURE [lekmer].[pProductGetIdAllByCampaignId]
	@CampaignId		INT
AS
BEGIN
	SET NOCOUNT ON

---The logic also exists in lekmer.pProductTagAllForCampaigns, keep them identical!!

--PercentagePriceDiscountAction - inlclude brand
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--PercentagePriceDiscountAction - inlclude product
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId = @CampaignId
union
 --PercentagePriceDiscountAction - include category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --PercentagePriceDiscountAction - include category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1
 
 
 
 union
 
 
 
 -- ProductDiscount - include product
 select ProductId from campaign.tProductAction pa
inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pa.ProductActionId
where CampaignId = @CampaignId


union


-- FixedDiscount - include brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedDiscount - include product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedDiscount - include category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId

union


-- FixedPrice - include brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedPrice - include product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedPrice- include category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId


union

-- GiftVardViaMailProductAction - include brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--GiftVardViaMailProductAction - include product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId = @CampaignId
union 
 --GiftVardViaMailProductAction - include category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --GiftVardViaMailProductAction - include category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1



 
except




--PercentagePriceDiscountAction - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--PercentagePriceDiscountAction - exclude product
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId = @CampaignId
union
 --PercentagePriceDiscountAction - exclude category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --PercentagePriceDiscountAction - exclude category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1
 
 
 
 union
 
 
-- FixedDiscount - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedDiscount - exclude product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedDiscount - exclude category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId

union 

-- FixedPrice - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId = @CampaignId
union
-- FixedPrice - exclude product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId = @CampaignId
 union
-- FixedPrice - exclude category (this one has no option for sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId = @CampaignId


union

-- GiftVardViaMailProductAction - exclude brand
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId = @CampaignId
union
--GiftVardViaMailProductAction - exclude product
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId = @CampaignId
union 
 --GiftVardViaMailProductAction - exclude category (no sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 0
union
  --GiftVardViaMailProductAction - exclude category (with sub categories)
select ProductId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId = @CampaignId and caic.IncludeSubCategories = 1
 
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionCurrencyDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionCurrencyDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionCurrency]
	WHERE
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeProductDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeProductDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionIncludeProduct]
	WHERE
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeCategoryDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeCategoryDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionExcludeCategory]
	WHERE
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeBrandDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeBrandDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionExcludeBrand]
	WHERE
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeBrandDeleteAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeBrandDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionIncludeBrand]
	WHERE
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionDelete]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionDelete]
	@ProductActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pFixedPriceActionIncludeBrandDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedPriceActionExcludeBrandDeleteAll] @ProductActionId

	EXEC [campaignlek].[pFixedPriceActionIncludeCategoryDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedPriceActionExcludeCategoryDeleteAll] @ProductActionId

	EXEC [campaignlek].[pFixedPriceActionIncludeProductDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedPriceActionExcludeProductDeleteAll] @ProductActionId
	
	EXEC [campaignlek].[pFixedPriceActionCurrencyDeleteAll] @ProductActionId

	DELETE FROM [campaignlek].[tFixedPriceAction]
	WHERE ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [product].[fnGetPriceOfItemWithLowestPrice]'
GO


CREATE FUNCTION [product].[fnGetPriceOfItemWithLowestPrice]
(
	@CurrencyId INT,
	@ProductId INT,
	@CustomerId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @Price decimal(16,2)
	
	DECLARE @DateNow DATETIME
	SET @DateNow = GETDATE()
	
	SELECT TOP(1)
		@Price = pli.PriceIncludingVat		
	FROM
		product.tPriceListItem AS pli
		INNER JOIN product.tPriceList AS pl ON pl.PriceListId = pli.PriceListId
		LEFT JOIN product.tPriceListCustomerGroup AS cgp ON cgp.PriceListId = pl.PriceListId
	WHERE
		pl.PriceListStatusId = 0
		AND pl.CurrencyId = @CurrencyId
		AND (pl.StartDateTime IS NULL OR pl.StartDateTime <= @DateNow)
		AND (pl.EndDateTime   IS NULL OR @DateNow <= pl.EndDateTime)
		AND pli.ProductId = @ProductId
		AND ( -- if customer is not defined then should be available all pricelists that are not assigned to any customer group
			cgp.CustomerGroupId IS NULL
			OR
			(
				@CustomerId IS NOT NULL
				AND
				cgp.CustomerGroupId IN (
					SELECT
						cg.CustomerGroupId
					FROM
						customer.tCustomerGroup AS cg
						INNER JOIN customer.tCustomerGroupCustomer AS cgc ON cgc.CustomerGroupId = cg.CustomerGroupId
					WHERE
						cg.StatusId = 0
						AND cgc.CustomerId = @CustomerId
				)
			)
		)
	ORDER BY
		pli.PriceIncludingVat

	RETURN @Price
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [lekmer].[pProductGetPriceByChannelAndCampaign]'
GO

CREATE PROCEDURE [lekmer].[pProductGetPriceByChannelAndCampaign]
	@CurrencyId int,
	@Price decimal(16,2),
	@IncludeCategories as varchar(max) = '',
	@IncludeBrands as varchar(max) = '',
	@IncludeProducts as varchar(max) = '',
	@ExcludeCategories varchar(max) = '',
	@ExcludeBrands as varchar(max) = '',
	@ExcludeProducts as varchar(max) = ''
AS
BEGIN
	SET NOCOUNT ON

select pp.ErpId,  CAST([product].[fnGetPriceOfItemWithLowestPrice](@CurrencyId,pp.ProductId, null) as Decimal(16,2)) as Price from (
select Cast(SepString as INT) as ProductId from [generic].[fStringToStringTablePerformance](@IncludeProducts,',')
union
select ProductId from product.tProduct p
inner join [generic].[fStringToStringTablePerformance](@IncludeCategories,',') c on c.SepString = p.CategoryId
union
select ProductId from lekmer.tLekmerProduct p
inner join [generic].[fStringToStringTablePerformance](@IncludeBrands,',') c on c.SepString = p.BrandId
except
select Cast(SepString as INT) as ProductId from [generic].[fStringToStringTablePerformance](@ExcludeProducts,',')
union
select ProductId from product.tProduct p
inner join [generic].[fStringToStringTablePerformance](@ExcludeCategories,',') c on c.SepString = p.CategoryId
union
select ProductId from lekmer.tLekmerProduct p
inner join [generic].[fStringToStringTablePerformance](@ExcludeBrands,',') c on c.SepString = p.BrandId
) c
inner join product.tProduct pp on pp.ProductId = c.ProductId
where [product].[fnGetPriceOfItemWithLowestPrice](@CurrencyId,pp.ProductId, null) < @Price
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionSave]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT
AS
BEGIN
	UPDATE
		[campaignlek].[tFixedPriceAction]
	SET
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		ProductActionId = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tFixedPriceAction] (
			ProductActionId,
			IncludeAllProducts
		)
		VALUES (
			@ProductActionId,
			@IncludeAllProducts
		)
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductTagAllForCampaigns]'
GO

ALTER PROCEDURE [lekmer].[pProductTagAllForCampaigns]
AS
BEGIN
	SET NOCOUNT ON

--Create temporary table for holding campaigns to tag products for
CREATE TABLE #Campaigns (
CampaignId int
)

--Get all campaigns online, and with landing page selected (we dont care if landing page exists, we just tag?)
insert into #Campaigns select c.CampaignId from campaign.tCampaign c where c.UseLandingPage = 1 and c.CampaignStatusId = 0

--Delete all tags for these campaigns before inserting new tags
delete from lekmer.tProductTag where TagId in (select t.TagId from lekmer.tTag t inner join lekmer.tTagGroup tg on tg.TagGroupId = t.TagGroupId where tg.CommonName = 'auto-kampanj' and t.CommonName in (select CampaignId from #Campaigns))

--Do the major selection and filter. The logic is copy of lekmer.pProductGetIdAllByCampaignId (keep them identical!!)
INSERT INTO lekmer.tProductTag (ProductId, TagId)
select ProductId, TagId from (
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1
 union
 select ProductId, CampaignId from campaign.tProductAction pa
inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pa.ProductActionId
where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) 
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union 
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1
except
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) 
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union 
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1

) c
inner join lekmer.tTag t on t.CommonName = c.CampaignId
inner join lekmer.tTagGroup tg on tg.TagGroupId = t.TagGroupId
where tg.CommonName = 'auto-kampanj'


drop table #Campaigns

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeBrandInsert]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeBrandInsert]
	@ProductActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionExcludeBrand] (
		ProductActionId,
		BrandId
	)
	VALUES (
		@ProductActionId,
		@BrandId
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeProductGetIdAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@ProductActionId	INT
AS
BEGIN
	SELECT
		p.[Product.Id]
	FROM
		[campaignlek].[tFixedPriceActionIncludeProduct] fdaip
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = fdaip.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		fdaip.[ProductActionId] = @ProductActionId
		AND p.[Product.ChannelId] = @ChannelId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeCategoryInsert]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeCategoryInsert]
	@ProductActionId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionExcludeCategory] (
		ProductActionId,
		CategoryId
	)
	VALUES (
		@ProductActionId,
		@CategoryId
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeBrandGetIdAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tFixedPriceActionIncludeBrand]
	WHERE 
		ProductActionId = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionCurrencyGetByAction]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionCurrencyGetByAction]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedPriceActionCurrency]
	WHERE 
		[FixedPriceActionCurrency.ProductActionId] = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionCurrencyInsert]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionCurrencyInsert]
	@ProductActionId	INT,
	@CurrencyId			INT,
	@Value				DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionCurrency] (
		ProductActionId,
		CurrencyId,
		Value
	)
	VALUES (
		@ProductActionId,
		@CurrencyId,
		@Value
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[vFixedPriceAction]'
GO


CREATE VIEW [campaignlek].[vFixedPriceAction]
AS
	SELECT
		fda.[ProductActionId] AS 'FixedPriceAction.ProductActionId',
		fda.[IncludeAllProducts] AS 'FixedPriceAction.IncludeAllProducts',
		pa.*
	FROM
		[campaignlek].[tFixedPriceAction] fda
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = fda.[ProductActionId]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionExcludeProductGetIdAll]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@ProductActionId	INT
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		[campaignlek].[tFixedPriceActionExcludeProduct] fdaep
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = fdaep.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		fdaep.[ProductActionId] = @ProductActionId
		AND p.[Product.ChannelId] = @ChannelId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeCategoryInsert]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeCategoryInsert]
	@ProductActionId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionIncludeCategory] (
		ProductActionId,
		CategoryId
	)
	VALUES (
		@ProductActionId,
		@CategoryId
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeBrandInsert]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeBrandInsert]
	@ProductActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionIncludeBrand] (
		ProductActionId,
		BrandId
	)
	VALUES (
		@ProductActionId,
		@BrandId
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeProductGetIdAllSecure]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeProductGetIdAllSecure]
	@ProductActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tFixedPriceActionIncludeProduct] fdaip
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = fdaip.[ProductId]
	WHERE
		fdaip.[ProductActionId] = @ProductActionId
		AND p.[IsDeleted] = 0
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionIncludeProductInsert]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeProductInsert]
	@ProductActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionIncludeProduct] (
		ProductActionId,
		ProductId
	)
	VALUES (
		@ProductActionId,
		@ProductId
	)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [campaignlek].[pFixedPriceActionGetById]'
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionGetById]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedPriceAction]
	WHERE
		[FixedPriceAction.ProductActionId] = @ProductActionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [lekmer].[pProductRegistryProductDeleteByCategory]'
GO
ALTER PROCEDURE [lekmer].[pProductRegistryProductDeleteByCategory]
	@CategoryId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN 
	DELETE prp 
	FROM [product].[tProductRegistryProduct] prp
		 INNER JOIN [product].[tProduct] p ON p.[ProductId] = prp.[ProductId]
	WHERE p.[CategoryId] IN (SELECT CategoryId FROM [product].[fnGetSubCategories] (@CategoryId))
		  AND prp.[ProductRegistryId] IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter))
		  
	EXEC [productlek].[pPackageRestrictionsUpdate] NULL, NULL, @CategoryId, @ProductRegistryIds, @Delimiter
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedPriceAction]'
GO
ALTER TABLE [campaignlek].[tFixedPriceAction] ADD CONSTRAINT [FK_tFixedPriceAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedPriceActionIncludeBrand]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeBrand] ADD CONSTRAINT [FK_tFixedPriceActionIncludeBrand_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeBrand] ADD CONSTRAINT [FK_tFixedPriceActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedPriceActionExcludeBrand]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeBrand] ADD CONSTRAINT [FK_tFixedPriceActionExcludeBrand_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeBrand] ADD CONSTRAINT [FK_tFixedPriceActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedPriceActionExcludeProduct]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeProduct] ADD CONSTRAINT [FK_tFixedPriceActionExcludeProduct_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeProduct] ADD CONSTRAINT [FK_tFixedPriceActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedPriceActionIncludeCategory]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeCategory] ADD CONSTRAINT [FK_tFixedPriceActionIncludeCategory_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeCategory] ADD CONSTRAINT [FK_tFixedPriceActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedPriceActionIncludeProduct]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeProduct] ADD CONSTRAINT [FK_tFixedPriceActionIncludeProduct_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeProduct] ADD CONSTRAINT [FK_tFixedPriceActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedPriceActionCurrency]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionCurrency] ADD CONSTRAINT [FK_tFixedPriceActionCurrency_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
ALTER TABLE [campaignlek].[tFixedPriceActionCurrency] ADD CONSTRAINT [FK_tFixedPriceActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [campaignlek].[tFixedPriceActionExcludeCategory]'
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeCategory] ADD CONSTRAINT [FK_tFixedPriceActionExcludeCategory_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeCategory] ADD CONSTRAINT [FK_tFixedPriceActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
