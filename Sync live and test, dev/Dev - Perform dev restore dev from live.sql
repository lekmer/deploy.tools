USE [master]
GO

ALTER DATABASE [Lekmer] SET OFFLINE WITH ROLLBACK IMMEDIATE
GO

ALTER DATABASE [Lekmer] SET ONLINE
GO

RESTORE DATABASE [Lekmer] 
FROM  DISK = N'D:\Backup\Lekmer_Production_2014-03-04_1.bak'
WITH  FILE = 1,  
MOVE N'Scensum2' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Lekmer.mdf',  
MOVE N'fgOrderData' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Lekmer_1.ndf',  
MOVE N'Scensum2_log' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Lekmer_2.ldf',  
NOUNLOAD,  STATS = 10
GO

USE [master]
GO
ALTER DATABASE [Lekmer] SET RECOVERY SIMPLE WITH NO_WAIT
GO

USE [Lekmer]
GO
DBCC SHRINKFILE (N'Scensum2_log' , 0)
GO
