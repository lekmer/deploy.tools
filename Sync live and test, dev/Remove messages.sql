DELETE [messaging].[tRecipientCustomField]
GO
DELETE [messaging].[tRecipient]
GO
DELETE [messaging].[tMessageCustomField]
GO

WHILE EXISTS (SELECT TOP (1) 1 FROM [messaging].[tMessage])
DELETE TOP (10000) [messaging].[tMessage]
GO

DELETE [messaging].[tBatch]
GO

DELETE FROM [campaignlek].[tGiftCardViaMailInfo] WHERE [StatusId] != 2
GO

DELETE FROM [orderlek].[tKlarnaPendingOrder]
GO

UPDATE [orderlek].[tSavedCart] SET [IsNeedReminder] = 0 WHERE [IsNeedReminder] = 1
GO

TRUNCATE TABLE [lekmer].[tMonitorProduct]
GO

UPDATE [campaignlek].[tGiftCardViaMailInfo]
SET [StatusId] = 4
WHERE [StatusId] = 1
GO

DELETE FROM [orderlek].[tMaksuturvaPendingOrder]
GO

DELETE [integration].[tOrderQue]

UPDATE [order].[tOrder]
SET [OrderStatusId] = 3 -- Canceled
WHERE [OrderStatusId] = 2 -- Payment confirmed
GO