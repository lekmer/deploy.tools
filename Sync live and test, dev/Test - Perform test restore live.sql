USE [master]
GO

ALTER DATABASE [Lekmer] SET OFFLINE WITH ROLLBACK IMMEDIATE
GO

ALTER DATABASE [Lekmer] SET ONLINE
GO

RESTORE DATABASE [Lekmer] 
FROM  DISK = N'D:\Backups\Lekmer_Production_2014-03-04_1.bak'
WITH  FILE = 1,  
MOVE N'Scensum2' TO N'D:\MSSQL\Data\Lekmer_212.mdf',  
MOVE N'fgOrderData' TO N'D:\MSSQL\Data\Lekmer_212.ndf',  
MOVE N'Scensum2_log' TO N'D:\MSSQL\Data\Lekmer_212.ldf',  
NOUNLOAD,  STATS = 10
GO

USE [master]
GO
ALTER DATABASE [Lekmer] SET RECOVERY SIMPLE WITH NO_WAIT
GO

USE [Lekmer]
GO
DBCC SHRINKFILE (N'Scensum2_log' , 0)
GO
