DROP USER [scensum]
GO

CREATE USER [scensum] FOR LOGIN [scensum]
GO

EXEC sp_addrolemember N'db_owner', N'scensum'
GO



DROP USER [hyIntegration]
GO

CREATE USER [hyIntegration] FOR LOGIN [hyIntegration]
GO

EXEC sp_addrolemember N'db_owner', N'hyIntegration'
GO
