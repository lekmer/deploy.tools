title Remove all files

echo off

echo.
echo Removing all files...

del /s /f /q "Src\IoC\*.*"
del /s /f /q "Src\Setting\*.*"

del /s /f /q "Src\Litium.Lekmer.Api.WebService\*.*"
for /d %%i in ("Src\Litium.Lekmer.Api.WebService\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.Esales.WebService\*.*"
for /d %%i in ("Src\Litium.Lekmer.Esales.WebService\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Scensum.BackOffice\*.*"
for /d %%i in ("Src\Litium.Scensum.BackOffice\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Scensum.Media.Web\*.*"
for /d %%i in ("Src\Litium.Scensum.Media.Web\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Scensum.Web\*.*"
for /d %%i in ("Src\Litium.Scensum.Web\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.Order.MonitorWeb\*.*"
for /d %%i in ("Src\Litium.Lekmer.Order.MonitorWeb\*.*") do rd /s /q "%%i"

del /s /f /q "Voucher\*.*"
for /d %%i in ("Voucher\*.*") do rd /s /q "%%i"


del /s /f /q "Tools\Litium.Lekmer.Cache.Cleaner.Service\bin\Release\*.*"
for /d %%i in ("Tools\Litium.Lekmer.Cache.Cleaner.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.Campaign.CampaignTagging.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Campaign.CampaignTagging.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.CdonExport.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.CdonExport.Console\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.CdonExport.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.CdonExport.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Tools\Litium.Lekmer.Cleaner.Service\bin\Release\*.*"
for /d %%i in ("Tools\Litium.Lekmer.Cleaner.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.DataExport.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.DataExport.Console\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.DropShip.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.DropShip.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.Esales.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Esales.Console\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.Esales.ConsoleDefrag\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Esales.ConsoleDefrag\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.Esales.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Esales.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.FileExport.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.FileExport.Console\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.GFK.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.GFK.Console\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.GoogleFeed.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.GoogleFeed.Console\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Tools\Litium.Lekmer.FtpServer.Console\bin\Release\*.*"
for /d %%i in ("Tools\Litium.Lekmer.FtpServer.Console\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Tools\Litium.Lekmer.FtpServer.Service\bin\Release\*.*"
for /d %%i in ("Tools\Litium.Lekmer.FtpServer.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.IndexBuilder.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.IndexBuilder.Console\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.IndexBuilder.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.IndexBuilder.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.InsuranceInfo.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.InsuranceInfo.Console\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.InsuranceInfo.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.InsuranceInfo.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.MediaArchive.Cleanup.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.MediaArchive.Cleanup.Console\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.NewsletterSubscriber.Console\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.NewsletterSubscriber.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.OculusService\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.OculusService\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.Cart.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Cart.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.Order.MonitorConsole\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Order.MonitorConsole\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.Order.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Order.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.Payment.Maksuturva.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Payment.Maksuturva.Console\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.PageBuilder.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.PageBuilder.Console\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Tools\Litium.Lekmer.Payment.Klarna.FetchPClasses\bin\Release\*.*"
for /d %%i in ("Tools\Litium.Lekmer.Payment.Klarna.FetchPClasses\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.Product.MonitorConsole\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Product.MonitorConsole\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Src\Litium.Lekmer.Product.MonitorService\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Product.MonitorService\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.RatingReviewMessaging.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.RatingReviewMessaging.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.Pacsoft.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.Pacsoft.Console\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.SIR.Console\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.SIR.Console\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Src\Litium.Lekmer.TwosellExport.Service\bin\Release\*.*"
for /d %%i in ("Src\Litium.Lekmer.TwosellExport.Service\bin\Release\*.*") do rd /s /q "%%i"


del /s /f /q "Tools\Litium.Lekmer.StatistcsUtil.Console\bin\Release\*.*"
for /d %%i in ("Tools\Litium.Lekmer.StatistcsUtil.Console\bin\Release\*.*") do rd /s /q "%%i"

del /s /f /q "Tools\Litium.Lekmer.StatistcsUtil.Service\bin\Release\*.*"
for /d %%i in ("Tools\Litium.Lekmer.StatistcsUtil.Service\bin\Release\*.*") do rd /s /q "%%i"

echo.
echo All files removed.

echo.
pause

exit