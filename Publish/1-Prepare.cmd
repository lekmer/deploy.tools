title Prepare deployment files

echo off

echo.
echo Removing config files...

del /f /q "Src\Litium.Lekmer.Api.WebService\Web.config"
del /f /q "Src\Litium.Lekmer.Esales.WebService\Web.config"
del /f /q "Src\Litium.Scensum.BackOffice\Web.config"
del /f /q "Src\Litium.Scensum.Media.Web\Web.config"
del /f /q "Src\Litium.Scensum.Web\Web.config"
del /f /q "Src\Litium.Scensum.Web\redirect.txt"
del /f /q "Src\Litium.Scensum.Web\robots.txt"
del /f /q "Src\Litium.Lekmer.Order.MonitorWeb\Web.config"
del /f /q "Src\Litium.Lekmer.Order.MonitorWeb\Views\Web.config"

del /f /q "Voucher\Web.config"


del /f /q "Tools\Litium.Lekmer.Cache.Cleaner.Service\bin\Debug\Litium.Lekmer.Cache.Cleaner.Service.exe.config"
del /f /q "Tools\Litium.Lekmer.Cache.Cleaner.Service\bin\Debug\Litium.Lekmer.Cache.Cleaner.Service.vshost.exe.config"
del /f /q "Tools\Litium.Lekmer.Cache.Cleaner.Service\bin\Release\Litium.Lekmer.Cache.Cleaner.Service.exe.config"
del /f /q "Tools\Litium.Lekmer.Cache.Cleaner.Service\bin\Release\Litium.Lekmer.Cache.Cleaner.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.Campaign.CampaignTagging.Service\bin\Debug\Litium.Lekmer.Campaign.CampaignTagging.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Campaign.CampaignTagging.Service\bin\Debug\Litium.Lekmer.Campaign.CampaignTagging.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Campaign.CampaignTagging.Service\bin\Release\Litium.Lekmer.Campaign.CampaignTagging.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Campaign.CampaignTagging.Service\bin\Release\Litium.Lekmer.Campaign.CampaignTagging.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console\bin\Debug\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console.exe.config"
del /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console\bin\Debug\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console\bin\Release\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console.exe.config"
del /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console\bin\Release\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console.vshost.exe.config"

del /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service\bin\Debug\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service\bin\Debug\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service\bin\Release\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service\bin\Release\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.CdonExport.Console\bin\Debug\Litium.Lekmer.CdonExport.Console.exe.config"
del /f /q "Src\Litium.Lekmer.CdonExport.Console\bin\Debug\Litium.Lekmer.CdonExport.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.CdonExport.Console\bin\Release\Litium.Lekmer.CdonExport.Console.exe.config"
del /f /q "Src\Litium.Lekmer.CdonExport.Console\bin\Release\Litium.Lekmer.CdonExport.Console.vshost.exe.config"

del /f /q "Src\Litium.Lekmer.CdonExport.Service\bin\Debug\Litium.Lekmer.CdonExport.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.CdonExport.Service\bin\Debug\Litium.Lekmer.CdonExport.Service.exe.config"
del /f /q "Src\Litium.Lekmer.CdonExport.Service\bin\Release\Litium.Lekmer.CdonExport.Service.exe.config"
del /f /q "Src\Litium.Lekmer.CdonExport.Service\bin\Release\Litium.Lekmer.CdonExport.Service.vshost.exe.config"


del /f /q "Tools\Litium.Lekmer.Cleaner.Service\bin\Debug\Litium.Lekmer.Cleaner.Service.exe.config"
del /f /q "Tools\Litium.Lekmer.Cleaner.Service\bin\Debug\Litium.Lekmer.Cleaner.Service.vshost.exe.config"
del /f /q "Tools\Litium.Lekmer.Cleaner.Service\bin\Release\Litium.Lekmer.Cleaner.Service.exe.config"
del /f /q "Tools\Litium.Lekmer.Cleaner.Service\bin\Release\Litium.Lekmer.Cleaner.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.DataExport.Console\bin\Debug\Litium.Lekmer.DataExport.Console.exe.config"
del /f /q "Src\Litium.Lekmer.DataExport.Console\bin\Debug\Litium.Lekmer.DataExport.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.DataExport.Console\bin\Release\Litium.Lekmer.DataExport.Console.exe.config"
del /f /q "Src\Litium.Lekmer.DataExport.Console\bin\Release\Litium.Lekmer.DataExport.Console.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.DropShip.Service\bin\Debug\Litium.Lekmer.DropShip.Service.exe.config"
del /f /q "Src\Litium.Lekmer.DropShip.Service\bin\Debug\Litium.Lekmer.DropShip.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.DropShip.Service\bin\Release\Litium.Lekmer.DropShip.Service.exe.config"
del /f /q "Src\Litium.Lekmer.DropShip.Service\bin\Release\Litium.Lekmer.DropShip.Service.exe.config"


del /f /q "Src\Litium.Lekmer.Esales.Console\bin\Debug\Litium.Lekmer.Esales.Console.exe.config"
del /f /q "Src\Litium.Lekmer.Esales.Console\bin\Debug\Litium.Lekmer.Esales.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Esales.Console\bin\Release\Litium.Lekmer.Esales.Console.exe.config"
del /f /q "Src\Litium.Lekmer.Esales.Console\bin\Release\Litium.Lekmer.Esales.Console.vshost.exe.config"

del /f /q "Src\Litium.Lekmer.Esales.ConsoleDefrag\bin\Debug\Litium.Lekmer.Esales.ConsoleDefrag.exe.config"
del /f /q "Src\Litium.Lekmer.Esales.ConsoleDefrag\bin\Debug\Litium.Lekmer.Esales.ConsoleDefrag.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Esales.ConsoleDefrag\bin\Release\Litium.Lekmer.Esales.ConsoleDefrag.exe.config"
del /f /q "Src\Litium.Lekmer.Esales.ConsoleDefrag\bin\Release\Litium.Lekmer.Esales.ConsoleDefrag.vshost.exe.config"

del /f /q "Src\Litium.Lekmer.Esales.Service\bin\Debug\Litium.Lekmer.Esales.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Esales.Service\bin\Debug\Litium.Lekmer.Esales.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Esales.Service\bin\Release\Litium.Lekmer.Esales.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Esales.Service\bin\Release\Litium.Lekmer.Esales.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.FileExport.Console\bin\Debug\Litium.Lekmer.FileExport.Console.exe.config"
del /f /q "Src\Litium.Lekmer.FileExport.Console\bin\Debug\Litium.Lekmer.FileExport.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.FileExport.Console\bin\Release\Litium.Lekmer.FileExport.Console.exe.config"
del /f /q "Src\Litium.Lekmer.FileExport.Console\bin\Release\Litium.Lekmer.FileExport.Console.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.GFK.Console\bin\Debug\Litium.Lekmer.GFK.Console.exe.config"
del /f /q "Src\Litium.Lekmer.GFK.Console\bin\Debug\Litium.Lekmer.GFK.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.GFK.Console\bin\Release\Litium.Lekmer.GFK.Console.exe.config"
del /f /q "Src\Litium.Lekmer.GFK.Console\bin\Release\Litium.Lekmer.GFK.Console.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.GoogleFeed.Console\bin\Debug\Litium.Lekmer.GoogleFeed.Console.exe.config"
del /f /q "Src\Litium.Lekmer.GoogleFeed.Console\bin\Debug\Litium.Lekmer.GoogleFeed.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.GoogleFeed.Console\bin\Release\Litium.Lekmer.GoogleFeed.Console.exe.config"
del /f /q "Src\Litium.Lekmer.GoogleFeed.Console\bin\Release\Litium.Lekmer.GoogleFeed.Console.vshost.exe.config"


del /f /q "Tools\Litium.Lekmer.FtpServer.Console\bin\Debug\Litium.Lekmer.FtpServer.Console.exe.config"
del /f /q "Tools\Litium.Lekmer.FtpServer.Console\bin\Debug\Litium.Lekmer.FtpServer.Console.vshost.exe.config"
del /f /q "Tools\Litium.Lekmer.FtpServer.Console\bin\Release\Litium.Lekmer.FtpServer.Console.exe.config"
del /f /q "Tools\Litium.Lekmer.FtpServer.Console\bin\Release\Litium.Lekmer.FtpServer.Console.vshost.exe.config"

del /f /q "Tools\Litium.Lekmer.FtpServer.Service\bin\Debug\Litium.Lekmer.FtpServer.Service.exe.config"
del /f /q "Tools\Litium.Lekmer.FtpServer.Service\bin\Debug\Litium.Lekmer.FtpServer.Service.vshost.exe.config"
del /f /q "Tools\Litium.Lekmer.FtpServer.Service\bin\Release\Litium.Lekmer.FtpServer.Service.exe.config"
del /f /q "Tools\Litium.Lekmer.FtpServer.Service\bin\Release\Litium.Lekmer.FtpServer.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.IndexBuilder.Console\bin\Debug\Litium.Lekmer.IndexBuilder.Console.exe.config"
del /f /q "Src\Litium.Lekmer.IndexBuilder.Console\bin\Debug\Litium.Lekmer.IndexBuilder.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.IndexBuilder.Console\bin\Release\Litium.Lekmer.IndexBuilder.Console.exe.config"
del /f /q "Src\Litium.Lekmer.IndexBuilder.Console\bin\Release\Litium.Lekmer.IndexBuilder.Console.vshost.exe.config"

del /f /q "Src\Litium.Lekmer.IndexBuilder.Service\bin\Debug\Litium.Lekmer.IndexBuilder.Service.exe.config"
del /f /q "Src\Litium.Lekmer.IndexBuilder.Service\bin\Debug\Litium.Lekmer.IndexBuilder.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.IndexBuilder.Service\bin\Release\Litium.Lekmer.IndexBuilder.Service.exe.config"
del /f /q "Src\Litium.Lekmer.IndexBuilder.Service\bin\Release\Litium.Lekmer.IndexBuilder.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.InsuranceInfo.Console\bin\Debug\Litium.Lekmer.InsuranceInfo.Console.exe.config"
del /f /q "Src\Litium.Lekmer.InsuranceInfo.Console\bin\Debug\Litium.Lekmer.InsuranceInfo.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.InsuranceInfo.Console\bin\Release\Litium.Lekmer.InsuranceInfo.Console.exe.config"
del /f /q "Src\Litium.Lekmer.InsuranceInfo.Console\bin\Release\Litium.Lekmer.InsuranceInfo.Console.vshost.exe.config"

del /f /q "Src\Litium.Lekmer.InsuranceInfo.Service\bin\Debug\Litium.Lekmer.InsuranceInfo.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.InsuranceInfo.Service\bin\Debug\Litium.Lekmer.InsuranceInfo.Service.exe.config"
del /f /q "Src\Litium.Lekmer.InsuranceInfo.Service\bin\Release\Litium.Lekmer.InsuranceInfo.Service.exe.config"
del /f /q "Src\Litium.Lekmer.InsuranceInfo.Service\bin\Release\Litium.Lekmer.InsuranceInfo.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.MediaArchive.Cleanup.Console\bin\Debug\Litium.Lekmer.MediaArchive.Cleanup.Console.exe.config"
del /f /q "Src\Litium.Lekmer.MediaArchive.Cleanup.Console\bin\Debug\Litium.Lekmer.MediaArchive.Cleanup.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.MediaArchive.Cleanup.Console\bin\Release\Litium.Lekmer.MediaArchive.Cleanup.Console.exe.config"
del /f /q "Src\Litium.Lekmer.MediaArchive.Cleanup.Console\bin\Release\Litium.Lekmer.MediaArchive.Cleanup.Console.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Console\bin\Debug\Litium.Lekmer.NewsletterSubscriber.Console.exe.config"
del /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Console\bin\Debug\Litium.Lekmer.NewsletterSubscriber.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Console\bin\Release\Litium.Lekmer.NewsletterSubscriber.Console.exe.config"
del /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Console\bin\Release\Litium.Lekmer.NewsletterSubscriber.Console.vshost.exe.config"

del /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Service\bin\Debug\Litium.Lekmer.NewsletterSubscriber.Service.exe.config"
del /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Service\bin\Debug\Litium.Lekmer.NewsletterSubscriber.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Service\bin\Release\Litium.Lekmer.NewsletterSubscriber.Service.exe.config"
del /f /q "Src\Litium.Lekmer.NewsletterSubscriber.Service\bin\Release\Litium.Lekmer.NewsletterSubscriber.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.OculusService\bin\Debug\Litium.Lekmer.OculusService.exe.config"
del /f /q "Src\Litium.Lekmer.OculusService\bin\Debug\Litium.Lekmer.OculusService.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.OculusService\bin\Release\Litium.Lekmer.OculusService.exe.config"
del /f /q "Src\Litium.Lekmer.OculusService\bin\Release\Litium.Lekmer.OculusService.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.Cart.Service\bin\Debug\Litium.Lekmer.Cart.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Cart.Service\bin\Debug\Litium.Lekmer.Cart.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Cart.Service\bin\Release\Litium.Lekmer.Cart.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Cart.Service\bin\Release\Litium.Lekmer.Cart.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.Order.MonitorConsole\bin\Debug\Litium.Lekmer.Order.MonitorConsole.exe.config"
del /f /q "Src\Litium.Lekmer.Order.MonitorConsole\bin\Debug\Litium.Lekmer.Order.MonitorConsole.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Order.MonitorConsole\bin\Release\Litium.Lekmer.Order.MonitorConsole.exe.config"
del /f /q "Src\Litium.Lekmer.Order.MonitorConsole\bin\Release\Litium.Lekmer.Order.MonitorConsole.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.Order.Service\bin\Debug\Litium.Lekmer.Order.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Order.Service\bin\Debug\Litium.Lekmer.Order.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Order.Service\bin\Release\Litium.Lekmer.Order.Service.exe.config"
del /f /q "Src\Litium.Lekmer.Order.Service\bin\Release\Litium.Lekmer.Order.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.Payment.Maksuturva.Console\bin\Debug\Litium.Lekmer.Payment.Maksuturva.Console.exe.config"
del /f /q "Src\Litium.Lekmer.Payment.Maksuturva.Console\bin\Debug\Litium.Lekmer.Payment.Maksuturva.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Payment.Maksuturva.Console\bin\Release\Litium.Lekmer.Payment.Maksuturva.Console.exe.config"
del /f /q "Src\Litium.Lekmer.Payment.Maksuturva.Console\bin\Release\Litium.Lekmer.Payment.Maksuturva.Console.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.PageBuilder.Console\bin\Debug\Litium.Lekmer.PageBuilder.Console.exe.config"
del /f /q "Src\Litium.Lekmer.PageBuilder.Console\bin\Debug\Litium.Lekmer.PageBuilder.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.PageBuilder.Console\bin\Release\Litium.Lekmer.PageBuilder.Console.exe.config"
del /f /q "Src\Litium.Lekmer.PageBuilder.Console\bin\Release\Litium.Lekmer.PageBuilder.Console.vshost.exe.config"


del /f /q "Tools\Litium.Lekmer.Payment.Klarna.FetchPClasses\bin\Debug\Litium.Lekmer.Payment.Klarna.FetchPClasses.exe.config"
del /f /q "Tools\Litium.Lekmer.Payment.Klarna.FetchPClasses\bin\Debug\Litium.Lekmer.Payment.Klarna.FetchPClasses.vshost.exe.config"
del /f /q "Tools\Litium.Lekmer.Payment.Klarna.FetchPClasses\bin\Release\Litium.Lekmer.Payment.Klarna.FetchPClasses.exe.config"
del /f /q "Tools\Litium.Lekmer.Payment.Klarna.FetchPClasses\bin\Release\Litium.Lekmer.Payment.Klarna.FetchPClasses.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.Product.MonitorConsole\bin\Debug\Litium.Lekmer.Product.MonitorConsole.exe.config"
del /f /q "Src\Litium.Lekmer.Product.MonitorConsole\bin\Debug\Litium.Lekmer.Product.MonitorConsole.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Product.MonitorConsole\bin\Release\Litium.Lekmer.Product.MonitorConsole.exe.config"
del /f /q "Src\Litium.Lekmer.Product.MonitorConsole\bin\Release\Litium.Lekmer.Product.MonitorConsole.vshost.exe.config"

del /f /q "Src\Litium.Lekmer.Product.MonitorService\bin\Debug\Litium.Lekmer.Product.MonitorService.exe.config"
del /f /q "Src\Litium.Lekmer.Product.MonitorService\bin\Debug\Litium.Lekmer.Product.MonitorService.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Product.MonitorService\bin\Release\Litium.Lekmer.Product.MonitorService.exe.config"
del /f /q "Src\Litium.Lekmer.Product.MonitorService\bin\Release\Litium.Lekmer.Product.MonitorService.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.RatingReviewMessaging.Service\bin\Debug\Litium.Lekmer.RatingReviewMessaging.Service.exe.config"
del /f /q "Src\Litium.Lekmer.RatingReviewMessaging.Service\bin\Debug\Litium.Lekmer.RatingReviewMessaging.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.RatingReviewMessaging.Service\bin\Release\Litium.Lekmer.RatingReviewMessaging.Service.exe.config"
del /f /q "Src\Litium.Lekmer.RatingReviewMessaging.Service\bin\Release\Litium.Lekmer.RatingReviewMessaging.Service.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.Pacsoft.Console\bin\Debug\Litium.Lekmer.Pacsoft.Console.exe.config"
del /f /q "Src\Litium.Lekmer.Pacsoft.Console\bin\Debug\Litium.Lekmer.Pacsoft.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.Pacsoft.Console\bin\Release\Litium.Lekmer.Pacsoft.Console.exe.config"
del /f /q "Src\Litium.Lekmer.Pacsoft.Console\bin\Release\Litium.Lekmer.Pacsoft.Console.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.SIR.Console\bin\Debug\Litium.Lekmer.SIR.Console.exe.config"
del /f /q "Src\Litium.Lekmer.SIR.Console\bin\Debug\Litium.Lekmer.SIR.Console.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.SIR.Console\bin\Release\Litium.Lekmer.SIR.Console.exe.config"
del /f /q "Src\Litium.Lekmer.SIR.Console\bin\Release\Litium.Lekmer.SIR.Console.vshost.exe.config"


del /f /q "Src\Litium.Lekmer.TwosellExport.Service\bin\Debug\Litium.Lekmer.TwosellExport.Service.exe.config"
del /f /q "Src\Litium.Lekmer.TwosellExport.Service\bin\Debug\Litium.Lekmer.TwosellExport.Service.vshost.exe.config"
del /f /q "Src\Litium.Lekmer.TwosellExport.Service\bin\Release\Litium.Lekmer.TwosellExport.Service.exe.config"
del /f /q "Src\Litium.Lekmer.TwosellExport.Service\bin\Release\Litium.Lekmer.TwosellExport.Service.vshost.exe.config"


del /f /q "Tools\Litium.Lekmer.StatistcsUtil.Console\bin\Debug\Litium.Lekmer.StatisticsUtil.Console.exe.config"
del /f /q "Tools\Litium.Lekmer.StatistcsUtil.Console\bin\Debug\Litium.Lekmer.StatisticsUtil.Console.vshost.exe.config"
del /f /q "Tools\Litium.Lekmer.StatistcsUtil.Console\bin\Release\Litium.Lekmer.StatisticsUtil.Console.exe.config"
del /f /q "Tools\Litium.Lekmer.StatistcsUtil.Console\bin\Release\Litium.Lekmer.StatisticsUtil.Console.vshost.exe.config"

del /f /q "Tools\Litium.Lekmer.StatistcsUtil.Service\bin\Debug\Litium.Lekmer.StatistcsUtil.Service.exe.config"
del /f /q "Tools\Litium.Lekmer.StatistcsUtil.Service\bin\Debug\Litium.Lekmer.StatistcsUtil.Service.vshost.exe.config"
del /f /q "Tools\Litium.Lekmer.StatistcsUtil.Service\bin\Release\Litium.Lekmer.StatistcsUtil.Service.exe.config"
del /f /q "Tools\Litium.Lekmer.StatistcsUtil.Service\bin\Release\Litium.Lekmer.StatistcsUtil.Service.vshost.exe.config"

echo.
echo Config files removed.

echo.
pause

exit