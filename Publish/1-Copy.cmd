title Copy all files

echo off

echo.
echo Copy all files...

setlocal

::Source path
set /p "tr=Enter trunk name (Main, Production, Release\Release 000): "
set "sp=D:\Projects\lmheppo\Scensum\trunk\%tr%"
set "xo=/e /c /i /q /h /r /k /y"
set "br=bin\Release"

echo.
echo %sp%

echo.
echo IoC
xcopy "%sp%\Src\IoC\*" "Src\IoC" %xo%

echo.
echo CampaignTagging
xcopy "%sp%\Src\Litium.Lekmer.Campaign.CampaignTagging.Service\%br%\*"            "Src\Litium.Lekmer.Campaign.CampaignTagging.Service\%br%"      %xo%

echo.
echo GiftCardViaMailMessaging
xcopy "%sp%\Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console\%br%\*" "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Console\%br%" %xo%
xcopy "%sp%\Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service\%br%\*" "Src\Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service\%br%" %xo%

echo.
echo Cache.Cleaner
xcopy "%sp%\Src\Litium.Lekmer.Cache.Cleaner.Service\%br%\*"            "Tools\Litium.Lekmer.Cache.Cleaner.Service\%br%"      %xo%

echo.
echo CdonExport
xcopy "%sp%\Src\Litium.Lekmer.CdonExport.Console\%br%\*"               "Src\Litium.Lekmer.CdonExport.Console\%br%"               %xo%
xcopy "%sp%\Src\Litium.Lekmer.CdonExport.Service\%br%\*"               "Src\Litium.Lekmer.CdonExport.Service\%br%"               %xo%

echo.
echo Cleaner
xcopy "%sp%\Src\Litium.Lekmer.Cleaner.Service\%br%\*"                  "Tools\Litium.Lekmer.Cleaner.Service\%br%"            %xo%

echo.
echo DataExport
xcopy "%sp%\Src\Litium.Lekmer.DataExport.Console\%br%\*"               "Src\Litium.Lekmer.DataExport.Console\%br%"               %xo%

echo.
echo DropShip
xcopy "%sp%\Src\Litium.Lekmer.DropShip.Service\%br%\*"                 "Src\Litium.Lekmer.DropShip.Service\%br%"                 %xo%

echo.
echo Esales
xcopy "%sp%\Src\Litium.Lekmer.Esales.Console\%br%\*"                   "Src\Litium.Lekmer.Esales.Console\%br%"                   %xo%
xcopy "%sp%\Src\Litium.Lekmer.Esales.ConsoleDefrag\%br%\*"             "Src\Litium.Lekmer.Esales.ConsoleDefrag\%br%"             %xo%
xcopy "%sp%\Src\Litium.Lekmer.Esales.Service\%br%\*"                   "Src\Litium.Lekmer.Esales.Service\%br%"                   %xo%

echo.
echo FileExport
xcopy "%sp%\Src\Litium.Lekmer.FileExport.Console\%br%\*"               "Src\Litium.Lekmer.FileExport.Console\%br%"               %xo%

echo.
echo GFK
xcopy "%sp%\Src\Litium.Lekmer.GFK.Console\%br%\*"                      "Src\Litium.Lekmer.GFK.Console\%br%"                      %xo%

echo.
echo GoogleFeed
xcopy "%sp%\Src\Litium.Lekmer.GoogleFeed.Console\%br%\*"               "Src\Litium.Lekmer.GoogleFeed.Console\%br%"               %xo%

echo.
echo FtpServer
xcopy "%sp%\Src\Litium.Lekmer.FtpServer.Console\%br%\*"                "Tools\Litium.Lekmer.FtpServer.Console\%br%"          %xo%
xcopy "%sp%\Src\Litium.Lekmer.FtpServer.Service\%br%\*"                "Tools\Litium.Lekmer.FtpServer.Service\%br%"          %xo%

echo.
echo IndexBuilder
xcopy "%sp%\Src\Litium.Lekmer.IndexBuilder.Console\%br%\*"             "Src\Litium.Lekmer.IndexBuilder.Console\%br%"             %xo%
xcopy "%sp%\Src\Litium.Lekmer.IndexBuilder.Service\%br%\*"             "Src\Litium.Lekmer.IndexBuilder.Service\%br%"             %xo%

echo.
echo InsuranceInfo
xcopy "%sp%\Src\Litium.Lekmer.InsuranceInfo.Console\%br%\*"            "Src\Litium.Lekmer.InsuranceInfo.Console\%br%"            %xo%
xcopy "%sp%\Src\Litium.Lekmer.InsuranceInfo.Service\%br%\*"            "Src\Litium.Lekmer.InsuranceInfo.Service\%br%"            %xo%

echo.
echo MediaArchiveCleanup
xcopy "%sp%\Src\Litium.Lekmer.MediaArchive.Cleanup.Console\%br%\*"     "Src\Litium.Lekmer.MediaArchive.Cleanup.Console\%br%"     %xo%

echo.
echo NewsletterSubscriber
xcopy "%sp%\Src\Litium.Lekmer.NewsletterSubscriber.Console\%br%\*"     "Src\Litium.Lekmer.NewsletterSubscriber.Console\%br%"     %xo%
xcopy "%sp%\Src\Litium.Lekmer.NewsletterSubscriber.Service\%br%\*"     "Src\Litium.Lekmer.NewsletterSubscriber.Service\%br%"     %xo%

echo.
echo OculusService
xcopy "%sp%\Src\Litium.Lekmer.OculusService\%br%\*"                    "Src\Litium.Lekmer.OculusService\%br%"                    %xo%

echo.
echo Cart.Service
xcopy "%sp%\Src\Litium.Lekmer.Cart.Service\%br%\*"                     "Src\Litium.Lekmer.Cart.Service\%br%"                    %xo%

echo.
echo OrderMonitor
xcopy "%sp%\Src\Litium.Lekmer.Order.MonitorConsole\%br%\*"             "Src\Litium.Lekmer.Order.MonitorConsole\%br%"             %xo%

echo.
echo Order.Service
xcopy "%sp%\Src\Litium.Lekmer.Order.Service\%br%\*"                    "Src\Litium.Lekmer.Order.Service\%br%"                    %xo%

echo.
echo MaksuturvaConsole
xcopy "%sp%\Src\Litium.Lekmer.Payment.Maksuturva.Console\%br%\*"       "Src\Litium.Lekmer.Payment.Maksuturva.Console\%br%"       %xo%

echo.
echo PageBuilder
xcopy "%sp%\Src\Litium.Lekmer.PageBuilder.Console\%br%\*"              "Src\Litium.Lekmer.PageBuilder.Console\%br%"              %xo%

echo.
echo Payment.Klarna.FetchPClasses
xcopy "%sp%\Src\Litium.Lekmer.Payment.Klarna.FetchPClasses\%br%\*"     "Tools\Litium.Lekmer.Payment.Klarna.FetchPClasses\%br%"   %xo%

echo.
echo Product.Monitor
xcopy "%sp%\Src\Litium.Lekmer.Product.MonitorConsole\%br%\*"           "Src\Litium.Lekmer.Product.MonitorConsole\%br%"           %xo%
xcopy "%sp%\Src\Litium.Lekmer.Product.MonitorService\%br%\*"           "Src\Litium.Lekmer.Product.MonitorService\%br%"           %xo%

echo.
echo RatingReviewMessaging
xcopy "%sp%\Src\Litium.Lekmer.RatingReviewMessaging.Service\%br%\*"    "Src\Litium.Lekmer.RatingReviewMessaging.Service\%br%"    %xo%

echo.
echo Pacsoft
xcopy "%sp%\Src\Litium.Lekmer.Pacsoft.Console\%br%\*"    "Src\Litium.Lekmer.Pacsoft.Console\%br%"    %xo%

echo.
echo SIR
xcopy "%sp%\Src\Litium.Lekmer.SIR.Console\%br%\*"    "Src\Litium.Lekmer.SIR.Console\%br%"    %xo%

echo.
echo TwosellExport
xcopy "%sp%\Src\Litium.Lekmer.TwosellExport.Service\%br%\*"    "Src\Litium.Lekmer.TwosellExport.Service\%br%"    %xo%

echo.
echo StatistcsUtil
xcopy "%sp%\Tools\Litium.Lekmer.StatisticsUtil.Console\%br%\*"         "Tools\Litium.Lekmer.StatistcsUtil.Console\%br%"          %xo%
xcopy "%sp%\Tools\Litium.Lekmer.StatisticsUtil.Service\%br%\*"         "Tools\Litium.Lekmer.StatistcsUtil.Service\%br%"          %xo%

echo.
echo All files copied.

endlocal

echo.
pause

exit